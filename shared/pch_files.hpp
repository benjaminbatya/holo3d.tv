#ifndef __BOOST_PCH_HPP__
#define __BOOST_PCH_HPP__

// This is used to help compile times for boost includes
// Include this whereever boost is to speed up the processing.
// NOTE: Only helps with processing mostly non-template classes

#include <boost/asio.hpp>
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <boost/system/error_code.hpp>
#include <boost/lockfree/spsc_queue.hpp>

#include <boost/bind.hpp>
#include <boost/pool/pool_alloc.hpp>

#include <boost/array.hpp>
#include <boost/atomic.hpp>

#include <boost/filesystem.hpp> 
#include <boost/program_options.hpp>

#if defined OPEN_CL_ENABLED

#   define __CL_ENABLE_EXCEPTIONS

// We are only using OpenCL 1.1 for now. It's the only distributed version
#   define CL_USE_DEPRECATED_OPENCL_1_1_APIS
#   include <CL/cl.h>
#   undef CL_VERSION_1_2
#   include <CL/cl.hpp>

#   include <boost/compute.hpp>
#endif // defined OPEN_CL_ENABLED

#endif //__BOOST_PCH_HPP__
