#ifndef _DEVICE_BASE_HPP_
#define _DEVICE_BASE_HPP_

namespace holo3d
{

class camera_frame;

class device_base
{
public:
    virtual ~device_base() = default;

    virtual bool read_frame(camera_frame&) = 0;

};

}; // namespace holo3d

#endif // _DEVICE_BASE_HPP_
