#ifndef _CAMERA_INFO_HPP_
#define _CAMERA_INFO_HPP_

#include "fragmenting_socket.hpp"
#include "fps_counter.hpp"
#include "frame.hpp"

namespace holo3d
{

class camera_info : public std::enable_shared_from_this<camera_info>
{
public:
    camera_info(boost::asio::io_service& service, boost::asio::ip::udp::endpoint endpoint, uint16_t local_port);
    ~camera_info();

    const STREAM_HEADER& header() const { return m_header; }

    bool valid() const { return header().width>0 && header().height>0; }

    std::string description() const { return m_description; }

    boost::asio::ip::udp::endpoint endpoint() const { return m_endpoint; }
    uint16_t local_port() const { return m_local_port; }

    bool pop_frame(camera_frame& frame) { return m_frame_queue.pop(frame); }

    // void run(uint64_t start_time);

    typedef std::weak_ptr<camera_info> ptr_t;
    // typedef camera_info* ptr_t;
    typedef std::function<void(ptr_t)> inform_callback_t;
    void inform_on_file_header(inform_callback_t fn) { m_inform_on_file_header_cb = fn; }

    void handle_camera_reply(const boost::system::error_code& error,
                             const size_t& bytes_sent);

    void set_new_frame_cb(inform_callback_t fn) { m_new_frame_cb = fn; }

    void set_end_stream_cb(inform_callback_t fn) { m_end_stream_cb = fn; }
    
protected:

    void handle_new_stream(const boost::system::error_code& error,
                           const size_t& bytes_recvd);
    void handle_new_frame(const boost::system::error_code& error,
                          const size_t& bytes_recvd);
    void handle_color_buffer(const boost::system::error_code& error,
                             const size_t& bytes_recvd);
    void handle_depth_buffer(const boost::system::error_code& error,
                             const size_t& bytes_recvd);
    void finish_handling_frame();

    fragmenting_socket  m_socket;   // << used to send and receive data inside the camera_info

    boost::asio::ip::udp::endpoint m_endpoint; // << The endpoint of the camera being listened to

    std::string         m_description;      // << A description of the camera and its local port

    uint16_t            m_local_port = 0;   // << port on localhost to listen for frames from camera

    STREAM_HEADER         m_header;

    camera_frame        m_frame;

    typedef boost::lockfree::spsc_queue<camera_frame> frame_queue_t;

    frame_queue_t       m_frame_queue { 30 };     // << The queue holds up to a second worth of frames

    // This is only used to receive data from the camera for the FILE_HEADER and FRAME_HEADER.
    // NOTE: Maybe replace this with a frame from the frame_queue and 
    // then reference the color and depth data as offsets into that queue. 
    static const size_t MAX_BUFFER_SIZE = 0xff;
    uint8_t             m_receive_buffer[MAX_BUFFER_SIZE];

    fps_counter         m_fps;

    inform_callback_t   m_inform_on_file_header_cb  = nullptr;  // Called when a FILE_HEADER is received
    inform_callback_t   m_new_frame_cb              = nullptr;  // Called when a new frame is received
    inform_callback_t   m_end_stream_cb             = nullptr;  // Called when a END_STREAM_MESSAGE is received
};

}; // namespace holo3d


#endif // _CAMERA_INFO_HPP_
