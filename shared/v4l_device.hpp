#ifndef _V4L_DEVICE_HPP_
#define _V4L_DEVICE_HPP_

#include "device_base.hpp"

namespace holo3d
{

class v4l_device : public device_base
{
public:
    virtual ~v4l_device();

    virtual bool read_frame(camera_frame &) override;

protected:
    friend class v4l_factory;

    v4l_device();

    bool init(int desired_fps, int desired_width, std::string uri);
    void uninit();

    bool start_capturing();
    bool stop_capturing();

    int             m_fd = -1;

    struct buffer 
    {
        void   *start;
        size_t  length;
    };

    buffer*         m_buffers = nullptr;
    uint32_t        m_num_buffers = 0;
};

}; // namespace holo3d

#endif // _V4L_DEVICE_HPP_
