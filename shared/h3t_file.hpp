#ifndef _H3T_FILE_
#define _H3T_FILE_

#include <stdint.h>

namespace holo3d 
{

const std::string   DEFAULT_FILE_EXTENSION  = "h3t";    // << This is the default file extension for holo3d
const std::string   DEFAULT_PORT            = "15677";   // << This is the default port (0x3D3D)

/**
 * If FILE_HEADER::max_num_of_regions == 0, then:
 * The layout of the .h3t file format is 
 * FILE_HEADER - this is the stream header 
 *   FRAME_HEADER #1 
 *     Color Frame Data #1 
 *     Depth Frame Data #1 
 *   FRAME_HEADER #2 
 *     Color Frame Data #2 
 *     Depth Frame Data #2 
 * . 
 * . 
 * . 
 * EOF 
 * It is aligned on 32bit...
 *  
 **/ 

// This magic follows PNG closely
const char FILE_MAGIC[8] = {'\211', 'H', '3', 'T', '\r', '\n', '\032', '\n'};

// The version numbers
const uint16_t MAJOR_VERSION = 0;
const uint16_t MINOR_VERSION = 8;

enum class STREAM_FORMAT : uint8_t
{
    COLOR_RGB888 = 0        // << RGB24 (8bits/channel, 3 channels per pixel) format, know as RAW by libyuv
    , COLOR_YUV422 = 1      // << YUV422 (1byte luminance for every 2x1 (XxY) subsampled block of u,v) 1.5 bytes/pixel
    , COLOR_YUYV = 2        // << YUY2 in FOURCC notation. Each 4 byte block contains hortizontal 2 pixels.
    , COLOR_YV12 = 3        // << encoding format required by the cp8 encoder. It is I420 but with the u and v planes switched
    , COLOR_JPEG = 4        // << JPEG encoding format, its input and output has to be RGB24
    , COLOR_VP8 = 5         // << The VP8 encoding format, Its input and output has to be I420 (???)
//  , COLOR_H264 = 6        // << The H264 format, not implemented yet

    , DEPTH_RAW = 7         // << Raw depth data in 16-bits(uint16_t)/pixel format
    , DEPTH_ENC = 8         // << Depth data in LHaHb format (8bits/channel) from the paper "Adapting Standard Video Codecs for Depth Streaming"
    , DEPTH_JPEG = 9        // << Encoded depth data in JPEG format. Input and output is in the LHaHb format from above and was encoded using a table lookup
    , DEPTH_PNG = 10        // << Encoded depth data in PNG format.
    , DEPTH_VP8 = 11        // << Encoded depth data in VP8 format.
//  , DEPTH_H264 = 12

    , INVALID = 0xFF        // << Indicates that the stream format is invalid. Used to indicate no depth or color buffer
};

class STREAM_HEADER
{
public:
    STREAM_HEADER()
    {
        // Copy the magic over. 
        // We're not using memcpy to avoid including <cstring> in this base header
        for (uint16_t i=0; i<sizeof(FILE_MAGIC); ++i)
        {
            other[i] = FILE_MAGIC[i];
        }
        major_version = MAJOR_VERSION;
        minor_version = MINOR_VERSION;
    }

    union { 
        uint64_t magic;             // << The magic number for the holo3d.tv (.h3t) file format
        char other[8];              // << Allows not using memcpy
    };
    uint8_t major_version;          // << The major version number
    uint8_t minor_version;          // << The minor version number
                                    
    STREAM_FORMAT color_format;     // << format of each color frame
    STREAM_FORMAT depth_format;     // << format of each depth frame

    // NOTE: Width and height can be part of the intrinsic camera parameters later
    uint16_t width;                 // << The full width of the camera which recorded the stream
    uint16_t height;                // << The full height of the camera which recorded the stream

    // NOTE: The stream is unique by the combination of it's camera id and it's tracker id.
    // The tracker id is the id of the target being tracked by this stream
    uint16_t camera_id;             // << The camera id of this stream
    uint16_t tracker_id;            // << The tracker id of this stream. 

    // TODO: Add the intrinsic and extrinsic camera parameters of the camera which recorded this stream

    bool has_color() const { return color_format != STREAM_FORMAT::INVALID; }
    bool has_depth() const { return depth_format != STREAM_FORMAT::INVALID; }
};

struct RECT
{
    uint16_t    x;              // << The left-starting point of the region relative to the frame
    uint16_t    y;              // << The top-starting point of the region relative to the frame
    uint16_t    width;          // << The width of the region
    uint16_t    height;         // << The height of the region

    // EMPTY_RECT is actually defined in encoder.cpp
    // TODO: Make a h3t_file.cpp for this and the various stream operators
    static const RECT EMPTY;

    bool is_empty() const { return width == EMPTY.width && height == EMPTY.height; }
};

extern const RECT EMPTY;

struct FRAME_HEADER
{
    uint64_t frame_number;          // << The frame number of this frame
                                    
    uint64_t color_time_stamp;      // << The color frame timestamp. The upper 32 bits are seconds in posix format, the lower 32 bits are microseconds
    uint64_t depth_time_stamp;      // << The depth frame timestamp. Not necessarily the same as the color frame timestamp                   
                                    
    RECT rect;                      // << The rectangle describing the coverage of the frame

    uint32_t color_buffer_size;      // << size of each color frame in bytes
    uint32_t depth_buffer_size;      // << size of each depth frame in bytes
};

const uint64_t END_STEAM_MESSAGE = (uint64_t)-1; // << FRAME_HEADER.frame_number is set to this when the stream is finished

/** Holds the value of a single depth pixel in a 16bit short */
typedef uint16_t    DepthPixel;
                                   
/** Holds the value of a single color image pixel in 24-bit RGB format. */
typedef struct
{
	/* Red value of this pixel. */
	uint8_t r;
	/* Green value of this pixel. */
	uint8_t g;
	/* Blue value of this pixel. */
	uint8_t b;
} RGB888Pixel;

/**
 Holds the value of two pixels in YUV422 format (Luminance/Chrominance,16-bits/pixel).
 The first pixel has the values y1, u, v.
 The second pixel has the values y2, u, v. 
 NOTE: YUYV format is also 16bits/pixel but the layout is 
 different. 
*/ 
typedef struct
{
	/** First chrominance value for two pixels, stored as blue luminance difference signal. */
	uint8_t u;
	/** Overall luminance value of first pixel. */
	uint8_t y1;
	/** Second chrominance value for two pixels, stored as red luminance difference signal. */
	uint8_t v;
	/** Overall luminance value of second pixel. */
	uint8_t y2;
} YUV422DoublePixel;

/**
 Holds the value of two pixels in YUYV format 
 (Luminance/Chrominance,16-bits/pixel). 
*/ 
typedef struct
{
    /** Overall luminance value of first pixel. */
    uint8_t y1;
	/** First chrominance value for two pixels, stored as blue luminance difference signal. */
	uint8_t u;
	/** Overall luminance value of second pixel. */
	uint8_t y2;
	/** Second chrominance value for two pixels, stored as red luminance difference signal. */
	uint8_t v;
} YUYVDoublePixel;

// Returns the number of bytes per pixel for a given STREAM_FORMAT
size_t get_pixel_depth(const STREAM_FORMAT& fmt);

struct NEW_CAMERA_REQUEST
{
    uint32_t    ip_address;
};

struct NEW_CAMERA_REPLY
{
    NEW_CAMERA_REPLY() { }
    NEW_CAMERA_REPLY(const uint32_t& port, const uint64_t& st) { assigned_port = port; start_time = st;}

    uint64_t    start_time = 0;
    uint32_t    assigned_port = 0;
    uint32_t    padding;
};

enum class RESOLUTION : uint8_t
{
    SMALL,  // << 320x240
    MEDUIM, // << 640x480
    // LARGE,  // << 1024x768

    MIN = SMALL,
    MAX = MEDUIM, // LARGE,
    COUNT = MAX+1
};

enum class QUALITY : uint8_t
{
    REALTIME = 0,
    AVERAGE,
    GOOD,
    BEST,

    MIN = REALTIME,
    MAX = BEST,
    COUNT = MAX+1
};


// These are all of the different message types, 
// NOTE: maybe in the future use a hash of the message structure name as the BLOCK_TYPE id
enum class BLOCK_TYPE : uint16_t
{
    INVALID             = 0
    , REGISTER          = 1
    , QUERY             = 2
    , QUERY_ACK         = 3
    , FULL_FRAME        = 4
    , FULL_FRAME_ACK    = 5
    , CAMERA_PARAMS     = 6
    , LOCATION_UPDATE   = 7
    , ASSIGN_PROCESS    = 8
    , START_TRACKING    = 9
    , STOP_TRACKING     = 10
    , IMAGE_FRAGMENT    = 11
    , UPDATE_POSE       = 12
};

struct BLOCK_BASE
{
    uint16_t block_id;          // << This is the id of the block
    uint16_t block_version;     // << This is the version number of the block
};

// A couple useful stream operators
std::ostream& operator<<(std::ostream& os, const STREAM_FORMAT& pf);
std::ostream& operator<<(std::ostream& os, const NEW_CAMERA_REQUEST& c);
std::ostream& operator<<(std::ostream& os, const NEW_CAMERA_REPLY& c);
std::ostream& operator<<(std::ostream& os, const STREAM_HEADER& header);
std::ostream& operator<<(std::ostream& os, const RECT& rect);
std::ostream& operator<<(std::ostream& os, const FRAME_HEADER& header);

std::ostream& operator<<(std::ostream& os, const QUALITY& quality);
std::istream& operator>>(std::istream& is, QUALITY& quality);

std::ostream& operator<<(std::ostream& os, const RESOLUTION& mode);
std::istream& operator>>(std::istream& is, RESOLUTION& mode);

} // namespace holo3d

#endif // _H3T_FILE_
