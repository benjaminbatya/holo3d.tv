#ifndef _ONI_HOLO_3D_UTILS_
#define _ONI_HOLO_3D_UTILS_

inline std::ostream& operator<<(std::ostream& os, const openni::PixelFormat& pf)
{
#define OUTPUT_PIXEL_FORMAT(e) case openni::e : os << #e; break;

    switch (pf)
    {
    // Depth
    OUTPUT_PIXEL_FORMAT(PIXEL_FORMAT_DEPTH_1_MM);
    OUTPUT_PIXEL_FORMAT(PIXEL_FORMAT_DEPTH_100_UM);
    OUTPUT_PIXEL_FORMAT(PIXEL_FORMAT_SHIFT_9_2);
    OUTPUT_PIXEL_FORMAT(PIXEL_FORMAT_SHIFT_9_3);

    // Color
    OUTPUT_PIXEL_FORMAT(PIXEL_FORMAT_RGB888);
    OUTPUT_PIXEL_FORMAT(PIXEL_FORMAT_YUV422);
    OUTPUT_PIXEL_FORMAT(PIXEL_FORMAT_GRAY8);
    OUTPUT_PIXEL_FORMAT(PIXEL_FORMAT_GRAY16);
    OUTPUT_PIXEL_FORMAT(PIXEL_FORMAT_JPEG);
    OUTPUT_PIXEL_FORMAT(PIXEL_FORMAT_YUYV);
    default: os << "Unknown pixel format"; break;
    }
#undef OUTPUT_PIXEL_FORMAT

    return os;
}

inline std::ostream& operator<<(std::ostream& os, const openni::VideoMode& mode)
{
    os  << "Resolution=" << mode.getResolutionX() << "x" << mode.getResolutionY()
        << ", Fps=" << mode.getFps() << ", PixelFormat=" << mode.getPixelFormat();

    return os;
}

inline std::ostream& operator<<(std::ostream& os, const openni::SensorType& st)
{
    os  << ((st==openni::SENSOR_COLOR) ? "COLOR" : ((st==openni::SENSOR_DEPTH) ? "DEPTH" : "IR"));
    return os;
}

inline std::ostream& operator<<(std::ostream& os, const openni::ImageRegistrationMode& mode)
{
    os << ((mode==openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR) ? "IMAGE_REGISTRATION_DEPTH_TO_COLOR" : "IMAGE_REGISTRATION_OFF");
    return os;
}

#endif // _ONI_HOLO_3D_UTILS_
