
#ifndef _UTIL_FUNCS_H_
#define _UTIL_FUNCS_H_

#include <stdexcept>
#include <iostream>
#include <sstream>
#include <sys/time.h>

const int MICROS_PER_SEC = 1000000;

bool wasKeyboardHit();

void Sleep(int millisecs);

inline uint64_t get_time()
{
    timeval time_struct;
    gettimeofday(&time_struct, NULL); //This actually returns a struct that has microsecond precision.
    return ((unsigned long long)time_struct.tv_sec * MICROS_PER_SEC) + time_struct.tv_usec;
}

enum class VERBOSE_LEVEL : uint8_t
{
    INFO = 0
    , WARN = 1
    , ERROR = 2
    , FATAL = 3

    , ALWAYS = 255
};
void set_verbose_level(VERBOSE_LEVEL new_level);
VERBOSE_LEVEL verbose_level();

// Start INFO and ERROR, code borrowed from https://gist.github.com/4poc/3155033

inline const std::string method_name(const std::string& long_name)
{
    size_t end = long_name.rfind("(");
    size_t begin = long_name.rfind(" ", end) + 1;
    
    return long_name.substr(begin, end - (begin));
}

inline void _OUT_(boost::format& fmt) {
    // This is the base case. Just return..
}

template<typename TValue, typename... TArgs>
void _OUT_(boost::format& fmt, TValue arg, TArgs... args)
{ 
    fmt % arg;

    _OUT_(fmt, args...);
}

template<typename... TArgs>
void _INFO_(const std::string& file, size_t line, const std::string& func_name, const std::string& msg, TArgs... args)
{
    if (verbose_level() <= VERBOSE_LEVEL::INFO) 
    {
        // boost::format fmt = boost::format("INFO %s:%d(%s): ") % file % line % method_name(func_name);
        boost::format fmt = boost::format("%s: ") % method_name(func_name);
        fmt = boost::format(fmt.str() + msg);

        _OUT_(fmt, args...);

        std::cout << fmt.str() << std::endl;
    }
}

#define INFO(...)   _INFO_(__FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)

template<typename... TArgs>
void _WARN_(const std::string& file, size_t line, const std::string& func_name, const std::string& msg, TArgs... args)
{
    if (verbose_level() <= VERBOSE_LEVEL::WARN) 
    {
        // boost::format fmt = boost::format("INFO %s:%d(%s): ") % file % line % method_name(func_name);
        boost::format fmt = boost::format("WARN %s: ") % method_name(func_name);
        fmt = boost::format(fmt.str() + msg);

        _OUT_(fmt, args...);

        std::cout << fmt.str() << std::endl;
    }
}

#define WARN(...)   _WARN_(__FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)

template<typename... TArgs>
void _ERROR_(const std::string& file, size_t line, const std::string& func_name, const std::string& msg, TArgs... args)
{
    if (verbose_level() <= VERBOSE_LEVEL::ERROR) 
    {
        boost::format fmt = boost::format("ERROR %s:%d(%s): ") % file % line % method_name(func_name);
        fmt = boost::format(fmt.str() + msg);

        _OUT_(fmt, args...);

        std::cerr << fmt.str() << std::endl;
    }
}

#define ERROR(...)  _ERROR_(__FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)

template<typename... TArgs>
void _FATAL_(const std::string& file, size_t line, const std::string& func_name, const std::string& msg, TArgs... args)
{
    if (verbose_level() <= VERBOSE_LEVEL::FATAL) 
    {
        boost::format fmt = boost::format("FATAL %s:%d(%s): ") % file % line % method_name(func_name);
        fmt = boost::format(fmt.str() + msg);

        _OUT_(fmt, args...);

        std::cerr << fmt.str() << std::endl;
    }
}

#define FATAL(...)  _FATAL_(__FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)

template<typename... TArgs>
void _ALWAYS_(const std::string& file, size_t line, const std::string& func_name, const std::string& msg, TArgs... args)
{
    if (verbose_level() <= VERBOSE_LEVEL::ALWAYS) 
    {
        boost::format fmt = boost::format("ALWAYS %s:%d(%s): ") % file % line % method_name(func_name);
        fmt = boost::format(fmt.str() + msg);

        _OUT_(fmt, args...);

        std::cout << fmt.str() << std::endl;
    }
}

#define ALWAYS(...)  _ALWAYS_(__FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)

// End INFO and ERROR

#define INFO_CPP(...) { std::cout << method_name(__PRETTY_FUNCTION__) << ": " << __VA_ARGS__ << std::endl; }

#define ERROR_CPP(...) { \
    std::cerr   <<  __FILE__ << ", line " << __LINE__ << "(" << method_name(__PRETTY_FUNCTION__) \
                << "): error: " << __VA_ARGS__ \
                << std::endl; \
}

template<typename... TArgs>
void _THROW_(const std::string& file, size_t line, const std::string& func_name, const std::string& msg, TArgs... args)
{
    boost::format fmt = boost::format("%s:%d(%s): ") % file % line % method_name(func_name);
    fmt = boost::format(fmt.str() + msg);

    _OUT_(fmt, args...);

    std::runtime_error e(fmt.str());
    throw e;
}

#define THROW(...) _THROW_(__FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)

#define THROW_CPP(...) \
{ \
    std::stringstream ss; \
    ss << "file '" << __FILE__ << "', line " << __LINE__ << "(" << method_name(__PRETTY_FUNCTION__) << \
    "): " << __VA_ARGS__; \
    std::runtime_error e(ss.str()); \
    throw e; \
}

// This is a always on sanity checker. Dirty because it's hard to catch
#define ASSERT_ALWAYS(expr) { if(!(expr)) THROW(#expr); }
#define ASSERT_THROW_ALWAYS(expr, ...) if(!(expr)) { THROW(__VA_ARGS__); }
#define ASSERT_EQUAL_ALWAYS(result, expected) ASSERT_ALWAYS((result) == (expected))
#define ASSERT_EQUAL_THROW_ALWAYS(result, expected, ...) if(!((result) == (expected))) { THROW(__VA_ARGS__); }

#ifdef NDEBUG

// Do the expression to avoid used warnings
#define ASSERT(expr) ((void)(expr)) 
#define ASSERT_THROW(expr, ...) ((void)(expr));

#define ASSERT_EQUAL(result, expected) ((void)(result)); ((void)(expected));
#define ASSERT_EQUAL_THROW(result, expected, ...) ((void)(result)); ((void)(expected));

#else // NDEBUG

#include <assert.h>
#define ASSERT(expr) assert((expr))
#define ASSERT_THROW(expr, ...) if(!(expr)) { THROW(__VA_ARGS__); }

#define ASSERT_EQUAL(result, expected) assert((result) == (expected))
#define ASSERT_EQUAL_THROW(result, expected, ...) if(!((result) == (expected))) { THROW(__VA_ARGS__); }


/*
inline void ASSERT(expr, ...)
{
    if(!(expr))
    {
        snprintf(__THROW_STR__, __MAX_STR_LEN__, "ASSERT(#expr) failed!\n");
        // snprintf(__THROW_STR__+strlen(__THROW_STR__), __MAX_STR_LEN__-strlen(__THROW_STR__), "ERROR: file '%s', line %d: error: ", __FILE__, __LINE__);

        va_list args;
        va_start(args, fmt);
        vsnprintf(__THROW_STR__+strlen(__THROW_STR__), __MAX_STR_LEN__-strlen(__THROW_STR__), fmt, args);
        va_end(args);

        fprintf(stderr, "%s\n", __THROW_STR__);
        throw __THROW_STR__;
    }
}

inline void EQUALS(test, expected, fmt...)
{
    if(!(expected == test))
    {
        snprintf(__THROW_STR__, __MAX_STR_LEN__, "EQUAL(#test, #expected) failed!\n");
        // snprintf(__THROW_STR__+strlen(__THROW_STR__), __MAX_STR_LEN__-strlen(__THROW_STR__), "ERROR: file '%s', line %d: error: ", __FILE__, __LINE__);

        va_list args;
        va_start(args, fmt);
        snprintf(__THROW_STR__+strlen(__THROW_STR__), __MAX_STR_LEN__-strlen(__THROW_STR__), fmt, args);
        va_end(args);

        fprintf(stderr, "%s\n", __THROW_STR__);
        throw __THROW_STR__;
    }
}
*/

#endif // NDEBUG

#define ASSERT_EQUALS ASSERT_EQUAL

#endif // _UTIL_FUNCS_H_
