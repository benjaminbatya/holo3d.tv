#ifndef _OPENNI_DEVICE_HPP_
#define _OPENNI_DEVICE_HPP_

#include <OpenNI2/OpenNI.h>

#include "device_base.hpp"

namespace holo3d
{

class camera_frame;
class openni_factory;

class openni_device : public device_base
{
public:
    virtual ~openni_device();

    void toggle_mirroring();

    virtual bool read_frame(camera_frame&) override;

protected:
    friend class openni_factory;

    openni_device(std::string uri);

    bool init(int desired_fps, int desired_width, bool enable_ir = FALSE);

    void create_stream(openni::VideoStream& stream, openni::SensorType sensor_type, openni::PixelFormat pix_fmt, int width, int fps);
    void shutdown();

    openni::VideoMode select_target_video_mode(const openni::SensorType& sensor_type,
                                               const openni::PixelFormat& required_pixel_format,
                                               const int& width,
                                               const int& required_fps = 30);

    void set_mirroring();

    openni::Status wait_for_changed_stream(openni::SensorType& updated_sensor);
    openni::Status read_frame_helper(const openni::SensorType& updated_sensor); 

    openni::Device          m_device;

    openni::VideoStream     m_color_stream;
    openni::VideoStream     m_depth_stream;
    openni::VideoStream     m_ir_stream;

    openni::VideoStream**   m_streams = nullptr;
    int                     m_num_streams = 0;

    bool                    m_enable_ir = false;
    bool                    m_mirrored = false;

    openni::VideoFrameRef   m_frame;

    int                     m_fps = 30;
};

}; // namespace holo3d

#endif // _OPENNI_DEVICE_HPP_
