#ifndef __LOADER_H_
#define __LOADER_H_

#include <cstdio>
#include <string>

#include "frame.hpp"

namespace holo3d
{

// Forward declaration
class camera_frame;

class loader_base
{
protected:
    loader_base();

public:

    virtual ~loader_base();

    virtual bool valid() const { return false; }

    virtual bool run() = 0;

    typedef std::function<void(void)> inform_cb_t;
    void load_handler(inform_cb_t fn) { m_load_handler = fn; }

    void set_end_stream_handler(inform_cb_t fn) { m_end_handler = fn; }

    /**
     * Returns a string containing the connection info
     * 
     * @author benjamin (12/1/2014)
     * 
     * @return std::string 
     */
    virtual std::string connection_info() const = 0;
    /**
     * Returns the next frame if possible
     * 
     * @author benjamin (11/28/2014)
     * 
     * @param frame_ptr the next frame's data
     * 
     * @return bool true if the frame was loaded properly, false otherwise
     */
    virtual bool next_frame(camera_frame& frame_ptr) = 0;

    size_t width() const { return m_file_header.width; }
    size_t height() const { return m_file_header.height; }

    const STREAM_HEADER& file_header() const { return m_file_header; }

protected:

    STREAM_HEADER m_file_header;

    inform_cb_t m_load_handler = nullptr;   // << called when the the stream is beginning

    inform_cb_t m_end_handler = nullptr;    // << called when the stream is finished
};

class file_loader : public loader_base
{
public:
    file_loader(const std::string& file_name);
    ~file_loader();

    bool valid() const;

    /**
     * load (or reload) the loader
     * 
     * @author benjamin (12/1/2014)
     */
    bool run();

    std::string connection_info() const 
    {
        return m_file_name;
    }

    /**
     * Loads the next frame from the stream
     * 
     * @author benjamin (11/9/2014)
     * 
     * @param frame_ptr pointer to the frame to be loaded. If 
     *                  successful, frame_ptr is populated with the
     *                  decoded frame data, otherwise, frame_ptr is
     *                  set to NULL. If there are no more frames to
     *                  load, frame_ptr is also set to NULL
     *  
     * @return true if the next frame was successfully read, false 
     *         otherwise
     */
    bool next_frame(camera_frame& frame_ptr);

private:

    void shutdown();

    std::string m_file_name;
    FILE*       m_stream;
};

class udp_loader_impl;

class udp_loader : public loader_base
{
public:
    udp_loader(std::string port);
    ~udp_loader();

    bool valid() const;

    /**
     * Never returns
     * 
     * @author benjamin (12/1/2014)
     * 
     * @return bool returns true
     */
    bool run();

    std::string connection_info() const;

    virtual bool next_frame(camera_frame &frame_ptr);

protected:

    friend class udp_loader_impl;

    udp_loader_impl*    m_impl;
};


}; // namespace holo3d

#endif // __LOADER_H_
