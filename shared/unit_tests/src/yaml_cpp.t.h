#include <cxxtest/TestSuite.h>

#include <yaml-cpp/yaml.h>

class test_yaml : public CxxTest::TestSuite
{

public:

    /**
     * Simple test from 
     * https://github.com/jbeder/yaml-cpp/wiki/Tutorial 
     * 
     * @author benjamin (4/22/2015)
     * @param  
     */
void test_simple()
{
    YAML::Node node = YAML::Load("[1, 2, 3]");
    TS_ASSERT_EQUALS(node.Type(), YAML::NodeType::Sequence);
    TS_ASSERT(node.IsSequence());

    YAML::Node primes = YAML::Load("[2, 3, 5, 7, 11]");
    TS_ASSERT_EQUALS(primes[0].as<int>(), 2);
    TS_ASSERT_EQUALS(primes[1].as<int>(), 3);
    TS_ASSERT_EQUALS(primes[2].as<int>(), 5);
    TS_ASSERT_EQUALS(primes[3].as<int>(), 7);
    TS_ASSERT_EQUALS(primes[4].as<int>(), 11);

    primes.push_back(13);
    TS_ASSERT_EQUALS(primes.size(), 6);

    YAML::Emitter out;
    out << YAML::Flow << primes;
    TS_ASSERT_EQUALS("[2, 3, 5, 7, 11, 13]", out.c_str());
};

};
