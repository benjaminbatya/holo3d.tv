#include <cxxtest/TestSuite.h>

#include <boost/asio.hpp>
#include <boost/format.hpp>
#include <boost/array.hpp>

#include <thread>
#include <random>

#include "h3t_file.hpp"
#include "util_funcs.hpp"

#include "fragmenting_socket.hpp"

using boost::asio::ip::udp;
using namespace holo3d;

class test_fragmenting_socket : public CxxTest::TestSuite
{


boost::asio::io_service m_service;
boost::asio::io_service::work m_work { m_service };

fragmenting_socket m_send_socket { m_service, 0, 0 };
fragmenting_socket m_receive_socket { m_service, 0, 0 };

typedef std::minstd_rand0 random_number_generator_t;
random_number_generator_t m_random { (random_number_generator_t::result_type)get_time() };

public:

void setUp()
{
    m_service.reset();

    boost::system::error_code ec;
    udp::resolver resolver(m_service);
    udp::resolver::query query(udp::v4(), "localhost", holo3d::DEFAULT_PORT);
    m_send_socket.endpoint() = *resolver.resolve(query, ec);
    TS_ASSERT(!ec);

//  INFO("opening sending socket");
    ec = m_send_socket.open();
    TSM_ASSERT(ec.message(), !ec);
    m_send_socket.do_yield(true);

//  INFO("binding socket");
    m_receive_socket.endpoint().port(atoi(holo3d::DEFAULT_PORT.c_str()));

    ec = m_receive_socket.bind();
    TSM_ASSERT(ec.message(), !ec);
}

void tearDown()
{
    boost::system::error_code ec;
    ec = m_receive_socket.close();
    TSM_ASSERT(ec.message(), !ec);
    ec = m_send_socket.close();
    TSM_ASSERT(ec.message(), !ec);

    m_service.stop();
}

inline uint8_t gen_value()
{
    return (uint8_t)m_random();
}

void test_service(void)
{
    int num_waits = 0;
    std::thread service_thread;
    boost::asio::deadline_timer timer(m_service);

    service_thread = std::thread([&] ()
    {
        size_t num_handlers_processed = m_service.run();
        TS_ASSERT_EQUALS(num_handlers_processed, 2);
        // INFO("m_service.run() is done");
    });

    timer.expires_from_now(boost::posix_time::millisec(1));

    timer.async_wait([&] (const boost::system::error_code& ec) { TSM_ASSERT(ec.message(), !ec); num_waits++; });

    timer.async_wait([&] (const boost::system::error_code& ec) { TSM_ASSERT(ec.message(), !ec); num_waits++; });

    while (num_waits < 2) { std::this_thread::sleep_for(std::chrono::milliseconds(1)); }
    // INFO("All work done");
    m_service.stop();
    service_thread.join();
    // INFO("After join");

    TS_ASSERT_EQUALS(num_waits, 2);

    m_service.reset();
    service_thread = std::thread([&] ()
    {
        size_t num_handlers_processed = m_service.run();
        TS_ASSERT_EQUALS(num_handlers_processed, 2);
        // INFO("m_service.run() is done");
    });

    timer.async_wait([&] (const boost::system::error_code& ec) { TSM_ASSERT(ec.message(), !ec); num_waits++; });

    timer.async_wait([&] (const boost::system::error_code& ec) { TSM_ASSERT(ec.message(), !ec); num_waits++; });

    while (num_waits < 4) { std::this_thread::sleep_for(std::chrono::milliseconds(1)); }
    m_service.stop();
    service_thread.join();

    TS_ASSERT_EQUALS(num_waits, 4);
}

void test_sync_send_sync_receive_min(void)
{
    boost::system::error_code ec;

    int num = gen_value();

    // Have to run the receiving thread before the sending thread because it blocks
    std::thread recv_thread([&] ()
    {
//      INFO("receive endpoint = %1%", m_receive_socket.endpoint());

        int rec_num;

//      INFO("Receiving bytes");
        size_t bytes_recvd =
            m_receive_socket.receive(&rec_num, sizeof(rec_num), ec);
        TS_ASSERT(!ec);
        TS_ASSERT_EQUALS(bytes_recvd, sizeof(int));

        TS_ASSERT_EQUALS(rec_num, num);

//      INFO("After receiving on socket");

    });

//  INFO("sending bytes");
    size_t bytes_sent =
        m_send_socket.send(&num, sizeof(num), ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, sizeof(int));

//  INFO("Joining receiving thread");

    recv_thread.join();

    TS_ASSERT(!m_receive_socket.busy());
    TS_ASSERT(!m_send_socket.busy());
}

void test_sync_send_sync_receive_max(void)
{
    boost::system::error_code ec;

    size_t data_size = (1 << 17) + (uint16_t)m_random();
    uint8_t* data = new uint8_t[data_size];

    for (size_t i=0; i<data_size; i++)
    {
        data[i] = gen_value();
    }

    // Have to run the receiving thread before the sending thread because it blocks
    std::thread recv_thread([&] ()
    {
        uint8_t* recvd_data = new uint8_t[data_size];
        std::memset(recvd_data, 0, data_size);

        size_t bytes_recvd =
            m_receive_socket.receive(recvd_data, data_size, ec);
        TS_ASSERT(!ec);
        TS_ASSERT_EQUALS(bytes_recvd, data_size);

        for(size_t i=0; i<data_size; i++)
        {
            TSM_ASSERT_EQUALS((boost::format("i=%1%")%i).str(), recvd_data[i], data[i]);
        }
    });

    size_t bytes_sent =
        m_send_socket.send(data, data_size, ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, data_size);

    recv_thread.join();

    TS_ASSERT(!m_receive_socket.busy());
    TS_ASSERT(!m_send_socket.busy());
}

void test_sync_send_sync_receive_change_fragment_size(void)
{
    boost::system::error_code ec;

    size_t data_size = (1 << 17);
    uint8_t* data = new uint8_t[data_size];

    for (size_t i=0; i<data_size; i++)
    {
        data[i] = gen_value();
    }

    // Have to run the receiving thread before the sending thread because it blocks
    std::thread recv_thread([&] ()
    {
        uint8_t* recvd_data = new uint8_t[data_size];

        size_t bytes_recvd =
            m_receive_socket.receive(recvd_data, data_size, ec);
        TS_ASSERT(!ec);
        TS_ASSERT_EQUALS(bytes_recvd, data_size);

        for(size_t i=0; i<data_size; i++)
        {
            TSM_ASSERT_EQUALS((boost::format("i=%1%")%i).str(), recvd_data[i], data[i]);
        }
    });

    // change the fragment size
    m_send_socket.fragment_size(1 << 10);

    size_t bytes_sent =
        m_send_socket.send(data, data_size, ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, data_size);

    recv_thread.join();

    TS_ASSERT(!m_receive_socket.busy());
    TS_ASSERT(!m_send_socket.busy());
}

void test_async_send_sync_receive_min(void)
{
    std::thread service_thread([&] () { m_service.run(); });

    boost::system::error_code ec;

    size_t data_size = 4;
    uint8_t* data = new uint8_t[data_size];

    for (size_t i=0; i<data_size; i++)
    {
        data[i] = gen_value();
    }

    // Have to run the receiving thread before the sending thread because it blocks
    std::thread recv_thread([&] ()
    {
        uint8_t* recvd_data = new uint8_t[data_size];

        size_t bytes_recvd =
            m_receive_socket.receive(recvd_data, data_size, ec);
        TS_ASSERT(!ec);
        TS_ASSERT_EQUALS(bytes_recvd, data_size);

        for(size_t i=0; i<data_size; i++)
        {
            TSM_ASSERT_EQUALS((boost::format("i=%1%")%i).str(), recvd_data[i], data[i]);
        }
    });

    m_send_socket.async_send(data, data_size, [&] (const boost::system::error_code& ec, size_t bytes_sent)
    {
        TS_ASSERT(!ec);
        TS_ASSERT_EQUALS(bytes_sent, data_size);
    });

    recv_thread.join();

    m_service.stop();
    service_thread.join();

    TS_ASSERT(m_service.stopped());

    TS_ASSERT(!m_receive_socket.busy());
    TS_ASSERT(!m_send_socket.busy());
}

void test_async_send_sync_receive_mid(void)
{
    std::thread service_thread([&] () { m_service.run(); });

    m_send_socket.fragment_size(1 << 10);
    size_t data_size = (1 << 11) + (uint8_t)m_random();
    uint8_t* data = new uint8_t[data_size];

    for (size_t i=0; i<data_size; i++)
    {
        data[i] = gen_value();
    }

    boost::system::error_code ec;

    // Have to run the receiving thread before the sending thread because it blocks
    std::thread recv_thread([&] ()
    {
        uint8_t* recvd_data = new uint8_t[data_size];

        size_t bytes_recvd =
            m_receive_socket.receive(recvd_data, data_size, ec);
        TS_ASSERT(!ec);
        TS_ASSERT_EQUALS(bytes_recvd, data_size);

        for(size_t i=0; i<data_size; i++)
        {
            TSM_ASSERT_EQUALS((boost::format("i=%1%")%i).str(), recvd_data[i], data[i]);
        }
    });

    m_send_socket.async_send(data, data_size, [&] (const boost::system::error_code& ec, size_t bytes_sent)
    {
        TS_ASSERT(!ec);
        TS_ASSERT_EQUALS(bytes_sent, data_size);
    });

    recv_thread.join();

    m_service.stop();
    service_thread.join();

    TS_ASSERT(!m_receive_socket.busy());
    TS_ASSERT(!m_send_socket.busy());
}

void test_async_send_sync_receive_max(void)
{
    std::thread service_thread([&] () { m_service.run(); });

    size_t data_size = (1 << 18) + (uint16_t)m_random();
    uint8_t* data = new uint8_t[data_size];

    for (size_t i=0; i<data_size; i++)
    {
        data[i] = gen_value();
    }

    boost::system::error_code ec;

    // Have to run the receiving thread before the sending thread because it blocks
    bool finished = false;
    std::thread recv_thread([&] ()
    {
        uint8_t* recvd_data = new uint8_t[data_size];

        size_t bytes_recvd =
            m_receive_socket.receive(recvd_data, data_size, ec);
        TS_ASSERT(!ec);
        TS_ASSERT_EQUALS(bytes_recvd, data_size);

        for(size_t i=0; i<data_size; i++)
        {
            TSM_ASSERT_EQUALS((boost::format("i=%1%")%i).str(), recvd_data[i], data[i]);
        }

        finished = true;
    });

    m_send_socket.async_send(data, data_size, [&] (const boost::system::error_code& ec, size_t bytes_sent)
    {
        TS_ASSERT(!ec);
        TS_ASSERT_EQUALS(bytes_sent, data_size);
    });

    recv_thread.join();

    m_service.stop();
    service_thread.join();

    TS_ASSERT(!m_receive_socket.busy());
    TS_ASSERT(!m_send_socket.busy());
}

void test_sync_send_async_receive_min(void)
{
    std::thread service_thread([&] () { m_service.run(); });

    int num = gen_value();

    boost::system::error_code ec;

    int recvd_data = 0;
    m_receive_socket.async_receive(&recvd_data, sizeof(recvd_data),
                                   [&] (const boost::system::error_code& ec, size_t bytes_recvd)
    {
        // INFO("received data");
        TSM_ASSERT(ec.message(), !ec);
        TS_ASSERT_EQUALS(bytes_recvd, sizeof(int));

        TS_ASSERT_EQUALS(recvd_data, num);
    });

    // INFO("Sent data");
    size_t bytes_sent =
        m_send_socket.send(&num, sizeof(num), ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, sizeof(int));

    // INFO("Done sending data");

    while (recvd_data == 0)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    m_service.stop();
    service_thread.join();

    // INFO("Thread shutdown");

    TS_ASSERT(!m_receive_socket.busy());
    TS_ASSERT(!m_send_socket.busy());
}

void test_sync_send_async_receive_max(void)
{
    std::thread service_thread([&] () { m_service.run(); });

    size_t data_size = (1 << 17) + (uint16_t)m_random();
    uint8_t* data = new uint8_t[data_size];

    for (size_t i=0; i<data_size; i++)
    {
        data[i] = gen_value();
    }

    boost::system::error_code ec;

    uint8_t* recvd_data = new uint8_t[data_size];
    bool finished = false;
    m_receive_socket.async_receive(recvd_data, data_size,
                                   [&] (const boost::system::error_code& ec, size_t bytes_recvd)
    {
        // INFO("received data");
        TSM_ASSERT(ec.message(), !ec);
        TS_ASSERT_EQUALS(bytes_recvd, data_size);

        for(size_t i=0; i<data_size; i++)
        {
            TSM_ASSERT_EQUALS((boost::format("i=%1%")%i).str(), recvd_data[i], data[i]);
        }

        delete [] recvd_data;
        finished = true;
    });

    // INFO("Sent data");
    size_t bytes_sent =
        m_send_socket.send(data, data_size, ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, data_size);

    // INFO("Done sending data");

    while (!finished)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    m_service.stop();
    service_thread.join();

    // INFO("Thread shutdown");

    TS_ASSERT(!m_receive_socket.busy());
    TS_ASSERT(!m_send_socket.busy());

    delete [] data;
}

void test_async_send_async_receive_min(void)
{
    std::thread service_thread([&] () { m_service.run(); });

    size_t data_size = 4;
    uint8_t* data = new uint8_t[data_size];

    for (size_t i=0; i<data_size; i++)
    {
        data[i] = gen_value();
    }

    boost::system::error_code ec;

    uint8_t* recvd_data = new uint8_t[data_size];
    bool finished = false;
    m_receive_socket.async_receive(recvd_data, data_size,
                                   [&] (const boost::system::error_code& ec, size_t bytes_recvd)
    {
        // INFO("received data");
        TSM_ASSERT(ec.message(), !ec);
        TS_ASSERT_EQUALS(bytes_recvd, data_size);

        for(size_t i=0; i<data_size; i++)
        {
            TSM_ASSERT_EQUALS((boost::format("i=%1%")%i).str(), recvd_data[i], data[i]);
        }

        delete [] recvd_data;
        finished = true;
    });

    // INFO("Sent data");
    m_send_socket.async_send(data, data_size,
                             [&] (const boost::system::error_code& ec, size_t bytes_sent)
    {
        TSM_ASSERT(ec.message(), !ec);
        TS_ASSERT_EQUALS(bytes_sent, data_size);
    });

    // INFO("Done sending data");

    while (!finished)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    m_service.stop();
    service_thread.join();

    // INFO("Thread shutdown");

    TS_ASSERT(!m_receive_socket.busy());
    TS_ASSERT(!m_send_socket.busy());

    delete [] data;
}

void test_async_send_async_receive_max(void)
{
    std::thread service_thread([&] () { m_service.run(); });

    size_t data_size = (1 << 17) + (uint16_t)m_random();
    uint8_t* data = new uint8_t[data_size];

    for (size_t i=0; i<data_size; i++)
    {
        data[i] = gen_value();
    }

    boost::system::error_code ec;

    uint8_t* recvd_data = new uint8_t[data_size];
    bool finished = false;
    m_receive_socket.async_receive(recvd_data, data_size,
                                   [&] (const boost::system::error_code& ec, size_t bytes_recvd)
    {
        // INFO("received data");
        TSM_ASSERT(ec.message(), !ec);
        TS_ASSERT_EQUALS(bytes_recvd, data_size);

        for(size_t i=0; i<data_size; i++)
        {
            TSM_ASSERT_EQUALS((boost::format("i=%1%")%i).str(), recvd_data[i], data[i]);
        }

        delete [] recvd_data;
        finished = true;
    });

    // INFO("Sent data");
    m_send_socket.async_send(data, data_size,
                             [&] (const boost::system::error_code& ec, size_t bytes_sent)
    {
        TSM_ASSERT(ec.message(), !ec);
        TS_ASSERT_EQUALS(bytes_sent, data_size);
    });

    // INFO("Done sending data");

    while (!finished)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    m_service.stop();
    service_thread.join();

    // INFO("Thread shutdown");

    TS_ASSERT(!m_receive_socket.busy());
    TS_ASSERT(!m_send_socket.busy());

    delete [] data;
}

void test_interleaved_packets_sync_tx_sync_rx(void)
{
    // change the fragment size
    
    boost::system::error_code ec;

    size_t data_size = 16;

    uint8_t* data1 = new uint8_t[data_size];
    for (size_t i=0; i<data_size; i++)
    {
        data1[i] = gen_value();
    }
    uint8_t* data2 = new uint8_t[data_size];
    for (size_t i=0; i<data_size; i++)
    {
        data2[i] = gen_value();
    }

    const size_t chunk_size = data_size / 2;

    m_receive_socket.fragment_size(chunk_size);

    // Have to run the receiving thread before the sending thread because it blocks
    std::thread recv_thread([&] ()
    {
        uint8_t* recvd_data = new uint8_t[data_size];

        size_t bytes_recvd = m_receive_socket.receive(recvd_data, data_size, ec);
        TS_ASSERT_EQUALS(ec, boost::asio::error::timed_out);
        
        bytes_recvd = m_receive_socket.receive(recvd_data, data_size, ec);
        TSM_ASSERT(ec.message(), !ec);
        TS_ASSERT_EQUALS(bytes_recvd, data_size);

        for(size_t i=0; i<data_size; i++)
        {
            TSM_ASSERT_EQUALS((boost::format("data2: i=%1%")%i).str(), recvd_data[i], data2[i]);
        }

    });

    udp::socket send_socket(m_service);
    ec = send_socket.open(boost::asio::ip::udp::v4(), ec);
    TSM_ASSERT(ec.message(), !ec);

    udp::endpoint endpoint = m_send_socket.endpoint();

    // Split data1 into 2 pieces and send each separately in a regular (non-fragmenting) socket
    fragmenting_socket::chunk_t header;

    // Send first chunk of data1
    header.packet_id = 0;
    header.chunk_count = 2;
    header.chunk_index = 0;
    header.chunk_size = chunk_size;

    boost::array<boost::asio::const_buffer, 2> bufs = {
        boost::asio::buffer(&header, sizeof(fragmenting_socket::chunk_t)),
        boost::asio::buffer(data1, header.chunk_size)
    };
    size_t bytes_sent = send_socket.send_to(bufs, endpoint, 0, ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, sizeof(fragmenting_socket::chunk_t) + header.chunk_size);

    // yield
    std::this_thread::sleep_for(std::chrono::milliseconds(0));

    // Send first chunk of data2
    header.packet_id = 1;
    header.chunk_count = 2;
    header.chunk_index = 0;
    header.chunk_size = chunk_size;

    bufs = {
        boost::asio::buffer(&header, sizeof(fragmenting_socket::chunk_t)),
        boost::asio::buffer(data2, header.chunk_size)
    };
    bytes_sent = send_socket.send_to(bufs, endpoint, 0, ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, sizeof(fragmenting_socket::chunk_t) + header.chunk_size);

    // yield
    std::this_thread::sleep_for(std::chrono::milliseconds(0));

    // Send second chunk of data2
    header.packet_id = 1;
    header.chunk_count = 2;
    header.chunk_index = 1;
    header.chunk_size = data_size - chunk_size;

    bufs = {
        boost::asio::buffer(&header, sizeof(fragmenting_socket::chunk_t)),
        boost::asio::buffer(data2 + chunk_size, header.chunk_size)
    };
    bytes_sent = send_socket.send_to(bufs, endpoint, 0, ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, sizeof(fragmenting_socket::chunk_t) + header.chunk_size);

    // Finish up
    recv_thread.join();

    TS_ASSERT(!m_receive_socket.busy());
}

void test_interleaved_packets_sync_tx_async_rx(void)
{
    std::thread service_thread([&] () { m_service.run(); });

    boost::system::error_code ec;

    size_t data_size = 16;

    uint8_t* data1 = new uint8_t[data_size];
    for (size_t i=0; i<data_size; i++)
    {
        data1[i] = gen_value();
    }
    uint8_t* data2 = new uint8_t[data_size];
    for (size_t i=0; i<data_size; i++)
    {
        data2[i] = gen_value();
    }

    const size_t chunk_size = data_size / 2;

    m_receive_socket.fragment_size(chunk_size);

    // Have to run the receiving thread before the sending thread because it blocks
    uint8_t* recvd_data = new uint8_t[data_size];
    bool finished = false;
    m_receive_socket.async_receive(recvd_data, data_size,
                                   [&] (const boost::system::error_code& ec, size_t bytes_recvd)
    {
        TS_ASSERT_EQUALS(ec, boost::asio::error::timed_out);

        m_receive_socket.async_receive(recvd_data, data_size,
                                       [&] (const boost::system::error_code& ec, size_t bytes_recvd)
        {
            TS_ASSERT(!ec);
            TS_ASSERT_EQUALS(bytes_recvd, data_size);

            for(size_t i=0; i<data_size; i++)
            {
                TSM_ASSERT_EQUALS((boost::format("data2: i=%1%")%i).str(), recvd_data[i], data2[i]);
            }

            finished = true;
        });
    });

    udp::socket send_socket(m_service);
    ec = send_socket.open(boost::asio::ip::udp::v4(), ec);
    TSM_ASSERT(ec.message(), !ec);

    udp::endpoint endpoint = m_send_socket.endpoint();

    // Split data1 into 2 pieces and send each separately in a regular (non-fragmenting) socket
    fragmenting_socket::chunk_t header;

    // Send first chunk of data1
    header.packet_id = 0;
    header.chunk_count = 2;
    header.chunk_index = 0;
    header.chunk_size = chunk_size;

    boost::array<boost::asio::const_buffer, 2> bufs = {
        boost::asio::buffer(&header, sizeof(fragmenting_socket::chunk_t)),
        boost::asio::buffer(data1, header.chunk_size)
    };
    size_t bytes_sent = send_socket.send_to(bufs, endpoint, 0, ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, sizeof(fragmenting_socket::chunk_t) + header.chunk_size);

    // yield
    std::this_thread::sleep_for(std::chrono::milliseconds(0));

    // Send first chunk of data2
    header.packet_id = 1;
    header.chunk_count = 2;
    header.chunk_index = 0;
    header.chunk_size = chunk_size;

    bufs = {
        boost::asio::buffer(&header, sizeof(fragmenting_socket::chunk_t)),
        boost::asio::buffer(data2, header.chunk_size)
    };
    bytes_sent = send_socket.send_to(bufs, endpoint, 0, ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, sizeof(fragmenting_socket::chunk_t) + header.chunk_size);

    // yield
    std::this_thread::sleep_for(std::chrono::milliseconds(0));

    // Send second chunk of data2
    header.packet_id = 1;
    header.chunk_count = 2;
    header.chunk_index = 1;
    header.chunk_size = data_size - chunk_size;

    bufs = {
        boost::asio::buffer(&header, sizeof(fragmenting_socket::chunk_t)),
        boost::asio::buffer(data2 + chunk_size, header.chunk_size)
    };
    bytes_sent = send_socket.send_to(bufs, endpoint, 0, ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, sizeof(fragmenting_socket::chunk_t) + header.chunk_size);

    while (!finished)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    m_service.stop();
    service_thread.join();

    // INFO("Thread shutdown");

    TS_ASSERT(!m_receive_socket.busy());
}

void test_interleaved_packets_failure(void)
{
    std::thread service_thread([&] () { m_service.run(); });

    boost::system::error_code ec;

    size_t data_size = 16;

    uint8_t* data1 = new uint8_t[data_size];
    for (size_t i=0; i<data_size; i++)
    {
        data1[i] = gen_value();
    }
    uint8_t* data2 = new uint8_t[data_size];
    for (size_t i=0; i<data_size; i++)
    {
        data2[i] = gen_value();
    }

    const size_t chunk_size = data_size / 2;

    m_receive_socket.fragment_size(chunk_size);

    // Have to run the receiving thread before the sending thread because it blocks
    uint8_t* recvd_data = new uint8_t[data_size];
    bool finished = false;
    m_receive_socket.async_receive(recvd_data, data_size,
                                   [&] (const boost::system::error_code& ec, size_t bytes_recvd)
    {
        TS_ASSERT_EQUALS(ec, boost::asio::error::timed_out);

        m_receive_socket.async_receive(recvd_data, data_size,
                                       [&] (const boost::system::error_code& ec, size_t bytes_recvd)
        {
            TS_ASSERT_EQUALS(ec, boost::asio::error::operation_not_supported);

            finished = true;
        });
    });

    udp::socket send_socket(m_service);
    ec = send_socket.open(boost::asio::ip::udp::v4(), ec);
    TSM_ASSERT(ec.message(), !ec);

    udp::endpoint endpoint = m_send_socket.endpoint();

    // Split data1 into 2 pieces and send each separately in a regular (non-fragmenting) socket
    fragmenting_socket::chunk_t header;

    // Send first chunk of data1
    header.packet_id = 0;
    header.chunk_count = 2;
    header.chunk_index = 0;
    header.chunk_size = chunk_size;

    boost::array<boost::asio::const_buffer, 2> bufs = {
        boost::asio::buffer(&header, sizeof(fragmenting_socket::chunk_t)),
        boost::asio::buffer(data1, header.chunk_size)
    };
    size_t bytes_sent = send_socket.send_to(bufs, endpoint, 0, ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, sizeof(fragmenting_socket::chunk_t) + header.chunk_size);

    // yield
    std::this_thread::sleep_for(std::chrono::milliseconds(0));

    // Send first chunk of data2
    header.packet_id = 1;
    header.chunk_count = 2;
    header.chunk_index = 0;
    header.chunk_size = chunk_size;

    bufs = {
        boost::asio::buffer(&header, sizeof(fragmenting_socket::chunk_t)),
        boost::asio::buffer(data2, header.chunk_size)
    };
    bytes_sent = send_socket.send_to(bufs, endpoint, 0, ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, sizeof(fragmenting_socket::chunk_t) + header.chunk_size);

    // yield
    std::this_thread::sleep_for(std::chrono::milliseconds(0));

    // Send second chunk of data2
    header.packet_id = 0;
    header.chunk_count = 2;
    header.chunk_index = 1;
    header.chunk_size = data_size - chunk_size;

    bufs = {
        boost::asio::buffer(&header, sizeof(fragmenting_socket::chunk_t)),
        boost::asio::buffer(data1 + chunk_size, header.chunk_size)
    };
    bytes_sent = send_socket.send_to(bufs, endpoint, 0, ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, sizeof(fragmenting_socket::chunk_t) + header.chunk_size);

    // yield
    std::this_thread::sleep_for(std::chrono::milliseconds(0));

    // Send second chunk of data2
    header.packet_id = 1;
    header.chunk_count = 2;
    header.chunk_index = 1;
    header.chunk_size = data_size - chunk_size;

    bufs = {
        boost::asio::buffer(&header, sizeof(fragmenting_socket::chunk_t)),
        boost::asio::buffer(data2 + chunk_size, header.chunk_size)
    };
    bytes_sent = send_socket.send_to(bufs, endpoint, 0, ec);
    TS_ASSERT(!ec);
    TS_ASSERT_EQUALS(bytes_sent, sizeof(fragmenting_socket::chunk_t) + header.chunk_size);

    while (!finished)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    m_service.stop();
    service_thread.join();

    // INFO("Thread shutdown");

    TS_ASSERT(!m_receive_socket.busy());
}

};
