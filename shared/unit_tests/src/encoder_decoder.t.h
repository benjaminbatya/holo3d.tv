#include <cxxtest/TestSuite.h>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <thread>
#include <random>

#include "h3t_file.hpp"
#include "util_funcs.hpp"

#include "loader.hpp"

#include "encoder.hpp"
#include "decoder.hpp"

using namespace holo3d;

class loader : public CxxTest::TestSuite
{

public:

void test_loader()
{
    file_loader loader("data/single_frame.h3t");
    bool ret = loader.run();
    TS_ASSERT(ret);

    TS_ASSERT(loader.valid());

    // Make sure that the file is as raw as possible
    TS_ASSERT_EQUALS(loader.file_header().color_format, STREAM_FORMAT::COLOR_RGB888);
    TS_ASSERT_EQUALS(loader.file_header().depth_format, STREAM_FORMAT::DEPTH_RAW);

    TS_ASSERT_EQUALS(loader.file_header().width, 640);
    TS_ASSERT_EQUALS(loader.file_header().height, 480);
}

void test_next_frame()
{
    file_loader loader("data/single_frame.h3t");
    bool ret = loader.run();
    TS_ASSERT(ret);

    camera_frame frame;
    ret = loader.next_frame(frame);
    TS_ASSERT(ret);

    TS_ASSERT(frame.valid());

    TS_ASSERT(frame.color_buffer());
    TS_ASSERT(frame.depth_buffer());

    TS_ASSERT(frame.color_buffer_size() > 0);
    TS_ASSERT(frame.depth_buffer_size() > 0);
}



};

class encoder_decoder : public CxxTest::TestSuite
{

public:

    file_loader m_loader { "data/single_frame.h3t" };
    camera_frame m_frame;

    const size_t WIDTH = 640;
    const size_t HEIGHT = 480;

    encoder::factory m_encoder_factory { WIDTH, HEIGHT, encoder::QUALITY::GOOD }; // NOTE: These constants would be bad if not for the fact that the same file is always being used
    decoder::factory m_decoder_factory { WIDTH, HEIGHT, WIDTH };

    const size_t BUFFER_SIZE = WIDTH * HEIGHT * sizeof(RGB888Pixel);
    uint8_t* m_buffer = new uint8_t[BUFFER_SIZE];

void setUp()
{
    bool ret = m_loader.run();
    ASSERT(ret);

    ASSERT(m_loader.valid());

    ret = m_loader.next_frame(m_frame);
    ASSERT(ret);
}

void tearDown()
{
    // nothing for now
}

void test_encode_RGB()
{
    // encode and then decode the color frame in RGB
    // Check for norm squared error
    encoder::color_encoder_t encoder = m_encoder_factory.color(STREAM_FORMAT::COLOR_RGB888, m_loader.file_header().color_format);

    size_t size = 
        encoder->encode(m_buffer, m_frame.color_buffer(), BUFFER_SIZE, m_frame.color_buffer_size());

    TS_ASSERT(size <= BUFFER_SIZE);

    size_t accum = 0;
    for (size_t i = 0; i<size; i++)
    {
        size_t diff = m_buffer[i] - m_frame.color_buffer()[i];
        accum += diff*diff;
    }

    TS_ASSERT_EQUALS(accum, 0);

};

void test_encode_decode_color_jpeg()
{
    // encode and then decode the color frame in RGB
    // Check for norm squared error
    encoder::color_encoder_t encoder = m_encoder_factory.color(STREAM_FORMAT::COLOR_JPEG, m_loader.file_header().color_format);
    decoder::color_decoder_t decoder = m_decoder_factory.color(STREAM_FORMAT::COLOR_JPEG);

    size_t size = 
        encoder->encode(m_buffer, m_frame.color_buffer(), BUFFER_SIZE, m_frame.color_buffer_size());

    TS_ASSERT(size <= BUFFER_SIZE);

    std::shared_ptr<uint8_t> output_buf(new uint8_t[BUFFER_SIZE]);

    decoder->decode((RGB888Pixel*)output_buf.get(), m_buffer, BUFFER_SIZE, size);

    // Check the average absolute error
    double accum = 0;
    for (size_t i = 0; i<BUFFER_SIZE; i++)
    {
        double diff = ((double)output_buf.get()[i]) - m_frame.color_buffer()[i];
        accum += std::abs(diff);
    }

    double ratio = accum / BUFFER_SIZE;

    // NOTE: This is NOT a good way to check error!!
    TS_ASSERT_LESS_THAN(ratio, 5.0); 
};


void test_encode_decode_depth_raw()
{
    // encode and then decode the depth frame using RAW
    encoder::depth_encoder_t encoder = m_encoder_factory.depth(STREAM_FORMAT::DEPTH_RAW, m_loader.file_header().depth_format);
    decoder::depth_decoder_t decoder = m_decoder_factory.depth(STREAM_FORMAT::DEPTH_RAW);

    size_t size = 
        encoder->encode(m_buffer, m_frame.depth_buffer(), BUFFER_SIZE, m_frame.depth_buffer_size());

    TS_ASSERT(size <= BUFFER_SIZE);

    const size_t num_pixels = WIDTH*HEIGHT;
    std::shared_ptr<DepthPixel> output_buf(new DepthPixel[num_pixels]);

    decoder->decode_depth(output_buf.get(), m_buffer, num_pixels*sizeof(DepthPixel), size);

    const DepthPixel* depth_input = (const DepthPixel*)m_frame.depth_buffer();

    // Check the mean absolute difference
    double accum = 0;
    for (size_t i = 0; i<num_pixels; i++)
    {
        double diff = ((double)output_buf.get()[i]) - depth_input[i];
        accum += std::abs(diff);
    }

    double ratio = accum / num_pixels;

    TS_ASSERT_LESS_THAN(ratio, 0.0001);
};

void test_encode_decode_depth_enc()
{
    // encode and then decode the depth frame using ENC
    encoder::depth_encoder_t encoder = m_encoder_factory.depth(STREAM_FORMAT::DEPTH_ENC, m_loader.file_header().depth_format);
    decoder::depth_decoder_t decoder = m_decoder_factory.depth(STREAM_FORMAT::DEPTH_ENC);

    size_t size = 
        encoder->encode(m_buffer, m_frame.depth_buffer(), BUFFER_SIZE, m_frame.depth_buffer_size());

    TS_ASSERT(size <= BUFFER_SIZE);

    const size_t num_pixels = WIDTH*HEIGHT;
    std::shared_ptr<DepthPixel> output_buf(new DepthPixel[num_pixels]);

    decoder->decode_depth(output_buf.get(), m_buffer, num_pixels*sizeof(DepthPixel), size);

    const DepthPixel* depth_input = (const DepthPixel*)m_frame.depth_buffer();

    // Check the mean absolute difference
    double accum = 0;
    for (size_t i = 0; i<num_pixels; i++)
    {
        double diff = ((double)output_buf.get()[i]) - depth_input[i];
        accum += std::abs(diff);
    }

    double ratio = accum / num_pixels;

    TS_ASSERT_LESS_THAN(ratio, 0.05);
};

void test_encode_decode_depth_jpeg()
{
    // encode and then decode the depth frame using JPEG
    encoder::depth_encoder_t encoder = m_encoder_factory.depth(STREAM_FORMAT::DEPTH_JPEG, m_loader.file_header().depth_format);
    decoder::depth_decoder_t decoder = m_decoder_factory.depth(STREAM_FORMAT::DEPTH_JPEG);

    size_t size = 
        encoder->encode(m_buffer, m_frame.depth_buffer(), BUFFER_SIZE, m_frame.depth_buffer_size());

    TS_ASSERT(size <= BUFFER_SIZE);

    const size_t num_pixels = WIDTH*HEIGHT;
    std::shared_ptr<DepthPixel> output_buf(new DepthPixel[num_pixels]);

    decoder->decode_depth(output_buf.get(), m_buffer, num_pixels*sizeof(DepthPixel), size);

    const DepthPixel* depth_input = (const DepthPixel*)m_frame.depth_buffer();

    // Check the mean absolute difference
    double accum = 0;
    for (size_t i = 0; i<num_pixels; i++)
    {
        double diff = ((double)output_buf.get()[i]) - depth_input[i];
        accum += std::abs(diff);
    }

    double ratio = accum / num_pixels;

    // NOTE: These are the numbers that http://web4.cs.ucl.ac.uk/staff/j.kautz/publications/depth-streaming.pdf got too
    TS_ASSERT_LESS_THAN(ratio, 310.0);
};


void test_encode_decode_depth_png()
{
    // encode and then decode the depth frame using JPEG
    encoder::depth_encoder_t encoder = m_encoder_factory.depth(STREAM_FORMAT::DEPTH_PNG, m_loader.file_header().depth_format);
    decoder::depth_decoder_t decoder = m_decoder_factory.depth(STREAM_FORMAT::DEPTH_PNG);

    size_t size = 
        encoder->encode(m_buffer, m_frame.depth_buffer(), BUFFER_SIZE, m_frame.depth_buffer_size());

    TS_ASSERT(size <= BUFFER_SIZE);

    const size_t num_pixels = WIDTH*HEIGHT;
    std::shared_ptr<DepthPixel> output_buf(new DepthPixel[num_pixels]);

    decoder->decode_depth(output_buf.get(), m_buffer, num_pixels*sizeof(DepthPixel), size);

    const DepthPixel* depth_input = (const DepthPixel*)m_frame.depth_buffer();

    // Check the mean absolute difference
    double accum = 0;
    for (size_t i = 0; i<num_pixels; i++)
    {
        double diff = ((double)output_buf.get()[i]) - depth_input[i];
        accum += std::abs(diff);
    }

    double ratio = accum / num_pixels;

    TS_ASSERT_LESS_THAN(ratio, 0.00001);
};

// The conversion matrices are copied from http://www.brucelindbloom.com/index.html?Eqn_RGB_XYZ_Matrix.html
// Using the CIE RGB profile
void convert_RGB_to_xyz(double* output, const RGB888Pixel& input)
{
    double rgb[3];
    rgb[0] = (double)input.r;
    rgb[1] = (double)input.g;
    rgb[2] = (double)input.b;

    rgb[0] /= 256;
    rgb[1] /= 256;
    rgb[2] /= 256;

    static double convert[9] = 
    {
        0.4887180,  0.3106803,  0.2006017,
        0.1762044,  0.8129847,  0.0108109,
        0.0000000,  0.0102048,  0.9897952,
    };

    for (int y=0; y<3; y++)
    {
        output[y] = 0.0;
        for (int x=0; x<3; x++)
        {
            double left = convert[y*3 + x];
            output[y] += left * rgb[x];
        }
    }
}

void convert_xyz_to_RGB(RGB888Pixel& output, const double* input)
{
    double rgb[3];
    
    static double convert[9] = 
    {
        2.3706743, -0.9000405, -0.4706338,
        -0.5138850,  1.4253036,  0.0885814,
        0.0052982, -0.0146949,  1.0093968,
    };

    for (int y=0; y<3; y++)
    {
        rgb[y] = 0.0;
        for (int x=0; x<3; x++)
        {
            double left = convert[y*3 + x];
            rgb[y] += left * input[x];
        }
    }

    rgb[0] *= 256;
    rgb[1] *= 256;
    rgb[2] *= 256;

    output.r = (uint8_t)rgb[0];
    output.g = (uint8_t)rgb[1];
    output.b = (uint8_t)rgb[2];
}


void test_conversion_funcs()
{
    const RGB888Pixel& pixel = ((const RGB888Pixel*)m_frame.color_buffer())[0];

    double xyz[3];

    convert_RGB_to_xyz(xyz, pixel);

    RGB888Pixel result;
    convert_xyz_to_RGB(result, xyz);

    TS_ASSERT_DELTA(result.r, pixel.r, 1);
    TS_ASSERT_DELTA(result.g, pixel.g, 1);
    TS_ASSERT_DELTA(result.b, pixel.b, 1);

}

void test_encode_decode_color_yv12()
{
    // encode and then decode the depth frame using JPEG
    encoder::color_encoder_t encoder = m_encoder_factory.color(STREAM_FORMAT::COLOR_YV12, m_loader.file_header().color_format);
    decoder::color_decoder_t decoder = m_decoder_factory.color(STREAM_FORMAT::COLOR_YV12);

    size_t size = encoder->encode(m_buffer, m_frame.color_buffer(), BUFFER_SIZE, m_frame.color_buffer_size());

    TS_ASSERT(size <= BUFFER_SIZE);

    const size_t num_pixels = WIDTH*HEIGHT;
    std::shared_ptr<RGB888Pixel> output_buf(new RGB888Pixel[num_pixels]);

    decoder->decode(output_buf.get(), m_buffer, num_pixels*sizeof(RGB888Pixel), size);

    const RGB888Pixel* input = (const RGB888Pixel*)m_frame.color_buffer();

    // Check the mean absolute difference
    double accum = 0;
    for (size_t i = 0; i<num_pixels; i++)
    {
        const RGB888Pixel& input_pixel = input[i];
        const RGB888Pixel& output_pixel = output_buf.get()[i];

        double input_xyz[3];
        double output_xyz[3];

        convert_RGB_to_xyz(input_xyz, input_pixel);
        convert_RGB_to_xyz(output_xyz, output_pixel);

        for (int i=0; i<3; i++)
        {
            double diff = std::abs(output_xyz[i] - input_xyz[i]);
            accum += diff;
        }
//
//      accum += std::abs(((double)output_pixel.r) - input_pixel.r);
//      accum += std::abs(((double)output_pixel.g) - input_pixel.g);
//      accum += std::abs(((double)output_pixel.b) - input_pixel.b);
    }

    double ratio = accum / (num_pixels * 3);

    TS_ASSERT_LESS_THAN(ratio, 0.012);
};

// VP8 is not really working..
void test_encode_decode_color_vp8()
{
    // encode and then decode the depth frame using JPEG
    encoder::color_encoder_t encoder = m_encoder_factory.color(STREAM_FORMAT::COLOR_VP8, m_loader.file_header().color_format);
    decoder::color_decoder_t decoder = m_decoder_factory.color(STREAM_FORMAT::COLOR_VP8);

    size_t size = encoder->encode(m_buffer, m_frame.color_buffer(), BUFFER_SIZE, m_frame.color_buffer_size());

    TS_ASSERT(size <= BUFFER_SIZE);

    const size_t num_pixels = WIDTH*HEIGHT;
    std::shared_ptr<RGB888Pixel> output_buf(new RGB888Pixel[num_pixels]);

    decoder->decode(output_buf.get(), m_buffer, num_pixels*sizeof(RGB888Pixel), size);

    const RGB888Pixel* input = (const RGB888Pixel*)m_frame.color_buffer();

    // Check the mean absolute difference
    double accum = 0;
    for (size_t i = 0; i<num_pixels; i++)
    {
        const RGB888Pixel& input_pixel = input[i];
        const RGB888Pixel& output_pixel = output_buf.get()[i];

//      double input_xyz[3];
//      double output_xyz[3];
//
//      convert_RGB_to_xyz(input_xyz, input_pixel);
//      convert_RGB_to_xyz(output_xyz, output_pixel);
//
//      for (int i=0; i<3; i++)
//      {
//          double diff = std::abs(output_xyz[i] - input_xyz[i]);
//          accum += diff;
//      }

        accum += std::abs(((double)output_pixel.r) - input_pixel.r);
        accum += std::abs(((double)output_pixel.g) - input_pixel.g);
        accum += std::abs(((double)output_pixel.b) - input_pixel.b);
    }

    double ratio = accum / (num_pixels * 3);

    TS_ASSERT_LESS_THAN(ratio, 0.0);
};



};



