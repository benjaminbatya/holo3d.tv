#include "pch_files.hpp"

#include <cxxtest/TestSuite.h>

#include <thread>
#include <random>

#include "control_protocol.hpp"

#include "fragmenting_socket.hpp"
#include "util_funcs.hpp"

#include "h3t_file.hpp"

using boost::asio::ip::udp;
using namespace holo3d;

class test_protocol : public CxxTest::TestSuite
{

boost::asio::io_service m_service;
boost::asio::io_service::work m_work { m_service };

fragmenting_socket m_server_socket { m_service, 0, 0 };
fragmenting_socket m_camera_socket { m_service, 0, 0 };


// typedef std::minstd_rand0 random_number_generator_t;
// random_number_generator_t m_random { (random_number_generator_t::result_type)get_time() };
//inline uint8_t gen_value()
//{
//    return (uint8_t)m_random();
//}

public:

void setUp()
{
    m_service.reset();

    boost::system::error_code ec;
    udp::resolver resolver(m_service);
    udp::resolver::query query(udp::v4(), "localhost", holo3d::protocol::DEFAULT_PORT);
    m_server_socket.endpoint() = *resolver.resolve(query, ec);
    TS_ASSERT(!ec);

//  INFO("opening sending socket");
    ec = m_server_socket.open();
    TSM_ASSERT(ec.message(), !ec);

//  INFO("binding socket");
    m_camera_socket.endpoint().port(atoi(holo3d::protocol::DEFAULT_PORT.c_str()));

    ec = m_camera_socket.bind();
    TSM_ASSERT(ec.message(), !ec);

    // Setup the sidecar buffers
    m_camera_socket.set_sidecar_buf_size(1024);
    m_server_socket.set_sidecar_buf_size(1024);
}

void tearDown()
{
    boost::system::error_code ec;
    ec = m_camera_socket.close();
    TSM_ASSERT(ec.message(), !ec);
    ec = m_server_socket.close();
    TSM_ASSERT(ec.message(), !ec);

    m_service.stop();
}

void test_alive_query()
{
    std::thread camera([&]
    {
        TS_ASSERT_THROWS_NOTHING(
            protocol::Receive::basic(m_camera_socket, protocol::ID::QUERY)
        );
        
    });

    std::thread server([&]
    {
        TS_ASSERT_THROWS_NOTHING(
            protocol::Send::basic(m_server_socket, protocol::ID::QUERY)
        );
    });

    server.join();
    camera.join();

    TS_ASSERT(!m_camera_socket.busy());
    TS_ASSERT(!m_server_socket.busy());
};

void test_alive_query_ack()
{
    std::thread camera([&]
    {
        protocol::Receive::basic(m_camera_socket, protocol::ID::QUERY);
        
        TS_ASSERT_THROWS_NOTHING(
            protocol::Send::basic(m_camera_socket, protocol::ID::QUERY_ACK)
        );
    });

    std::thread server([&]
    {
        protocol::Send::basic(m_server_socket, protocol::ID::QUERY);

        TS_ASSERT_THROWS_NOTHING(
            protocol::Receive::basic(m_server_socket, protocol::ID::QUERY_ACK)
        );
    });

    server.join();
    camera.join();

    TS_ASSERT(!m_camera_socket.busy());
    TS_ASSERT(!m_server_socket.busy());
};

void test_start_basic()
{
    std::thread camera([&]
    {
        TS_ASSERT_THROWS_NOTHING(
            protocol::Receive::basic(m_camera_socket, protocol::ID::START)
        );

    });

    std::thread server([&]
    {
        TS_ASSERT_THROWS_NOTHING(
            protocol::Send::basic(m_server_socket, protocol::ID::START)
        );
    });

    server.join();
    camera.join();

    TS_ASSERT(!m_camera_socket.busy());
    TS_ASSERT(!m_server_socket.busy());
};

void test_start_data()
{
    std::thread camera([&]
    {
        protocol::StartData data;

        TS_ASSERT_THROWS_NOTHING(
            protocol::Receive::start(m_camera_socket, data);
        );

        TS_ASSERT_EQUALS(data.resolution, RESOLUTION::SMALL);
        TS_ASSERT_EQUALS(data.color_format, STREAM_FORMAT::COLOR_VP8);
        TS_ASSERT_EQUALS(data.depth_format, STREAM_FORMAT::DEPTH_RAW);
    });

    std::thread server([&]
    {
        protocol::StartData data;
        data.resolution = RESOLUTION::SMALL;
        data.color_format = STREAM_FORMAT::COLOR_VP8;
        data.depth_format = STREAM_FORMAT::DEPTH_RAW;
        
        TS_ASSERT_THROWS_NOTHING(
            protocol::Send::start(m_server_socket, data);
        );
    });

    server.join();
    camera.join();

    TS_ASSERT(!m_camera_socket.busy());
    TS_ASSERT(!m_server_socket.busy());
};

void test_async_query()
{
    std::thread service_thread([&] { TS_ASSERT_THROWS_NOTHING(m_service.run()); });

    bool finished = false;
    protocol::Receive::async_basic(m_camera_socket, protocol::ID::QUERY, [&finished] ()
    {
        finished = true;
    });

    protocol::Send::async_basic(m_server_socket, protocol::ID::QUERY);

    while (!finished)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    m_service.stop();
    service_thread.join();

    TS_ASSERT(!m_camera_socket.busy());
    TS_ASSERT(!m_server_socket.busy());
}

void test_async_query_ack()
{
    std::thread service_thread([&] { TS_ASSERT_THROWS_NOTHING(m_service.run()); });

    bool finished = false;
    protocol::Receive::async_basic(m_camera_socket, protocol::ID::QUERY, [this] ()
    {
        protocol::Send::basic(m_camera_socket, protocol::ID::QUERY_ACK);
    });

    protocol::Send::async_basic(m_server_socket, protocol::ID::QUERY, [this, &finished] ()
    {
        protocol::Receive::async_basic(m_server_socket, protocol::ID::QUERY_ACK, [&finished] ()
        {
            finished = true;
        });
    });

    while (!finished) 
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    m_service.stop();
    service_thread.join();

    TS_ASSERT(!m_camera_socket.busy());
    TS_ASSERT(!m_server_socket.busy());
}

void test_async_start()
{
    std::thread service_thread([&] { TS_ASSERT_THROWS_NOTHING(m_service.run()); });

    bool finished = false;
    protocol::Receive::async_start(m_camera_socket, [&] (const protocol::StartData& msg)
    {
        TS_ASSERT_EQUALS(msg.resolution, RESOLUTION::MEDUIM);
        TS_ASSERT_EQUALS(msg.color_format, STREAM_FORMAT::COLOR_VP8);
        TS_ASSERT_EQUALS(msg.depth_format, STREAM_FORMAT::DEPTH_JPEG);
        TS_ASSERT_EQUALS(msg.quality, QUALITY::REALTIME);
        TS_ASSERT_EQUALS(msg.fps, 30);
        finished = true;
    });

    protocol::StartData msg;
    msg.resolution = RESOLUTION::MEDUIM;
    msg.color_format = STREAM_FORMAT::COLOR_VP8;
    msg.depth_format = STREAM_FORMAT::DEPTH_JPEG;
    msg.quality = QUALITY::REALTIME;
    msg.fps = 30;
    protocol::Send::async_start(m_server_socket, msg);

    while (!finished) 
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    m_service.stop();
    service_thread.join();

    TS_ASSERT(!m_camera_socket.busy());
    TS_ASSERT(!m_server_socket.busy());
}

void test_stopped_async_end_ack()
{
    std::thread service_thread([&] { TS_ASSERT_THROWS_NOTHING(m_service.run()); });

    bool got_ack = false;
    boost::asio::deadline_timer timer(m_service);
    protocol::Receive::async_basic(m_camera_socket, protocol::ID::END, [&] ()
    {
        timer.expires_from_now(boost::posix_time::milliseconds(30));
        timer.async_wait([&](const boost::system::error_code& ec)
        {
            boost::posix_time::time_duration time = timer.expires_from_now();
//          INFO("expires at = %1%", time.total_microseconds());
            bool expired = (time <= boost::posix_time::milliseconds(0));
            TS_ASSERT_EQUALS(expired, true);
//          INFO("after async_wait: Expired = %1%", expired);
            TSM_ASSERT(ec.message(), !ec);
            protocol::Send::basic(m_camera_socket, protocol::ID::END_ACK);
        });
    });

    protocol::Send::async_basic(m_server_socket, protocol::ID::END, [&] ()
    {
        protocol::Receive::async_basic(m_server_socket, protocol::ID::END_ACK, [&] ()
        {
            got_ack = true;
        });
    });

    std::this_thread::sleep_for(std::chrono::milliseconds(20));

    bool expired = (timer.expires_from_now() <= boost::posix_time::milliseconds(0));
    TS_ASSERT_EQUALS(expired, false);
//  INFO("after sleeping: expired = %1%", expired);

    TS_ASSERT_EQUALS(got_ack, false);

    std::this_thread::sleep_for(std::chrono::milliseconds(15));

    expired = (timer.expires_from_now() <= boost::posix_time::milliseconds(0));
    TS_ASSERT_EQUALS(expired, true);

    TS_ASSERT_EQUALS(got_ack, true);

    m_service.stop();
    service_thread.join();

    TS_ASSERT(!m_camera_socket.busy());
    TS_ASSERT(!m_server_socket.busy());
}



}; // class test_protocol
