# Sconstruct for shared. Different configs are Debug-x86, Release-x86, Debug-Arm and Release-Arm
build_all = False

try:
    Import('env')
    if(env['CFG']):
        cfg = env['CFG']
        build_all = True
except: 
    None # Do nothing

if not build_all:
    cfg = ARGUMENTS.get('CFG', 'Debug-x86')

cfg = cfg.split('-')
assert len(cfg) > 0
config = cfg[0]

platform = 'x86'
if(len(cfg) > 1):
    platform = cfg[1]

### Import in the needed tools
TOOLS = ['default', 'gch']

env = Environment(tools = TOOLS)

BUILD_PATH = config + '-' + platform
env['BUILD_PATH'] = BUILD_PATH

env.Append(CPPFLAGS = ['-Wall', '-std=c++11', '-DPLATFORM_'+platform, '-fPIC']) # ,'-ftime-report', '-H'

enable_opencl = False
#if platform == 'Arm':
#    enable_opencl = True
#elif platform == 'x86':
#    # Figure out if OPEN_CL is enabled by checking if clinfo reports a usable platform
#    import subprocess
#    #print "checking for OpenCL"
#    if not subprocess.call('which clinfo > /dev/null', shell=True):
#    #   print "clinfo exists"
#        results = subprocess.Popen(['clinfo'], stdout=subprocess.PIPE)
#        for line in results.stdout:
#            if 'Platform Version:' in line and 'OpenCL' in line:
#                print "OPEN_CL_ENABLED!"
#                enable_opencl = True
#                break

if enable_opencl:
    env.Append(CPPFLAGS = ['-DOPEN_CL_ENABLED']) # the XU3 always has OPEN_CL enabled
    env.Append(LIBS = ['OpenCL'])

env.Append(CPPPATH = ['..', "."])
env.Append(LIBS = ['boost_system', 'boost_thread', 'boost_filesystem', 'boost_program_options', 'pthread', 'OpenNI2', 'vpx', 'yuv', 'jpeg', 'turbojpeg', 'png']) 

if(platform == 'x86'):
    env.Replace(CXX = 'g++')
    env.Replace(LINK = 'g++')
    env.Append(CPPPATH = ['/usr/local/include', '/opt/libjpeg-turbo/include'])
    env.Append(LIBPATH = ['/opt/libjpeg-turbo/lib64'])

elif(platform == 'Arm'):
    env.Replace(CXX = 'arm-linux-gnueabihf-g++')
    env.Replace(LINK = 'arm-linux-gnueabihf-g++')
    env.Append(CPPPATH = Split( '''
            /usr/arm-linux-gnueabihf/include 
            /usr/arm-linux-gnueabihf/include/c++/4.8.2
            /usr/arm-linux-gnueabihf/include/c++/4.8.2/arm-linux-gnueabihf
                                ''') )
    env.Append(LIBPATH = Split('''
            /usr/arm-linux-gnueabihf/lib/boost
            /usr/arm-linux-gnueabihf/lib/OpenNI2
                               ''') )
    env.Append(LIBS = [])
else:
    raise Exception('Unknown platform: ' + platform)

if(config == 'Debug'):
    env.Append(CPPFLAGS = '-g')
    env.Append(LINKFLAGS = '-g')
elif(config == 'Release'):
    env.Append(CPPFLAGS = Split('-O3 -DNDEBUG'))
elif(config == 'Profile'):
    env.Append(CPPFLAGS = Split('-g -O3 -DNDEBUG'))
    env.Append(LINKFLAGS = '-g -pg')
else:
    raise Exception('Unknown configuration: ' + config)

main = env.SConscript('src/SConscript', variant_dir=BUILD_PATH, duplicate=0, exports='env')

print "Building {0} in {1} configuration".format(main[0], config)

env.Default(main)

Return('main')
