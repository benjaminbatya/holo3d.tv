#ifndef __FPS_COUNTER_H_
#define __FPS_COUNTER_H_

#include <stdint.h>
#include <string>

namespace holo3d
{

class fps_counter
{
public:
    fps_counter(const std::string& name = "");

    void name(std::string name) { m_name = name; }

    void reset();

    void update();

protected:
    std::string m_name;
    uint64_t m_last_time;
    int m_num_frames = 0;
};

}; // namespace holo3d

#endif // __FPS_COUNTER_H_
