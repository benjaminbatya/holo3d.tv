# -*- python -*-

"""
Created on Wed Jan 14 13:00:35 2015

This is the beginning of the python prototyping code

@author: benjamin
"""
import holo3d
import datetime
import numpy as np
import os

class time_struct:
    stamp = 0
    second = 0
    millisec = 0
    date = 0
    
    def __init__(self, stamp):
        self.stamp = stamp
        self.second = self.stamp / MICROS_PER_SEC
        self.millisec = self.stamp % MICROS_PER_SEC / 1000
        self.date = datetime.datetime.fromtimestamp(self.second)
    
class DeltaError(Exception):
    def __init__(self, value):
        self.value = value
    
    def __str__(self):
        return repr(self.value)
    
    
HOLO_ROOT = str(os.environ['HOLO_ROOT'])
FILE_NAME = HOLO_ROOT + "/videos/cube_test/"
MICROS_PER_SEC = 1000000
num_cameras = 2
num_frames = 100

loader = [None] * num_cameras
for i in range(num_cameras):
    print "Loading camera {}".format(FILE_NAME + str(i) + ".h3t")
    loader[i] = holo3d.file_loader(FILE_NAME + str(i) + ".h3t") # Load the file
    loader[i].run() # This is very annoying to have to remember to do... I have to change the API

start_time = [None] * num_cameras
time = [None] * num_cameras
deltas = [0] * num_cameras

frame = holo3d.camera_frame() # Build a frame to use

for i in range(num_frames): # Load ten frames and print out their time codes
    try:
        for j in range(num_cameras):
            loader[j].next_frame(frame) # Load the first frame
            
            # Make a massive assumption that color and depth time codes 
            # are close enough so just average the two of them
            # They seem to be about 4 millisec apart
            delta = abs(frame.color_time_stamp-frame.depth_time_stamp)
            if(delta > 10000):
                print "Frame#{}: Camera #{}: time delta ({}) is too large, skipping frame number".format(i, j, delta)
                raise DeltaError(j)
            print "camera[{}] time delta = {}".format(j, delta)
            deltas[j] += delta
                        
            if(start_time[j] == None):
                start_time[j] = (frame.color_time_stamp + frame.depth_time_stamp) / 2
                        
            time[j] = time_struct((frame.color_time_stamp + frame.depth_time_stamp) / 2 - start_time[j])
              
        print "Frame #{}: camera#0: time(sec)={}.{}, camera#1 time(sec)={}.{}".format(
                frame.frame_number, 
                time[0].second % 100, time[0].millisec, 
                time[1].second % 100, time[1].millisec)
    except DeltaError as e:
        print e
        continue

for j in range(num_cameras):
    print "Camera #{}: average abs delta = {} microsec".format(j, deltas[j] / num_frames)

#num_pixels = loader.width * loader.height

## Build the decoders
#decoder_fac = holo3d.factory_decoder(loader.width, loader.height, loader.width)
#color_decoder = decoder_fac.color(loader.file_header.color_format)
#depth_decoder = decoder_fac.depth(loader.file_header.depth_format)
        
#color_input = frame.color_buffer 
#color_output = np.ndarray(shape=(num_pixels, 3), dtype=np.uint8)
#color_decoder.decode(color_output, color_input, num_pixels * 3, frame.color_buffer_size)

##print color_output[0:2]
        
#depth_input = frame.depth_buffer 
#depth_output = np.ndarray(shape=(num_pixels, 1), dtype=np.uint16)
#depth_decoder.decode(depth_output, depth_input, num_pixels * 2, frame.depth_buffer_size)
    
    
    
