# -*- python -*-

"""
Created on Wed Jan 14 13:00:35 2015

This is the beginning of the python prototyping code

@author: benjamin
"""
import holo3d
import datetime
import numpy as np
import os

HOLO_ROOT = str(os.environ['HOLO_ROOT'])
FILE_NAME = HOLO_ROOT + "/videos/cube_test/0.h3t"
MICROS_PER_SEC = 1000000

loader = holo3d.file_loader(FILE_NAME) # Load the file
loader.run() # This is very annoying to have to remember to do... I have to change the API

num_pixels = loader.width * loader.height

# Build the decoders
decoder_fac = holo3d.factory_decoder(loader.width, loader.height, loader.width)
color_decoder = decoder_fac.color(loader.file_header.color_format)
depth_decoder = decoder_fac.depth(loader.file_header.depth_format)

frame = holo3d.camera_frame() # Build a frame to use

loader.next_frame(frame) # Load the first frame
        
color_input = frame.color_buffer 
color_output = np.ndarray(shape=(num_pixels, 3), dtype=np.uint8)
color_decoder.decode(color_output, color_input, num_pixels * 3, frame.color_buffer_size)

#print color_output[0:2]
        
depth_input = frame.depth_buffer 
depth_output = np.ndarray(shape=(num_pixels, 1), dtype=np.uint16)
depth_decoder.decode(depth_output, depth_input, num_pixels * 2, frame.depth_buffer_size)
    