#ifndef __BLOB_TRACKER_HPP__
#define __BLOB_TRACKER_HPP__

#include <boost/python.hpp>
namespace bp = boost::python;

#include "boost/numpy.hpp"
namespace bn = boost::numpy;

#include <vector>

#include <Eigen/Dense>
typedef Eigen::Matrix<double, 2, 1> Vector2d;
typedef Eigen::Matrix<double, 2, 2> Matrix22d;
typedef Eigen::Matrix<double, 2, Eigen::Dynamic> Array2Xd;

#define PY_ASSERT(expr) { if(!(expr)) { \
        PyErr_SetString(PyExc_TypeError, (boost::format("PY_ASSERT(%1%:%2%): !(%3%) '%4%'") % (__FILE__) % (__LINE__) % (expr) % (#expr)).str().c_str()); \
        bp::throw_error_already_set(); \
    }; };

#define PY_ASSERT_EQUALS(a, b) { if((a) != (b)) { \
        PyErr_SetString(PyExc_TypeError, (boost::format("PY_ASSERT_EQUALS(%1%:%2%): %3% != %4%") % (__FILE__) % (__LINE__) % (a) % (b)).str().c_str()); \
        bp::throw_error_already_set(); \
    }; };

namespace holo3d
{

// There should never be more then this-1 clusters active at the same time
const size_t MAX_POSSIBLE_GROUPINGS = 100;

struct Point : Vector2d
{
    Point() : Vector2d() { }
    Point(const Vector2d& that) : Vector2d(that) { }

    inline const double x() const { return operator()(0); }
    inline const double y() const { return operator()(1); }
    inline double& x() { return operator()(0); }
    inline double& y() { return operator()(1); }
};
typedef std::vector<Point> PointVec;

struct Cluster
{
    Point mean;
    Point size_sq;
    bool settled; 
    size_t num_points;
};
typedef std::vector<Cluster> ClusterVec;

struct Settings
{
    Settings(const bp::tuple);

    int left;
    int right;
    int bottom;
    int top;
    size_t min_num_points;
    size_t max_num_iterations;

    Point origin;
    Matrix22d matrix;

    double radius;
    double percent_samples;
    double radius_sq;

    bool use_transform = false;
};

class BlobTracker
{
public:
    BlobTracker(const bp::tuple tup);
    virtual ~BlobTracker();

    void set_points(const bn::ndarray& points);

    void set_clusters(const bp::list& clusters);

    size_t num_clusters() const { return _clusters.size(); }

    bp::list clusters();

    bn::ndarray track_map_points_to_indices();

    bp::list track_reduce_points();

    bp::list track_recalc_existing_clusters();

    bp::list merge_clusters();

    bp::list partitioned_merge_clusters();

    bp::list run_EM();

    bp::list cluster_points();

    bp::list track_clusters();

    void create_clusters();

protected:

    const Settings _settings;

    PointVec _points;

    friend std::ostream& operator<<(std::ostream&, const Cluster&);

    ClusterVec _clusters;   // << The current working set of clusters

    typedef std::vector<int> IdxVec;
    std::vector<IdxVec> _grouped_indices;

    std::vector<Array2Xd> _grouped_points;

    ClusterVec _temp_clusters;    // << The recently merged clusters

    ClusterVec _create_clusters_helper(PointVec& points);

    bp::list _gen_cluster_list(const ClusterVec& clusters);

    // Methods which do the actual work.
    void _track_reduce_points();
    ClusterVec _track_recalc_existing_clusters();

    void _merge_clusters(ClusterVec& clusters) const; 

    void _partitioned_merge_clusters(ClusterVec& clusters) const;

    void _run_EM(const PointVec& points, const ClusterVec& clusters, ClusterVec& out_clusters);

    void _cluster_points(const PointVec& points, ClusterVec& clusters);

    void _track_clusters();
};

void export_tracker();

}; // namespace holo3d

#endif // __BLOB_TRACKER_HPP__
