#include <boost/python.hpp>
using namespace boost::python;

#include <boost/format.hpp>
#include "util_funcs.hpp"

#include "loader.hpp"
#include "frame.hpp"

using namespace holo3d;

namespace
{

class file_loader_wrap : public file_loader
{
public:

    file_loader_wrap(const std::string& file_name)
    : file_loader(file_name)
    {

    }

    FILE_HEADER file_header()
    {
        return loader_base::file_header();
    }

    bool next_frame(PyObject * obj)
    {
        INFO("Object = %1%", obj->ob_type->tp_name);
        extract<camera_frame_wrap&> extracted(obj);
        extracted.result_type
        if (!extracted.check())
        {
//          extracted.
            ERROR("Failed to extract a camera_frame");
            return false;
        }

        camera_frame_wrap& frame = extracted();

        // return file_loader::next_frame(frame);
        return false;
    }

};

}; // namespace

void export_loader()
{
    class_<file_loader_wrap>("file_loader", init<std::string>())
        .def("width", &loader_base::width)
        .def("height", &loader_base::height)
        .def("file_header", &file_loader_wrap::file_header)
        .def("valid", &file_loader::valid)
        .def("connection_info", &file_loader::connection_info)
        .def("run", &file_loader::run)
        .def("next_frame", &file_loader_wrap::next_frame)
    ;
}
