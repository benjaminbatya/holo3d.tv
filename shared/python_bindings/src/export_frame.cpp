#include <string>
#include <boost/python.hpp>
using namespace boost::python;

#include "frame.hpp"

using namespace holo3d;

namespace // Doesn't clutter up the global namespace
{

class camera_frame_wrap : public camera_frame
{

public:
    FILE_HEADER get_file_header() const { return file_header(); }
    void        set_file_header(const FILE_HEADER& header) { file_header(header); }

    FRAME_HEADER get_frame_header() const { return frame_header(); }
    void         set_frame_header(const FRAME_HEADER& header) { frame_header(header); }

    uint64_t    get_frame_number() const { return frame_number(); }
    void        set_frame_number(uint64_t fn) { frame_number(fn); }

    uint64_t    get_color_time_stamp() const { return color_time_stamp(); }
    void        set_color_time_stamp(uint64_t ts) { color_time_stamp(ts); }

    uint64_t    get_depth_time_stamp() const { return depth_time_stamp(); }
    void        set_depth_time_stamp(uint64_t ts) { depth_time_stamp(ts); }

    uint32_t    get_color_buffer_size() const { return color_buffer_size(); }
    void        set_color_buffer_size(uint32_t size) { color_buffer_size(size); }

    uint32_t    get_depth_buffer_size() const { return depth_buffer_size(); }
    void        set_depth_buffer_size(uint32_t size) { depth_buffer_size(size); }

    std::weak_ptr<uint8_t> color_buffer() { return std::shared_ptr<uint8_t>(camera_frame::color_buffer()); }
    std::weak_ptr<uint8_t> depth_buffer() { return std::shared_ptr<uint8_t>(camera_frame::depth_buffer()); }

}; // class camera_frame_wrap

}; // namespace

void export_frame()
{
    class_<camera_frame_wrap>("camera_frame", init<>())
        .def(init<camera_frame_wrap>())
        .add_property("file_header", &camera_frame_wrap::get_file_header, &camera_frame_wrap::set_file_header)
        .add_property("frame_header", &camera_frame_wrap::get_frame_header, &camera_frame_wrap::set_frame_header)
        .add_property("frame_number", &camera_frame_wrap::get_frame_number, &camera_frame_wrap::set_frame_number)
        .add_property("color_time_stamp", &camera_frame_wrap::get_color_time_stamp, &camera_frame_wrap::set_color_time_stamp)
        .add_property("depth_time_stamp", &camera_frame_wrap::get_depth_time_stamp, &camera_frame_wrap::set_depth_time_stamp)
        .add_property("color_buffer_size", &camera_frame_wrap::get_color_buffer_size, &camera_frame_wrap::set_color_buffer_size)
        .add_property("depth_buffer_size", &camera_frame_wrap::get_depth_buffer_size, &camera_frame_wrap::set_depth_buffer_size)
        .add_property("width", &camera_frame::width)
        .add_property("height", &camera_frame::height)
        .def("reset", &camera_frame::reset)
        .add_property("color_buffer", &camera_frame_wrap::color_buffer)
        .add_property("depth_buffer", &camera_frame_wrap::depth_buffer)
        .def("valid", &camera_frame::valid)
    ;

};
