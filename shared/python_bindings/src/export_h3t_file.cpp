#include <boost/python.hpp>
using namespace boost::python;

#include "h3t_file.hpp"

using namespace holo3d;

void export_h3t_file()
{
    enum_<STREAM_FORMAT>("STREAM_FORMAT")
        .value("COLOR_RGB888",  STREAM_FORMAT::COLOR_RGB888)
        .value("COLOR_YUV422",  STREAM_FORMAT::COLOR_YUV422)
        .value("COLOR_YUYV",    STREAM_FORMAT::COLOR_YUYV)
        .value("COLOR_YV12",    STREAM_FORMAT::COLOR_YV12)
        .value("COLOR_JPEG",    STREAM_FORMAT::COLOR_JPEG)
        .value("COLOR_VP8",     STREAM_FORMAT::COLOR_VP8)

        .value("DEPTH_RAW",     STREAM_FORMAT::DEPTH_RAW)
        .value("DEPTH_ENC",     STREAM_FORMAT::DEPTH_ENC)
        .value("DEPTH_JPEG",    STREAM_FORMAT::DEPTH_JPEG)
        .value("DEPTH_PNG",     STREAM_FORMAT::DEPTH_PNG)
    ;


    // FILE_HEADER
    class_<FILE_HEADER>("FILE_HEADER")
        .def_readonly("magic", &FILE_HEADER::magic)
        .def_readonly("major_version", &FILE_HEADER::major_version)
        .def_readonly("minor_version", &FILE_HEADER::minor_version)
        .def_readwrite("color_format", &FILE_HEADER::color_format)
        .def_readwrite("depth_format", &FILE_HEADER::depth_format)
        .def_readwrite("width", &FILE_HEADER::width)
        .def_readwrite("height", &FILE_HEADER::height)
    ;

    class_<FRAME_HEADER>("FRAME_HEADER")
        .def_readwrite("frame_number", &FRAME_HEADER::frame_number)
        .def_readwrite("color_time_stamp", &FRAME_HEADER::color_time_stamp)
        .def_readwrite("depth_time_stamp", &FRAME_HEADER::depth_time_stamp)
        .def_readwrite("color_frame_size", &FRAME_HEADER::color_frame_size)
        .def_readwrite("depth_frame_size", &FRAME_HEADER::depth_frame_size)
    ;

    class_<RGB888Pixel>("RGB888Pixel");
//  class_<DepthPixel>("DepthPixel");
}




