
#include <boost/python.hpp>
namespace bp = boost::python;

#include "boost/numpy.hpp"
namespace bn = boost::numpy;

#include <boost/format.hpp>
#include "util_funcs.hpp"

#include "h3t_file.hpp"
#include "frame.hpp"
#include "loader.hpp"
#include "decoder.hpp"

#include "blob_tracker.hpp"

using namespace holo3d;

namespace // Doesn't clutter up the global namespace
{

const std::string default_extension()
{
    return DEFAULT_FILE_EXTENSION;
}

void export_h3t_file()
{
    bp::enum_<STREAM_FORMAT>("STREAM_FORMAT")
        .value("COLOR_RGB888",  STREAM_FORMAT::COLOR_RGB888)
        .value("COLOR_YUV422",  STREAM_FORMAT::COLOR_YUV422)
        .value("COLOR_YUYV",    STREAM_FORMAT::COLOR_YUYV)
        .value("COLOR_YV12",    STREAM_FORMAT::COLOR_YV12)
        .value("COLOR_JPEG",    STREAM_FORMAT::COLOR_JPEG)
        .value("COLOR_VP8",     STREAM_FORMAT::COLOR_VP8)

        .value("DEPTH_RAW",     STREAM_FORMAT::DEPTH_RAW)
        .value("DEPTH_ENC",     STREAM_FORMAT::DEPTH_ENC)
        .value("DEPTH_JPEG",    STREAM_FORMAT::DEPTH_JPEG)
        .value("DEPTH_PNG",     STREAM_FORMAT::DEPTH_PNG)
    ;

    // FILE_HEADER
    bp::class_<STREAM_HEADER>("FILE_HEADER")
        .def_readonly("magic", &STREAM_HEADER::magic)
        .def_readonly("major_version", &STREAM_HEADER::major_version)
        .def_readonly("minor_version", &STREAM_HEADER::minor_version)
        .def_readwrite("color_format", &STREAM_HEADER::color_format)
        .def_readwrite("depth_format", &STREAM_HEADER::depth_format)
        .def_readwrite("width", &STREAM_HEADER::width)
        .def_readwrite("height", &STREAM_HEADER::height)
    ;

    bp::class_<FRAME_HEADER>("FRAME_HEADER")
        .def_readwrite("frame_number", &FRAME_HEADER::frame_number)
        .def_readwrite("color_time_stamp", &FRAME_HEADER::color_time_stamp)
        .def_readwrite("depth_time_stamp", &FRAME_HEADER::depth_time_stamp)
        .def_readwrite("color_frame_size", &FRAME_HEADER::color_buffer_size)
        .def_readwrite("depth_frame_size", &FRAME_HEADER::depth_buffer_size)
    ;

    bp::def("DEFAULT_EXTENSION", default_extension);

//  bp::class_<RGB888Pixel>("RGB888Pixel");
}

class camera_frame_wrap : public camera_frame
{

public:
    STREAM_HEADER get_file_header() const { return file_header(); }
    void        set_file_header(const STREAM_HEADER& header) { file_header(header); }

    FRAME_HEADER get_frame_header() const { return frame_header(); }
    void         set_frame_header(const FRAME_HEADER& header) { frame_header(header); }

    uint64_t    get_frame_number() const { return frame_number(); }
    void        set_frame_number(uint64_t fn) { frame_number(fn); }

    uint64_t    get_color_time_stamp() const { return color_time_stamp(); }
    void        set_color_time_stamp(uint64_t ts) { color_time_stamp(ts); }

    uint64_t    get_depth_time_stamp() const { return depth_time_stamp(); }
    void        set_depth_time_stamp(uint64_t ts) { depth_time_stamp(ts); }

    uint32_t    get_color_buffer_size() const { return color_buffer_size(); }
    void        set_color_buffer_size(uint32_t size) { color_buffer_size(size); }

    uint32_t    get_depth_buffer_size() const { return depth_buffer_size(); }
    void        set_depth_buffer_size(uint32_t size) { depth_buffer_size(size); }

    // This returns an mutable ndarray with copying, not used for now...
//  bn::ndarray color_buffer()
//  {
//      const uint8_t* buf = camera_frame::color_buffer();
//
//      // This copies the buffer into a bn::ndarray object
//      Py_intptr_t shape[1] = { get_color_buffer_size() };
//      bn::ndarray result = bn::zeros(1, shape, bn::dtype::get_builtin<uint8_t>());
//      std::copy(buf, buf+get_color_buffer_size(), reinterpret_cast<uint8_t*>(result.get_data()));
//      return result;
//  }
//  bn::ndarray depth_buffer()
//  {
//      uint8_t* buf = camera_frame::depth_buffer();
//      Py_intptr_t shape[1] = { get_depth_buffer_size() };
//      bn::ndarray result = bn::zeros(1, shape, bn::dtype::get_builtin<uint8_t>());
//      std::copy(buf, buf+get_depth_buffer_size(), reinterpret_cast<uint8_t*>(result.get_data()));
//      return result;
//  }
//

    // This returns a immutable ndarray to the color_buffer without copying
    static bn::ndarray color_buffer(bp::object & self)
    {
        bp::extract<camera_frame_wrap&> extracted(self);
        if (!extracted.check())
        {
            PyErr_SetString(PyExc_TypeError, "Failed to extract a camera_frame");
            bp::throw_error_already_set();
        }
        camera_frame & frame = static_cast<camera_frame&>(extracted());
        const uint8_t* buf = frame.color_buffer();

        // This returns an immutable ndarray which is not copied
        bn::ndarray result = bn::from_data(buf, bn::dtype::get_builtin<uint8_t>(),
                                           bp::make_tuple(frame.color_buffer_size()),
                                           bp::make_tuple(frame.depth_buffer_size()),
                                           self);
        return result;
    }

    static bn::ndarray depth_buffer(bp::object & self)
    {
        bp::extract<camera_frame_wrap&> extracted(self);
        if (!extracted.check())
        {
            PyErr_SetString(PyExc_TypeError, "Failed to extract a camera_frame");
            bp::throw_error_already_set();
        }

        camera_frame & frame = static_cast<camera_frame&>(extracted());
        const uint8_t* buf = frame.depth_buffer();

        // This returns an immutable ndarray which is not copied
        bn::ndarray result = bn::from_data(buf, bn::dtype::get_builtin<uint8_t>(),
                                           bp::make_tuple(frame.depth_buffer_size()),
                                           bp::make_tuple(frame.depth_buffer_size()),
                                           self);
        return result;
    }

}; // class camera_frame_wrap

void export_frame()
{

    bp::class_<camera_frame_wrap>("camera_frame", bp::init<>())
        .def(bp::init<camera_frame_wrap>())
        .add_property("file_header", &camera_frame_wrap::get_file_header, &camera_frame_wrap::set_file_header)
        .add_property("frame_header", &camera_frame_wrap::get_frame_header, &camera_frame_wrap::set_frame_header)
        .add_property("frame_number", &camera_frame_wrap::get_frame_number, &camera_frame_wrap::set_frame_number)
        .add_property("color_time_stamp", &camera_frame_wrap::get_color_time_stamp, &camera_frame_wrap::set_color_time_stamp)
        .add_property("depth_time_stamp", &camera_frame_wrap::get_depth_time_stamp, &camera_frame_wrap::set_depth_time_stamp)
        .add_property("color_buffer_size", &camera_frame_wrap::get_color_buffer_size, &camera_frame_wrap::set_color_buffer_size)
        .add_property("depth_buffer_size", &camera_frame_wrap::get_depth_buffer_size, &camera_frame_wrap::set_depth_buffer_size)
        .add_property("width", &camera_frame::width)
        .add_property("height", &camera_frame::height)
        .def("reset", &camera_frame::reset)
        .add_property("color_buffer", &camera_frame_wrap::color_buffer)
        .add_property("depth_buffer", &camera_frame_wrap::depth_buffer)
        .def("valid", &camera_frame::valid)
    ;

};

class file_loader_wrap : public file_loader
{
public:

    file_loader_wrap(const std::string& file_name)
    : file_loader(file_name)
    {

    }

    STREAM_HEADER file_header()
    {
        return loader_base::file_header();
    }

    bool next_frame(PyObject * obj)
    {
        // INFO("Object = %1%", obj->ob_type->tp_name);
        bp::extract<camera_frame_wrap&> extracted(obj);
        if (!extracted.check())
        {
            PyErr_SetString(PyExc_TypeError, "Failed to extract a camera_frame");
            bp::throw_error_already_set();
        }

        camera_frame_wrap& frame = extracted();

        return file_loader::next_frame(frame);
    }

};

void export_loader()
{
    bp::class_<file_loader_wrap>("file_loader", bp::init<std::string>())
        .add_property("width", &loader_base::width)
        .add_property("height", &loader_base::height)
        .add_property("file_header", &file_loader_wrap::file_header)
        .def("valid", &file_loader::valid)
        .add_property("connection_info", &file_loader::connection_info)
        .def("run", &file_loader::run)
        .def("next_frame", &file_loader_wrap::next_frame)
    ;
}

class color_decoder_wrap
{
    decoder::color_decoder_t m_worker;

public:
    color_decoder_wrap()
    {
        PyErr_SetString(PyExc_TypeError, "Cannot create a standalone color_decoder!");
        bp::throw_error_already_set();
    }

    color_decoder_wrap(decoder::color_decoder_t worker)
    : m_worker(worker)
    {

    }

    void decode(bn::ndarray& output,
                const bn::ndarray& input,
                const size_t output_buffer_size_in_bytes,
                const size_t input_buffer_size_in_bytes)
    {
        PY_ASSERT_EQUALS(output.get_dtype().get_itemsize(), sizeof(uint8_t));
        PY_ASSERT_EQUALS(input.get_dtype().get_itemsize(), sizeof(uint8_t));

        PY_ASSERT_EQUALS(output.get_nd(), 2);
//      INFO("input.get_nd() = %1%", input.get_nd());
        PY_ASSERT_EQUALS(input.get_nd(), 1);

        PY_ASSERT_EQUALS((size_t)output.shape(0), m_worker->num_pixels());
        PY_ASSERT_EQUALS(output_buffer_size_in_bytes, m_worker->num_pixels() * 3);
        PY_ASSERT_EQUALS(output.shape(1), 3);

        PY_ASSERT_EQUALS((size_t)input.shape(0), input_buffer_size_in_bytes);

        PY_ASSERT(output.get_flags() & bn::ndarray::WRITEABLE);
        PY_ASSERT(output.get_flags() & bn::ndarray::C_CONTIGUOUS);

//      INFO("output type size = %1%, input type size = %2%",
//           output.get_dtype().get_itemsize(),
//           input.get_dtype().get_itemsize());

        // We assume that the data is contiguous
        RGB888Pixel* out_buf = reinterpret_cast<RGB888Pixel*>(output.get_data());
        const uint8_t* in_buf = reinterpret_cast<const uint8_t*>(input.get_data());

        m_worker->decode(out_buf, in_buf,
                         output_buffer_size_in_bytes,
                         input_buffer_size_in_bytes);
    }
};

class depth_decoder_wrap
{
    decoder::depth_decoder_t m_worker;

public:
    depth_decoder_wrap()
    {
        PyErr_SetString(PyExc_TypeError, "Cannot create a standalone depth_decoder!");
        bp::throw_error_already_set();
    }

    depth_decoder_wrap(decoder::depth_decoder_t worker)
    : m_worker(worker)
    {

    }

    void use_histogram(const bool flag)
    {
        m_worker->use_histogram(flag);
    }

    void decode(bn::ndarray& output,
                const bn::ndarray& input,
                const size_t output_buffer_size_in_bytes,
                const size_t input_buffer_size_in_bytes)
    {
        PY_ASSERT_EQUALS(output.get_dtype().get_itemsize(), sizeof(uint8_t));
        PY_ASSERT_EQUALS(input.get_dtype().get_itemsize(), sizeof(uint8_t));

        PY_ASSERT_EQUALS(output.get_nd(), 2);
//      INFO("input.get_nd() = %1%", input.get_nd());
        PY_ASSERT_EQUALS(input.get_nd(), 1);

        PY_ASSERT_EQUALS((size_t)output.shape(0), m_worker->num_pixels());
        PY_ASSERT_EQUALS(output_buffer_size_in_bytes, m_worker->num_pixels() * 3);
        PY_ASSERT_EQUALS(output.shape(1), 3);

        PY_ASSERT_EQUALS((size_t)input.shape(0), input_buffer_size_in_bytes);

        PY_ASSERT(output.get_flags() & bn::ndarray::WRITEABLE);
        PY_ASSERT(output.get_flags() & bn::ndarray::C_CONTIGUOUS);

//      INFO("output type size = %1%, input type size = %2%",
//           output.get_dtype().get_itemsize(),
//           input.get_dtype().get_itemsize());

        // We assume that the data is contiguous
        RGB888Pixel* out_buf = reinterpret_cast<RGB888Pixel*>(output.get_data());
        const uint8_t* in_buf = reinterpret_cast<const uint8_t*>(input.get_data());

        m_worker->decode(out_buf, in_buf,
                         output_buffer_size_in_bytes,
                         input_buffer_size_in_bytes);
    }

    void decode_depth(bn::ndarray& output,
                      const bn::ndarray& input,
                      const size_t output_buffer_size_in_bytes,
                      const size_t input_buffer_size_in_bytes)
    {
        PY_ASSERT_EQUALS(output.get_dtype().get_itemsize(), sizeof(uint16_t));
        PY_ASSERT_EQUALS(input.get_dtype().get_itemsize(), sizeof(uint8_t));

        PY_ASSERT_EQUALS(output.get_nd(), 2);
//      INFO("input.get_nd() = %1%", input.get_nd());
        PY_ASSERT_EQUALS(input.get_nd(), 1);

        PY_ASSERT_EQUALS((size_t)output.shape(0), m_worker->num_pixels());
        PY_ASSERT_EQUALS(output_buffer_size_in_bytes, m_worker->num_pixels() * 2);
        PY_ASSERT_EQUALS(output.shape(1), 1);

        PY_ASSERT_EQUALS((size_t)input.shape(0), input_buffer_size_in_bytes);

        PY_ASSERT(output.get_flags() & bn::ndarray::WRITEABLE);
        PY_ASSERT(output.get_flags() & bn::ndarray::C_CONTIGUOUS);

//      INFO("output type size = %1%, input type size = %2%",
//           output.get_dtype().get_itemsize(),
//           input.get_dtype().get_itemsize());

        // We assume that the data is contiguous
        DepthPixel* out_buf = reinterpret_cast<DepthPixel*>(output.get_data());
        const uint8_t* in_buf = reinterpret_cast<const uint8_t*>(input.get_data());

        m_worker->decode_depth(out_buf, in_buf,
                               output_buffer_size_in_bytes,
                               input_buffer_size_in_bytes);
    }
};

class factory_wrap : public decoder::factory
{
public:
    factory_wrap(size_t width, size_t height, size_t output_stride_in_pixels)
    : decoder::factory(width, height, output_stride_in_pixels)
    {

    }

    color_decoder_wrap color(STREAM_FORMAT format)
    {
        decoder::color_decoder_t decoder = decoder::factory::color(format);

        return color_decoder_wrap(decoder);
    }

    depth_decoder_wrap depth(STREAM_FORMAT format)
    {
        decoder::depth_decoder_t decoder = decoder::factory::depth(format, false);
        return depth_decoder_wrap(decoder);
    }
};

void export_decoder()
{
    bp::class_<factory_wrap>("factory_decoder", bp::init<size_t, size_t, size_t>())
        .def("color", &factory_wrap::color) // , bp::return_value_policy<bp::manage_new_object>())
        .def("depth", &factory_wrap::depth) // , bp::return_value_policy<bp::manage_new_object>())
    ;

    bp::class_<color_decoder_wrap>("color_decoder")
        .def("decode", &color_decoder_wrap::decode)
    ;

    bp::class_<depth_decoder_wrap>("depth_decoder")
        .def("decode", &depth_decoder_wrap::decode)
        .def("decode_depth", &depth_decoder_wrap::decode_depth)
        .def("use_histogram", &depth_decoder_wrap::use_histogram)
    ;
}

}; // namespace

BOOST_PYTHON_MODULE(holo3d)
{
    // This has to be run before any boost.numpy wrapping can be done
    bn::initialize(); 

    export_h3t_file();

    export_frame();

    export_loader();

    export_decoder();

    holo3d::export_tracker();

}

