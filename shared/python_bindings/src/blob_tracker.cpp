
#include "blob_tracker.hpp"

#include <boost/format.hpp>
#include "util_funcs.hpp"

#include <cmath>

namespace holo3d
{

std::ostream& operator<<(std::ostream& os, const Point& pt)
{
    os << "(" << pt.x() << ", " << pt.y() << ")";
    return os;
}

std::ostream& operator<<(std::ostream& os, const Cluster& c)
{
    os << "(" << c.mean << ", " << c.size_sq << ", " << c.settled << ", " << c.num_points << ")";
    return os;
}

template<int COLS, int ROWS>
Eigen::Matrix<double, COLS, ROWS> extract_matrix(const bp::tuple& tup, const size_t index)
{
    auto matrix_ext = bp::extract<bn::ndarray>(tup[index]);
    PY_ASSERT(matrix_ext.check());

    const bn::ndarray& mat_array = matrix_ext();
    PY_ASSERT(bn::equivalent(mat_array.get_dtype(), bn::dtype::get_builtin<double>()) );
    if (ROWS > 1) 
    {
        PY_ASSERT_EQUALS(mat_array.get_nd(), 2); 
        PY_ASSERT_EQUALS(mat_array.shape(0), COLS);
        PY_ASSERT_EQUALS(mat_array.shape(1), ROWS);
    } else
    {
        PY_ASSERT_EQUALS(mat_array.get_nd(), 1); 
        PY_ASSERT_EQUALS(mat_array.shape(0), COLS);
    }

    // NOTE: I have no idea what numpy is doing here with strides.
    // This was obtained through experimenting below
    size_t stride = mat_array.get_strides()[0] / 8;
//  INFO("stride = %1%", stride);

    double* mat_data = (double*)mat_array.get_data();

    Eigen::Matrix<double, COLS, ROWS> matrix;
    for (size_t i=0; i<ROWS; i++) 
    {
        for (size_t j=0; j<COLS; j++) 
        {
            size_t idx = j*stride + i;
//          INFO("idx = %1%", idx);
            matrix(j, i) = mat_data[idx];
        }
    }

    return matrix;
}

Cluster _expectation(const Array2Xd& points)
{
//  INFO("points =\n%1%", points);

    Vector2d mean = points.rowwise().sum();
    mean /= points.cols();

//  INFO("points.cols() = %1%, mean = %2%", points.cols(), mean);

    Vector2d min = points.rowwise().minCoeff();
    Vector2d max = points.rowwise().maxCoeff();

    Vector2d abs_min = (mean - min).cwiseAbs();
    Vector2d abs_max = (mean - max).cwiseAbs();

    Vector2d size = abs_min.cwiseMax(abs_max);

    Vector2d size_sq = size.cwiseProduct(size);

    Cluster cluster = { mean, size_sq, false, (size_t)points.cols() };

    return cluster;
}

// From http://stackoverflow.com/questions/9345087/choose-m-elements-randomly-from-a-vector-containing-n-elements
template<class bidiiter>
bidiiter random_unique(bidiiter begin, bidiiter end, size_t num_random) {
    size_t left = std::distance(begin, end);
    while (num_random--) {
        bidiiter r = begin;
        std::advance(r, rand()%left);
        std::swap(*begin, *r);
        ++begin;
        --left;
    }
    return begin;
}

Settings::Settings(const bp::tuple tup)
{
    PY_ASSERT(bp::len(tup) >= 7);

    left = bp::extract<int>(tup[0]);
    right = bp::extract<int>(tup[1]);
    bottom = bp::extract<int>(tup[2]);
    top = bp::extract<int>(tup[3]);
    radius = bp::extract<double>(tup[4]);
    percent_samples = bp::extract<double>(tup[5]); 
    min_num_points = bp::extract<int>(tup[6]);

    if (bp::len(tup) >= 8) 
    {
        max_num_iterations = bp::extract<int>(tup[7]);
    }

    if (bp::len(tup) >= 10) 
    {
        use_transform = true;
        INFO("Extracting origin");
        origin = extract_matrix<2,1>(tup, 8);
        INFO("Extracting matrix");
        matrix = extract_matrix<2,2>(tup, 9);

        INFO("got origin = %1%, matrix = %2%", origin, matrix);
    }

    radius_sq = radius*radius;
}

BlobTracker::BlobTracker(const bp::tuple tup)
: _settings(tup)
, _grouped_indices(MAX_POSSIBLE_GROUPINGS)
, _grouped_points(MAX_POSSIBLE_GROUPINGS)
{
//  INFO("called, left=%d, right=%d, bottom=%d, top=%d", _left, _right, _bottom, _top);
}

BlobTracker::~BlobTracker()
{
//  INFO("called");
}

// NOTE: maybe don't copy the point data..
void BlobTracker::set_points(const bn::ndarray& points)
{
//  INFO("called");

    PY_ASSERT_EQUALS(points.get_nd(), 2);
    PY_ASSERT_EQUALS(points.shape(1), 2);

    size_t num_points = points.shape(0);

    _points.resize(num_points);

    // Handle either type of ndarray order
    int offset = 1;
    int step = 2;
    if(points.get_flags() & bn::ndarray::F_CONTIGUOUS)
    {
        offset = num_points;
        step = 1;
    }

    //  INFO("stride/8=%1%, num_points=%2%, format=%3%",
    //       points.get_strides()[1]/8, num_points,
    //       points.get_flags() & bn::ndarray::F_CONTIGUOUS ? "F_CONTIGUOUS":
    //       (points.get_flags() & bn::ndarray::C_CONTIGUOUS ? "C_CONTIGUOUS" : "None"));

    if (bn::equivalent(points.get_dtype(), bn::dtype::get_builtin<double>()))
    {
        double* pyData = reinterpret_cast<double*>(points.get_data());

        for (size_t i=0; i<num_points; i++) 
        {
            _points[i].x() = *pyData;
            _points[i].y() = *(pyData+offset);
            pyData += step;
        }
    } else if (bn::equivalent(points.get_dtype(), bn::dtype::get_builtin<float>()))
    {
        float* pyData = reinterpret_cast<float*>(points.get_data());

        for (size_t i=0; i<num_points; i++) 
        {
            _points[i].x() = *pyData;
            _points[i].y() = *(pyData+offset);
            pyData += step;
        }
    } else
    {
        PY_ASSERT(false && "Type of point array must be float or double!");
    }

//  for (size_t i=0; i<10; i++)
//  {
//      INFO("point %1% = %2%", i, _points[i]);
//  }
}

void BlobTracker::set_clusters(const bp::list& clusters)
{
//  INFO("called");

    bp::ssize_t num_clusters = bp::len(clusters);

    _clusters.resize(num_clusters);

    for (bp::ssize_t i=0; i<num_clusters; i++) 
    {
        Cluster& cluster_cpp = _clusters[i];

        bp::object elem = clusters[i];

        auto tup_ext = bp::extract<bp::tuple>(elem);
        PY_ASSERT(tup_ext.check());

        const bp::tuple& tup = tup_ext();
        PY_ASSERT_EQUALS(bp::len(tup), 5);

        cluster_cpp.mean = extract_matrix<2,1>(tup, 0);

//      if (i == 0)
//      {
//          for (int j=0; j<4; j++)
//          {
//              INFO("mean value = %1%", mean_data[j*3992/8]);
//          }
//
//          INFO("mean, nd=%1%, shape=%2%, strides=%3%, dtype=%4%",
//               mean.get_nd(), mean.get_shape()[0],
//               mean.get_strides()[0], mean.get_dtype().get_itemsize());
//      }

        cluster_cpp.size_sq.x() = bp::extract<double>(tup[1]);
        cluster_cpp.size_sq.y() = bp::extract<double>(tup[2]);
        cluster_cpp.settled = bp::extract<bool>(tup[3]);
        cluster_cpp.num_points = bp::extract<int>(tup[4]); 

//      INFO("Got cluster = %1%", cluster_cpp);
    }

//  INFO("num _clusters = %1%", _clusters.size());

}

bp::list BlobTracker::clusters()
{
    bp::list ret = _gen_cluster_list(_clusters);

    return ret;
}

ClusterVec BlobTracker::_create_clusters_helper(PointVec& points)
{
    IdxVec nums(points.size());
    for (size_t i=0; i<points.size(); i++) 
    {
        nums[i] = (int)i;
    }

    // Choose the percent_samples of the number of valid_points
    size_t num_samples =  (size_t)(_settings.percent_samples * points.size());

    random_unique(points.begin(), points.end(), num_samples);

//      INFO("Got %1% valid points, num_samples = %2%", valid_points.size(), num_samples);

    ClusterVec new_clusters(0);
    for (size_t i=0; i<num_samples; i++) 
    {
        Cluster c = { points[i], Vector2d(_settings.radius_sq, _settings.radius_sq), false, 0};
        new_clusters.push_back(c);
    }

    return new_clusters;
}

void BlobTracker::create_clusters()
{
    _clusters = _create_clusters_helper(_points);
}

// NOTE: split this into protected and not-protected later...
bn::ndarray BlobTracker::track_map_points_to_indices()
{
    size_t num_points = _points.size();
    size_t num_clusters = _clusters.size();

    if (num_clusters < 1) 
    {
        num_points = 0;
    }

    bn::ndarray ret = bn::zeros(bp::make_tuple(num_points), bn::dtype::get_builtin<int>());

    int* data = reinterpret_cast<int*>(ret.get_data());

    for (size_t i=0; i<num_points; i++) 
    {
        // Find the best cluster or num_clusters if none of the clusters are close enough
        const Point& pt = _points[i];

        size_t best_cluster = num_clusters;
        double min_dist_sq = std::numeric_limits<double>::max();
        for (size_t j=0; j<num_clusters; j++) 
        {
            Vector2d delta = _clusters[j].mean - pt;
            double dist_sq = delta.squaredNorm();
            
            if (dist_sq < min_dist_sq) 
            {
                best_cluster = j;
                min_dist_sq = dist_sq;
            }
        }

        if (min_dist_sq > (_clusters[best_cluster].size_sq.sum())) 
        {
            best_cluster = num_clusters;
        }

//      if (i == 0)
//      {
//          INFO("After: best_cluster = %1%, min_dist_sq = %2%", best_cluster, min_dist_sq);
//      }

        data[i] = best_cluster;
    }

    return ret;
}

void BlobTracker::_track_reduce_points()
{
    size_t num_points = _points.size();
    size_t num_mapped_groups = _clusters.size() + 1;

    ASSERT(num_mapped_groups < MAX_POSSIBLE_GROUPINGS);

    bn::ndarray mapped_indices = track_map_points_to_indices();
    const int* data = reinterpret_cast<const int*>(mapped_indices.get_data());

    // Clear out all of the groups of indices (clustered and untracked)
    for (size_t i=0; i<num_mapped_groups; i++) 
    {
        _grouped_indices[i].clear();
    }

    // Group the indice data
    for (size_t i=0; i<num_points; i++) 
    {
        size_t cluster_idx = data[i];
        _grouped_indices[cluster_idx].push_back(i);
    }

    // Group the point data
    for (size_t i=0; i<num_mapped_groups; i++) 
    {
        const IdxVec& indices = _grouped_indices[i];
        size_t size = indices.size();

        Array2Xd& points = _grouped_points[i];
        points.resize(Eigen::NoChange, size);

        for (size_t j=0; j<size; j++) 
        {
            size_t index = indices[j];
            const Point& pt = _points[index];
            points.col(j) = pt;
        }
    }
}

bp::list BlobTracker::track_reduce_points()
{
    _track_reduce_points();

    size_t num_mapped_groups = _clusters.size() + 1;

    // Convert the vector of vectors into bp::lists of lists
    bp::list ret;
    for (size_t i=0; i<num_mapped_groups; i++) 
    {
        const IdxVec indices = _grouped_indices[i];
        bp::list group;
        for(auto iter = indices.begin(); iter != indices.end(); iter++)
        {
            group.append(*iter);
        }
        ret.append(group);
    }

    return ret;
}

ClusterVec BlobTracker::_track_recalc_existing_clusters()
{
    _track_reduce_points();

    // Ignore the untracked points here.
    size_t num_clusters = _clusters.size();

    size_t num_valid_clusters = 0;
    for (size_t i=0; i<num_clusters; i++) 
    {
        const Array2Xd& points = _grouped_points[i];
        if (points.cols() >= (int)_settings.min_num_points) 
        {
            num_valid_clusters++;
        }
    }

    // NOTE: Get rid of new_clusters and replace it with _merged_clusters (which should be renamed to _temp_clusters)
//  _temp_clusters
    ClusterVec new_clusters(num_valid_clusters);
    size_t new_cluster_idx=0;

    for (size_t i=0; i<num_clusters; i++) 
    {
        const Array2Xd& points = _grouped_points[i];
        if (points.cols() < (int)_settings.min_num_points) 
        {
            continue;
        }

        Cluster cluster = _expectation(points);

        new_clusters[new_cluster_idx] = cluster;

        new_cluster_idx++;
    }

    return new_clusters;
}

bp::list BlobTracker::_gen_cluster_list(const ClusterVec& clusters)
{
    bp::list ret;

    for (auto it : clusters) 
    {
        bp::tuple tup = bp::make_tuple(it.mean, it.size_sq[0], it.size_sq[1], it.settled, it.num_points);

        ret.append(tup);
    }

    return ret;
}

bp::list BlobTracker::track_recalc_existing_clusters()
{
    ClusterVec new_clusters = _track_recalc_existing_clusters();

    bp::list ret = _gen_cluster_list(new_clusters);

    return ret;
}

// 
void merge_helper(ClusterVec& clusters)
{
    size_t num_clusters = clusters.size();

    size_t num_valid_clusters = 0;

    for (size_t i=0; i<num_clusters; i++) 
    {
        const Cluster& c_i = clusters[i];

        const Point& mean_i = c_i.mean;
//      INFO("idx = %1%, mean = %2%", i, mean_i);

        bool removed = false;

        for (size_t j=i+1; j<num_clusters; j++) 
        {
            const Cluster& c_j = clusters[j];
            const Vector2d& mean_j = c_j.mean;

            const Vector2d& size_sq_j = c_j.size_sq;

            Vector2d diff_vec = mean_i - mean_j;
            double delta_sq = diff_vec.squaredNorm();

            if (delta_sq < size_sq_j.sum()) 
            {
                removed = true;
                break;
            }
        }

        if (!removed) 
        {
            clusters[num_valid_clusters] = c_i;
            num_valid_clusters++;
        }
    }

    clusters.resize(num_valid_clusters);
}

void BlobTracker::_merge_clusters(ClusterVec& clusters) const
{
    // First merge forward
    merge_helper(clusters);

    // Reverse the clusters
    std::reverse(clusters.begin(), clusters.end());

    // Merge in reverse
    merge_helper(clusters);
}

// Returns the list of merged clusters
bp::list BlobTracker::merge_clusters()
{
    _merge_clusters(_clusters);

//  for (auto c : _clusters)
//  {
//      INFO("cluster = %1%", c);
//  }

    bp::list ret = _gen_cluster_list(_clusters);

    return ret;
}

//// From http://stackoverflow.com/questions/3407012/c-rounding-up-to-the-nearest-multiple-of-a-number
//template<typename T>
//T roundMultiple( T value, T multiple )
//{
//    if (multiple == 0) return value;
//    return static_cast<T>(std::round(static_cast<double>(value)/static_cast<double>(multiple))*static_cast<double>(multiple));
//}

double double_floor(double v) { return std::floor(v); }
double double_ceil(double v) { return std::ceil(v); }

void BlobTracker::_partitioned_merge_clusters(ClusterVec& clusters) const
{
    // Calculate the min and max
    Vector2d min(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
    Vector2d max(std::numeric_limits<double>::min(), std::numeric_limits<double>::min());

    // Make sure that we are dealing with more then one cluster
    if (clusters.size() < 2) 
    {
        return;
    }

    for (const auto& c : clusters) 
    {
        const Vector2d& mean = c.mean;

        min = min.cwiseMin(mean);
        max = max.cwiseMax(mean);
    }

    min = min.unaryExpr(std::ptr_fun(double_floor));
    max = max.unaryExpr(std::ptr_fun(double_ceil));

    double cell_size = _settings.radius * 2;
    size_t x_cell_count = (size_t)double_ceil((max[0] - min[0]) / cell_size);
    size_t y_cell_count = (size_t)double_ceil((max[1] - min[1]) / cell_size);

    // Create a grid of cells containing clusters
    std::vector<ClusterVec> cell_grid(x_cell_count*y_cell_count);

    for (Cluster c : clusters) 
    {
        const Vector2d& mean = c.mean;
        Vector2d cell_idx = (mean - min) / cell_size;
        int idx = (x_cell_count * (int)cell_idx[1]) + (int)cell_idx[0];
        cell_grid[idx].push_back(c);
    }

    for (size_t i=0; i<x_cell_count*y_cell_count; i++) 
    {
        ClusterVec& cell_clusters = cell_grid[i];
        _merge_clusters(cell_clusters);
    }

    clusters.resize(0);

    for (size_t i=0; i<x_cell_count*y_cell_count; i++) 
    {
        ClusterVec& cell_clusters = cell_grid[i];
        clusters.insert(clusters.end(), cell_clusters.begin(), cell_clusters.end());
    }

    _merge_clusters(clusters);
}

bp::list BlobTracker::partitioned_merge_clusters()
{
    _partitioned_merge_clusters(_clusters);

//  for (auto c : _clusters)
//  {
//      INFO("cluster = %1%", c);
//  }

    bp::list ret = _gen_cluster_list(_clusters);

    return ret;
}

void BlobTracker::_run_EM(const PointVec& points, const ClusterVec& clusters, ClusterVec& out_clusters)
{
//  INFO("Called, num points = %1%, num clusters = %2%", points.size(), clusters.size());
    size_t num_clusters = clusters.size();
    size_t num_points = points.size();

    // For each point, find the closest cluster
    IdxVec closest_clusters(num_points);

    for (size_t i=0; i<num_points; i++) 
    {
        const Point& pt = points[i];

        int closest_cluster = -1;
        double min_dist = std::numeric_limits<double>::max();
        for (size_t j=0; j<num_clusters; j++) 
        {
            const Cluster& c = clusters[j];

            const Point& mean = c.mean;
            const Point& size_sq = c.size_sq;

            Vector2d diff_vec = pt - mean;
            double delta_sq = diff_vec.squaredNorm();

            if (delta_sq < size_sq.sum())
            {
                if (closest_cluster < 0 || delta_sq < min_dist) 
                {
                    closest_cluster = j;
                    min_dist = delta_sq;
                }
            }
        }
        closest_clusters[i] = closest_cluster;
    }
    

    // Clear out all of the groups of indices (clustered and untracked)
    for (size_t i=0; i<num_clusters; i++) 
    {
        _grouped_indices[i].clear();
    }

    // Group the indice data (ignoring points that are too far from cluster centers)
    for (size_t i=0; i<num_points; i++) 
    {
        int cluster_idx = closest_clusters[i];
        if (cluster_idx < 0) { continue; }
        _grouped_indices[cluster_idx].push_back(i);
    }

    // Group the point data
    for (size_t i=0; i<num_clusters; i++) 
    {
        const IdxVec& indices = _grouped_indices[i];
        size_t size = indices.size();

//      INFO("Cluster %1%: size = %2%", i, size);

        Array2Xd& point_group = _grouped_points[i];
        point_group.resize(Eigen::NoChange, size);

        for (size_t j=0; j<size; j++) 
        {
            size_t index = indices[j];
            const Point& pt = points[index];
            point_group.col(j) = pt;
//          INFO("index = %1%, pt = %2%", index, pt);
        }
    }
    
    // Calculate the new mean and size of the clusters
    out_clusters.resize(0);
    for (size_t i=0; i<num_clusters; i++) 
    {
        const Cluster& old_cluster = clusters[i];

        const IdxVec& indices = _grouped_indices[i];

        // NOTE: EM will "lose" clusters which don't have enough member points
        if (indices.size() < _settings.min_num_points) 
        { 
//          INFO("Losing cluster %1%, size=%2%", i, indices.size());
            continue; 
        }

        const Array2Xd& grouped_points = _grouped_points[i];

        Cluster new_cluster = _expectation(grouped_points);

        // From http://stackoverflow.com/questions/15051367/how-to-compare-vectors-approximately-in-eigen
        if ((new_cluster.mean-old_cluster.mean).isMuchSmallerThan(0.0001)) 
        {
            new_cluster.settled = true;
        }

        out_clusters.push_back(new_cluster);
    }

}

bp::list BlobTracker::run_EM()
{
    _run_EM(_points, _clusters, _temp_clusters);

    bp::list ret = _gen_cluster_list(_temp_clusters);

    return ret;
}

void BlobTracker::_cluster_points(const PointVec& points, ClusterVec& clusters)
{
//  INFO("num clusters = %1%", clusters.size());

    for (size_t i=0; i<_settings.max_num_iterations; i++) 
    {
        _partitioned_merge_clusters(clusters);

        // Count the number of unsettled
        size_t num_unsettled = 0;
        for (auto& c : clusters)
        {
            if (!c.settled)
            {
                num_unsettled++;
            }
        }

//      INFO("Iteration %1%, after merging, number unsettled clusters = %2%", i, num_unsettled);
//      for (auto& c : clusters)
//      {
//          INFO("cluster = %1%", c);
//      }

        if (num_unsettled == 0) 
        {
            break;
        }

        _run_EM(points, clusters, _temp_clusters);

        clusters = _temp_clusters;

//      INFO("After EM, number clusters = %2%", i, clusters.size());
//      for (auto& c : clusters)
//      {
//          INFO("cluster = %1%", c);
//      }
    }
}

bp::list BlobTracker::cluster_points()
{
    _cluster_points(_points, _clusters);

    bp::list ret = _gen_cluster_list(_clusters);

    return ret;
}

void BlobTracker::_track_clusters()
{
    ClusterVec updated_clusters = _track_recalc_existing_clusters();

    const Array2Xd& untracked_points = _grouped_points[num_clusters()];

    PointVec valid_points(0);

    if (_settings.use_transform) 
    {
        for (int i=0; i<untracked_points.cols(); i++) 
        {
            Vector2d pt = untracked_points.col(i);
            // Remove the transformation from pt defined by _settings.matrix
            pt -= _settings.origin;
            pt = _settings.matrix * pt;
            pt += _settings.origin;

            if (pt[0] < _settings.left || pt[0] > _settings.right ||
                pt[1] < _settings.bottom || pt[1] > _settings.top) 
            {
                valid_points.push_back(pt);
            }
        }

    } else
    {
        for (int i=0; i<untracked_points.cols(); i++) 
        {
            Vector2d pt = untracked_points.col(i);

            if (pt[0] < _settings.left || pt[0] > _settings.right ||
                pt[1] < _settings.bottom || pt[1] > _settings.top) 
            {
                valid_points.push_back(pt);
            }
        }
    }

    if (valid_points.size() >= _settings.min_num_points) 
    {
        ClusterVec new_clusters = _create_clusters_helper(valid_points);

//      for (auto& c : new_clusters)
//      {
//          INFO("Before merging, new cluster = %1%", c);
//      }

//      for (auto pt : valid_points)
//      {
//          INFO("Valid point = %1%", pt);
//      }

        _cluster_points(valid_points, new_clusters);

//      for (auto& c : new_clusters)
//      {
//          INFO("After merging, new cluster = %1%", c);
//      }

        updated_clusters.insert(updated_clusters.end(), new_clusters.begin(), new_clusters.end());
    }

    _clusters = updated_clusters;
}

bp::list BlobTracker::track_clusters()
{
    _track_clusters();

    bp::list ret = _gen_cluster_list(_clusters);

    return ret;
}

// Code for boost::python

struct PointToNDArray
{
    static PyObject* convert(const Point& pt)
    {
       Py_intptr_t shape[1] = { 2 };
       bn::ndarray result = bn::zeros(1, shape, bn::dtype::get_builtin<double>());
       std::memcpy(result.get_data(), &pt, sizeof(Point));

       // Increment the reference count on the pointer in the boost manner
       return bp::incref(result.ptr());
    }
};

void export_tracker()
{
    // Register a Point-ndarray converter
    bp::to_python_converter<Point, PointToNDArray>();

    bp::class_<BlobTracker>("CPPTracker", bp::init<bp::tuple>())
        .def("set_points", &BlobTracker::set_points)
        .def("set_clusters", &BlobTracker::set_clusters)
        .def("create_clusters", &BlobTracker::create_clusters)
        .def("clusters", &BlobTracker::clusters)
        .def("num_clusters", &BlobTracker::num_clusters)
        .def("track_map_points_to_indices", &BlobTracker::track_map_points_to_indices)
        .def("track_reduce_points", &BlobTracker::track_reduce_points)
        .def("track_recalc_existing_clusters", &BlobTracker::track_recalc_existing_clusters)
        .def("merge_clusters", &BlobTracker::merge_clusters)
        .def("partitioned_merge_clusters", &BlobTracker::partitioned_merge_clusters)
        .def("run_EM", &BlobTracker::run_EM)
        .def("cluster_points", &BlobTracker::cluster_points)
        .def("track_clusters", &BlobTracker::track_clusters)
    ;
}

}; // namespace holo3d
