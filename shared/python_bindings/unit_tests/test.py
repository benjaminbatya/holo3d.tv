# -*- python -*-

"""
Created on Wed Jan 14 13:00:35 2015

@author: benjamin
"""
import unittest
import holo3d
import datetime
import numpy as np
import os

HOLO_ROOT = str(os.environ['HOLO_ROOT'])
FILE_NAME = HOLO_ROOT + "/videos/test1/0.h3t"
MICROS_PER_SEC = 1000000

class TestBasic(unittest.TestCase):
        
    def testLoading(self):
    
        loader = holo3d.file_loader(FILE_NAME)
        self.assert_(loader.run())

        frame = holo3d.camera_frame()

        header = loader.file_header

        self.assertEquals(header.width, 640)
        self.assertEquals(header.height, 480)

        self.assert_(loader.next_frame(frame))

        color_time = frame.color_time_stamp
        color_sec = color_time / MICROS_PER_SEC
        color_microsec = color_time % MICROS_PER_SEC
        color_date = datetime.datetime.fromtimestamp(color_sec)

        depth_time = frame.depth_time_stamp
        depth_sec = depth_time / MICROS_PER_SEC
        depth_microsec = depth_time % MICROS_PER_SEC
        depth_date = datetime.datetime.fromtimestamp(depth_sec)

        self.assertLess(abs(color_time - depth_time), 15000)

        #print str(color_date) + ", " + str(depth_date)

class TestBuffers(unittest.TestCase):

    def setUp(self):
        self.loader = holo3d.file_loader(FILE_NAME)
        self.loader.run() # This is very annoying to have to remember to do... I have to change the API
        
        self.frame = holo3d.camera_frame()

        self.loader.next_frame(self.frame)

    def tearDown(self):
        del self.frame
        del self.loader

    def testDims(self):
        self.assertEquals(self.frame.width, self.loader.width())
        self.assertEquals(self.frame.height, self.loader.height())

    def testBuffer(self):
        color_buffer = self.frame.color_buffer    
        # print type(color_buffer)
        self.assert_(isinstance(color_buffer, np.ndarray))
        self.assertEquals(len(color_buffer), self.frame.color_buffer_size)

        depth_buffer = self.frame.depth_buffer       
        self.assert_(isinstance(depth_buffer, np.ndarray))
        self.assertEquals(len(depth_buffer), self.frame.depth_buffer_size)

    def testBufferChangeFail(self):
        color_buffer = self.frame.color_buffer  
        
        self.assertGreater(len(color_buffer), 0)
        
        with self.assertRaises(ValueError):
            color_buffer[0] = 123
        
class TestBuffers(unittest.TestCase):
    
    def setUp(self):
        self.loader = holo3d.file_loader(FILE_NAME)
        self.loader.run() # This is very annoying to have to remember to do... I have to change the API
        
        self.frame = holo3d.camera_frame()

        self.loader.next_frame(self.frame)
        
        self.decoder_fac = holo3d.factory_decoder(self.loader.width, self.loader.height, self.loader.width)
        
        self.color_decoder = self.decoder_fac.color(self.loader.file_header.color_format)
        
        self.depth_decoder = self.decoder_fac.depth(self.loader.file_header.depth_format)
        
    def tearDown(self):
        del self.color_decoder
        del self.depth_decoder
        del self.decoder_fac
        del self.frame
        del self.loader
        
    def testFactory(self):
        self.assert_(self.color_decoder)
        self.assert_(self.depth_decoder)
        
    def testColorDecoder(self):
        num_pixels = self.frame.width * self.frame.height
        
        color_input = self.frame.color_buffer 
        color_output = np.ndarray(shape=(num_pixels, 3), dtype=np.uint8)
        self.color_decoder.decode(color_output, color_input, num_pixels * 3, self.frame.color_buffer_size)
                      
        depth_input = self.frame.depth_buffer 
        depth_output = np.ndarray(shape=(num_pixels, 1), dtype=np.uint16)
        self.depth_decoder.decode_depth(depth_output, depth_input, num_pixels * 2, self.frame.depth_buffer_size)
        
    def testDepthDecoder(self):
        num_pixels = self.frame.width * self.frame.height
        
        depth_input = self.frame.depth_buffer 
        depth_output = np.ndarray(shape=(num_pixels, 3), dtype=np.uint8)
        self.depth_decoder.decode(depth_output, depth_input, num_pixels * 3, self.frame.depth_buffer_size)

class TestLoading(unittest.TestCase):
    
    def testReloading(self):
        loader = holo3d.file_loader(FILE_NAME)
        # Load once
        loader.run()
        
        # Load a second time
        loader.run()

    def testReloading2(self):
        loader = holo3d.file_loader(FILE_NAME)
        # Load once
        loader.run()
        file_header = loader.file_header
        frame = holo3d.camera_frame()
        loader.next_frame(frame)        
        
        frame_header = frame.frame_header        
        
        # Load a second time
        loader.run()
        file_header2 = loader.file_header
        loader.next_frame(frame)    
        
        frame_header2 = frame.frame_header            
        
        self.assertEquals(file_header.width, file_header2.width)
        self.assertEquals(file_header.height, file_header2.height)
        self.assertEquals(file_header.color_format, file_header2.color_format) 
        self.assertEquals(file_header.depth_format, file_header2.depth_format) 
        
        self.assertEquals(frame_header.frame_number, frame_header2.frame_number)
        self.assertEquals(frame_header.color_time_stamp, frame_header2.color_time_stamp)
        self.assertEquals(frame_header.depth_time_stamp, frame_header2.depth_time_stamp)
        self.assertEquals(frame_header.color_frame_size, frame_header2.color_frame_size)
        self.assertEquals(frame_header.depth_frame_size, frame_header2.depth_frame_size)
                
if __name__ == "__main__":
    unittest.main()