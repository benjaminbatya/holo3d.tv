#ifndef __OPENNI_FACTORY_HPP__
#define __OPENNI_FACTORY_HPP__

#include "openni_device.hpp"

#include "factory_base.hpp"

#include <map>

namespace holo3d
{

// This initializes the OpenNI framework and creates devices.
// Should probably be wrapped in a unique_ptr<> and passed around
class openni_factory : public factory_base
{
public:
    openni_factory();
    ~openni_factory();

    void set_enable_ir(bool f) { m_enable_ir = f; }

    factory_base::uri_vec_t device_uris() override;

    factory_base::device_ptr_t device(int designed_fps, int desired_width, std::string uri = "") override;

    bool initialized() { return m_initialized; } 

    STREAM_FORMAT color_format() const override { return STREAM_FORMAT::COLOR_RGB888; }
    STREAM_FORMAT depth_format() const override { return STREAM_FORMAT::DEPTH_RAW; }

    int default_fps() const override { return 30; }
    int default_width() const override { return 640; }

protected:

    bool m_initialized = false;
    bool m_enable_ir = false;

    typedef std::shared_ptr<openni_device> oni_dev_ptr_t;

    typedef std::map<std::string, oni_dev_ptr_t> device_map_t;
    device_map_t    m_devices;
};

}; // namespace holo3d


#endif // __OPENNI_FACTORY_HPP__
