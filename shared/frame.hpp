#ifndef _HOLO3D_FRAME_HPP_
#define _HOLO3D_FRAME_HPP_

#include <cstdint>
#include <memory>

#include "h3t_file.hpp"

namespace holo3d
{

class camera_frame
{
public:

    camera_frame() { reset(); }

    camera_frame(const camera_frame& that)
    {
        this->operator =(that);
    }

    camera_frame& operator=(const camera_frame& that);
    
    const STREAM_HEADER& file_header() const { return m_file_header; }
    void file_header(const STREAM_HEADER& file_header) { m_file_header = file_header; }

    const FRAME_HEADER& frame_header() const { return m_header; }
    void frame_header(const FRAME_HEADER& header) { m_header = header; }

    uint16_t    width() const { return m_header.rect.width; }
    uint16_t    height() const { return m_header.rect.height; }

    uint64_t    frame_number() const { return m_header.frame_number; }
    void        frame_number(uint64_t fn) { m_header.frame_number = fn; }

    uint64_t    color_time_stamp() const { return m_header.color_time_stamp; }
    void        color_time_stamp(uint64_t ts) { m_header.color_time_stamp = ts; }

    uint64_t    depth_time_stamp() const { return m_header.depth_time_stamp; }
    void        depth_time_stamp(uint64_t ts) { m_header.depth_time_stamp = ts; }

    uint32_t    color_buffer_size() const { return m_header.color_buffer_size; }
    void        color_buffer_size(uint32_t size) { m_header.color_buffer_size = size; }

    uint32_t    depth_buffer_size() const { return m_header.depth_buffer_size; }
    void        depth_buffer_size(uint32_t size) { m_header.depth_buffer_size = size; }

    size_t      max_color_buffer_size() const { return m_file_header.width * m_file_header.height * sizeof(holo3d::RGB888Pixel); }
    size_t      max_depth_buffer_size() const { return m_file_header.width * m_file_header.height * sizeof(holo3d::RGB888Pixel); }

    void reset();

    uint8_t* color_buffer();
    uint8_t* depth_buffer();
            
    bool valid();
                 
protected:

    STREAM_HEADER    m_file_header;
    
    FRAME_HEADER    m_header;

    typedef std::shared_ptr<uint8_t> image_buffer_t;

    image_buffer_t  m_color_buffer = nullptr;   // << The color frame data, note this need to be allocated using a fast pool like in the camera
    image_buffer_t  m_depth_buffer = nullptr;   // << The depth frame data
};

}; // namespace holo3d

#endif // _HOLO3D_FRAME_HPP_
