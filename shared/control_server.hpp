#ifndef __COMM_CHANNEL_SERVER_HPP__
#define __COMM_CHANNEL_SERVER_HPP__

#include "control_protocol.hpp"
#include "fragmenting_socket.hpp"

#include <thread>

namespace holo3d
{

class ControlServer
{
public:
    ControlServer();
    ~ControlServer();

    bool query(const std::string& host);
    bool start(const std::string& host, const protocol::StartData& data);
    bool end(const std::string& host);

protected:

    typedef std::function<void(void)> message_func_t;
    bool handle_message(const std::string& host, message_func_t);

    void connect(const std::string& host);
    void close();

    boost::asio::io_service m_service;
    boost::asio::io_service::work m_work;

    fragmenting_socket  m_socket;

    std::thread         m_thread;
    bool                m_got_response = false;
};
    

}; // namespace holo3d


#endif // __COMM_CHANNEL_SERVER_HPP__
