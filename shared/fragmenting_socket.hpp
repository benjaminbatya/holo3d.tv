#ifndef _FRAGMENTING_SOCKET_HPP_
#define _FRAGMENTING_SOCKET_HPP_

namespace holo3d
{

// forward decl
struct fragmenting_socket_impl;

/**
 * Each packet sent and received by fragmenting_socket is broken up into CHUNK_SIZE byte chunks and each chunk has a 8 byte header prepended into it. 
 * The header consists of: 
 * uint16_t packet_num 
 * uint16_t chunk_index within the packet 
 * uint16_t chunk_count of all chunks within the packet 
 * uint16_t chunk_size excluding the header 
 * 
 * @author benjamin (12/5/2014)
 */
class fragmenting_socket
{
public:

    typedef boost::asio::ip::udp::endpoint endpoint_t;

    typedef std::function<void (const boost::system::error_code&, size_t)> write_handler_t;

    explicit fragmenting_socket(boost::asio::io_service& io_service, uint32_t host, uint16_t port);
    ~fragmenting_socket();

    boost::system::error_code open();
    boost::system::error_code cancel();
    boost::system::error_code close();
    boost::system::error_code connect();
    boost::system::error_code bind();

    endpoint_t local_endpoint() const;
    
    // Remote endpoint for the socket
    endpoint_t& endpoint();
    const endpoint_t& endpoint() const;
    
    /**
     * Syncronously send a buffer  
     * 
     * @author benjamin (12/23/2014)
     * 
     * @param buffer The buffer to send
     * @param buffer_size The size of the buffer (in bytes) to send
     * @param ec returns 0 for no error, not 0 otherwise
     * 
     * @return std::size_t The number of bytes sent
     */
    std::size_t send(const void* buffer, size_t buffer_size, boost::system::error_code& ec);

    /**
     * Asyncronously send a buffer
     * 
     * @author benjamin (12/23/2014)
     * 
     * @param buffer The buffer to send
     * @param buffer_size the number of bytes in the buffer
     * @param handler The callback handler to call when the buffer is done being sent
     */
    void async_send(const void* buffer, size_t buffer_size, write_handler_t handler);

    /**
     * Syncronously receive a buffer
     * 
     * @author benjamin (12/23/2014)
     * 
     * @param buffer The receiving buffer to read into
     * @param buffer_size the size of the receiving buffer
     * @param ec returns 0 for no error, out_of_order if the complete buffer could not be received do to an out-of-order chunk
     * 
     * @return std::size_t The number of bytes received
     */
    std::size_t receive(void* buffer, size_t buffer_size, boost::system::error_code& ec);

    /**
     * Asyncronously receive a buffer
     * 
     * @author benjamin (12/23/2014)
     * 
     * @param buffer The buffer to receive
     * @param buffer_size The size of the buffer in bytes
     * @param handler The callback handler to call when the receive is complete
     */
    void async_receive(void* buffer, size_t buffer_size, write_handler_t handler);

    void fragment_size(size_t fragment_size);
    size_t fragment_size(void) const;

    bool busy() const;

    void do_yield(bool flag = false);

    struct chunk_t
    {
        uint16_t packet_id  = 0;        // << Indicates the packet id for this socket. Allows ordering to be done correctly
        uint16_t chunk_count= 0;        // << The total number of chunks in this packet. Allows the socket to reassemble the original buffer once all of the chunks of a single packet are recieved
        uint16_t chunk_index= 0;        // << The chunk's index 
        uint16_t chunk_size = 0;        // << The size of the chunk not including the header.
    };

    // Play with this to eliminate fragmentation on the IP layer, maybe set it to only 2^10 to fit into a normal IP frame
    static const size_t DEFAULT_FRAGMENT_SIZE = 1 << 15; 

    void set_sidecar_buf_size(size_t size);
    void* sidecar_buf();                // NOTE: DO NOT delete the sidecar pointer!!
    const void* sidecar_buf() const;    // This doesn't create the sidecar_buf so it might      
    size_t sidecar_buf_size() const { return m_sidecar_buffer_size; }

protected:

    typedef boost::asio::ip::udp::socket socket_t;
    socket_t                            m_socket;
    fragmenting_socket::endpoint_t      m_endpoint;

    size_t                              m_packets_sent = 0;

    fragmenting_socket::write_handler_t m_handler = nullptr;
    size_t                              m_bytes_transferred;
    size_t                              m_max_bytes;
    uint8_t*                            m_local_buffer = nullptr;
    fragmenting_socket::chunk_t         m_curr_header;
    fragmenting_socket::chunk_t         m_prev_header;

    fragmenting_socket::chunk_t         m_saved_header;
    uint8_t*                            m_saved_buffer = nullptr;

    size_t                              m_fragment_size = fragmenting_socket::DEFAULT_FRAGMENT_SIZE;     // << Value set from the application if a different size is desired

    char*                               m_sidecar_buffer = nullptr;     // << a sidecar buffer used by static methods inside lambdas. This memory will not go out of scope as long as the socket is in scope
    size_t                              m_sidecar_buffer_size =  0;     // << The size of the sidecar buffer

    bool                                m_do_yield = false;

    void                                async_send_next_chunk();
    void                                async_receive_next_chunk();
    bool                                check_saved_chunk();
    bool                                handle_received_chunk(boost::system::error_code& ec, size_t bytes_recvd);

    void                                reset();

    void                                async_handler(const boost::system::error_code& ec);

};

std::ostream& operator<<(std::ostream& os, const fragmenting_socket::chunk_t& header);

}; // namespace holo3d


#endif // _FRAGMENTING_SOCKET_HPP_
