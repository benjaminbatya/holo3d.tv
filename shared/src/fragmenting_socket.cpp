#include "pch_files.hpp"

#include <algorithm>
#include <thread>
#include <sstream>
#include <list>

#include "util_funcs.hpp"

#include "fragmenting_socket.hpp"

namespace holo3d
{

fragmenting_socket::fragmenting_socket(boost::asio::io_service& service, uint32_t host, uint16_t port)
: m_socket(service)
{
    m_endpoint.address(boost::asio::ip::address_v4(host));
    m_endpoint.port(port);

    // INFO("endpoint=%1%", m_endpoint);
}

fragmenting_socket::~fragmenting_socket()
{
    m_socket.close();

    delete [] m_sidecar_buffer;
}

boost::system::error_code fragmenting_socket::open() 
{ 
    boost::system::error_code error;
    m_socket.open(boost::asio::ip::udp::v4(), error);

    // Always call reset when opening the socket to ensure that it is in a good state
    reset();

    return error;
}

boost::system::error_code fragmenting_socket::cancel()
{
    boost::system::error_code error;
    m_socket.cancel(error);
    return error;
}

boost::system::error_code fragmenting_socket::close() 
{ 
    boost::system::error_code error;
    // m_socket.shutdown(boost::asio::socket_base::shutdown_both, error);
    // NOTE: Ignore the error for now
    // ASSERT_THROW_ALWAYS(!error, error.message());
    m_socket.close(error);
    return error;
}

boost::system::error_code fragmenting_socket::connect() 
{ 
    boost::system::error_code error;
    m_socket.connect(m_endpoint, error); 
    return error;
}

boost::system::error_code fragmenting_socket::bind()
{
    boost::system::error_code error;
    m_socket.open(boost::asio::ip::udp::v4(), error);
    if (error) { return error; }
    m_socket.bind(m_endpoint, error); 

    return error;
}

fragmenting_socket::endpoint_t fragmenting_socket::local_endpoint() const 
{ 
    return m_socket.local_endpoint(); 
}

fragmenting_socket::endpoint_t& fragmenting_socket::endpoint() 
{ 
    return m_endpoint;
}

const fragmenting_socket::endpoint_t& fragmenting_socket::endpoint() const
{
    return m_endpoint;
}

void fragmenting_socket::fragment_size(size_t fragment_size) 
{ 
    m_fragment_size = fragment_size; 
}

size_t fragmenting_socket::fragment_size(void) const 
{ 
    return m_fragment_size - sizeof(fragmenting_socket::chunk_t); 
}

void fragmenting_socket::do_yield(bool flag) 
{ 
    m_do_yield = flag; 
}

void fragmenting_socket::set_sidecar_buf_size(size_t size)
{
    if (size == m_sidecar_buffer_size) { return; }

    delete [] m_sidecar_buffer;
    m_sidecar_buffer = nullptr;

    m_sidecar_buffer_size = size;
}
void* fragmenting_socket::sidecar_buf()
{
    if (m_sidecar_buffer == nullptr) 
    {
        ASSERT(m_sidecar_buffer_size > 0); 
        m_sidecar_buffer = new char[m_sidecar_buffer_size]; 
    }

    return m_sidecar_buffer;
}

const void* fragmenting_socket::sidecar_buf() const
{
    return m_sidecar_buffer;
}

void fragmenting_socket::reset()
{
    m_local_buffer = nullptr;
    m_handler = nullptr;
    m_max_bytes = 0;
    m_bytes_transferred = 0;

    memset(&m_prev_header, 0, sizeof(fragmenting_socket::chunk_t));
    memset(&m_curr_header, 0, sizeof(fragmenting_socket::chunk_t));
}

bool fragmenting_socket::busy() const 
{   
    return(m_local_buffer != nullptr);
}

void fragmenting_socket::async_handler(const boost::system::error_code& ec)
{
    fragmenting_socket::write_handler_t handler = m_handler;
    size_t total_bytes = m_bytes_transferred;

    // INFO("Clearing all info for packet = %d", m_curr_header.packet_id);
    reset();

    // Call the handler
    handler(ec, total_bytes);
}

std::size_t fragmenting_socket::send(const void* buf, size_t buffer_size, boost::system::error_code& ec)
{
    ASSERT(!busy());

    m_local_buffer = (uint8_t*)buf;
    m_max_bytes = buffer_size;
    m_bytes_transferred = 0;

    const size_t packet_id = m_packets_sent++;

    // Calculate the number of chunks to send
    const size_t num_chunks = m_max_bytes / fragment_size() + (m_max_bytes % fragment_size() ? 1 : 0);

    size_t chunk_index = 0;
    
    while (m_max_bytes > 0)
    {
        size_t write_size = std::min(m_max_bytes, fragment_size());
    
        m_curr_header.chunk_count = num_chunks;
        m_curr_header.packet_id = packet_id;
        m_curr_header.chunk_size = write_size;
        m_curr_header.chunk_index = chunk_index++;

//      INFO("Sending buffer");
        boost::array<boost::asio::const_buffer, 2> bufs = {
            boost::asio::buffer(&m_curr_header, sizeof(fragmenting_socket::chunk_t)),
            boost::asio::buffer(m_local_buffer, m_curr_header.chunk_size)
        };

        size_t bytes_sent = m_socket.send_to(bufs, m_endpoint, 0, ec);
        ASSERT_THROW(!ec, ec.message());
        ASSERT_EQUAL(bytes_sent, sizeof(fragmenting_socket::chunk_t) + m_curr_header.chunk_size);
        
        if (m_do_yield)
        {
            // Allows for a context switch
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        m_max_bytes -= m_curr_header.chunk_size;
        m_local_buffer += m_curr_header.chunk_size;
        m_bytes_transferred += m_curr_header.chunk_size;
    }

    size_t total_sent = m_bytes_transferred;

    reset();

    return total_sent;
}

void fragmenting_socket::async_send(const void* buf, size_t buffer_size, fragmenting_socket::write_handler_t handler)
{
    ASSERT(!busy());

    m_local_buffer = (uint8_t*)buf;
    m_max_bytes = buffer_size;
    m_bytes_transferred = 0;
    m_handler = handler;

    m_curr_header.packet_id = m_packets_sent++;
    m_curr_header.chunk_count = m_max_bytes / fragment_size() + (m_max_bytes % fragment_size() ? 1 : 0);
    m_curr_header.chunk_index = 0;

    async_send_next_chunk();
}

void fragmenting_socket::async_send_next_chunk()
{
    size_t write_size = std::min(m_max_bytes, fragment_size());
    
    m_curr_header.chunk_size = write_size;

//      INFO("Sending buffer");
    boost::array<boost::asio::const_buffer, 2> bufs = {
        boost::asio::buffer(&m_curr_header, sizeof(fragmenting_socket::chunk_t)),
        boost::asio::buffer(m_local_buffer, m_curr_header.chunk_size)
    };

    m_socket.async_send_to(bufs, m_endpoint, [&] (const boost::system::error_code& ec, size_t bytes_sent)
    {
        if (ec == boost::asio::error::operation_aborted) 
        {
            INFO("Send cancelled");
            return;
        }

        ASSERT_THROW(!ec, ec.message());
        ASSERT_EQUAL(bytes_sent, sizeof(fragmenting_socket::chunk_t) + m_curr_header.chunk_size);
        
        if (m_do_yield)
        {
            // Allows for a context switch
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        m_max_bytes -= m_curr_header.chunk_size;
        m_local_buffer += m_curr_header.chunk_size;
        m_bytes_transferred += m_curr_header.chunk_size;

        if (m_max_bytes == 0)
        {
            async_handler(ec);
        } else
        {
            m_curr_header.chunk_index++;
            async_send_next_chunk();
        }
    });
}

bool fragmenting_socket::check_saved_chunk()
{
    // Check if we have a saved header from the previous call to receive
    if (m_saved_header.chunk_count > 0)
    {
        // We have a saved chunk
        m_curr_header = m_saved_header;
        memset(&m_saved_header, 0, sizeof(fragmenting_socket::chunk_t));

        // Update the pointers and sizes
        memcpy(m_local_buffer, m_saved_buffer, m_curr_header.chunk_size);

        delete [] m_saved_buffer;
        m_saved_buffer = nullptr;

        m_local_buffer += m_curr_header.chunk_size;
        m_max_bytes -= m_curr_header.chunk_size;
        m_bytes_transferred += m_curr_header.chunk_size;

        // Copy over the previous header
        m_prev_header = m_curr_header;

        if (m_curr_header.chunk_index+1 == m_curr_header.chunk_count)
        {
            // INFO("processed packet->packet_id = %1%", m_packet_id);
            return true;
        }        
    }

    return false;
}

bool fragmenting_socket::handle_received_chunk(boost::system::error_code& ec, size_t recvd_bytes)
{
    ASSERT_THROW(!ec, ec.message());

    // We always should be receiving buffers bigger then sizeof(chunk_t)
    ASSERT(recvd_bytes > sizeof(fragmenting_socket::chunk_t));

    // Make sure that we got the right number of bytes
    ASSERT_EQUAL(recvd_bytes, sizeof(fragmenting_socket::chunk_t) + m_curr_header.chunk_size);

    if (m_prev_header.chunk_count == 0)
    {
        m_prev_header = m_curr_header;

        m_local_buffer += m_curr_header.chunk_size;
        m_max_bytes -= m_curr_header.chunk_size;
        m_bytes_transferred += m_curr_header.chunk_size;

    } else if(m_prev_header.packet_id > m_curr_header.packet_id)
    {
        ec = boost::asio::error::operation_not_supported;
    } else if(m_prev_header.packet_id == m_curr_header.packet_id)
    {
        if(m_prev_header.chunk_index >= m_curr_header.chunk_index)
        {
            ec = boost::asio::error::operation_not_supported;
        } else if (m_prev_header.chunk_index + 1 == m_curr_header.chunk_index)
        {
            m_prev_header = m_curr_header;

            m_local_buffer += m_curr_header.chunk_size;
            m_max_bytes -= m_curr_header.chunk_size;
            m_bytes_transferred += m_curr_header.chunk_size;
        } else
        {
            ec = boost::asio::error::timed_out;
        }
    } else
    {
        m_saved_header = m_curr_header;
        m_saved_buffer = new uint8_t[fragment_size()];
        memcpy(m_saved_buffer, m_local_buffer, m_saved_header.chunk_size);

        ec = boost::asio::error::timed_out;
    }

    if (ec)
    {
        return true;
    } else if (m_curr_header.chunk_index + 1 == m_curr_header.chunk_count) 
    {
        // INFO("processed packet->packet_id = %1%", m_packet_id);
        return true;
    } 

    return false;
}

std::size_t fragmenting_socket::receive(void* buf, size_t buffer_size, boost::system::error_code& ec)
{
    ASSERT(!busy());

    m_local_buffer = (uint8_t*)buf;
    m_max_bytes = buffer_size;
    m_bytes_transferred = 0;

    bool done = check_saved_chunk();

    while (!done) // Keep receiving until we fill up the buffer or break
    {
//      INFO("receiving chunk header");
        boost::array<boost::asio::mutable_buffer, 2> bufs = {
            boost::asio::buffer(&m_curr_header, sizeof(fragmenting_socket::chunk_t)),
            boost::asio::buffer(m_local_buffer, m_max_bytes)
        };
            
        size_t recvd_bytes = m_socket.receive_from(bufs, m_endpoint, 0, ec);

        done = handle_received_chunk(ec, recvd_bytes);
    }

    size_t total_received = m_bytes_transferred;

    reset();

    return total_received;
}

void fragmenting_socket::async_receive(void* buf, size_t buffer_size, write_handler_t handler)
{
    ASSERT(!busy());

    m_local_buffer = (uint8_t*)buf;
    m_handler = handler;
    m_max_bytes = buffer_size;
    m_bytes_transferred = 0;

    bool done = check_saved_chunk();

    if (done)
    {
        async_handler(boost::system::error_code());
    } else
    {
        async_receive_next_chunk();
    }
}

void fragmenting_socket::async_receive_next_chunk()
{
    boost::array<boost::asio::mutable_buffer, 2> bufs = {
        boost::asio::buffer(&m_curr_header, sizeof(fragmenting_socket::chunk_t)),
        boost::asio::buffer(m_local_buffer, m_max_bytes)
    };

    // Asyncronously get the header
    m_socket.async_receive_from(bufs, m_endpoint, [&] (const boost::system::error_code& ec, size_t bytes_recvd)
    {
        if (ec == boost::asio::error::operation_aborted) 
        {
            WARN("operation aborted");
            return;
        }

        boost::system::error_code err = ec;
        bool done = handle_received_chunk(err, bytes_recvd);
        if (done)
        {
            async_handler(err);
        } else
        {
            async_receive_next_chunk();
        }
    });
}

std::ostream& operator<<(std::ostream& os, const fragmenting_socket::chunk_t& header)
{
    os  << "{ packet_id = " << header.packet_id
        << ", chunk_count = " << header.chunk_count
        << ", chunk_index = " << header.chunk_index
        << ", chunk_size = " << header.chunk_size
        << "}";

    return os;
}

}; // namespace holo3d
