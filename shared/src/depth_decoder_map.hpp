#ifndef __DEPTH_DECODER_MAP_HPP__
#define __DEPTH_DECODER_MAP_HPP__

#include <cstdint>

namespace holo3d
{

extern const uint32_t SIZE_DEPTH_DECODE_MAP;

extern const int16_t DEPTH_DECODE_G[];

extern const int16_t DEPTH_DECODE_V[];

}; // namespace holo3d


#endif // __DEPTH_DECODER_MAP_HPP__
