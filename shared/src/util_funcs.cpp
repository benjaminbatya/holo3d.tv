#include "pch_files.hpp"

#include "util_funcs.hpp"

static VERBOSE_LEVEL g_verbose_level = VERBOSE_LEVEL::INFO;

void set_verbose_level(VERBOSE_LEVEL new_level)
{
    g_verbose_level = new_level;
}
VERBOSE_LEVEL verbose_level()
{
    return g_verbose_level;
}


