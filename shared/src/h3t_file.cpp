#include "pch_files.hpp"

#include "util_funcs.hpp"

#include "h3t_file.hpp"

namespace holo3d
{

// This is the definition of EMPTY_RECT in h3t_file.hpp
const RECT RECT::EMPTY = {0, 0, 0, 0};

// This is the definition of  get_pixel_depth in h3t_file.hpp
// Returns the size of the pixel given the stream format
size_t get_pixel_depth(const STREAM_FORMAT& fmt)
{
    switch (fmt) 
    {
    case STREAM_FORMAT::COLOR_RGB888:
        return sizeof(RGB888Pixel);
    case STREAM_FORMAT::COLOR_YUV422:
        return sizeof(YUV422DoublePixel) / 2;
    case STREAM_FORMAT::COLOR_YUYV:    
        return sizeof(YUYVDoublePixel) / 2;
    case STREAM_FORMAT::DEPTH_RAW:     
        return sizeof(DepthPixel);
    default:
        THROW("Invalid stream format: %s", fmt);
        return 0;
    };
}

// A couple useful stream operators
std::ostream& operator<<(std::ostream& os, const STREAM_FORMAT& pf)
{
#define OUTPUT_STREAM_FORMAT(e) case STREAM_FORMAT::e : os << #e; break;

    switch (pf)
    {
    OUTPUT_STREAM_FORMAT(COLOR_YUYV);
    OUTPUT_STREAM_FORMAT(COLOR_YUV422);
    OUTPUT_STREAM_FORMAT(COLOR_RGB888);
    OUTPUT_STREAM_FORMAT(COLOR_VP8);
//  OUTPUT_STREAM_FORMAT(COLOR_H264);
    OUTPUT_STREAM_FORMAT(COLOR_YV12);
    OUTPUT_STREAM_FORMAT(COLOR_JPEG);

    OUTPUT_STREAM_FORMAT(DEPTH_RAW);
    OUTPUT_STREAM_FORMAT(DEPTH_ENC);
    OUTPUT_STREAM_FORMAT(DEPTH_JPEG);
    OUTPUT_STREAM_FORMAT(DEPTH_PNG);
    OUTPUT_STREAM_FORMAT(DEPTH_VP8);
//  OUTPUT_STREAM_FORMAT(DEPTH_H264);
//  OUTPUT_STREAM_FORMAT(DEPTH_VY12);

    OUTPUT_STREAM_FORMAT(INVALID);
    default: os << "Unknown stream format"; break;
    }
#undef OUTPUT_STREAM_FORMAT

    return os;
}

std::ostream& operator<<(std::ostream& os, const NEW_CAMERA_REQUEST& c)
{
    os  << "ip_address=0x" << std::ios::hex << c.ip_address;

    return os;
}

std::ostream& operator<<(std::ostream& os, const NEW_CAMERA_REPLY& c)
{
    os  << "assigned_port=" << std::hex << c.assigned_port 
        << ", server_time=" << std::hex << c.start_time;

    return os;
}

std::ostream& operator<<(std::ostream& os, const STREAM_HEADER& header)
{
    os  << "{ version=" << (size_t)header.major_version << "." << (size_t)header.minor_version
        << ", color_format=" << header.color_format << ", depth_format=" << header.depth_format
        << ", width=" << header.width << ", height=" << header.height
        << ", camera_id=" << header.camera_id << ", tracker_id=" << header.tracker_id
        << "}"; 

    return os;
}

std::ostream& operator<<(std::ostream& os, const RECT& rect)
{
    os  << "{ x=" << rect.x << ", y=" << rect.y 
        << ", width=" << rect.width << ", height=" << rect.height 
        << "}";

    return os;
}

std::ostream& operator<<(std::ostream& os, const FRAME_HEADER& header)
{
    os  << "{ frame_number=" << header.frame_number 
        << ", color_time_stamp=" << header.color_time_stamp << ", depth_time_stamp=" << header.depth_time_stamp
        << ", rect=" << header.rect
        << ", color_buffer_size=" << header.color_buffer_size << ", depth_buffer_size=" << header.depth_buffer_size 
        << "}";
    
    return os; 
}

std::ostream& operator<<(std::ostream& os, const QUALITY& quality)
{
#define OUTPUT(e) case QUALITY::e : os << #e; break;

    switch (quality)
    {
    OUTPUT(BEST);
    OUTPUT(REALTIME);
    OUTPUT(AVERAGE);
    OUTPUT(GOOD);
    default: os << "Unknown quality " << (int)quality; break;
    }
#undef OUTPUT

    return os;
}

std::istream& operator>>(std::istream& is, QUALITY& quality)
{
    std::string input;
    is >> input;

#define INPUT(m) if (input == #m) { quality = QUALITY::m; }

    INPUT(BEST)
    else INPUT(REALTIME)
    else INPUT(AVERAGE)
    else INPUT(GOOD)
    else
    {
        std::string str = "Unknown quality '" + input + "'";
        throw std::runtime_error(str);
    }
#undef INPUT

    return is;
}


std::ostream& operator<<(std::ostream& os, const RESOLUTION& mode)
{
#define OUTPUT_MODE(e) case RESOLUTION::e : os << #e; break;

    switch (mode)
    {
    OUTPUT_MODE(SMALL)
    OUTPUT_MODE(MEDUIM)
    // OUTPUT_MODE(LARGE)
    default: os << "Unknown resolution " << (int)mode; break;
    }
#undef OUTPUT_MODE

    return os;
}

std::istream& operator>>(std::istream& is, RESOLUTION& mode)
{
    std::string input;
    is >> input;

#define INPUT_MODE(m) if (input == #m) { mode = RESOLUTION::m; }

    INPUT_MODE(SMALL)
    else INPUT_MODE(MEDUIM)
    // else INPUT_MODE(LARGE)
    else 
    {
        std::string str = "Unknown resolution '" + input + "'";
        throw std::runtime_error(str);
    }

#undef INPUT_MODE

    return is;
}

}; // namespace holo3d
