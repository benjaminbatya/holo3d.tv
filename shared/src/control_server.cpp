#include "pch_files.hpp"

#include "control_protocol.hpp"

#include "control_server.hpp"

#include "util_funcs.hpp"


using boost::asio::ip::udp;

namespace holo3d
{

ControlServer::ControlServer()
: m_service()
, m_work(m_service)
, m_socket( m_service, 0, 0 )
, m_thread([&] { m_service.run(); })
{
    m_socket.set_sidecar_buf_size(1024);

//  boost::system::error_code ec;
//  m_service.reset();
}

ControlServer::~ControlServer()
{
    close();

    m_service.stop();
    m_thread.join();
}

void ControlServer::connect(const std::string& host)
{
    boost::system::error_code ec;
    udp::resolver resolver(m_service);
    udp::resolver::query query(udp::v4(), host.c_str(), protocol::DEFAULT_PORT);
    m_socket.endpoint() = *resolver.resolve(query, ec);
    ASSERT_ALWAYS(!ec);

//  INFO("opening sending socket");
    ec = m_socket.open();
    ASSERT_THROW_ALWAYS(!ec, ec.message());
}

void ControlServer::close()
{
    boost::system::error_code ec;
    ec = m_socket.close();
    ASSERT_THROW_ALWAYS(!ec, ec.message());
}

bool ControlServer::handle_message(const std::string& host, message_func_t fn)
{
    connect(host);

    m_got_response = false;

    fn();

    for (int i=0; i<10; i++) 
    {
        boost::this_thread::sleep(boost::posix_time::milliseconds(10)); 
        if (m_got_response) 
        {
            break;
        }
    }

    close();

    return m_got_response;
}


bool ControlServer::query(const std::string& host)
{  
    return handle_message(host, [&]
    {
        // Send the query message
        protocol::Send::async_basic(m_socket, protocol::ID::QUERY, [&]
        {
            // Get the response back
            protocol::Receive::async_basic(m_socket, protocol::ID::QUERY_ACK, [&]
            {
                m_got_response = true;
            });
        });
    });
}


bool ControlServer::start(const std::string& host, const protocol::StartData& data)
{
    return handle_message(host, [&]
    {
        // Send the query message
        protocol::Send::async_start(m_socket, data, [&]
        {
            // Get the response back
            protocol::Receive::async_basic(m_socket, protocol::ID::START_ACK, [&]
            {
                m_got_response = true;
            });
        });
    });
}



bool ControlServer::end(const std::string& host)
{
    return handle_message(host, [&]
    {
        // Send the query message
        protocol::Send::async_basic(m_socket, protocol::ID::END, [&]
        {
            // Get the response back
            protocol::Receive::async_basic(m_socket, protocol::ID::END_ACK, [&]
            {
                m_got_response = true;
            });
        });
    });
}



}; // namespace holo3d
