#include "pch_files.hpp"

#include <cstring>
#include <cassert>
#include <stdexcept>

#include "util_funcs.hpp"
#include "fps_counter.hpp"

#include "fragmenting_socket.hpp"
#include "camera_info.hpp"

#include "loader.hpp"

using boost::asio::ip::udp;

namespace holo3d
{

// START loader_base

loader_base::loader_base()
{
    // do nothing
}

loader_base::~loader_base()
{
    // do nothing...
}

// END loader_base

// START file_loader

file_loader::file_loader(const std::string& file_name) 
: m_file_name(file_name)
, m_stream(nullptr)
{

}

file_loader::~file_loader()
{
    shutdown();
}

void file_loader::shutdown()
{
    if (m_stream != nullptr)
    {
        fclose(m_stream);
        m_stream = nullptr;
    }
}

bool file_loader::valid() const
{
    return(m_stream != nullptr);
}

bool file_loader::run()
{
    ASSERT_ALWAYS(!m_file_name.empty());

    // distinguish between files and incoming rtp streams

    // close the file,
    int ret = 0;
    if (m_stream != nullptr)
    {
        ret = fclose(m_stream); 
        ASSERT_ALWAYS(ret == 0);
        m_stream = nullptr;
    }
    
    // then reopen the file
    // NOTE: should we check the extension??
    m_stream = fopen (m_file_name.c_str(), "rb");
    if(!m_stream) // failed to open the file
    {
        THROW("Failed to open %s. Does it exist?", m_file_name.c_str());
    }

    // First read the FILE_HEADER
    ret = fread(&m_file_header, sizeof(STREAM_HEADER), 1, m_stream);
    ASSERT_ALWAYS(ret == 1);
    
    // Check the magic
    if(memcmp(m_file_header.other, FILE_MAGIC, sizeof(FILE_MAGIC)) != 0)
    {
        shutdown();
        THROW("file %s didn't have the right magic", m_file_name.c_str());
    }

    // Check the version, we only handle version 0.2 right now
    if (!(m_file_header.major_version == MAJOR_VERSION && 
          m_file_header.minor_version == MINOR_VERSION) )
    {
        shutdown();
        THROW("only handle version %1%.%2%.\nFile %3% is version %4%.%5%", 
              MAJOR_VERSION, MINOR_VERSION,
              m_file_name.c_str(),
              (size_t)m_file_header.major_version, (size_t)m_file_header.minor_version);
    }

    // Call the load handler when the file header is loaded
    if(m_load_handler) 
    {
        m_load_handler();
    }

    // The movie is ready to be played

    return true;
}

bool file_loader::next_frame(camera_frame& frame)
{
    frame.reset();
    frame.file_header(m_file_header);

    FRAME_HEADER frame_header;

    // This needs to take the next chunk of frame data from the stream and build a frame object using it.
    // Read the FRAME_HEADER
    uint32_t ret = fread(&frame_header, sizeof(FRAME_HEADER), 1, m_stream);
    bool finished = false;
    // If we don't read the correct number of bytes, quit
    if(ret != 1)
    {
        finished = true;
    }

    // Check if the END_STREAM_MESSAGE is read
    if (frame_header.frame_number == END_STEAM_MESSAGE) 
    {
        finished = true;
    }

    if (finished) 
    {

        if (m_end_handler) 
        {
            m_end_handler();
        }

        return false;
    }
        
    frame.frame_header(frame_header);

    ret = fread(frame.color_buffer(), sizeof(uint8_t), frame.color_buffer_size(), m_stream);
    if (ret != frame.color_buffer_size())
    {
        return false;
    }
    
    ret = fread(frame.depth_buffer(), sizeof(uint8_t), frame.depth_buffer_size(), m_stream);
    if (ret != frame.depth_buffer_size())
    {
        return false;
    }

    // frame is good to go
    return true;
}

// END file_loader

// START udp_loader

class udp_loader_impl
{
public:
    udp_loader_impl(udp_loader* interface, std::string port)
    : m_interface { interface }
    , m_service()
    , m_socket(m_service, 0, atoi(port.c_str()))
    , m_work(new boost::asio::io_service::work(m_service))
    , m_thread([&] () { m_service.run(); }) // spawn a separate thread and asio service to listen for incoming udp messages
    {
    }
    ~udp_loader_impl()
    {
//      INFO("Called");
        m_service.stop();

        m_socket.close();
    }

    udp_loader*                     m_interface;        // << a pointer back to the interface
    boost::asio::io_service         m_service;          // << The service to use
    fragmenting_socket              m_socket;           // << The socket that'll receive messages from the cameras
    typedef std::unique_ptr<boost::asio::io_service::work> work_ptr_t;
    work_ptr_t                      m_work;
    boost::thread                   m_thread;           // << the thread that'll process the incoming udp messages

    std::shared_ptr<camera_info>    m_sender;           // << Receives and queues the frames from the sender

    static const size_t             MAX_BUFFER_SIZE = 0xff;
    uint8_t                         m_buffer[MAX_BUFFER_SIZE];

    void handle_camera_connect(const boost::system::error_code& error,
                               const size_t& bytes_recvd);
};

void udp_loader_impl::handle_camera_connect(const boost::system::error_code& error,
                                            const size_t& bytes_recvd)
{
    ASSERT_THROW(!error, "error = %1%", error.message());
    ASSERT_EQUAL(bytes_recvd, sizeof(NEW_CAMERA_REQUEST));

    // Get the camera request with the ip address and
    // send a reply with the number of a new port for the client to transmit to
    NEW_CAMERA_REQUEST* request = (NEW_CAMERA_REQUEST*)m_buffer;

    INFO("Got a new sender request %1% from %2%, local_endpoint = %3%",
         *request, m_socket.endpoint(), m_socket.local_endpoint());

    m_sender = std::make_shared<camera_info>(m_service, m_socket.endpoint(), m_socket.local_endpoint().port()+1);

    uint64_t start_time = get_time(); // Start the sender immediately

    // Call the load handler when the file header is loaded
    m_sender->inform_on_file_header( [&] (camera_info::ptr_t sender ) 
    {   
        std::shared_ptr<camera_info> cam = sender.lock();
        ASSERT(cam);

        m_interface->m_file_header = cam->header();
        if(m_interface->m_load_handler) 
        {
            m_interface->m_load_handler();
        }
    });

    m_sender->set_end_stream_cb( [&] (camera_info::ptr_t sender)
    {
        if (m_interface->m_end_handler) 
        {
            m_interface->m_end_handler(); 
        }
    });

    NEW_CAMERA_REPLY reply(m_sender->local_port(), start_time);

//  INFO("Sending reply message to %1% with reply=%2%", m_socket.endpoint(), reply);

    m_socket.async_send(&reply, sizeof(NEW_CAMERA_REPLY),
                        boost::bind(&camera_info::handle_camera_reply, m_sender, 
                                    boost::asio::placeholders::error,
                                    boost::asio::placeholders::bytes_transferred));
}

udp_loader::udp_loader(std::string port)
: m_impl { new udp_loader_impl(this, port) }
{
}

udp_loader::~udp_loader()
{
//  INFO("Called");
    delete m_impl;
}

bool udp_loader::valid() const
{
    return(m_impl->m_sender && m_impl->m_sender->valid());
}

bool udp_loader::run()
{
    boost::system::error_code error = m_impl->m_socket.bind();
    ASSERT_THROW(!error, error.message());

    INFO("listening to endpoint %1%", m_impl->m_socket.endpoint());

    // Listen for a camera asyncronous connect message
    m_impl->m_socket.async_receive(m_impl->m_buffer, m_impl->MAX_BUFFER_SIZE,
                           boost::bind(&udp_loader_impl::handle_camera_connect, m_impl, 
                                       boost::asio::placeholders::error,
                                       boost::asio::placeholders::bytes_transferred));
    return true;
}

std::string udp_loader::connection_info() const
{
    if (!valid()) 
    {
        return "";
    }

    // NOTE: This is slow and should not be called alot
    return (boost::format("%1%") % m_impl->m_sender->endpoint()).str();
}

bool udp_loader::next_frame(camera_frame &frame_ptr)
{
    if (!valid()) 
    {
        return false;
    }

    // return the next frame in the queue if there is one available
    bool ret = m_impl->m_sender->pop_frame(frame_ptr);
    
    return ret;
}

// END udp_loader

}; // namespace holo3d
