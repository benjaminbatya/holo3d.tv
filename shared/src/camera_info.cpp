#include "pch_files.hpp"

#include <stdint.h>
#include <memory>
#include <functional>

#include "h3t_file.hpp"
#include "util_funcs.hpp"
#include "frame.hpp"
#include "fps_counter.hpp"

#include "camera_info.hpp"

using boost::asio::ip::udp;

namespace holo3d
{

camera_info::camera_info(boost::asio::io_service& service, boost::asio::ip::udp::endpoint endpoint, uint16_t local_port)
: m_socket(service, 0, local_port)
, m_endpoint(endpoint)
, m_local_port(local_port)
{
    m_description = (boost::format("%1%->%2%") % m_endpoint % m_local_port).str();

    std::string name = "camera_info: ";
    name += m_description;

    m_fps.name(name);
}

camera_info::~camera_info()
{
//  m_socket.cancel();
    m_socket.close();
}

// NOTE: this define really cuts back on the amount of template code required
#define ASYNC_RECEIVE(buf, size, func) \
    m_socket.async_receive(buf, size, \
                           boost::bind(&camera_info::func, this, \
                                       boost::asio::placeholders::error, \
                                       boost::asio::placeholders::bytes_transferred)) 

void camera_info::handle_camera_reply(const boost::system::error_code& error,
                                      const size_t& bytes_sent)
{
    if (error == boost::asio::error::operation_aborted) 
    {
        ERROR("Cancelled");
        return;
    }

//  INFO("called");
    ASSERT_THROW(!error, error.message());
    ASSERT_EQUAL(bytes_sent, sizeof(NEW_CAMERA_REPLY));

    INFO("m_socket bound to %1%", m_socket.endpoint());

    boost::system::error_code ec = m_socket.bind();
    ASSERT_THROW(!ec, ec.message());

    ASYNC_RECEIVE(m_receive_buffer, MAX_BUFFER_SIZE, handle_new_stream);
}

/**
 * Waits for a FILE_HEADER from the camera
 * 
 * @author benjamin (11/24/2014)
 * 
 * @param ip_address of the camera that sent the packet
 * @param error 0 for no error, otherwise non-zero
 * @param bytes_recvd number of bytes received
 */
void camera_info::handle_new_stream(const boost::system::error_code& error,
                                    const size_t& bytes_recvd)
{
    if (error == boost::asio::error::operation_aborted) 
    {
        ERROR("Cancelled");
        return;
    }

//  INFO("called");
    ASSERT_THROW(!error, error.message());
    ASSERT_EQUAL(bytes_recvd, sizeof(STREAM_HEADER));

    // receive_buffer should contain all of the frame data
    STREAM_HEADER* header = (STREAM_HEADER*)m_receive_buffer;

    // Check the magic
    if(memcmp(header->other, FILE_MAGIC, sizeof(FILE_MAGIC)) != 0)
    {
        THROW("Stream from %1% doesn't have the right magic", endpoint());
    }

    // Check the version, we only handle version 0.2 right now
    if (!(header->major_version >= MAJOR_VERSION && 
          header->minor_version >= MINOR_VERSION) )
    {
        THROW("only handle version %d.%d.\nStream has version %d.%d", 
              MAJOR_VERSION, MINOR_VERSION,
              header->major_version, header->minor_version);
    }

    m_header = *header;

    m_frame.file_header(m_header);

    INFO("Receiving stream = %1%", m_header);

    if (m_inform_on_file_header_cb)
    {
        m_inform_on_file_header_cb(shared_from_this()); 
    }

    // Now wait for the FRAME_HEADER
    ASYNC_RECEIVE(m_receive_buffer, MAX_BUFFER_SIZE, handle_new_frame);
}

void camera_info::handle_new_frame(const boost::system::error_code& error,
                                   const size_t& bytes_recvd)
{
    if (error == boost::asio::error::operation_aborted) 
    {
        ERROR("Cancelled");
        return;
    }

    // INFO("called");
    if (error != boost::asio::error::timed_out)
    {
        ASSERT_THROW(!error, error.message());
        ASSERT_EQUAL(bytes_recvd, sizeof(FRAME_HEADER));

//      INFO("bytes_recvd = %1%", bytes_recvd);
        FRAME_HEADER* header = (FRAME_HEADER*)m_receive_buffer;
        
        if (header->frame_number == END_STEAM_MESSAGE) 
        {
            INFO("Got the END_STREAM_SIGNAL.");
            if (m_end_stream_cb) 
            {
                m_end_stream_cb(shared_from_this());
            }
            
        } else
        {
            m_frame.reset();
            m_frame.frame_header(*header);

//          INFO("Accepting a frame header  = %1%", m_current_frame.frame_header());

            if (m_frame.file_header().has_color()) 
            {
                ASYNC_RECEIVE(m_frame.color_buffer(), m_frame.color_buffer_size(), handle_color_buffer);
            } else if(m_frame.file_header().has_depth())
            {
                ASYNC_RECEIVE(m_frame.depth_buffer(), m_frame.depth_buffer_size(), handle_depth_buffer);
            } else
            {
                finish_handling_frame();
            }

    //      static fps_counter fps("handle_new_frame");
    //      fps.update();
        }
    } else
    {
        // wait for the next frame
        ASYNC_RECEIVE(m_receive_buffer, MAX_BUFFER_SIZE, handle_new_frame);
    }
}

void camera_info::handle_color_buffer(const boost::system::error_code& error,
                                      const size_t& bytes_recvd)
{
    if (error == boost::asio::error::operation_aborted) 
    {
        ERROR("Cancelled");
        return;
    }

    // INFO("called");
    // If the the receive timed out, then don't push the frame and go back to handling another new frame
    if (error != boost::asio::error::timed_out)
    {
        ASSERT_THROW(!error, error.message());
        ASSERT_EQUAL(bytes_recvd, m_frame.color_buffer_size());

//      INFO("bytes_recvd = %1%", bytes_recvd);

        if(m_frame.file_header().has_depth())
        {
            ASYNC_RECEIVE(m_frame.depth_buffer(), m_frame.depth_buffer_size(), handle_depth_buffer);
        } else
        {
            finish_handling_frame();
        }
    } else
    {
        // Go back to waiting for a new frame
        ASYNC_RECEIVE(m_receive_buffer, MAX_BUFFER_SIZE, handle_new_frame);
    }
}

void camera_info::handle_depth_buffer(const boost::system::error_code& error,
                                      const size_t& bytes_recvd)
{
    if (error == boost::asio::error::operation_aborted) 
    {
        ERROR("Cancelled");
        return;
    }

    // INFO("called");
    // If the the receive timed out, then don't push the frame and go back to handling another new frame
    if (error != boost::asio::error::timed_out)
    {

        ASSERT_THROW(!error, error.message());
        ASSERT_EQUAL(bytes_recvd, m_frame.depth_buffer_size());

//      INFO("bytes_recvd = %1%", bytes_recvd);
        finish_handling_frame();

        // Update the fps counter
//      m_fps.update();
    } else
    {
        // Now wait for a new frame
        ASYNC_RECEIVE(m_receive_buffer, MAX_BUFFER_SIZE, handle_new_frame);
    }
}

void camera_info::finish_handling_frame() 
{ 
    // now that the new frame is done, push it into the queue
    if (m_frame_queue.push(m_frame)) 
    {
        // INFO("pushed current_frame");
    } 
    else // Try to pop a frame from the queue and then repushing. It's better to have new frames then old ones.
    {
        ERROR("frame_queue was full! Removing one frame!"); 
        camera_frame deleted_frame; 
        m_frame_queue.pop(deleted_frame); 
        
        if (!m_frame_queue.push(m_frame)) 
        {
            THROW("failed to push the frame after popping a frame. Something failed!");
        }
    }

    // Inform our listeners that a new frame was received
    if (m_new_frame_cb) 
    {
        m_new_frame_cb(shared_from_this());
    }

    // Now wait for a new frame
    ASYNC_RECEIVE(m_receive_buffer, MAX_BUFFER_SIZE, handle_new_frame);
}

} // namespace holo3d
