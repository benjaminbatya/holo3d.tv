// #include "pch_files.hpp"

#include <string>
#include <memory>
#include <cstring>

#include "frame.hpp"

namespace holo3d
{

camera_frame& camera_frame::operator=(const camera_frame& that)
{
    m_file_header = that.m_file_header;

    m_header = that.m_header;
    
    m_color_buffer = that.m_color_buffer;
    m_depth_buffer = that.m_depth_buffer;

    return *this;
}

void camera_frame::reset()
{
    memset(&m_header, 0, sizeof(FRAME_HEADER));

    m_color_buffer = nullptr;
    m_depth_buffer = nullptr;
}

bool camera_frame::valid() 
{ 
    if (file_header().has_color() && file_header().has_depth()) 
    {
        return m_color_buffer!=nullptr && m_depth_buffer!=nullptr; 
    } else if(file_header().has_color())
    {
        return m_color_buffer != nullptr;
    } else if(file_header().has_depth())
    {
        return m_depth_buffer != nullptr;
    } else
    {
        return false;
    }
}


// NOTE: We are using the same pool to allocate both the color and depth buffers
// NOTE: boost::fast_pool_allocator IS NOT THREAD SAFE without a mutex!!! 
// ADD A MUTEX if RANDOM segfaults start occurring during allocation and deallocation
// static boost::pool<> g_buffer_pool(sizeof(RGB888Pixel));

class buffer_deleter
{
public:
void operator()(uint8_t* obj)
{
    // INFO_CPP("deleting " << obj);
    // g_buffer_pool.ordered_free(obj);
    delete [] obj;
}
};

static buffer_deleter g_buffer_deleter;

uint8_t* camera_frame::color_buffer() 
{ 
    if(!m_color_buffer) // Do lazy allocation
    { 
        // m_color_buffer.reset(POOL_ALLOCATOR.malloc(NUM_FRAME_PIXELS), [&](void* ptr) { POOL_ALLOCATOR.free(ptr, NUM_FRAME_PIXELS)); });
        size_t size = max_color_buffer_size();
        // RGB888Pixel* buf = (RGB888Pixel*)g_buffer_pool.ordered_malloc(num_pixels); 
        uint8_t* buf = new uint8_t[size];
        m_color_buffer.reset(buf, g_buffer_deleter);
        // INFO_CPP("allocated " << m_color_buffer << " of size " << (num_pixels * sizeof(RGB888Pixel)));
    }

    return m_color_buffer.get();
}

uint8_t* camera_frame::depth_buffer() 
{ 
    if(!m_depth_buffer) // Do lazy allocation
    {
        // m_depth_buffer.reset(POOL_ALLOCATOR.malloc(NUM_FRAME_PIXELS), [&](void* ptr) { POOL_ALLOCATOR.free(ptr, NUM_FRAME_PIXELS)); });
        size_t size = max_depth_buffer_size();
        // RGB888Pixel* buf = (RGB888Pixel*)g_buffer_pool.ordered_malloc(num_pixels); 
        uint8_t* buf = new uint8_t[size];
        m_depth_buffer.reset(buf, g_buffer_deleter);
        // INFO_CPP("allocated " << m_depth_buffer << " of size " << (num_pixels * sizeof(RGB888Pixel)));
    }

    return m_depth_buffer.get(); 
}

}; // namespace holo3d
