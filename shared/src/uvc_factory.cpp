#include "uvc_factory.hpp"

namespace holo3d
{

uvc_factory::uvc_factory()
{
    uvc_error_t err = uvc_init(&m_ctx, NULL);
    if (err == UVC_SUCCESS ) 
    {
        INFO("Initialized the UVC system!");
        m_initialized = true;
    } else
    {
        ERROR("Failed to initialize the UVC system, error: %1%", uvc_strerror(err));
    }
}

uvc_factory::~uvc_factory()
{
    if (initialized()) 
    {
        uvc_exit(m_ctx);
        INFO("Shut down UVC system");
    }

    m_initialized = false;
    m_ctx = nullptr;
}

bool device_uris(uri_vec_t &uris)
{
    uvc_device_t** list;

    uvc_error_t err = uvc_get_device_list(m_ctx, &list);

    if (err != UVC_SUCCESS) 
    {
        ERROR("Failed to get the UVC device list: %1%", uvc_strerror(err));
        return false;
    }

    for(size_t dev_idx=0, uvc_device_t* device = list[dev_idx]; 
        device != NULL; 
        device = list[dev_idx++]) 
    {
        uvc_device_descriptor_t* desc;
        err = uvc_get_device_descriptor(desc);
        if (err != UVC_SUCCESS) 
        {
            continue;
        }

        desc;

        uvc_free_device_descriptor(desc);
    }


    uvc_free_device_list(list, 1);
}

}; // namespace holo3d
