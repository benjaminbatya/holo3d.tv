#include "pch_files.hpp"

#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <linux/videodev2.h>

#include "util_funcs.hpp"

#include "frame.hpp"

#include "fps_counter.hpp"

#include "v4l_device.hpp"

#define CLEAR(x) memset(&(x), 0, sizeof(x))

namespace holo3d
{

// From http://linuxtv.org/downloads/v4l-dvb-apis/capture-example.html
static int xioctl(int fh, int request, void *arg)
{
    int r;

    do 
    {
        r = ioctl(fh, request, arg);
    } while (-1 == r && EINTR == errno);

    return r;
}

v4l_device::v4l_device()
{

}

v4l_device::~v4l_device()
{
    stop_capturing();

    uninit();

}

void v4l_device::uninit()
{ 
    if (m_buffers) 
    {
        for (uint32_t i = 0; i < m_num_buffers; ++i) 
        {
            if (-1 == munmap(m_buffers[i].start, m_buffers[i].length)) 
            {
                ERROR("munmap"); 
                // Continue even with errors
            }
        }
        free(m_buffers);
    }
    
    if (m_fd != -1) 
    {
        if (-1 == close(m_fd)) 
        {
            ERROR("close");
        }
        m_fd = -1;
    }
}

bool v4l_device::start_capturing()
{
    // NOTE: Only hande streaming mmap devices for now...
    for (uint32_t i = 0; i < m_num_buffers; ++i) 
    {
        struct v4l2_buffer buf;

        CLEAR(buf);
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;

        if (-1 == xioctl(m_fd, VIDIOC_QBUF, &buf))
        {
            ERROR("VIDIOC_QBUF: %1%, %2%", errno, strerror(errno));
            return false;
        }
    }

    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(m_fd, VIDIOC_STREAMON, &type))
    {
        ERROR("VIDIOC_STREAMON: %1%, %2%", errno, strerror(errno));
        return false;
    }

    return true;
}

bool v4l_device::stop_capturing()
{
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(m_fd, VIDIOC_STREAMOFF, &type))
    {
        ERROR("VIDIOC_STREAMOFF: %1%, %2%", errno, strerror(errno));
        return false;
    }

    return true;
}


bool v4l_device::init(int desired_fps, int desired_width, std::string uri)
{
    struct stat st;

    const char* dev_name = uri.c_str();

    // Open the device
    if (-1 == stat(dev_name, &st))
    {
        ERROR("Cannot identify '%1%': %2%, %3%", dev_name, errno, strerror(errno));
        return false;
    }

    if (!S_ISCHR(st.st_mode))
    {
        ERROR("%1% is not a character device", dev_name);
        return false;
    }

    m_fd = open(dev_name, O_RDWR /* required */ | O_NONBLOCK, 0);

    if (-1 == m_fd)
    {
        ERROR("Cannot open '%1%': %2%, %3%", dev_name, errno, strerror(errno));
        return false;
    }

    // Check the capabilities of the device
    struct v4l2_capability cap;

    if (-1 == xioctl(m_fd, VIDIOC_QUERYCAP, &cap)) 
    {
        if (EINVAL == errno) 
        {
            ERROR("%1% is no V4L2 device", dev_name);
        } else 
        {
            ERROR("VIDIOC_QUERYCAP: %1%, %2%", errno, strerror(errno));
        }
        return false;
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) 
    {
        ERROR("%1% is no video capture device", dev_name);
        return false;
    }

    // NOTE: for now, we only support v4l2's streaming API
    if (!(cap.capabilities & V4L2_CAP_STREAMING)) 
    {
        ERROR("%1% does not support streaming i/o", dev_name);
        return false;
    }

    // Setup default cropping here
//  struct v4l2_cropcap cropcap;
//  CLEAR(cropcap);
//  cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//
//  if (0 == xioctl(fd, VIDIOC_CROPCAP, &cropcap))
//  {
//      struct v4l2_crop crop;
//      crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//      crop.c = cropcap.defrect; /* reset to default */
//
//      xioctl(fd, VIDIOC_S_CROP, &crop);
//      /* Errors ignored. */
//  }

    struct v4l2_format fmt;
    CLEAR(fmt);

    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    // NOTE: we are assuming two resolutions for now: 640x480 and 320x240
    if (desired_width > 320) 
    {
        fmt.fmt.pix.width       = 640;
        fmt.fmt.pix.height      = 480;
    } else
    {
        fmt.fmt.pix.width       = 320;
        fmt.fmt.pix.height      = 240;
    }
    // NOTE: this setting affects the STREAM_FORMAT for the encoder!!
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    fmt.fmt.pix.field       = V4L2_FIELD_ANY; // V4L2_FIELD_INTERLACED;

    if (-1 == xioctl(m_fd, VIDIOC_S_FMT, &fmt))
    {
        ERROR("VIDIOC_S_FMT: %1%, %2%", errno, strerror(errno));
        return false;
    }

    /* Note VIDIOC_S_FMT may change width and height. */

    // Get the video capture parameters
    v4l2_streamparm param;
    param.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(m_fd, VIDIOC_G_PARM, &param))
    {
        ERROR("VIDIOC_G_PARM: %1%, %2%", errno, strerror(errno));
        return false;
    }

    // Set the denominator to desired_fps
    param.parm.capture.timeperframe.denominator = desired_fps;
    if (-1 == xioctl(m_fd, VIDIOC_S_PARM, &param))
    {
        ERROR("VIDIOC_S_PARM: %1%, %2%", errno, strerror(errno));
        return false;
    }

    // initialize MMAP
    struct v4l2_requestbuffers req;
    CLEAR(req);
    // NOTE: we are requesting 4 buffers instead of the normal 2!
    req.count = 256; // 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(m_fd, VIDIOC_REQBUFS, &req)) 
    {
        if (EINVAL == errno) 
        {
            ERROR("%1% does not support memory mapping", dev_name);
        } else {
            ERROR("VIDIOC_REQBUFS: %1%, %2%", errno, strerror(errno));
        }
        return false;
    }

    if (req.count < 2) 
    {
        ERROR("Insufficient buffer memory on %s", dev_name);
        return false;
    }

    m_num_buffers = req.count;
    INFO("number of buffers successfully gotten = %1%", m_num_buffers);

    m_buffers = (buffer*)calloc(m_num_buffers, sizeof(*m_buffers));

    if (!m_buffers) 
    {
        ERROR("Out of memory");
        return false;
    }

    for (uint32_t buf_idx = 0; buf_idx < m_num_buffers; ++buf_idx) 
    {
        struct v4l2_buffer buf;
        CLEAR(buf);

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = buf_idx;

        if (-1 == xioctl(m_fd, VIDIOC_QUERYBUF, &buf))
        {
            ERROR("VIDIOC_QUERYBUF: %1%, %2%", errno, strerror(errno));
            return false;
        }

        m_buffers[buf_idx].length = buf.length;
        m_buffers[buf_idx].start =
                mmap(NULL /* start anywhere */,
                     buf.length,
                     PROT_READ | PROT_WRITE /* required */,
                     MAP_SHARED /* recommended */,
                     m_fd, buf.m.offset);

        if (MAP_FAILED == m_buffers[buf_idx].start)
        {
            ERROR("mmap: %1%, %2%", errno, strerror(errno));
            return false;
        }
    }

    // Begin capturing
    if (!start_capturing()) 
    {
        return false;
    }

    return true;
}

bool v4l_device::read_frame(camera_frame& frame)
{
    // Wait for select to signal that a frame is available
    fd_set fds;
    struct timeval tv;
    int r;
    do
    {
        FD_ZERO(&fds);
        FD_SET(m_fd, &fds);

        /* Timeout. */
        tv.tv_sec = 2;
        tv.tv_usec = 0;

        r = select(m_fd + 1, &fds, NULL, NULL, &tv);

    } while ((r == -1 && (errno = EINTR)));

    if (r == -1)
    {
        ERROR("select: %1%, %2%", errno, strerror(errno));
        return false;
    }

    // Get the next buffer from the driver
    struct v4l2_buffer buf;
    CLEAR(buf);

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(m_fd, VIDIOC_DQBUF, &buf)) 
    {
        switch (errno) 
        {
        case EAGAIN:
            ERROR("VIDIOC_DQBUF: %1%, %2%", errno, strerror(errno));
            return false;

        case EIO:
            /* Could ignore EIO, see spec. */

            /* fall through */
        default:
            ERROR("VIDIOC_DQBUF: %1%, %2%", errno, strerror(errno));
            return false;
        }
    }

    assert(buf.index < m_num_buffers);

    // Copy the data to the frame
    frame.color_time_stamp(get_time());

    ASSERT_ALWAYS(buf.bytesused <= frame.max_color_buffer_size());
    frame.color_buffer_size(buf.bytesused);
    memcpy(frame.color_buffer(), m_buffers[buf.index].start, buf.bytesused);

    // Give the buffer back to the driver
    if (-1 == xioctl(m_fd, VIDIOC_QBUF, &buf))
    {
        ERROR("VIDIOC_QBUF: %1%, %2%", errno, strerror(errno));
        return false;
    }

    return true;
}


}; // namespace holo3d
