#include "pch_files.hpp"

#include "control_protocol.hpp"

#include "fragmenting_socket.hpp"

#include "util_funcs.hpp"

namespace holo3d
{

namespace protocol
{

std::ostream& operator<<(std::ostream& os, const ID& id)
{
#define OUTPUT_ID(e) case ID::e : os << #e; break;
    switch (id)
    {
    OUTPUT_ID(QUERY);
    OUTPUT_ID(QUERY_ACK);
    OUTPUT_ID(START);
    OUTPUT_ID(START_ACK);
    OUTPUT_ID(END);
    OUTPUT_ID(END_ACK);
    OUTPUT_ID(FULL_FRAME);
    OUTPUT_ID(FULL_FRAME_ACK);
    OUTPUT_ID(CAMERA_PARAMS); 
    OUTPUT_ID(LOCATION_UPDATE);
    OUTPUT_ID(CONTROL_MESSAGE);
    OUTPUT_ID(CONTROL_MESSAGE_ACK);
    OUTPUT_ID(IMAGE_FRAGMENT);
    OUTPUT_ID(UPDATE_POSE);        
    default: os << "Unknown protocol id=" << (uint8_t)id; break;
    }
#undef OUTPUT_ID

    return os;
}

QueryData::QueryData(uint16_t sender_port, uint64_t clock_microsec)
{
    this->id = static_id();
    this->sender_port = sender_port;
    this->clock_microsec = clock_microsec;
}

QueryAckData::QueryAckData(TYPE listener_type, int64_t ms_diff)
{
    this->id = static_id();
    this->listener_type = listener_type;
    this->ms_diff = ms_diff;
}

std::ostream& operator<<(std::ostream& os, const QueryAckData::TYPE& id)
{
#define OUTPUT_ID(e) case QueryAckData::TYPE::e : os << #e; break;
    switch (id)
    {
    OUTPUT_ID(CAMERA);
    OUTPUT_ID(TRACKER);
    OUTPUT_ID(POSE);
    default: os << "Unknown listener type id=" << (uint8_t)id; break;
    }
#undef OUTPUT_ID

    return os;
}

std::ostream& operator<<(std::ostream& os, const QueryAckData& data)
{
    os  << "ID=" << data.id 
        << ", listener type=" << data.listener_type 
        << ", ms_diff = " << data.ms_diff;

    return os;
}

LocationUpdateData::LocationUpdateData(uint64_t clock_microsec)
{
    this->id = static_id();
    this->clock_microsec = clock_microsec;
}

std::ostream& operator<<(std::ostream& os, const LocationUpdateData& data)
{
    os  << "ID=" << data.id << ", clock_microsec = " << data.clock_microsec;
    return os;
}

// Static methods for Send
size_t Send::send(fragmenting_socket& socket, void* data, size_t size)
{
    boost::system::error_code ec;

    size_t bytes_tx = socket.send(data, size, ec);
    ASSERT_ALWAYS(!ec);

    return bytes_tx;
}

void Send::basic(fragmenting_socket& socket, const ID id)
{
    BaseData msg { id };
    size_t bytes_tx = send(socket, &msg, sizeof(msg));
    ASSERT_EQUAL_ALWAYS(bytes_tx, sizeof(BaseData));
}

void Send::start(fragmenting_socket& socket, const StartData& data)
{
    StartData msg = data;
    msg.id = ID::START;
    size_t bytes_tx = send(socket, &msg, sizeof(msg));
    ASSERT_EQUAL_ALWAYS(bytes_tx, sizeof(StartData));
}

void Send::async_basic(fragmenting_socket& socket, const ID id, basic_callback_t fn)
{
    BaseData* data = (BaseData*)socket.sidecar_buf();
    data->id = id;
    socket.async_send(data, sizeof(BaseData), [fn](const boost::system::error_code& ec, size_t bytes)
    {
        if (ec == boost::asio::error::operation_aborted) 
        {
            WARN("Operation aborted");
            return;
        }

        ASSERT_ALWAYS(!ec);
        ASSERT_EQUAL_ALWAYS(bytes, sizeof(BaseData));

        if (fn) 
        {
            fn();
        }
    });
}

void Send::async_start(fragmenting_socket& socket, const StartData& data, basic_callback_t fn)
{
    StartData* msg = (StartData*)socket.sidecar_buf();
    memcpy(msg, &data, sizeof(StartData));
    msg->id = ID::START;

    socket.async_send(msg, sizeof(StartData), [fn](const boost::system::error_code& ec, size_t bytes)
    {
        ASSERT_ALWAYS(!ec);
        ASSERT_EQUAL_ALWAYS(bytes, sizeof(StartData));

        if (fn) 
        {
            fn();
        }
    });
}

// Static methods for Receive
size_t Receive::receive(fragmenting_socket& socket, void* data, size_t size)
{
    boost::system::error_code ec;
    
    size_t bytes_rx = socket.receive(data, size, ec);
    ASSERT_ALWAYS(!ec);
    
    return bytes_rx;
}

void Receive::basic(fragmenting_socket& socket, const ID id)
{
    BaseData msg;

    size_t bytes_rx = receive(socket, &msg, sizeof(msg));
    ASSERT_EQUAL_ALWAYS(bytes_rx, sizeof(BaseData));

    ASSERT_EQUAL_ALWAYS(msg.id, id);
}

void Receive::start(fragmenting_socket& socket, StartData& data)
{
    size_t bytes_rx = receive(socket, &data, sizeof(data));
    ASSERT_EQUAL_ALWAYS(bytes_rx, sizeof(StartData));

    ASSERT_EQUAL_ALWAYS(data.id, ID::START);
}

void Receive::async_basic(fragmenting_socket& socket, const ID id, basic_callback_t fn)
{
    socket.async_receive(socket.sidecar_buf(), sizeof(BaseData), [&socket,id,fn](const boost::system::error_code& ec, size_t bytes)
    {
        if (ec == boost::asio::error::operation_aborted) 
        {
            WARN("Operation aborted");
            return;
        }
        ASSERT_ALWAYS(!ec); 
        ASSERT_EQUAL_ALWAYS(bytes, sizeof(BaseData));

        BaseData* message = (BaseData*)socket.sidecar_buf();
        ASSERT_EQUAL_ALWAYS(message->id, id);

        if (fn) 
        {
            fn(); 
        } else
        {
            THROW("async_basic::lambda: Could not call fn()");
        }
    });
}

void Receive::async_start(fragmenting_socket& socket, start_callback_t fn)
{
    socket.async_receive(socket.sidecar_buf(), sizeof(StartData), [&socket,fn](const boost::system::error_code& ec, size_t bytes)
    {
        if (ec == boost::asio::error::operation_aborted) 
        {
            INFO("Operation aborted");
            return;
        }
        ASSERT_ALWAYS(!ec);
        ASSERT_EQUAL_ALWAYS(bytes, sizeof(StartData));

        StartData* message = (StartData*)socket.sidecar_buf();
        ASSERT_EQUAL_ALWAYS(message->id, ID::START);

        if (fn) 
        {
            fn(*message); 
        } else
        {
            THROW("async_start::lambda: Could not call fn()");
        }
    });
}

}; // namespace protocol



}; // namespace holo3d
