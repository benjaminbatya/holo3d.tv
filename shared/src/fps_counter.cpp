#include "pch_files.hpp"

#include "fps_counter.hpp"

#include "util_funcs.hpp"

namespace holo3d
{
    
fps_counter::fps_counter(const std::string& name) : m_name(name)
{
    reset();
}

void fps_counter::reset()
{
    m_last_time = get_time();
    m_num_frames = 0;
}

void fps_counter::update()
{
    m_num_frames++;
    uint64_t curr_time = get_time();
    // INFO("read_frames: curr_time=%ld, last_time=%ld, diff=%ld", curr_time, last_time, curr_time - last_time);
    if ((curr_time - m_last_time) >= MICROS_PER_SEC)
    {
        INFO("%s: fps=%f", m_name, ((float)m_num_frames) * MICROS_PER_SEC / (curr_time - m_last_time));
        m_last_time = curr_time;    
        m_num_frames = 0;
    }
}

} // namespace holo3d
