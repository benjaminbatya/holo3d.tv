#include "pch_files.hpp"

#include <iostream>

#include "openni_device.hpp"

#include "util_funcs.hpp"

#include "oni_holo3d_utils.hpp"

#include "frame.hpp"

namespace holo3d
{

openni_device::openni_device(std::string uri)
: m_device()
, m_streams(NULL)
, m_num_streams(0)
, m_enable_ir(FALSE)
, m_mirrored(FALSE)
{
//  INFO("Called");
    try
    {
        const char* uri_str = uri.empty() ? openni::ANY_DEVICE : uri.c_str();

        INFO("Trying to open device: '%s'", uri_str);
        openni::Status rc = m_device.open(uri_str);
        
        if (rc == openni::STATUS_OK)
        {
            INFO("Using device '%s'", m_device.getDeviceInfo().getUri());
        }
    } catch (std::exception& e)
    {
        ERROR("\nCould not open device: %s: error: %s", uri, e.what());
    }
}

openni_device::~openni_device()
{
//  INFO("Called");
    shutdown();

    m_device.close();
}

void openni_device::shutdown()
{
    INFO("called");

    if (m_ir_stream.isValid())
    {
        m_ir_stream.stop(); 
        m_ir_stream.destroy();
    }

    if (m_depth_stream.isValid())
    {
        m_depth_stream.stop(); 
        m_depth_stream.destroy();
    }

    if (m_color_stream.isValid())
    {
        m_color_stream.stop(); 
        m_color_stream.destroy();
    }
    
    if (m_streams != NULL)
    {
        delete[] m_streams;
        m_streams = NULL;
        m_num_streams = 0;
    }
}

openni::VideoMode openni_device::select_target_video_mode(const openni::SensorType& sensor_type,
                                                          const openni::PixelFormat& required_pixel_format,
                                                          const int& width,
                                                          const int& required_fps)
{
    const openni::SensorInfo* sensor_info = m_device.getSensorInfo(sensor_type);
    const openni::Array<openni::VideoMode>& supported_modes = sensor_info->getSupportedVideoModes();
    
    // INFO_CPP("StreamManager::select_target_video_mode: sensor_type=" << sensor_type);

    bool been_set = false;
    openni::VideoMode video_mode; 
    for (int i=0; i<supported_modes.getSize(); i++)
    {
        INFO_CPP("Mode " << i << ": " << supported_modes[i]);                                                                

        // all of the requested parameters have to match
        if (supported_modes[i].getResolutionX() == width && 
            supported_modes[i].getPixelFormat() == required_pixel_format &&
            supported_modes[i].getFps() == required_fps)
        {
            video_mode = supported_modes[i];
            been_set = true;
            // INFO_CPP("Selecting Mode " << video_mode);
        }
    }

    // We want to fail if we can't find a good video mode
    if(!been_set)
    {
        THROW("Couldn't find a valid video mode to match desired properties: width=%1%, pixel format=%2%, fps=%3%",
              width, required_pixel_format, required_fps);
    }

    return video_mode;
}


void openni_device::create_stream(openni::VideoStream& stream, openni::SensorType sensor_type, openni::PixelFormat pix_fmt, int width, int fps)
{
    openni::Status rc = stream.create(m_device, sensor_type);
    if (rc == openni::STATUS_OK)
    {
        openni::VideoMode mode = select_target_video_mode(sensor_type, pix_fmt, width, fps); 
        // INFO_CPP("StreamManager::create_stream: selected mode=" << mode);
        rc = stream.setVideoMode(mode);
        ASSERT_EQUAL(rc, openni::STATUS_OK);
        
        rc = stream.start();
        if (rc != openni::STATUS_OK)
        {
            stream.destroy();
            THROW("couldn't start stream of type %d: '%s'", 
                  sensor_type, openni::OpenNI::getExtendedError());
        }
    } else
    {
        THROW("couldn't find the stream of type %d: '%s'", 
              sensor_type, openni::OpenNI::getExtendedError());
    }

    INFO_CPP("selected stream mode = " << stream.getVideoMode());
}

/**
 * 
 * 
 * @author benjamin (7/11/2015)
 * 
 * @param desired_width 
 * @param uri 
 * @param enable_ir 
 * 
 */
bool openni_device::init(int desired_fps, int desired_width, bool enable_ir)
{
/*                                                                              
    for (int i=openni::SENSOR_IR; i<=openni::SENSOR_DEPTH; i++)                             
    {                                                                                       
        const openni::SensorInfo *s = device.getSensorInfo((openni::SensorType)i);          
        if(s)                                                                               
        {                                                                                   
            std::stringstream ss;                                                           
            ss << "Sensor " << i << ": type=" <<                                            
                ((i==openni::SENSOR_IR)? "IR": (i==openni::SENSOR_COLOR)? "COLOR": "DEPTH");
            ss << ", modes=";                                                               
                                                                                            
            const openni::Array<openni::VideoMode>& modes = s->getSupportedVideoModes();    
            for (int j=0; j<modes.getSize(); j++)                                           
            {                                                                               
                const openni::VideoMode& m = modes[j];                                      
                ss << "\n\tMode " << j                                                      
                   << ": PixelFormat=" << m.getPixelFormat()                                
                   << ", resolution=" << m.getResolutionX()                                 
                   << "x" << m.getResolutionY()                                             
                   << ", FPS=" << m.getFps();                                               
            }                                                                               
            INFO("%s", ss.str().c_str());                                                   
        }                                                                                   
    }                                                                                                                                                                             
*/

    shutdown();

    if (!m_device.isValid())
    {
        ERROR("Device open failed: %s", openni::OpenNI::getExtendedError()); 
        return false;
    }

    m_enable_ir = enable_ir;
    if (m_enable_ir)
    {
        create_stream(m_ir_stream, openni::SENSOR_IR, openni::PIXEL_FORMAT_GRAY16, desired_width, 30);
        m_num_streams = 1;
        m_streams = new openni::VideoStream*[m_num_streams];
        m_streams[0] = &m_ir_stream;
    } else
    {
        create_stream(m_depth_stream, openni::SENSOR_DEPTH, openni::PIXEL_FORMAT_DEPTH_1_MM, desired_width, 30);
        create_stream(m_color_stream, openni::SENSOR_COLOR, openni::PIXEL_FORMAT_RGB888, desired_width, 30);
        m_num_streams = 2;
        m_streams = new openni::VideoStream*[m_num_streams];
        m_streams[0] = &m_depth_stream;
        m_streams[1] = &m_color_stream;

//      INFO("StreamManager::init: min depth=%d, max depth=%d",
//           m_depth_stream.getMinPixelValue(),
//           m_depth_stream.getMaxPixelValue());

        // INFO_CPP("StreamManager::init: stream #0 mode=" << m_streams[0]->getVideoMode())
    }
   
/*
    if (!m_ir_stream.isValid() || !m_depth_stream.isValid() || !m_color_stream.isValid())
    {                                                                                    
        ERROR("StreamManager::init: One or more of the streams are invalid. Exiting...");
        shutdown();                                                                      
        exit(-1);                                                                        
    }                                                                                    
*/

    // set_mirroring();

    // Always setup image registration if it's supported
    if(m_device.isImageRegistrationModeSupported(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR))
    {
        m_device.setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);
    }

    m_fps = desired_fps;

    INFO("Finished initializing the streams");

    return true;
}

openni::Status openni_device::wait_for_changed_stream(openni::SensorType& updated_sensor)
{
    int changed_index;
    // INFO("StreamManager::wait_for_changed_stream: %d=%x", m_num_streams, m_streams);

    openni::Status rc = openni::OpenNI::waitForAnyStream(m_streams, m_num_streams, &changed_index, 0);

//  INFO("changed index = %d",  changed_index);

    if (rc == openni::STATUS_OK)
    {
        if (m_streams[changed_index] == &m_ir_stream)
        {
            updated_sensor = openni::SENSOR_IR;
        } else if (m_streams[changed_index] == &m_depth_stream)
        {
            updated_sensor = openni::SENSOR_DEPTH;
        } else if (m_streams[changed_index] == &m_color_stream)
        {
            updated_sensor = openni::SENSOR_COLOR;
        }
    }

    return rc;
}

openni::Status openni_device::read_frame_helper(const openni::SensorType& updated_sensor)
{
    switch (updated_sensor)
    {
    case openni::SENSOR_IR:
        return m_ir_stream.readFrame(&m_frame);
        break;
    case openni::SENSOR_DEPTH:
        return m_depth_stream.readFrame(&m_frame);
        break;
    case openni::SENSOR_COLOR:
        return m_color_stream.readFrame(&m_frame);
        break;
    default:
        THROW("StreamManager::read_frame: invalid updated_sensor=%d", updated_sensor);
        return openni::STATUS_BAD_PARAMETER;
        break;
    }
}

bool openni_device::read_frame(camera_frame& current_frame)
{
    // Run at the specified fps
    size_t sleep_time = 1000 / m_fps - 5;
    boost::this_thread::sleep(boost::posix_time::milliseconds(sleep_time));

    bool color_set = false;
    bool depth_set = false;

    openni::SensorType updated_sensor;
    openni::Status rc = openni::STATUS_OK;

    while (rc == openni::STATUS_OK)
    {
        rc = wait_for_changed_stream(updated_sensor);

        if (rc == openni::STATUS_OK)
        {
            rc = read_frame_helper(updated_sensor);
            if (rc != openni::STATUS_OK) 
            {
                ERROR("failed to read from the %1% sensor. Skipping this frame...", updated_sensor);
                return false;
            }

            switch (updated_sensor)
            {
            case openni::SENSOR_COLOR:
                // if (!color_set)
                {
                    // Get the time as soon as possible after the frame is read
                    current_frame.color_time_stamp(get_time());

                    ASSERT_ALWAYS(m_frame.getDataSize() <= (int)current_frame.max_color_buffer_size());
                    current_frame.color_buffer_size(m_frame.getDataSize());
                    memcpy(current_frame.color_buffer(), m_frame.getData(), m_frame.getDataSize());
                    color_set = true;
                }
                break;
            case openni::SENSOR_DEPTH:
                // if (!depth_set)
                {
                    // Get the time as soon as possible after the frame is read
                    current_frame.depth_time_stamp(get_time());

                    ASSERT_ALWAYS(m_frame.getDataSize() <= (int)current_frame.max_depth_buffer_size());
                    current_frame.depth_buffer_size(m_frame.getDataSize());
                    memcpy(current_frame.depth_buffer(), m_frame.getData(), m_frame.getDataSize());
                    depth_set = true;
                }
                break;
            default:
                break;
            }
        }
    }

    return(color_set && depth_set);
}

void openni_device::toggle_mirroring()
{
    m_mirrored = !m_mirrored;

    set_mirroring();
}

void openni_device::set_mirroring()
{
    if (m_enable_ir)
    {
        openni::Status rc = m_ir_stream.setMirroringEnabled(m_mirrored);
        ASSERT(rc == openni::STATUS_OK); 
    } else
    {
        openni::Status rc = m_depth_stream.setMirroringEnabled(m_mirrored);
        ASSERT(rc == openni::STATUS_OK);
        rc = m_color_stream.setMirroringEnabled(m_mirrored);
        ASSERT(rc == openni::STATUS_OK);
    }
}

}; // namespace holo3d
