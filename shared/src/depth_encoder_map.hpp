#ifndef _DEPTH_ENCODER_MAP_HPP_
#define _DEPTH_ENCODER_MAP_HPP_

#include <cstdint>

namespace holo3d
{

extern const uint32_t SIZE_DEPTH_ENCODE_MAP;

extern const uint8_t DEPTH_ENCODE_MAP[];

}; // namespace holo3d

#endif // _DEPTH_ENCODER_MAP_HPP_
