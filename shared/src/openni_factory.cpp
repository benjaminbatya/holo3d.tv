#include "pch_files.hpp"

#include <OpenNI2/OpenNI.h>

#include "util_funcs.hpp"
#include "h3t_file.hpp"

#include "openni_factory.hpp"

namespace holo3d
{

openni_factory::openni_factory()
{   
    // Initialize OpenNI
    openni::Status stat = openni::OpenNI::initialize();
    if(stat == openni::STATUS_OK)
    {
        INFO("Initialized the Openni System");
        m_initialized = true;

        m_devices.clear();
    } else
    {
        ERROR("Failed to initialize OpenNI. status=%d, error=%s", stat, openni::OpenNI::getExtendedError());
    }
}

openni_factory::~openni_factory()
{
    if (initialized()) 
    {
        m_devices.clear();

        openni::OpenNI::shutdown();
        INFO("Shut down Openni system"); 
    }
    m_initialized = false;
}

factory_base::uri_vec_t openni_factory::device_uris()
{
    ASSERT_ALWAYS(initialized());

    factory_base::uri_vec_t uris(0);

    openni::Array<openni::DeviceInfo> device_info_list;
    openni::OpenNI::enumerateDevices(&device_info_list);

    if (device_info_list.getSize() < 1) 
    {
        ERROR("No OpenNI devices found.");
        return uris;
    }

//  INFO("Found devices:\n");
    for (int i=0; i<device_info_list.getSize(); i++)
    {
//      INFO("Device %d: URI='%s', Vendor='%s', Name='%s', USB id=%4x:%4x", i,
//           device_info_list[i].getUri(),
//           device_info_list[i].getVendor(),
//           device_info_list[i].getName(),
//           device_info_list[i].getUsbVendorId(),
//           device_info_list[i].getUsbProductId());

        uris.push_back(device_info_list[i].getUri());
    }

    return uris;
}

factory_base::device_ptr_t openni_factory::device(int designed_fps, int desired_width, std::string uri)
{
    ASSERT_ALWAYS(initialized());

    if (m_devices.find(uri) == m_devices.end()) 
    {
        m_devices[uri] = oni_dev_ptr_t(new openni_device(uri));
    }

    oni_dev_ptr_t device = m_devices.at(uri);
    bool ret = device->init(designed_fps, desired_width, m_enable_ir);
    if (!ret) 
    {
        return nullptr;
    }

    return device;
}

}; // namespace holo3d
