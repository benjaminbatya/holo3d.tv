#include "pch_files.hpp"

#include <iostream>
#include <libyuv.h>
#include <cstring>

#include "util_funcs.hpp"

#include "h3t_file.hpp"

#include "decoder.hpp"

#include "depth_decoder_map.hpp"

// Start VPX stuff

#define VPX_CODEC_DISABLE_COMPAT 1
#include "vpx/vpx_decoder.h"
#include "vpx/vp8dx.h"
#define VPX_INTERFACE (vpx_codec_vp8_dx())

// static int VPX_FLAGS = 0;

inline void VPX_CODEC_THROW(vpx_codec_ctx_t* codec, int res, const char* msg)
{
    if (res)
    {
        const char* detail = vpx_codec_error_detail(codec);
        THROW("%s: '%s'\n\tdetail: '%s'", msg, vpx_codec_error(codec) , (detail ? detail : "none") );
    } 
}

// End VPX stuff

// Start JPEG stuff

#include <turbojpeg.h>

// End JPEG stuff

// START png stuff

#include <png.h>

// END png stuff
//
//// Start OpenCL stuff
//#if defined OPEN_CL_ENABLED
//#define __CL_ENABLE_EXCEPTIONS
//
//// We are only using OpenCL 1.1 for now. It's the only distributed version
//#define CL_USE_DEPRECATED_OPENCL_1_1_APIS
//#include <CL/cl.h>
//#undef CL_VERSION_1_2
//#include <CL/cl.hpp>
//
//#endif
//// End OpenCL stuff

namespace holo3d
{

namespace decoder
{

factory::factory(size_t width, size_t height, size_t output_stride_in_pixels)
: m_width(width)
, m_height(height)
, m_output_stride_in_pixels(output_stride_in_pixels)
{
    
}

color_decoder_t factory::color(STREAM_FORMAT color_format)
{
    color_decoder_t decoder;

    switch (color_format)
    {
    case STREAM_FORMAT::COLOR_RGB888:   decoder.reset( new color::rgb888 ); break;
    case STREAM_FORMAT::COLOR_YUV422:   decoder.reset( new color::yuv422 ); break;
    case STREAM_FORMAT::COLOR_YUYV:     decoder.reset( new color::yuyv ); break;
    case STREAM_FORMAT::COLOR_YV12:     decoder.reset( new color::yv12 ); break;
    case STREAM_FORMAT::COLOR_JPEG:     decoder.reset( new color::jpeg ); break;
    case STREAM_FORMAT::COLOR_VP8:      decoder.reset( new color::vp8 ); break;
    default: THROW_CPP("INVALID format " << color_format); break;
    }

    decoder->configure(m_width, m_height, m_output_stride_in_pixels);
    decoder->set_format(color_format);

    return decoder;
}

depth_decoder_t factory::depth(STREAM_FORMAT depth_format, bool use_histogram, size_t histogram_size)
{
    depth_decoder_t decoder;

    switch (depth_format)
    {
    case STREAM_FORMAT::DEPTH_RAW:  decoder.reset( new depth::raw ); break;
    case STREAM_FORMAT::DEPTH_ENC:  decoder.reset( new depth::enc ); break;
    case STREAM_FORMAT::DEPTH_JPEG: decoder.reset( new depth::jpeg ); break;
    case STREAM_FORMAT::DEPTH_PNG:  decoder.reset( new depth::png ); break;
    case STREAM_FORMAT::DEPTH_VP8:  decoder.reset( new depth::vp8 ); break;
    case STREAM_FORMAT::INVALID:    decoder.reset( new depth::invalid ); break;
    default: THROW_CPP("INVALID format " << depth_format); break;
    }

    decoder->configure(m_width, m_height, m_output_stride_in_pixels);

    decoder->use_histogram(use_histogram); 
    decoder->histogram_size(histogram_size);
    decoder->set_format(depth_format);

    return decoder;
}

base::~base()
{
    if (m_work_buf)
    {
        delete [] m_work_buf;
    }
}

void base::configure(size_t width, size_t height, size_t output_stride_in_pixels)
{
    m_width = width;
    m_height = height;
    m_output_stride_in_pixels = output_stride_in_pixels;
}

uint8_t* base::work_buf()
{
    // I love lazy allocation
    if (!m_work_buf)
    {
        m_work_buf = new uint8_t[width() * height()*sizeof(RGB888Pixel)];
    }

    return m_work_buf;
}

static size_t stride_in_bytes(STREAM_FORMAT fmt, size_t width) 
{
    return width * get_pixel_depth(fmt);
}

namespace color
{

void rgb888::decode(RGB888Pixel* output, 
                    const uint8_t* input, 
                    const size_t /* output_buffer_size_in_bytes */, 
                    const size_t /* input_buffer_size_in_bytes */,
                    const RECT& rect)
{
    size_t width = rect.is_empty() ? this->width(): rect.width;
    size_t height = rect.is_empty() ? this->height(): rect.height;

    const RGB888Pixel *input_row = (const RGB888Pixel *)input;
    RGB888Pixel* output_row = output;

    const size_t num_bytes = stride_in_bytes(STREAM_FORMAT::COLOR_RGB888, width);

    for (size_t y = 0; y < height; ++y)
    {
        memcpy(output_row, input_row, num_bytes);

        input_row += width;
        output_row += output_stride_in_pixels();
    }
}

void yuv422::decode(RGB888Pixel* output, 
                    const uint8_t* input, 
                    const size_t /* output_buffer_size_in_bytes */, 
                    const size_t /* input_buffer_size_in_bytes */,
                    const RECT& rect)
{
    size_t width = rect.is_empty() ? this->width(): rect.width;
    size_t height = rect.is_empty() ? this->height(): rect.height;

    uint32_t i420_plane_size = width * height;

    uint8_t* y_plane = work_buf();
    uint8_t* u_plane = work_buf() + i420_plane_size * 1;
    uint8_t* v_plane = work_buf() + i420_plane_size * 2;

    int ret =
    libyuv::UYVYToI420(input, stride_in_bytes(STREAM_FORMAT::COLOR_YUV422, width), 
                       y_plane, width,
                       u_plane, width,
                       v_plane, width,
                       width, height );
    ASSERT(ret == 0);

    ret = 
    libyuv::I420ToRAW(y_plane, width,
                      u_plane, width,
                      v_plane, width,
                      (uint8*)output, stride_in_bytes(STREAM_FORMAT::COLOR_RGB888, output_stride_in_pixels()),
                      width, height );
    ASSERT(ret == 0);
}

void yuyv::decode(RGB888Pixel* output, 
                  const uint8_t* input, 
                  const size_t /* output_buffer_size_in_bytes */, 
                  const size_t /* input_buffer_size_in_bytes */,
                  const RECT& rect)
{
    size_t width = rect.is_empty() ? this->width(): rect.width;
    size_t height = rect.is_empty() ? this->height(): rect.height;

    uint32_t i420_plane_size = width * height;

    uint8_t* y_plane = work_buf();
    uint8_t* u_plane = work_buf() + i420_plane_size * 1;
    uint8_t* v_plane = work_buf() + i420_plane_size * 2;

    int ret =
    libyuv::YUY2ToI420(input, stride_in_bytes(STREAM_FORMAT::COLOR_YUV422, width), 
                       y_plane, width,
                       u_plane, width,
                       v_plane, width,
                       width, height );
    ASSERT(ret == 0);

    ret =
    libyuv::I420ToRAW(y_plane, width,
                      u_plane, width,
                      v_plane, width,
                      (uint8*)output, stride_in_bytes(STREAM_FORMAT::COLOR_RGB888, output_stride_in_pixels()),
                      width, height );
    ASSERT(ret == 0);
}

void yv12::decode(RGB888Pixel* output, 
                    const uint8_t* input, 
                    const size_t /* output_buffer_size_in_bytes */, 
                    const size_t /* input_buffer_size_in_bytes */,
                    const RECT& rect)
{
    size_t width = rect.is_empty() ? this->width(): rect.width;
    size_t height = rect.is_empty() ? this->height(): rect.height;

    // YV12 is the same as i420 but with the U and V planes switched
    uint32_t i420_plane_size = width * height;

    const uint8_t* y_plane = input;
    const uint8_t* v_plane = y_plane + i420_plane_size * 1;
    const uint8_t* u_plane = y_plane + i420_plane_size * 5 / 4;

    int ret =
    libyuv::I420ToRAW(y_plane, width,
                        u_plane, width / 2,
                        v_plane, width / 2,
                        (uint8*)output, stride_in_bytes(STREAM_FORMAT::COLOR_RGB888, output_stride_in_pixels()),
                        width, height );
    ASSERT(ret == 0);
}

jpeg::jpeg()
{
    m_jpeg_decompressor = tjInitDecompress();
    ASSERT_THROW(m_jpeg_decompressor, "failed to allocate the jpeg decompressor: %s", tjGetErrorStr());
}

jpeg::~jpeg()
{
    int ret = tjDestroy(m_jpeg_decompressor);
    ASSERT_THROW (!ret, "failed to destroy the jpeg decompressor: %s", tjGetErrorStr());
}

void jpeg::decode(RGB888Pixel* output, 
                    const uint8_t* input, 
                    const size_t /* output_buffer_size_in_bytes */, 
                    const size_t input_buffer_size_in_bytes,
                    const RECT& rect)
{
    size_t width = rect.is_empty() ? this->width(): rect.width;
    size_t height = rect.is_empty() ? this->height(): rect.height;

    int w, h, subsampling;
    int ret = 
    tjDecompressHeader2(m_jpeg_decompressor, (unsigned char *)input, input_buffer_size_in_bytes,
                        &w, &h, &subsampling);
    if (ret)
    {
        THROW_CPP("error decoding jpeg header: " << tjGetErrorStr());
    }

    ASSERT_EQUAL((size_t)w, width);
    ASSERT_EQUAL((size_t)h, height);

    ret = 
    tjDecompress2(m_jpeg_decompressor, (unsigned char *)input, input_buffer_size_in_bytes, 
                  (unsigned char*)output, width, stride_in_bytes(STREAM_FORMAT::COLOR_RGB888, output_stride_in_pixels()), 
                  height, TJPF_RGB, TJFLAG_FASTDCT);
    if (ret)
    {
        THROW_CPP("error decoding jpeg header: " << tjGetErrorStr());
    }
}

// Code from http://www.webmproject.org/docs/vp8-sdk/example__simple__decoder.html
vp8::vp8()
{
    // INFO("VPX interface: %s", vpx_codec_iface_name(VPX_INTERFACE));

    m_vpx_codec = new vpx_codec_ctx_t;

    vpx_codec_err_t  res = vpx_codec_dec_init(m_vpx_codec, VPX_INTERFACE, nullptr, 0);
    VPX_CODEC_THROW(m_vpx_codec, res, "failed to initialize the codec");
}

vp8::~vp8()
{
    if (m_vpx_codec)
    {   
        vpx_codec_err_t  res = vpx_codec_destroy(m_vpx_codec);
        VPX_CODEC_THROW(m_vpx_codec, res, "error destroying vpx codec");
    }
    delete m_vpx_codec;
}

void vp8::decode(RGB888Pixel* output, 
                 const uint8_t* input, 
                 const size_t /* output_buffer_size_in_bytes */, 
                 const size_t input_buffer_size_in_bytes,
                 const RECT& rect)
{
    size_t width = rect.is_empty() ? this->width(): rect.width;
    size_t height = rect.is_empty() ? this->height(): rect.height;

    vpx_codec_err_t res = vpx_codec_decode(m_vpx_codec, input, input_buffer_size_in_bytes, nullptr, 0);
    VPX_CODEC_THROW(m_vpx_codec, res, "error decoding frame");

    // Get a YV12 image frame from the decoder
    vpx_codec_iter_t  iter = NULL;
    vpx_image_t      *img;

    img = vpx_codec_get_frame(m_vpx_codec, &iter);

    ASSERT(vpx_codec_get_frame(m_vpx_codec, &iter) == nullptr);

    ASSERT_EQUAL(img->d_w, width);
    ASSERT_EQUAL(img->d_h, height);

    const uint8_t* y_plane = img->planes[VPX_PLANE_Y];
    const uint8_t* u_plane = img->planes[VPX_PLANE_U];
    const uint8_t* v_plane = img->planes[VPX_PLANE_V];

//  static bool printed = false;
//  if (!printed)
//  {
//      INFO("y stride=%d, u stride = %d, v stride = %d",
//           img->stride[VPX_PLANE_Y], img->stride[VPX_PLANE_U], img->stride[VPX_PLANE_V]);
//      printed = true;
//  }
    int ret =
    libyuv::I420ToRAW(y_plane, img->stride[VPX_PLANE_Y],
                      u_plane, img->stride[VPX_PLANE_U],
                      v_plane, img->stride[VPX_PLANE_V],
                      (uint8*)output, stride_in_bytes(STREAM_FORMAT::COLOR_RGB888, output_stride_in_pixels()),
                      width, height );
    ASSERT(ret == 0);

}

}; // namespace color

#if defined OPEN_CL_ENABLED
// this was generated from convolution.cl with xxd -i src/convolution.cl src/convolution.cl.h

const char* g_ocl_source = 
    #include "depth_decode.cl.h"
;
#endif //  defined OPEN_CL_ENABLED

namespace depth
{

void base::configure(size_t w, size_t h, size_t output_stride_in_pixels)
{
    ::holo3d::decoder::base::configure(w, h, output_stride_in_pixels);

#if defined OPEN_CL_ENABLED
    m_ocl_device = boost::compute::system::default_device();
    m_ocl_context = boost::compute::context(m_ocl_device);
    m_ocl_queue = boost::compute::command_queue(m_ocl_context, m_ocl_device);

    m_ocl_program = boost::compute::program::create_with_source(g_ocl_source, m_ocl_context);

    try
    {
        m_ocl_program.build();
    } catch(boost::compute::opencl_error e)
    {
        // NOTE: Throwing inside a ctor is bad and shouldn't be done, oh well...
        THROW("OpenCL Build error: %s", m_ocl_program.build_log());
    }

    m_ocl_kernel = boost::compute::kernel(m_ocl_program, "depth_decode");

//  m_ocl_input_image = boost::compute::image2d(m_ocl_context,
//                                              boost::compute::image2d::read_only |
//                                              boost::compute::image2d::copy_host_ptr,
//                                              boost::compute::image_format(boost::compute::image_format::rgba,
//                                                                           boost::compute::image_format::unsigned_int8),
//                                              width(), height());
    m_ocl_input_image = boost::compute::buffer(m_ocl_context, 4*sizeof(uint8_t)*width()*height());

//  m_ocl_output_image = boost::compute::image2d(m_ocl_context,
//                                               boost::compute::image2d::write_only,
//                                               boost::compute::image_format(boost::compute::image_format::r,
//                                                                            boost::compute::image_format::unorm_int16),
//                                               width(), height());
    m_ocl_output_image = boost::compute::buffer(m_ocl_context, sizeof(uint16_t)*width()*height());

    m_ocl_kernel.set_arg(0, m_ocl_input_image);
    m_ocl_kernel.set_arg(1, m_ocl_output_image);

    // setup the bounds
    m_ocl_origin[0] = 0; m_ocl_origin[1] = 0;
    m_ocl_region[0] = width(); m_ocl_region[1] = height();

#endif
}

base::~base()
{
    if (m_histogram)
    {
        delete [] m_histogram;
        m_histogram = nullptr;
    }

    if (m_work_buf2)
    {   
        delete [] m_work_buf2;
        m_work_buf2 = nullptr;
    }
}

void base::decode(RGB888Pixel* output, 
                  const uint8_t* input, 
                  const size_t /* output_buffer_size_in_bytes */, 
                  const size_t input_buffer_size_in_bytes,
                  const RECT& /*rect*/)
{
    decode_depth((DepthPixel*)work_buf2(), input, work_buf2_size(), input_buffer_size_in_bytes);

    calculate_histogram((DepthPixel*)work_buf2()); 

    convert_depth_to_color(output, (DepthPixel*)work_buf2());
}

void base::calculate_histogram(const DepthPixel* depth)
{
    if (!use_histogram()) { return; }

    float* h = histogram();
	// Calculate the accumulative histogram (the yellow display...)
	memset(h, 0, histogram_size()*sizeof(float));

	unsigned int number_of_points = 0;
	for (size_t y = 0; y < height(); ++y)
	{
		for (size_t x = 0; x < width(); ++x, ++depth)
		{
			if (*depth != 0)
			{
				h[*depth]++;
				number_of_points++;
			}
		}
	}

	for (size_t index=1; index < histogram_size(); index++)
	{
		h[index] += h[index-1];
	}

    if (number_of_points)
	{
		for (size_t index=1; index < histogram_size(); index++)
		{
			h[index] = (256 * (1.0f - (h[index] / number_of_points)));
		}
	}
}

void base::set_depth_pixel( RGB888Pixel* output, const DepthPixel& depth)
{
    if (use_histogram())
    {
        // This uses the histogram to determine the actual brightness of the pixel
        if (depth != 0)
        {
            float* histo = histogram();
            int hist_value = histo[depth];
            output->r = hist_value;
            output->g = hist_value;
            output->b = 0; // hist_value;
        }
    } else
    {
        // this is a linear interpolation from
        uint8_t val = 0xff - (uint8_t)(((uint32_t)depth) * 0xff / histogram_size());
        memset(output, val, 3);
    }
}

void base::convert_depth_to_color(RGB888Pixel* output, const DepthPixel* input)
{
    const DepthPixel* input_row = input;
    size_t input_stride = width();

    RGB888Pixel* output_row = output; // ignore cropping. don't use it right now...
    size_t output_stride = output_stride_in_pixels();

    for (size_t y=0; y<height(); y++)
    {
        const DepthPixel* input_idx = input_row;
        RGB888Pixel* output_idx = output_row;

        for (size_t x = 0; x < width(); x++, input_idx++, output_idx++)
        {
            set_depth_pixel(output_idx, *input_idx);
        }

        input_row += input_stride;
        output_row += output_stride;
    }
}


#if defined OPEN_CL_ENABLED

//// Use OpenCL to decode the input into a depth value
//void base::decode_to_depth(DepthPixel* output, const RGB888Pixel* input)
//{
//    // Convert the input to ARGB (OpenCl requires 16byte alignment
//    int ret = libyuv::RAWToARGB((const uint8_t*)input, width(),
//                                work_buf2(), width(),
//                                width(), height());
//    ASSERT_EQUALS(ret, 0);
//
////  m_ocl_input_img =
////      boost::compute::image2d(m_ocl_context,
////                              boost::compute::image2d::read_only |
////                              boost::compute::image2d::copy_host_ptr,
////                              boost::compute::image_format(boost::compute::image_format::rgba,
////                                                           boost::compute::image_format::unsigned_int8),
////                              width(), height(),
////                              0, work_buf2());
//
//    static uint64_t time = 0;
//    static uint64_t frames = 0;
//    uint64_t start_time = get_time();
//
//    // Let opencl handle all of the heavy lifting
//    // m_ocl_queue.enqueue_write_image(m_ocl_input_image, m_ocl_origin, m_ocl_region, 0, work_buf2());
//    m_ocl_queue.enqueue_write_buffer(m_ocl_input_image, 0, work_buf2_size(), work_buf2());
//
//    m_ocl_queue.enqueue_nd_range_kernel(m_ocl_kernel, 2, m_ocl_origin, m_ocl_region, 0);
//
//    //  m_ocl_queue.enqueue_read_image(m_ocl_output_image, m_ocl_origin, m_ocl_region, 0, output);
//    m_ocl_queue.enqueue_read_buffer(m_ocl_output_image, 0, sizeof(uint16_t)*width()*height(), output);
//
//    m_ocl_queue.finish();
//    time += get_time() - start_time;
//    frames++;
//    if (frames > 30)
//    {
//        INFO("profile: average time/frame = %d", time / frames);
//        frames = 0;
//        time = 0;
//    }
//}
//

// THIS is the slow way but it's more exact. The jpeg compression causes problems because it's values
// are not exact. It might be better if I use a lossless codec (like png) to work which will not cause interpolation problems
void base::decode_to_depth(DepthPixel* output, const RGB888Pixel* input)
{
//  static uint64_t time = 0;
//  static uint64_t frames = 0;
//  uint64_t start_time = get_time();

    const RGB888Pixel* input_row = input;
    size_t input_stride = width();

    DepthPixel* output_row = output; // ignore cropping. don't use it right now...
    size_t output_stride = width();

    const double np = 512.0;
    const double w = 10000.0; // 1<<16;

    // NOTE: These are defines to preserve precision
#define p np / w
#define half_p p / 2.0
#define quarter_p p / 4.0
#define eighth_p p / 8.0

    for (size_t y=0; y<height(); y++)
    {
       const RGB888Pixel* input_idx = input_row;
       DepthPixel*  output_idx = output_row;

       for (size_t x = 0; x < width(); x++, input_idx++, output_idx++)
       {
           double L = ((double)input_idx->r) / 256;
           double Ha =  ((double)input_idx->g) / 256;
           double Hb =  ((double)input_idx->b) / 256;

           double mL = 4.0 * L / p - 0.5;
           mL = fmod(floor(mL), 4.0);
           if (mL < 0) { mL += 4.0; }
           size_t temp = (size_t)mL;

           double L0 = L - fmod(L - eighth_p, p) + (mL * quarter_p) - eighth_p;

           double delta;
           switch (temp)
           {
           case 0: delta = Ha * half_p; break;
           case 1: delta = Hb * half_p; break;
           case 2: delta = (1.0 - Ha) * half_p; break;
           case 3: delta = (1.0 - Hb) * half_p; break;
           default: THROW("assert failed: mL=%lu", temp); break;
           }

           DepthPixel depth = (DepthPixel)(w * (L0 + delta));
           *output_idx = depth;
       }

       input_row += input_stride;
       output_row += output_stride;
    }

#undef p
#undef half_p
#undef quarter_p
#undef eighth_p

//  time += get_time() - start_time;
//  frames++;
//  if (frames > 30)
//  {
//      INFO("profile: average time/frame = %d", time / frames);
//      frames = 0;
//      time = 0;
//  }
}

#else // defined OPEN_CL_ENABLED

inline int hash_key(int d, const RGB888Pixel& key)
{
    if (d == 0)
    {
        d = 0x01000193;
    }

    // Use the FNV algorithm from http://isthe.com/chongo/tech/comp/fnv/
    d = ( (d * 0x01000193) ^ key.r ) & 0xffffffff;
    d = ( (d * 0x01000193) ^ key.g ) & 0xffffffff;
    d = ( (d * 0x01000193) ^ key.b ) & 0xffffffff;

    return d;
}

inline DepthPixel perfect_hash_lookup(const RGB888Pixel& key)
{
    int d = hash_key(0, key) % SIZE_DEPTH_DECODE_MAP;
    d = DEPTH_DECODE_G[d];
    if (d < 0)
    {
        d = -d - 1;
        return (DepthPixel)DEPTH_DECODE_V[d];
    }

    d = hash_key(d, key) % SIZE_DEPTH_DECODE_MAP;
    return (DepthPixel)DEPTH_DECODE_V[d];
}

void base::decode_to_depth(DepthPixel* output, const RGB888Pixel* input)
{
    const RGB888Pixel* input_row = input;
    size_t input_stride = width();

    DepthPixel* output_row = output; // ignore cropping. don't use it right now...
    size_t output_stride = width();
   
    for (size_t y=0; y<height(); y++)
    {
        const RGB888Pixel* input_idx = input_row;
        DepthPixel* output_idx = output_row;

        for (size_t x = 0; x < width(); x++, input_idx++, output_idx++)
        {
            *output_idx = perfect_hash_lookup(*input_idx);
        }

        input_row += input_stride;
        output_row += output_stride;
    }
}

#endif //  defined OPEN_CL_ENABLED

float* base::histogram()
{
    if (!m_histogram)
    {
        m_histogram = new float[histogram_size()];
    }

    return m_histogram;
}

uint8_t* base::work_buf2()
{
    // I love lazy allocation
    if (!m_work_buf2)
    {
        m_work_buf2 = new uint8_t[work_buf2_size()];
    }

    return m_work_buf2;
}

void invalid::decode(RGB888Pixel* output, 
                     const uint8_t* input, 
                     const size_t /* output_buffer_size_in_bytes */, 
                     const size_t input_buffer_size_in_bytes,
                     const RECT& /*rect*/)
{
    // Sanity check the input buffers
    ASSERT(input_buffer_size_in_bytes == 0);
}

void invalid::decode_depth(DepthPixel* output,
                           const uint8_t* input,
                           const size_t output_size_in_bytes,
                           const size_t input_buffer_size_in_bytes)
{
    // Do nothing
}

void raw::decode(RGB888Pixel* output, 
                 const uint8_t* input, 
                 const size_t /* output_buffer_size_in_bytes */, 
                 const size_t /* input_buffer_size_in_bytes */,
                 const RECT& /*rect*/)
{
    calculate_histogram((const DepthPixel*)input); 

    const DepthPixel* input_row = (const DepthPixel*)input;
    RGB888Pixel* output_row = output;

    for (size_t y = 0; y < height(); ++y)
    {
        const DepthPixel* input_idx = input_row;
        RGB888Pixel* output_idx = output_row;

        for (size_t x = 0; x < width(); ++x, ++input_idx, ++output_idx)
        {
            set_depth_pixel(output_idx, *input_idx);
        }

        input_row += width();
        output_row += output_stride_in_pixels();
    }
}

void raw::decode_depth(DepthPixel* output,
                       const uint8_t* input,
                       const size_t output_size_in_bytes,
                       const size_t input_buffer_size_in_bytes)
{
    ASSERT_EQUAL(input_buffer_size_in_bytes, num_pixels() * sizeof(DepthPixel));
    ASSERT(output_size_in_bytes >= num_pixels()*sizeof(DepthPixel));

    const DepthPixel* input_row = (const DepthPixel*)input;
    DepthPixel*  output_row = output;

    for (size_t y = 0; y < height(); ++y)
    {
        std::memcpy(output_row, input_row, width()*sizeof(DepthPixel));

        input_row += width();
        output_row += output_stride_in_pixels();
    }
}

void enc::decode_depth(DepthPixel* output,
                       const uint8_t* input,
                       const size_t output_size_in_bytes,
                       const size_t input_buffer_size_in_bytes)
{
    ASSERT_EQUAL(input_buffer_size_in_bytes, num_pixels() * sizeof(RGB888Pixel));
    ASSERT(output_size_in_bytes >= num_pixels()*sizeof(DepthPixel));

    decode_to_depth(output, (const RGB888Pixel*)input);
}

jpeg::jpeg()
{
    m_jpeg_decompressor = tjInitDecompress();
    ASSERT_THROW(m_jpeg_decompressor, "failed to allocate the jpeg decompressor: %s", tjGetErrorStr());
}

jpeg::~jpeg()
{
    int ret = tjDestroy(m_jpeg_decompressor);
    ASSERT_THROW (!ret, "failed to destroy the jpeg decompressor: %s", tjGetErrorStr());
}

void jpeg::decode_depth(DepthPixel* output,
                       const uint8_t* input,
                       const size_t output_size_in_bytes,
                       const size_t input_buffer_size_in_bytes)
{
    ASSERT(output_size_in_bytes >= num_pixels() * sizeof(DepthPixel));

    int w, h, subsampling;
    int ret = 
    tjDecompressHeader2(m_jpeg_decompressor, (unsigned char *)input, input_buffer_size_in_bytes,
                        &w, &h, &subsampling);
    ASSERT_THROW(!ret, "error decoding jpeg header: %s", tjGetErrorStr());

    ASSERT_EQUAL((size_t)w, width());
    ASSERT_EQUAL((size_t)h, height());

    ret = 
    tjDecompress2(m_jpeg_decompressor, (unsigned char *)input, input_buffer_size_in_bytes, 
                  (unsigned char*)work_buf(), width(), 0, height(), TJPF_RGB, TJFLAG_ACCURATEDCT);
    ASSERT_THROW(!ret, "error decoding jpeg header: %s", tjGetErrorStr());

//#define USE_FILTER

#if defined USE_FILTER

    decode_to_depth((DepthPixel*)work_buf2(), (const RGB888Pixel*)work_buf());

    int filter_width = 3;
    // Go through the buffer and run a 3x3 smoothing kernel over it
    float kernel[] =
    { 1.f/16, 1.f/8, 1.f/16
    , 1.f/8,  1.f/4, 1.f/8
    , 1.f/16, 1.f/8, 1.f/16
    };
//    // Generated from http://www.embege.com/gauss/
//    float kernel[] =
//    {
//        0.07511360795411207, 0.12384140315297386, 0.07511360795411207,
//        0.12384140315297386, 0.20417995557165622, 0.12384140315297386,
//        0.07511360795411207, 0.12384140315297386, 0.07511360795411207
//    };

//  // Generated from http://www.embege.com/gauss/
//  int filter_width = 5;
//  float kernel[] =
//  {
//  0.00296901674395065, 0.013306209891014005, 0.02193823127971504, 0.013306209891014005, 0.00296901674395065,
//  0.013306209891014005, 0.05963429543618023, 0.09832033134884507, 0.05963429543618023, 0.013306209891014005,
//  0.02193823127971504, 0.09832033134884507, 0.16210282163712417, 0.09832033134884507, 0.02193823127971504,
//  0.013306209891014005, 0.05963429543618023, 0.09832033134884507, 0.05963429543618023, 0.013306209891014005,
//  0.00296901674395065, 0.013306209891014005, 0.02193823127971504, 0.013306209891014005, 0.00296901674395065
//  };

//  int filter_width = 7;
//  float kernel[] =
//  {
//  0.00001965191612403453, 0.00023940934949729186, 0.0010729582649787318, 0.0017690091140439247, 0.0010729582649787318, 0.00023940934949729186, 0.00001965191612403453,
//  0.00023940934949729186, 0.002916602954386583, 0.013071307583189733, 0.021550942848268615, 0.013071307583189733, 0.002916602954386583, 0.00023940934949729186,
//  0.0010729582649787318, 0.013071307583189733, 0.058581536330607024, 0.09658462501856331, 0.058581536330607024, 0.013071307583189733, 0.0010729582649787318,
//  0.0017690091140439247, 0.021550942848268615, 0.09658462501856331, 0.1592411256906998, 0.09658462501856331, 0.021550942848268615, 0.0017690091140439247,
//  0.0010729582649787318, 0.013071307583189733, 0.058581536330607024, 0.09658462501856331, 0.058581536330607024, 0.013071307583189733, 0.0010729582649787318,
//  0.00023940934949729186, 0.002916602954386583, 0.013071307583189733, 0.021550942848268615, 0.013071307583189733, 0.002916602954386583, 0.00023940934949729186,
//  0.00001965191612403453, 0.00023940934949729186, 0.0010729582649787318, 0.0017690091140439247, 0.0010729582649787318, 0.00023940934949729186, 0.00001965191612403453
//  };

    int half_width = filter_width / 2;
    
    const DepthPixel* in_val = (DepthPixel*)work_buf2();
    DepthPixel* out_val = (DepthPixel*)output+width()+half_width;
    for (size_t j=half_width; j<height()-half_width; j++) 
    {
        for (size_t i=half_width; i<width()-half_width; i++, out_val++) 
        {
            float val = 0.f;
            for (int m=0; m<filter_width; m++) 
            {
                for (int n=0; n<filter_width; n++) 
                {
                    size_t val_idx = (m+j-half_width)*width() + (n+i-half_width);
                    size_t kern_idx = m*filter_width + n;
                    float temp = in_val[val_idx];
                    val += temp * kernel[kern_idx]; 
                }
            }
            *out_val = (DepthPixel)val;
        }
        out_val += 2*half_width;
    }

#else

     decode_to_depth(output, (const RGB888Pixel*)work_buf());

#endif // defined USE_FILTER
}

struct png_impl
{
    png_structp png_ptr = nullptr;
    png_infop info_ptr = nullptr;

    uint8_t* curr_buff_ptr = nullptr;
    size_t max_buff_size = 0;

    size_t bytes_transferred = 0;    
};

void read_data(png_structp png_ptr, png_bytep data, png_size_t length)
{
    png_impl* impl = (png_impl*)png_get_io_ptr(png_ptr);

    // INFO("length = %d, transferred = %d, max = %d", length, impl->bytes_transferred, impl->max_buff_size);

    ASSERT(impl->bytes_transferred + length <= impl->max_buff_size);

    memcpy(data, impl->curr_buff_ptr, length);

    impl->curr_buff_ptr += length;
    impl->bytes_transferred += length;
}

png::png()
{
    m_impl = new png_impl;

//  m_impl->png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
//  ASSERT_THROW_ALWAYS(m_impl->png_ptr, "Failed to create the read structure");
//
//  m_impl->info_ptr = png_create_info_struct(m_impl->png_ptr);
//  ASSERT_THROW_ALWAYS(m_impl->info_ptr, "Failed to create the info structure");
//
//  png_set_read_fn(m_impl->png_ptr, m_impl, read_data);
}

png::~png()
{
//  png_destroy_read_struct(&m_impl->png_ptr, &m_impl->info_ptr, nullptr);

    delete m_impl;
}

void png::decode_depth(DepthPixel* output,
                       const uint8_t* input,
                       const size_t output_size_in_bytes,
                       const size_t input_buffer_size_in_bytes)
{
    // START HACK
    m_impl->png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    ASSERT_THROW_ALWAYS(m_impl->png_ptr, "Failed to create the read structure");

    m_impl->info_ptr = png_create_info_struct(m_impl->png_ptr);
    ASSERT_THROW_ALWAYS(m_impl->info_ptr, "Failed to create the info structure");

    png_set_read_fn(m_impl->png_ptr, m_impl, read_data);  
    // END HACK

    const size_t HEADER_SIZE = 8;
    uint8_t* nonconst_input = (uint8_t*)input;
    int ret = png_sig_cmp(nonconst_input, 0, HEADER_SIZE);
    ASSERT_EQUAL_THROW_ALWAYS(ret, 0, "PNG header is incorrect!");

    if (setjmp(png_jmpbuf(m_impl->png_ptr))) { THROW("Error initializing"); }

    m_impl->max_buff_size = input_buffer_size_in_bytes;
    m_impl->curr_buff_ptr = nonconst_input + HEADER_SIZE;
    m_impl->bytes_transferred = HEADER_SIZE;  

    png_set_sig_bytes(m_impl->png_ptr, HEADER_SIZE);
    png_read_info(m_impl->png_ptr, m_impl->info_ptr);

    size_t w = png_get_image_width(m_impl->png_ptr, m_impl->info_ptr);
    ASSERT_ALWAYS(w == width());
    size_t h = png_get_image_height(m_impl->png_ptr, m_impl->info_ptr);
    ASSERT_ALWAYS(h == height());
    png_byte color_type = png_get_color_type(m_impl->png_ptr, m_impl->info_ptr);
    ASSERT_ALWAYS(color_type == PNG_COLOR_TYPE_GRAY);
    png_byte bit_depth = png_get_bit_depth(m_impl->png_ptr, m_impl->info_ptr);
    ASSERT_ALWAYS(bit_depth == 16);
    int number_of_passes = png_set_interlace_handling(m_impl->png_ptr);
    ASSERT_ALWAYS(number_of_passes == 1);

    png_read_update_info(m_impl->png_ptr, m_impl->info_ptr);

    if (setjmp(png_jmpbuf(m_impl->png_ptr))) { THROW("Error during read"); }

    png_bytep* row_pointers = (png_bytep*)work_buf();
    for (size_t y=0; y<height(); y++)
    {
        row_pointers[y] = (uint8_t*)(output + y*width());
    }

    png_read_image(m_impl->png_ptr, row_pointers);

    if (setjmp(png_jmpbuf(m_impl->png_ptr))){ THROW("Error during end of read"); }

    png_read_end(m_impl->png_ptr, m_impl->info_ptr);

    // START HACK
    png_destroy_read_struct(&m_impl->png_ptr, &m_impl->info_ptr, nullptr);
    // END HACK
}

// Code from http://www.webmproject.org/docs/vp8-sdk/example__simple__decoder.html
vp8::vp8()
{
    // INFO("VPX interface: %s", vpx_codec_iface_name(VPX_INTERFACE));

    m_vpx_codec = new vpx_codec_ctx_t;

    vpx_codec_err_t  res = vpx_codec_dec_init(m_vpx_codec, VPX_INTERFACE, nullptr, 0);
    VPX_CODEC_THROW(m_vpx_codec, res, "failed to initialize the codec");
}

vp8::~vp8()
{
    if (m_vpx_codec)
    {   
        vpx_codec_err_t  res = vpx_codec_destroy(m_vpx_codec);
        VPX_CODEC_THROW(m_vpx_codec, res, "error destroying vpx codec");
    }
    delete m_vpx_codec;
}

void vp8::decode_depth(DepthPixel* output,
                       const uint8_t* input,
                       const size_t output_size_in_bytes,
                       const size_t input_buffer_size_in_bytes)
{
    vpx_codec_err_t res = vpx_codec_decode(m_vpx_codec, input, input_buffer_size_in_bytes, nullptr, 0);
    VPX_CODEC_THROW(m_vpx_codec, res, "error decoding frame");

    // Get a YV12 image frame from the decoder
    vpx_codec_iter_t  iter = NULL;
    vpx_image_t      *img;

    img = vpx_codec_get_frame(m_vpx_codec, &iter);

    ASSERT(vpx_codec_get_frame(m_vpx_codec, &iter) == nullptr);

    ASSERT_EQUAL(img->d_w, width());
    ASSERT_EQUAL(img->d_h, height());

    const uint8_t* y_plane = img->planes[VPX_PLANE_Y];
    const uint8_t* u_plane = img->planes[VPX_PLANE_U];
    const uint8_t* v_plane = img->planes[VPX_PLANE_V];

//  static bool printed = false;
//  if (!printed)
//  {
//      INFO("y stride=%d, u stride = %d, v stride = %d",
//           img->stride[VPX_PLANE_Y], img->stride[VPX_PLANE_U], img->stride[VPX_PLANE_V]);
//      printed = true;
//  }
    int ret =
    libyuv::I420ToRAW(y_plane, img->stride[VPX_PLANE_Y],
                      u_plane, img->stride[VPX_PLANE_U],
                      v_plane, img->stride[VPX_PLANE_V],
                      work_buf(), output_stride_in_pixels() * sizeof(RGB888Pixel),
                      width(), height() );
    ASSERT(ret == 0);

    decode_to_depth(output, (const RGB888Pixel*)work_buf());
}


}; // namespace depth

}; // namespace decoder

}; // namespace holo3d

