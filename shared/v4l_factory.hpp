#ifndef _V4L2_FACTORY_HPP_
#define _V4L2_FACTORY_HPP_

#include "factory_base.hpp"

namespace holo3d
{

class v4l_factory : public factory_base
{
public:
    v4l_factory();
    virtual ~v4l_factory();

    virtual factory_base::uri_vec_t device_uris() override;
    virtual device_ptr_t device(int designed_fps, int desired_width, std::string uri) override;

    STREAM_FORMAT color_format() const override { return STREAM_FORMAT::COLOR_YUYV; }
    STREAM_FORMAT depth_format() const override { return STREAM_FORMAT::INVALID; }

    int default_fps() const override { return 30; }
    int default_width() const override { return 640; }

protected:
    uri_vec_t m_cached_uris;
};

}; // namespace holo3d

#endif // _V4L2_FACTORY_HPP_
