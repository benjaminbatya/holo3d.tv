#ifndef _UVC_FACTORY_HPP_
#define _UVC_FACTORY_HPP_

#include <libuvc/libuvc.h>

#include "factory_base.hpp"

namespace holo3d
{

// Documentation at https://int80k.com/libuvc/doc/index.html
class uvc_factory : public factory_base
{
public:
    uvc_factory();
    ~uvc_factory();

    virtual bool device_uris(uri_vec_t &uris) override;
    virtual device_ptr_t device(int desired_width, std::string uri) override;

protected:
    uvc_context_t*      m_ctx = nullptr;
};

}; // namespace holo3d

#endif // _UVC_FACTORY_HPP_
