#ifndef _FACTORY_BASE_HPP_
#define _FACTORY_BASE_HPP_

#include "device_base.hpp"

namespace holo3d
{

// From http://stackoverflow.com/questions/14323595/best-way-to-declare-an-interface-in-c11
class factory_base
{
public:
    virtual ~factory_base() = default;

    typedef std::vector<std::string> uri_vec_t;
    virtual uri_vec_t device_uris() = 0;

    typedef std::shared_ptr<device_base>    device_ptr_t;
    virtual device_ptr_t device(int designed_fps, int desired_width, std::string uri = "") = 0;

    virtual STREAM_FORMAT color_format() const = 0;
    virtual STREAM_FORMAT depth_format() const = 0;

    virtual int default_fps() const = 0;
    virtual int default_width() const = 0;

};

}; // namespace holo3d

#endif // _FACTORY_BASE_HPP_
