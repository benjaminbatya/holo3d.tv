extern crate getopts;
extern crate rand;
extern crate time;

use std::net::{UdpSocket, ToSocketAddrs, SocketAddr};
use std::u16;
use std::env;
use std::mem::size_of_val;

use getopts::Options;
use rand::Rng;
use time::PreciseTime;

const MAX_SIZE : usize = u16::MAX as usize / 2;

fn main() {

    let address = "127.0.0.1:5555".to_string();
    let num_messages : u16 = 100;
    let message_size : u16 = 16;

    let args: Vec<String> = env::args().collect();

    let program = args[0].clone();

    let mut opts = Options::new();

    opts.optflag("h", "help", "print this help menu");
    opts.optflag("s", "server", "Indicates that this should run as a server");
    opts.optflag("c", "client", "Indicates that this should run as a client");
    opts.optopt("a", "address", "The address to use", &address);
    opts.optopt("n", "num_messages", "Number of messages to send", &num_messages.to_string());
    opts.optopt("m", "message_size", "Size of each message to send", &message_size.to_string());

    let matches = match opts.parse(&args[1..])
    {
        Ok(m) => m,
        Err(f) => { panic!(f.to_string()) }
    };

    if matches.opt_present("h")
    {
        print_usage(&program, opts);
        return;
    }
    let is_server = matches.opt_present("s");
    let is_client = matches.opt_present("c");
    let address = match matches.opt_default("a", &address)
    {
        Some(x) => x,
        None => address,
    };
    let num_messages : u16 = match matches.opt_default("n", &num_messages.to_string())
    {
        Some(x) => x.parse::<u16>().unwrap(),
        None => num_messages,
    };
    let message_size : u16 = match matches.opt_default("m", &message_size.to_string())
    {
        Some(x) => x.parse::<u16>().unwrap(),
        None => message_size,
    };

    println!("Program:{}, is_server:{}, is_client:{}, address:{}, num_messages:{}, message_size:{}", 
        program, is_server, is_client, address, num_messages, message_size);

    if is_server { run_server(address); }
    else if is_client { run_client(address, num_messages, message_size); }
    else 
    { 
        print_usage(&program, opts);
    }

    // println!("Done!");
}

fn print_usage(program: &str, opts: Options)
{
    let brief = format!("Usage: {} -s|-c [options] ", program);
    print!("{}", opts.usage(&brief));
}

fn run_server(address: String)
{
    let socket = UdpSocket::bind(&*address).unwrap();

    let buf = &mut [0u8; MAX_SIZE];

    println!("{} bytes, MAX_SIZE={}", size_of_val(buf), MAX_SIZE);

    loop 
    {
        let (amt, src) = socket.recv_from(buf).unwrap();

        assert!(amt <= MAX_SIZE);

        // println!("Received {} bytes from {}", amt, src);
        let buf_send = &mut buf[..amt];
        
        /*let amt = */
        socket.send_to(buf_send, &src).unwrap(); // ok().expect("Send failed!");

        // println!("{:?}", amt);

        // Do we need this?
        // drop(socket);
    }
}

fn run_client(address: String, num_messages: u16, message_size: u16)
{
    let addrs: Vec<_> = address.to_socket_addrs().unwrap().collect();
    // println!("{:?}", addrs);

    let addr = addrs[0];
    // println!("{:?}", addr);

    let socket = UdpSocket::bind("0.0.0.0:0").unwrap();
    // let local_addr : SocketAddr = socket.local_addr().unwrap();
    // println!("{:?}", local_addr);

    let message_size = message_size as usize;

    let mut buf = vec![0u8; message_size];
    // let buf2 = &mut buf[..message_size];

    let mut rng = rand::thread_rng();
    for i in 0..message_size
    {
        buf[i] = rng.gen::<u8>();
    }

    let mut temp_buf = buf.clone();
    let send_buf = &mut temp_buf[..];

    let start = PreciseTime::now();

    for _ in 0..num_messages
    {
        let _bytes_sent = socket.send_to(send_buf, addr).unwrap();
        let (_bytes_recv, _) = socket.recv_from(send_buf).unwrap();

        // assert_eq!(_bytes_sent, _bytes_recv);
    }

    let end = PreciseTime::now();

    assert_eq!(buf, send_buf);

    let elapsed = start.to(end).num_microseconds().unwrap();

    println!("elapsed time = {} usecs", elapsed);

    let latency = elapsed / (num_messages as i64) / 2;

    println!("latency = {} usecs", latency);
}