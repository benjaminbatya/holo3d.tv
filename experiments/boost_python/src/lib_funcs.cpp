#include <string>
#include <boost/python.hpp>
using namespace boost::python;

namespace { // Avoid cluttering the global namespace.

    // A couple of simple C++ functions that we want to expose to Python.
    std::string greet() { return "hello, world"; }
    int square(int number) { return number * number; }

    class World
    {
      std::string m_msg;

    public:
      World(std::string msg) : m_msg(msg) { }
      void set(std::string msg) { m_msg = msg; }
      std::string greet() { return m_msg; }

      float m_value = 42.0f;
    };

    class Complex
    {
      float m_real = 0.0f;
      float m_img = 0.0f;

    public:

      Complex(float real = 0.0f, float img = 0.0f) : m_real(real), m_img(img) { }
      float real() const { return m_real; }
      void  real(float real) { m_real = real; }
      float img() const { return m_img; }
      void  img(float img) { m_img = img; }

      Complex operator+(const Complex& b)
      {
          Complex ret(real()+b.real(), img()+b.img());
          return ret;
      }

      std::string str()
      {
          std::ostringstream os;
          os << "(" << real() << "," << img() << ")";
          return os.str();
      }
    };

    #define INFO(msg) std::cout << msg << std::endl;

    struct Base
    {
      Base() { INFO("Base::Base: called"); }
      virtual ~Base() { INFO("Base::~Base: called"); }

      virtual int f() { return 0; }
    };

    struct Derived : Base
    {
      Derived() { INFO("Derived::Derived: called"); }
      virtual ~Derived() { INFO("Derived::~Derived: called"); }

      int f() { return 42; }
    };

    void b(Base*) { INFO("b: called"); }
    void d(Derived*) { INFO("d: called"); }

    Base* factory() { INFO("factory: called"); return new Derived; }
}

BOOST_PYTHON_MODULE(holo_funcs)
{
    // Add regular functions to the module.
    def("greet", greet);
    def("square", square);

    class_<World>("World", init<std::string>())
        .def("greet", &World::greet)
        .def("set", &World::set)
        .def_readwrite("value", &World::m_value)
    ;

    float (Complex::*get_real)(void) const = &Complex::real;
    void  (Complex::*set_real)(float) = &Complex::real;
    float (Complex::*get_img)(void) const = &Complex::img;
    void  (Complex::*set_img)(float) = &Complex::img;

    class_<Complex>("Complex", init<optional<float, float>>())
        .add_property("real", get_real, set_real)
        .add_property("img", get_img, set_img)
        .def(self + self)
        .def("__str__", &Complex::str)
    ;

    struct BaseWrap : Base, wrapper<Base>
    {
        int f()
        {
            if (override f = this->get_override("f"))
            {
                return f();
            }
            return Base::f();
        };

        int default_f() { return this->Base::f(); }
    };

    class_<BaseWrap, boost::noncopyable>("Base")
        .def("f", &Base::f, &BaseWrap::default_f)
    ;

    class_<Derived, bases<Base>>("Derived")
    ;

    def("b", b);
    def("d", d);
    def("factory", factory, return_value_policy<manage_new_object>());
}



