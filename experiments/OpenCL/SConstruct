# Sconstruct for OpenCL. Different configs are Debug-x86, Release-x86, Debug-Arm and Release-Arm

import os

build_all = False

try:
    Import('env')
    if(env['CFG']):
        cfg = env['CFG']
        build_all = True
except: 
    None # Do nothing

if not build_all:
    cfg = ARGUMENTS.get('CFG', 'Debug-x86')

cfg = cfg.split('-')
assert len(cfg) > 0
config = cfg[0]

platform = 'x86'
if(len(cfg) > 1):
    platform = cfg[1]

env = Environment()

BUILD_PATH = config + '-' + platform

env['CFG']=BUILD_PATH
shared_lib = env.SConscript('../../shared/SConstruct', exports='env')
env.Append(LIBS = [shared_lib])

env.Append(CPPFLAGS = ['-Wall', '-std=c++11', '-DPLATFORM_'+platform])
env.Append(CPPPATH = ['../../../shared', '.'])
env.Append(LIBS = ['OpenCL', 'boost_program_options', 'opencv_core', 'opencv_imgproc', 'opencv_objdetect', 'opencv_highgui']) #'boost_system', 'boost_thread', 'boost_filesystem', 'OpenNI2', 'vpx', 'yuv', 'turbojpeg'])

if(platform == 'x86'):
    env.Replace(CXX = 'g++')
    env.Replace(LINK = 'g++')
    env.Append(CPPPATH = ['/usr/local/include', os.environ['AMDAPPSDKROOT']+'/include'])
elif(platform == 'Arm'):
    env.Replace(CXX = 'arm-linux-gnueabihf-g++')
    env.Append(CPPFLAGS = ['-mfpu=neon', '-mfloat-abi=hard'])
    env.Replace(LINK = 'arm-linux-gnueabihf-g++')
    env.Append(CPPPATH = Split( '''
            /usr/arm-linux-gnueabihf/include
            /usr/arm-linux-gnueabihf/include/c++/4.8.2
            /usr/arm-linux-gnueabihf/include/c++/4.8.2/arm-linux-gnueabihf
                                ''') )
    env.Append(LIBPATH = Split('''
            /usr/arm-linux-gnueabihf/lib
                               ''') )
else:
    raise Exception('Unknown platform: ' + platform)

if(config == 'Debug'):
    env.Append(CPPFLAGS = '-g')
    env.Append(LINKFLAGS = '-g')
elif(config == 'Release'):
    env.Append(CPPFLAGS = Split('-O3 -DNDEBUG'))
elif(config == 'Profile'):
    env.Append(CPPFLAGS = Split('-O3 -pg -DNDEBUG'))
    env.Append(LINKFLAGS = '-pg')
else:
    raise Exception('Unknown configuration: ' + config)

main = env.SConscript('src/SConscript', variant_dir=BUILD_PATH, duplicate=0, exports='env')

print "Building {0} in {1} configuration".format(main[0], config)

# Setup the aliases for run
if not build_all:
    if(platform == 'x86'):
        run = env.Command("foo", main[0].get_path(), "scp $SOURCE 192.168.1.103:")
        
    else:
        run = env.Command("foo", main[0].get_path(), 'scp -P 38080 $SOURCE localhost:')

    run = env.Alias('run', run)

env.Default(main)

Return('main')
