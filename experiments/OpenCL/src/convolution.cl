__kernel void convolution(__read_only  image2d_t input_image,
                          __write_only image2d_t output_image,
                          __constant float* filter,
                          int filter_width)
{
    const sampler_t sampler = 
          CLK_NORMALIZED_COORDS_FALSE
        | CLK_ADDRESS_CLAMP_TO_EDGE
        | CLK_FILTER_NEAREST;

    // Get the work item's coordinates
    int x = get_global_id(0);
    int y = get_global_id(1);

    // Half the width of the filter is needed for indexing memory later...
    int half_width = (int)(filter_width / 2);

    // All accesses to images return data as four imae vectors (float4)
    float4 sum = {0.0f, 0.0f, 0.0f, 0.0f};

    int filter_idx = 0;

    int2 coords;

    // Iterate across the filter rows
    for(int i = -half_width; i <= half_width; i++)
    {
        coords.x = x + i;

        // Iterate across the filter columns
        for(int j = -half_width; j <= half_width; j++)
        {
            coords.y = y + j;

            float4 pixel;

            pixel = read_imagef(input_image, sampler, coords);
            sum.x += pixel.x * filter[filter_idx++];
            // sum.y += pixel.y * filter[filter_idx++];
            // sum.z += pixel.z * filter[filter_idx++];
        }
    }

    barrier(CLK_GLOBAL_MEM_FENCE);

    // Copy the data if its in bounds
    if(y < get_image_height(input_image) &&
       x < get_image_width(input_image))
    {
        coords.x = x;
        coords.y = y;

        write_imagef(output_image, coords, sum);
    }
}
