__kernel void bsort_init(__global float4* g_data, __local float4* l_data )
{
    // Do nothing for now...
}

__kernel void bsort_stage_0(__global float4* g_data, __local float4* l_data, uint high_stage )
{
    // Do nothing for now...
}

__kernel void bsort_stage_n(__global float4* g_data, __local float4* l_data, uint stage, uint high_stage )
{
    // Do nothing for now...
}

__kernel void bsort_merge(__global float4* g_data, __local float4* l_data, uint stage, int dir )
{
    // Do nothing for now...
}

__kernel void bsort_merge_last(__global float4* g_data, __local float4* l_data, int dir )
{
    // Do nothing for now...
}
