#include <vector>
#include <algorithm>
#include <iostream>
#include <boost/format.hpp>
#include <boost/compute.hpp>

#include "util_funcs.hpp"

namespace compute = boost::compute;


void test_sort(const size_t num_elems, 
               boost::compute::context& ctx, 
               boost::compute::command_queue& queue, 
               boost::compute::kernel& kernel)
/**
 * 
 * 
 * @author benjamin (1/23/2015)
 */
{ 
    std::vector<float> host_vector(num_elems); 
    std::generate(host_vector.begin(),  host_vector.end(), rand); 
    
    boost::compute::vector<float> device_vector(host_vector.size(),  ctx); 
    
    std::vector<float> result_vector(device_vector.size()); 
    
    uint64_t start_time = get_time(); 
    
    boost::compute::copy(host_vector.begin(), host_vector.end(),  
                         device_vector.begin(),  queue); 
    
    boost::compute::sort(device_vector.begin(), device_vector.end(), queue); 
    
    boost::compute::copy(device_vector.begin(), device_vector.end(), 
                         result_vector.begin(), queue); 
    queue.finish();
    uint64_t gpu_delta = get_time() - start_time; 
    
    boost::compute::vector<float> device_vector2(host_vector.size(),  ctx); 

    start_time = get_time(); 
    



//  kernel.set_arg(0, host_vector);
//
//  queue.enqueue_nd_range_kernel(kernel, )
//
    

//  start_time = get_time();
//  std::sort(host_vector.begin(), host_vector.end());
//  uint64_t cpu_delta = get_time() - start_time;
//
//  for (size_t i = 0; i < host_vector.size(); i++)
//  {
//      //      std::cout   << "idx=" << i << ": host=" << host_vector.at(i)
//      //                  << ", result=" << device_vector.at(i) << std::endl;
//      if (host_vector.at(i) != result_vector.at(i))
//      {
//          std::cerr << "DOESNT match at index " << i << std::endl;
//          return;
//      }
//  }
    
    INFO("num_elems=%d: GPU time delta = %d", num_elems, gpu_delta);
    
}
int main()
{
    try
    {
        boost::compute::device gpu = boost::compute::system::default_device();

        boost::compute::context ctx(gpu);
        boost::compute::command_queue queue(ctx, gpu);

#define PROGRAM_FILE       "bsort.cl"
#define BSORT_INIT         "bsort_init"
#define BSORT_STAGE_0      "bsort_stage_0"
#define BSORT_STAGE_N      "bsort_stage_n"
#define BSORT_MERGE        "bsort_merge"
#define BSORT_MERGE_LAST   "bsort_merge_last"

        compute::program program;

        program = compute::program::create_with_source_file(PROGRAM_FILE,  ctx);
   
        program.build();
        
        boost::compute::kernel kernel_init(program, BSORT_INIT);
        boost::compute::kernel kernel_stage_0(program, BSORT_STAGE_0);
        boost::compute::kernel kernel_stage_n(program, BSORT_STAGE_N);
        boost::compute::kernel kernel_merge(program, BSORT_MERGE);
        boost::compute::kernel kernel_merge_last(program,  BSORT_MERGE_LAST);

        size_t local_size = gpu.max_work_group_size();

        INFO("local_size = %d", local_size);

        size_t num_elems = 10;

        std::vector<float> host_vector(num_elems);
        std::generate(host_vector.begin(),  host_vector.end(), rand);

        // Create a buffer
        boost::compute::vector<float> device_vector(host_vector.size(),  ctx); 

        kernel_init.set_arg(0, device_vector);
        kernel_stage_0.set_arg(0, device_vector);
        kernel_stage_n.set_arg(0, device_vector);
        kernel_merge.set_arg(0, device_vector);
        kernel_merge_last.set_arg(0, device_vector);

        kernel_init.set_arg(1, compute::local_buffer<float>(8*local_size));
        kernel_stage_0.set_arg(1, compute::local_buffer<float>(8*local_size));
        kernel_stage_n.set_arg(1, compute::local_buffer<float>(8*local_size));
        kernel_merge.set_arg(1, compute::local_buffer<float>(8*local_size));
        kernel_merge_last.set_arg(1, compute::local_buffer<float>(8*local_size));

        size_t global_size = num_elems / 8;
        if (global_size < local_size) 
        {
            local_size = global_size;
        }

        // Enqueue the first kernel
        queue.enqueue_nd_range_kernel(kernel_init, 1, )

        fixed this later...

        return 0;

    //  size_t num_elems = 10;
    //
    //  std::vector<float> host_vector(num_elems);
    //  std::generate(host_vector.begin(),  host_vector.end(), rand);
    //
    //  kernel.set_arg(0, host_vector);
    //
    //  queue.enqueue_nd_range_kernel(kernel, )


    //  size_t num_elems = 1;
    //
    //  for (size_t i=0; i<8; i++)
    //  {
    //      num_elems <<= 3;
    //      test_sort(num_elems, ctx, queue, kernel);
    //  }

    } catch(boost::compute::opencl_error e)
    {
        ERROR("Program failure: %s", e);
        return -1;
    }

    return 0;
}

