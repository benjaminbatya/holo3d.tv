#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable

__constant char hw[] = "Hello World\n";

__kernel void hello(__global char * out)
{
    // This is a comment
    /*
    Regular block comment
     */

    size_t tid = get_global_id(0);

    out[tid] = hw[tid];
}
