#define __CL_ENABLE_EXCEPTIONS

// We are only using OpenCL 1.1 for now. It's the only distributed version 
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS
#include <CL/cl.h>
#undef CL_VERSION_1_2
#include <CL/cl.hpp>

#include <string>
#include <iostream>
#include <assert.h>

using namespace std;

int main(void)
{
    std::vector<cl::Device> platform_devices, all_devices, ctx_devices;

    try
    {
        cl_int err;
        cl_uint platformCount;
        err = ::clGetPlatformIDs(0, NULL, &platformCount);

        cerr << err <<endl;

        assert(err == CL_SUCCESS);
        assert(platformCount > 0 && "clGetPlatformIDs: no platforms detected");

        cl::Platform platform = cl::Platform::get();
        
        err = platform.getDevices(CL_DEVICE_TYPE_ALL, &platform_devices);
        assert(err == CL_SUCCESS && "Failed to get devices");

        cl::Context context(platform_devices);
        ctx_devices = context.getInfo<CL_CONTEXT_DEVICES>();

        for (cl_uint i=0; i<ctx_devices.size(); i++)
        {
            std::string device_name = ctx_devices[i].getInfo<CL_DEVICE_NAME>();
            cout << "Device: " << device_name << endl;
        }

    } catch(cl::Error& e)
    {
        cerr << "Error " << e.what() << ": code=" << e.err() << endl;
    };
}

