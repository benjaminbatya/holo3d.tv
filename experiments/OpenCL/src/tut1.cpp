#include <utility>

#include <CL/cl.hpp>

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <iterator>

const std::string hw("Hello World\n");

inline void
check_error(cl_int err, const char* name)
{
    if (err != CL_SUCCESS) 
    {
        std::cerr << "Error: " << name << " (" << err << ")" << std::endl;
        exit(EXIT_FAILURE);
    }
}

int main(void)
{
    cl_int err;

    std::vector<cl::Platform> platform_list;
    cl::Platform::get(&platform_list);
    check_error(platform_list.size() != 0 ? CL_SUCCESS : -1, "cl::Platform::get");
    
    std::cout <<"Platform number is: " << platform_list.size() << std::endl;

    std::string platform_vendor;

    platform_list[0].getInfo((cl_platform_info)CL_PLATFORM_VENDOR, &platform_vendor);

    std::cerr << "Platform is by: " << platform_vendor << std::endl;

    cl_context_properties cprops[3] = {CL_CONTEXT_PLATFORM, (cl_context_properties)(platform_list[0])(), 0};

    cl::Context context(CL_DEVICE_TYPE_CPU, cprops, nullptr, nullptr, &err);
    check_error(err,"cl::Context");

    char * out_hello = new char [hw.length() + 1];
    cl::Buffer out_cl(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, hw.length()+1, out_hello, &err);
    check_error(err, "Buffer::Buffer()");

    std::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
    check_error(devices.size() > 0 ? CL_SUCCESS : -1, "devices.size() > 0");

    std::ifstream file("tut1_kernels.cl");
    check_error(file.is_open() ? CL_SUCCESS : -1, "opening kernel");

    std::string prog((std::istreambuf_iterator<char>(file)),
                     (std::istreambuf_iterator<char>()));
    cl::Program::Sources source(1, std::make_pair(prog.c_str(), prog.length()+1));

    cl::Program program(context, source);

    err = program.build(devices, "");
    check_error(err, "Program::build()");

    cl::Kernel kernel(program, "hello", &err);
    check_error(err, "Kernel()");

    err = kernel.setArg(0, out_cl);
    check_error(err, "setArg()");

    cl::CommandQueue queue(context, devices[0], 0, &err);
    check_error(err, "commandqueue()");

    cl::Event event;
    err = queue.enqueueNDRangeKernel(kernel, 
                                     cl::NullRange, 
                                     cl::NDRange(hw.length()+1),
                                     cl::NDRange(1, 1),
                                     nullptr,
                                     &event);
    check_error(err, "enqueueNDRangeKernel()");

    event.wait();

    err = queue.enqueueReadBuffer(out_cl, CL_TRUE, 0, hw.length()+1, out_hello);
    check_error(err, "enqueueReadBuffer()");

    std::cout << out_hello;
    return EXIT_SUCCESS;
}
