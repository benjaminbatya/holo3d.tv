#include <iostream>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <boost/compute/system.hpp>
#include <boost/compute/interop/opencv/core.hpp>
#include <boost/compute/interop/opencv/highgui.hpp>
#include <boost/compute/utility/source.hpp>

#include <boost/program_options.hpp>

namespace compute = boost::compute;
namespace po = boost::program_options;

#include <boost/format.hpp>
#include "util_funcs.hpp"
#include "fps_counter.hpp"

// this was generated from convolution.cl with 
// 'echo "BOOST_COMPUTE_STRINGIZE_SOURCE (" > $TARGET && cat $SOURCE >> $TARGET && echo ")" >> $TARGET'
const char* source = 
#include "convolution.cl.h"
;

int main(int argc, char** argv)
{
    po::options_description desc;
    desc.add_options()
    ("help",                                        "show available options")
    ("camera", po::value<int>()->default_value(-1), "if not default camera, specify a camera id")
    ("image",  po::value<std::string>(),            "path to image file")
    ;

    // parse commandline
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) 
    {
        std::cout << desc <<std::endl;
        return 0;
    }

    cv::Mat matrix;

    cv::VideoCapture capture;

    // Filter
    int filter_width = 3;
    float filter[9] = {
        -1.0, 0.0, 1.0,
        -2.0, 0.0, 2.0,
        -1.0, 0.0, 1.0
    };

    // Create all of the opencl objects needed
    compute::device gpu = compute::system::default_device();
    compute::context context(gpu);
    compute::command_queue queue(context, gpu);

    compute::buffer dev_filter(context, sizeof(filter),
                               compute::memory_object::read_only |
                               compute::memory_object::copy_host_ptr,
                               filter);

    compute::program filter_program = compute::program::create_with_source(source, context);

    try
    {
        filter_program.build();
    } catch(compute::opencl_error e)
    {
        std::cerr << "Build error: " << std::endl << filter_program.build_log();
        return -1;
    }

    compute::kernel filter_kernel(filter_program, "convolution");

    // check args
    if (vm.count("image")) 
    {
        std::string image_file = vm["image"].as<std::string>();
        matrix = cv::imread(image_file, CV_LOAD_IMAGE_COLOR);
        if (!matrix.data) 
        {
            std::cerr << "Failed to load image: " << image_file << std::endl;
            return -1;
        }
    } else
    {
        capture.open(vm["camera"].as<int>());

        capture >> matrix;
        if (!matrix.data) 
        {
            std::cerr << "Failed to capture frame" << std::endl;
            return -1;
        }
    }

    cv::cvtColor(matrix, matrix, CV_BGR2RGBA);

    // transfer the image data to the gpu
    compute::image2d dev_input_image = 
        compute::opencv_create_image2d_with_mat(matrix, compute::image2d::read_write, queue);

    INFO("dims = %dx%d", dev_input_image.width(), dev_input_image.height());

    compute::image2d dev_output_image(context,
                                      compute::image2d::write_only,
                                      dev_input_image.get_format(),
                                      dev_input_image.width(),
                                      dev_input_image.height());

    filter_kernel.set_arg(0, dev_input_image);
    filter_kernel.set_arg(1, dev_output_image);
    filter_kernel.set_arg(2, dev_filter);
    filter_kernel.set_arg(3, filter_width);

    // setup the kernel
    size_t origin[2] = { 0, 0 };
    size_t region[2] = { dev_input_image.width(), dev_input_image.height() };

    queue.enqueue_nd_range_kernel(filter_kernel, 2, origin, region, 0);

    if (vm.count("image")) 
    {
        // Just show the host and convolved images
        cv::imshow("Original Image", matrix);

        compute::opencv_imshow("Convoluted Image", dev_output_image, queue);

        cv::waitKey(0);
    } else
    {
        char key = '\0';

        uint64_t time = 0;
        uint64_t frames = 0;
        holo3d::fps_counter fps;
        while (key != 27) 
        {
            capture >> matrix;
            if (matrix.data) 
            {  
                cv::cvtColor(matrix, matrix, CV_BGR2BGRA);

                uint64_t start_time = get_time();    
                compute::opencv_copy_mat_to_image(matrix, dev_input_image, queue);

                queue.enqueue_nd_range_kernel(filter_kernel, 2, origin, region, 0);

                queue.finish();
                time += get_time() - start_time;

                // Just show the host and convolved images
                cv::imshow("Original Image", matrix);

                compute::opencv_imshow("Convoluted Image", dev_output_image, queue);
            }
            frames++;
            if (frames >= 30) 
            {
                INFO("Average time to process=%d", (time/frames));
                frames = 0;
                time = 0;
            }   
            fps.update();
            key = cv::waitKey(1);
        }
    }

    return 0;
}



