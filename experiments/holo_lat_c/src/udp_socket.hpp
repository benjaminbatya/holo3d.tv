#ifndef __HOLO3D_UDP_SOCKET_HPP__
#define __HOLO3D_UDP_SOCKET_HPP__

#include <stdint.h>
#include <string>

struct sockaddr_in;

namespace holo3d
{

namespace socket
{

// NOTE: This is just IPv4 for now...
class endpoint
{
public:
    endpoint();
    endpoint(uint32_t address, uint16_t port);
    endpoint(std::string address, uint16_t port = 0);
    endpoint(const sockaddr_in& addr);

//  uint32_t addr() const { return m_addr; }
//  uint16_t port() const { return m_port; }

    std::string str_addr() const;
    std::string full_addr() const;

    void extract_sockaddr(sockaddr_in& sock) const;

    uint16_t port() const { return m_port; }

protected:

    uint32_t m_addr;
    uint16_t m_port;
};

std::ostream& operator<<(std::ostream& os, const endpoint& ep);

class base_socket
{
public:
    virtual ~base_socket();

    endpoint local_address();

//  static uint16_t resolve_service(const std::string& service, const string& protocol = "udp");

private:
    // Prevent value semantics on this class
    base_socket(const base_socket&);
    void operator=(const base_socket& sock);

protected:
    base_socket(int type, int protocol);

    int sock_descriptor;
};

class udp_socket : public base_socket
{
public:
    udp_socket();

    void disconnect();

    void send(const void* buffer, size_t buffer_size);
    void send_to(const void* buffer, size_t buffer_size, const endpoint& endp);

    size_t receive(void* buffer, size_t buffer_size);
    size_t receive_from(void* buffer, size_t buffer_size, endpoint& endp);

    // figure out the multi-cast stuff later...
};

class udp_receiver : public udp_socket
{
public:

    void connect(const endpoint& local_endpoint);
};

class udp_sender : public udp_socket
{
public:

    void connect(const endpoint& remote_endpoint, bool broadcast = false);
};

} // namespace socket

} // namespace holo3d



#endif // __HOLO3D_UDP_SOCKET_HPP__
