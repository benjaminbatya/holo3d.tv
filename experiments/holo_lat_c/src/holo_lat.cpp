
#include "pch_files.hpp"

#include <stdint.h>
#include <memory>
#include <sys/mman.h>

#ifdef CFG_PROFILE
#   include <gperftools/profiler.h>
#endif

#include "util_funcs.hpp"
#include "h3t_file.hpp"

//#define ERROR(...) ((void)(0));
//#define ERROR_CPP(...) ((void)(0));
//#define INFO(...) ((void)(0));
//#define INFO_CPP(...) ((void)(0));
//#define ASSERT_THROW(...) ((void)(0));

//#include <zmq.h>
//#include <zmq_utils.h>

#include "udp_socket.hpp"

namespace bpo = boost::program_options;
namespace bfs = boost::filesystem;
namespace hs = holo3d::socket;

const std::string server_arg = "-s";
const std::string client_arg = "-c";

const uint32_t MAX_BUF_SIZE = 65536;

void print_help(const char* prog_name)
{
    INFO("Usage: %s -s|-c address port [size_of_packet] [num_packets]\n", prog_name);
}

void set_prio(int prio);

void run_server(const hs::endpoint& endpoint);
int run_client(const hs::endpoint& endpoint, uint32_t size_of_packet, uint32_t num_packets);

//void run_zmq_server(const hs::endpoint& endpoint);
//void run_zmq_client(const hs::endpoint& endpoint, uint32_t size_of_packet, uint32_t num_packets);

class Timer
{
public:
    Timer() { clock_gettime(CLOCK_REALTIME, &beg_); }

    // Returns the number of microseconds that have expired since creation or reset
    long elapsed() {
        clock_gettime(CLOCK_REALTIME, &end_);
        return long((end_.tv_sec - beg_.tv_sec)*1000000) + 
            long((end_.tv_nsec - beg_.tv_nsec) / 1000);
    }

    void reset() { clock_gettime(CLOCK_REALTIME, &beg_); }

private:
    timespec beg_, end_;
};

int main (int argc, char *argv[])
{
    // Args are in the form
    // ./holo_lat (-s|-c) address port -z size_of_packet num_cycles
    // NOTE: The server must be started before the client to get accurate latency numbers

    bool is_server = false;

    std::string host          = "127.0.0.1:5555"; // + holo3d::DEFAULT_PORT;

    std::string app_name            = bfs::basename(argv[0]);

    uint32_t    message_size        = 16;
    uint32_t    num_messages        = 100;

    bpo::options_description visible(std::string("Usage: ") + app_name + " <options> \nAllowed Options");
    visible.add_options()
    ("help,h",                                                                                          "Display this help message")
    ("client,c",                                                                                        "Do the ntpdate before running the camera, only enabled on the ARM platform")
    ("server,s",                                                                                        "The port this camera should listen for messages on")
    ("addr,a",          bpo::value<std::string>(&host)->default_value(host),                             "The address in string format \"address:port\" format")
    ("message_size,m",  bpo::value<uint32_t>(&message_size)->default_value(message_size),                "The size of the message to send")
    ("num_messages,n",  bpo::value<uint32_t>(&num_messages)->default_value(num_messages),                "The number of messages to send")
    ;

    bpo::options_description all;
    all.add(visible);

    int ret_val = 0;

    bpo::variables_map vm;
    try
    {
        bpo::store(bpo::command_line_parser(argc, argv).options(all).run(), vm);

        if (vm.count("help"))
        {
            INFO_CPP(visible);
            return 0;
        }

        bool is_client = vm.count("client");
        is_server = vm.count("server");

        if ((is_client ^ is_server) == false)
        {
            ERROR("Either --client or --server must be specified!");
            INFO_CPP(visible);
            return -1;
        }

        bpo::notify(vm);
    }
    catch (bpo::error& e)
    {
        ERROR_CPP(e.what());
        INFO_CPP(visible);
        return -1;
    }
    
#ifdef CFG_PROFILE
      ProfilerStart("prof.out"); 
#endif
     
    try
    {
        hs::endpoint endpoint = hs::endpoint(host, uint16_t(stoi(holo3d::DEFAULT_PORT)));

        // Set this up to preempt if PREEMPT is available
//      set_prio(49);

        if (is_server) 
        {
//          if (use_zmq)
//          {
//              run_zmq_server(endpoint);
//          } else
            {
                run_server(endpoint); 
            }
        } else  
        {
//          if (run_zmq)
//          {
//              run_zmq_client(endpoint, size_of_packet, num_packets);
//          } else
            {
                ret_val = run_client(endpoint, message_size, num_messages); 
                INFO("ret=%1%", ret_val);
                ret_val = 0;
            }
        } 
    } catch (std::runtime_error& e)
    {
        ERROR_CPP(std::endl << "Caught runtime_error: " << e.what());
        ret_val = -1;
    } catch (std::exception& e)
    {
        ERROR_CPP(std::endl << "Caught exception: " << e.what());
        ret_val = -1;
    } catch(...)
    {
        ERROR_CPP(std::endl << "Unknown exception!!");
        ret_val = -1;
    }

#ifdef CFG_PROFILE
      ProfilerStop(); 
#endif

    return(ret_val);
}


//---------------------------------------------------------------------
// Thread priority
//---------------------------------------------------------------------

void set_prio(int prio)
{
	struct sched_param param;
	memset(&param, 0, sizeof(param));
	param.sched_priority = prio;

	int ret = sched_setscheduler(0, SCHED_FIFO, &param);
    if (ret < 0) 
    {
        WARN("sched_setscheduler failed: errno=%1%", ::strerror(errno)); 
    }
}


void run_server(const hs::endpoint& endpoint)
{
    // The server endpoint has to always be 0.0.0.0, not localhost or 127.0.0.1!
    hs::endpoint new_endpoint(0, endpoint.port());

    uint8_t buf[MAX_BUF_SIZE];

    hs::udp_receiver server;

    server.connect(new_endpoint);

//  int ret = mlockall(MCL_CURRENT | MCL_FUTURE);
//  if (ret < 0)
//  {
//      WARN("mlockall failed: errno=%1%", ::strerror(errno));
//  }

    hs::endpoint remote_endpoint;
    while (true)
    {
        size_t bytes_recved = server.receive_from(buf, MAX_BUF_SIZE, remote_endpoint);

//      INFO("received %1% bytes\n", bytes_recved);

        server.send_to(buf, bytes_recved, remote_endpoint);
    }

//  ret = munlockall();
//  if (ret < 0)
//  {
//      WARN("munlockall failed: errno=%1%", ::strerror(errno));
//  }
}


int run_client(const hs::endpoint& endpoint, uint32_t message_size, uint32_t num_messages)
{
    uint8_t orig_buf[MAX_BUF_SIZE];

    // Setup the socket
    hs::udp_sender client;
    client.connect(endpoint);

    for (uint32_t i=0; i<message_size; i++) 
    {
        orig_buf[i] = uint8_t(rand());
    }

    uint8_t* temp_buf = new uint8_t[message_size];
    ::memcpy(temp_buf, orig_buf, message_size);

    INFO("Sending %1% messages of size %2% to %3%", num_messages, message_size, endpoint);

//  int ret = mlockall(MCL_CURRENT | MCL_FUTURE);
//  if (ret < 0)
//  {
//      WARN("mlockall failed: errno=%1%", ::strerror(errno));
//  }

    Timer timer;

    for (uint32_t i=0; i<num_messages; i++) 
    {
        client.send(temp_buf, message_size);

        size_t bytes = client.receive(temp_buf, message_size);
        ASSERT_THROW(bytes == message_size, "Num bytes don't match!");
    }

    long elapsed = timer.elapsed();

//  ret = munlockall();
//  if (ret < 0)
//  {
//      WARN("munlockall failed: errno=%1%", ::strerror(errno));
//  }

    int comp_result = ::memcmp(orig_buf, temp_buf, message_size);
    ASSERT_THROW(comp_result == 0, "The temp buffer doesnt match the original buffer!");

    INFO("Total elapsed time = %u usecs", elapsed);

    long latency = elapsed / num_messages / 2;

    int lat_int = latency;

    INFO("latency = %u usecs", lat_int);

    return (lat_int);

}

//
//
//void run_zmq_server(const hs::endpoint& endpoint)
//{
////  void* ctx = zmq::socket
////  ZMQ_ASSERT(ctx, "zmq_ctx_new failed");
////
////  void* socket = zmq_socket(ctx, ZMQ_REP);
////  ZMQ_ASSERT(socket, "zmq_socket failed");
//
//
//}
//
//void run_zmq_client(const hs::endpoint& endpoint, uint32_t size_of_packet, uint32_t num_packets)
//{
//
//}
