
#include "pch_files.hpp"

#include "util_funcs.hpp"

#include "udp_socket.hpp"

//#include <sys/types.h>       // For data types
#include <sys/socket.h>      // For socket(), connect(), send(), and recv()
#include <netdb.h>           // For gethostbyname()
#include <arpa/inet.h>       // For inet_addr()
#include <unistd.h>          // For close()
#include <netinet/in.h>      // For sockaddr_in
#include <string.h>         // for strerror
//
//#include <stdexcept>

namespace holo3d
{

namespace socket
{

#define SOCK_ASSERT(expr, msg, ...) ASSERT_THROW(expr, std::string(msg) + ": errno=%s", strerror(errno));

endpoint::endpoint()
: endpoint(0, 0)
{

}

endpoint::endpoint(uint32_t addr, uint16_t port)
: m_addr(addr)
, m_port(port)
{
}

endpoint::endpoint(std::string address, uint16_t port) 
: m_port(port)
{
    size_t split_pt = address.find(':');
    if (split_pt != std::string::npos) 
    {
        std::string port_str = address.substr(split_pt+1);
        int temp_port = std::stoi(port_str);

        m_port = uint16_t(temp_port);
        address = address.substr(0, split_pt);
    }

    hostent *host = ::gethostbyname(address.c_str());
    SOCK_ASSERT(host != NULL, "Failed to resolve name (gethostbyname())");

    m_addr = ntohl(*((unsigned long *) host->h_addr_list[0]));
}

endpoint::endpoint(const sockaddr_in& addr)
: m_addr(ntohl(addr.sin_addr.s_addr))
, m_port(ntohs(addr.sin_port))
{
}

std::string endpoint::str_addr() const
{
    char buf[INET_ADDRSTRLEN];
    uint32_t net_addr = htonl(m_addr);
    inet_ntop(AF_INET, &(net_addr), buf, INET_ADDRSTRLEN);
    return buf;
}
std::string endpoint::full_addr() const
{
    std::string addr_str = str_addr();
    char temp_str[64];
    snprintf(temp_str, 64, "%s:%d", addr_str.c_str(), m_port);

    return temp_str;
}


void endpoint::extract_sockaddr(sockaddr_in& addr) const
{
    ::memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(m_addr);
    addr.sin_port = htons(m_port);
}

std::ostream& operator<<(std::ostream& os, const endpoint& ep)
{
    os << ep.full_addr();
    return os;
}

base_socket::base_socket(int type, int protocol)
{
    // Make a new socket
    this->sock_descriptor = ::socket(PF_INET, type, protocol);
    SOCK_ASSERT(this->sock_descriptor >= 0, "Socket creation failed (socket())");
}

base_socket::~base_socket()
{
    ::close(this->sock_descriptor);
    this->sock_descriptor = -1;
}

endpoint base_socket::local_address()
{
    sockaddr_in addr;
    unsigned int addr_len = sizeof(addr);

    int ret = ::getsockname(this->sock_descriptor, (sockaddr*)&addr, &addr_len);
    SOCK_ASSERT(ret >= 0, "Fetch of local address failed (getsockname())");

    endpoint addr_out(addr.sin_addr.s_addr, addr.sin_port);
    return addr_out;
}

//  static uint16_t resolve_service(const std::string& service, const string& protocol = "udp");


udp_socket::udp_socket()
: base_socket(SOCK_DGRAM, IPPROTO_UDP)
{

}

void udp_socket::disconnect()
{
    sockaddr_in nullAddr;
    memset(&nullAddr, 0, sizeof(nullAddr));
    nullAddr.sin_family = AF_UNSPEC;

    // Try to disconnect
    int ret = ::connect(this->sock_descriptor, (sockaddr*)&nullAddr, sizeof(nullAddr));
    SOCK_ASSERT(ret >= 0 /*| errno == EAFNOSUPPORT*/, "Disconnect failed (connect())");
}

void udp_socket::send(const void* buffer, size_t buffer_size)
{
    // Write out the whole buffer as a single message.
    // NOTE: The socket has to be connected beforehand
    size_t ret = ::send(this->sock_descriptor, buffer, buffer_size, 0);
    SOCK_ASSERT(ret == buffer_size, "Send failed (send())");
}

void udp_socket::send_to(const void* buffer, size_t buffer_size, const endpoint& endpoint)
{
    sockaddr_in addr;
    endpoint.extract_sockaddr(addr);
    size_t ret = ::sendto(this->sock_descriptor, buffer, buffer_size, 0, (sockaddr*)&addr, sizeof(addr));
    SOCK_ASSERT(ret == buffer_size, "Send failed (send())");
}

size_t udp_socket::receive(void* buffer, size_t buffer_size)
{
    int ret = ::recv(this->sock_descriptor, buffer, buffer_size, 0);
    SOCK_ASSERT(ret > 0, "Receive failed (recvfrom())");

    return size_t(ret);
}

size_t udp_socket::receive_from(void* buffer, size_t buffer_size, endpoint& source_addr)
{
    sockaddr_in clntAddr;
    socklen_t addrLen = sizeof(clntAddr);
//  printf("calling recvfrom\n");
    int ret = ::recvfrom(this->sock_descriptor, buffer, buffer_size, 0, (sockaddr*)&clntAddr, &addrLen);
    SOCK_ASSERT(ret > 0, "Receive failed (recvfrom())");

    source_addr = endpoint(clntAddr);
//  printf("Got %d bytes from %s", ret, source_addr.full_addr().c_str());

    return size_t(ret);
}

void udp_receiver::connect(const endpoint& endpoint)
{
    // Bind the socket to its port and any local address
    sockaddr_in localAddr;
    endpoint.extract_sockaddr(localAddr);

    int ret = ::bind(this->sock_descriptor, (sockaddr*)&localAddr, sizeof(sockaddr_in));
    SOCK_ASSERT(ret >= 0, "Set of local port failed (bind())");

    INFO("Receiver bound to %1%", endpoint);
}

void udp_sender::connect(const endpoint& endpoint, bool broadcast)
{
    // If this fails, we'll hear about it when we try to send.  This will allow 
    // system that cannot broadcast to continue if they don't plan to broadcast
    int broadcastPermission = broadcast ? 1 : 0;
    int ret = ::setsockopt(this->sock_descriptor, SOL_SOCKET, SO_BROADCAST, 
                           &broadcastPermission, sizeof(broadcastPermission));
    SOCK_ASSERT(ret >= 0, "Failed to setup broadcast socket");

    INFO("endpoint=%1%", endpoint);

    // Get the address of the requested host
    sockaddr_in destAddr;
    endpoint.extract_sockaddr(destAddr);
    
    char str[INET_ADDRSTRLEN];
    INFO("destAddr=%x:%d, broadcast=%d", 
         inet_ntop(AF_INET, &(destAddr.sin_addr), str, INET_ADDRSTRLEN), 
         ntohs(destAddr.sin_port), broadcast);
        
    // Try to connect to the given port
    ret = ::connect(this->sock_descriptor, (sockaddr*)&destAddr, sizeof(destAddr));
    SOCK_ASSERT(ret >= 0, "Connect failed (connect())");
}


}; // namespace socket

}; // namespace holo3d

