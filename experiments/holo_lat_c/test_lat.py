#!/usr/bin/python

import sys, getopt

def main(argv):    
    import os
    latencies = []

    try:
        opts, args = getopt.getopt(argv, "cgn:")
    except getopt.GetoptError:
        print("Specify -c or -g!")
        sys.exit(2)

    use_cpp, use_go = False, False
    num_msg = 100

    for opt, arg in opts:
        if opt == '-c': use_cpp = True
        elif opt == '-g': use_go = True
        elif opt == '-n': num_msg = int(arg)

    print(opts)

    # Use cpp by default
    exec_cmd = "LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CWD holo_lat_c/Release-x86/holo_lat"
    if use_go:
        exec_cmd = "go_alt/bin/holo_lat"

    for exp in xrange(1, 16):
        # print(2**size)
        size = 2**exp
        cmd = "%s -c -a camera1:5555 -m %s -n %d" % (exec_cmd, size, num_msg)
        print(cmd)
        stream = os.popen(cmd)
        output = stream.read()
        stream.close()
        
        arr = output.split('\n')
        # print(len(arr))
        
        latency = 0
        for line in arr:
            # print line
            if(line.startswith("main: ret=")):
                latency = line.split('=')[1]
                
        print("python latency = %s" % latency)
    
        latencies.append([size, int(latency)])
    
    print(latencies)
    
    import csv
    with open('latencies.csv', 'wb') as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['size', 'latency'])
        for lat in latencies:
            csv_writer.writerow(lat)
    
if __name__ == "__main__":
    main(sys.argv[1:])