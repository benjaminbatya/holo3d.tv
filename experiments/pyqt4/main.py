# -*- coding: utf-8 -*-
"""
Created on Tue Jan 13 00:34:56 2015

@author: benjamin
"""

import sys

from PyQt4 import QtCore, QtGui
import main_window

def main():
    app = QtGui.QApplication(sys.argv)
    form = main_window.Ui_MainWindow()
    form.setupUi(form)
    app.exec_()
    
if __name__ == "__main__":
    sys.exit(main())
    

