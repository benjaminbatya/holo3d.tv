/* V4L2 video picture grabber
   Copyright (C) 2009 Mauro Carvalho Chehab <mchehab@infradead.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
//#include <sys/time.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <libv4l2.h>

#include <time.h>
//#include <linux/time.h>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

struct buffer {
        void   *start;
        size_t length;
};

static void xioctl(int fh, int request, void *arg)
{
        int r;

        do {
                r = v4l2_ioctl(fh, request, arg);
        } while (r == -1 && ((errno == EINTR) || (errno == EAGAIN)));

        if (r == -1) {
                fprintf(stderr, "error %d, %s\n", errno, strerror(errno));
                exit(EXIT_FAILURE);
        }
}

int main(int argc, char **argv)
{
        struct v4l2_format              fmt;
        struct v4l2_buffer              buf;
        struct v4l2_requestbuffers      req;
        enum v4l2_buf_type              type;
        fd_set                          fds;
        struct timeval                  tv;
        int                             r, fd = -1;
        unsigned int                    i, n_buffers;
        const char*                     dev_name = "/dev/video0";
        char                            out_name[256];
        FILE                            *fout;
        struct buffer                   *buffers;

        // Find the device of interest
        fd = v4l2_open(dev_name, O_RDWR | O_NONBLOCK, 0);
        if (fd < 0) {
                perror("Cannot open device");
                exit(EXIT_FAILURE);
        }

        struct v4l2_capability          cap;

        xioctl(fd, VIDIOC_QUERYCAP, &cap);
        printf("driver=%s, card=%s, bus_info=%s\n", cap.driver, cap.card, cap.bus_info);
        printf("version=%d.%d.%d.%d\n", (cap.version>>24)&8, (cap.version>>16)&8, (cap.version>>8)&8, cap.version&8);
        printf("capabilities = %d, device_caps=%d\n", cap.capabilities & V4L2_CAP_STREAMING, cap.device_caps);

//      v4l2_fmtdesc fmt_desc;
//      fmt_desc.index = 0;
//      fmt_desc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//      do
//      {
//          r = v4l2_ioctl(fd, VIDIOC_ENUM_FMT, &fmt_desc);
//          fmt_desc.index++;
//          printf("Format = %s\n", fmt_desc.description);
//      } while (r == 0);

        fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        xioctl(fd, VIDIOC_G_FMT, &fmt);

        // Get the video capture parameters
        v4l2_streamparm param;
        param.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        xioctl(fd, VIDIOC_G_PARM, &param);

        // Set the fps to 60
        param.parm.capture.timeperframe.denominator = 60;
        xioctl(fd, VIDIOC_S_PARM, &param);

        // Set the capture format to 640x480 at RGB24 and interlaced
        // NOTE: perfer to use the native YUYV format
        CLEAR(fmt);
        fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        fmt.fmt.pix.width       = 640;
        fmt.fmt.pix.height      = 480;
        fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
        fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;
        xioctl(fd, VIDIOC_S_FMT, &fmt);
        // Checking the result of setting the format
        if (fmt.fmt.pix.pixelformat != V4L2_PIX_FMT_RGB24) {
                printf("Libv4l didn't accept RGB24 format. Can't proceed.\n");
                exit(EXIT_FAILURE);
        }
        if ((fmt.fmt.pix.width != 640) || (fmt.fmt.pix.height != 480))
                printf("Warning: driver is sending image at %dx%d\n",
                        fmt.fmt.pix.width, fmt.fmt.pix.height);

        // Setup double buffered mmap capturing
        CLEAR(req);
        req.count = 2;
        req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        req.memory = V4L2_MEMORY_MMAP;
        xioctl(fd, VIDIOC_REQBUFS, &req);

        // Allocate and clear space for the buffer metadata
        buffers = (buffer*)calloc(req.count, sizeof(*buffers));
        for (n_buffers = 0; n_buffers < req.count; ++n_buffers) {
                CLEAR(buf);

                // get the size and start of buffers from the driver
                buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buf.memory      = V4L2_MEMORY_MMAP;
                buf.index       = n_buffers;

                xioctl(fd, VIDIOC_QUERYBUF, &buf);

                // mmap the driver's buffers to userspace
                buffers[n_buffers].length = buf.length;
                buffers[n_buffers].start = v4l2_mmap(NULL, buf.length,
                              PROT_READ | PROT_WRITE, MAP_SHARED,
                              fd, buf.m.offset);

                if (MAP_FAILED == buffers[n_buffers].start) {
                        perror("mmap");
                        exit(EXIT_FAILURE);
                }
        }

        // Tell the device to queue the buffers
        for (i = 0; i < n_buffers; ++i) {
                CLEAR(buf);
                buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buf.memory = V4L2_MEMORY_MMAP;
                buf.index = i;
                xioctl(fd, VIDIOC_QBUF, &buf);
        }

        // Begin to stream capture video
        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        xioctl(fd, VIDIOC_STREAMON, &type);

        timespec start;
        clock_gettime(CLOCK_MONOTONIC, &start);
        long prev_ts = 0;

        // Only capture 20 frames
        for (i = 0; i < 20; i++) 
        {
            // Wait until the device is ready
            do 
            {
                FD_ZERO(&fds);
                FD_SET(fd, &fds);

                /* Timeout. */
                tv.tv_sec = 2;
                tv.tv_usec = 0;

                r = select(fd + 1, &fds, NULL, NULL, &tv);
            } while ((r == -1 && (errno = EINTR)));

            if (r == -1) 
            {
                    perror("select");
                    return errno;
            }

            // Deque a buffer
            CLEAR(buf);
            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;
            xioctl(fd, VIDIOC_DQBUF, &buf);

            // Write the buffer to file in PPM format
            sprintf(out_name, "out%03d.ppm", i);
            fout = fopen(out_name, "w");
            if (!fout) 
            {
                    perror("Cannot open image");
                    exit(EXIT_FAILURE);
            }
            fprintf(fout, "P6\n%d %d 255\n",
                    fmt.fmt.pix.width, fmt.fmt.pix.height);
            fwrite(buffers[buf.index].start, buf.bytesused, 1, fout);
            fclose(fout);

            // Track how long each buffer deque and read took
            long delta = buf.timestamp.tv_usec - prev_ts;
            prev_ts = buf.timestamp.tv_usec;

            printf("delta = %ld\n", delta);

            // Reque the buffer
            xioctl(fd, VIDIOC_QBUF, &buf);
        }

        timespec current;
        clock_gettime(CLOCK_MONOTONIC, &current);
        long elapsed_s = current.tv_sec - start.tv_sec;
        long elapsed_ns = current.tv_nsec - start.tv_nsec;
        long msec = elapsed_ns / 1000000;
        msec += elapsed_s*1000;

        printf("elapsed time = %ld ms", msec);

        // Turn off streaming
        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        xioctl(fd, VIDIOC_STREAMOFF, &type);

        // Unmap the buffers
        for (i = 0; i < n_buffers; ++i)
                v4l2_munmap(buffers[i].start, buffers[i].length);

        // Close the file descriptor
        v4l2_close(fd);

        return 0;
}

