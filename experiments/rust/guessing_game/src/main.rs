
extern crate rand;

use std::io;
use std::cmp::Ordering;
// use rand::Rng;

fn main() {

    // let value = rand::random::<u32>() % 10;

    println!("Guess the number!");

    macro_rules! times_two {
        ($e:expr) => { (2.0 * $e) as u32 }
    };

    let value = times_two!(2.0 + 1.0 / 2.0);

    loop {
        println!("Please input your guess:");

        let mut guess = String::new();
        io::stdin().read_line(&mut guess).ok().expect("Failed to read line");

        let guess:u32 = match guess.trim().parse()
        {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("You guessed {}", guess);

        match guess.cmp(&value)
        {
            Ordering::Less      => println!("Too small!"),
            Ordering::Greater   => println!("Too Big!"),
            Ordering::Equal     => { println!("Just right!"); break; },
        }
    }

    println!("Real value = {}", value);
}

// Main 2

// extern crate rustc_serialize;
// extern crate csv;
// use std::path::Path;
// // use std::io;
// // use std::error::Error;

// #[derive(Debug)]
// enum CliError {
//     Io(csv::Error),
//     Write(csv::Error),
// }

// #[derive(RustcEncodable)]
// struct Movie
// {
//     title: String,
//     bad_guy: String,
//     pub_year: usize,
// }

// fn main() 
// {
//     match do_work()
//     {
//         Err(err) => println!("Failed to do_work(). Err:{:?}", err),
//         _ => println!("OK!"),
//     }    
// }

// fn do_work() -> Result<(), CliError>
// {
//     let dollar_films = vec!
//     [
//         Movie { title:"A Fistful of dollars".to_owned(), bad_guy:"Rojo".to_owned(), pub_year:1964},
//         Movie { title:"For a Few Dollars More".to_owned(), bad_guy:"El Indio".to_owned(), pub_year:1965 },
//         Movie { title:"The Good, the Bad, and the Ugly".to_owned(), bad_guy:"Tuco".to_owned(), pub_year:1966 },
//     ];

//     let path = Path::new("westerns.csv");
//     let mut writer = try!(csv::Writer::from_file(&path)); // .map_err(CliError::Write)); 

//     for movie in dollar_films
//     {
//         try!(writer.encode(movie).map_err(CliError::Write)); 
//     }

//     // let movie = Movie { title:"A Fistful of dollars".to_owned(), bad_guy:"Rojo".to_owned(), pub_year:1964};
//     // let result = writer.encode(movie);
//     // assert!(result.is_ok());
//     // writer.encode(["1",2,"test"]).ok().expect("CSV writer error!");

//     Ok(())
// }

// impl From<csv::Error> for CliError
// {
//     fn from(err: csv::Error) -> CliError
//     {
//         println!("Converting error={:?}", err);

//         CliError::Io(err) // "Error!".to_owned())
//     }
// }


// Main 3

// fn main() 
// {
//     let v = Vector3 { x: 1.0, y: 2.0, z: 3.0 };

//     println!("v.mag_sq = {}", v.magnitued_squared());    
// }

// pub struct Vector3
// {
//     pub x: f32,
//     pub y: f32,
//     pub z: f32
// }

// impl Vector3
// {
//     pub fn dot(&self, other: &Vector3) -> f32
//     {
//         self.x*other.x + self.y*other.y + self.z*other.z
//     }

//     pub fn magnitued_squared(&self) -> f32
//     {
//         self.dot(self)
//     }
// }


