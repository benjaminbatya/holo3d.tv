// use std::thread;
// use std::sync::{Mutex, Arc};
// use std::sync::mpsc;

// fn main() 
// {
//     let data = Arc::new(Mutex::new(0));

//     let (tx, rx) = mpsc::channel();

//     let mut threads = Vec::with_capacity(10);

//     for i in 0..10
//     {
//         let (data, tx) = (data.clone(), tx.clone());

//         threads.push(thread::spawn(move ||
//         {
//             let mut data = data.lock().unwrap();

//             *data += 1;

//             tx.send(i).ok().expect("Failed to send");
//             println!("{:?}", *data);
//             // if((*data%2) == 0) { panic!("Whoops!"); }
//         }));
//     }

//     // for _ in 0..10
//     // {
//     //     let val : usize = rx.recv().ok().expect("failed to receive");
//     //     println!("received {}", val);
//     // }

//     for t in threads
//     {
//         let result = t.join().is_err();
//         println!("result = {:?}", result);
//     }
// }

fn main()
{
    fn take(v: Vec<i32>) -> Vec<i32> {
    // what happens here isn’t important.
        v
    }

    let v = vec![1, 2, 3];

    let m = take(v);

    println!("v[0] is: {}", m[0]);
}