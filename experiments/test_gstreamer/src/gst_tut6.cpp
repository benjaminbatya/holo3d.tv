#include <gst/gst.h>

/* Functions below print the Capabilities in a human-friendly format */
static gboolean print_field(GQuark field, const GValue *value, gpointer pfx) 
{ 
    gchar *str = gst_value_serialize(value); 
    
    g_print("%s  %15s: %s\n", (gchar *)pfx, g_quark_to_string(field), str); 
    g_free(str); 
    return TRUE;
}

static void print_caps(const GstCaps *caps, const gchar *pfx) 
{ 
    guint i; 
    
    g_return_if_fail(caps != NULL); 
    
    if (gst_caps_is_any(caps)) 
    {
        g_print("%sANY\n", pfx); 
        return;
    }
    if (gst_caps_is_empty(caps)) 
    {
        g_print("%sEMPTY\n", pfx); 
        return;
    }
    
    for (i = 0; i < gst_caps_get_size(caps); i++) 
    {
        GstStructure *structure = gst_caps_get_structure(caps, i); 
        
        g_print("%s%s\n", pfx, gst_structure_get_name(structure)); 
        gst_structure_foreach(structure, print_field, (gpointer)pfx);
    }
}

/* Prints information about a Pad Template, including its Capabilities */
static void print_pad_templates_information(GstElementFactory *factory) 
{ 
    g_print("Pad Templates for %s:\n", gst_element_factory_get_longname(factory)); 
    if (!gst_element_factory_get_num_pad_templates(factory)) 
    {
        g_print("  none\n"); 
        return;
    }
    
    const GList *pads = gst_element_factory_get_static_pad_templates(factory); 
    while (pads) 
    {
        GstStaticPadTemplate *padtemplate = (GstStaticPadTemplate *)pads->data; 
        pads = g_list_next(pads); 
        
        if (padtemplate->direction == GST_PAD_SRC) g_print("  SRC template: '%s'\n", padtemplate->name_template); 
        else if (padtemplate->direction == GST_PAD_SINK) g_print("  SINK template: '%s'\n", padtemplate->name_template); 
        else g_print("  UNKNOWN!!! template: '%s'\n", padtemplate->name_template); 
        
        if (padtemplate->presence == GST_PAD_ALWAYS) g_print("    Availability: Always\n"); 
        else if (padtemplate->presence == GST_PAD_SOMETIMES) g_print("    Availability: Sometimes\n"); 
        else if (padtemplate->presence == GST_PAD_REQUEST) 
        {
            g_print("    Availability: On request\n");
        } 
        else g_print("    Availability: UNKNOWN!!!\n"); 
        
        if (padtemplate->static_caps.string) 
        {
            GstCaps *caps; 
            
            g_print("    Capabilities:\n"); 
            caps = gst_static_caps_get(&padtemplate->static_caps); 
            print_caps(caps, "      "); 
            gst_caps_unref(caps);
        }
        
        g_print("\n");
    }
}

/* Shows the CURRENT capabilities of the requested pad in the given element */
static void print_pad_capabilities(GstElement *element, gchar *pad_name) 
{ 
    GstPad *pad = NULL; 
    GstCaps *caps = NULL; 
    
    /* Retrieve pad */
    pad = gst_element_get_static_pad(element, pad_name); 
    if (!pad) 
    {
        g_printerr("On element %s, could not retrieve pad '%s'\n", gst_element_get_name(element), pad_name); 
        return;
    }
    
    /* Retrieve negotiated caps (or acceptable caps if negotiation is not finished yet) */
    caps = gst_pad_get_current_caps(pad); 
    if (!caps) caps = gst_pad_query_caps(pad, NULL); 
    
    /* Print and free */
    g_print("On element %s, caps for the %s pad:\n", gst_element_get_name(element), pad_name); 
    print_caps(caps, "      "); 
    gst_caps_unref(caps); 
    gst_object_unref(pad);
}

struct CustomData {
    GstElement* pipeline;
    GstElement* source;

    GstElement* convert;
    GstElement* sink;
};

static void pad_added_handler(GstElement* src, GstPad* new_pad, CustomData* data)
{
    GstCaps* new_pad_caps;
    GstStructure* new_pad_struct;
    const gchar* new_pad_type;
    GstPadLinkReturn ret;
    GstPad* sink_pad = nullptr;
        
    g_print("Received new pad '%s' from '%s':\n", GST_PAD_NAME(new_pad), GST_ELEMENT_NAME(src));
    
    new_pad_caps = gst_pad_query_caps(new_pad, nullptr);
    if (new_pad_caps == nullptr) 
    {
        g_print("error at gst_pad_get_allowed_caps\n");
        goto exit;
    }

    new_pad_struct = gst_caps_get_structure(new_pad_caps, 0); 
    if (new_pad_struct == nullptr) 
    {
        g_print("error at gst_caps_get_structure\n");
        goto exit;
    }

    new_pad_type = gst_structure_get_name(new_pad_struct);
    if (new_pad_type == nullptr) 
    {
        g_print("error at gst_structure_get_name\n");
        goto exit;
    }

    if (g_str_has_prefix(new_pad_type, "audio/x-raw"))
    {
        sink_pad = gst_element_get_static_pad(data->convert, "sink");
    } else
    {
        g_print("New pad has type '%s' which is not raw audio. Ignoring...\n", new_pad_type);
        goto exit;
    }

    if (gst_pad_is_linked(sink_pad)) 
    {
        g_print("Pad '%s' are already linked. ignoring..\n", gst_pad_get_name(sink_pad));
        goto exit;
    }

    /* Print the current capabilities of the sink element */
    g_print("Before link: ");
    print_pad_capabilities(data->source, gst_pad_get_name(new_pad));
    print_pad_capabilities(data->convert, (gchar*)"sink");

    ret = gst_pad_link(new_pad, sink_pad);
    if (GST_PAD_LINK_FAILED(ret)) 
    {
        g_print("type is '%s' but link failed\n", new_pad_type);
        goto exit;
    } else
    {
        g_print("Link succeed from %s to %s (type='%s')\n", 
                gst_pad_get_name(new_pad), gst_pad_get_name(sink_pad), new_pad_type);
    }

    g_print("After link: ");
    print_pad_capabilities(data->source, gst_pad_get_name(new_pad));
    print_pad_capabilities(data->convert, (gchar*)"sink");

    exit:
    if (new_pad_caps) 
    {
        gst_caps_unref(new_pad_caps);
    }
    if (sink_pad) 
    {
        gst_object_unref(sink_pad); 
    }
}

#define MY_ASSERT(exp, msg) { \
    if(!(exp)) \
    { \
        g_printerr(msg); g_printerr("\n"); \
        if(data.pipeline) { gst_object_unref (data.pipeline); } \
        return -1; \
    } \
}

int main(int argc, char *argv[]) 
{ 
    CustomData data;
    GstElementFactory * source_factory,*sink_factory; 
    GstBus *bus; 
    GstMessage *msg; 
    GstStateChangeReturn ret; 
    gboolean terminate = FALSE; 
    
    /* Initialize GStreamer */
    gst_init(&argc, &argv); 
    
    /* Create the element factories */
    // source_factory = gst_element_factory_find("audiotestsrc"); 
    source_factory = gst_element_factory_find("uridecodebin");
    sink_factory = gst_element_factory_find("autoaudiosink"); 
    if (!source_factory || !sink_factory) 
    {
        g_printerr("Not all element factories could be created.\n"); 
        return -1;
    }
    
    /* Print information about the pad templates of these factories */
    print_pad_templates_information(source_factory); 
    print_pad_templates_information(sink_factory); 
    
    /* Ask the factories to instantiate actual elements */
    data.source = gst_element_factory_create(source_factory, "source");
    /* Set the URI to play */
    data.convert = gst_element_factory_make("audioconvert", "convert"); 
    data.sink = gst_element_factory_create(sink_factory, "sink"); 
    
    /* Create the empty pipeline */
    data.pipeline = gst_pipeline_new("test-pipeline"); 
    
    if (!data.pipeline || !data.source || !data.sink) 
    {
        g_printerr("Not all elements could be created.\n"); 
        return -1;
    }

    g_object_set (data.source, "uri", "http://docs.gstreamer.com/media/sintel_trailer-480p.webm", NULL);

    int handler_id = g_signal_connect(data.source, "pad-added", G_CALLBACK(pad_added_handler), &data);
    MY_ASSERT(handler_id > 0, "failed to connect 'pad-added' signal");

    /* Build the pipeline */
    gst_bin_add_many(GST_BIN(data.pipeline), data.source, data.convert, data.sink, NULL); 

//  print_pad_capabilities(data.convert, (gchar*)"sink");

    g_print("Before link: ");
    print_pad_capabilities(data.convert, (gchar*)"src");
    print_pad_capabilities(data.sink, (gchar*)"sink");

    bool suc = gst_element_link(data.convert, data.sink);
    MY_ASSERT(suc, "Failed to link convert and sink");

    g_print("After link: ");
    print_pad_capabilities(data.convert, (gchar*)"src");
    print_pad_capabilities(data.sink, (gchar*)"sink");
    
    /* Print initial negotiated caps (in NULL state) */
    g_print("In NULL state:\n"); 
    print_pad_capabilities(data.sink, (gchar*)"sink"); 
    
    /* Start playing */
    ret = gst_element_set_state(data.pipeline, GST_STATE_PLAYING); 
    if (ret == GST_STATE_CHANGE_FAILURE) 
    {
        g_printerr("Unable to set the pipeline to the playing state (check the bus for error messages).\n");
    }
    
    /* Wait until error, EOS or State Change */
    bus = gst_element_get_bus(data.pipeline); 
    do 
    {
        msg = gst_bus_timed_pop_filtered(bus, GST_CLOCK_TIME_NONE, 
                                         GstMessageType(GST_MESSAGE_ERROR | GST_MESSAGE_EOS | GST_MESSAGE_STATE_CHANGED)); 
        
        /* Parse message */
        if (msg != NULL) 
        {
            GError *err; 
            gchar *debug_info; 
            
            switch (GST_MESSAGE_TYPE(msg)) 
            {
            case GST_MESSAGE_ERROR:
                gst_message_parse_error(msg, &err, &debug_info); 
                g_printerr("Error received from element %s: %s\n", GST_OBJECT_NAME(msg->src), err->message); 
                g_printerr("Debugging information: %s\n", debug_info ? debug_info : "none"); 
                g_clear_error(&err); 
                g_free(debug_info); 
                terminate = TRUE; 
                break; 
            case GST_MESSAGE_EOS:
                g_print("End-Of-Stream reached.\n"); 
                terminate = TRUE; 
                break; 
            case GST_MESSAGE_STATE_CHANGED:
                /* We are only interested in state-changed messages from the pipeline */
                if (GST_MESSAGE_SRC(msg) == GST_OBJECT(data.pipeline)) 
                {
                    GstState old_state, new_state, pending_state; 
                    gst_message_parse_state_changed(msg, &old_state, &new_state, &pending_state); 
                    g_print("\nPipeline state changed from %s to %s:\n", 
                            gst_element_state_get_name(old_state), gst_element_state_get_name(new_state)); 
                }
                break; 
            default:
                /* We should not reach here because we only asked for ERRORs, EOS and STATE_CHANGED */
                g_printerr("Unexpected message received.\n"); 
                break;
            }
            gst_message_unref(msg);
        }
    } 
    while (!terminate); 
    
    /* Free resources */
    gst_object_unref(bus); 
    gst_element_set_state(data.pipeline, GST_STATE_NULL); 
    gst_object_unref(data.pipeline); 
    gst_object_unref(source_factory); 
    gst_object_unref(sink_factory); 
    return 0;
}

