#pragma once

// NOTE: Need to disable deprecated-declarations for now until gstmm is fully c++11 compliant
// From http://stackoverflow.com/a/32587042/4228547
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gstreamermm.h>
#pragma GCC diagnostic warning  "-Wdeprecated-declarations"

#include <glibmm/main.h>
#include <iostream>

#include "util_funcs.hpp"

using ElementPtr = Glib::RefPtr<Gst::Element>;
using PipelinePtr = Glib::RefPtr<Gst::Pipeline>;
using BusPtr = Glib::RefPtr<Gst::Bus>;

using MessagePtr = Glib::RefPtr<Gst::Message>;
using ErrorMsgPtr = Glib::RefPtr<Gst::MessageError>;
using MsgStateChgPtr = Glib::RefPtr<Gst::MessageStateChanged>;

using BinPtr = Glib::RefPtr<Gst::Bin>;
using PadPtr = Glib::RefPtr<Gst::Pad>;
using CapsPtr = Glib::RefPtr<Gst::Caps>;

// helper func for connecting to signals
// From http://stackoverflow.com/questions/22563621/create-sigcslot-with-no-arguments
#define CONNECT(src, signal, ...) (src)->signal_##signal().connect(__VA_ARGS__)

std::ostream& operator<<(std::ostream& os,const Gst::State& st)
{
    switch(st)
    {
#define OUTPUT_ENUM(e) case Gst::e : os << #e; break;
    OUTPUT_ENUM(STATE_VOID_PENDING);
    OUTPUT_ENUM(STATE_NULL);
    OUTPUT_ENUM(STATE_READY);
    OUTPUT_ENUM(STATE_PAUSED);
    OUTPUT_ENUM(STATE_PLAYING);
#undef OUTPUT_ENUM
    default: 
        os << "Unknown State=" << st; 
        break;
    }

    return os;
}

