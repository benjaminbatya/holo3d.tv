#include <gst/gst.h>
#include <gst/audio/audio.h>
#include <string.h>

#define CHUNK_SIZE 1024
#define SAMPLE_RATE 44100
#define AUDIO_CAPS "audio/x-raw-int, channels=1,rate=%d,signed=(boolean)true,width=16,depth=16,endianness=BYTE_ORDER"

using GstElementPtr = GstElement *; 
using GstPadPtr = GstPad *; 

struct CustomData {
    GstElementPtr pipeline, app_source, tee; 
    GstElementPtr audio_queue, audio_convert1, audio_resample, audio_sink; 
    GstElementPtr video_queue, audio_convert2, visual, video_convert, video_sink; 
    GstElementPtr app_queue, app_sink;

    guint64 num_samples;
    float a, b, c, d;

    guint sourceid;

    GMainLoop* main_loop;
};

//static void pad_added_handler(GstElement* src, GstPad* new_pad, CustomData* data);

static bool push_data(CustomData* data)
{
    int num_samples = CHUNK_SIZE / 2;

    GstBuffer* buffer = gst_buffer_new_and_alloc(CHUNK_SIZE);
    GST_BUFFER_TIMESTAMP(buffer) = gst_util_uint64_scale(data->num_samples, GST_SECOND, SAMPLE_RATE);
    GST_BUFFER_DURATION(buffer) = gst_util_uint64_scale(CHUNK_SIZE, GST_SECOND, SAMPLE_RATE);

    // Generate some waveforms
    GstMapInfo map;
    bool ret = gst_buffer_map(buffer, &map, GST_MAP_WRITE);
    if (!ret) { return false; }
    gint16* raw = (gint16*)map.data;
    data->c += data->d;
    data->d -= data->c / 1000;
    float freq = 1100 + 1000*data->d;
    for (int i=0; i<num_samples; i++) 
    {
        data->a += data->b;
        data->b -= data->a / freq;
        raw[i] = gint16(500 * data->a);
    }
    gst_buffer_unmap(buffer, &map);
    data->num_samples += num_samples;

    // the the buffer into app_source
    GstFlowReturn fret;
    g_signal_emit_by_name(data->app_source, "push-buffer", buffer, &fret);

    gst_buffer_unref(buffer);

    return (fret == GST_FLOW_OK);
}

static void start_feed(GstElement* source, guint size, CustomData* data)
{
    if (data->sourceid == 0) 
    {
        g_print("Start feeding\n");
        data->sourceid = g_idle_add(GSourceFunc(push_data), data);
    }
}


static void stop_feed(GstElement* source, CustomData* data)
{
    if (data->sourceid != 0) 
    {
        g_print("Stop feeding\n");
        g_source_remove(data->sourceid);
        data->sourceid = 0;
    }
}

static void new_sample(GstElement* sink, CustomData* data)
{
    GstSample* sample;
    g_signal_emit_by_name(sink, "pull-sample", &sample);
    if (sample) 
    {
        // indicate that a new sample was received
        g_print("*");
        gst_sample_unref(sample);
    }
}

static void error_cb(GstBus* bus, GstMessage* msg, CustomData* data)
{
    GError *err;
    gchar *debug_info;
  
    /* Print error details on the screen */
    gst_message_parse_error (msg, &err, &debug_info);
    g_printerr ("Error received from element %s: %s\n", GST_OBJECT_NAME (msg->src), err->message);
    g_printerr ("Debugging information: %s\n", debug_info ? debug_info : "none");
    g_clear_error (&err);
    g_free (debug_info);

    g_main_loop_quit (data->main_loop);
}

int main(int argc, char **argv) 
{ 
    CustomData data;
    memset(&data, 0, sizeof(data));
    data.b = 1;
    data.d = 1;
          
    GstPadTemplate *tee_src_pad_template; 
    GstPadPtr tee_audio_pad, tee_video_pad, tee_app_pad; 
    GstPadPtr queue_audio_pad, queue_video_pad, queue_app_pad; 
    
    gst_init(&argc, &argv); 
    
    /* Create the elements */
    data.app_source = gst_element_factory_make("appsrc", "app_source"); 
    data.tee = gst_element_factory_make("tee", "tee"); 

    data.audio_queue = gst_element_factory_make("queue", "audio_queue"); 
    data.audio_convert1 = gst_element_factory_make("audioconvert", "audio_convert1"); 
    data.audio_resample = gst_element_factory_make("audioresample", "audio_resample"); 
    data.audio_sink = gst_element_factory_make("autoaudiosink", "audio_sink"); 

    data.video_queue = gst_element_factory_make("queue", "video_queue"); 
    data.audio_convert2 = gst_element_factory_make("audioconvert", "audio_convert2"); 
    data.visual = gst_element_factory_make("wavescope", "visual"); 
    data.video_convert = gst_element_factory_make("videoconvert", "video_convert"); 
    data.video_sink = gst_element_factory_make("autovideosink", "video_sink"); 
    
    data.app_queue = gst_element_factory_make("queue", "app_queue");
    data.app_sink = gst_element_factory_make("appsink", "app_sink");

    /* Create the empty pipeline */
    data.pipeline = gst_pipeline_new("test-pipeline"); 
    
    if (!data.pipeline || !data.app_source || !data.tee || 
        !data.audio_queue || !data.audio_convert1 || !data.audio_resample || !data.audio_sink || 
        !data.video_queue || !data.audio_convert2 || !data.visual || !data.video_convert || !data.video_sink ||
        !data.app_queue || !data.app_sink) 
    {
        g_printerr("Not all elements could be created.\n"); 
        return -1;
    }

//  g_object_set (source, "uri", "http://docs.gstreamer.com/media/sintel_trailer-480p.webm", NULL);

//  int handler_id = g_signal_connect(data.source, "pad-added", G_CALLBACK(pad_added_handler), &data);
//  if (handler_id <= 0)
//  {
//      g_printerr("failed to connect 'pad-added' signal\n");
//      return -1;
//  }
    
    // configure the elements
    g_object_set(data.visual, "shader", 2, "style", 3, NULL); 

    // configure app_source
    GstAudioInfo info;
    gst_audio_info_set_format(&info, GST_AUDIO_FORMAT_S16, SAMPLE_RATE, 1, NULL);
    GstCaps* audio_caps = gst_audio_info_to_caps(&info);
    g_object_set(data.app_source, "caps", audio_caps, "format", GST_FORMAT_TIME, NULL);
    g_signal_connect(data.app_source, "need-data", G_CALLBACK(start_feed), &data);
    g_signal_connect(data.app_source, "enough-data", G_CALLBACK(stop_feed), &data);

    // configure app_sink
    g_object_set(data.app_sink, "emit-signals", true, "caps", audio_caps, NULL);
    g_signal_connect(data.app_sink, "new-sample", G_CALLBACK(new_sample), &data);
    gst_caps_unref(audio_caps);

    /* Link all elements that can be automatically linked because they have "Always" pads */
    gst_bin_add_many(GST_BIN(data.pipeline), data.app_source, data.tee, 
                     data.audio_queue, data.audio_convert1, data.audio_resample, data.audio_sink, 
                     data.video_queue, data.audio_convert2, data.visual, data.video_convert, data.video_sink, 
                     data.app_queue, data.app_sink, NULL); 
    if (!gst_element_link_many(data.app_source, data.tee, NULL) || 
        !gst_element_link_many(data.audio_queue, data.audio_convert1, data.audio_resample, data.audio_sink, NULL) || 
        !gst_element_link_many(data.video_queue, data.audio_convert2, data.visual, data.video_convert, data.video_sink, NULL) ||
        !gst_element_link_many(data.app_queue, data.app_sink, NULL)) 
    {
        g_printerr("Elements could not be linked.\n"); 
        gst_object_unref(data.pipeline); 
        return -1;
    }
    
    // Manually link the tee's request pads
    tee_src_pad_template = gst_element_class_get_pad_template(GST_ELEMENT_GET_CLASS(data.tee), "src_%u");

    tee_audio_pad = gst_element_request_pad(data.tee, tee_src_pad_template, NULL, NULL);
    g_print("Obtained requested pad %s for audio branch\n", gst_pad_get_name(tee_audio_pad));
    queue_audio_pad = gst_element_get_static_pad(data.audio_queue, "sink");

    tee_video_pad = gst_element_request_pad (data.tee, tee_src_pad_template, NULL, NULL);
    g_print ("Obtained request pad %s for video branch.\n", gst_pad_get_name (tee_video_pad));
    queue_video_pad = gst_element_get_static_pad(data.video_queue, "sink");

    tee_app_pad = gst_element_request_pad (data.tee, tee_src_pad_template, NULL, NULL);
    g_print ("Obtained request pad %s for app branch.\n", gst_pad_get_name (tee_app_pad));
    queue_app_pad = gst_element_get_static_pad(data.app_queue, "sink");
    
    if (gst_pad_link(tee_audio_pad, queue_audio_pad) != GST_PAD_LINK_OK || 
        gst_pad_link(tee_video_pad, queue_video_pad) != GST_PAD_LINK_OK ||
        gst_pad_link(tee_app_pad, queue_app_pad) != GST_PAD_LINK_OK) 
    {
        g_printerr("Tee could not be linked.\n"); 
        gst_object_unref(data.pipeline); 
        return -1;
    }
    gst_object_unref(queue_audio_pad); 
    gst_object_unref(queue_video_pad); 
    gst_object_unref(queue_app_pad);

    /* Instruct the bus to emit signals for each received message, and connect to the interesting signals */
    GstBus* bus = gst_element_get_bus (data.pipeline);
    gst_bus_add_signal_watch (bus);
    g_signal_connect (G_OBJECT (bus), "message::error", (GCallback)error_cb, &data);
    gst_object_unref (bus);

    gst_element_set_state(data.pipeline, GST_STATE_PLAYING);

    data.main_loop = g_main_loop_new(NULL, false);
    g_main_loop_run(data.main_loop);

    // playing is done, shutdown...
    gst_element_release_request_pad(data.tee, tee_audio_pad);
    gst_element_release_request_pad(data.tee, tee_video_pad);
    gst_element_release_request_pad(data.tee, tee_app_pad);

    gst_object_unref(tee_audio_pad);
    gst_object_unref(tee_video_pad);
    gst_object_unref(tee_app_pad);

    gst_element_set_state(data.pipeline, GST_STATE_NULL);

    gst_object_unref(data.pipeline);
    return 0;
}

//
//static void pad_added_handler(GstElement* src, GstPad* new_pad, CustomData* data)
//{
//    GstCaps* new_pad_caps;
//    GstStructure* new_pad_struct;
//    const gchar* new_pad_type;
//    GstPadLinkReturn ret;
//    GstPad* sink_pad = nullptr;
//
//    g_print("Received new pad '%s' from '%s':\n", GST_PAD_NAME(new_pad), GST_ELEMENT_NAME(src));
//
//    new_pad_caps = gst_pad_query_caps(new_pad, nullptr);
//    if (new_pad_caps == nullptr)
//    {
//        g_print("error at gst_pad_get_allowed_caps\n");
//        goto exit;
//    }
//
//    new_pad_struct = gst_caps_get_structure(new_pad_caps, 0);
//    if (new_pad_struct == nullptr)
//    {
//        g_print("error at gst_caps_get_structure\n");
//        goto exit;
//    }
//
//    new_pad_type = gst_structure_get_name(new_pad_struct);
//    if (new_pad_type == nullptr)
//    {
//        g_print("error at gst_structure_get_name\n");
//        goto exit;
//    }
//
//    if (g_str_has_prefix(new_pad_type, "audio/x-raw"))
//    {
//        sink_pad = gst_element_get_static_pad(data->convert, "sink");
//    } else
//    {
//        g_print("New pad has type '%s' which is not raw audio. Ignoring...\n", new_pad_type);
//        goto exit;
//    }
//
//    if (gst_pad_is_linked(sink_pad))
//    {
//        g_print("Pad '%s' are already linked. ignoring..\n", gst_pad_get_name(sink_pad));
//        goto exit;
//    }
//
//    ret = gst_pad_link(new_pad, sink_pad);
//    if (GST_PAD_LINK_FAILED(ret))
//    {
//        g_print("type is '%s' but link failed\n", new_pad_type);
//        goto exit;
//    } else
//    {
//        g_print("Link succeed from %s to %s (type='%s')\n",
//                gst_pad_get_name(new_pad), gst_pad_get_name(sink_pad), new_pad_type);
//    }
//
//    exit:
//    if (new_pad_caps)
//    {
//        gst_caps_unref(new_pad_caps);
//    }
//    if (sink_pad)
//    {
//        gst_object_unref(sink_pad);
//    }
//}
