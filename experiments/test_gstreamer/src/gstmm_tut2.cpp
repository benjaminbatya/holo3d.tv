#include "common_gstmm.hpp"

int main(int argc, char** argv)
{
    Gst::init(argc, argv);

    ElementPtr source = Gst::ElementFactory::create_element("videotestsrc");
    ASSERT_THROW(source, "Failed to create videotestsrc");
    ElementPtr vertigotv = Gst::ElementFactory::create_element("vertigotv");
    ASSERT_THROW(vertigotv, "Failed to create vertigotv");
    ElementPtr videoconvert = Gst::ElementFactory::create_element("videoconvert");
    ASSERT_THROW(videoconvert, "Failed to create videoconvert");
    ElementPtr sink = Gst::ElementFactory::create_element("autovideosink");
    ASSERT_THROW(sink, "Failed to create sink");

    PipelinePtr pipeline = Gst::Pipeline::create();
    ASSERT_THROW(pipeline, "Failed to create pipeline");

    pipeline->add(source)->add(vertigotv)->add(videoconvert)->add(sink);

    source->link(vertigotv)->link(videoconvert)->link(sink);

    auto ret = pipeline->set_state(Gst::STATE_PLAYING);
    ASSERT_THROW(ret!=Gst::STATE_CHANGE_FAILURE, "Failed to start playing");

    BusPtr bus = pipeline->get_bus();

    // Wait for a msg
    MessagePtr msg = bus->pop(Gst::CLOCK_TIME_NONE, Gst::MESSAGE_ERROR | Gst::MESSAGE_EOS);

    if (msg) 
    {
        switch (msg->get_message_type()) 
        {
        case Gst::MESSAGE_ERROR:
        {
            ErrorMsgPtr err = ErrorMsgPtr::cast_static(msg);
            ERROR("Error received from element {}: {}", 
                  err->get_source()->get_name(), err->parse().what());
            ERROR("Debugging information: {}", err->parse_debug());
        } break;
        case Gst::MESSAGE_EOS:
            INFO("End-of-stream reached");
            break;
        default:
            ERROR("Unexcepted message received: {}", msg->get_message_type());
            break;
        }
    }

    pipeline->set_state(Gst::STATE_NULL);

    return 0;
}
