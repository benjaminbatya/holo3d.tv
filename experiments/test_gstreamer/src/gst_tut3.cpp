#include <gst/gst.h>
#include <cstdio>

struct CustomData {
    GstElement* pipeline;
    GstElement* source;

    GstElement* audio_convert;
    GstElement* audio_sink;

    GstElement* video_convert;
    GstElement* video_sink;
};

static void pad_added_handler(GstElement* src, GstPad* pad, CustomData* data);


#define MY_ASSERT(exp, msg) { \
    if(!(exp)) \
    { \
        g_printerr("%s\n", msg); \
        if(data.pipeline) { gst_object_unref (data.pipeline); } \
        return -1; \
    } \
}

int main(int argc, char** argv)
{
    CustomData data;
    GstBus* bus;
    GstMessage* msg;
    GstStateChangeReturn cret;
    bool ret = false;
    bool terminate = false;

    gst_init(&argc, &argv);

    data.source = gst_element_factory_make("uridecodebin", "source");
    MY_ASSERT(data.source, "Failed to create source");

    data.audio_convert = gst_element_factory_make("audioconvert", "audio_convert");
    MY_ASSERT(data.audio_convert, "Failed to create audio_convert");

    data.audio_sink = gst_element_factory_make("autoaudiosink", "audio_sink");
    MY_ASSERT(data.audio_sink, "Failed to create audio_sink");

    data.video_convert = gst_element_factory_make("videoconvert", "video_convert");
    MY_ASSERT(data.video_convert, "Failed to create video_convert");

    data.video_sink = gst_element_factory_make("autovideosink", "video_sink");
    MY_ASSERT(data.video_sink, "Failed to create video_sink");

    data.pipeline = gst_pipeline_new("test-pipeline");
    MY_ASSERT(data.pipeline, "Failed to create pipeline");

    // build the pipeline
    GstBin* bin = GST_BIN(data.pipeline);

    ret = gst_bin_add(bin, data.source);
    MY_ASSERT(ret, "Failed to add source");

    ret = gst_bin_add(bin, data.audio_convert);
    MY_ASSERT(ret, "failed to add audio_convert");

    ret = gst_bin_add(bin, data.audio_sink);
    MY_ASSERT(ret, "Failed to add audio_sink");

    ret = gst_bin_add(bin, data.video_convert);
    MY_ASSERT(ret, "failed to add video_convert");

    ret = gst_bin_add(bin, data.video_sink);
    MY_ASSERT(ret, "Falied to add video_sink");

    ret = gst_element_link(data.audio_convert, data.audio_sink);
    MY_ASSERT(ret, "Failed to link audio_convert and audio_sink");

    ret = gst_element_link(data.video_convert, data.video_sink);
    MY_ASSERT(ret, "Failed to link video_convert and video_sink");

//  g_object_set(data.source, "uri", "http://docs.gstreamer.com/media/sintel_trailer-480p.webm", nullptr);
    g_object_set(data.source, "uri", "file:///home/benjamin/Documents/holo3d.tv/experiments/test_gstreamer/sintel-trailer-480p.webm", nullptr);

    int handler_id = g_signal_connect(data.source, "pad-added", G_CALLBACK(pad_added_handler), &data);
    MY_ASSERT(handler_id > 0, "failed to connect 'pad-added' signal");

    cret = gst_element_set_state(data.pipeline, GST_STATE_PLAYING);
    MY_ASSERT(cret != GST_STATE_CHANGE_FAILURE, "Unable to set the pipeline to the playing state");

    bus = gst_element_get_bus(data.pipeline);
    do
    {
        msg = gst_bus_timed_pop_filtered(bus, GST_CLOCK_TIME_NONE, 
                                         GstMessageType(GST_MESSAGE_STATE_CHANGED | 
                                                        GST_MESSAGE_ERROR | 
                                                        GST_MESSAGE_EOS));

        if (msg != nullptr) 
        {
            GError *err; 
            char *debug_info; 
            
            switch (GST_MESSAGE_TYPE(msg)) 
            {
            case GST_MESSAGE_ERROR:
                gst_message_parse_error(msg, &err, &debug_info); 
                g_printerr("Error received from element %s: %s\n", GST_OBJECT_NAME(msg->src), err->message); 
                g_printerr("Debugging information: %s\n", debug_info ? debug_info : "none"); 
                g_clear_error(&err); 
                g_free(debug_info); 
                terminate = true; 
                break; 
            case GST_MESSAGE_EOS:
                g_print("End-Of-Stream reached.\n"); 
                terminate = true; 
                break; 
            case GST_MESSAGE_STATE_CHANGED:
                /* We are only interested in state-changed messages from the pipeline */
                if (GST_MESSAGE_SRC(msg) == GST_OBJECT(data.pipeline)) 
                {
                    GstState old_state, new_state, pending_state; 
                    gst_message_parse_state_changed(msg, &old_state, &new_state, &pending_state); 
                    g_print("Pipeline state changed from %s to %s:\n", 
                            gst_element_state_get_name(old_state), gst_element_state_get_name(new_state));

//                  char buf[42];
//                  snprintf(buf, 43, "graph_%d.dot", new_state);
//                  GST_DEBUG_BIN_TO_DOT_FILE(bin, GST_DEBUG_GRAPH_SHOW_ALL, buf);
                }
                break; 
            default:
                /* We should not reach here */
                g_printerr("Unexpected message received.\n"); 
                break;
            }
            gst_message_unref(msg);
        }
    } while(!terminate);

    // Free resources
    gst_object_unref(bus);
    gst_element_set_state(data.pipeline, GST_STATE_NULL);
    gst_object_unref(data.pipeline);
    return 0;
}

static void pad_added_handler(GstElement* src, GstPad* new_pad, CustomData* data)
{
    GstCaps* new_pad_caps;
    GstStructure* new_pad_struct;
    const gchar* new_pad_type;
    GstPadLinkReturn ret;
    GstPad* sink_pad = nullptr;
        
    g_print("Received new pad '%s' from '%s':\n", GST_PAD_NAME(new_pad), GST_ELEMENT_NAME(src));
    
    new_pad_caps = gst_pad_query_caps(new_pad, nullptr);
    if (new_pad_caps == nullptr) 
    {
        g_print("error at gst_pad_get_allowed_caps\n");
        goto exit;
    }

    new_pad_struct = gst_caps_get_structure(new_pad_caps, 0); 
    if (new_pad_struct == nullptr) 
    {
        g_print("error at gst_caps_get_structure\n");
        goto exit;
    }

    new_pad_type = gst_structure_get_name(new_pad_struct);
    if (new_pad_type == nullptr) 
    {
        g_print("error at gst_structure_get_name\n");
        goto exit;
    }

    if (g_str_has_prefix(new_pad_type, "audio/x-raw"))
    {
        sink_pad = gst_element_get_static_pad(data->audio_convert, "sink");
    } else if(g_str_has_prefix(new_pad_type, "video/x-raw"))
    {
        sink_pad = gst_element_get_static_pad(data->video_convert, "sink");
    } else
    {
        g_print("New pad has type '%s' which is not raw audio. Ignoring...\n", new_pad_type);
        goto exit;
    }

    if (gst_pad_is_linked(sink_pad)) 
    {
        g_print("Pad '%s' are already linked. ignoring..\n", gst_pad_get_name(sink_pad));
        goto exit;
    }

    ret = gst_pad_link(new_pad, sink_pad);
    if (GST_PAD_LINK_FAILED(ret)) 
    {
        g_print("type is '%s' but link failed\n", new_pad_type);
    } else
    {
        g_print("Link succeed from %s to %s (type='%s')\n", 
                gst_pad_get_name(new_pad), gst_pad_get_name(sink_pad), new_pad_type);
    }

    exit:
    if (new_pad_caps != nullptr) 
    {
        gst_caps_unref(new_pad_caps);
    }

    gst_object_unref(sink_pad);
}
