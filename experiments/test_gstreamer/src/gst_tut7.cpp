#include <gst/gst.h>


struct CustomData {
    GstElement* source;
    GstElement* convert;
};

static void pad_added_handler(GstElement* src, GstPad* new_pad, CustomData* data);

int main(int argc, char **argv) 
{ 
    using GstElementPtr = GstElement *; 
    using GstPadPtr = GstPad *; 
    
    GstElementPtr pipeline, source, convert, tee; 
    GstElementPtr audio_queue, audio_convert, audio_resample, audio_sink; 
    GstElementPtr video_queue, visual, video_convert, video_sink; 
    GstBus *bus; 
    GstPadTemplate *tee_src_pad_template; 
    GstPadPtr tee_audio_pad, tee_video_pad; 
    GstPadPtr queue_audio_pad, queue_video_pad; 
    
    gst_init(&argc, &argv); 
    
    /* Create the elements */
    source = gst_element_factory_make("uridecodebin", "audio_source"); 
    convert = gst_element_factory_make("audioconvert", "convert");
    tee = gst_element_factory_make("tee", "tee"); 
    audio_queue = gst_element_factory_make("queue", "audio_queue"); 
    audio_convert = gst_element_factory_make("audioconvert", "audio_convert"); 
    audio_resample = gst_element_factory_make("audioresample", "audio_resample"); 
    audio_sink = gst_element_factory_make("autoaudiosink", "audio_sink"); 
    video_queue = gst_element_factory_make("queue", "video_queue"); 
    visual = gst_element_factory_make("wavescope", "visual"); 
    video_convert = gst_element_factory_make("videoconvert", "video_convert"); 
    video_sink = gst_element_factory_make("autovideosink", "video_sink"); 
    
    /* Create the empty pipeline */
    pipeline = gst_pipeline_new("test-pipeline"); 
    
    if (!pipeline || !source || !convert || !tee || !audio_queue || !audio_convert || !audio_resample || !audio_sink || 
        !video_queue || !visual || !video_convert || !video_sink) 
    {
        g_printerr("Not all elements could be created.\n"); 
        return -1;
    }

    g_object_set (source, "uri", "http://docs.gstreamer.com/media/sintel_trailer-480p.webm", NULL);

    CustomData data;
    data.source = source;
    data.convert = convert;

    int handler_id = g_signal_connect(data.source, "pad-added", G_CALLBACK(pad_added_handler), &data);
    if (handler_id <= 0) 
    {
        g_printerr("failed to connect 'pad-added' signal\n");
        return -1;
    }
    
//  g_object_set(audio_source, "freq", 215.0f, NULL);
    g_object_set(visual, "shader", 2, "style", 3, NULL); 
    
    /* Link all elements that can be automatically linked because they have "Always" pads */
    gst_bin_add_many(GST_BIN(pipeline), source, convert, tee, 
                     audio_queue, audio_convert, audio_resample, audio_sink, 
                     video_queue, visual, video_convert, video_sink, NULL); 
    if (gst_element_link_many(convert, tee, NULL) != TRUE || 
        gst_element_link_many(audio_queue, audio_convert, audio_resample, audio_sink, NULL) != TRUE || 
        gst_element_link_many(video_queue, visual, video_convert, video_sink, NULL) != TRUE) 
    {
        g_printerr("Elements could not be linked.\n"); 
        gst_object_unref(pipeline); 
        return -1;
    }
    
    // Manually link the tee's request pads
    tee_src_pad_template = gst_element_class_get_pad_template(GST_ELEMENT_GET_CLASS(tee), "src_%u");

    tee_audio_pad = gst_element_request_pad(tee, tee_src_pad_template, NULL, NULL);
    g_print("Obtained requested pad %s for audio branch\n", gst_pad_get_name(tee_audio_pad));
    queue_audio_pad = gst_element_get_static_pad(audio_queue, "sink");

    tee_video_pad = gst_element_request_pad (tee, tee_src_pad_template, NULL, NULL);
    g_print ("Obtained request pad %s for video branch.\n", gst_pad_get_name (tee_video_pad));
    queue_video_pad = gst_element_get_static_pad(video_queue, "sink");
    
    if (gst_pad_link(tee_audio_pad, queue_audio_pad) != GST_PAD_LINK_OK || 
        gst_pad_link(tee_video_pad, queue_video_pad) != GST_PAD_LINK_OK) 
    {
        g_printerr("Tee could not be linked.\n"); 
        gst_object_unref(pipeline); 
        return -1;
    }
    gst_object_unref(queue_audio_pad); 
    gst_object_unref(queue_video_pad); 

    gst_element_set_state(pipeline, GST_STATE_PLAYING);

    bus = gst_element_get_bus(pipeline);
    GstMessage* msg = gst_bus_timed_pop_filtered(bus, GST_CLOCK_TIME_NONE, 
                                                 GstMessageType(GST_MESSAGE_ERROR | GST_MESSAGE_EOS));

    // playing is done, shutdown...
    gst_element_release_request_pad(tee, tee_audio_pad);
    gst_element_release_request_pad(tee, tee_video_pad);

    gst_object_unref(tee_audio_pad);
    gst_object_unref(tee_video_pad);

    if (msg) 
    {
        gst_message_unref(msg);
    }
    gst_object_unref(bus);
    gst_element_set_state(pipeline, GST_STATE_NULL);

    gst_object_unref(pipeline);
    return 0;
}


static void pad_added_handler(GstElement* src, GstPad* new_pad, CustomData* data)
{
    GstCaps* new_pad_caps;
    GstStructure* new_pad_struct;
    const gchar* new_pad_type;
    GstPadLinkReturn ret;
    GstPad* sink_pad = nullptr;
        
    g_print("Received new pad '%s' from '%s':\n", GST_PAD_NAME(new_pad), GST_ELEMENT_NAME(src));
    
    new_pad_caps = gst_pad_query_caps(new_pad, nullptr);
    if (new_pad_caps == nullptr) 
    {
        g_print("error at gst_pad_get_allowed_caps\n");
        goto exit;
    }

    new_pad_struct = gst_caps_get_structure(new_pad_caps, 0); 
    if (new_pad_struct == nullptr) 
    {
        g_print("error at gst_caps_get_structure\n");
        goto exit;
    }

    new_pad_type = gst_structure_get_name(new_pad_struct);
    if (new_pad_type == nullptr) 
    {
        g_print("error at gst_structure_get_name\n");
        goto exit;
    }

    if (g_str_has_prefix(new_pad_type, "audio/x-raw"))
    {
        sink_pad = gst_element_get_static_pad(data->convert, "sink");
    } else
    {
        g_print("New pad has type '%s' which is not raw audio. Ignoring...\n", new_pad_type);
        goto exit;
    }

    if (gst_pad_is_linked(sink_pad)) 
    {
        g_print("Pad '%s' are already linked. ignoring..\n", gst_pad_get_name(sink_pad));
        goto exit;
    }

    ret = gst_pad_link(new_pad, sink_pad);
    if (GST_PAD_LINK_FAILED(ret)) 
    {
        g_print("type is '%s' but link failed\n", new_pad_type);
        goto exit;
    } else
    {
        g_print("Link succeed from %s to %s (type='%s')\n", 
                gst_pad_get_name(new_pad), gst_pad_get_name(sink_pad), new_pad_type);
    }

    exit:
    if (new_pad_caps) 
    {
        gst_caps_unref(new_pad_caps);
    }
    if (sink_pad) 
    {
        gst_object_unref(sink_pad); 
    }
}
