#include <gst/gst.h>

struct CustomData
{
    GstElement* playbin = nullptr;
    bool playing = false;
    bool terminate = false;
    gboolean seek_enabled = false;
    bool seek_done = false;
    gint64 duration = 0;
};

#define MY_ASSERT(exp, msg) { \
    if(!(exp)) \
    { \
        g_printerr("%s\n", msg); \
        if(data.playbin) { gst_object_unref (data.playbin); } \
        return -1; \
    } \
}

static void handle_message(CustomData* data, GstMessage* msg);

int main(int argc, char** argv)
{
    CustomData data;

    gst_init(&argc, &argv);

    data.playbin = gst_element_factory_make("playbin", "playbin");
    MY_ASSERT(data.playbin, "Failed to create element playbin element");

    // Set the URI to play
    g_object_set(data.playbin, "uri", "http://docs.gstreamer.com/media/sintel_trailer-480p.webm", NULL);

    GstStateChangeReturn cret = gst_element_set_state(data.playbin, GST_STATE_PLAYING);
    MY_ASSERT(cret != GST_STATE_CHANGE_FAILURE, "Failed to set playbin to playing state");

    GstBus* bus = gst_element_get_bus(data.playbin);
    MY_ASSERT(bus, "failed to get bus of playbin");

    while (!data.terminate) 
    {
        GstMessage* msg = gst_bus_timed_pop_filtered(bus, 1*GST_SECOND, 
                                                     GstMessageType(GST_MESSAGE_STATE_CHANGED | GST_MESSAGE_ERROR | GST_MESSAGE_EOS | GST_MESSAGE_DURATION));
        if (msg != NULL) 
        {
            handle_message(&data, msg);
        } else if(data.playing)
        {
            GstFormat fmt = GST_FORMAT_TIME;
            gint64 current = -1;

            /* Query the current position of the stream */
            if (!gst_element_query_position(data.playbin, fmt, &current)) 
            {
                g_printerr("Could not query current position.\n");
                break;
            }
            
            /* If we didn't know it yet, query the stream duration */
            if (!GST_CLOCK_TIME_IS_VALID(data.duration)) 
            {
                if (!gst_element_query_duration(data.playbin, fmt, &data.duration)) 
                {
                    g_printerr("Could not query current duration.\n");
                    break;
                }
            }

            g_print("Position %" GST_TIME_FORMAT " / %" GST_TIME_FORMAT "\n",
                    GST_TIME_ARGS(current), GST_TIME_ARGS(data.duration));

            if (data.seek_enabled && !data.seek_done && current > 10 * GST_SECOND) 
            {
                g_print("\nReached 10s, performing seek...\n");
                gst_element_seek_simple(data.playbin, GST_FORMAT_TIME,
                                        GstSeekFlags(GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_KEY_UNIT), 
                                        30 * GST_SECOND);
                data.seek_done = true;
            }
        }
    }

    gst_object_unref(bus);
    gst_element_set_state(data.playbin, GST_STATE_NULL);
    gst_object_unref(data.playbin);
    return 0;
}

static void handle_message(CustomData* data, GstMessage* msg)
{
    GError* err;
    char* debug_info;

    switch (GST_MESSAGE_TYPE(msg)) 
    {
    case GST_MESSAGE_ERROR:
        gst_message_parse_error(msg, &err, &debug_info);
        g_printerr("Error received from element %s: %s\n", GST_OBJECT_NAME(msg->src), err->message);
        g_printerr("Debugging info: %s\n", debug_info ? debug_info : "none");
        g_clear_error(&err);
        data->terminate = true;
        break;
    case GST_MESSAGE_EOS:
        g_print("End-of-Stream reached.\n");
        data->terminate = true;
        break;
    case GST_MESSAGE_DURATION:
        // The duration has changed, mark the current one as invalid
        data->duration = GST_CLOCK_TIME_NONE;
        break;
    case GST_MESSAGE_STATE_CHANGED:
    {
        GstState old_state, new_state, pending_state;
        gst_message_parse_state_changed(msg, &old_state, &new_state, &pending_state);

        if (GST_MESSAGE_SRC(msg) == GST_OBJECT(data->playbin)) 
        {
            g_print("Pipeline changed from %s to %s\n", 
                    gst_element_state_get_name(old_state), gst_element_state_get_name(new_state));
            data->playing = (new_state == GST_STATE_PLAYING);

            if (data->playing) 
            {
                // check if seeking is possible
                GstQuery* query = gst_query_new_seeking(GST_FORMAT_TIME);
                if (gst_element_query(data->playbin, query)) 
                {
                    gint64 start, end;
                    gst_query_parse_seeking(query, NULL, &data->seek_enabled, &start, &end);
                    if (data->seek_enabled) 
                    {
                        g_print("Seeking is ENABLED from %" GST_TIME_FORMAT " til %" GST_TIME_FORMAT "\n",
                                GST_TIME_ARGS(start), GST_TIME_ARGS(end));
                    } else
                    {
                        g_print("Seeking is DISABLED for this stream\n");
                    }
                } else
                {
                    g_printerr("Seeking query failed");
                }
                gst_query_unref(query);
            } 
        }
    } break;
    default:
        g_printerr("Unexpected message received\n");
        break;
    }
    gst_message_unref(msg);
}
