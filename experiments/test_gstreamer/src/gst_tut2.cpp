#include <gst/gst.h>

#define MY_ASSERT(exp, msg) { \
    if(!(exp)) \
    { \
        g_printerr(msg); g_printerr("\n"); \
        if(pipeline) { gst_object_unref (pipeline); } \
        return -1; \
    } \
}

int main(int argc, char **argv) 
{ 
    GstElement *pipeline = nullptr; 
    GstElement *source = nullptr; 
    GstElement* vertigotv = nullptr;
    GstElement* videoconvert = nullptr;
    GstElement *sink = nullptr; 
    
    GstBus *bus; 
    GstMessage *msg; 
//  GstStateChangeReturn ret;
    
    gst_init(&argc, &argv); 
    
    // Create the elements
    source = gst_element_factory_make("videotestsrc", "source"); 
    vertigotv = gst_element_factory_make("vertigotv", "vertigotv");
    videoconvert = gst_element_factory_make("videoconvert", "videoconvert");
    sink = gst_element_factory_make("autovideosink", "sink"); 
    
    // Create the empty pipeline
    pipeline = gst_pipeline_new("test-pipeline"); 
    
    MY_ASSERT(pipeline && source && vertigotv && videoconvert && sink, "Not all elements could be created!"); 
    
    GstBin* bin = GST_BIN(pipeline);

    // build the pipeline
    bool ret = gst_bin_add(bin, source); 
    MY_ASSERT(ret, "Failed to add source"); 
    
    ret = gst_bin_add(bin, vertigotv);
    MY_ASSERT(ret, "Failed to add vertigo");

    ret = gst_bin_add(bin, videoconvert);
    MY_ASSERT(ret, "Failed to add videoconvert");

    ret = gst_bin_add(bin, sink); 
    MY_ASSERT(ret, "Failed to add sink"); 
    
    // link the elements
    ret = gst_element_link(source, vertigotv); 
    MY_ASSERT(ret, "source and vertigotv elements could not be linked."); 
    
    ret = gst_element_link(vertigotv, videoconvert);
    MY_ASSERT(ret, "vertigotv and videoconvert link failed");

    ret = gst_element_link(videoconvert, sink); 
    MY_ASSERT(ret, "videoconvert and sink elements could not be linked."); 

    // modify the source's properties
    g_object_set(source, "pattern", 0, nullptr); 
    
    // start playing
    GstStateChangeReturn cret = gst_element_set_state(pipeline, GST_STATE_PLAYING); 
    MY_ASSERT(cret!=GST_STATE_CHANGE_FAILURE, "Unable to set the pipeline to the playing state"); 
    
    bus = gst_element_get_bus(pipeline); 
    
    msg = gst_bus_timed_pop_filtered(bus, GST_CLOCK_TIME_NONE, GstMessageType(GST_MESSAGE_ERROR | GST_MESSAGE_EOS)); 
    
    // parse the message
    if (msg != NULL) 
    {
        GError *err; 
        gchar *debug_info; 
        
        switch (GST_MESSAGE_TYPE(msg)) 
        {
        case GST_MESSAGE_ERROR:
            gst_message_parse_error(msg, &err, &debug_info); 
            g_printerr("Error received from element %s: %s\n", GST_OBJECT_NAME(msg->src), err->message); 
            g_printerr("Debugging information: %s\n", debug_info ? debug_info : "none"); 
            g_clear_error(&err); 
            g_free(debug_info); 
            break; 
        case GST_MESSAGE_EOS:
            g_print("End-of-Stream reached\n"); 
            break; 
        default:
            g_printerr("Unexpected message received\n"); 
            break;
        }
        
        gst_message_unref(msg);
    }
    
    gst_object_unref(bus); 
    gst_element_set_state(pipeline, GST_STATE_NULL); 
    gst_object_unref(pipeline); 
    return 0;
}

