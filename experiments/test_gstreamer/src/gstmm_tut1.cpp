#include "common_gstmm.hpp"

int main(int argc, char** argv)
{
    Gst::init(argc, argv);

    ElementPtr pipeline = Gst::Parse::launch("playbin uri=http://docs.gstreamer.com/media/sintel_trailer-480p.webm");
    ASSERT(pipeline);

    ASSERT(pipeline->set_state(Gst::STATE_PLAYING) != Gst::STATE_CHANGE_FAILURE);

    Glib::RefPtr<Gst::Bus> bus = pipeline->get_bus();
    // Wait for a message
    Glib::RefPtr<Gst::Message> msg = bus->pop(Gst::CLOCK_TIME_NONE, Gst::MESSAGE_ERROR | Gst::MESSAGE_EOS);

    return 0;
}

