#include <gstreamermm.h>
#include <gstreamermm/playbin.h>
#include <glibmm/main.h>
#include <iostream>

using BusPtr = Glib::RefPtr<Gst::Bus>;
using MessagePtr = Glib::RefPtr<Gst::Message>;
using MainLoopPtr = Glib::RefPtr<Glib::MainLoop>;

bool on_bus_callback(const BusPtr& bus, 
                     const MessagePtr& message,
                     const MainLoopPtr& loop)
{
    switch (message->get_message_type()) 
    {
    case Gst::MESSAGE_EOS:
        loop->quit();
        break;
    default:
        std::cerr << "Not handling message " << message->get_message_type() << std::endl;
        break;
    }
    return true;
}

int main(int argc, char** argv)
{
    Gst::init(argc, argv);

    MainLoopPtr loop = Glib::MainLoop::create();
    Glib::RefPtr<Gst::PlayBin> playbin = Gst::PlayBin::create();
    std::string file_name = argv[1];
    playbin->set_property("uri", file_name);

    BusPtr bus = playbin->get_bus();
    bus->add_watch(sigc::bind(sigc::ptr_fun(&on_bus_callback), loop));
    Gst::StateChangeReturn ret = playbin->set_state(Gst::STATE_PLAYING);
    if (ret == Gst::STATE_CHANGE_FAILURE) 
    {
        std::cerr << "Failed to start playing" << std::endl;
        return -1;
    }

    loop->run();
    std::cout << "done!" << std::endl;
    playbin->set_state(Gst::STATE_NULL);
    return 0;
}

