#include "common_gstmm.hpp"

#include <cstring>

struct CustomData
{
    ElementPtr video_convert;
    ElementPtr audio_convert;

    void pad_added(const PadPtr& pad);
};

int main(int argc, char** argv)
{
    Gst::init(argc, argv);

    CustomData data;

    ElementPtr uridecodebin = Gst::ElementFactory::create_element("uridecodebin");
    data.audio_convert = Gst::ElementFactory::create_element("audioconvert");
    ElementPtr audio_sink = Gst::ElementFactory::create_element("autoaudiosink");
    data.video_convert = Gst::ElementFactory::create_element("videoconvert");
    ElementPtr video_sink = Gst::ElementFactory::create_element("autovideosink");

    PipelinePtr pipeline = Gst::Pipeline::create();

    // FIRST add!!
    pipeline->add(uridecodebin)->add(data.audio_convert)->add(audio_sink);
    pipeline->add(data.video_convert)->add(video_sink);

    // ALWAYS link AFTER adding!!!
    data.audio_convert->link(audio_sink);
    data.video_convert->link(video_sink);

//  auto uri_str = std::string("http://docs.gstreamer.com/media/sintel_trailer-480p.webm");
    auto uri_str = std::string("file:///home/benjamin/Documents/holo3d.tv/experiments/test_gstreamer/sintel-trailer-480p.webm");
    uridecodebin->property("uri", uri_str);

    CONNECT(uridecodebin, pad_added, [&] (const PadPtr& pad) { data.pad_added(pad); });

    Gst::StateChangeReturn cret = pipeline->set_state(Gst::STATE_PLAYING);
    ASSERT_THROW(cret != Gst::STATE_CHANGE_FAILURE, "Failed to set pipeline to playing state");

    BusPtr bus = pipeline->get_bus();

    bool running = true;
    while (running) 
    {
        MessagePtr msg = bus->pop(Gst::CLOCK_TIME_NONE, 
                                  Gst::MESSAGE_STATE_CHANGED | Gst::MESSAGE_ERROR | Gst::MESSAGE_EOS);
        if (msg) 
        {
            switch (msg->get_message_type()) 
            {
            case Gst::MESSAGE_ERROR:
            {
                ErrorMsgPtr err = ErrorMsgPtr::cast_static(msg);
                ERROR("Error received from element '{}': {}", err->get_source()->get_name(), err->parse().what());
                ERROR("Debugging information: {}", err->parse_debug());
                running = false;
            } break;
            case Gst::MESSAGE_EOS:
                INFO("End of stream reached");
                running = false;
                break;
            case Gst::MESSAGE_STATE_CHANGED:
                if (msg->get_source() == pipeline) 
                {
                    MsgStateChgPtr chgmsg = MsgStateChgPtr::cast_static(msg);
                    Gst::State old_state = chgmsg->parse_old();
                    Gst::State new_state = chgmsg->parse();
                    INFO("Pipeline changed from {} to {}", old_state, new_state);

//                  GstBin* bin = GST_BIN(pipeline->gobj());
//                  char buf[42];
//                  snprintf(buf, 43, "graph_%d", new_state);
//                  GST_DEBUG_BIN_TO_DOT_FILE(bin, GST_DEBUG_GRAPH_SHOW_ALL, buf);
                }
                break;
            default:
                ERROR("Unexpected message received");
                break;
            }
        }
    }

    pipeline->set_state(Gst::STATE_NULL);
}

void CustomData::pad_added(const PadPtr& new_pad)
{
    INFO("Received new pad '{}' from {}", new_pad->get_name(), new_pad->get_parent_element()->get_name());

    CapsPtr new_caps = new_pad->query_caps(CapsPtr());
    const Gst::Structure new_struct = new_caps->get_structure(0);
    std::string new_pad_type = new_struct.get_name();

    PadPtr sink_pad;
    if (new_pad_type.find("audio/x-raw") == 0) 
    {
        sink_pad = this->audio_convert->get_static_pad("sink");
    } else if(new_pad_type.find("video/x-raw") == 0)
    {
        sink_pad = this->video_convert->get_static_pad("sink");
    } else
    {
        WARN("New pad has type {} which is not connectable. Ignoring...", new_pad_type);
        return;
    }

    if (sink_pad->is_linked()) 
    {
        ERROR("Sink pad {} is already linked. Ignoring...", sink_pad->get_name());
        return;
    }

    Gst::PadLinkReturn ret = new_pad->link(sink_pad);
    if (ret == Gst::PAD_LINK_OK) 
    {
        INFO("Link between {}:{} and {}:{} succeeded (type='{}')",
             new_pad->get_parent_element()->get_name(), new_pad->get_name(), 
             sink_pad->get_parent_element()->get_name(), sink_pad->get_name(),
             new_pad_type);
    } else 
    {
        ERROR("Link failed!");
    }
}

