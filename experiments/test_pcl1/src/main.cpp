#include <iostream>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>

typedef pcl::PointCloud<pcl::PointXYZ> cloud_t;

#define INFO(msg) std::cout << msg << std::endl;

void print_cloud(const cloud_t::Ptr& cloud)
{
    for (size_t i = 0; i<cloud->points.size(); i++)
    {
        INFO("    (" <<
             cloud->points[i].x << "," << 
             cloud->points[i].y << "," << 
             cloud->points[i].z << ")");
    }
}

int main(int argc, char** argv)
{
    cloud_t::Ptr cloud(new cloud_t);
    cloud_t::Ptr cloud_filtered(new cloud_t);

    cloud->width = 5;
    cloud->height = 1;
    cloud->points.resize(cloud->width * cloud->height);

    for (size_t i = 0; i < cloud->points.size(); i++)
    {
        cloud->points[i].x = 1024 * rand() / (RAND_MAX + 1.0f);
        cloud->points[i].y = 1024 * rand() / (RAND_MAX + 1.0f);
        cloud->points[i].z = 1024 * rand() / (RAND_MAX + 1.0f);
    }

    INFO("Before filtering");
    print_cloud(cloud);

    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud(cloud);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(0.0f, 1.0f);
    pass.filter(*cloud_filtered);

    INFO("After filtering");
    print_cloud(cloud_filtered);

    return (0);
}
