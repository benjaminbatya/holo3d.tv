#include <iostream>

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>

#define ERROR(msg) { std::cerr << msg << std::endl; }

void show_help(char* program_name)
{
    ERROR(std::endl <<
          "Usage: " << program_name << " cloud_filename.[pcd|ply]" << std::endl <<
          "-h: Show this help. ");
}

typedef pcl::PointCloud<pcl::PointXYZ> cloud_t;

int main(int argc, char** argv)
{
    if (pcl::console::find_switch(argc, argv, "-h") ||
        pcl::console::find_switch(argc, argv, "--help"))
    {
        show_help(argv[0]);
        return 0;
    }

    std::vector<int> filenames;
    bool file_is_pcd = false;

    filenames = pcl::console::parse_file_extension_argument(argc, argv, ".ply");

    if (filenames.size() != 1)
    {
        filenames = pcl::console::parse_file_extension_argument(argc, argv, ".pcd");

        if (filenames.size() != 1)
        {
            show_help(argv[0]);
            return -1;
        } else
        {
            file_is_pcd = true;
        }
    }

    cloud_t::Ptr source_cloud ( new cloud_t());

    if (file_is_pcd)
    {
        if (pcl::io::loadPCDFile(argv[filenames[0]], *source_cloud) < 0)
        {
            ERROR("Error loading pcd file " << argv[filenames[0]]);
            show_help(argv[0]);
            return -1;
        }
    } else
    {
        if (pcl::io::loadPLYFile(argv[filenames[0]], *source_cloud) < 0)
        {
            ERROR("Error loading PLY file" << argv[filenames[0]]);
            show_help(argv[0]);
            return -1;
        }
    }

    ERROR("cloud is organized: " << source_cloud->isOrganized());

    if (source_cloud->isOrganized())
    {
        ERROR("SIZE = " << source_cloud->width << "x" << source_cloud->height);
    } else
    {
        ERROR("SIZE = " << source_cloud->size());
    }

    float theta = M_PI/4;
    Eigen::Affine3f transform = Eigen::Affine3f::Identity();

    // Translate a
    transform.translation() << 2.5, 0.0, 0.0;
    transform.rotate(Eigen::AngleAxisf (theta, Eigen::Vector3f::UnitZ()));

    ERROR("Affine transform:");
    ERROR(transform.matrix());

    cloud_t::Ptr transformed_cloud ( new cloud_t());

    pcl::transformPointCloud(*source_cloud, *transformed_cloud, transform);
    ERROR("Point Cloud Colors : white = original point cloud" << std::endl <<
          "                     red   = transformed point cloud");
    pcl::visualization::PCLVisualizer viewer("Matrix transformation example");

    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> source_cloud_color_handler(source_cloud, 255, 255, 255); // white

    viewer.addPointCloud(source_cloud, source_cloud_color_handler, "original_cloud");

    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> transformed_cloud_color_handler(transformed_cloud, 230, 20, 20); // red
    viewer.addPointCloud(transformed_cloud, transformed_cloud_color_handler, "transformed_cloud");

    viewer.addCoordinateSystem(1.0, "cloud", 0);
    viewer.setBackgroundColor(0.05, 0.05, 0.05, 0);
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "original_cloud");
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "transformed_cloud");

    while (!viewer.wasStopped())
    {
        viewer.spinOnce();
    }

    return 0;
}
