#!/usr/bin/python
# Uses perfect_hash.py to create a decoding table given the input values

import perfect_hash

DATA_FILE = "values.csv"
FILE_NAME = "depth_decoder_map"
CPP_FILE = FILE_NAME + ".cpp"
HPP_FILE = FILE_NAME + ".hpp"

dict = {}
value = 0

# Calculates a distinct hash function given a set of ints. Each value of the
# integer d results in a different hash value.
def hash( d, key ):
    if d == 0: d = 0x01000193

    # Use the FNV algorithm from http://isthe.com/chongo/tech/comp/fnv/ 
    for num in key:
        d = ( (d * 0x01000193) ^ num ) & 0xffffffff;

    return d

print "Reading values"
for line in open(DATA_FILE, 'rt').readlines():
    line = line.strip()
    key = tuple( [int(x) for x in line.split(", ")] )
    # print "key = %s, value = %d" % ("," .join([str(x) for x in key]), value)
    dict[key] = value
    value += 1

#print "Creating perfect hash"
(G, V) = perfect_hash.CreateMinimalPerfectHash( hash, dict )

#print "Dumping tables"
#keys = [
#    (255,241,144),
#    (255,242,114),
#    (230,246,139)
#    ]
#for key in keys:
#    value = perfect_hash.PerfectHashLookup(hash, G, V, key)
#    print "Key %s = value %d" % (",".join([str(x) for x in key]), value)

# print "len(G) = %d" % len(G)
# print "len(V) = %d" % len(V)
assert len(G) == len(V)

#for i in range(0, 250):
#    print G[i]

with open(CPP_FILE, 'wt') as file:
    # file.write("Hello world %s %d" % ("test", 42))
    file.write(("/**\n"
                " * This is an autogenerated map from python file create_depth_tables.py\n"
                " * The first array is the G table, then the V table and then a lookup function\n"
                " * WARNING: DO NOT MODIFY!\n"
                " */\n\n"
                "#include \"%s\"\n\n"
                "namespace holo3d {\n\n"
                ) % (HPP_FILE))
    
    file.write("const uint32_t SIZE_DEPTH_DECODE_MAP = %d;\n\n" % len(G))
    
    file.write("const int16_t DEPTH_DECODE_G[] = {\n")  

    for i in range(0, len(G)):
        file.write("%d, " % G[i])
        if(i % 20 == 19):
            file.write("\n")

    file.write("};\n\n")

    file.write("const int16_t DEPTH_DECODE_V[] = {\n")  

    for i in range(0, len(V)):
        file.write("%d, " % V[i])
        if(i % 20 == 19):
            file.write("\n")

    file.write("};\n\n")

    file.write("}; // namespace holo3d\n")

