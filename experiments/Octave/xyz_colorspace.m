#! /usr/bin/octave -qf

# This is a test to get the depth encoding algorithm from 
# "Adapting Standard Video Codecs for Depth Streaming"

mat = [
  2.76, 1.75, 1.13;
  1.00, 4.59, 0.06; 
  0.00, 0.05, 5.59
];

inv_mat = inv(mat);

inv_mat
