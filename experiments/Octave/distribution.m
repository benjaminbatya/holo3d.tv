
range = 1000
histogram = zeros(range);

for i=1:100
  r1 = rand;
  r2 = rand;

  key = sqrt(-2.0 * log(r1)) * cos(2.0 * pi * r2) * 1000;

  key = uint16(key);
  
  if key == 0
    continue;
  endif
  
  histogram(key) += 1;
  
endfor

plot(histogram);

keyboard;