/*****************************************************************************
*                                                                            *
*  OpenNI 2.x Alpha                                                          *
*  Copyright (C) 2012 PrimeSense Ltd.                                        *
*                                                                            *
*  This file is part of OpenNI.                                              *
*                                                                            *
*  Licensed under the Apache License, Version 2.0 (the "License");           *
*  you may not use this file except in compliance with the License.          *
*  You may obtain a copy of the License at                                   *
*                                                                            *
*      http://www.apache.org/licenses/LICENSE-2.0                            *
*                                                                            *
*  Unless required by applicable law or agreed to in writing, software       *
*  distributed under the License is distributed on an "AS IS" BASIS,         *
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
*  See the License for the specific language governing permissions and       *
*  limitations under the License.                                            *
*                                                                            *
*****************************************************************************/
#include <stdio.h>
#include <OpenNI2/OpenNI.h>
#include <vector>
#include <memory>

#include "OniSampleUtilities.h"

using namespace openni;

void analyzeFrame(const VideoFrameRef& frame, const int device_idx)
{
	DepthPixel* pDepth;
	RGB888Pixel* pColor;

	int middleIndex = (frame.getHeight()+1)*frame.getWidth()/2;

	switch (frame.getVideoMode().getPixelFormat())
	{
	case PIXEL_FORMAT_DEPTH_1_MM:
	case PIXEL_FORMAT_DEPTH_100_UM:
		pDepth = (DepthPixel*)frame.getData();
		printf("[%08llu] dev=%u %8d\n", (long long)frame.getTimestamp(), device_idx,
			pDepth[middleIndex]);
		break;
	case PIXEL_FORMAT_RGB888:
		pColor = (RGB888Pixel*)frame.getData();
		printf("[%08llu] dev=%u 0x%02x%02x%02x\n", (long long)frame.getTimestamp(), device_idx,
			pColor[middleIndex].r&0xff,
			pColor[middleIndex].g&0xff,
			pColor[middleIndex].b&0xff);
		break;
	default:
		printf("Unknown format\n");
	}
}

class PrintCallback : public VideoStream::NewFrameListener
{
public:
    PrintCallback(int deviceIdx)
    : VideoStream::NewFrameListener()
    , m_device_idx(deviceIdx)
    {

    }

	void onNewFrame(VideoStream& stream)
	{
		stream.readFrame(&m_frame);

		analyzeFrame(m_frame, m_device_idx);
	}

    Device m_device;
    VideoStream m_color_stream;
    VideoStream m_depth_stream;

    int m_device_idx;

	VideoFrameRef m_frame;
};

class OpenNIDeviceListener : public OpenNI::DeviceConnectedListener,
									public OpenNI::DeviceDisconnectedListener,
									public OpenNI::DeviceStateChangedListener
{
public:
	virtual void onDeviceStateChanged(const DeviceInfo* pInfo, DeviceState state) 
	{
		printf("Device \"%s\" error state changed to %d\n", pInfo->getUri(), state);
	}

	virtual void onDeviceConnected(const DeviceInfo* pInfo)
	{
		printf("Device \"%s\" connected\n", pInfo->getUri());
	}

	virtual void onDeviceDisconnected(const DeviceInfo* pInfo)
	{
		printf("Device \"%s\" disconnected\n", pInfo->getUri());
	}
};



int main()
{
	Status rc = OpenNI::initialize();
	if (rc != STATUS_OK)
	{
		printf("Initialize failed\n%s\n", OpenNI::getExtendedError());
		return 1;
	}

	OpenNIDeviceListener devicePrinter;

	OpenNI::addDeviceConnectedListener(&devicePrinter);
	OpenNI::addDeviceDisconnectedListener(&devicePrinter);
	OpenNI::addDeviceStateChangedListener(&devicePrinter);

	Array<openni::DeviceInfo> deviceList;
	OpenNI::enumerateDevices(&deviceList);

    typedef std::shared_ptr<PrintCallback> callback_t;
    std::vector<callback_t> callbacks;

    for (int i = 0; i < deviceList.getSize(); ++i)
	{
		printf("Device \"%s\" already connected\n", deviceList[i].getUri());

        callback_t callback(new PrintCallback(i));

        rc = callback->m_device.open(deviceList[i].getUri());
    	if (rc != STATUS_OK)
    	{
    		printf("Couldn't open device '%s'\n%s\n", deviceList[i].getUri(), OpenNI::getExtendedError());
    		return 2;
    	}

    	if (callback->m_device.getSensorInfo(SENSOR_DEPTH) != NULL)
    	{
    		rc = callback->m_depth_stream.create(callback->m_device, SENSOR_DEPTH);
    		if (rc != STATUS_OK)
    		{
    			printf("Couldn't create depth stream on device '%s'\n%s\n", deviceList[i].getUri(), OpenNI::getExtendedError());
    		}
    	}
    	rc = callback->m_depth_stream.start();
    	if (rc != STATUS_OK)
    	{
    		printf("Couldn't start the depth stream on device '%s'\n%s\n", deviceList[i].getUri(), OpenNI::getExtendedError());
    	}

    	// Register to new frame
    	callback->m_depth_stream.addNewFrameListener(callback.get());

        callbacks.push_back(callback);
    }
	// Wait while we're getting frames through the printer
	while (!wasKeyboardHit())
	{
		Sleep(100);
	}

    for (size_t i=0; i<callbacks.size(); i++)
    {
        callback_t& callback = callbacks[i];
        callback->m_depth_stream.removeNewFrameListener(callback.get());


        callback->m_depth_stream.stop();
        callback->m_depth_stream.destroy();
        callback->m_device.close();
    }

	OpenNI::shutdown();

	return 0;
}

