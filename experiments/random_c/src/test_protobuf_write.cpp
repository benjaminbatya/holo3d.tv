#include <iostream>
#include <fstream>
#include <string>

#include <algorithm>

#include "addressbook.pb.h"

using namespace std;

using namespace tutorial;

void PromptForAddress(Person* person)
{
    cout << "Enter person ID number: ";
    int id;
    cin >> id;
    person->set_id(id);
    cin.ignore(256, '\n');

    cout << "Enter name:";
    getline(cin, *person->mutable_name());

    cout << "Enter email address (blank for none): ";
    string email;
    getline(cin, email);
    if (!email.empty()) 
    {
        person->set_email(email);
    }

    while (true) 
    {
        cout << "Enter a phone number (or leave blank to finish): ";
        string number;
        getline(cin, number);
        if (number.empty()) 
        {
            break;
        }

        Person::PhoneNumber* phone_number = person->add_phone();
        phone_number->set_number(number);

        cout << "Is it a mobile, home, or work number? ";
        string type;
        getline(cin, type);
        
        std::transform(type.begin(), type.end(), type.begin(), ::toupper);

        Person::PhoneType type_enum;
        if (Person::PhoneType_Parse(type, &type_enum))
        {
//          cout << "Parsed " << type_enum << endl;
            phone_number->set_type(type_enum);
        }
    }
}

int main(int argc, char** argv)
{
    // Make sure the version is correct
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    if (argc != 2) 
    {
        cerr << "Usage: " << argv[0] << " ADDRESS_BOOK_FILE" << endl;
        return -1;
    }

    tutorial::AddressBook address_book;

    {
        // Read in
        fstream input(argv[1], ios::in | ios::binary);
        if (!input) 
        {
            cout << argv[1] << ": File not found. Creating a new file." << endl;
        } else if(!address_book.ParseFromIstream(&input)) 
        {
            cerr << "Failed to parse address book." << endl;
            return -1;
        }
    }

    PromptForAddress(address_book.add_person());

    {
        // Write out
        fstream output(argv[1], ios::out | ios::trunc | ios::binary);
        if (!address_book.SerializeToOstream(&output)) 
        {
            cerr << "Failed to write address book." << endl;
            return -1;
        }
    }

    google::protobuf::ShutdownProtobufLibrary();

    return 0;
}
