#include <iostream>
#include <thread>
#include <chrono>

void thread_func()
{
    std::this_thread::sleep_for(std::chrono::nanoseconds(1));
    std::cout << "Hello world" << std::endl;
}

static const size_t NUM_THREADS = 10;

int main()
{
    std::thread threads[NUM_THREADS];

    for (size_t i=0; i<NUM_THREADS; i++) 
    {
        threads[i] = std::thread(thread_func);
    }

    std::cout << "main: spawned all threads" << std::endl;

    for (size_t i=0; i<NUM_THREADS; i++) 
    {
        threads[i].join();
    }
}
