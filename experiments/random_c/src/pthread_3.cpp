#include <pthread.h>
#include <stdio.h>

#define NUM_THREADS 5

struct thread_data
{
    size_t thread_id;
    int sum;
    const char* message;
};

thread_data thread_data_array[NUM_THREADS];

void* print_hello(void* thread_id)
{
    const thread_data* data = (thread_data*)thread_id; 

    printf("Thread %ld: sum=%d, str=%s\n", data->thread_id, data->sum, data->message);

    pthread_exit(NULL);
}

int main(int argc, char** argv)
{
    pthread_t threads[NUM_THREADS];

    for (size_t t=0; t<NUM_THREADS; t++) 
    {
        printf("In main: creating thread %ld\n", t);

        thread_data_array[t].thread_id = t;
        thread_data_array[t].sum = t+42;
        thread_data_array[t].message = "Good morning, Vietnam!!";

        int rc = pthread_create(&threads[t], NULL, print_hello, (void*)&thread_data_array[t]);

        if (rc) 
        {
            printf("ERROR: return code from pthread_create() is %d\n", rc);
            return(-1);
        }
    }

    pthread_exit(NULL);
}
