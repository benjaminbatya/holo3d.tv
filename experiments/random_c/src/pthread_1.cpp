#include <pthread.h>
#include <stdio.h>

#define NUM_THREADS 5

void* print_hello(void* thread_id)
{
    size_t tid = size_t(thread_id);

    printf("Hello World! From thread #%ld\n", tid);

    pthread_exit(NULL);
}

int main(int argc, char** argv)
{
    pthread_t threads[NUM_THREADS];

    for (size_t t=0; t<NUM_THREADS; t++) 
    {
        printf("In main: creating thread %ld\n", t);

        int rc = pthread_create(&threads[t], NULL, print_hello, (void*)t);

        if (rc) 
        {
            printf("ERROR: return code from pthread_create() is %d\n", rc);
            return(-1);
        }
    }

    pthread_exit(NULL);
}
