#include <iostream>
//#include <thread>
#include <vector>
#include <future>
#include <chrono>
//#include <algorithm>

#include "util_funcs.hpp"

void call()
{
//  INFO("Hello from async!");
    std::cout << "Hello from async!" << std::endl;
}

int main() 
{
    std::vector<std::future<int>> futures;

    for (int i=0; i<10; i++) 
    {
        futures.push_back( std::async(std::launch::async,
                                      [](int a) { std::this_thread::sleep_for(std::chrono::seconds(3)); return a*2; }, 
                                      i)); 
    }

    for (auto& f: futures) 
    {
        INFO("value={}", f.get());
    }

    return 0;
}
