#include <errno.h>
#include <moodycamel/readerwriterqueue.h>

#include "util_funcs.hpp"

#include <thread>

using namespace moodycamel;

int main()
{
    ReaderWriterQueue<int> q(100);

    q.enqueue(17);

    bool succeeded = false;

    succeeded = q.try_enqueue(18);
    ASSERT(succeeded);
    INFO("q = {}", succeeded);

    int number;
    succeeded = q.try_dequeue(number);
    ASSERT(succeeded && number == 17);

    INFO("number = {}", number);

    succeeded = q.pop();
    ASSERT(succeeded);

    int* front = q.peek();
    ASSERT(front == nullptr);

    BlockingReaderWriterQueue<int> q2;

    std::thread reader([&]
    {
        int item;
        for(int i=0; i<100; i++)
        {
            q2.wait_dequeue(item);
        }
    }); 

    std::thread writer([&]
    {
        for(int i=0; i<100; i++)
        {
            INFO("putting {} in queue", i);
            q2.enqueue(i);
        }
    }); 

    writer.join();
    reader.join();

    ASSERT(q2.size_approx() == 0);

}
