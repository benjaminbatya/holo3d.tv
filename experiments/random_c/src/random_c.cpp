
#include "pch_files.hpp"

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#include "util_funcs.hpp"

int exit_flag = 0;

void handler(int sig)
{
    INFO("Caught signal %d", sig);
    exit_flag = 1;
}

int main(int argc, char** argv)
{
    struct sigaction act;

    memset(&act, '\0', sizeof(act));

    act.sa_handler = &handler;


    if (sigaction(SIGHUP, &act, nullptr) < 0)
    {
        ERROR("sigaction");
        return 1;
    }

    while (!exit_flag) 
    {
    }

    INFO("Goodbye, cruel world!");

    return 0;
}
