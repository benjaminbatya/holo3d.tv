//Sample code to read in test cases:
#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <stdexcept>

using namespace std;

class Vec
{
public:
    float x;
    float y;

    Vec(float x=0.f, float y=0.f) : x(x), y(y) { }
    Vec& operator=(const Vec& that)
    {
        this->x = that.x;
        this->y = that.y;
        return *this;
    }
    Vec operator-(const Vec& that) const
    {
        return Vec(this->x-that.x, this->y-that.y);
    }
    
    float cross(const Vec& that) const
    {
        return this->x * that.y - that.x * this->y;
    }
};

ostream& operator<<(ostream& os, const Vec& v)
{
    os << "(" << v.x << ", " << v.y << ")";
    return os;
}

class Bridge
{
    int m_num;
    Vec m_start {0, 0};
    Vec m_offset {0, 0};

    friend ostream& operator<<(ostream& os, const Bridge& b);
public:
    bool operator==(const Bridge& that) const
    {
        return m_num == that.m_num;
    }
    bool operator<(const Bridge& that) const
    {
        return m_num < that.m_num;
    }

    bool parse(string line)
    {
        enum class PARSE_STATE
        {
            NUM,
            BEFORE_LAT1,
            LAT1,
            BEFORE_LONG1,
            LONG1,
            BEFORE_LAT2,
            LAT2,
            BEFORE_LONG2,
            LONG2,
            DONE
        };

        PARSE_STATE state = PARSE_STATE::NUM;

        string tok = "";
        Vec pt1, pt2;

#define PARSE_NUM(st, next_st, break_char, store_exp)     \
case PARSE_STATE::st:                  \
if (ch == break_char)              \
{                           \
    store_exp;      \
    tok = "";               \
    state = PARSE_STATE::next_st ;       \
} else                      \
{                           \
    tok += ch;              \
} break        
               
#define PARSE_BEFORE(st, next_st, break_char) \
case PARSE_STATE::st:               \
if (ch == break_char)                 \
{                                   \
    state = PARSE_STATE::next_st;   \
} break

        for (char ch : line) 
        {
            switch (state) 
            {
            PARSE_NUM(NUM, BEFORE_LAT1, ':', m_num=stoi(tok));

            PARSE_BEFORE(BEFORE_LAT1, LAT1, '[');
            PARSE_NUM(LAT1, LONG1, ',', pt1.x=stof(tok));

            PARSE_NUM(LONG1, BEFORE_LAT2, ']', pt1.y=stof(tok));

            PARSE_BEFORE(BEFORE_LAT2, LAT2, '[');
            PARSE_NUM(LAT2, LONG2, ',', pt2.x=stof(tok));

            PARSE_NUM(LONG2, DONE, ']', pt2.y=stof(tok));

            case PARSE_STATE::DONE:
                break;
            default:
                throw runtime_error("shouldn't be here!!");
            }
        }

#undef PARSE_BEFORE
#undef PARSE_NUM

        m_start = pt1;
        m_offset = pt2 - pt1;

        return true;
    }
    bool intersect(const Bridge& that) const
    {
        const Vec& r = m_offset;
        const Vec& s = that.m_offset;

        const Vec& diff = that.m_start - m_start;
        float num_cross = diff.cross(s);
        float den_cross = r.cross(s);

        if (abs(den_cross) < 0.000001) 
        {
            return false;
        }

        float t = num_cross / den_cross;

        return (0.f <= t && t <= 1.f);
    }
};

ostream& operator<<(ostream& os, const Bridge& b)
{
    os << b.m_num;
    return os;
}


using BridgeSet = set<Bridge>;
using BridgeSetVec = vector<BridgeSet>;

void check_intersection(const BridgeSet& cset, const Bridge& br, BridgeSetVec& new_sets)
{
    BridgeSet crossing_bridges;
    for (const Bridge& elem : cset) 
    {
        if (elem.intersect(br)) 
        {
            crossing_bridges.insert(elem);
        }
    }

    BridgeSet new_set;
    set_difference(cset.cbegin(), cset.cend(),
                   crossing_bridges.cbegin(), crossing_bridges.cend(),
                   std::inserter(new_set, new_set.begin()));
    new_set.insert(br);

    new_sets.push_back(new_set);
    if (crossing_bridges.size() > 0) 
    {
        new_sets.push_back(cset);
    }
}

int main(int argc, char *argv[]) {

    ifstream stream(argv[1]);
    string line;

    vector<Bridge> bridges;

    while (getline(stream, line)) {
        Bridge b;
        if(b.parse(line))
        {
            bridges.push_back(b);
        }
    }

//  std::copy(begin(bridges), end(bridges), std::ostream_iterator<const Bridge&>(std::cout, " "));

    BridgeSetVec current_sets = { BridgeSet() };

    for (const Bridge& br : bridges) 
    {
        BridgeSetVec new_sets;

        for (const BridgeSet& cset : current_sets) 
        {
            check_intersection(cset, br, new_sets);
        }

        current_sets = new_sets;
    }

    size_t max_size = 0;
    const BridgeSet* largest_set = nullptr;
    for (const BridgeSet& cset : current_sets) 
    {
        if (cset.size() > max_size) 
        {
            largest_set = &cset;
            max_size = cset.size();
        }
    }

    std::copy(begin(*largest_set), end(*largest_set), std::ostream_iterator<const Bridge&>(cout, "\n"));

    return 0;
} 
