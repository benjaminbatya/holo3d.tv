#include <iostream>
#include <chrono>
#include <thread>

namespace myc = std::chrono;

int main(int, char**)
{
    const size_t MAX_COUNT = 1000;
    const size_t DELAY = 10;

    myc::time_point<myc::system_clock> start, end;
    start = myc::system_clock::now();

    for (size_t i=0; i<MAX_COUNT; i++) 
    {
        std::cout << i << std::endl;
        std::this_thread::sleep_for(myc::milliseconds(DELAY));
    }

    end = myc::system_clock::now();
    myc::duration<double> elapsed_secs = end - start;
    std::cout << "Average duration between ticks = " << 
        (elapsed_secs.count() / MAX_COUNT) << " secs" << std::endl;

}
