#include "util_funcs.hpp"

class Date 
{
    int year_, month_, day_;
public:
    Date(int year, int month, int day) : year_(year), month_(month), day_(day) {}

    friend std::ostream &operator<<(std::ostream &os, const Date &d) {
        return os << d.year_ << '-' << d.month_ << '-' << d.day_;
    }
};

int main()
{
    INFO("Hello, {0} {1} {0}!\n", "world", "Dear");

    INFO("Hello everyone! date={}\n", Date(2012, 12, 28));

    INFO("this is informative! {}!={}", 1, "false");

    ERROR("This should have worked! {}=={}", "false", Date(2015,1,8));

}
