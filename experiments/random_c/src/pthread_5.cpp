#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define NUM_THREADS     3
#define T_COUNT         10
#define COUNT_LIMIT     12

int g_count               = 0;
int thread_ids[3]       = {1, 2, 3};

pthread_mutex_t count_mutex;
pthread_cond_t  count_threshold_cv;

void* inc_count(void* t)
{
    size_t my_id = size_t(t);

    for (size_t i=0; i<T_COUNT; i++) 
    {
        pthread_mutex_lock(&count_mutex);
        g_count++;

        if (g_count == COUNT_LIMIT) 
        {
            pthread_cond_signal(&count_threshold_cv);
            printf("inc_count(): thread %ld, g_count=%d threshold reached.\n", my_id, g_count);
        }

        printf("inc_count(): thread %ld, g_count=%d, unlocking mutex\n", my_id, g_count);

        pthread_mutex_unlock(&count_mutex);

        // work
        sleep(1);
    }

    pthread_exit(NULL);
}

void* watch_count(void* t)
{
    size_t my_id = size_t(t);

    printf("watch_count: thread %ld\n", my_id);

    pthread_mutex_lock(&count_mutex);

    printf("watch_count: thread %ld, got mutex\n", my_id);

    while (g_count < COUNT_LIMIT) 
    {
        pthread_cond_wait(&count_threshold_cv, &count_mutex);
        printf("watch_count: thread %ld, condition signal received\n", my_id);
        g_count += 125;
        printf("watch_count: thread %ld, g_count = %d\n", my_id, g_count);
    }

    pthread_mutex_unlock(&count_mutex);
    pthread_exit(NULL);
}

int main(int argc, char** argv)
{
    size_t t1=1, t2=2, t3=3;

    pthread_mutex_init(&count_mutex, NULL);
    pthread_cond_init(&count_threshold_cv, NULL);

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    pthread_t threads[NUM_THREADS];
    pthread_create(&threads[0], &attr, watch_count, (void*)t1);
    pthread_create(&threads[1], &attr, inc_count, (void*)t2);
    pthread_create(&threads[2], &attr, inc_count, (void*)t3);

    for (size_t i=0; i<NUM_THREADS; i++) 
    {
        pthread_join(threads[i], NULL);
    }

    printf("main: waited on %d threads. done\n", NUM_THREADS);

    pthread_attr_destroy(&attr);
    pthread_mutex_destroy(&count_mutex);
    pthread_cond_destroy(&count_threshold_cv);
    pthread_exit(NULL);

    return 0;

}
