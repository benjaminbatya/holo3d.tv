#include <pthread.h>
#include <stdio.h>

#define NUM_THREADS 4

#define N 1000
#define MEG_EXTRA 1000000

pthread_attr_t attr;

void* busy_work(void* t)
{
    size_t tid = long(t);

    double A[N][N];

    size_t stack_size;
    pthread_attr_getstacksize(&attr, &stack_size);
    printf("Thread %ld starting, stack size = %ld\n", tid, stack_size);

    for (size_t j=0; j<N; j++) 
    {
        for (size_t i=0; i<N; i++) 
        {
            A[i][j] = (i*j)/3.452 + N-i;
        }
    }
    printf("Thread %ld done. A[145][145] = %e\n", tid, A[145][145]); 
    pthread_exit(NULL);
}

int main(int argc, char** argv)
{
    pthread_t threads[NUM_THREADS];

    pthread_attr_init(&attr);

    size_t stack_size;
    pthread_attr_getstacksize(&attr, &stack_size);

    printf("default stack size = %ld\n", stack_size);
    stack_size = sizeof(double)*N*N + MEG_EXTRA;
    printf("size needed = %ld\n", stack_size);

    pthread_attr_setstacksize(&attr, stack_size);

    for (size_t t=0; t<NUM_THREADS; t++) 
    {
        printf("Creating thread #%ld\n", t);
        int rc = pthread_create(&threads[t], &attr, busy_work, (void*)t);
        if (rc) 
        {
            printf("Error: return code from pthread_create = %d\n", rc);
            return -1;
        }
    }

    printf("main complete\n");

    pthread_exit(NULL);
}
