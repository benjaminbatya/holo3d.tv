#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NUM_THREADS 4

void* busy_work(void* t)
{
    size_t tid = long(t);
    double result = 0.0;

    printf("Thread %ld starting...\n", tid);

    for (size_t i=0; i<1000000; i++) 
    {
        result = result + sin(i) + tan(i);
    }
    printf("Thread %ld done. result = %e\n", tid, result);
    pthread_exit(t);
}

int main(int argc, char** argv)
{
    pthread_t threads[NUM_THREADS];
    pthread_attr_t attr;

    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    for (size_t t=0; t<NUM_THREADS; t++) 
    {
        printf("Creating thread #%ld\n", t);
        int rc = pthread_create(&threads[t], &attr, busy_work, (void*)t);
        if (rc) 
        {
            printf("Error: return code from pthread_create = %d\n", rc);
            return -1;
        }
    }

    pthread_attr_destroy(&attr);
    for (size_t t=0; t<NUM_THREADS; t++) 
    {
        void* status;
        int rc = pthread_join(threads[t], &status);
        if (rc) 
        {
            printf("Error: return code from pthread_join() = %d\n", rc);
            return -1;
        }
        printf("Completed join with thread %ld having a status = %ld\n", t, long(status));
    }


   printf("main complete\n");

    pthread_exit(NULL);
}
