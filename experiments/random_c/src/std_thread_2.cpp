#include <iostream>
#include <thread>
#include <vector>
#include <mutex>

using namespace std;

vector<size_t> bounds(size_t parts, size_t mem)
{
    vector<size_t> bnd;
    size_t delta = mem / parts;
    size_t reminder = mem % parts;

    size_t N1 = 0, N2 = 0;

    bnd.push_back(N1);
    for (size_t i=0; i<parts; i++) 
    {
        N2 = N1 + delta;
        if (i == parts - 1) 
        {
            N2 += reminder;
        }
        bnd.push_back(N2);
        N1 = N2; 
    }

    return bnd;
}

static mutex barrier;

void dot_product(const vector<int>& v1, const vector<int>& v2, int& result, size_t L, size_t R)
{
    int partial_sum = 0;

    for (size_t i=L; i<R; ++i) 
    {
        partial_sum += v1[i] * v2[i];
    }

    std::lock_guard<std::mutex> block_threads_until_done(barrier);
    result += partial_sum;
}

int main()
{
    size_t num_elems = 100000;
    size_t num_threads = 2;
    int result = 0;
    vector<thread> threads;

    vector<int> v1(num_elems, 1), v2(num_elems, 2);

    vector<size_t> limits = bounds(num_threads, num_elems);

    for (size_t i=0; i<num_threads; i++) 
    {
        threads.push_back(thread(dot_product, ref(v1), ref(v2), ref(result), limits[i], limits[i+1]));
    }

    for (auto& t: threads) 
    {
        t.join();
    }

    cout << result << endl;

    return 0;

}

