void _workspace_opened_set_special_path()
{
   switch (_workspace_filename) {
      case 'intro' :
         set_env('GOPATH', get_env("PWD") + "/../..");
         return;
   }
}
