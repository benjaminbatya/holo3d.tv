__author__ = 'benjamin'

import time

class FpsCounter:
    def __init__(self, name = "fps_counter"):
        self.set_name(name)

        self.reset()

    def set_name(self, name):
        self._name = name

    def reset(self):
        self._last_time = time.time()
        self._num_frames = 0

    def update(self):
        self._num_frames += 1
        curr_time = time.time()
        delta = curr_time - self._last_time
        if delta >= 1:
            fps = float(self._num_frames) / delta
            print "%r: fps=%r" % (self._name, fps)
            self._last_time = curr_time
            self._num_frames = 0

