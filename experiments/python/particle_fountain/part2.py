#from OpenGL.GL import GL_ARRAY_BUFFER, GL_DYNAMIC_DRAW, glFlush
from OpenGL.GL import *
import OpenGL.arrays as GLArrays
import pyopencl as cl
import numpy

from fps_counter import FpsCounter
import timing

timings = timing.Timing()
class Part2(object):
    def __init__(self, num, dt, *args, **kwargs):
        self.clinit()
        self.loadProgram("part2.cl");

        self.num = num
        self.dt = numpy.float32(dt)

        self.timings = timings

        self._fps = FpsCounter("rendering")

    def loadData(self, pos, col, vel):
        import pyopencl as cl
        mf = cl.mem_flags

        pos_vbo = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, pos_vbo)
        pos_size = GLArrays.ArrayDatatype.arrayByteCount(pos)
        glBufferData(GL_ARRAY_BUFFER, pos_size, pos.tostring(), GL_DYNAMIC_DRAW)

        col_vbo = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, col_vbo)
        color_size = GLArrays.ArrayDatatype.arrayByteCount(col)
        glBufferData(GL_ARRAY_BUFFER, color_size, col.tostring(), GL_DYNAMIC_DRAW)

        self.pos_vbo = pos_vbo
        self.col_vbo = col_vbo

        self.pos = pos
        self.col = col
        self.vel = vel

        #Setup vertex buffer objects and share them with OpenCL as GLBuffers
        # self.pos_vbo.bind()
        #For some there is no single buffer but an array of buffers
        #https://github.com/enjalot/adventures_in_opencl/commit/61bfd373478767249fe8a3aa77e7e36b22d453c4
        # try:
        self.pos_cl = cl.GLBuffer(self.ctx, mf.READ_WRITE, int(self.pos_vbo))
        self.col_cl = cl.GLBuffer(self.ctx, mf.READ_WRITE, int(self.col_vbo))
        # except AttributeError:
        #     self.pos_cl = cl.GLBuffer(self.ctx, mf.READ_WRITE, int(self.pos_vbo.buffers[0]))
        #     self.col_cl = cl.GLBuffer(self.ctx, mf.READ_WRITE, int(self.col_vbo.buffers[0]))
        # self.col_vbo.bind()

        #pure OpenCL arrays
        # self.vel_cl = cl.Buffer(self.ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=vel)
        self.vel_cl = cl.Buffer(self.ctx, mf.READ_WRITE | mf.COPY_HOST_PTR, hostbuf=vel)
        self.pos_gen_cl = cl.Buffer(self.ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=self.pos)
        self.vel_gen_cl = cl.Buffer(self.ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=self.vel)
        # self.queue.finish()

        # set up the list of GL objects to share with opencl
        self.gl_objects = [self.pos_cl, self.col_cl]

        self.host_vel = numpy.empty((self.num,4), dtype=numpy.float32)
        self.host_pos = numpy.empty((self.num,4), dtype=numpy.float32)

    @timings
    def execute(self, sub_intervals):
        cl.enqueue_acquire_gl_objects(self.queue, self.gl_objects)

        global_size = (self.num,)
        local_size = None

        kernelargs = (self.pos_cl, 
                      self.col_cl, 
                      self.vel_cl, 
                      self.pos_gen_cl, 
                      self.vel_gen_cl, 
                      self.dt)

        # cl.enqueue_copy(self.queue, self.host_pos, self.pos_cl)
        # cl.enqueue_copy(self.queue, self.host_vel, self.vel_cl)
        # self.queue.finish()
        # print "Before: pos[0] = %r, vel[0] = %r, dt=%r" % (self.host_pos[0], self.host_vel[0], self.dt)

        for i in xrange(0, sub_intervals):
            self.program.part2(self.queue, global_size, local_size, *(kernelargs))

        cl.enqueue_release_gl_objects(self.queue, self.gl_objects)
        self.queue.finish()


    def clinit(self):
        plats = cl.get_platforms()
        from pyopencl.tools import get_gl_sharing_context_properties
        import sys 
        if sys.platform == "darwin":
            self.ctx = cl.Context(properties=get_gl_sharing_context_properties(),
                             devices=[])
        else:
            self.ctx = cl.Context(properties=[
                (cl.context_properties.PLATFORM, plats[0])]
                + get_gl_sharing_context_properties(), devices=None)
                
        self.queue = cl.CommandQueue(self.ctx)

    def loadProgram(self, filename):
        #read in the OpenCL source file as a string
        f = open(filename, 'r')
        fstr = "".join(f.readlines())
        #print fstr
        #create the program
        self.program = cl.Program(self.ctx, fstr).build()


    def render(self):

        glEnable(GL_POINT_SMOOTH)
        glPointSize(2)
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        #setup the VBOs
        glBindBuffer(GL_ARRAY_BUFFER, self.col_vbo)
        glColorPointer(4, GL_FLOAT, 0, None)

        glBindBuffer(GL_ARRAY_BUFFER, self.pos_vbo)
        glVertexPointer(4, GL_FLOAT, 0, None)

        glEnableClientState(GL_VERTEX_ARRAY)
        glEnableClientState(GL_COLOR_ARRAY)
        #draw the VBOs
        glDrawArrays(GL_POINTS, 0, self.num)

        glDisableClientState(GL_COLOR_ARRAY)
        glDisableClientState(GL_VERTEX_ARRAY)

        glDisable(GL_BLEND)
        glFlush()

        self._fps.update()

