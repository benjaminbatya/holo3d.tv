
import numpy as np
from numpy import ma
import math

angle = 90

angle = math.radians(angle)

# Always rotate around the y axis
rot_matrix = np.array([[math.cos(angle), 0., -math.sin(angle)],
                      [0.,         1.,          0.],
                      [math.sin(angle), 0.,  math.cos(angle)] ])

vectors = np.array([[-3.0, 2.5, 6.0],
           [-3.0, 2.0, 6.0],
           [+0.0, -2.0, 6.0]])

# vectors = vectors.tolist()

new_vectors = []
for vec in vectors:
    print vec
    new_vectors.append(rot_matrix.dot(vec))
    
print "original vectors="
print np.array(vectors)

new_vectors = np.array(new_vectors)
low_values = np.abs(new_vectors) < 0.001
new_vectors[low_values] = 0

print "new_vectors="
print new_vectors
