__author__ = 'benjamin'

import pyopencl as cl
import pyopencl.array as cl_array
# import pyopencl.algorithm as cl_algo

import numpy as np

import os
# HACK: Disable program caching to allow opencl's printf to work
# See http://documen.tician.de/pyopencl/runtime.html#programs-and-kernels
# and http://www.thebigblob.com/opencl-bugs/
os.environ["PYOPENCL_NO_CACHE"] = "1"

platform = cl.get_platforms()[0]
device = platform.get_devices()[0]

context = cl.Context([device])

queue = cl.CommandQueue(context)

## Test ElementwiseKernel
n = 40

x_origin_cl = cl_array.to_device(queue, np.arange(0, n, 1, dtype=np.float32))

from pyopencl.elementwise import ElementwiseKernel
calc_polygon_verts = ElementwiseKernel(context,
                                       "float r, float* x_origin, float* x ",
                                       operation="x[i] = x_origin[i] + convert_float(i)/n",
                                       name="calc_polygon_verts",
                                       preamble="#define M_PI 3.14159265358")

x_cl = cl_array.empty_like(x_origin_cl)
event = calc_polygon_verts(50.0, x_origin_cl, x_cl)

x_host = x_cl.get()
print x_host

# knl = calc_polygon_verts.get_kernel(False)
# source = knl[0]._source
# print source

## Test ReductionKernel
n = 50
x_np = np.random.randn(n).astype(np.float32)
x_cl = cl_array.to_device(queue, x_np)
y_cl = cl_array.to_device(queue, np.random.randn(n).astype(np.float32))

from pyopencl.reduction import ReductionKernel
reduction_kernel = ReductionKernel(context,
                                   dtype_out=np.float32,
                                   neutral="0",
                                   # map_expr="min(x[i], y[i])",
                                   map_expr="x[i]",
                                   reduce_expr="a+b",
                                   arguments="__global float* x, __global float* y",
                                   name="reduction_kernel")

result = reduction_kernel(x_cl, y_cl).get()

# print result
# print x_np.sum()
TOL = 0.00001
print "The opencl reduction kernel is correct: %r" % ((result-x_np.sum()) < TOL)

# source = reduction_kernel.stage_1_inf.source
# print "Stage 1 source = ", source
# source = reduction_kernel.stage_2_inf.source
# print "Stage 2 source = ", source

from pyopencl.scan import GenericScanKernel
scan_kernel = GenericScanKernel(context, np.int32,
                                arguments="__global int *ary",
                                input_expr="ary[i]",
                                scan_expr="a+b", neutral="0",
                                output_statement="ary[i]= item;")

a = cl.array.arange(queue, 10, dtype=np.int32)
scan_kernel(a, queue=queue)

print "results of scan kernel = %r" % a.get()

print scan_kernel.first_level_scan_info.scan_src

# from pyopencl.algorithm import copy_if


