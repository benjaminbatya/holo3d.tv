# -*- coding: utf-8 -*-

import pyopencl as cl
import numpy as np
from time import time

if __name__ == "__main__":
    platform = cl.get_platforms()[0]
    
    device = platform.get_devices()[0]
    
    context = cl.Context([device])
    
    program = cl.Program(context, """
        __kernel void vadd(
            __global float* a,
            __global float* b,
            __global float* c,
            __global float* d,
            const unsigned int count)
        {
            int gid = get_global_id(0);
            if(gid < count)
            {
                d[gid] = a[gid] + b[gid] + c[gid];
            }
        }
    """).build()
    
    queue = cl.CommandQueue(context)
    
    TOL = 0.001
    LENGTH = 1024
    
    a = np.random.rand(LENGTH).astype(np.float32)
    b = np.random.rand(LENGTH).astype(np.float32)
    c = np.random.rand(LENGTH).astype(np.float32)
    d = np.zeros(LENGTH, dtype=np.float32)
    
    #print a.shape[0]
    #print a.dtype
    
    mem_flags = cl.mem_flags
    a_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=a)
    b_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=b)
    c_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=c)        
    d_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, d.nbytes)

    rtime = time()    
    
    program.vadd(queue, d.shape, None, a_buf, b_buf, c_buf, d_buf, np.uint32(LENGTH))
    
    delta = time()-rtime   
    
    print "Kernel ran in", delta , "secs"  
    
    cl.enqueue_copy(queue, d, d_buf)
    
    queue.finish()    
    
    correct = 0
    # Check correctness
    for ax,bx,cx,dx in zip(a,b,c,d):
        tmp = ax + bx + cx
        tmp -= dx
        
        if(tmp*tmp < TOL*TOL):
            correct += 1
        else:
            print "Failed at idx=%d" % i
    
    print "num correct=%d" % correct
        
    

