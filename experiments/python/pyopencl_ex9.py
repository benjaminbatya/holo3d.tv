#
#Simple python integration

from helper import *
from definitions import *

import pyopencl as cl
import numpy as np
from time import time

def main():
    # First do simple python

    stime = time()

    NUM_STEPS = 2**24
    #
    # step = 1.0 / NUM_STEPS
    # sum = 0.0
    # for i in range(NUM_STEPS):
    #     x = (i+0.5) * step
    #     sum += 4.0 / (1.0 + x*x)
    #
    # py_value = step * sum
    #
    # delta_time = time() - stime
    # # delta_time *= 1000000.0
    # print 'pure python, final value =', py_value, "time to run=", delta_time

    # Set up OpenCL
    platform = cl.get_platforms()[0]
    devices = platform.get_devices()
    # print len(devices)
    device = devices[0] # Use the GPU
    context = cl.Context([device])
    queue = cl.CommandQueue(context)

    program = cl.Program(context, '''
    __kernel void pi (
        const int num_iters, const float step_size,
        __local float * local_sums, __global float * partial_sums)
    {
        int num_work_items = get_local_size(0);
        int local_id = get_local_id(0);
        int group_id = get_group_id(0);

        float x, sum = 0.0f;
        int start = (group_id * num_work_items + local_id) * num_iters;
        int end = start + num_iters;

        for(int i = start; i<end; i++)
        {
            x = (i+0.5) * step_size;
            sum += 4.0 / (1.0 + x*x);
        }

        local_sums[local_id] = sum;

        barrier(CLK_LOCAL_MEM_FENCE);

        // Sum up the local_results if local_id == 0
        if(local_id == 0)
        {
            sum = 0.0f;
            for(int i=0; i<num_work_items; i++)
            {
                sum += local_sums[i];
            }

           partial_sums[group_id] = sum;
        }
    }
    ''').build()

    LOCAL_ITERS = 2**8

    work_group_size = program.pi.get_work_group_info(cl.kernel_work_group_info.WORK_GROUP_SIZE, device)

    num_work_groups = NUM_STEPS / (work_group_size * LOCAL_ITERS)

    if num_work_groups < 1:
        num_work_groups = device.max_compute_units
        work_group_size = NUM_STEPS / (num_work_groups * LOCAL_ITERS)

    print 'work_group_size=', work_group_size, "num_work_groups=", num_work_groups, "num_iters=", LOCAL_ITERS

    # Recalculate the number of steps
    num_steps = work_group_size * LOCAL_ITERS * num_work_groups
    step_size = 1.0 / float(num_steps)

    results = numpy.empty(num_work_groups).astype(numpy.float32)

    results_buf = cl.Buffer(context, cl.mem_flags.WRITE_ONLY, results.nbytes)

    # Do the reduction
    start_time = time()

    globalrange = (num_work_groups*work_group_size, )
    localrange = (work_group_size, )
    local_memory = cl.LocalMemory(np.dtype(np.float32).itemsize * work_group_size)

    program.pi(queue, globalrange, localrange,
               np.int32(LOCAL_ITERS), np.float32(step_size),
               local_memory, results_buf)

    cl.enqueue_copy(queue, results, results_buf)

    ocl_value1 = results.sum() * step_size

    queue.finish()

    run_time = time() - start_time

    print   'simple opencl result: pi =', ocl_value1, \
            "time to run=", run_time, \
            "num steps=", num_steps

    flops = (NUM_STEPS * 6) / (run_time)
    print "flops = ", float(flops) / (10**9), "GFLOPs"

    # print 'error =', abs(ocl_value1 - py_value)


if __name__ == "__main__":
    main()