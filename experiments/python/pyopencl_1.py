# -*- coding: utf-8 -*-
"""
Created on Tue Jan 27 21:41:39 2015

@author: benjamin
"""

import numpy as np
import pyopencl as cl
import pyopencl.array as cl_array

ctx = cl.create_some_context()
queue = cl.CommandQueue(ctx)

a_gpu = cl_array.to_device(queue, np.random.randn(4,4).astype(np.float32))
a_doubled = (2*a_gpu).get()

print "a_doubled=", a_doubled
print "a_gpu=", a_gpu
