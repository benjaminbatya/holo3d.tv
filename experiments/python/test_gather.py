#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pyopencl as cl
import pyopencl.array
from pyopencl.elementwise import ElementwiseKernel

from pyopencl.reduction import ReductionKernel
n = 10
points_np = np.array([1, 2, 3, 4]).astype(np.float32)
color_np = np.array([1, 1, 0, 1]).astype(np.float32)

print "Using Data Elements:"
print "points=%r" % points_np
print "colors=%r" % color_np

ctx = cl.create_some_context()
queue = cl.CommandQueue(ctx)

points_cl = cl.array.to_device(queue, points_np)
colors_cl = cl.array.to_device(queue, color_np)



out_cl = cl.array.zeros()
