__author__ = 'benjamin'

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse, Patch
from matplotlib.colors import LinearSegmentedColormap
import math

import pyopencl as cl
import pyopencl.algorithm as cl_algo
import pyopencl.scan as cl_scan

# cl_algo.copy_if()
# cl_scan.GenericScanKernel()


RADIUS = 30.0
MAX_ITERATIONS = 20
MIN_POINTS_IN_ESTIMATE = 10

RADIUS_SQ = RADIUS ** 2


def maximize(points, pos, width_sq, height_sq):
    '''
    :param points:
    :param pos:
    :param within_delta:
    :return: (list of points within delta of pos)
    '''
    ret = []

    num_pts = len(points)
    for i in xrange(num_pts):
        vec = pos - points[i]
        check = vec.dot(vec)

        if check < (width_sq + height_sq):
            ret.append(i)

    return ret

def expectation(points, indices):
    '''
    :param points:
    :return: (mean of points, radius of points from mean)
    '''
    mean = np.sum(points[indices], axis=0)
    mean /= len(indices)

    max_width_sq = 0.0
    max_height_sq = 0.0
    for i in indices:
        vec = mean - points[i]
        width_sq = vec[0]*vec[0]
        height_sq = vec[1]*vec[1]

        if max_width_sq < width_sq:
            max_width_sq = width_sq

        if max_height_sq < height_sq:
            max_height_sq = height_sq

    return(mean, max_width_sq, max_height_sq)

def rand(low, high):
    return int(np.random.random() * (high - low) + low)

def main():
    fig = plt.figure(1)
    ax = fig.add_subplot(1,1,1)

    # Always use the same seed for testing
    # np.random.seed(100)

    blobs = []

    for i in xrange(rand(1, 10)):
        blobs.append(
            {'count':rand(20,200), 'x':rand(-200, 200), 'y':rand(-200, 200), 'w':rand(5,25), 'h':rand(5,25)}
        )

    print "Num blobs = %r" % len(blobs)
    for i in xrange(len(blobs)):
        blob = blobs[i]
        print "Blob %r = %r" % (i, blob)
        c = Ellipse(xy=(blob['x'], blob['y']), width=4*blob['w'], height=4*blob['h'], color='g', fill=False)
        if i == 0:
            c.set_label('Data Distribution')
        ax.add_patch(c)

    x = []
    y = []

    for blob in blobs:
        # Generate a random blob
        new_x = np.random.standard_normal(blob['count']) * blob['w'] + blob['x']
        new_y = np.random.standard_normal(blob['count']) * blob['h'] + blob['y']

        x = np.hstack((x, new_x))
        y = np.hstack((y, new_y))

    points = np.vstack((x,y)).T
    num_points = points.shape[0]

    num_samples = int(num_points * 0.1)

    indices = np.random.choice(num_points, num_samples)

    plt.plot(x, y, 'wo', label='Original data')

    plt.plot(x[indices], y[indices], 'bx', label='Sampled Data')

    estimates = []
    for i in indices:
        # first index is the mean of the cluster
        # Second index is the width squared
        # Third index is the height squared
        # Fourth index indicates if the cluster is settled or not
        estimates.append((points[i], RADIUS_SQ, RADIUS_SQ, False))

    estimates = cluster_points(estimates, points)

    idx = 0
    for (mean, width_sq, height_sq, settled) in estimates:
        width = math.sqrt(width_sq)
        height = math.sqrt(height_sq)
        print "Estimate %r: mean=%r, width=%r, height=%r" % ( idx, mean, width, height )
        c = Ellipse(xy=mean, width=2*width, height=2*height, color='r', fill=False)
        if idx == 0:
            c.set_label("Estimate")
        ax.add_patch(c)
        idx += 1

    ax.legend(loc='best')

    plt.show()

def cluster_points(estimates, points):

    for i in xrange(MAX_ITERATIONS):
        print "iteration %r, num estimates = %r" % (i, len(estimates))

        num_unsettled = 0

        # Run EM and check for settled estimates
        new_estimates = []
        for estimate in estimates:
            # If the estimate is settled already, don't run EM on it again
            if estimate[3] is True:
                new_estimates.append(estimate)
                continue

            # Try different sizes for maximization
            indices = maximize(points, estimate[0], estimate[1], estimate[2]) # RADIUS_SQ, RADIUS_SQ) # estimate[1]*1.1, estimate[2]*1.1) #

            # Only save the estimate if it contains more then the required minimum number of points
            if len(indices) > MIN_POINTS_IN_ESTIMATE:
                (mean, width_sq, height_sq) = expectation(points, indices)

                if np.allclose(mean, estimate[0]): # and np.allclose(width_sq, estimate[1]) and np.allclose(height_sq, estimate[2]):
                    # Mark the estimate as settled
                    new_estimates.append((mean, width_sq, height_sq, True))
                    # print "Settled: mean=%r, radius=%r" % ( mean, math.sqrt(radius_sq) )

                else:
                    # The estimate is still unsettled
                    new_estimates.append((mean, width_sq, height_sq, False))
                    num_unsettled += 1
                    # print "New estimate: mean=%r, radius=%r" % ( mean, math.sqrt(radius_sq) )

        estimates = new_estimates

        # print "After EM: number of unsettled estimates = %r" %  num_unsettled

        # ax = fig.add_subplot(1,1,1)
        #
        # for (mean, radius_sq) in estimates:
        #     c = plt.Circle(mean, radius=math.sqrt(radius_sq), color=colors[i], fill=False)
        #     ax.add_patch(c)

        # merge together estimates if their means are closer then the greater radius
        num_unsettled = 0
        new_estimates = []
        for est_i in xrange(len(estimates)):

            (mean_i, width_sq_i, height_sq_i, settled_i) = estimates[est_i]
            removed_i = False

            est_j = est_i+1
            while est_j < len(estimates):
                (mean_j, width_sq_j, height_sq_j, settled_j) = estimates[est_j]

                diff_vec = mean_i - mean_j
                delta_sq = diff_vec.dot(diff_vec)

                # if(i >= 4) and (est_i==0):
                #     pass

                if delta_sq < (width_sq_j + height_sq_j):
                    # print "Removing duplicate estimate %i (mean=%r, radius=%r) because of estimate %r" % \
                    #       (est_i, mean_i, math.sqrt(radius_sq_i), est_j)
                    removed_i = True
                    break

                est_j += 1

            if not removed_i:
                # print "Saving estimate %r (mean=%r, radius=%r)" %(est_i, mean_i, math.sqrt(radius_sq_i))
                new_estimates.append((mean_i, width_sq_i, height_sq_i, settled_i))
                if not settled_i:
                    num_unsettled += 1

        estimates = new_estimates

        # print "After pruning, num unsettled estimates = %r" % num_unsettled

        # merge in reverse
        num_unsettled = 0
        new_estimates = []
        for est_i in xrange(len(estimates)-1, -1, -1):

            (mean_i, width_sq_i, height_sq_i, settled_i) = estimates[est_i]
            removed_i = False

            est_j = est_i - 1
            while est_j > -1:
                (mean_j, width_sq_j, height_sq_j, settled_j) = estimates[est_j]

                diff_vec = mean_i - mean_j
                delta_sq = diff_vec.dot(diff_vec)

                # if(i >= 4) and (est_i==0):
                #     pass

                if delta_sq < (width_sq_j + height_sq_j):
                    # print "Removing duplicate estimate %i (mean=%r, radius=%r) because of estimate %r" % \
                    #       (est_i, mean_i, math.sqrt(radius_sq_i), est_j)
                    removed_i = True
                    break

                est_j -= 1

            if not removed_i:
                # print "Saving estimate %r (mean=%r, radius=%r)" %(est_i, mean_i, math.sqrt(radius_sq_i))
                new_estimates.append((mean_i, width_sq_i, height_sq_i, settled_i))
                if not settled_i:
                    num_unsettled += 1

        estimates = new_estimates

        # print "After pruning, num unsettled estimates = %r" % num_unsettled

        # Break if there are no more unsettled groups
        if num_unsettled == 0:
            break

    print "Finished iterating, num estimates = %r" % len(estimates)

    return estimates

if __name__ == "__main__":
    main()
