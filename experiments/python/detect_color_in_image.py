__author__ = 'benjamin'


import numpy as np
import cv
import cv2
from matplotlib import pyplot as plt

def graph_image(file_path):
    img = cv2.imread(file_path, cv2.IMREAD_UNCHANGED)

    # img_Lab = cv2.cvtColor(img, cv.CV_BGR2Lab)

    weights = np.linspace(0, 255, 256)

    plt.figure()
    plt.title(file_path)
    color = ('b', 'g', 'r')
    channels = cv2.split(img)
    histos = [0, 1, 2]
    for i,col in enumerate(color):
        histo = cv2.calcHist([channels[i]], [0], None, [256], [0, 256])
        # histo = cv2.calcHist([img], [i], None, [256], [0.0, 256])

        for j in xrange(len(histo)):
            if histo[j] <= 30:
                histo[j] = 0

        #
        # equalized_image = cv2.equalizeHist(channels[i])
        # imgplot = plt.imshow(equalized_image)
        # imgplot.set_cmap('gray')

        # histo = cv2.calcHist([equalized_image], [0], None, [256], [0, 256])

        plt.plot(histo, color = col)
        plt.xlim([0, 256])

        # values = histo.reshape((histo.shape[0],))
        # sum = np.dot(values, weights)
        # print("file=%r, col=%r, sum=%r" % (file_path, col, sum))

        histos[i] = histo

    # Find the minimum and maximum value for all of the histograms
    min_domain = 256
    max_domain = 0
    min_range = 10E9
    max_range = 0
    for i,col in enumerate(color):
        histo = histos[i]
        prev = 0
        for j in xrange(len(histo)):
            val = histo[j]
            # Look for a zero -> non-zero change
            if prev == 0 and val > 0:
                if j < min_domain:
                    min_domain = j

            # Look for a non-zero -> zero change
            if prev > 0 and val == 0:
                if j > max_domain:
                    max_domain = j
            prev = val

        if histo.min() < min_range:
            min_range = histo.min()
        if histo.max() > max_range:
            max_range = histo.max()

    print("file=%r, min_range=%r, max_range=%r, min_domain=%r, max_domain=%r" %
          (file_path, min_range, max_range, min_domain, max_domain))

    # cv2.namedWindow('image', cv2.WINDOW_NORMAL)
    # cv2.imshow('image', img)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

if __name__ == "__main__":
    graph_image('images/blue_side.png')
    graph_image('images/black_side.png')
    graph_image('images/green_side.png')
    graph_image('images/red_side.png')

    plt.show()