#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pyopencl as cl
import pyopencl.array as cl_array
from pyopencl.elementwise import ElementwiseKernel

from pyopencl.reduction import ReductionKernel

from pyopencl.scan import *

n = 10
a_np = np.random.randn(n).astype(np.float32)
b_np = np.random.randn(n).astype(np.float32)

print "Using Data Elements:"
print "a=%r" % a_np
print "b=%r" % b_np

ctx = cl.create_some_context()
queue = cl.CommandQueue(ctx)

a_cl = cl.array.to_device(queue, a_np)
b_cl = cl.array.to_device(queue, b_np)

# Test the ElementwiseKernel
print "\nTesting the elementwise kernel"
lin_comb = ElementwiseKernel(ctx,
    "float k1, float *a_cl, float k2, float *b_cl, float *res_g",
    "res_g[i] = k1 * a_cl[i] + k2 * b_cl[i]",
    "lin_comb"
)

res_g = cl.array.empty_like(a_cl)
lin_comb(2, a_cl, 3, b_cl, res_g)

# print 2 * a_np + 3 * b_np
# print res_g

# Check on GPU with PyOpenCL Array:
print((res_g - (2 * a_cl + 3 * b_cl)).get())

# Check on CPU with Numpy:
res_np = res_g.get()
print(res_np - (2 * a_np + 3 * b_np))
print(np.linalg.norm(res_np - (2 * a_np + 3 * b_np)))

print "\nTesting ReductionKernel"

krnl = ReductionKernel(ctx, np.float32, neutral="0",
        reduce_expr="a+b", map_expr="x[i]*y[i]",
        arguments="__global float *x, __global float *y", options=["-w"])

cl_dot_prod = krnl(a_cl, b_cl).get()

# print cl_dot_prod

np_dot = np.dot(a_np, b_np)

print cl_dot_prod - np_dot

print '\nTesting InclusiveScanKernel'

knl = InclusiveScanKernel(ctx, np.int32, "a+b", neutral="0", options=["-w"])

n = 2**20-2**18+5
host_data = np.random.randint(0, 10, n).astype(np.int32)
dev_data = cl_array.to_device(queue, host_data)

res_g = cl.array.empty_like(dev_data)

knl(dev_data) # , res_g)
assert (dev_data.get() == np.cumsum(host_data, axis=0)).all()
print dev_data.shape


