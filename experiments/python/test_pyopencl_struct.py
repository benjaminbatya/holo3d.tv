#http://documen.tician.de/pyopencl/

import pyopencl as cl
import numpy as np
import struct

ctx = cl.create_some_context()
queue = cl.CommandQueue(ctx)

mf = cl.mem_flags

prg = cl.Program(ctx, """
typedef struct
{
    float A;
    float B;
    int C;
} Params;

__kernel void part3(__global const float* a,
                    __global const float* b,
                    __global float* c,
                    __constant Params* test)
{
    int gid = get_global_id(0);
    c[gid] = test->A * a[gid] + test->B * b[gid] + test->C;
}
""").build()

def add(a, b):
    a_cl = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a)
    b_cl = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=b)
    dest_cl = cl.Buffer(ctx, mf.WRITE_ONLY, b.nbytes)

    params = struct.pack('ffi', .5, 15., 3)
    print len(params), struct.calcsize('ffffi')

    params_cl = cl.Buffer(ctx, mf.READ_ONLY, len(params))
    cl.enqueue_copy(queue, params_cl, params)

    global_size = a.shape
    local_size = None

    kernel_args = (a_cl, b_cl, dest_cl, params_cl)
    prg.part3(queue, global_size, local_size, *(kernel_args))

    c = np.empty_like(a)
    cl.enqueue_copy(queue, c, dest_cl)
    queue.finish()

    return c

if __name__ == "__main__":
    x = np.ndarray((10,1), dtype=np.float32)
    y = np.ndarray((10,1), dtype=np.float32)
    x[:] = 1.
    y[:] = 2.

    z = add(x, y)
    print z


