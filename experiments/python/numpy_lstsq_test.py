__author__ = 'benjamin'

import numpy as np
import matplotlib.pyplot as plt

x = np.array([4.3862, 83.72626, -60.137, -2.47524])
y = np.array([79.59295, -6.5113745, 22.6253, -87.6422])

A = np.vstack([x, np.ones(len(x))]).T

m, c = np.linalg.lstsq(A, y)[0]

print m, c

plt.plot(x, y, 'o', label="Original Data", markersize=10)
plt.plot(x, m*x + c, 'r', label="Fitted line")
plt.legend()
plt.show()