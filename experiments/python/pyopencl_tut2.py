# -*- coding: utf-8 -*-

import pyopencl as cl
import numpy as np
from time import time

if __name__ == "__main__":
    platform = cl.get_platforms()[0]
    
    device = platform.get_devices()[0]
    
    context = cl.Context([device])
    
    program = cl.Program(context, """
        __kernel void vadd(
            __global float* a,
            __global float* b,
            __global float* c,
            const unsigned int count)
        {
            int gid = get_global_id(0);
            if(gid < count)
            {
                c[gid] = a[gid] + b[gid];
            }
        }
    """).build()
    
    queue = cl.CommandQueue(context)
    
    TOL = 0.001
    LENGTH = 1024
    
    a = np.random.rand(LENGTH).astype(np.float32)
    b = np.random.rand(LENGTH).astype(np.float32)
    c = np.zeros(LENGTH, dtype=np.float32)
    
    #print a.shape[0]
    #print a.dtype
    
    mem_flags = cl.mem_flags
    a_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=a)
    b_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=b)
    c_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, c.nbytes)

    rtime = time()    
    
    program.vadd(queue, c.shape, None, a_buf, b_buf, c_buf, np.uint32(LENGTH))
    
    delta = time()-rtime   
    
    print "Kernel ran in", delta , "secs"  
    
    cl.enqueue_copy(queue, c, c_buf)
    
    queue.finish()    
    
    correct = 0
    # Check correctness
    for i in range(LENGTH):
        tmp = a[i] + b[i]
        tmp -= c[i]
        
        if(tmp*tmp < TOL*TOL):
            correct += 1
        else:
            print "Failed at idx=%d" % i
    
    print "num correct=%d" % correct
        
    

