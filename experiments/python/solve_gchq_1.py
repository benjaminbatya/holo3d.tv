#!/bin/python
__author__ = 'benjamin'

def enumeration(**enums):
    return type('Enum', (), enums)

STATES = enumeration(UNKNOWN=1, FIXED=2, FILLED=3, EMPTY=4)
SIZE = 25
ARRAY = [[STATES.UNKNOWN for i in range(SIZE)] for j in range(SIZE)]

FIXED_CELLS = [
    [3,3], [3,4], [3,12], [3,13], [3,21],
    [8,6], [8,7], [8,10], [8,14], [8,15], [8,18],
    [16,6], [16,11], [16,16], [16,20],
    [21,3], [21,4], [21,9], [21,10], [21,15], [21,20], [21,21]
]

ROW_RUNS = [
    [7,3,1,1,7],
    [1,1,2,2,1,1],
    [1,3,1,3,1,1,3,1],
    [1,3,1,1,6,1,3,1],
    [1,3,1,5,2,1,3,1],
    [1,1,2,1,1],
    [7,1,1,1,1,1,7],
    [3,3],
    [1,2,3,1,1,3,1,1,2],
    [1,1,3,2,1,1],
    [4,1,4,2,1,2],
    [1,1,1,1,1,4,1,3],
    [2,1,1,1,2,5],
    [3,2,2,6,3,1],
    [1,9,1,1,2,1],
    [2,1,2,2,3,1],
    [3,1,1,1,1,5,1],
    [1,2,2,5],
    [7,1,2,1,1,1,3],
    [1,1,2,1,2,2,1],
    [1,3,1,4,5,1],
    [1,3,1,3,10,2],
    [1,3,1,1,6,6],
    [1,1,2,1,1,2],
    [7,2,1,2,5]
]

COL_RUNS = [
    [7,2,1,1,7],
    [1,1,2,2,1,1],
    [1,3,1,3,1,3,1,3,1],
    [1,3,1,1,5,1,3,1],
    [1,3,1,1,4,1,3,1],
    [1,1,1,2,1,1],
    [7,1,1,1,1,1,7],
    [1,1,3],
    [2,1,2,1,8,2,1],
    [2,2,1,2,1,1,1,2],
    [1,7,3,2,1],
    [1,2,3,1,1,1,1,1],
    [4,1,1,2,6],
    [3,3,1,1,1,3,1],
    [1,2,5,2,2],
    [2,2,1,1,1,1,1,2,1],
    [1,3,3,2,1,8,1],
    [6,2,1],
    [7,1,4,1,1,3],
    [1,1,1,1,4],
    [1,3,1,3,7,1],
    [1,3,1,1,1,2,1,1,4],
    [1,3,1,4,3,3],
    [1,1,2,2,2,6,1],
    [7,1,3,2,1,1]
]

class PuzzleSolver(object):
    def __init__(self, size):
        self.size = size
        self.arr = [[STATES.UNKNOWN for i in range(self.size)] for j in range(self.size)]
        self.row_runs = None
        self.col_runs = None
        self.fixed_cells = []

    def set_row_runs(self, runs):
        if len(runs) != self.size: raise "problem!"
        self.row_runs = runs

    def set_col_runs(self, runs):
        if len(runs) != self.size: raise "problem!"
        self.col_runs = runs

    def set_fixed_cells(self, fixed_cells):
        self.fixed_cells = fixed_cells

    def solve(self):
        self.arr = [[STATES.UNKNOWN for i in range(self.size)] for j in range(self.size)]
        for c in self.fixed_cells:
            self.arr[c[0]][c[1]] = STATES.FIXED

        return self.check_array(0, 0)

    def print_array(self):
        for row in self.arr:
            st = ""
            sep = ""
            for col in row:
                char = {
                    STATES.UNKNOWN: '?',
                    STATES.FIXED: 'F',
                    STATES.FILLED: 'X',
                    STATES.EMPTY: ' '
                }[col]
                st += sep + char
                sep = ","
            print st

    # Go to next square:
    def check_array(self, curr_row, curr_col):
        # print "check_array called: (%u, %u)" % (curr_row, curr_col)
        # We have successfully found a solution
        if curr_row >= self.size: return True

        #   If it is passed the end of the row, recurse to the next row
        if curr_col >= self.size:
            return self.check_array(curr_row+1, 0)

        #   if its fixed, recurse to the next square
        if self.arr[curr_row][curr_col] == STATES.FIXED:
            # print "(%u, %u): fixed" % (curr_row, curr_col)
            return self.check_array(curr_row, curr_col+1)

        if self.arr[curr_row][curr_col] != STATES.UNKNOWN:
            raise "Problem"

        # Set it to FILLED
        self.arr[curr_row][curr_col] = STATES.FILLED
        # if(curr_col == 1 and curr_row == 1):
        #     print "(%u, %u): Trying filled" % (curr_row, curr_col)

        # if that row and column is valid, continue to the next square
        if self.check_cell(curr_row, curr_col):
            ret = self.check_array(curr_row, curr_col+1)
            # print "(%u, %u) filled: array_check = %s" % (curr_row, curr_col, ret)
            if ret: return True

        # Set it to EMPTY,
        self.arr[curr_row][curr_col] = STATES.EMPTY
        # print "(%u, %u): Trying empty" % (curr_row, curr_col)

        # if that row and column is valid, continue to the next square
        if self.check_cell(curr_row, curr_col):
            ret = self.check_array(curr_row, curr_col+1)
            # print "(%u, %u) empty: array_check = %s" % (curr_row, curr_col, ret)
            if ret: return True

        #   If not, set it to UNKNOWN and return false
        self.arr[curr_row][curr_col] = STATES.UNKNOWN
        return False

    @staticmethod
    def check_cells_helper(cells, runs):
        curr_run = 0
        run_count = 0
        in_run = False
        for cell in cells:
            # If the cell is unknown, then we have validated up to what we know about the run
            if cell == STATES.UNKNOWN: return True

            # If the cell is filled
            elif cell == STATES.FILLED or cell == STATES.FIXED:
                if not in_run:
                    if curr_run >= len(runs):
                        return False
                    in_run = True

                run_size = runs[curr_run]
                if run_count >= run_size: return False
                run_count += 1

            # the cell is empty
            elif cell == STATES.EMPTY:
                if in_run:
                    run_size = runs[curr_run]
                    if run_count != run_size: return False

                    in_run = False
                    curr_run += 1
                    run_count = 0

        if in_run:
            run_size = runs[curr_run]
            if run_count != run_size: return False
            # in_run = False
            curr_run += 1

        if curr_run != len(runs): return False

        return True

    def check_row(self, row_idx):
        cells = self.arr[row_idx]
        runs = self.row_runs[row_idx]

        return PuzzleSolver.check_cells_helper(cells, runs)

    def check_col(self, col_idx):
        if col_idx >= self.size: raise "Problem!"
        cells = []
        for i in range(len(self.arr)):
            cells.append(self.arr[i][col_idx])

        runs = self.col_runs[col_idx]

        return PuzzleSolver.check_cells_helper(cells, runs)

    def check_cell(self, row_idx, col_idx):
        ret = self.check_row(row_idx)
        if not ret: return False

        ret = self.check_col(col_idx)
        return ret

import unittest

class Unit_Tests(unittest.TestCase):
    def setUp(self):
        self.ps = PuzzleSolver(8)

        # self.ps.arr = [[STATES.FILLED, STATES.UNKNOWN], [STATES.EMPTY, STATES.FILLED]]
        # self.ps.set_row_runs([[1], [2]])
        # self.ps.set_col_runs([[1], [1]])

    def test_check_row(self):
        self.ps.arr = [[STATES.FILLED, STATES.EMPTY, STATES.EMPTY, STATES.FILLED, STATES.UNKNOWN]]
        self.ps.row_runs = [[1,1,2]]
        self.assertTrue(self.ps.check_row(0))

        self.ps.row_runs = [[2,1,2]]
        self.assertFalse(self.ps.check_row(0))

        self.ps.arr = [[STATES.FILLED, STATES.EMPTY, STATES.FILLED, STATES.EMPTY, STATES.EMPTY, STATES.FILLED,STATES.FILLED, STATES.EMPTY]]
        self.ps.row_runs = [[1,1,2]]
        self.assertTrue(self.ps.check_row(0))

    def test_check_col(self):
        self.ps.arr = [[STATES.FILLED], [STATES.EMPTY], [STATES.EMPTY], [STATES.FILLED], [STATES.EMPTY], [STATES.UNKNOWN]]
        self.ps.col_runs = [[1,1,2]]
        self.assertTrue(self.ps.check_col(0))

        self.ps.col_runs = [[2,1,2]]
        self.assertFalse(self.ps.check_col(0))

        self.ps.arr = [[STATES.FILLED], [STATES.EMPTY], [STATES.FILLED], [STATES.EMPTY], [STATES.FILLED], [STATES.FILLED], [STATES.EMPTY]]
        self.ps.col_runs = [[1,1,2]]
        self.assertTrue(self.ps.check_col(0))

class EasySolve(unittest.TestCase):
    def setUp(self):
        self.ps = PuzzleSolver(2)

        self.ps.set_row_runs([[1], [2]])
        self.ps.set_col_runs([[2], [1]])

    def test_1(self):
        ret = self.ps.solve()

        self.assertTrue(ret)

    def test_2(self):
        self.ps.set_fixed_cells([[1, 1]])

        ret = self.ps.solve()

        self.assertFalse(ret)


class MediumSolve(unittest.TestCase):
    def setUp(self):
        self.ps = PuzzleSolver(3)

        self.ps.set_row_runs([[1, 1], [2], [1]])
        self.ps.set_col_runs([[1, 1], [1], [2]])

    def test_1(self):
        ret = self.ps.solve()
        self.assertTrue(ret)

    def test_2(self):
        self.ps.set_fixed_cells([[1, 2]])
        ret = self.ps.solve()

        self.assertTrue(ret)

def main():
    ps = PuzzleSolver(SIZE)
    ps.set_row_runs(ROW_RUNS)
    ps.set_col_runs(COL_RUNS)
    ps.set_fixed_cells(FIXED_CELLS)

    ret = ps.solve()

    if ret:
        print "Solution found!"
        ps.print_array()

    else:
        print "No solution found!"

if __name__ == "__main__":
    main()
