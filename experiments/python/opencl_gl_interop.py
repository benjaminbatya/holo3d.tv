from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.arrays import vbo
import OpenGL.arrays as GLArrays
from OpenGL.raw.GL.VERSION.GL_1_5 import glBufferData as rawGlBufferData
import pyopencl as cl
import numpy as np


n_vertices = 50000

src = """

__kernel void generate_sin(__global float2* a, const int tick)
{
    int id = get_global_id(0);
    int n = get_global_size(0);
    float r = (float)id / (float)n;
    float offset = tick;
    offset *= M_PI / 180.0f;
    float x = r * 16.0f * 3.1415f;
    a[id].x = r * 2.0f - 1.0f;
    a[id].y = native_sin(x + offset);
}

"""

ctx = None
coords_dev = None
queue = None
prog = None
pt_vbo = None
tick = 0
initialized = False

def initialize():
    global pt_vbo, ctx, coords_dev, queue, prog

    glEnableClientState(GL_VERTEX_ARRAY)

    glClearColor(1, 1, 1, 1)
    glColor(0, 0, 1)

    data = np.linspace(0.0, 2.0, n_vertices)
    data = np.vstack((data, data)).T

    # Initialize the opencl platform
    platform = cl.get_platforms()[0]

    from pyopencl.tools import get_gl_sharing_context_properties
    import sys
    if sys.platform == "darwin":
        ctx = cl.Context(properties=get_gl_sharing_context_properties(),
                devices=[])
    else:
        # Some OSs prefer clCreateContextFromType, some prefer
        # clCreateContext. Try both.
        try:
            ctx = cl.Context(properties=[
                (cl.context_properties.PLATFORM, platform)]
                + get_gl_sharing_context_properties())
        except:
            ctx = cl.Context(properties=[
                (cl.context_properties.PLATFORM, platform)]
                + get_gl_sharing_context_properties(),
                devices = [platform.get_devices()[0]])

    prog = cl.Program(ctx, src).build()
    queue = cl.CommandQueue(ctx)

    pt_vbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, pt_vbo)
    # rawGlBufferData(GL_ARRAY_BUFFER, n_vertices * 2 * 4, None, GL_DYNAMIC_DRAW)
    # rawGlBufferData(GL_ARRAY_BUFFER, n_vertices * 2 * 4, data.tostring(), GL_DYNAMIC_DRAW)
    glBufferData(GL_ARRAY_BUFFER, n_vertices * 2 * 4, data.tostring(), GL_DYNAMIC_DRAW)

    glVertexPointer(2, GL_FLOAT, 0, None)
    glBindBuffer(GL_ARRAY_BUFFER, 0)

    coords_dev = cl.GLBuffer(ctx, cl.mem_flags.READ_WRITE, int(pt_vbo))


def display():
    global pt_vbo, ctx, initialized

    if not initialized:
        idle()

    # print ("Finished generating, tick = %r" % tick)

    glBindBuffer(GL_ARRAY_BUFFER, pt_vbo)

    glClear(GL_COLOR_BUFFER_BIT)
    glDrawArrays(GL_LINE_STRIP, 0, n_vertices)
    glFlush()

    glBindBuffer(GL_ARRAY_BUFFER, 0)

def idle():
    global tick, initialized


    cl.enqueue_acquire_gl_objects(queue, [coords_dev])
    prog.generate_sin(queue, (n_vertices,), None, coords_dev, np.int32(tick))
    cl.enqueue_release_gl_objects(queue, [coords_dev])

    queue.finish()

    tick += 1

    if initialized:
        glutPostRedisplay()

    initialized = True

def reshape(w, h):
    glViewport(0, 0, w, h)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glMatrixMode(GL_MODELVIEW)

if __name__ == '__main__':
    import sys
    glutInit(sys.argv)
    if len(sys.argv) > 1:
        n_vertices = int(sys.argv[1])
    glutInitWindowSize(800, 160)
    glutInitWindowPosition(0, 0)
    glutCreateWindow('OpenCL/OpenGL Interop Tutorial: Sin Generator')
    glutDisplayFunc(display)
    glutReshapeFunc(reshape)
    glutIdleFunc(idle)
    initialize()
    glutMainLoop()
