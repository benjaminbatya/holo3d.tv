__author__ = 'benjamin'

import numpy as np
import cv2
import cv
import sys
from PySide import QtCore, QtGui

class OpenCVQImage(QtGui.QImage):

    def __init__(self, cv_img):
        print cv_img.shape
        print cv_img.dtype
        height, width, bytesPerComponent = cv_img.shape
        bytesPerLine = bytesPerComponent * width;
        cv2.cvtColor(cv_img, cv.CV_BGR2RGB, cv_img)
        super(OpenCVQImage, self).__init__(cv_img.data, width, height, bytesPerLine, QtGui.QImage.Format_RGB888)

class ImageWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(ImageWidget, self).__init__(parent)

        self._image = None

    def set_image(self, img):
        self._image = img
        self.update()

    def paintEvent(self, e):
        if self._image is None: return

        painter = QtGui.QPainter(self)
        painter.drawImage(QtCore.QPoint(0,0), self._image)

def main():

    img = cv2.imread("images/simple.jpg")
    print img.shape
    print img.dtype
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    corners = cv2.goodFeaturesToTrack(gray, 25, 0.01, 10)
    corners = np.int0(corners)

    for i in corners:
        x,y = i.ravel()
        cv2.circle(img, (x,y), 3, 255, -1)

    qt_image = OpenCVQImage(img)

    app = QtGui.QApplication(sys.argv)

    widget = ImageWidget()
    widget.set_image(qt_image)
    widget.show()

    sys,exit(app.exec_())




if __name__ == "__main__":
    main()