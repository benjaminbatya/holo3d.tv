from OpenGL import GL
from PyQt4 import QtCore, QtGui, QtOpenGL
from OpenGL.GL.ARB.texture_rg import GL_R32F
import numpy as np
import ctypes
import time
import os
import sys
import holo3d

HOLO_ROOT = str(os.environ['HOLO_ROOT'])
FILE_NAME = HOLO_ROOT + "/videos/cube_test/"

w, h = 400, 400

class TestWidget(QtOpenGL.QGLWidget):
    
    def __init__(self):
        QtOpenGL.QGLWidget.__init__(self)
        self.resize(w, h)

        self.t = time.time()        
        self._update_timer = QtCore.QTimer()
        self._update_timer.timeout.connect(self.update)        
        self._update_timer.start(1e3 / 60.)
    
    def initializeGL(self):
        # create an image
#        Y, X = np.ogrid[-2.5:2.5:h*1j, -2.5:2.5:w*1j]
#        image = np.empty((h, w), dtype=np.float32)
#        image[:] = np.exp(- X**2 - Y**2)# * (1. + .5*(np.random.rand(h, w)-.5))
#        image[-30:] = np.linspace(0, 1, w)

        file_name = FILE_NAME + '0.h3t'
        pixel_depth = 3
            
        print "loading {}".format(file_name)
        # Load the video from video_name  
        self._frame = holo3d.camera_frame()
        self._loader = holo3d.file_loader(file_name)
        self._loader.run()
        w = self._loader.width 
        h = self._loader.height
        self._num_pixels = self._loader.width * self._loader.height        
        
        factory = holo3d.factory_decoder(self._loader.width, 
                                         self._loader.height, 
                                         self._loader.width)
        self._color_decoder = factory.color(self._loader.file_header.color_format)
        self._depth_decoder = factory.depth(self._loader.file_header.depth_format)
        
        # Load the first frame
        self._loader.next_frame(self._frame)
        
        self._color_buffer = np.ndarray(shape=(self._num_pixels, pixel_depth), 
                                        dtype=np.uint8)
        self._color_decoder.decode(self._color_buffer, 
                                   self._frame.color_buffer, 
                                   self._num_pixels * pixel_depth, 
                                   self._frame.color_buffer_size)
        
        # create pixel buffer object for transferring textures
        self._buffer_id = GL.glGenBuffers(1)
        GL.glBindBuffer(GL.GL_PIXEL_UNPACK_BUFFER, self._buffer_id)
        GL.glBufferData(GL.GL_PIXEL_UNPACK_BUFFER, w*h*pixel_depth, None, GL.GL_STREAM_DRAW)
        GL.glBindBuffer(GL.GL_PIXEL_UNPACK_BUFFER, 0)

        # map and modify pixel buffer
        GL.glBindBuffer(GL.GL_PIXEL_UNPACK_BUFFER, self._buffer_id)
        pbo_addr = GL.glMapBuffer(GL.GL_PIXEL_UNPACK_BUFFER, GL.GL_WRITE_ONLY)
        # write to PBO using ctypes memmove
        ctypes.memmove(pbo_addr, self._color_buffer.ctypes.data, (self._num_pixels * pixel_depth))
        # write to PBO using numpy interface
        pbo_ptr = ctypes.cast(pbo_addr, ctypes.POINTER(ctypes.c_uint8))
        pbo_np = np.ctypeslib.as_array(pbo_ptr, shape=(self._num_pixels, pixel_depth))
        pbo_np[:] = self._color_buffer
        GL.glUnmapBuffer(GL.GL_PIXEL_UNPACK_BUFFER)
        GL.glBindBuffer(GL.GL_PIXEL_UNPACK_BUFFER, 0)
        
        # create texture from pixel buffer object
        self._texture_id = GL.glGenTextures(1)
        GL.glBindBuffer(GL.GL_PIXEL_UNPACK_BUFFER, self._buffer_id)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self._texture_id)
        GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR)
        GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR)
        GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB, 
                        w, h, 
                        0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, None)
        GL.glBindBuffer(GL.GL_PIXEL_UNPACK_BUFFER, 0)
        
        # create a shader for coloring the texture
        shader_program = QtOpenGL.QGLShaderProgram()
        vertex_src = """
        void main() {
            gl_TexCoord[0] = gl_MultiTexCoord0;
            gl_Position = gl_Vertex;
        }
        """
        fragment_src = """
        uniform highp sampler2D tex;
                
        void main() {
            float r = texture2D(tex, gl_TexCoord[0]).r;
            float g = texture2D(tex, gl_TexCoord[0]).g;
            float b = texture2D(tex, gl_TexCoord[0]).b;
            gl_FragColor = vec4(r, g, b, 0.0);
        }
        """
        shader_program.addShaderFromSourceCode(QtOpenGL.QGLShader.Vertex, vertex_src)
        shader_program.addShaderFromSourceCode(QtOpenGL.QGLShader.Fragment, fragment_src)
        shader_program.link()
        self._shader_program = shader_program
        
        self.resize(w, h)

    def paintGL(self):
        target = QtCore.QRectF(-1, -1, 2, 2)
        self._shader_program.bind()
        self.drawTexture(target, self._texture_id)
    
    def resizeGL(self, w, h):
        GL.glViewport(0, 0, w, h)


app = QtGui.QApplication(sys.argv)
win = TestWidget()
win.show()
app.exec_()