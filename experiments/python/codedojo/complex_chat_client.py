# chat_client.py

# From http://www.bogotobogo.com/python/python_network_programming_tcp_server_client_chat_server_chat_client_select.php
import sys
import time

from client_socket import ClientSocket
import collections
import threading

LOCAL_PROMPT = '>> '

_do_work = True
_queue = collections.deque()

def poll_input():
    global _do_work, _queue
    while _do_work:
        msg = sys.stdin.readline().strip()
        _queue.append(msg)

        msg = msg.lower()
        if msg == 'quit' or msg == 'exit':
            _do_work = False
            break

def chat_client(host, port):
    global _do_work, _queue

    try:
        socket = ClientSocket(host, port)

        t = threading.Thread(target=poll_input)
        t.start()

        print("Connected to remote host %s. Type in your message and press Enter to send it. Text 'quit' to quit." % socket.get_remote_addr())
        sys.stdout.write(LOCAL_PROMPT)
        sys.stdout.flush()

        while _do_work:

            if socket.has_messages():
                while socket.has_messages():
                    print(socket.get_message())

                sys.stdout.write(LOCAL_PROMPT)
                sys.stdout.flush()

            # user entered a message
            if _queue:
                while _queue:
                    socket.send_message(_queue.popleft())

                sys.stdout.write(LOCAL_PROMPT)
                sys.stdout.flush()

            time.sleep(0.1)

    finally:
        _do_work = False
        t.join()
        socket.disconnect()


if __name__ == "__main__":
    if(len(sys.argv) < 3) :
        print("Usage : python chat_client.py hostname port)")
        sys.exit()

    host = sys.argv[1]
    port = int(sys.argv[2])


    sys.exit(chat_client(host, port))
