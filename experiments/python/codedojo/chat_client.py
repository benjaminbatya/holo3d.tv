# chat_client.py

# From http://www.bogotobogo.com/python/python_network_programming_tcp_server_client_chat_server_chat_client_select.php
import sys
import os

from client_socket import ClientSocket

LOCAL_PROMPT = '[Me] '

def chat_client(host, port):

    try:
        socket = ClientSocket(host, port)

        print("Connected to remote host %s. Type in your message and press Enter to send it. Text 'quit' to quit." % socket.get_remote_addr())

        while True:

            while socket.has_messages():
                print(socket.get_message())

            sys.stdout.write(LOCAL_PROMPT)

            # user entered a message
            msg = sys.stdin.readline().strip()
            socket.send_message(msg)

            msg = msg.lower()
            if msg == 'quit' or msg == 'exit':
                break
    finally:
        socket.disconnect()


if __name__ == "__main__":
    if(len(sys.argv) < 3) :
        print("Usage : python chat_client.py hostname port)")
        sys.exit()

    host = sys.argv[1]
    port = int(sys.argv[2])


    sys.exit(chat_client(host, port))
