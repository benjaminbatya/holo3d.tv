# -*- coding: utf-8 -*-

import pyopencl as cl
import numpy as np
from time import time

if __name__ == "__main__":
    platform = cl.get_platforms()[0]
    
    device = platform.get_devices()[0]
    
    context = cl.Context([device])
    
    program = cl.Program(context, """
        __kernel void vadd(
            __global float* a,
            __global float* b,
            __global float* c,
            const unsigned int count)
        {
            int gid = get_global_id(0);
            if(gid < count)
            {
                c[gid] = a[gid] + b[gid];
            }
        }
    """).build()
    
    queue = cl.CommandQueue(context)
    
    TOL = 0.001
    LENGTH = 1024
    
    a = np.random.rand(LENGTH).astype(np.float32)
    b = np.random.rand(LENGTH).astype(np.float32)
    c = np.zeros(LENGTH, dtype=np.float32)    
    d = np.zeros(LENGTH, dtype=np.float32)    
    e = np.random.rand(LENGTH).astype(np.float32)
    f = np.zeros(LENGTH, dtype=np.float32)
    g = np.random.rand(LENGTH).astype(np.float32)
    
    #print a.shape[0]
    #print a.dtype
    
    mem_flags = cl.mem_flags
    a_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=a)
    b_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=b)
    c_buf = cl.Buffer(context, mem_flags.READ_WRITE, c.nbytes)
    d_buf = cl.Buffer(context, mem_flags.READ_WRITE, d.nbytes)
    e_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=e)
    f_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, f.nbytes)
    g_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=g)
        
    # Note: it runs faster without set_scalar_arg_dtypes
    #program.vadd.set_scalar_arg_dtypes([None, None, None, np.uint32])    

    rtime = time()    
        
    program.vadd(queue, c.shape, None, a_buf, b_buf, c_buf, np.uint32(LENGTH))

    program.vadd(queue, d.shape, None, c_buf, e_buf, d_buf, np.uint32(LENGTH))
    
    program.vadd(queue, f.shape, None, d_buf, g_buf, f_buf, np.uint32(LENGTH))
       
    delta = time()-rtime   
       
    print "Kernel ran in", delta , "secs"

    cl.enqueue_copy(queue, f, f_buf)    
    
    correct = 0
    # Check correctness
    for i in range(LENGTH):
        tmp = a[i] + b[i] + e[i] + g[i]
        tmp -= f[i]
        
        if(tmp*tmp < TOL*TOL):
            correct += 1
        else:
            print "Failed at idx=%d" % i
    
    print "num correct=%d" % correct
        

        
    
    
    

