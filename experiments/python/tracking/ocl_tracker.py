__author__ = 'benjamin'

import numpy as np
import math

from base_funcs import *

from base_tracker import BaseTracker

import pyopencl as cl
import pyopencl.array as cl_array
# import pyopencl.algorithm as cl_algo
# import pyopencl.scan as cl_scan

# cl_algo.copy_if()
# cl_scan.GenericScanKernel()

MAX_ITERATIONS = 20
MIN_POINTS_IN_ESTIMATE = 10

import os
# HACK: Disable program caching to allow opencl's printf to work
# See http://documen.tician.de/pyopencl/runtime.html#programs-and-kernels
# and http://www.thebigblob.com/opencl-bugs/
os.environ["PYOPENCL_NO_CACHE"] = "1"

# Was used for ElementwiseKernel
# class Range:
#     def __init__(self, stop, start=0, step=1):
#         self.start = start
#         self.stop = stop
#         self.step = step

class OCLTracker(BaseTracker):
    def __init__(self, left, right, bottom, top, radius, percent_samples):
        super(OCLTracker, self).__init__(left, right, bottom, top, radius, percent_samples)

        self.context = cl.create_some_context()
        self.queue = cl.CommandQueue(self.context)
        self.mf = cl.mem_flags

        self.load_program("ocl_tracker.cl")

    def load_program(self, filename):
        #read in the OpenCL source file as a string
        f = open(filename, 'r')
        fstr = "".join(f.readlines())
        #print fstr
        #create the program
        self.program = cl.Program(self.context, fstr).build()

    class ClusterContext(object):
        def __init__(self, context, clusters):

            self.num = 0
            self.means_cl = None
            self.dims_cl = None
            self.settled_cl = None

            if clusters is None: return

            if type(clusters) is int:
                self.num = clusters

                self.means_cl = cl.Buffer(context, cl.mem_flags.WRITE_ONLY, self.num * 4*2)
                self.dims_cl = cl.Buffer(context, cl.mem_flags.WRITE_ONLY, self.num * 4*2)
                self.settled_cl = cl.Buffer(context, cl.mem_flags.WRITE_ONLY, self.num * 1)

            elif type(clusters) is list:

                self.num = len(clusters)

                means = [cluster[0] for cluster in clusters]
                means_np = np.array(means, dtype=np.float32)
                self.means_cl = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=means_np)

                dims = [(cluster[1], cluster[2]) for cluster in clusters]
                dims_np = np.array(dims, dtype=np.float32)
                self.dims_cl = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=dims_np)

                settled = [1 if cluster[3] else 0 for cluster in clusters]
                settled_np = np.array(settled, dtype=np.int8) # Waste of space using char for a bool
                self.settled_cl = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=settled_np)

            else:
                assert False

        def tolist(self, queue):
            means_np = np.ndarray((self.num, 2), dtype=np.float32)
            cl.enqueue_copy(queue, means_np, self.means_cl)

            dims_np = np.ndarray((self.num, 2), dtype=np.float32)
            cl.enqueue_copy(queue, dims_np, self.dims_cl)

            settled_np = np.ndarray((self.num, ), dtype=np.int8)
            cl.enqueue_copy(queue, settled_np, self.settled_cl)

            new_clusters = []
            for i in xrange(self.num):
                mean = means_np[i].astype(np.float64)
                size = dims_np[i]
                settled = settled_np[i]

                cluster = (mean, size[0], size[1], (True if settled==1 else False))
                new_clusters.append(cluster)

            return new_clusters

    class PointContext(object):
        def __init__(self, context, points):
            self.num = 0
            self.pos_cl = None

            if points is None: return

            if type(points) is np.ndarray:
                self.num = points.shape[0]

                points_np = np.array(points, dtype=np.float32)
                self.pos_cl = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=points_np.ravel())

            elif type(points) is int:
                self.num = points

                self.pos_cl = cl.Buffer(context, cl.mem_flags.WRITE_ONLY, points*4*2)

            else:
                assert False

    def set_points(self, points):
        super(OCLTracker, self).set_points(points)

        self.points_ctx = OCLTracker.PointContext(self.context, points)

    def set_clusters(self, clusters):
        super(OCLTracker, self).set_clusters(clusters)

        self.clusters_ctx = OCLTracker.ClusterContext(self.context, clusters)

    def clusters(self):
        '''
        Call this a little as possible!! it is slow!
        :return:
        '''
        return self.clusters_ctx.tolist(self.queue)

    def track_map_points_to_indices(self):
        '''
        This is a wrapper to call track_map_points_to_indices from the outside and it converts the list/np.ndarray into a cl.Array
        :return:
        '''
        mapped_idxs_cl = self._track_map_points_cl(self.clusters_ctx, self.points_ctx)

        mapped_idxs_np = np.ndarray((self.points_ctx.num,), dtype=np.int32)
        cl.enqueue_copy(self.queue, mapped_idxs_np, mapped_idxs_cl)

        return mapped_idxs_np

        # return self.mapped_clusters_cl.get()

    def _track_map_points_cl(self, clusters_ctx, points_ctx):

        # Allocate the mapped_clusters_cl
        self.mapped_idxs_cl = cl.Buffer(self.context, cl.mem_flags.WRITE_ONLY, points_ctx.num*4)

        kernel_args = (np.int32(clusters_ctx.num),
                       clusters_ctx.means_cl, clusters_ctx.dims_cl,
                       points_ctx.pos_cl, self.mapped_idxs_cl)

        self.program.map_points(self.queue, (points_ctx.num,), None, *(kernel_args))

        return self.mapped_idxs_cl

    def track_reduce_points(self):
        # start = time.clock()

        num_histogram_elems = self._track_reduce_points_cl(self.clusters_ctx, self.points_ctx)

        num_points = len(self._points)

        # print self.clustered_idx_cl.get()

        clustered_idx_np = np.ndarray((num_points,), dtype=np.int32)
        cl.enqueue_copy(self.queue, clustered_idx_np, self.clustered_idx_cl)

        histogram_np = np.ndarray((num_histogram_elems,), dtype=np.int32)
        cl.enqueue_copy(self.queue, histogram_np, self.histogram_cl)

        sums_np = np.ndarray((num_histogram_elems+1,), dtype=np.int32)
        cl.enqueue_copy(self.queue, sums_np, self.offsets_cl)

        # end = time.clock()
        # delta = end - start
        # print("time for transfer_pt_indices = %r ms" % (delta*1000))
        # start = time.clock()

        # Split the clustered points into separate lists...
        clustered_idx = [None] * num_histogram_elems
        for idx in xrange(num_histogram_elems):
            seg_start = sums_np[idx]
            seg_end = seg_start + histogram_np[idx]
            clustered_idx[idx] = clustered_idx_np[seg_start:seg_end]

        # end = time.clock()
        # delta = end - start
        # print("time for splitting _clusters = %r ms" % (delta*1000))

        return clustered_idx

    def _track_reduce_points_cl(self, clusters_ctx, points_ctx):

        self._track_map_points_cl(clusters_ctx, points_ctx)

        # start = time.clock()

        num_clusters = np.int32(clusters_ctx.num)
        num_points = np.int32(points_ctx.num)

        num_histogram_elems = np.int32(num_clusters + 1)

        # NOTE: Experiment with this number!!!
        # Cannot exceed the number of work items in a work group
        num_histograms = 64
        min_pts = round_up_to_multiple(num_points, num_histograms)
        pts_per_histogram = np.int32(min_pts / num_histograms)
        total_elements = num_histogram_elems * num_histograms

        # allocate memory
        # NOTE: move this into __init__ later!!
        all_histograms = cl.Buffer(self.context, cl.mem_flags.WRITE_ONLY, total_elements * 4)
        # self.queue.finish()

        # end = time.clock()
        # delta = end - start
        # print("time to transfer mapped indices = %r ms" % (delta*1000))
        # start = time.clock()

        # Split the counting into one workgroup
        kernel_args = (num_points, num_clusters, pts_per_histogram, self.mapped_idxs_cl, all_histograms)
        self.program.count_mapped_pts(self.queue, (num_histograms,), None, *(kernel_args))
        # self.queue.finish()

        # end = time.clock()
        # delta = end - start
        # print("time for count_mapped_pts = %r ms" % (delta*1000))
        # start = time.clock()

        # The last index is for the untracked points
        self.histogram_cl = cl.Buffer(self.context, cl.mem_flags.WRITE_ONLY, num_histogram_elems*4)

        kernel_args = (num_clusters, np.int32(num_histograms), all_histograms, self.histogram_cl)
        self.program.build_histograms(self.queue, (1,), None, *(kernel_args))
        # self.queue.finish()

        # end = time.clock()
        # delta = end - start
        # print("time for sum_histograms = %r ms" % (delta*1000))
        # start = time.clock()

        # print histogram_cl.get()

        # end = time.clock()
        # delta = end - start
        # print("time for transfer data = %r ms" % (delta*1000))
        # start = time.clock()

        # Then sum an inclusive sume over the counts
        self.offsets_cl = cl.Buffer(self.context, cl.mem_flags.WRITE_ONLY, (num_histogram_elems+1)*4)

        kernel_args = (num_histogram_elems, self.histogram_cl, self.offsets_cl)
        self.program.sum_histogram(self.queue, (1,), None, *(kernel_args))
        # self.queue.finish()

        # print offsets_cl.get()

        # end = time.clock()
        # delta = end - start
        # print("time for inclusive_scan = %r ms" % (delta*1000))
        # start = time.clock()

        local_mem = cl.LocalMemory(num_histogram_elems*4)

        # Then pull the elements from the mapped_idxs into clustered_idx
        self.clustered_idx_cl = cl.Buffer(self.context, cl.mem_flags.WRITE_ONLY, num_points*4)

        kernel_args = (num_points, num_histogram_elems,
                       self.mapped_idxs_cl,
                       self.offsets_cl, local_mem,
                       self.clustered_idx_cl)
        self.program.transfer_pt_indices(self.queue, (1,), None, *(kernel_args))

        return num_histogram_elems

    def track_recalc_existing_clusters(self):

        self._track_recalc_existing_clusters(self.clusters_ctx, self.points_ctx)

        cluster_means_np = np.ndarray((self.clusters_ctx.num, 2), dtype=np.float32)
        cl.enqueue_copy(self.queue, cluster_means_np, self.clusters_ctx.means_cl)

        cluster_dims_np = np.ndarray((self.clusters_ctx.num, 2), dtype=np.float32)
        cl.enqueue_copy(self.queue, cluster_dims_np, self.clusters_ctx.dims_cl)

        # settled_list = [False] * self.num_clusters()

        ret = []
        for i in xrange(self.num_clusters()):
            ret.append((cluster_means_np[i],
                        cluster_dims_np[i][0], cluster_dims_np[i][1],
                        False))

        # sums_np = np.ndarray((self.num_clusters()+2,), dtype=np.int32)
        # cl.enqueue_copy(self.queue, sums_np, self.offsets_cl)
        # print "num_clusters = %r, Sums = %r" % (self.num_clusters(), sums_np)

        return ret

    def _track_recalc_existing_clusters(self, clusters_ctx, points_ctx):

        self._track_reduce_points_cl(clusters_ctx, points_ctx)

        kernel_args = (points_ctx.pos_cl, self.clustered_idx_cl,
                       self.offsets_cl, clusters_ctx.means_cl, clusters_ctx.dims_cl)
        self.program.recalc_cluster_dims(self.queue, (clusters_ctx.num,), None, *(kernel_args))

        new_count_cl = cl.Buffer(self.context, cl.mem_flags.WRITE_ONLY, 4)

        kernel_args = (np.int32(clusters_ctx.num),
                       clusters_ctx.means_cl, clusters_ctx.dims_cl, clusters_ctx.settled_cl,
                       new_count_cl)

        self.program.prune_clusters(self.queue, (1,), None, *(kernel_args))

        new_count_np = np.ndarray((1,), dtype=np.int32)
        cl.enqueue_copy(self.queue, new_count_np, new_count_cl)
        clusters_ctx.num = int(new_count_np[0])

        return clusters_ctx

    def merge_clusters(self):

        # start = time.clock()

        (new_clusters_ctx, num_unsettled) = self._merge_clusters(self.clusters_ctx)

        # end = time.clock()
        # delta = end - start
        # print("delta for ocl._merge_clusters = %r ms" % (delta*1000))

        new_clusters = new_clusters_ctx.tolist(self.queue)

        return (new_clusters, num_unsettled)

    def _merge_clusters(self, clusters_ctx):

        new_clusters = OCLTracker.ClusterContext(self.context, clusters_ctx.num)

        updated_num_clusters_cl = cl.Buffer(self.context, cl.mem_flags.WRITE_ONLY, 4)
        out_num_unsettled_cl = cl.Buffer(self.context, cl.mem_flags.WRITE_ONLY, 4)

        temp_clusters_cl = cl.Buffer(self.context, cl.mem_flags.HOST_NO_ACCESS, clusters_ctx.num*4)

        kernel_args = (np.int32(clusters_ctx.num),
                       clusters_ctx.means_cl, clusters_ctx.dims_cl, clusters_ctx.settled_cl,
                       temp_clusters_cl,
                       updated_num_clusters_cl,
                       new_clusters.means_cl, new_clusters.dims_cl, new_clusters.settled_cl,
                       out_num_unsettled_cl)

        self.program.merge_clusters(self.queue, (1, ), None, *(kernel_args))

        updated_num_clusters_np = np.ndarray((1,), dtype=np.int32)
        cl.enqueue_copy(self.queue, updated_num_clusters_np, updated_num_clusters_cl)

        new_clusters.num = int(updated_num_clusters_np[0])

        out_num_unsettled_np = np.ndarray((1,), dtype=np.int32)
        cl.enqueue_copy(self.queue, out_num_unsettled_np, out_num_unsettled_cl)

        return (new_clusters, out_num_unsettled_np[0])

    def run_EM(self):

        self._run_EM(self.clusters_ctx, self.points_ctx)

        clusters = self.clusters_ctx.tolist(self.queue)

        # sums_np = np.ndarray((self.num_clusters()+2,), dtype=np.int32)
        # cl.enqueue_copy(self.queue, sums_np, self.offsets_cl)
        # print "num_clusters = %r, Sums = %r" % (self.num_clusters(), sums_np)

        return clusters

    def _run_EM(self, clusters_ctx, points_ctx):

        self._track_reduce_points_cl(clusters_ctx, points_ctx)

        kernel_args = (points_ctx.pos_cl,
                       self.clustered_idx_cl, self.offsets_cl,
                       clusters_ctx.means_cl, clusters_ctx.dims_cl, clusters_ctx.settled_cl)
        self.program.run_EM(self.queue, (clusters_ctx.num,), None, *(kernel_args))

    def cluster_points(self):

        new_clusters = self._cluster_points(self.clusters_ctx, self.points_ctx)

        return new_clusters.tolist(self.queue)

    def _cluster_points(self, clusters_ctx, points_ctx):
        # print("OCLTracker.cluster_points: called, num clusters = %r" % clusters_ctx.num)

        for i in xrange(MAX_ITERATIONS):

            (new_clusters_ctx, num_unsettled) = self._merge_clusters(clusters_ctx)

            clusters_ctx = new_clusters_ctx

            # print "iteration %r, After pruning, num unsettled _clusters = %r" % (i, num_unsettled)
            # clusters = clusters_ctx.tolist(self.queue)
            # print "clusters = %r" % clusters

            if num_unsettled == 0:
                break

            self._run_EM(clusters_ctx, points_ctx)

        return clusters_ctx

    def track_clusters(self):

        self.clusters_ctx = self._track_clusters(self.clusters_ctx, self.points_ctx)

        return self.clusters_ctx.tolist(self.queue)

    def _track_clusters(self, clusters_ctx, points_ctx):

        self._track_recalc_existing_clusters(clusters_ctx, points_ctx)

        kernel_args = (np.int32(clusters_ctx.num), clusters_ctx.settled_cl)
        self.program.unsettle_clusters(self.queue, (1,), None, *(kernel_args))

        histogram_np = np.ndarray((clusters_ctx.num+1,), dtype=np.int32)
        cl.enqueue_copy(self.queue, histogram_np, self.histogram_cl)

        num_untracked_points = int(histogram_np[clusters_ctx.num])

        if num_untracked_points==0: return clusters_ctx

        untracked_points = OCLTracker.PointContext(self.context, num_untracked_points)

        num_valid_points_cl = cl.Buffer(self.context, cl.mem_flags.READ_ONLY, 1*4)

        # Note that the last element self.offsets_cl and self.histogram_cl is for the untracked points
        kernel_args = (np.int32(self.left), np.int32(self.right), np.int32(self.bottom), np.int32(self.top),
                       np.int32(clusters_ctx.num),
                       self.offsets_cl,
                       self.clustered_idx_cl, # NOTE: get rid of this remapping index!! Just make it an array of points!
                       points_ctx.pos_cl,
                       untracked_points.pos_cl,
                       num_valid_points_cl)
        self.program.populate_untracked_points(self.queue, (1,), None, *(kernel_args))

        num_valid_points_np = np.ndarray((1,), dtype=np.int32)
        cl.enqueue_copy(self.queue, num_valid_points_np, num_valid_points_cl)
        num_valid_points = int(num_valid_points_np[0])

        # Allocate memory for the number of untracked points
        num_samples = int(num_untracked_points * self.percent_samples)

        if num_samples < MIN_POINTS_IN_ESTIMATE: return clusters_ctx

        # untracked_clusters = OCLTracker.ClusterContext(self.context, num_samples)
        #
        # chosen_indices = np.random.choice(num_valid_points, num_samples)
        #
        # # Now that we have the untracked indices, choose a random sample of them with percent_samples frequency
        # kernel_args = \
        #     (np.int32(num_valid_points), untracked_points.pos_cl,
        #      np.int32(num_samples), chosen_indices,
        #      untracked_clusters.means_cl, untracked_clusters.dims_cl, untracked_clusters.settled_cl)
        # self.program.populate_sampled_clusters(self.queue, (1,), None, *(kernel_args))

        return clusters_ctx

        # # Using the untracked points, relax the reduce the new clusters using cluster_points
        # new_clusters = self._cluster_points(untracked_clusters, untracked_points)
        #
        # # Append the new clusters to the list of tracked clusters
        # # Note: this is a really stupid way to manage the current set of clusters
        # # Instead, there should be an array of known clusters with a very large max size
        # # with which all of the cluster manipulations can take place
        # num_total_clusters = clusters_ctx.num + new_clusters.num
        #
        # total_clusters = OCLTracker.ClusterContext(self.context, num_total_clusters)
        #
        # kernel_args = ( np.int32(clusters_ctx.num), clusters_ctx.means_cl, clusters_ctx.dims_cl, clusters_ctx.settled_cl,
        #                 np.int32(new_clusters.num), new_clusters.means_cl, new_clusters.dims_cl, new_clusters.settled_cl,
        #                 np.int32(total_clusters.num), total_clusters.means_cl, clusters_ctx.dims_cl, total_clusters.settled_cl)
        # self.program.add_clusters_together(self.queue, (1,), None, *(kernel_args))
        #
        # return total_clusters

    def regen_clusters(self):
        return self.cluster_points()

if __name__ == "__main__":
    ###
    # Unittests for the opencl implementation
    ###
    import unittest
    from naive_tracker import NaiveTracker
    import time

    (LEFT, RIGHT, BOTTOM, TOP) = (-200, 200, -200, 200)
    (RADIUS, PERCENT_SAMPLES) = (30.0, 0.1)

    class TestOpenCL_Tracking(unittest.TestCase):

        def setUp(self):
            init_rand()

            self.naive = NaiveTracker(LEFT, RIGHT, TOP, BOTTOM, RADIUS, PERCENT_SAMPLES)

            self.ocl = OCLTracker(LEFT, RIGHT, TOP, BOTTOM, RADIUS, PERCENT_SAMPLES)

            self.blobs = gen_blobs(LEFT, RIGHT, BOTTOM, TOP) #, 20, 200)

            (self.indices, self.points, self.clusters) = init_data(self.blobs, PERCENT_SAMPLES, RADIUS**2)

            self.naive.set_points(self.points)
            self.ocl.set_points(self.points)

            self.naive.set_clusters(self.clusters)

            self.naive.regen_clusters()

            # NOTE: use the naive clusters to initialize the ocl tracker
            self.ocl.set_clusters(self.naive.clusters())

            # total = 0
            # for blob in self.blobs:
            #     total += blob['count']
            #
            # print "Total = %r" % total

        def tearDown(self):
            pass

        def test_map_clusters(self):

            start = time.clock()

            mapped_naive = self.naive.track_map_points_to_indices()

            end = time.clock()
            delta = end - start
            print("delta for naive track_map_points_to_indices = %r ms" % (delta*1000))
            start = time.clock()

            mapped_cl = self.ocl.track_map_points_to_indices()

            end = time.clock()
            delta = end - start
            print("delta for ocl track_map_points_to_indices = %r ms" % (delta*1000))

            self.assertEquals(mapped_cl.shape, mapped_naive.shape)
            self.assertEquals(mapped_cl.dtype, mapped_naive.dtype)

            for idx in xrange(len(self.points)):
                self.assertEquals(mapped_cl[idx], mapped_naive[idx])

        def test_reduce_clusters(self):

            start = time.clock()

            clustered_idx_naive = self.naive.track_reduce_points()

            end = time.clock()
            delta = end - start
            print("delta for naive reduce = %r ms" % (delta*1000))

            start = time.clock()

            clustered_idx_ocl = self.ocl.track_reduce_points()

            end = time.clock()
            delta = end - start
            print("delta for ocl reduce = %r ms" % (delta*1000))

            self.assertEquals(len(clustered_idx_ocl), len(self.naive.clusters())+1)

            self.assertEquals(len(clustered_idx_ocl), len(clustered_idx_naive))

            # NOTE: the ocl returns numpy arrays and the naive returns lists

            for idx in xrange(len(clustered_idx_naive)):
                self.assertEquals(clustered_idx_ocl[idx].tolist(), clustered_idx_naive[idx])

        def test_recalc_clusters(self):

            start = time.clock()

            updated_clusters_naive = self.naive.track_recalc_existing_clusters()

            end = time.clock()
            delta = end - start
            print("delta for naive.track_recalc_existing_clusters = %r ms" % (delta*1000))
            start = time.clock()

            updated_clusters_ocl = self.ocl.track_recalc_existing_clusters()

            end = time.clock()
            delta = end - start
            print("delta for ocl.track_recalc_existing_clusters = %r ms" % (delta*1000))

            self.assertEquals(len(updated_clusters_ocl), self.ocl.num_clusters())

            self.assertEquals(len(updated_clusters_ocl), len(updated_clusters_naive))

            for idx in xrange(self.ocl.num_clusters()):
                # print "idx = %r, ocl[0]=%r, naive[0]=%r" % (idx, updated_clusters_ocl[idx][0], updated_clusters_naive[idx][0])
                self.assertTrue(np.allclose(updated_clusters_ocl[idx][0], updated_clusters_naive[idx][0]))
                # Note: I think that ocl and naive have to be compared to 0.001 precision because
                # in opencl we are using float precision and in naive we are using double precision
                self.assertAlmostEquals(updated_clusters_ocl[idx][1], updated_clusters_naive[idx][1], None, None, 0.001)
                self.assertAlmostEquals(updated_clusters_ocl[idx][2], updated_clusters_naive[idx][2], None, None, 0.001)
                self.assertEquals(updated_clusters_ocl[idx][3], updated_clusters_naive[idx][3])

        def test_regen_clusters(self):

            naive_clusters = self.naive.regen_clusters()

            ocl_clusters = self.ocl.regen_clusters()

            naive_clusters.sort(key=lambda c: c[1])
            ocl_clusters.sort(key=lambda c: c[1])

            self.assertEquals(len(ocl_clusters), self.ocl.num_clusters())

            self.assertEquals(len(ocl_clusters), len(naive_clusters))

            for idx in xrange(self.ocl.num_clusters()):
                # print "idx = %r, ocl[0]=%r, naive[0]=%r" % (idx, ocl_clusters[idx][0], naive_clusters[idx][0])
                self.assertTrue(np.allclose(ocl_clusters[idx][0], naive_clusters[idx][0]))
                # Note: I think that ocl and naive have to be compared to 0.001 precision because
                # in opencl we are using float precision and in naive we are using double precision
                self.assertAlmostEquals(ocl_clusters[idx][1], naive_clusters[idx][1], None, None, 0.001)
                self.assertAlmostEquals(ocl_clusters[idx][2], naive_clusters[idx][2], None, None, 0.001)
                self.assertEquals(ocl_clusters[idx][3], naive_clusters[idx][3])

        def test_track_clusters(self):

            start = time.clock()

            naive_clusters = self.naive.track_clusters()

            end = time.clock()
            delta = end - start
            print("delta for naive.track_clusters = %r ms" % (delta*1000))
            start = time.clock()

            ocl_clusters = self.ocl.track_clusters()

            end = time.clock()
            delta = end - start
            print("delta for ocl.track_clusters = %r ms" % (delta*1000))

            self.assertEquals(len(ocl_clusters), self.ocl.num_clusters())

            self.assertEquals(len(ocl_clusters), len(naive_clusters))

            for idx in xrange(self.ocl.num_clusters()):
                # print "idx = %r, ocl[0]=%r, naive[0]=%r" % (idx, ocl_clusters[idx][0], naive_clusters[idx][0])
                self.assertTrue(np.allclose(ocl_clusters[idx][0], naive_clusters[idx][0]))
                # Note: I think that ocl and naive have to be compared to 0.001 precision because
                # in opencl we are using float precision and in naive we are using double precision
                self.assertAlmostEquals(ocl_clusters[idx][1], naive_clusters[idx][1], None, None, 0.001)
                self.assertAlmostEquals(ocl_clusters[idx][2], naive_clusters[idx][2], None, None, 0.001)
                self.assertEquals(ocl_clusters[idx][3], naive_clusters[idx][3])

            # self.blobs = gen_blobs(LEFT, RIGHT, BOTTOM, TOP, 20, 2000)
            # (self.indices, self.points, self.clusters) = init_data(self.blobs, PERCENT_SAMPLES, RADIUS**2)
            #
            # print "num clusters = %r" % len(self.clusters)
            #
            # self.naive.set_points(self.points)
            # self.ocl.set_points(self.points)
            #
            # self.naive.regen_clusters()
            # self.ocl.regen_clusters()
            #
            # print "Before"
            # print "naive results = %r" % len(self.naive._clusters)
            # print "ocl results = %r" % len(self.ocl.clusters_ctx.tolist(self.ocl.queue))
            #
            # start = time.clock()
            #
            # naive_clusters = self.naive.track_clusters()
            #
            # end = time.clock()
            # delta = end - start
            # print("delta for naive.track_clusters = %r ms" % (delta*1000))
            # start = time.clock()
            #
            # ocl_clusters = self.ocl.track_clusters()
            #
            # end = time.clock()
            # delta = end - start
            # print("delta for ocl.track_clusters = %r ms" % (delta*1000))
            #
            # naive_clusters.sort(key=lambda c: c[1])
            # ocl_clusters.sort(key=lambda c: c[1])
            #
            # self.assertEquals(len(ocl_clusters), len(naive_clusters))
            #
            # print "After"
            # print "naive results = %r" % naive_clusters
            # print "ocl results = %r" % ocl_clusters
            #
            # for idx in xrange(self.ocl.num_clusters()):
            #     print "idx = %r, ocl[0]=%r, naive[0]=%r" % (idx, ocl_clusters[idx][0], naive_clusters[idx][0])
            #     self.assertTrue(np.allclose(ocl_clusters[idx][0], naive_clusters[idx][0]))
            #     # Note: I think that ocl and naive have to be compared to 0.001 precision because
            #     # in opencl we are using float precision and in naive we are using double precision
            #     self.assertAlmostEquals(ocl_clusters[idx][1], naive_clusters[idx][1], None, None, 0.001)
            #     self.assertAlmostEquals(ocl_clusters[idx][2], naive_clusters[idx][2], None, None, 0.001)
            #     self.assertEquals(ocl_clusters[idx][3], naive_clusters[idx][3])

    class TestOpenCL_Clustering(unittest.TestCase):
        def setUp(self):
            init_rand()

            self.naive = NaiveTracker(LEFT, RIGHT, TOP, BOTTOM, RADIUS, PERCENT_SAMPLES)

            self.ocl = OCLTracker(LEFT, RIGHT, TOP, BOTTOM, RADIUS, PERCENT_SAMPLES)

            self.blobs = gen_blobs(LEFT, RIGHT, BOTTOM, TOP)#, 40, 200)

            # total = 0
            # for blob in self.blobs:
            #     total += blob['count']
            #
            # print "Total = %r" % total

            (self.indices, self.points, self.clusters) = init_data(self.blobs, PERCENT_SAMPLES, RADIUS**2)

            # print "Num clusters = %r" % len(self.clusters)

            self.naive.set_points(self.points)
            self.ocl.set_points(self.points)

            self.naive.set_clusters(self.clusters)

            self.ocl.set_clusters(self.clusters)

        def test_merge_clusters(self):

            start = time.clock()

            (naive_clusters, naive_unsettled) = self.naive.merge_clusters(self.clusters)

            end = time.clock()
            delta = end - start
            print("delta for naive.merge_clusters = %r ms" % (delta*1000))
            start = time.clock()

            (partitioned_clusters, partitioned_unsettled) = self.naive.partitioned_merge_clusters(self.clusters)

            end = time.clock()
            delta = end - start
            print("delta for naive.partitioned_merge_clusters = %r ms" % (delta*1000))
            start = time.clock()

            (ocl_clusters, ocl_unsettled) = self.ocl.merge_clusters()

            end = time.clock()
            delta = end - start
            print("delta for ocl.merge_clusters = %r ms" % (delta*1000))

            self.assertEquals(ocl_unsettled, naive_unsettled)
            self.assertEquals(len(ocl_clusters), len(naive_clusters))

            for idx in xrange(len(ocl_clusters)):
                self.assertTrue(np.allclose(ocl_clusters[idx][0], naive_clusters[idx][0]))
                # Note: I think that ocl and naive have to be compared to 0.001 precision because
                # in opencl we are using float precision and in naive we are using double precision
                self.assertAlmostEquals(ocl_clusters[idx][1], naive_clusters[idx][1], None, None, 0.01)
                self.assertAlmostEquals(ocl_clusters[idx][2], naive_clusters[idx][2], None, None, 0.01)
                self.assertEquals(ocl_clusters[idx][3], naive_clusters[idx][3])

        def test_EM(self):
            start = time.clock()

            updated_clusters_naive = self.naive.run_new_EM(self.clusters, self.points)

            end = time.clock()
            delta = end - start
            print("delta for naive.run_EM = %r ms" % (delta*1000))
            start = time.clock()

            updated_clusters_ocl = self.ocl.run_EM()

            end = time.clock()
            delta = end - start
            print("delta for ocl.run_EM = %r ms" % (delta*1000))

            self.assertEquals(len(updated_clusters_ocl), self.ocl.num_clusters())

            self.assertEquals(len(updated_clusters_ocl), len(updated_clusters_naive))

            for idx in xrange(self.ocl.num_clusters()):
                # print "idx = %r" % idx
                # print "mean = %r vs %r, width = %r vs %r, height = %r vs %s, settled = %r vs %r" % \
                #       (updated_clusters_ocl[idx][0], updated_clusters_naive[idx][0],
                #        updated_clusters_ocl[idx][1], updated_clusters_naive[idx][1],
                #        updated_clusters_ocl[idx][2], updated_clusters_naive[idx][2],
                #        updated_clusters_ocl[idx][3], updated_clusters_naive[idx][3])
                self.assertTrue(np.allclose(updated_clusters_ocl[idx][0], updated_clusters_naive[idx][0]))
                # Note: I think that ocl and naive have to be compared to 0.001 precision because
                # in opencl we are using float precision and in naive we are using double precision
                self.assertAlmostEquals(updated_clusters_ocl[idx][1], updated_clusters_naive[idx][1], None, None, 0.01)
                self.assertAlmostEquals(updated_clusters_ocl[idx][2], updated_clusters_naive[idx][2], None, None, 0.01)
                self.assertEquals(updated_clusters_ocl[idx][3], updated_clusters_naive[idx][3])

        def test_cluster_points(self):

            start = time.clock()

            naive_clusters = self.naive.cluster_points(self.clusters, self.points)

            end = time.clock()
            delta = end - start
            print("delta for naive.cluster_points = %r ms" % (delta*1000))
            start = time.clock()

            ocl_clusters = self.ocl.cluster_points()

            end = time.clock()
            delta = end - start
            print("delta for ocl.cluster_points = %r ms" % (delta*1000))

            self.assertEquals(len(ocl_clusters), len(naive_clusters))

            naive_clusters.sort(key=lambda c: c[1])
            ocl_clusters.sort(key=lambda c: c[1])

            # print "naive results = %r" % naive_clusters
            # print "ocl results = %r" % ocl_clusters

            for idx in xrange(len(ocl_clusters)):
                # print "idx = %r" % idx
                # print "mean = %r vs %r, width = %r vs %r, height = %r vs %s, settled = %r vs %r" % \
                #       (ocl_clusters[idx][0], naive_clusters[idx][0],
                #        ocl_clusters[idx][1], naive_clusters[idx][1],
                #        ocl_clusters[idx][2], naive_clusters[idx][2],
                #        ocl_clusters[idx][3], naive_clusters[idx][3])
                self.assertTrue(np.allclose(ocl_clusters[idx][0], naive_clusters[idx][0], atol=0.01))
                # Note: I think that ocl and naive have to be compared to 0.001 precision because
                # in opencl we are using float precision and in naive we are using double precision
                self.assertAlmostEquals(ocl_clusters[idx][1], naive_clusters[idx][1], None, None, 0.01)
                self.assertAlmostEquals(ocl_clusters[idx][2], naive_clusters[idx][2], None, None, 0.01)
                self.assertEquals(ocl_clusters[idx][3], naive_clusters[idx][3])

        def test_cluster_points2(self):

            self.blobs = gen_blobs(LEFT, RIGHT, BOTTOM, TOP, 40, 200)

            # total = 0
            # for blob in self.blobs:
            #     total += blob['count']
            #
            # print "Total = %r" % total

            (self.indices, self.points, self.clusters) = init_data(self.blobs, PERCENT_SAMPLES, RADIUS**2)

            print "Num clusters = %r" % len(self.clusters)

            self.naive.set_points(self.points)
            self.ocl.set_points(self.points)

            self.naive.set_clusters(self.clusters)

            self.ocl.set_clusters(self.clusters)

            start = time.clock()

            naive_clusters = self.naive.cluster_points(self.clusters, self.points)

            end = time.clock()
            delta = end - start
            print("delta for naive.cluster_points2 = %r ms" % (delta*1000))
            start = time.clock()

            ocl_clusters = self.ocl.cluster_points()

            end = time.clock()
            delta = end - start
            print("delta for ocl.cluster_points2 = %r ms" % (delta*1000))

            self.assertEquals(len(ocl_clusters), len(naive_clusters))

            naive_clusters.sort(key=lambda c: c[1])
            ocl_clusters.sort(key=lambda c: c[1])

            # print "naive results = %r" % naive_clusters
            # print "ocl results = %r" % ocl_clusters

            for idx in xrange(len(ocl_clusters)):
                # print "idx = %r" % idx
                # print "mean = %r vs %r, width = %r vs %r, height = %r vs %s, settled = %r vs %r" % \
                #       (ocl_clusters[idx][0], naive_clusters[idx][0],
                #        ocl_clusters[idx][1], naive_clusters[idx][1],
                #        ocl_clusters[idx][2], naive_clusters[idx][2],
                #        ocl_clusters[idx][3], naive_clusters[idx][3])
                self.assertTrue(np.allclose(ocl_clusters[idx][0], naive_clusters[idx][0], atol=0.01))
                # Note: I think that ocl and naive have to be compared to 0.001 precision because
                # in opencl we are using float precision and in naive we are using double precision
                self.assertAlmostEquals(ocl_clusters[idx][1], naive_clusters[idx][1], None, None, 0.01)
                self.assertAlmostEquals(ocl_clusters[idx][2], naive_clusters[idx][2], None, None, 0.01)
                self.assertEquals(ocl_clusters[idx][3], naive_clusters[idx][3])



    unittest.main()

