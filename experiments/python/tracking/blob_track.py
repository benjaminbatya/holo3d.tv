__author__ = 'benjamin'

import numpy as np
import math

from PySide import QtCore, QtGui

from base_funcs import init_rand, gen_points, gen_blobs, init_data
from naive_tracker import NaiveTracker
from ocl_tracker import OCLTracker

PERCENT_SAMPLES = 0.1

FRAME_WAIT = 200

LEFT = -200
RIGHT = 200
TOP = 200
BOTTOM = -200

RADIUS = 30.0

RADIUS_SQ = RADIUS ** 2

def main():
    # Always use the same seed for testing
    init_rand()

    import sys
    app = QtGui.QApplication(sys.argv)

    dialog = MyDialog()
    dialog.setModal(False)
    dialog.show()

    app.exec_()

class MyDialog(QtGui.QDialog):
    def __init__(self):
        super(MyDialog, self).__init__()

        self.start_button = QtGui.QPushButton("&Play", self)
        self.start_button.clicked.connect(self.start_clicked)

        self.gen_new_button = QtGui.QPushButton("&Gen New", self)
        self.gen_new_button.clicked.connect(self.gen_new_clicked)

        self.recluster_button = QtGui.QPushButton("&Recluster", self)
        self.recluster_button.clicked.connect(self.recluster_clicked)

        self.back_button = QtGui.QPushButton("&Back One", self)
        self.back_button.clicked.connect(self.back_clicked)

        self.forward_button = QtGui.QPushButton("&Forward One", self)
        self.forward_button.clicked.connect(self.forward_clicked)

        self.quit_button = QtGui.QPushButton("&Quit", self)
        self.quit_button.clicked.connect(self.quit_clicked)

        self.scene = QtGui.QGraphicsScene(self)

        # self.scene.addText("Hello World!")
        self.view = QtGui.QGraphicsView(self.scene, self)

        self.scene.setSceneRect(LEFT, BOTTOM, RIGHT-LEFT, TOP-BOTTOM)

        self.resize(700, 700)

        button_layout = QtGui.QHBoxLayout()
        button_layout.addWidget(self.start_button)
        button_layout.addWidget(self.gen_new_button)
        button_layout.addWidget(self.recluster_button)
        button_layout.addWidget(self.back_button)
        button_layout.addWidget(self.forward_button)
        button_layout.addStretch(1)
        button_layout.addWidget(self.quit_button)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.view)
        layout.addLayout(button_layout)

        self.setLayout(layout)

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.advance_sim)

        self.running = False

        self.tracker = NaiveTracker(LEFT, RIGHT, BOTTOM, TOP, RADIUS, PERCENT_SAMPLES)
        # self.tracker = OCLTracker(LEFT, RIGHT, BOTTOM, TOP, RADIUS, PERCENT_SAMPLES)

        self.gen_blobs()

        self.init_data()

        self.advance_sim()

    @QtCore.Slot()
    def start_clicked(self):
        print("start_clicked: called")
        self.set_running(not self.running)

    def set_running(self, flag):
        self.running = flag
        if self.running:
            self.start_button.setText(self.tr("&Stop"))
            self.timer.start(FRAME_WAIT)
        else:
            self.start_button.setText(self.tr("&Play"))
            self.timer.stop()

    @QtCore.Slot()
    def gen_new_clicked(self):
        # self.set_running(False)
        # for blob in self.blobs:
        #     blob['pos'] = np.array([ blob['x'], blob['y'] ])

        self.gen_blobs()

        self.init_data()

        self.advance_sim()

    @QtCore.Slot()
    def recluster_clicked(self):
        self.init_data()

        self.advance_sim()

    @QtCore.Slot()
    def back_clicked(self):
        self.set_running(False)

        self.reverse_sim()

    @QtCore.Slot()
    def forward_clicked(self):
        self.set_running(False)

        self.advance_sim()

    @QtCore.Slot()
    def quit_clicked(self):
        print("quit_clicked: called")
        QtGui.QApplication.instance().quit()

    def wheelEvent(self, event):
        """
        Zoom in or out of the view.
        """
        zoomInFactor = 1.25
        zoomOutFactor = 1 / zoomInFactor

        # Save the scene pos
        oldPos = self.view.mapToScene(event.pos())

        # Zoom
        if event.delta() > 0:
            zoomFactor = zoomInFactor
        else:
            zoomFactor = zoomOutFactor
        self.view.scale(zoomFactor, zoomFactor)

        # Get the new position
        newPos = self.view.mapToScene(event.pos())

        # Move scene to old position
        delta = newPos - oldPos
        self.view.translate(delta.x(), delta.y())

    def gen_blobs(self):
        self.blobs = gen_blobs(LEFT, RIGHT, BOTTOM, TOP)

    def init_data(self):
        (self.indices, self.points, clusters) = init_data(self.blobs, PERCENT_SAMPLES, RADIUS_SQ)

        self.tracker.set_points(self.points)
        self.tracker.set_clusters(clusters)

        self.tracker.regen_clusters()

        self.frame_num = 0

    def render_view(self):
        scene = self.scene

        scene.clear()

        # for i in xrange(self._num):
        #     self.scene.addEllipse(100+(i%16)*20, 100 + int(i/16)*20, 5, 5, QtGui.QPen(QtCore.Qt.darkBlue))

        for i in xrange(len(self.blobs)):
            blob = self.blobs[i]
            # print "Blob %r = %r" % (i, blob)
            rect = QtCore.QRectF()
            scene.addEllipse(blob['pos'][0]-2*blob['w'], blob['pos'][1]-2*blob['h'],
                             4*blob['w'], 4*blob['h'],
                             QtGui.QPen(QtCore.Qt.green))

        for pt in self.points:
            scene.addEllipse(pt[0]-2, pt[1]-2, 4, 4, QtGui.QPen(QtCore.Qt.black))

        # for idx in self.indices:
        #     scene.addEllipse(self.points[idx][0], self.points[idx][1], 5, 5,
        #                      QtGui.QPen(QtCore.Qt.black),
        #                      QtGui.QBrush(QtCore.Qt.blue))

        idx = 0
        for (mean, width_sq, height_sq, settled) in self.tracker.clusters():
            width = math.sqrt(width_sq)
            height = math.sqrt(height_sq)
            # print "Estimate %r: mean=%r, width=%r, height=%r" % ( idx, mean, width, height )
            color = QtCore.Qt.red if not settled else QtCore.Qt.blue
            scene.addEllipse(mean[0]-width, mean[1]-height, 2*width, 2*height, QtGui.QPen(color))
            idx += 1

        # Draw a frame around the scene
        pen = QtGui.QPen(QtCore.Qt.black)
        pen.setWidth(2)
        scene.addRect(LEFT, BOTTOM, RIGHT-LEFT, TOP-BOTTOM, pen)

    @QtCore.Slot()
    def advance_sim(self):
        self.frame_num += 1
        # print "advance_sim: called, frame_num = %r, num blobs = %r, num _clusters = %r" % \
        #       (self.frame_num, len(self.blobs), len(self._clusters))

        # First re-render the view
        self.render_view()

        # Update the blobs
        update_blobs(self.blobs)

        # Generate a new set of random points
        self.points = gen_points(self.blobs)

        self.tracker.set_points(self.points)

        # Track the _clusters based on the new points
        self.tracker.track_clusters()

    def reverse_sim(self):
        self.frame_num -= 1
        # print "reverse_sim: called, frame_num = %r" % self.frame_num

        self.render_view()

        # Update the blobs
        update_blobs(self.blobs, False)

        self.points = gen_points(self.blobs)

        self.tracker.set_points(self.points)

        # Track the _clusters based on the new points
        # NOTE: tracking backwards will FAIL to wrap the _clusters to their opposite points
        # The reverse logic is possible to do with flags
        self.tracker.track_clusters()

def update_blobs(blobs, forward = True):
    for blob in blobs:
        pos = blob['pos']
        dir = blob['dir']
        size = blob['size']

        # print ("updating blob %r" % blob)
        # blob['pos'] = rot_matrix.dot(blob['pos'])
        if forward:
            pos += dir

            if( (dir[0] < 0 and pos[0] + 2*size[0] < LEFT) or
                (dir[0] > 0 and pos[0] - 2*size[0] > RIGHT) or
                (dir[1] < 0 and pos[1] + 2*size[1] < BOTTOM) or
                (dir[1] > 0 and pos[1] - 2*size[1] > TOP) ):

                pos = np.array([ blob['x'], blob['y'] ])

                # try to find the intersection point
                t_l = float(pos[0] + 2*size[0] - LEFT) / dir[0] if dir[0] != 0 else -1
                if t_l < 0: t_l = 10**6
                t_r = float(pos[0] - 2*size[0] - RIGHT) / dir[0] if dir[0] != 0 else -1
                if t_r < 0: t_r = 10**6
                t_b = float(pos[1] + 2*size[1] - BOTTOM) / dir[1] if dir[1] != 0 else -1
                if t_b < 0: t_b = 10**6
                t_t = float(pos[1] - 2*size[1] - TOP) / dir[1] if dir[1] != 0 else -1
                if t_t < 0: t_t = 10**6

                t = float(min(t_l, t_r, t_b, t_t))

                # Reset pos
                pos = (pos - dir * t)
                pos = pos.astype(np.int32)
        else:

            pos -= dir
            # NOTE: in reverse, no wrapping is attempted

        blob['pos'] = pos


if __name__ == "__main__":
    main()
