__author__ = 'benjamin'

import numpy as np

class BaseTracker(object):
    def __init__(self, left, right, bottom, top, radius, percent_samples):
        self.left = left
        self.right = right
        self.bottom = bottom
        self.top = top
        self.radius = radius
        self.radius_sq = self.radius**2
        self.percent_samples = percent_samples

    def set_points(self, points):
        '''
        sets the internal points
        :param points: must be convertible to numpy.ndarray format
        :return: none
        '''
        self._points = np.array(points)

    def num_points(self):
        return len(self._points)

    def set_clusters(self, clusters):
        '''

        :param clusters: assumed to be in [(mean, size, settled)] format
        :return:
        '''
        self._clusters = clusters

    def clusters(self):
        return self._clusters

    def num_clusters(self):
        return len(self._clusters)

