__author__ = 'benjamin'


from base_funcs import *

import unittest
import time

from holo3d import CPPTracker

from naive_tracker import NaiveTracker

(LEFT, RIGHT, BOTTOM, TOP) = (-200, 200, -200, 200)
(RADIUS, PERCENT_SAMPLES) = (30.0, 0.1)

class Test_CPPTracker(unittest.TestCase):
    def setUp(self):
        print "Hello?"
        init_rand()

        self.naive = NaiveTracker(LEFT, RIGHT, TOP, BOTTOM, RADIUS, PERCENT_SAMPLES)

        self.cpp = CPPTracker(LEFT, RIGHT, TOP, BOTTOM, RADIUS, PERCENT_SAMPLES)

        self.blobs = gen_blobs(LEFT, RIGHT, BOTTOM, TOP) #, 20, 200)

        (self.indices, self.points, self.clusters) = init_data(self.blobs, PERCENT_SAMPLES, RADIUS**2)

        self.naive.set_points(self.points)
        self.cpp.set_points(self.points)

        self.naive.set_clusters(self.clusters)

        self.naive.regen_clusters()

        # NOTE: use the naive clusters to initialize the ocl tracker
        self.cpp.set_clusters(self.naive.clusters())

        total = 0
        for blob in self.blobs:
            total += blob['count']

        print "Total = %r" % total

    def tearDown(self):
        pass

    def test_map_clusters(self):

        start = time.clock()

        mapped_naive = self.naive.track_map_points_to_indices()

        end = time.clock()
        delta = end - start
        print("delta for naive track_map_points_to_indices = %r ms" % (delta*1000))
        start = time.clock()

        mapped_cpp = self.cpp.track_map_points_to_indices()

        end = time.clock()
        delta = end - start
        print("delta for ocl track_map_points_to_indices = %r ms" % (delta*1000))

        self.assertEquals(mapped_cpp.shape, mapped_naive.shape)
        self.assertEquals(mapped_cpp.dtype, mapped_naive.dtype)

        for idx in xrange(len(self.points)):
            self.assertEquals(mapped_cpp[idx], mapped_naive[idx])
