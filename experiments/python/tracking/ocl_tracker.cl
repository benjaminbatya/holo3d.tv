
// NOTE: the PYOPENCL_COMPILER_OUTPUT environment variable MUST be set to 1 for this to not complain
// See http://stackoverflow.com/questions/20722242/compilerwarning-with-opencl
// I have a idea that printf doesn't work well if there is more the one context initialized at the same time
#define INFO(_format, ...) {\
    printf((__constant char *)"(%u,%u,%u) ", get_global_id(0), get_group_id(0), get_local_id(0)); \
    printf((__constant char *)(_format), __VA_ARGS__); \
    printf((__constant char *)"%s", "\n"); \
}

#define ASSERT(x) { \
    if(!(x)) { \
        INFO("Assert(%s) failed at line %d", #x, __LINE__); \
        return; \
	}; \
}

#define ZERO_INFO(...) { if(gid==0) INFO(__VA_ARGS__); }

#define MIN_POINTS_IN_ESTIMATE 10

__kernel void map_points(   const    int     num_clusters,
                            __global float2* clusters,
                            __global float2* cluster_dims,
                            __global float2* points,
                            __global int*    mapped )
{
    int gid = get_global_id(0);
    // if(i == 0) { INFO("global_size = %d, local_size=%d", get_global_size(0), get_local_size(0)); }
    float2 pt = points[gid];
//    if(gid < 10) INFO("idx=%d: pt = %v2f", gid, pt);
    int best_cluster = num_clusters;
    float min_dist_sq = 10000000000.0f;
    for(int idx=0; idx<num_clusters; idx++)
    {
        float2 delta = clusters[idx] - pt;

//        ZERO_INFO("cluster=%d, clusters[idx]=%v2f, pt = %v2f", idx, clusters[idx], pt);
//        ZERO_INFO("cluster=%d, delta = %v2f", idx, delta);

        float dist_sq = dot(delta, delta);

//        ZERO_INFO("cluster=%d, delta_sq = %f, min_dist_sq=%f", idx, dist_sq, min_dist_sq);

        if(dist_sq < min_dist_sq)
        {
            best_cluster = idx;
            min_dist_sq = dist_sq;
        }
    }

//    if(gid == 0)
//    {
//        INFO("min_dist_sq=%f, size=%v2f", min_dist_sq, cluster_dims[best_cluster]);
//    }

    if(min_dist_sq > (cluster_dims[best_cluster].s0 + cluster_dims[best_cluster].s1))
    {
        // INFO("point idx=%d, going to untracked", gid);
        best_cluster = num_clusters;
    }
    mapped[gid] = best_cluster;
}

/*
__kernel void count_mapped_pts( const int num_points,
                                const int num_clusters,
                                __global int* mapped_pts,
                                __global int* cluster_count)
{
    for(int i=0; i<num_clusters+1; i++)
    {
        cluster_count[i] = 0;
    }

//    INFO("num clusters = %d", num_clusters)
    for(int i=0; i<num_points; i++)
    {
        int cluster_idx = mapped_pts[i];
        if(cluster_idx < 0)
        {
//            INFO("idx =%d", i);
            cluster_count[num_clusters] += 1;
        } else
        {
            cluster_count[cluster_idx] += 1;
        }
    }
}
*/

// This breaks the count into separate points
__kernel void count_mapped_pts( const    int  num_points,
                                const    int  num_clusters,
                                const    int  pts_per_histogram,
                                __global int* mapped_pts,
                                __global int* all_histograms)
{
    int gid = get_global_id(0);

    // INFO("gid=%d, lid=%d, group_id=%d", gid, get_local_id(0), get_group_id(0));

//    if(gid == 0) { INFO("elems_per_histogram = %d", elems_per_histogram); }

    int histogram_offset = gid*(num_clusters+1);
    for(int i=0; i<=num_clusters; i++)
    {
        all_histograms[histogram_offset + i] = 0;
    }

//    INFO("num clusters = %d", num_clusters)
    int offset = gid*pts_per_histogram;
    // Somehow get rid of the second check
    for(int i=0; i<pts_per_histogram && offset<num_points; i++, offset++)
    {
        int cluster_idx = mapped_pts[offset];
        if(cluster_idx < 0)
        {
//            INFO("idx =%d", i);
            all_histograms[histogram_offset + num_clusters] += 1;
        } else
        {
            all_histograms[histogram_offset + cluster_idx] += 1;
        }
    }
}

__kernel void build_histograms( const    int  num_clusters,
                                const    int  num_histograms,
                                __global int* in_histogram,
                                __global int* out_histogram)
{
    for(int j=0; j<=num_clusters; j++)
    {
        out_histogram[j] = 0;
    }

    // Sum up the histograms like this to preserve cache coherency
    int offset = 0;
    for(int i=0; i<num_histograms; i++)
    {
        for(int j=0; j<=num_clusters; j++, offset++)
        {
            out_histogram[j] += in_histogram[offset];
        }
    }

//    for(int j=0; j<=num_clusters; j++, offset++)
//    {
//        INFO("out_histogram[%d] = %d", j, out_histogram[j]);
//        Figure out how to remove a small histogram...
//    }
}


__kernel void transfer_pt_indices(  const    int  num_points,
                                    const    int  num_histogram_elems,
                                    __global int* mapped_pts,
                                    __global int* offsets,
                                    __local  int* local_offsets,
                                    __global int* out_clustered_pts)
{
    for(int i=0; i<num_histogram_elems; i++)
    {
        local_offsets[i] = offsets[i];
    }

    // NOTE: the reason this is serial is because the offsets[cluster_idx] must be updated
    // NOTE: Figure out a better way!!!
    for(int i=0; i<num_points; i++)
    {
        int cluster_idx = mapped_pts[i];
        int offset = local_offsets[cluster_idx];
        out_clustered_pts[offset] = i;
        local_offsets[cluster_idx] = offset+1;
    }
}

__kernel void copy_pt_data( __global float2* points,
                            __global int*    indices,
                            __global float2* out_points)
{
    int gid = get_global_id(0);

    int index = indices[gid];

    float2 pt = points[gid];

    out_points[index] = pt;
}

// This is brain dead simple inclusive scan with a 0 offset at the beginning
__kernel void sum_histogram(   const int num_elems,
                                __global int* counts,
                                __global int* offsets)
{
    offsets[0] = 0;
    for(int i=1; i<=num_elems; i++)
    {
        offsets[i] = offsets[i-1] + counts[i-1];
//        INFO("counts[%d] = %d, sums[%d] = %d", i, counts[i-1], i, sums[i]);
    }
}

__kernel void recalc_cluster_dims(  __global float2* points,
                                    __global int* point_idxs,
                                    __global int* offsets,
                                    __global float2* out_means,
                                    __global float2* out_sizes)
{
    int gid = get_global_id(0);
    // Calculate the cluster mean and dim
    float2 mean = 0.f;
    float2 min = +100000.0f;
    float2 max = -100000.0f;

    int begin = offsets[gid];
    int end = offsets[gid+1];

//    if(gid == 0)
//    {
//        INFO("begin=%d, end=%d", begin, end);
//    }

    for(int i=begin; i<end; i++)
    {
        int idx = point_idxs[i];
        float2 pt = points[idx];
        mean += pt;

        if(pt.s0 < min.s0) { min.s0 = pt.s0; }
        else if(pt.s0 > max.s0) { max.s0 = pt.s0; }

        if(pt.s1 < min.s1) { min.s1 = pt.s1; }
        else if(pt.s1 > max.s1) { max.s1 = pt.s1; }
    }

    mean /= (end-begin);

    min = mean - min;
    max = max - mean;
    float width = fmax(min.s0, max.s0);
    width *= width;

    float height = fmax(min.s1, max.s1);
    height *= height;

    float2 size = (float2)(width, height);

    if((end-begin) < MIN_POINTS_IN_ESTIMATE)
    {
        size = 0.0f;
    }

    out_means[gid] = mean;
    out_sizes[gid] = size;
}

kernel void prune_clusters( const int num_clusters,
                            global float2* means,
                            global float2* sizes,
                            global char* settled,
                            global int* new_num_clusters)
{
    int offset = 0;
    for(int i=0; i<num_clusters; i++)
    {
        float2 size = sizes[i];
        if(size.s0 > 0.0f && size.s1 > 0.0f)
        {
            means[offset] = means[i];
            sizes[offset] = size;
            settled[offset] = settled[offset];
            offset++;
        }
    }
    new_num_clusters[0] = offset;
}

// This is almost the same as above but
// it checks the old mean against the new mean to decide if the cluster is settled or not
__kernel void run_EM(   __global float2* points,
                        __global int* point_idxs,
                        __global int* offsets,
                        __global float2* out_means,
                        __global float2* out_sizes,
                        __global char*   out_settleds)
{
    int gid = get_global_id(0);

    float2 old_mean = out_means[gid];
    float2 old_size = out_sizes[gid];

    // Calculate the cluster mean
    float2 mean = 0.f;

    int begin = offsets[gid];
    int end = offsets[gid+1];

    for(int i=begin; i<end; i++)
    {
        int idx = point_idxs[i];
        float2 pt = points[idx];

        mean += pt;
    }

    mean /= (end-begin);

    float max_width_sq = 0.f;
    float max_height_sq = 0.f;
    for(int i=begin; i<end; i++)
    {
        int idx = point_idxs[i];
        float2 vec = mean - points[idx];
        float width_sq = vec.s0*vec.s0;
        float height_sq = vec.s1*vec.s1;

        if(max_width_sq < width_sq)
        {
            max_width_sq = width_sq;
        }
        if(max_height_sq < height_sq)
        {
            max_height_sq = height_sq;
        }
    }
    float2 size = (float2)(max_width_sq, max_height_sq);

    if(end-begin > MIN_POINTS_IN_ESTIMATE)
    {
        out_means[gid] = mean;
        out_sizes[gid] = size;

        // Test for equality
        int2 eq = isequal(old_mean, mean);
        out_settleds[gid] = all(eq);
    } else
    {
        out_means[gid] = old_mean;
        out_sizes[gid] = old_size;
        out_settleds[gid] = 1;
    }
}

// This is meant to only be run as a single task
__kernel void merge_clusters(const   int     in_num_clusters,
                            __global float2* in_cluster_means,
                            __global float2* in_cluster_sizes,
                            __global char*   in_cluster_settleds,

                            __global  int*    temp_clusters,

                            __global int*    out_num_clusters,
                            __global float2* out_cluster_means,
                            __global float2* out_cluster_sizes,
                            __global char*   out_cluster_settleds,
                            __global int*    out_num_unsettled)
{
    int gid = get_global_id(0);
    // It should only be the first workitem running this kernel
    if(gid != 0)
    {
        return;
    }

//    int temp_clusters[in_num_clusters];
    int num_temp_clusters = 0;

    // Note: this is a direct copy of Naive_Tracker.merge_clusters()
    for(int i=0; i<in_num_clusters; i++)
    {
        bool removed_i = false;

        float2 mean_i = in_cluster_means[i];
//        float2 size_i = in_cluster_sizes[i];
//        INFO("Forward merge: looking at elem %d size=%v2f", i, size_i);

        for(int j=i+1; j<in_num_clusters; j++)
        {
            float2 mean_j = in_cluster_means[j];
            float2 size_j = in_cluster_sizes[j];

            float2 diff = mean_i - mean_j;
            float delta_sq = dot(diff, diff);

            if(delta_sq < (size_j.s0 + size_j.s1))
            {
//                INFO("Forward prune: Removing cluster %d because of cluster %d", i, j);
                removed_i = true;
                break;
            }
        }

        if(!removed_i)
        {
//            INFO("Forward merge: adding idx %d", i);
            temp_clusters[num_temp_clusters] = i;
            num_temp_clusters++;
        }
    }

//    INFO("After forward merge, num clusters = %d", num_temp_clusters);

    // Merge in reverse
    int out_index = 0;
    int num_unsettled = 0;
    for(int i=num_temp_clusters-1; i>-1; i--)
    {
        bool removed_i = false;

        int i_idx = temp_clusters[i];

        float2 mean_i = in_cluster_means[i_idx];

        for(int j=i-1; j>-1; j--)
        {
            int j_idx = temp_clusters[j];
            float2 mean_j = in_cluster_means[j_idx];
            float2 size_j = in_cluster_sizes[j_idx];

            float2 diff = mean_i - mean_j;
            float delta_sq = dot(diff, diff);

            if(delta_sq < (size_j.s0 + size_j.s1))
            {
                removed_i = true;
                break;
            }
        }

        if(!removed_i)
        {
//            INFO("Reverse merge: adding idx %d", i);
            out_cluster_means[out_index] = mean_i;
            float2 size_i = in_cluster_sizes[i_idx];
            out_cluster_sizes[out_index] = size_i;
            char settled_i = in_cluster_settleds[i_idx];
            out_cluster_settleds[out_index] = settled_i;
            out_index++;

            if(settled_i == 0)
            {
                num_unsettled++;
            }
        }
    }

    *out_num_clusters = out_index;
    *out_num_unsettled = num_unsettled;
}

kernel void unsettle_clusters(  const int num_clusters,
                                global char* settled)
{
    for(int i=0; i<num_clusters; i++)
    {
        settled[i] = 0;
    }
}

__kernel void populate_untracked_points(const int left, const int right, const int bottom, const int top,
                                        const int num_clusters,
                                        __global int* offsets,
                                        __global int* in_pt_idxs,
                                        __global float2* in_points,
                                        __global float2* out_points,
                                        __global int*    out_num_valid_pts)
{
    // Copy over the untracked points
    int begin = offsets[num_clusters];
    int end = offsets[num_clusters+1];

    int out_idx = 0;
    for(int i=begin; i<end; i++)
    {
        int pt_idx = in_pt_idxs[i];
        float2 pt = in_points[pt_idx];

        if(pt.s0 < left || pt.s0 > right || pt.s1 < bottom || pt.s1 > top)
        {
            out_points[out_idx] = pt;
            out_idx++;
        }
    }

    out_num_valid_pts[0] = out_idx;
}

// Follow Knuth's random choice algorithm from
// http://rosettacode.org/wiki/Knuth%27s_algorithm_S
kernel void populate_sampled_clusters(const  int     num_in_points,
                                      global float2* in_points,
                                      const  int     num_samples,
                                      global int*    sample_idxs,
                                      global float2* out_means,
                                      global float2* out_sizes,
                                      global char*   out_settled)
{
    for(int i=0; i<num_samples; i++)
    {
        int idx = sample_idxs[i];
        out_means[i] = in_points[idx];
        out_sizes[i] = 900.0f;
        out_settled[i] = 0;
    }
}
