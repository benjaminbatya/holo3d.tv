__author__ = 'benjamin'

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse, Patch
from matplotlib.colors import LinearSegmentedColormap
import math

from  matplotlib.animation import FuncAnimation

import pyopencl as cl
import pyopencl.algorithm as cl_algo
import pyopencl.scan as cl_scan

# cl_algo.copy_if()
# cl_scan.GenericScanKernel()


RADIUS = 30.0
MAX_ITERATIONS = 20
MIN_POINTS_IN_ESTIMATE = 10

RADIUS_SQ = RADIUS ** 2

LEFT = -200
RIGHT = 200
TOP = 200
BOTTOM = -200

def maximize(points, pos, width_sq, height_sq):
    '''
    :param points:
    :param pos:
    :param within_delta:
    :return: (list of points within delta of pos)
    '''
    ret = []

    num_pts = len(points)
    for i in xrange(num_pts):
        vec = pos - points[i]
        check = vec.dot(vec)

        if check < (width_sq + height_sq):
            ret.append(i)

    return ret

def expectation(points, indices):
    '''
    :param points:
    :return: (mean of points, radius of points from mean)
    '''
    mean = np.sum(points[indices], axis=0)
    mean /= len(indices)

    max_width_sq = 0.0
    max_height_sq = 0.0
    for i in indices:
        vec = mean - points[i]
        width_sq = vec[0]*vec[0]
        height_sq = vec[1]*vec[1]

        if max_width_sq < width_sq:
            max_width_sq = width_sq

        if max_height_sq < height_sq:
            max_height_sq = height_sq

    return(mean, max_width_sq, max_height_sq)

def rand(low, high):
    return int(np.random.random() * (high - low) + low)

# Always use the same seed for testing
np.random.seed(100)

blobs = []
ellipses = []

# angle_degs = 5
# rot_angle = math.radians(angle_degs)
# rot_matrix = np.array( [[math.cos(rot_angle), -math.sin(rot_angle)],
#                         [math.sin(rot_angle),  math.cos(rot_angle)]])

data_pts = []

scat = None
ax = None

def main():
    fig = plt.figure(1)
    ax = fig.add_subplot(1,1,1)

    # Generate the blob parameters
    for i in xrange(rand(1, 10)):
        d = {'count':rand(20,200),
             'x':rand(LEFT, RIGHT), 'y':rand(BOTTOM, TOP),
             'w':rand(5,25), 'h':rand(5,25),
             'dir_x':rand(-10,10), 'dir_y':rand(-10, 10)}
        d['pos'] = np.array([ d['x'], d['y'] ])
        d['size'] = np.array([ d['w'], d['h'] ])
        d['dir'] = np.array([ d['dir_x'], d['dir_y'] ])
        blobs.append( d )
    print "Num blobs = %r" % len(blobs)

    for i in xrange(len(blobs)):
        blob = blobs[i]
        print "Blob %r = %r" % (i, blob)

        c = Ellipse(xy=blob['pos'], width=4*blob['w'], height=4*blob['h'], color='g', fill=False)
        if i == 0:
            c.set_label('Data Distribution')
        ax.add_patch(c)
        ellipses.append( c )

    # Create the scatter data
    total = 0
    for blob in blobs:
        total += blob['count']

    global data_pts, scat
    data_pts = np.zeros((total,2), dtype=np.float32)
    scat = ax.scatter(data_pts[:,0], data_pts[:,1]) #, s=5, lw=0.5, edgecolors=(0,0,0,0), facecolors='none')

    ax.set_xlim([LEFT, RIGHT])
    ax.set_ylim([BOTTOM, TOP])

    animation = FuncAnimation(fig, update, init_func=init, interval=200) #, blit=True)
    plt.show()

    ax.legend(loc='best')

init_called = False

def init():
    # print("init: called")

    global init_called
    init_called = True
    for el in ellipses:
        el.set_visible(False)

    scat.set_visible(False)

    return ellipses # , scat,

def update(i):
    # print("update: Called, i=%r" % i)

    global init_called
    if init_called:
        for el in ellipses:
            el.set_visible(True)
    init_called = False

    for i in xrange(len(blobs)):
        blob = blobs[i]
        pos = blob['pos']
        dir = blob['dir']
        size = blob['size']

        # print ("updating blob %r" % blob)
        # blob['pos'] = rot_matrix.dot(blob['pos'])
        pos += dir

        if( (dir[0] < 0 and pos[0] + 2*size[0] < LEFT) or
            (dir[0] > 0 and pos[0] - 2*size[0] > RIGHT) or
            (dir[1] < 0 and pos[1] + 2*size[1] < BOTTOM) or
            (dir[1] > 0 and pos[1] - 2*size[1] > TOP) ):

            pos = np.array([ blob['x'], blob['y'] ])

            # try to find the intersection point
            t_l = float(pos[0] + 2*size[0] - LEFT) / dir[0] if dir[0] != 0 else -1
            if t_l < 0: t_l = 10**6
            t_r = float(pos[0] - 2*size[0] - RIGHT) / dir[0] if dir[0] != 0 else -1
            if t_r < 0: t_r = 10**6
            t_b = float(pos[1] + 2*size[1] - BOTTOM) / dir[1] if dir[1] != 0 else -1
            if t_b < 0: t_b = 10**6
            t_t = float(pos[1] - 2*size[1] - TOP) / dir[1] if dir[1] != 0 else -1
            if t_t < 0: t_t = 10**6

            t = float(min(t_l, t_r, t_b, t_t))

            # Reset pos
            pos = (pos - dir * t)
            pos = pos.astype(np.int32)

        blob['pos'] = pos

        ellipses[i].center = blob['pos']

    x = []
    y = []

    for blob in blobs:
        pos = blob['pos']
        # Generate a random distribution of points for each blob
        new_x = np.random.standard_normal(blob['count']) * blob['w'] + pos[0]
        new_y = np.random.standard_normal(blob['count']) * blob['h'] + pos[1]

        x = np.hstack((x, new_x))
        y = np.hstack((y, new_y))

    scat.set_offsets([x,y])

    # objs = plt.plot(x, y, 'wo', label='Original data')

    # ret += objs

    # points = np.vstack((x,y)).T
    # num_points = points.shape[0]

    # num_samples = int(num_points * 0.1)

    # indices = np.random.choice(num_points, num_samples)
    #
    # plt.plot(x[indices], y[indices], 'bx', label='Sampled Data')

    # estimates = []
    # for i in indices:
    #     # first index is the mean of the cluster
    #     # Second index is the width squared
    #     # Third index is the height squared
    #     # Fourth index indicates if the cluster is settled or not
    #     estimates.append((points[i], RADIUS_SQ, RADIUS_SQ, False))
    #
    # estimates = cluster_points(estimates, points)
    #
    # idx = 0
    # for (mean, width_sq, height_sq, settled) in estimates:
    #     width = math.sqrt(width_sq)
    #     height = math.sqrt(height_sq)
    #     print "Estimate %r: mean=%r, width=%r, height=%r" % ( idx, mean, width, height )
    #     c = Ellipse(xy=mean, width=2*width, height=2*height, color='r', fill=False)
    #     if idx == 0:
    #         c.set_label("Estimate")
    #     ax.add_patch(c)
    #     idx += 1
    #

    return ellipses #, scat,

def cluster_points(estimates, points):

    for i in xrange(MAX_ITERATIONS):
        print "iteration %r, num estimates = %r" % (i, len(estimates))

        num_unsettled = 0

        # Run EM and check for settled estimates
        new_estimates = []
        for estimate in estimates:
            # If the estimate is settled already, don't run EM on it again
            if estimate[3] is True:
                new_estimates.append(estimate)
                continue

            # Try different sizes for maximization
            indices = maximize(points, estimate[0], estimate[1], estimate[2]) # RADIUS_SQ, RADIUS_SQ) # estimate[1]*1.1, estimate[2]*1.1) #

            # Only save the estimate if it contains more then the required minimum number of points
            if len(indices) > MIN_POINTS_IN_ESTIMATE:
                (mean, width_sq, height_sq) = expectation(points, indices)

                if np.allclose(mean, estimate[0]): # and np.allclose(width_sq, estimate[1]) and np.allclose(height_sq, estimate[2]):
                    # Mark the estimate as settled
                    new_estimates.append((mean, width_sq, height_sq, True))
                    # print "Settled: mean=%r, radius=%r" % ( mean, math.sqrt(radius_sq) )

                else:
                    # The estimate is still unsettled
                    new_estimates.append((mean, width_sq, height_sq, False))
                    num_unsettled += 1
                    # print "New estimate: mean=%r, radius=%r" % ( mean, math.sqrt(radius_sq) )

        estimates = new_estimates

        # print "After EM: number of unsettled estimates = %r" %  num_unsettled

        # ax = fig.add_subplot(1,1,1)
        #
        # for (mean, radius_sq) in estimates:
        #     c = plt.Circle(mean, radius=math.sqrt(radius_sq), color=colors[i], fill=False)
        #     ax.add_patch(c)

        # merge together estimates if their means are closer then the greater radius
        num_unsettled = 0
        new_estimates = []
        for est_i in xrange(len(estimates)):

            (mean_i, width_sq_i, height_sq_i, settled_i) = estimates[est_i]
            removed_i = False

            est_j = est_i+1
            while est_j < len(estimates):
                (mean_j, width_sq_j, height_sq_j, settled_j) = estimates[est_j]

                diff_vec = mean_i - mean_j
                delta_sq = diff_vec.dot(diff_vec)

                # if(i >= 4) and (est_i==0):
                #     pass

                if delta_sq < (width_sq_j + height_sq_j):
                    # print "Removing duplicate estimate %i (mean=%r, radius=%r) because of estimate %r" % \
                    #       (est_i, mean_i, math.sqrt(radius_sq_i), est_j)
                    removed_i = True
                    break

                est_j += 1

            if not removed_i:
                # print "Saving estimate %r (mean=%r, radius=%r)" %(est_i, mean_i, math.sqrt(radius_sq_i))
                new_estimates.append((mean_i, width_sq_i, height_sq_i, settled_i))
                if not settled_i:
                    num_unsettled += 1

        estimates = new_estimates

        # print "After pruning, num unsettled estimates = %r" % num_unsettled

        # merge in reverse
        num_unsettled = 0
        new_estimates = []
        for est_i in xrange(len(estimates)-1, -1, -1):

            (mean_i, width_sq_i, height_sq_i, settled_i) = estimates[est_i]
            removed_i = False

            est_j = est_i - 1
            while est_j > -1:
                (mean_j, width_sq_j, height_sq_j, settled_j) = estimates[est_j]

                diff_vec = mean_i - mean_j
                delta_sq = diff_vec.dot(diff_vec)

                # if(i >= 4) and (est_i==0):
                #     pass

                if delta_sq < (width_sq_j + height_sq_j):
                    # print "Removing duplicate estimate %i (mean=%r, radius=%r) because of estimate %r" % \
                    #       (est_i, mean_i, math.sqrt(radius_sq_i), est_j)
                    removed_i = True
                    break

                est_j -= 1

            if not removed_i:
                # print "Saving estimate %r (mean=%r, radius=%r)" %(est_i, mean_i, math.sqrt(radius_sq_i))
                new_estimates.append((mean_i, width_sq_i, height_sq_i, settled_i))
                if not settled_i:
                    num_unsettled += 1

        estimates = new_estimates

        # print "After pruning, num unsettled estimates = %r" % num_unsettled

        # Break if there are no more unsettled groups
        if num_unsettled == 0:
            break

    print "Finished iterating, num estimates = %r" % len(estimates)

    return estimates

if __name__ == "__main__":
    main()
