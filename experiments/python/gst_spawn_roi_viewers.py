#!/usr/bin/python

import sys
import subprocess
import signal
import os

sub_processes = []
def handler(signum, frame):
    print("Received signal %r. Quitting!" % signum)
    for pro in sub_processes:
        os.kill(pro.pid, signal.SIGINT)

def main():
    global sub_processes

    num_rois = 4
    if len(sys.argv) > 1:
        num_rois = int(sys.argv[1])

    port_start = 5000
    if len(sys.argv) > 2:
        port_start = int(sys.argv)

    CMD = """
        gst-launch-1.0 -v udpsrc port={PORT} caps="application/x-rtp, media=(string)video,
        clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96" ! rtph264depay
        ! avdec_h264 ! fpsdisplaysink
    """.split()
    CMD = " ".join(CMD)

    # start up all of the processes
    for i in xrange(num_rois):
        port = port_start+i
        cmd_str = CMD.format(PORT=port)
        print("exec'ing \"%s\"" % cmd_str)
        pro = subprocess.Popen("exec " + cmd_str, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
        sub_processes.append(pro)

    # Register our signal handlers
    signal.signal(signal.SIGINT, handler)
    signal.signal(signal.SIGQUIT, handler)

    completed = []

    while True:
        # wait for all threads to complete
        for pro in sub_processes:
            if pro in completed:
                continue

            out = pro.stdout.readline()
            if out == '' and pro.poll() != None:    # Check if the process is done or not
                print("Process %r has finished. returncode=%r" %(pro.pid, pro.returncode))
                completed.append(pro)
            elif out != '':
                if out.find('fpsdisplaysink0') == -1:
                    sys.stdout.write(out)
                    sys.stdout.flush()

        if len(completed) == len(sub_processes):
            break

    print("All rois are closed! quitting!")

if __name__ == "__main__":
    main()
