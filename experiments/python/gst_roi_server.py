#!/usr/bin/python

import sys
import subprocess
import signal
import os
import math
import random

cmd_process = None
def handler(signum, frame):
    # print("signum=%r, frame=%r" % (signum, frame))
    os.killpg(os.getpgid(cmd_process.pid), signal.SIGINT)

def main():
    global cmd_process

    num_rois = 4
    if len(sys.argv) > 1:
        num_rois = int(sys.argv[1])

    port_start = 5000
    if len(sys.argv) > 2:
        port_start = int(sys.argv)

    NUM_X = int(math.ceil(math.sqrt(num_rois)))
    NUM_Y = int((num_rois-1)/NUM_X) + 1

    WIDTH = 1280
    HEIGHT = 720

    ROI_WIDTH = 256
    ROI_HEIGHT = 256

    X_OFFSET = (WIDTH-ROI_WIDTH)/NUM_X
    Y_OFFSET = (HEIGHT-ROI_HEIGHT)/NUM_Y

    SERVER_PREFIX = """
        gst-launch-1.0 -v v4l2src device=/dev/video0 do-timestamp=true !
        video/x-raw,width={WIDTH},height={HEIGHT},framerate=49/1 ! videoflip method=vertical-flip ! tee name=s
    """.split()
    SERVER_PREFIX = " ".join(SERVER_PREFIX).format(WIDTH=WIDTH, HEIGHT=HEIGHT)

    # print(SERVER_PREFIX)

    ROI_STR = """
        s. ! queue ! videocrop top={TOP} bottom={BOTTOM} left={LEFT} right={RIGHT} !
        omxh264enc ! rtph264pay config-interval=1 pt=96 ! udpsink host=192.168.10.1 port={PORT} sync=false
    """.split()
    ROI_STR = " ".join(ROI_STR)

    TOP = 0
    LEFT = 0

    cmd_str = SERVER_PREFIX
    for i in xrange(num_rois):

        # Jitter left just a little
        left = int(random.uniform(-10, 10)) + LEFT
        if left < 0: left = 0
        elif left >= WIDTH-ROI_WIDTH: left = WIDTH-ROI_WIDTH
        right = WIDTH - ROI_WIDTH - left

        # Jitter top just a little
        top = int(random.uniform(-10, 10)) + TOP
        if top < 0: top = 0
        elif top >= HEIGHT-ROI_HEIGHT: top = HEIGHT-ROI_HEIGHT
        bot = HEIGHT - ROI_HEIGHT - top

        cmd_str += " " + ROI_STR.format(PORT=port_start+i, TOP=top, BOTTOM=bot, LEFT=left, RIGHT=right) + " "
        LEFT += X_OFFSET
        if LEFT + ROI_WIDTH >= WIDTH:
            LEFT = 0
            TOP += Y_OFFSET

    # cmd_str = "ping server"

    signal.signal(signal.SIGINT, handler)
    signal.signal(signal.SIGQUIT, handler)
    signal.signal(signal.SIGTERM, handler)

    print("executing \"%s\"" % cmd_str)

    # Run the subprocess
    # example from http://www.cyberciti.biz/faq/python-execute-unix-linux-command-examples/
    global cmd_process
    cmd_process = subprocess.Popen(cmd_str, shell=True,
                                   stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                   preexec_fn=os.setsid)
    while True:
        out = cmd_process.stdout.readline()
        if out == '' and cmd_process.poll() != None:
            break
        if out != '':
            sys.stderr.write(out)
            sys.stderr.flush()

if __name__ == "__main__":
    main()

