# -*- coding: utf-8 -*-
"""
Created on Tue Jan 27 23:15:06 2015

@author: benjamin
"""

import pyopencl as cl

platforms = cl.get_platforms()

print "\nNumber of platforms = ", len(platforms)

print "\n---------------------------"

for p in platforms:
    print "Name: ", p.name
    print "Vendor: ", p.vendor
    print "Version: ", p.version
   
    devices = p.get_devices()
    print "Number of devices: ", len(devices)
    
    for d in devices:
        print "\t------------------"
        print "\t\tName: ", d.name
        print "\t\tVersion: ", d.version
        print "\t\tMax. Compute Units: ", d.max_compute_units
        print "\t\tLocal Memory Size: ", d.local_mem_size/1024, "KB"
        print "\t\tGlobal Memory Size: ", d.global_mem_size/(1024*1024), "MB"
        print "\t\tMax Alloc Size: ", d.max_mem_alloc_size/(1024*1024), "MB"
        print "\t\tMax Work-Group Total Size: ", d.max_work_group_size
        
        dim = d.max_work_item_sizes
        print "\t\tMax Work-group Dims: (", dim[0], " ".join(map(str, dim[1:])), ")"
    
        print "\t--------------------------------"
        
    print "\n--------------------------------"
    
    
    

     
    