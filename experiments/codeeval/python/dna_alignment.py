__author__ = 'benjamin'

import sys
import unittest

class Unit_Test(unittest.TestCase):
    def test_simple(self):
        input = """
            GAAAAAAT | GAAT
        """
        # input = """
        #     GAAAAAAT | GAA----T
        # """
        # input = """
        #     GAAAAAAT | G--A-A-T
        # """
        tests = input.split('\n')
        process(tests)

GAP = '-'

def score_seqs(seq1, seq2):
    index = 0
    score = 0
    is_gap_start_1 = True
    is_gap_start_2 = True

    assert len(seq1) == len(seq2)

    while index < len(seq1):
        if seq1[index] != GAP:
            if seq1[index] == seq2[index]:
                score += 3
                is_gap_start_2 = True
            elif seq2[index] != GAP:
                score -= 3
                is_gap_start_2 = True
            elif is_gap_start_2:
                score -= 8
                is_gap_start_2 = False
            else:
                score -= 1
            is_gap_start_1 = True

        elif is_gap_start_1:
            score -= 8
            is_gap_start_1 = False
        else:
            score -= 1

        index += 1

    return score

def num_matched_bases(seq1, seq2):
    index = 0
    score = 0

    assert len(seq1) == len(seq2)

    while index < len(seq1):
        if seq1[index] != GAP:
            if seq1[index] == seq2[index]:
                score += 1

        index += 1

    return score

def process(input):
    for test in input:
        stripped = test.strip()
        args = stripped.split(' ')
        if len(args) < 1: continue
        if not args[0]: continue

        seq_1 = args[0]
        seq_2 = args[2]

        prev_score = -(sys.maxint-1)

        # print(seq_1, seq_2)

        # add enough GAPs to either sequence so they are the same length

        while len(seq_1) < len(seq_2):
            seq_1 += GAP

        while len(seq_2) < len(seq_1):
            seq_2 += GAP

        print(seq_1, seq_2)

        print num_matched_bases(seq_1, seq_2)

        new_score = score_seqs(seq_1, seq_2)
        print new_score


        # Try to add gaps to seq_2 and test score.
        # If score goes up, save the new sequence and add another gap
        # if it goes down, ignore the new sequence and


def main():
    test_cases = open(sys.argv[1], 'r')

    process(test_cases)

    test_cases.close()

if __name__ == "__main__":
    main()

