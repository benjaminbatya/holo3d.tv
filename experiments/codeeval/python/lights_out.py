__author__ = 'benjamin'

import sys
import unittest

class Unit_Test(unittest.TestCase):
    def test_simple(self):
        # input = """
        #     4 10 ...OOOOOOO|.OO.O.O...|.OO..OO.OO|...O....O.
        #     3 3 ..O|OOO|OOO
        #     5 7 .O.O...|..O.O..|.O.O..O|.O..OOO|OO.OOOO
        # """
        # input = """
        #     3 3 ..O|OOO|OOO
        # """
        # input = """
        #     3 4 ....|.O..|....
        # """
        input = """
            5 7 .O.O...|..O.O..|.O.O..O|.O..OOO|OO.OOOO
        """
        # input = """
        #     3 3 OOO|.O.|OOO
        # """
        # input = """
        #     3 3 ...|O.O|...
        # """
        tests = input.split('\n')
        process(tests)

def array_to_str(array):
    if not array: return None

    out_str = ''
    sep = ''
    for r in array:
        out_str += sep + ''.join(['O' if a else '.' for a in r])
        sep = '|'
    return out_str

def str_to_array(in_str):
    if not in_str: return None

    rows = in_str.split('|')
    array = []
    for r in rows:
        array.append([True if a=='O' else False for a in r])
    return array

class Pattern(object):
    def __init__(self, width, height):
        self.distance = sys.maxint
        self.width = width
        self.height = height
        self._str = None
        self._array = None
        self.prev = None

    def clone(self):
        import copy
        p = Pattern(self.width, self.height)
        p.set_array(copy.deepcopy(self.array()))
        p.distance = self.distance

        return p

    def __eq__(self, other):
        return self.str() == other.str()

    def set_str(self, in_str):
        self._str = in_str
        self._array = None

    def str(self):
        if not self._str:
            self._str = array_to_str(self._array)
        return self._str

    def set_array(self, arr):
        self._array = arr
        self._str = None

    def array(self):
        if not self._array:
            self._array = str_to_array(self._str)
        return self._array

    def clear(self):
        self._array = [[False for x in xrange(self.width)] for y in xrange(self.height)]
        self._str = None

    def __str__(self):
        str_arr = '\n'.join(self.str().split('|'))
        return "Pattern: \n" + str_arr

    def _toggle_cell(self, x, y):
        if 0<=x and x < self.width:
            if 0<=y and y < self.height:
                self._array[y][x] = not self._array[y][x]

    def toggle(self, x, y):
        # Create a new pattern that is transformed
        p = self.clone()
        ta = p.array()

        # Toggle each of the spots
        p._toggle_cell(x, y)
        p._toggle_cell(x, y-1)
        p._toggle_cell(x, y+1)
        p._toggle_cell(x-1, y)
        p._toggle_cell(x+1, y)

        # clear the str field so it will be lazily regenerated later
        p._str = None

        return p


def calc_distance(goal):
    # These are the memoized patterns
    memos = {}

    # This is the breath-first queue
    queue = []

    start = Pattern(goal.width, goal.height)
    start.clear()
    start.distance = 0
    memos[start.str()] = start
    queue.append(start)

    # goal.distance = 0
    # memos[goal.str()] = goal
    # queue.append(goal)

    # count = 0

    while queue:
        pattern = queue.pop(0)
        # count += 1
        # calculate each possible
        for x in xrange(pattern.width):
            for y in xrange(pattern.height):
                new_p = pattern.toggle(x, y)

                new_p.distance = pattern.distance + 1

                new_p.prev = pattern

                # print str(new_p)

                # Check if this element has already been memoized
                if new_p == goal:
                    # print count
                    return new_p

                if not memos.has_key(new_p.str()):
                    memos[new_p.str()] = new_p
                    # Add it to the queue
                    queue.append(new_p)

    # print count
    return -1


def process(input):
    for test in input:
        stripped = test.strip()
        args = stripped.split(' ')
        if len(args) < 1: continue
        if not args[0]: continue

        rows = int(args[0])
        cols = int(args[1])

        goal = Pattern(cols, rows)
        goal.set_str(args[2])

        # print goal

        result = calc_distance(goal)

        # print("Num toggles=" + str(result.distance) + ", order:")
        # while(result):
        #     print result
        #     result = result.prev
        print(result.distance)

def main():
    test_cases = open(sys.argv[1], 'r')

    process(test_cases)

    test_cases.close()

if __name__ == "__main__":
    main()
