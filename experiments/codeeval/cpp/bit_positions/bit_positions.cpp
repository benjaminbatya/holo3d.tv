// Sample code to read in test cases:
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <assert.h>

using namespace std;

typedef std::vector<std::string> StringVec;

// From http://stackoverflow.com/questions/236129/split-a-string-in-c/237280#237280
StringVec &split(const std::string &s, char delim, StringVec &elems) 
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) 
    {
        elems.push_back(item);
    }
    return elems;
}

int main(int argc, char *argv[]) {
    ifstream stream(argv[1]);
    string line;
    while (getline(stream, line)) {
        // Do something with the line
        StringVec elems;
        split(line, ',', elems);

        assert(elems.size() == 3);

        int num = stoi(elems[0]);
        int pos1 = stoi(elems[1]);
        int pos2 = stoi(elems[2]);

        pos1 -= 1;
        pos2 -= 1;

        int val1 = (num & (1<<pos1)) >> pos1;
        int val2 = (num & (1<<pos2)) >> pos2;

        if (val1 == val2) 
        {
            cout << "true" << endl;
        } else
        {
            cout << "false" << endl;
        }
    }
    return 0;
}

