#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <assert.h>
#include <queue>
#include <memory>
#include <map>

using namespace std;

typedef std::vector<std::string> StringVec;

// From http://stackoverflow.com/questions/236129/split-a-string-in-c/237280#237280
StringVec &split(const std::string &s, char delim, StringVec &elems) 
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) 
    {
        elems.push_back(item);
    }
    return elems;
}

struct Node
{
    Node(const char* str1, const char* str2);
    ~Node();

    const char* str1 = nullptr;
    const char* str2 = nullptr;

//  typedef std::shared_ptr<Node> ptr_t;
    typedef Node* ptr_t;
    typedef vector<ptr_t> vec_t;

    static ptr_t make_ptr(const char* a, const char* b);

    vec_t children;

    ostream& print(ostream& os, size_t depth=0);

    void add_letters(const char*, const char*);
    void add_letter(const char*, const char*);

    vec_t find_longest_sequence();
};

Node::Node(const char* str1, const char* str2)
: str1(str1)
, str2(str2)
{
    children.clear();
}

Node::~Node()
{
}

Node::ptr_t Node::make_ptr(const char* a, const char* b)
{
//  return make_shared<Node>(a, b);
    return new Node(a, b);
}

ostream& Node::print(ostream& os, size_t depth)
{
    os << std::string(depth*2, ' ') << "Node: A=" << *str1 << ", B=" << *str2 << endl;
    for (auto& c : children) 
    {
        c->print(os, depth+1);
    }

    return os;
}

void Node::add_letters(const char* str1, const char* str2)
{
    for (const char* a=str1; *a!=0; a++) 
    {
        for (const char* b=str2; *b!=0; b++) 
        {
            if (*a == *b) 
            {
                this->add_letter(a, b);
            }
        }
    }
}

void Node::add_letter(const char* a, const char* b)
{
    bool unique_letter = true;
    // Check if *a is already a child of parent node
    for (auto& child : children) 
    {
        if (*(child->str1) == *a) 
        {
            unique_letter = false;
            break;
        }
    }

    if (unique_letter) 
    {
        ptr_t child = Node::make_ptr(a, b);
        children.emplace_back(child);
    }
}

Node::vec_t Node::find_longest_sequence()
{
    vec_t longest_seq = {};

    for (auto& child : children) 
    {
        vec_t child_seq = child->find_longest_sequence();
        if (child_seq.size() > longest_seq.size()) 
        {
            longest_seq = child_seq;
        }
    }

    longest_seq.insert(longest_seq.begin(), this);
    return longest_seq;
}

void process_line(const char* str1, const char* str2)
{
    std::queue<Node::ptr_t> seq_queue;

    Node::ptr_t root = Node::make_ptr(nullptr, nullptr);

    root->add_letters(str1, str2);

    for (Node::ptr_t& c: root->children) 
    {
        seq_queue.push(c);
    }

    while (!seq_queue.empty())
    {
        Node::ptr_t parent_node = seq_queue.front();
        seq_queue.pop();

        parent_node->add_letters(parent_node->str1+1, parent_node->str2+1);
        for (Node::ptr_t& c: parent_node->children) 
        {
            seq_queue.push(c);
        }
    }

//  for (Node::ptr_t& c: root->children)
//  {
//      c->print(cout);
//  }

    Node::vec_t longest_seq = root->find_longest_sequence();

    for (int i=1; i<longest_seq.size(); i++) 
    {
        const char* str = longest_seq[i]->str1;
//      string str = longest_seq[i]->str1;
        cout << *str;
    }
    cout << endl;
}

int main(int argc, char *argv[]) 
{
    ifstream stream(argv[1]);
    string line;
    while (getline(stream, line)) 
    {
//      cout << "Line: " << line << endl;
        // Do something with the line
        StringVec elems;
        split(line, ';', elems);

        assert(elems.size() == 2);

        string seq1 = elems[0];
        string seq2 = elems[1];

        const char* str1 = seq1.c_str();
        const char* str2 = seq2.c_str();

        process_line(str1, str2);
    }

    return 0;
}
