#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <assert.h>
#include <queue>
//#include <memory>
//#include <map>
#include <algorithm>
#include <cstdlib>

using namespace std;

// From http://stackoverflow.com/questions/236129/split-a-string-in-c/237280#237280
typedef std::vector<std::string> StringVec;
StringVec &split(const std::string &s, char delim, StringVec &elems) 
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) 
    {
        elems.push_back(item);
    }
    return elems;
}

// From http://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
string trim(const string& s)
{
    auto space_fn = [](int c) { return isspace(c); };
    auto wsfront = std::find_if_not(s.begin(), s.end(), space_fn );
    return string(wsfront, std::find_if_not(s.rbegin(), string::const_reverse_iterator(wsfront), space_fn).base());
}

const char GAP = '-';

int calc_score(const string& A, const string& B) 
{
    int score = 0;
    bool is_gap_startA = true, is_gap_startB = true;

    assert(A.size() == B.size());

    for (size_t i = 0; i < A.size(); i++) 
    {
        if (A[i] != GAP) 
        {
            if (A[i] == B[i]) 
            {
                score += 3;
                is_gap_startB = true;
            } 
            else if (B[i] != GAP) 
            {
                score -= 3;
                is_gap_startB = true;
            }
            else if (is_gap_startB) 
            {
                score -= 8;
                is_gap_startB = false;
            }
            else
            {
                score -= 1;
            }
            is_gap_startA = true;
        }
        else if (is_gap_startA) 
        {
            score -= 8;
            is_gap_startA = false;
        }
        else
        {
            score -= 1;
        }
    }

    return score;
}

typedef vector<int> pattern_t;
struct Pattern
{
    pattern_t elems;
    int last = 0;

    Pattern(int N);
};

Pattern::Pattern(int N)
: elems(N, -1)
, last(-1)
{
}

ostream& operator<<(ostream& os, const Pattern& p)
{
    cout << "Pattern: last=" << p.last << " [";
    string sep = "";
    for (auto& i : p.elems) 
    {
        cout << sep << i;
        sep = ", ";
    }
    cout << "]";

    return os;
}

typedef vector<Pattern> pattern_vec_t;
pattern_vec_t gen_patterns(const int N, const int K)
{
    queue<Pattern> p_queue;
    pattern_vec_t output_vec;

    Pattern new_p(N);
    p_queue.push(new_p);

    while (!p_queue.empty()) 
    {
        Pattern p = p_queue.front();
        p_queue.pop();

        int min_value = 0;
        if (p.last >= 0) 
        {
            min_value = p.elems[p.last]; 
        }

        int new_last = p.last+1;

        for (int i=min_value; i<K; i++) 
        {
            Pattern new_p(N);
            new_p.elems = p.elems;
            new_p.last = new_last;
            new_p.elems[new_last] = i;

            if (new_last < (N-1)) 
            {
                p_queue.push(new_p);
            } else
            {
                output_vec.push_back(new_p);
            }
        }
    }

    return output_vec;
}

void process_line(string str1, string str2)
{
    str1 = trim(str1);
    str2 = trim(str2);
    
    if (str1.size() == str2.size()) 
    {
//      cout << "equal length strings: score=" << calc_score(str1, str2) << endl;
        cout << calc_score(str1, str2) << endl;
        return;
    }

    // Figure out the longer string
    string longer = str1;
    string shorter = str2;
    if (str2.size() > str1.size()) 
    {
        longer = str2;
        shorter = str1;
    }

//  cout << "|" << longer << "|" << shorter << "|" << endl;

    int N = longer.size() - shorter.size();

    int K = shorter.size() + 1;

    cout << "N=" << N << ", K=" << K << endl;

    // Generate all possible versions of shorter with len_diff GAPs inserted
    pattern_vec_t patterns = gen_patterns(N, K);

    cout << patterns.size() << endl;

    // Calculate where to place the GAPs in the shorter string
    int highest_score = INT32_MIN;
    for (auto& p: patterns) 
    {
//      cout << p << endl;
        pattern_t gaps = p.elems;

        string new_string;
        int last = 0;
        for (int idx : gaps) 
        {
            for (int j=last; j<idx; j++) 
            {
                new_string += shorter[j];
            }
            new_string += GAP;
            last = idx;
        }
        for (int j=last; j<shorter.size(); j++) 
        {
            new_string += shorter[j];
        }

        int score = calc_score(longer, new_string);
        if (score > highest_score) 
        {
            highest_score = score;
        }
//      cout << new_string << ", score=" << calc_score(longer, new_string) << endl;
    }

    cout << highest_score << endl;
}

int main(int argc, char *argv[]) 
{
    ifstream stream(argv[1]);
    string line;
    while (getline(stream, line)) 
    {
//      cout << "Line: " << line << endl;
        // Do something with the line
        StringVec elems;
        split(line, '|', elems);

        if (elems.size() != 2) 
        {
            continue;
        }

        string seq1 = elems[0];
        string seq2 = elems[1];

        process_line(seq1, seq2);
    }

    return 0;
}
