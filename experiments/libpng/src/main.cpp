#include <png.h>
#include <cstdint>
#include <assert.h>
#include <iostream>

typedef uint16_t DepthPixel;

size_t WIDTH = 640;
size_t HEIGHT = 480;

const char* FILE_NAME = "test.png";

// buffer to write to
size_t MAX_SIZE = WIDTH*HEIGHT*2*2;

struct Io_Data
{
    uint8_t* curr_buff_ptr = nullptr;
    size_t bytes_transferred = 0;
};

void write_data(png_structp png_ptr, png_bytep data, png_size_t length)
{
    Io_Data* io_data = (Io_Data*)png_get_io_ptr(png_ptr);

    assert(io_data->bytes_transferred + length <= MAX_SIZE);

    memcpy(io_data->curr_buff_ptr, data, length);

    io_data->curr_buff_ptr += length;
    io_data->bytes_transferred += length;
}

void flush_data(png_structp png_ptr)
{
    // Don't do anything...
}

void read_data(png_structp png_ptr, png_bytep data, png_size_t length)
{
    // std::cout << "read_data: length = " << length << std::endl;

    Io_Data* io_data = (Io_Data*)png_get_io_ptr(png_ptr);

    memcpy(data, io_data->curr_buff_ptr, length);

    io_data->curr_buff_ptr += length;
    io_data->bytes_transferred += length;
}

int main(int argc, char** argv)
{
    // Generate a image in Depth_Pixel format
    DepthPixel* buffer = new DepthPixel[WIDTH*HEIGHT];
    assert(buffer);

    DepthPixel* curr_pos = buffer;
    for (size_t y=0; y<HEIGHT; y++)
    {
        for (size_t x=0; x<WIDTH; x++, curr_pos++)
        {
            size_t val = (x+y) * 0xff / (WIDTH+HEIGHT);
            *curr_pos = (DepthPixel)val;
        }
    }

    // save to a buffer in png format
    uint8_t* io_buffer = new uint8_t[MAX_SIZE];
    assert(io_buffer);

    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    assert(png_ptr);

    png_infop info_ptr = png_create_info_struct(png_ptr);
    assert(info_ptr);

    // Setup the write functions
    Io_Data io_data;
    io_data.curr_buff_ptr = io_buffer;

    png_set_write_fn(png_ptr, &io_data, write_data, flush_data);

    // This is where libpng gotos to if theres a failure...
    if (setjmp(png_jmpbuf(png_ptr))) { assert(false && "Error during writing header"); }

    png_set_IHDR(png_ptr, info_ptr, WIDTH, HEIGHT,
                 16, PNG_COLOR_TYPE_GRAY, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

    png_write_info(png_ptr, info_ptr);


    /* write bytes */
    if (setjmp(png_jmpbuf(png_ptr))) { assert(false && "Error during writing bytes"); }

    // Setup the row_pointers
    png_bytep* row_pointers = (png_bytep*) new png_bytep[HEIGHT];
    for (size_t y=0; y<HEIGHT; y++)
    {
        row_pointers[y] = (uint8_t*)(buffer + y*WIDTH);
    }

    png_write_image(png_ptr, row_pointers); 

    /* end write */
    if (setjmp(png_jmpbuf(png_ptr))){ assert(false && "Error during end of write"); }

    png_write_end(png_ptr, nullptr);


    // clean up 
    png_destroy_write_struct(&png_ptr, &info_ptr);
    png_ptr = nullptr;
    info_ptr = nullptr;

    // write buffer to file
    FILE* file = fopen(FILE_NAME, "wb");
    assert(file);

    size_t bytes_transferred = 
        fwrite(io_buffer, sizeof(uint8_t), io_data.bytes_transferred, file);
    assert(bytes_transferred == io_data.bytes_transferred);

    fclose(file);
    file = nullptr;

    std::cout << "Done Writing!!" << std::endl;

    // Read in the file
    file = fopen(FILE_NAME, "rb");
    assert(file);

    size_t bytes_read =
        fread(io_buffer, sizeof(uint8_t), io_data.bytes_transferred, file); // We are cheating reading everything in like this...
    assert(bytes_read == io_data.bytes_transferred);

    fclose(file);
    file = nullptr;

    assert(0 == png_sig_cmp(io_buffer, 0, 8));

    // Uncompress the io_buffer 
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    assert(png_ptr);

    info_ptr = png_create_info_struct(png_ptr);
    assert(info_ptr);

    if (setjmp(png_jmpbuf(png_ptr))) { assert(0 && "Error during initization"); }

    io_data.curr_buff_ptr = io_buffer + 8;
    io_data.bytes_transferred = 8;
    png_set_read_fn(png_ptr, &io_data, read_data);    

    png_set_sig_bytes(png_ptr, 8);
    png_read_info(png_ptr, info_ptr);

    size_t width = png_get_image_width(png_ptr, info_ptr);
    assert(width == WIDTH);
    size_t height = png_get_image_height(png_ptr, info_ptr);
    assert(height == HEIGHT);
    png_byte color_type = png_get_color_type(png_ptr, info_ptr);
    assert(color_type == PNG_COLOR_TYPE_GRAY);
    png_byte bit_depth = png_get_bit_depth(png_ptr, info_ptr);
    assert(bit_depth == 16);
    int number_of_passes = png_set_interlace_handling(png_ptr);
    assert(number_of_passes == 1);
    png_read_update_info(png_ptr, info_ptr);

    if (setjmp(png_jmpbuf(png_ptr))) { assert(0 && "Error during read"); }

    DepthPixel* input_buffer = new DepthPixel[WIDTH*HEIGHT];
    assert(input_buffer);
    for (size_t y=0; y<HEIGHT; y++)
    {
        row_pointers[y] = (uint8_t*)(input_buffer + y*WIDTH);
    }
    png_read_image(png_ptr, row_pointers);

    // clean up 
    png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);
    png_ptr = nullptr;
    info_ptr = nullptr;

    std::cout << "Done reading!" << std::endl;

    // Compare buffer and input_buffer
    size_t num_failures = 0;
    for (size_t i=0; i<WIDTH*HEIGHT; i++)
    {
        if(buffer[i] != input_buffer[i])
        {
            std::cout << "buffers to not match at i=" << i <<
                ", buffer[i] = " << (size_t)buffer[i] << 
                ", input_buffer[i] = " << (size_t)input_buffer[i] << std::endl;
            num_failures++;

            if (num_failures >= 100)
            {
                break;
            }
        }
    }

    delete [] input_buffer;
    delete [] buffer;

    if (num_failures == 0)
    {
        std::cout << "Successfully read and compared the buffers!!" << std::endl;
    }
}
