#include <stdio.h>

double fib(int num)
{
   if (num <= 1)
       return 1;
   else
       return fib(num-1) + fib(num-2);
 }
 int main(void)
{
    printf("%.0f\n", fib(45));
    return 0;
 }