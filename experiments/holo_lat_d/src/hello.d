// Round floating point numbers
//import std.algorithm,
//    std.conv,
//    std.functional,
//    std.math,
//    std.regex,
//    std.stdio;
//
//alias round = pipe!(to!real, std.math.round, to!string);
//
//static reFloatingPoint = ctRegex!`[0-9]+\.[0-9]+`;
//
//void main(string[] args)
//{
//    if (args.length > 1)
//    {
//        args[1..$].map!round.joiner(" ").writeln;
//    } else
//    {
//        stdin.byLine(KeepTerminator.yes)
//            .map!(l => l.replaceAll!(c => c.hit.round)(reFloatingPoint))
//            . copy(stdout.lockingTextWriter());
//    }
//}
//
import std.stdio;

void main()
{
    writeln("Hello World!");
}


