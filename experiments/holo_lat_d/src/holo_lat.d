import std.stdio,
    std.socket,
    std.string,
    std.conv,
    std.random,
    std.outbuffer,
    std.getopt,
    std.array,
    std.datetime;

int main(string[] args)
{
    bool server = false;
    bool client = false;
    string addr = "localhost";
    ushort port = 5555;
    string complete_addr = addr ~ ":" ~ to!string(port);
    ushort num_messages = 100;
    ushort message_size = 16;

    auto help_info = getopt(
        args,
        "s", "This is the server option", &server,
        "c", "This is the client option", &client,
        "addr|a", "This is the address", &complete_addr,
        "num_messages|n", "This is the num_messages", &num_messages,
        "messages_size|m", "This is the message size", &message_size
    );

    if(help_info.helpWanted)
    {
        defaultGetoptPrinter("Usage:", help_info.options);
        return 0;
    }

    string[] addr_parts = split(complete_addr, ":");
    addr = addr_parts[0];
    if(addr_parts.length > 1)
    {
        string port_str = addr_parts[1];
        port = to!ushort(port_str); 
    }

    auto ip_addr = new InternetAddress(addr, port);

    if(server)
    {
        run_server(ip_addr);
    } else if(client)
    {
        run_client(ip_addr, num_messages, message_size);
    } else
    {
        defaultGetoptPrinter("Usage:", help_info.options);
        return -1;
    }

    return 0;
}

void run_server(InternetAddress addr)
{
    writefln("run_server: addr=%s", addr);

    auto s = new UdpSocket();

    s.bind(addr);

    Address remote_addr;

    ubyte[] recv_buf = uninitializedArray!(ubyte[])(ushort.max);

    while(true)
    {
        auto bytes_read = s.receiveFrom(recv_buf, remote_addr);

        recv_buf.length = bytes_read;
//      writefln("Received %s \n", recv_buf);

        s.sendTo(recv_buf, remote_addr);
    }

//  writeln("sent");
}

void run_client(InternetAddress addr, ushort num_messages, ushort message_size)
{
    writefln("run_client: addr=%s, num_messages=%d, message_size=%d", addr, num_messages, message_size);

    auto s = new UdpSocket();

    s.connect(addr);
    scope(exit) s.close();

    ubyte[] buf = uninitializedArray!(ubyte[])(message_size);
    for(int i=0; i<message_size; i++)
    {
        buf[i] = uniform(ubyte.min, ubyte.max);
    }
    auto send_buf = new OutBuffer();
    send_buf.write(buf);

    auto sw = StopWatch(AutoStart.yes);

    for(int i=0; i<num_messages; i++)
    {
//          writefln("i=%d", i);
        s.sendTo(send_buf.toBytes());
        s.receiveFrom(send_buf.toBytes());
    }
    TickDuration elapsed = sw.peek();

    writefln("Total elapsed time = %s usecs", elapsed.usecs);

    writefln("latency = %s usecs", elapsed.usecs / (num_messages*2));

//  if(buf != send_buf.toBytes())
    {
//      writefln("run_client: %s != %s", buf, send_buf.toBytes());
    }
}




