package main

// import "golang.org/x/tour/tree"

import (
	"fmt"
	"math/rand"
	"time"
	"tree"
)

func main() {
	// rand.Seed(42) // Try changing this number!
	rand.Seed(time.Now().UTC().UnixNano())

	t := tree.New(1)

	// fmt.Println(t)

	ch := make(chan int)

	go func() {
		tree.Walk(t, ch)
		close(ch)
	}()

	for i := range ch {
		fmt.Println(i)
	}

	t1 := tree.New(4)
	t2 := tree.New(4)

	fmt.Println("t1 = ", t1)
	fmt.Println("t2 = ", t2)

	fmt.Println(tree.Same(t1, t2))
}
