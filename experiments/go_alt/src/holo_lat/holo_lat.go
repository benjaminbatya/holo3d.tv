package main

import (
	// "bufio"
	"flag"
	"fmt"
	"math"
	"math/rand"
	"net"
	"os"
	"os/signal"
	"runtime/pprof"
	"time"
)

func checkError(err error) {
	if err != nil {
		fmt.Println("Error: ", err)
		os.Exit(0)
	}
}

func show_help() {
	fmt.Printf("Usage: %s [options]:\n", os.Args[0])
	flag.PrintDefaults()
}

func main() {
	is_help := flag.Bool("h", false, "Display this help message")
	is_server := flag.Bool("s", false, "Run in server mode")
	is_client := flag.Bool("c", false, "Run in client mode")

	addr := flag.String("a", "0.0.0.0:5555", "Address to use")
	num_messages := flag.Int("n", 100, "Num messages to send")
	message_size := flag.Int("m", 16, "Message size to send")

	cpuprofile := flag.String("p", "", "write cpu profile to file")

	flag.Parse()

	if *is_help {
		show_help()
		return
	}

	fmt.Printf("is_server: %v, is_client: %v, addr:%v, num_messages:%v, message_size:%v\n",
		*is_server, *is_client, *addr, *num_messages, *message_size)

	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			fmt.Println("Failed to open cpuprofile file!")
			return
		}

		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()

		// Setup an interrupt handler
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		go func() {
			for sig := range c {
				fmt.Printf("Received signal %v, stopping and exiting...", sig)
				pprof.StopCPUProfile()
				os.Exit(1)
			}
		}()
	}

	// Run the networking functionality
	if *is_server {
		server(*addr)
	} else if *is_client {
		client(*addr, *num_messages, *message_size)
	} else {
		fmt.Println("Either -s or -c must be specified!")
		show_help()
	}

}

func server(addr string) {
	udp_addr, err := net.ResolveUDPAddr("udp", addr)
	checkError(err)

	conn, err := net.ListenUDP("udp", udp_addr)
	defer conn.Close() // Make sure this is cleaned up
	checkError(err)

	const MAX_SIZE = math.MaxInt16

	buf := make([]byte, MAX_SIZE)

	for {
		_, remote_addr, err := conn.ReadFromUDP(buf)
		checkError(err)
		// fmt.Printf("Received %v bytes from %v\n", n, addr)

		_, err = conn.WriteToUDP(buf, remote_addr)
		checkError(err)

		// fmt.Println("Wroted back")

		// if(bytes_recv != bytes_sent)
		// {
		// 	fmt.Println("Error didn't")
		// }

	}

}

func client(addr string, num_messages int, message_size int) {
	remote_addr, err := net.ResolveUDPAddr("udp", addr)
	checkError(err)

	// _, err := net.ResolveUDPAddr("udp", "127.0.0.1:0")
	// checkError(err)

	conn, err := net.DialUDP("udp", nil, remote_addr)
	defer conn.Close()
	checkError(err)

	// reader := bufio.NewReader(conn)

	orig_buf := []byte{}

	rand.Seed(time.Now().UTC().UnixNano())
	for i := 0; i < message_size; i++ {
		orig_buf = append(orig_buf, byte(rand.Intn(math.MaxUint8)))
	}

	buf := orig_buf[:]

	fmt.Printf("New test")

	start := time.Now()

	for i := 0; i < num_messages; i++ {
		_, err := conn.Write(buf)
		checkError(err)

		_, err = conn.Read(buf)
		// reader.Read(buf)
		checkError(err)

		time.Sleep(10000 * time.Microsecond)
	}

	elapsed := time.Since(start)

	for i := 0; i < message_size; i++ {
		if buf[i] != orig_buf[i] {
			fmt.Println("buffers dont match!")
			os.Exit(0)
		}
	}

	usecs := elapsed.Nanoseconds()/1000 - int64(num_messages*10000)
	fmt.Printf("elapsed time = %d usecs\n", usecs)

	latency := usecs / int64(num_messages) / 2
	fmt.Printf("Latency = %d usecs\n", latency)
	fmt.Printf("main: ret=%d\n", latency)
}
