package tree

import (
	"fmt"
	"math/rand"
	"strings"
)

type Tree struct {
	Left  *Tree
	Value int
	Right *Tree
}

// Build a random binary tree
func New(k int) *Tree {
	arr := []int{}
	for i := 0; i < 10; i++ {
		arr = append(arr, i*k)
	}

	fmt.Println(arr)

	return recurse(arr)
}

func recurse(arr []int) *Tree {
	if len(arr) == 0 {
		return nil
	}

	loc := rand.Intn(len(arr))

	left := recurse(arr[:loc])
	right := recurse(arr[loc+1:])

	return &Tree{left, arr[loc], right}
}

// Walk walks the tree t sending all values
// from the tree to the channel ch.
func Walk(t *Tree, ch chan int) {
	if t == nil {
		return
	}

	Walk(t.Left, ch)
	ch <- t.Value
	Walk(t.Right, ch)
}

// Same determines whether the trees
// t1 and t2 contain the same values.
func Same(t1, t2 *Tree) bool {
	a := make(chan int)
	b := make(chan int)

	go Walk(t1, a)
	go Walk(t2, b)

	for i := 0; i < 10; i++ {
		t1_res := <-a
		t2_res := <-b
		if t1_res != t2_res {
			return false
		}
	}

	return true
}

func (t *Tree) String() string {
	return t.StringHelper(0)
}

func (t *Tree) StringHelper(depth int) string {
	if t == nil {
		return "nil"
	}
	padding := strings.Repeat("  ", depth)
	return fmt.Sprintf( /*"\n%s[Val:%v, Left:%s, Right:%s%s]\n",*/
		"\n%s%v, %s, %s\n",
		padding, t.Value,
		t.Left.StringHelper(depth+1),
		t.Right.StringHelper(depth+1))
	// ,padding)
}
