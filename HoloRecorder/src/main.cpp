// Main.cpp for HoloRecorder
#include "pch_files.hpp"

#include <stdio.h>
#include <cstring>

namespace po = boost::program_options;
namespace fs = boost::filesystem;

#include "util_funcs.hpp"

#include "server.hpp"

int main(int argc, char **argv)
{    
    boost::system::error_code ec;

    try
    {
        std::string listening_port = holo3d::DEFAULT_PORT;
        size_t num_cameras = 1; // Default number of cameras to listen for is one for now.
        std::string file_prefix = "";
        std::string app_name = fs::basename(argv[0]);
        po::options_description visible(std::string("Usage: ") + app_name + " <options> file_prefix \nAllowed Options");
        visible.add_options()
        ("help,h",                                                                                          "display this help message")
        ("port,p",              po::value<std::string>(&listening_port)->default_value(listening_port),     "The port to start listening for connection on")
        ("num_cameras,n",       po::value<size_t>(&num_cameras)->default_value(num_cameras),                "Number of cameras to listen for")
        ;

        po::options_description hidden;
        hidden.add_options()
        ("file_prefix", po::value<std::string>(&file_prefix)->required(),                             "The prefix for each file to save to. The suffix is the camera number and '.h3t'")
        ;

        po::options_description all;
        all.add(visible).add(hidden);

        po::positional_options_description required;
        required.add("file_prefix", 1);

        po::variables_map vm;
        try
        {
            po::store(po::command_line_parser(argc, argv).options(all).positional(required).run(), vm);

            if (vm.count("help"))
            {
                INFO_CPP(visible);
                return 0;
            }

            po::notify(vm);
        }
        catch (po::error& e)
        {
            ERROR_CPP(e.what());
            INFO_CPP(visible);
            return -1;
        }

        boost::filesystem::path path(file_prefix);

        // Make sure the file is always saved in the current directory. 
        // OpenNI2 has the annoying habit of redirecting the file to its Drivers directory
        path = boost::filesystem::absolute(path);
        boost::filesystem::create_directories(path);

        path = boost::filesystem::canonical(path);

        INFO("Saving streams into %1%", path.string());

        holo3d::file_saver::context context;
        context.file_prefix = path.string();

        holo3d::server server(listening_port, num_cameras, context);
        
        server.run();

    } catch (std::exception &e)
    {
        ERROR_CPP(std::endl << e.what());
        return -1;
    } 
}
