#ifndef _INPUT_HANDLER_HPP_
#define _INPUT_HANDLER_HPP_

namespace holo3d
{

/**
 * This is a singleton class for handling the keyboard input
 * 
 * @author benjamin (11/26/2014)
 */
class input_handler : public std::enable_shared_from_this<input_handler>
{
public:
    typedef std::shared_ptr<input_handler> ptr_t;

    static void run(boost::asio::io_service& io_service);

protected:
    explicit input_handler(boost::asio::io_service& io_service);

    void do_wait();

    void check_keys(const boost::system::error_code& error);

protected:
    static ptr_t m_singleton;
    boost::posix_time::time_duration m_delay;
    boost::asio::deadline_timer m_timer;
};

}; // namespace holo3d

#endif // _INPUT_HANDLER_HPP_
