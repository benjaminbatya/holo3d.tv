#include "pch_files.hpp"

#include <OpenNI2/OpenNI.h>

#include "util_funcs.hpp"
#include "oni_sample_utils.hpp"

#include "input_handler.hpp"

namespace holo3d
{

input_handler::ptr_t input_handler::m_singleton;

void input_handler::run(boost::asio::io_service& io_service)
{
    if (!m_singleton)
    {
        m_singleton.reset(new input_handler(io_service)); 
        m_singleton->do_wait();
    }
}

input_handler::input_handler(boost::asio::io_service& io_service)
: m_delay(boost::posix_time::milliseconds(33))
, m_timer (io_service, m_delay)
{
}

void input_handler::do_wait()
{
    m_timer.expires_at(m_timer.expires_at() + m_delay);
    m_timer.async_wait([&](const boost::system::error_code& ec) { this->check_keys(ec); });
}

void input_handler::check_keys(const boost::system::error_code& error)
{
    ASSERT_THROW(!error, "read error: %s", boost::system::system_error(error).what());

    if(wasKeyboardHit())
    {
        INFO("quitting!");

        // Kill the io_service
        m_timer.get_io_service().stop();

    } else
    {
        this->do_wait();
    }
}

}; // namespace holo3d
