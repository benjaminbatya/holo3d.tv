#ifndef _SERVER_H_
#define _SERVER_H_

#include "fragmenting_socket.hpp"
#include "camera_info.hpp"
#include "file_saver.hpp"

namespace holo3d
{

class server  // : private boost::noncopyable
{
public:
    server(const std::string& listening_port, const size_t& num_cameras, const file_saver::context& save_context);
    ~server();

    void run();

protected:

    void handle_camera_connect(const boost::system::error_code& error,
                               const size_t& bytes_recvd);

    const size_t                    m_max_cameras;      // << The maximum number of cameras listened for by this

    boost::asio::io_service         m_service;          // << The service to use
    fragmenting_socket                      m_socket;           // << The socket that'll receive messages from the cameras

    boost::thread_group             m_threads;          // << Threads spawned by the server

    typedef std::auto_ptr<boost::asio::io_service::work> work_ptr_t;
    work_ptr_t                      m_work;

    file_saver::context             m_save_context;
    typedef std::shared_ptr<file_saver> file_saver_ptr_t;
    typedef std::vector<file_saver_ptr_t> file_saver_vec_t;
    file_saver_vec_t                m_file_savers;       // << Saves to file one of the camera streams

    static const size_t MAX_BUFFER_SIZE = 0xffff;
    uint8_t m_buffer[MAX_BUFFER_SIZE];                  // << This is the temporary buffer for all incoming data

    typedef std::shared_ptr<camera_info> camera_ptr_t;
    // typedef camera_info* camera_ptr_t;
    typedef std::map<uint32_t, camera_ptr_t> map_t;
    map_t                           m_cameras;          // << A mapping of ip addresses to corresponding camera information. Not really needed..
    uint16_t                        m_next_port;        // << The next port to assign a camera to transmit it's frames to
    uint64_t                        m_start_time = 0;   // << Time the cameras should start grabbing frames at
};


}; // namespace holo3d

#endif // _SERVER_H_
