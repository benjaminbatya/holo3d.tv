#ifndef _FILE_SAVER_H_
#define _FILE_SAVER_H_

#include "fragmenting_socket.hpp"
#include "fps_counter.hpp"

namespace holo3d
{

class camera_info;

// This will save a h3t stream to file
class file_saver
{
public:

    struct context
    {
        std::string file_prefix;
    };

    file_saver(const file_saver& that);
    file_saver(boost::asio::io_service& service, const context& context, size_t interval);
    ~file_saver();

    void run(camera_info::ptr_t cam);

protected:

    void setup(const boost::system::error_code& error);

    void save_frame(const boost::system::error_code& error);

    void send_data(const void* data, const size_t& amount_to_send);
    void receive_data(void* data, const size_t& amount_to_recv);

    boost::asio::deadline_timer         m_timer;                // << timer object
    context                             m_context;              // << context in which to operate in
                                                
    FILE*                               m_file = nullptr;       // << Used to save the data to file

    camera_info::ptr_t                  m_camera;               // weak pointer to the camera_info object
    boost::posix_time::time_duration    m_interval;             // How often to capture frames (in ms)

    fps_counter                         m_fps { "file_saver" };

    size_t                              m_count = 0;
};

} // namespace holo3d

#endif // _FILE_SAVER_H_
