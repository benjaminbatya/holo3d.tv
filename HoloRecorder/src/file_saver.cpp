#include "pch_files.hpp"

#include "util_funcs.hpp"

#include "camera_info.hpp"
#include "file_saver.hpp"

using boost::asio::ip::udp;

namespace holo3d
{

file_saver::file_saver(boost::asio::io_service &service, const context& save_context, size_t interval)
: m_timer(service, boost::posix_time::milliseconds(interval))
, m_context(save_context)
, m_interval(boost::posix_time::milliseconds(interval))
{
}

file_saver::~file_saver()
{
    if (m_file)
    {
        INFO("processed %1% frames", m_count); 

        fclose(m_file); // follow RAII semantics
        m_file = nullptr;
    }

    m_timer.cancel();
}

void file_saver::run(camera_info::ptr_t weak_cam)
{   
    m_camera = weak_cam;

    INFO("file prefix = %s", m_context.file_prefix);

    // Make the setup occur asycronously as soon as possible
    m_timer.async_wait(boost::bind(&file_saver::setup, this, boost::asio::placeholders::error));
}

void file_saver::setup(const boost::system::error_code& error)
{
    ASSERT_THROW(!error, error.message());

    std::shared_ptr<camera_info> cam = m_camera.lock();
    ASSERT(cam); // just assume that it won't expire now...

    STREAM_HEADER file_header = cam->header();
    INFO_CPP("file_header=" << file_header);

    // Open the file for writing
    m_file = fopen(m_context.file_prefix.c_str(), "wb");
    ASSERT_THROW(m_file, "failed to open '%s' for writing", m_context.file_prefix);

    size_t ret = fwrite(&file_header, sizeof(STREAM_HEADER), 1, m_file);
    ASSERT_EQUAL(ret, 1);

    // Readjust the timer to compensate for drift
    m_timer.expires_at(m_timer.expires_at() + m_interval);
    m_timer.async_wait(boost::bind(&file_saver::save_frame, this, boost::asio::placeholders::error));

    std::string name = (boost::format("file_saver: camera=%1%") % cam->endpoint()).str();

    m_fps.name(name);

    m_count = 0;
}

void file_saver::save_frame(const boost::system::error_code& error)
{
    ASSERT_THROW(!error, error.message());

    std::shared_ptr<camera_info> cam = m_camera.lock();
    // camera_info* cam = m_camera;
    ASSERT(cam);

    camera_frame frame;
    bool has_frame = cam->pop_frame(frame);
    if (has_frame) // save if there is a frame
    {
        FRAME_HEADER frame_header = frame.frame_header(); 

        size_t ret = fwrite(&frame_header, sizeof(FRAME_HEADER), 1, m_file);
        ASSERT_EQUAL(ret, 1);

        ret = fwrite(frame.color_buffer(), frame.color_buffer_size(), 1, m_file);
        ASSERT_EQUAL(ret, 1);

        ret = fwrite(frame.depth_buffer(), frame.depth_buffer_size(), 1, m_file);
        ASSERT_EQUAL(ret, 1);

        // update the fps counter
        m_fps.update();

        m_count++;
    }

    // Readjust the timer to compensate for drift
    m_timer.expires_at(m_timer.expires_at() + m_interval);
    m_timer.async_wait(boost::bind(&file_saver::save_frame, this, boost::asio::placeholders::error));
}

}; // namespace holo3d
