#include "pch_files.hpp"

#include "util_funcs.hpp"

#include "camera_info.hpp"
#include "file_saver.hpp"

using boost::asio::ip::udp;

namespace holo3d
{

file_saver::file_saver(boost::asio::io_service &service, const save_context& save_context, size_t interval)
: m_timer(service, boost::posix_time::milliseconds(interval))
, m_context(save_context)
, m_socket(service, 0, 0)
, m_interval(boost::posix_time::milliseconds(interval))
{
    udp::resolver resolver(service);
    udp::resolver::query query(udp::v4(), save_context.client_address, save_context.client_port);
    m_socket.endpoint() = *resolver.resolve(query);
}

file_saver::~file_saver()
{
    if (m_file)
    {
        INFO("processed %1% frames", m_count); 

        fclose(m_file); // follow RAII semantics
        m_file = nullptr;
    }

    m_socket.close();
    m_timer.cancel();
}

void file_saver::run(camera_info::ptr_t weak_cam)
{
    if (!m_context.save_to_file && !m_context.send_to_client)
    {
        return;
    }

    m_camera = weak_cam;

    // Make the setup occur asycronously as soon as possible
    m_timer.async_wait(boost::bind(&file_saver::setup, this, boost::asio::placeholders::error));
}

void file_saver::setup(const boost::system::error_code& error)
{
    ASSERT_THROW(!error, error.message());

    std::shared_ptr<camera_info> cam = m_camera.lock();
    // camera_info* cam = m_camera;
    ASSERT(cam); // just assume that it won't expire now...

    STREAM_HEADER file_header = cam->header();
    INFO_CPP("file_header=" << file_header);

    if (m_context.save_to_file)
    {
        // Open the file for writing
        m_file = fopen(m_context.file_name.c_str(), "wb");
        ASSERT_THROW(m_file, "failed to open '%s' for writing", m_context.file_name);

        size_t ret = fwrite(&file_header, sizeof(STREAM_HEADER), 1, m_file);
        ASSERT_EQUAL(ret, 1);
    }

    if (m_context.send_to_client)
    {
        // Get the local ip
        m_socket.open();
        m_socket.connect();
        boost::asio::ip::address local_addr = m_socket.local_endpoint().address();
        m_socket.close();

        m_socket.open();

        // First send a NEW_CAMERA_REQUEST
        NEW_CAMERA_REQUEST request;
        request.ip_address = local_addr.to_v4().to_ulong();

        INFO_CPP("Sending " << request << " to " << m_socket.endpoint().address().to_string());

        send_data(&request, sizeof(request));

        // Get the reply
        NEW_CAMERA_REPLY reply;
        receive_data(&reply, sizeof(reply));
        
        // Reset the port
        m_socket.close();
        m_socket.endpoint().port((unsigned short)reply.assigned_port);
        
        uint64_t sleep_for = reply.start_time - get_time();
        
        INFO("Got reply from %1%: assigned port=0x%2$x, sleep_for=%3%", m_socket.endpoint(), reply.assigned_port, sleep_for);

        // boost::this_thread::sleep_for(boost::chrono::microseconds(sleep_for));

        m_socket.open();
        m_socket.connect();

        INFO_CPP("sending file header = " << file_header)

        send_data(&file_header, sizeof(file_header));
    }

    // Readjust the timer to compensate for drift
    m_timer.expires_at(m_timer.expires_at() + m_interval);
    m_timer.async_wait(boost::bind(&file_saver::save_frame, this, boost::asio::placeholders::error));

    m_fps.name((boost::format("file_saver: camera ip=%1%") % cam->endpoint()).str());

    m_count = 0;
}

void file_saver::save_frame(const boost::system::error_code& error)
{
    ASSERT_THROW(!error, error.message());

    std::shared_ptr<camera_info> cam = m_camera.lock();
    // camera_info* cam = m_camera;
    ASSERT(cam);

    camera_frame frame;
    bool has_frame = cam->pop_frame(frame);
    if (has_frame) // save if there is a frame
    {
        FRAME_HEADER frame_header = frame.frame_header(); 

        if (m_context.save_to_file)
        {
            size_t ret = fwrite(&frame_header, sizeof(FRAME_HEADER), 1, m_file);
            ASSERT_EQUAL(ret, 1);

            ret = fwrite(frame.color_buffer(), frame.color_buffer_size(), 1, m_file);
            ASSERT_EQUAL(ret, 1);

            ret = fwrite(frame.depth_buffer(), frame.depth_buffer_size(), 1, m_file);
            ASSERT_EQUAL(ret, 1);
        }

        if (m_context.send_to_client)
        {
            send_data(&frame_header, sizeof(frame_header));

            send_data(frame.color_buffer(), frame.color_buffer_size());

            send_data(frame.depth_buffer(), frame.depth_buffer_size());
        }

        // update the fps counter
        m_fps.update();

        m_count++;
    }

    // Readjust the timer to compensate for drift
    m_timer.expires_at(m_timer.expires_at() + m_interval);
    m_timer.async_wait(boost::bind(&file_saver::save_frame, this, boost::asio::placeholders::error));
}

void file_saver::send_data(const void* data, const size_t& amount_to_send)
{
    // INFO_CPP("Sending " << data << " of size " << amount_to_send);

    boost::system::error_code error;
    size_t ret = m_socket.send(data, amount_to_send, error);
    ASSERT_THROW(!error, error.message()); // error occurred
    ASSERT_EQUAL(ret, amount_to_send);
}

void file_saver::receive_data(void* data, const size_t& amount_to_recv)
{
    boost::system::error_code error;
    size_t ret = m_socket.receive(data, amount_to_recv, error);
    ASSERT_THROW(!error, error.message()); // error occurred
    ASSERT_EQUAL(ret, amount_to_recv);
}


}; // namespace holo3d
