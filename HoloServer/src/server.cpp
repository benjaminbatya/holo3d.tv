#include "pch_files.hpp"

#include <ctime>
#include <iostream>
#include <string>
#include <memory>
#include <thread>
#include <map>

#include "h3t_file.hpp"
#include "util_funcs.hpp"

#include "server.hpp"
#include "input_handler.hpp"

using boost::asio::ip::udp;

namespace holo3d
{

server::server(const std::string& listening_port, const size_t& num_cameras, const save_context& save_context)
: m_max_cameras(num_cameras)
, m_socket(m_service, 0, atoi(listening_port.c_str()))
, m_work(new boost::asio::io_service::work(m_service))
, m_file_saver(m_service, save_context, 33)
, m_next_port(atoi(listening_port.c_str())+1)
{
    size_t num_threads = std::thread::hardware_concurrency();

    INFO("Listening for %1% cameras, spawning %2% threads", num_cameras, num_threads);

    for(size_t i=0; i<num_threads; i++)
    {
        m_threads.create_thread(boost::bind(&boost::asio::io_service::run, &m_service));
    }
}

server::~server()
{
    m_socket.close();

    // the socket will automatically destruct when this instance is destroyed
    m_service.stop();
}

/**
 * Runs the server. This runs asyncronously
 * 
 * @author benjamin (11/20/2014)
 */
void server::run()
{
    boost::system::error_code error = 
        m_socket.bind();
    ASSERT_THROW(!error, error.message());

    INFO("listening to port %1%", m_socket.endpoint());

    // Listen for a camera connect message
    m_socket.async_receive(m_buffer, MAX_BUFFER_SIZE, 
                           boost::bind(&server::handle_camera_connect, this,
                                       boost::asio::placeholders::error,
                                       boost::asio::placeholders::bytes_transferred));

    input_handler::run(m_service);

    // Wait until all of the threads are idle
    m_threads.join_all();

    INFO("done!");
}

void server::handle_camera_connect(const boost::system::error_code& error,
                                   const size_t& bytes_recvd)
{
    ASSERT(!error);
    ASSERT_EQUAL(bytes_recvd, sizeof(NEW_CAMERA_REQUEST));

    // Get the camera request with the ip address and
    // send a reply with the number of a new port for the client to transmit to

    // NOTE: We don't actually do anything with the request right now...
    // The socket endpoint provides all of the information we need
//  NEW_CAMERA_REQUEST* request = (NEW_CAMERA_REQUEST*)m_buffer;

    boost::asio::ip::udp::endpoint cam_endpoint = m_socket.endpoint();

    INFO("Got a new camera request from %1%", cam_endpoint);

    // This will be unique for each camera
    uint16_t local_port = m_next_port++; 

    // Make sure the camera exists
    if (m_cameras.find(local_port) == m_cameras.end())
    {
        INFO("Assigning new camera port = %1% to camera %2%", local_port, cam_endpoint);
        camera_ptr_t cam_ptr = std::make_shared<camera_info>(m_service, cam_endpoint, local_port);
        m_cameras.emplace(local_port, cam_ptr);
    }

    INFO("num cameras = %1%, max num cameras = %2%", m_cameras.size(), m_max_cameras);

    // We have received enough connect requests.
    if (m_cameras.size() >= m_max_cameras)
    {
        uint64_t start_time = get_time() + 1 * MICROS_PER_SEC; // Give each of the cameras 1 second to connect from now

        // Send back the replies
        for (auto& pair : m_cameras)
        {
            camera_ptr_t cam = pair.second;

            NEW_CAMERA_REPLY reply(cam->local_port(), start_time);

            INFO("Sending reply message to %s with assigned_port=%u and start_time=%u", 
                 cam->endpoint(), cam->local_port(), start_time);

            // This send has to be syncronous
//          std::this_thread::sleep_for(std::chrono::milliseconds(10));

            // Make sure that we are sending to the correct endpoint
            m_socket.endpoint() = cam->endpoint();

            m_socket.async_send(&reply, sizeof(NEW_CAMERA_REPLY),
                                boost::bind(&camera_info::handle_camera_reply, cam, 
                                            boost::asio::placeholders::error,
                                            boost::asio::placeholders::bytes_transferred));

        }

        if (m_cameras.size() > 0)
        {
            camera_ptr_t cam = m_cameras.begin()->second;
            // Only write out the frames received by the first camera
            cam->inform_on_file_header(
                [&] (camera_info::ptr_t cam) { m_file_saver.run(cam); } // Gotta love lambdas
                );
        }
    } else
    {
        // Listen for a camera connect message
        m_socket.async_receive(m_buffer, MAX_BUFFER_SIZE,
                               boost::bind(&server::handle_camera_connect, this,
                                           boost::asio::placeholders::error,
                                           boost::asio::placeholders::bytes_transferred));
    }
}

}; // namespace holo3d
