// Main.cpp for HoloViewer
#include "pch_files.hpp"

#include <stdio.h>
#include <cstring>

namespace po = boost::program_options;

#include "util_funcs.hpp"

#include "server.hpp"

int main(int argc, char **argv)
{    
    try
    {
        std::string listening_port = holo3d::DEFAULT_PORT;
        size_t num_cameras = 1; // Default number of cameras to listen for is one for now.
        std::string stream_save_path = "";
        std::string client_port = holo3d::DEFAULT_PORT;
        std::string display_client = ""; // "localhost:" + client_port;
        std::string app_name = boost::filesystem::basename(argv[0]);
        po::options_description visible(std::string("Usage: ") + app_name + " <options> \nAllowed Options");
        visible.add_options()
        ("help,h",                                                                                          "display this help message")
        ("port,p",              po::value<std::string>(&listening_port)->default_value(listening_port),     "The port to start listening for connection on")
        ("num_cameras,n",       po::value<size_t>(&num_cameras)->default_value(num_cameras),                "Number of cameras to listen for")  
        ("file,f",              po::value<std::string>(&stream_save_path)->default_value(stream_save_path), "File to save the first camera stream to. Will be given the .h3t extension.")
        ("display_client,d",    po::value<std::string>(&display_client)->default_value(display_client),     "The display client to send the frames to from the first camera. Formatted as host:port or just host with default port")
        ;

        po::options_description all;
        all.add(visible);

        po::variables_map vm;
        try
        {
            po::store(po::command_line_parser(argc, argv).options(all).run(), vm);

            if (vm.count("help"))
            {
                INFO_CPP(visible);
                return 0;
            }

            po::notify(vm);
        }
        catch (po::error& e)
        {
            ERROR_CPP(e.what());
            INFO_CPP(visible);
            return -1;
        }

        holo3d::save_context context;

        // Fix up the stream_save_path if it's not empty
        if (stream_save_path.length() > 0)
        {
            boost::filesystem::path path(stream_save_path);
            // Make sure the file always has the proper extension
            path = path.replace_extension(holo3d::DEFAULT_FILE_EXTENSION);
            // Make sure the file is always saved in the current directory. 
            // OpenNI2 has the annoying habit of redirecting the file to its Drivers directory
            path = boost::filesystem::absolute(path);
            context.file_name = path.string();
            context.save_to_file = true;

            INFO("Saving frames to '%s'", context.file_name);
        }

        if (!display_client.empty())
        {
            size_t pos = display_client.find_first_of(":");
            if (pos != std::string::npos)
            {
                client_port = display_client.substr(pos+1);
                display_client = display_client.substr(0, pos);
            }
            context.client_address = display_client; 
            context.client_port = client_port;
            context.send_to_client = true;

            INFO("Streaming frames to %s:%s", context.client_address, context.client_port);
        }

        holo3d::server server(listening_port, num_cameras, context);
        
        server.run();

    } catch (std::exception &e)
    {
        ERROR_CPP(std::endl << e.what());
        return -1;
    } 
}
