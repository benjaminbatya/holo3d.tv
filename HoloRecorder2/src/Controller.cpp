#include "pch_files.hpp"

#include <Qt/QtGui>

#include <yaml-cpp/yaml.h>

#include "Controller.hpp"
#include "server.hpp"
#include "Window.h"

#include "option_enums.hpp"
#include "util_funcs.hpp"

namespace fs = boost::filesystem;

namespace holo3d
{

Controller::Controller(Server* server, QWidget* parent)
: QMainWindow(parent)
, m_server(server)
{
    setupUi(this);

    connect(load_action, SIGNAL(triggered()), this, SLOT(load_settings()));
    connect(save_action, SIGNAL(triggered()), this, SLOT(save_settings()));
    connect(quit_action, SIGNAL(triggered()), QApplication::instance(), SLOT(quit()));

    connect(record_button, SIGNAL(clicked()), this, SLOT(toggle_recording()));

    directory_edit->installEventFilter(this);

    server->set_new_camera_cb( [&](file_saver* saver) 
    { 
        QMetaObject::invokeMethod(this, "build_window", Q_ARG(void*, saver));
    });
}

Controller::~Controller()
{
    quit();
}

void Controller::show()
{
    // Populate the dialog
    Server::string_vec found_cameras = m_server->found_cameras();

    for (const auto& cam : found_cameras)
    {
        QListWidgetItem* item = new QListWidgetItem(tr(cam.c_str()));
        list_cameras_available->addItem( item );
    }

    // Select the first host in the list
    QListWidgetItem* item = list_cameras_available->item(0);
    list_cameras_available->setCurrentItem(item);

    typedef std::vector<std::tuple<std::string, uint8_t>> tuple_vec;

    tuple_vec color_formats;
    for (uint8_t i=(uint8_t)COLOR_MODE::MIN; i<=(uint8_t)COLOR_MODE::MAX; i++) 
    {
        std::stringstream ss;
        ss << (COLOR_MODE)i;
        color_formats.push_back(std::make_tuple(ss.str(), i));
    }

    for (const auto& format: color_formats) 
    {
        QListWidgetItem* item = new QListWidgetItem(tr(std::get<0>(format).c_str()));
        item->setData(Qt::UserRole, std::get<1>(format));
        list_color_stream_types->addItem( item );
        // Default is JPEG for now...
        if(std::get<1>(format) == (uint8_t)STREAM_FORMAT::COLOR_JPEG)
        {
            list_color_stream_types->setCurrentItem(item);
        }
    }

    tuple_vec depth_formats;
    for (uint8_t i=(uint8_t)DEPTH_MODE::MIN; i<=(uint8_t)DEPTH_MODE::MAX; i++) 
    {
        std::stringstream ss;
        ss << (DEPTH_MODE)i;
        depth_formats.push_back(std::make_tuple(ss.str(), i));
    }

    for (const auto& format: depth_formats) 
    {
        QListWidgetItem* item = new QListWidgetItem(tr(std::get<0>(format).c_str()));
        item->setData(Qt::UserRole, std::get<1>(format));
        list_depth_stream_types->addItem( item );
        // Default is PNG for now...
        if(std::get<1>(format) == (uint8_t)STREAM_FORMAT::DEPTH_PNG)
        {
            list_depth_stream_types->setCurrentItem(item);
        }
    }

    auto resolutions = 
    { std::make_tuple("Small (320x240)", (uint8_t)RESOLUTION::SMALL)
    , std::make_tuple("Medium (640x480)", (uint8_t)RESOLUTION::MEDUIM)
    //, {"Large (1024x768)", RESOLUTION::LARGE}
    };
    for (auto& it: resolutions) 
    {
        resolution_combo->addItem(std::get<0>(it), std::get<1>(it)); 
    }

    // Default is medium for now...
    resolution_combo->setCurrentIndex(1);

    auto qualities = 
    { std::make_tuple("Realtime", (uint8_t)QUALITY::REALTIME)
    , std::make_tuple("Average", (uint8_t)QUALITY::AVERAGE)
    , std::make_tuple("Good", (uint8_t)QUALITY::GOOD)
    , std::make_tuple("Best", (uint8_t)QUALITY::BEST)
    };
    for (auto& it: qualities) 
    {
        quality_combo->addItem(std::get<0>(it), std::get<1>(it)); 
    }
    // Default is QUALITY::GOOD for now...
    quality_combo->setCurrentIndex(2);

    // Default is 15 fps
    fps_spin_box->setValue(15);

    auto record_options = 
    { std::make_tuple("Do Recording", true)
    , std::make_tuple("Just Display", false)
    };
    for (auto& it : record_options) 
    {
        record_combo->addItem(std::get<0>(it), std::get<1>(it));
    }
    record_combo->setCurrentIndex(0);

    this->record_button->setText("Start Recording");

    // Load the last settings
    load_settings(true);

    QMainWindow::show();
}

// NOTE: ptr is void* because it's hard to pass through a typed pointer through Qt
// doing an unsafe cast to file_saver afterwards is easiest
void Controller::build_window(void* ptr)
{
    file_saver* saver = (file_saver*)ptr;

    INFO("Called");
    Window* window = new Window("Default", saver);

    m_windows.push_back(window);

    window->show();
}


bool Controller::eventFilter(QObject* obj, QEvent* event)
{
    if (!(obj == directory_edit && event->type() == QEvent::MouseButtonRelease))
    {
        return QObject::eventFilter(obj, event);
    }

    // Make sure the path is valid
    std::string path_str = directory_edit->text().toStdString();

    if (!fs::exists(path_str)) 
    {
        path_str = getenv("HOLO_ROOT");
        if (path_str.empty()) 
        {
            path_str = getenv("PWD");
        }
    }

    std::string new_path = QFileDialog::getExistingDirectory(this, tr("Open Directory"), path_str.c_str()).toStdString();

    if (!new_path.empty()) 
    {
        path_str = new_path;
    }

    directory_edit->setText(path_str.c_str());

    return true;
}

void Controller::toggle_recording()
{
    bool start = !m_server->is_running();

    if (start) 
    {
        // Gather the context
        Server::Context context;

        // Get the selected host
        QList<QListWidgetItem*> list = list_cameras_available->selectedItems();
        if (list.length() < 1) 
        {
            QMessageBox::critical(this, "Invalid Selection", "You must select at least one (1) camera to use!");
            return;
        }

        for (auto& item : list) 
        {
            context.cameras_to_run.push_back(item->text().toStdString());
        }

        int index = resolution_combo->currentIndex();
        context.camera_config.resolution = (RESOLUTION)resolution_combo->itemData(index).toInt();

        QListWidgetItem* item = list_color_stream_types->currentItem();
        context.camera_config.color_format = (STREAM_FORMAT)item->data(Qt::UserRole).toInt();

        item = list_depth_stream_types->currentItem();
        context.camera_config.depth_format = (STREAM_FORMAT)item->data(Qt::UserRole).toInt();

        context.camera_config.fps = (uint8_t)fps_spin_box->value();

        index = quality_combo->currentIndex();
        context.camera_config.quality = (QUALITY)quality_combo->itemData(index).toInt();

        // Make sure the path is valid
        std::string path_str = directory_edit->text().toStdString();

        boost::filesystem::path path(path_str);

        context.save_directory = path.string();

        index = record_combo->currentIndex();
        context.recording = record_combo->itemData(index).toBool();

        if (context.recording)
        {
            if(!boost::filesystem::exists(path))
            {
                std::string error_str = std::string("Invalid save directory '") + path_str + "' specified!";
                QMessageBox::critical(this, "Invalid Path", error_str.c_str());
                return;
            } else
            {
                INFO("Saving streams into %1%", context.save_directory);
            }
        }

        //  start recording
        m_server->start_recording(context);

        this->record_button->setText("Stop Recording");
        QPalette pal = record_button->palette();
        pal.setColor(QPalette::Button, Qt::red);
        this->record_button->setPalette(pal);

    } else
    {
        // Stop recording
        stop_recording();

        this->record_button->setText("Start Recording");
        QPalette pal = record_button->palette();
        pal.setColor(QPalette::Button, Qt::lightGray);
        this->record_button->setPalette(pal);
    }
}

void Controller::quit()
{
    INFO("Called");

    stop_recording();

    save_settings(true);
}

void Controller::stop_recording()
{
    INFO("Called");

    m_server->stop_recording();

    // Close all of the open windows
    for (Window* window : m_windows) 
    {
        window->deleteLater();
    }
    m_windows.clear();

}

void Controller::load_settings(bool use_last)
{
    QSettings settings("Holo3D", "HoloRecorder2");

    QString group = "last";

    if (!use_last) 
    {
        // Display a dialog to display all saved groups
        bool ok;

        group = QInputDialog::getItem(this, "Select setting to load", " Available Settings", 
                                      settings.childGroups(), 0, false, &ok);
        if (!ok) 
        {
            return;
        }
    }

    INFO("Loading settings from '%s'", group.toStdString());

    settings.beginGroup(group);
    {

        int index = settings.value("resolution", 1).toInt();
        resolution_combo->setCurrentIndex(index);

        index = settings.value("color_stream", (uint8_t)COLOR_MODE::JPEG).toInt();
        list_color_stream_types->setCurrentRow(index);

        index = settings.value("depth_stream", (uint8_t)DEPTH_MODE::PNG).toInt();
        list_depth_stream_types->setCurrentRow(index);

        index = settings.value("fps", 15).toInt();
        fps_spin_box->setValue(index);

        index = settings.value("quality", 2).toInt();
        quality_combo->setCurrentIndex(index);

        index = settings.value("recording", 0).toInt();
        record_combo->setCurrentIndex(index);

        QString path = settings.value("path", "").toString();
        directory_edit->setText(path);

    }
    settings.endGroup();
}

void Controller::save_settings(bool use_last)
{
    QSettings settings("Holo3D", "HoloRecorder2");

    QString group = "last";

    if (!use_last) 
    {
        bool ok;

        group = QInputDialog::getItem(this, "Select setting to save", "Setting name to use", 
                                      settings.childGroups(), 0, true, &ok);
        if (!ok || group.isEmpty()) 
        {
            return;
        }
    }

    INFO("Saving settings to '%s'", group.toStdString());


    settings.beginGroup(group);
    {

        int index = resolution_combo->currentIndex();
        settings.setValue("resolution", index );

        index = list_color_stream_types->currentRow();
        settings.setValue("color_stream", index );

        index = list_depth_stream_types->currentRow();
        settings.setValue("depth_stream", index );

        index = fps_spin_box->value();
        settings.setValue("fps", index );

        index = quality_combo->currentIndex();
        settings.setValue("quality", index );

        index = record_combo->currentIndex();
        settings.setValue("recording", index );

        settings.setValue("path", directory_edit->text() );

    }
    settings.endGroup();
}


}; // namespace holo3d
