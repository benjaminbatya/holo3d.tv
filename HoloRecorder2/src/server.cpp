#include "pch_files.hpp"

#include <ctime>
#include <iostream>
#include <string>
#include <memory>
#include <thread>
#include <map>

#include "h3t_file.hpp"
#include "util_funcs.hpp"

#include "server.hpp"
#include "input_handler.hpp"

using boost::asio::ip::udp;

namespace holo3d
{

const int SAVE_INTERVAL = 33;

Server::Server(const std::string& listening_port) 
: m_socket(m_service, 0, atoi(listening_port.c_str()))
, m_work(m_service) 
, m_next_port(atoi(listening_port.c_str())+1)
, m_control_server()
{
    // NOTE: I can't debug HoloRecorder2 in slickedit if std::thread::hardware_concurrency is used because it relies on GLIBCXX_3.4.17 and
    // slickedit only goes to GLIBCXX_3.4.16. Hopefully slickedit support will come up with a fix
    size_t num_threads = 4; // std::thread::hardware_concurrency();

    INFO("Spawning %1% threads", num_threads);

    for(size_t i=0; i<num_threads; i++)
    {
        m_threads.create_thread(boost::bind(&boost::asio::io_service::run, &m_service));
    }

    boost::system::error_code error = m_socket.bind();
    ASSERT_THROW(!error, error.message());
}

Server::~Server()
{
    m_socket.close();

    // the socket will automatically destruct when this instance is destroyed
    m_service.stop();

    m_threads.join_all();
}

/**
 * Finds cameras from a list of possible cameras
 * 
 * @author benjamin (11/20/2014)
 */
Server::string_vec Server::found_cameras()
{
    string_vec found_cameras;

    string_vec possible_cameras = { "localhost", "camera1", "camera2", "camera3", "camera4" };

    for (auto& cam : possible_cameras) 
    {
        if (m_control_server.query(cam)) 
        {
            found_cameras.push_back(cam);
        }
    }

    return found_cameras;
}

void Server::start_recording(const Context& context)
{
    // Listen for a camera connect message
    m_socket.async_receive(m_buffer, MAX_BUFFER_SIZE, 
                           boost::bind(&Server::handle_camera_connect, this,
                                       boost::asio::placeholders::error,
                                       boost::asio::placeholders::bytes_transferred));

    m_context = context;

    for (auto& cam : context.cameras_to_run) 
    {
        bool got_response = m_control_server.start(cam, context.camera_config);
        if (!got_response) 
        {
            ERROR("Camera %s didn't respond, ignoring", cam);
        }
    }
}

void Server::stop_recording()
{
    for (auto& cam : m_context.cameras_to_run) 
    {
        bool got_response = m_control_server.end(cam);
        if (!got_response) 
        {
            ERROR("Camera %s didnt respond, ignoring", cam);
        }
    }

    if (m_delete_camera_cb) 
    {
        for (auto& saver : m_file_savers) 
        {
            m_delete_camera_cb(saver.get());
        }
    }

    m_file_savers.clear();
    m_cameras.clear();

    m_next_port = m_socket.local_endpoint().port() + 1;

    m_context = Context();

    // Sleep for a while to clear out the queues
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
}

// NOTE: this is probably a stupid way to detect if we are recording or not
bool Server::is_running() const
{
    return !m_context.cameras_to_run.empty();
}

bool Server::is_recording() const
{
    return is_running() && m_context.recording;
}

void Server::handle_camera_connect(const boost::system::error_code& error,
                                   const size_t& bytes_recvd)
{
    ASSERT(!error);
    ASSERT_EQUAL(bytes_recvd, sizeof(NEW_CAMERA_REQUEST));

    // Get the camera request with the ip address and
    // send a reply with the number of a new port for the client to transmit to

    // NOTE: We don't actually do anything with the request right now...
    // The socket endpoint provides all of the information we need
    // NEW_CAMERA_REQUEST* request = (NEW_CAMERA_REQUEST*)m_buffer;

    boost::asio::ip::udp::endpoint cam_endpoint = m_socket.endpoint();

    // std::string addr_str = (boost::format("%s:%d") % cam_endpoint.address().to_string() % cam_endpoint.port()).str();
    INFO("Got a new camera request from %1%", cam_endpoint);

    // This will be unique for each camera
    uint16_t local_port = m_next_port++; 

    INFO("Assigning new camera port = %1% to camera %2%", local_port, cam_endpoint);
    camera_ptr_t cam_ptr = std::make_shared<camera_info>(m_service, cam_endpoint, local_port);
    m_cameras.push_back(cam_ptr);

    INFO("current num cameras = %1%, expected num cameras = %2%", m_cameras.size(), m_context.cameras_to_run.size());

    // We have received enough connect requests.
    if (m_cameras.size() >= m_context.cameras_to_run.size())
    {
        uint64_t start_time = get_time() + 1 * MICROS_PER_SEC; // Give each of the cameras 1 second to connect from now

        // Send back the replies
        for (auto& cam : m_cameras)
        {
            NEW_CAMERA_REPLY reply(cam->local_port(), start_time);

            // std::string addr_str = (boost::format("%1%") % cam->endpoint()).str();

            INFO("Sending reply message to %s with assigned_port=%u and start_time=%u",
                 cam->endpoint(), cam->local_port(), start_time);

            // Make sure that we are sending to the correct endpoint
            m_socket.endpoint() = cam->endpoint();

            // Send the reply syncronously
            boost::system::error_code ec;
            size_t bytes_sent = m_socket.send(&reply, sizeof(NEW_CAMERA_REPLY), ec);
            ASSERT(!ec);
            ASSERT_EQUAL(bytes_sent, sizeof(NEW_CAMERA_REPLY));

            cam->handle_camera_reply(ec, bytes_sent);

            // Append port number to the file_prefix and add the FILE_EXTENSION
            file_saver::context context;
            context.file_prefix = (boost::format("%1%/%2%.%3%") %
                                   m_context.save_directory %
                                   cam->endpoint().address() %
                                   DEFAULT_FILE_EXTENSION).str();
            INFO("Saving to %s", context.file_prefix);
            context.recording = m_context.recording;

            file_saver_ptr_t saver(new file_saver(m_service, context, SAVE_INTERVAL));

            m_file_savers.push_back(saver);

            // Let the saver know when it is time to save saving...
            cam->inform_on_file_header(boost::bind(&file_saver::run, saver, _1));

            // If there is a new camera cb, use it
            if (m_new_camera_cb)
            {
                m_new_camera_cb(saver.get());
            }
        }

    } else
    {
        // Listen for a camera connect message
        m_socket.async_receive(m_buffer, MAX_BUFFER_SIZE,
                               boost::bind(&Server::handle_camera_connect, this,
                                           boost::asio::placeholders::error,
                                           boost::asio::placeholders::bytes_transferred));
    }
}

}; // namespace holo3d
