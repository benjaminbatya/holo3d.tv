#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#include <Qt/QtGui>

#include "ui_Controller.h"

namespace holo3d
{

class Window;
class Server;
class file_saver;

/**
 * This is a Qt Controller for HoloRecorder
 * 
 * @author benjamin (3/3/2015)
 * @param  
 */
class Controller : public QMainWindow, private Ui_Controller
{
    Q_OBJECT
public:
    Controller(Server* server, QWidget* parent=0);
    ~Controller();

    void show();

protected slots:
    void build_window(void* ptr);
    void toggle_recording();

    void quit();
    void load_settings(bool use_last = false);
    void save_settings(bool use_last = false);

protected:

    void populate_controls();

    void stop_recording();

    bool eventFilter(QObject* obj, QEvent* event);

    Server* m_server = nullptr;
    
    typedef std::vector<Window*> window_vec_t;
    window_vec_t    m_windows;
};

}; // namespace holo3d

#endif // __CONTROLLER_H__
