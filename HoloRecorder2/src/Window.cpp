#include "pch_files.hpp"

#include "Window.h"

#include "server.hpp"
#include "util_funcs.hpp"
#include "fps_counter.hpp"
#include "decoder.hpp"

namespace holo3d
{

#define DEFAULT_DISPLAY_MODE	DISPLAY_MODE_DEPTH

#define TEXTURE_SIZE    512

#define MIN_NUM_CHUNKS(data_size, chunk_size) ((((data_size) - 1) / (chunk_size) + 1))
#define MIN_CHUNKS_SIZE(data_size, chunk_size) (MIN_NUM_CHUNKS(data_size, chunk_size) * (chunk_size))

Window::Window(const char* window_name, file_saver* saver, QWidget* parent)
: QWidget(parent)
, m_saver(saver)
{
    setWindowTitle(window_name);

    m_painter = new QPainter();

    saver->set_init_cb([&](file_saver* saver) { this->saver_init(saver);  });
    saver->set_new_frame_cb([&](camera_frame& frame) { this->process_frame(frame); });
}

Window::~Window()
{
    delete m_color_texmap;
    m_color_texmap = nullptr;

    delete m_depth_texmap;
    m_depth_texmap = nullptr;

    delete m_painter;
    m_painter = nullptr;

    m_saver = nullptr;
}

void Window::show()
{
    QWidget::show();
}

void Window::saver_init(file_saver* saver)
{
//  INFO("Called");
    auto cam = saver->camera().lock();
    if (!cam)
    {
        INFO("Failed to lock the camera pointer!!");
    }

    setWindowTitle(cam->description().c_str());
        
    m_width = cam->header().width;
    m_height = cam->header().height;

    // Texture map init
    m_texmap_x = MIN_CHUNKS_SIZE(m_width, TEXTURE_SIZE);
    m_texmap_y = MIN_CHUNKS_SIZE(m_height, TEXTURE_SIZE);
    m_color_texmap = new RGB888Pixel[m_texmap_x * m_texmap_y];
    ASSERT(m_color_texmap);
    m_depth_texmap = new RGB888Pixel[m_texmap_x * m_texmap_y];
    ASSERT(m_depth_texmap);

    decoder::factory factory(m_width, m_height, m_texmap_x);
    m_color_decoder = factory.color(cam->header().color_format);
    m_depth_decoder = factory.depth(cam->header().depth_format);

    INFO("playing '%1%' at %2%x%3%, color format=%4%, depth format=%5%",
         cam->description(), m_width, m_height,
         cam->header().color_format,
         cam->header().depth_format);        

}

void Window::process_frame(camera_frame& frame)
{
//  INFO("Called");

    // Try to prevent crashes
    if (m_saver == nullptr) 
    {
        return;
    }

    if (!m_paused && isVisible()) 
    {
        // Process the frame here

        //      // NOTE: Can we do this the slot of the emitted new_frame signal??
        // INFO("TestViewer::display_blended: m_color_frame.isValid()=%d", m_color_frame.isValid());
        {
            // Make sure non of the state can change while decoding
            QMutexLocker locker(&m_mutex);

            // Draw the color buffer
            m_color_decoder->decode(m_color_texmap, (uint8_t*)frame.color_buffer(), sizeof(RGB888Pixel) * m_texmap_x * m_texmap_y, frame.color_buffer_size());

            // Draw out the depth buffer
            m_depth_decoder->decode(m_depth_texmap, (uint8_t*)frame.depth_buffer(), sizeof(RGB888Pixel) * m_texmap_x * m_texmap_y, frame.depth_buffer_size());
        }

        QImage color_image = QImage((uint8_t*)m_color_texmap, frame.width(), frame.height(), m_texmap_x*sizeof(RGB888Pixel), QImage::Format_RGB888);
        m_color_pixmap = QPixmap::fromImage(color_image);

        if (m_depth_decoder->format() != STREAM_FORMAT::INVALID) 
        {
            QImage depth_image = QImage((uint8_t *)m_depth_texmap, frame.width(), frame.height(), m_texmap_x * sizeof(RGB888Pixel), QImage::Format_RGB888); 
            m_depth_pixmap = QPixmap::fromImage(depth_image);
        }
        
        // NOTE: ONLY update() maybe called here. NOT repaint()!
        // Because process_frame will be called from a different thread!!
        update();
    }
}

void Window::paintEvent(QPaintEvent* e)
{
    if (m_color_pixmap.isNull()) 
    {
        return;
    }

    //  INFO("Called");
    double scaleX = ((double)width()) / m_color_pixmap.width();
    double scaleY = ((double)height()) / m_color_pixmap.height();
    double scale = std::min(scaleX, scaleY);

    m_painter->begin(this);
    m_painter->save();
    m_painter->scale(scale, scale);
    
    if (m_depth_decoder->format() != STREAM_FORMAT::INVALID) 
    {
        switch (m_view_state)
        {
        case DisplayModes::BLEND:
            m_painter->drawPixmap(0, 0, m_color_pixmap);
            m_painter->setCompositionMode(QPainter::CompositionMode_Multiply);
            m_painter->drawPixmap(0, 0, m_depth_pixmap);
            break;
        case DisplayModes::COLOR:
            m_painter->drawPixmap(0, 0, m_color_pixmap);
            break;
        case DisplayModes::DEPTH:
            m_painter->drawPixmap(0, 0, m_depth_pixmap);
            break;
        default:
            THROW("INVALID DisplayMode");
            break;
        }
    } else
    {
        m_painter->drawPixmap(0, 0, m_color_pixmap);
    }

    m_painter->restore();
    m_painter->end();

    static fps_counter fps("paintEvent");
    fps.update();
}

bool Window::use_histogram() const
{ 
    return m_depth_decoder->use_histogram(); 
}

void Window::set_use_histogram(bool f) 
{ 
    QMutexLocker locker(&m_mutex);
    m_depth_decoder->use_histogram(f);
}

void Window::keyReleaseEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Escape: // esc key
        close();
        break;

    case Qt::Key_1:
        m_view_state = DisplayModes::BLEND;
        update();
        break;

    case Qt::Key_2:
        m_view_state = DisplayModes::COLOR;
        update();
        break;

    case Qt::Key_3:
        m_view_state = DisplayModes::DEPTH;
        update();
        break;

    case Qt::Key_P:
        m_paused = !m_paused;
        break;

    case Qt::Key_H:
        set_use_histogram(!use_histogram());
        break;

//  case Qt::Key_M:
//      m_stream_manager.toggle_mirroring();
//      break;

    default:
        QWidget::keyReleaseEvent(event);
        break;
    }
}

}; // namespace holo3d


