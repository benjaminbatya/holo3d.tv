#ifndef _SERVER_H_
#define _SERVER_H_

#include "fragmenting_socket.hpp"
#include "camera_info.hpp"
#include "file_saver.hpp"
#include "control_server.hpp"

namespace holo3d
{

class Server  // : private boost::noncopyable
{
public:
    Server(const std::string& listening_port);
    ~Server();

    void set_done(bool f);      // << call this when you want the Server to shutdown

    typedef std::shared_ptr<file_saver> file_saver_ptr_t;
    typedef std::function<void(file_saver*)> camera_cb_t;

    void set_new_camera_cb(camera_cb_t cb) { m_new_camera_cb = cb; }
    void set_delete_camera_cb(camera_cb_t cb) { m_delete_camera_cb = cb; }

    typedef std::vector<std::string> string_vec;

    string_vec found_cameras();

    struct Context
    {
        string_vec          cameras_to_run { }; // << List of cameras to run
        protocol::StartData camera_config { };  // << The camera configuration info
        std::string         save_directory { }; // << The directory to save the recordings to
        bool                recording { };      // << Indicates whether the file_savers should record or not
    };

    // Causes the server to start recording based on the context
    void start_recording(const Context& context);

    // This cancels all of the file_savers and cleans up
    void stop_recording();

    bool is_running() const;
    bool is_recording() const;

protected:

    void handle_camera_connect(const boost::system::error_code& error,
                               const size_t& bytes_recvd);

    boost::asio::io_service         m_service;          // << The service to use
    fragmenting_socket              m_socket;           // << The socket that'll receive messages from the cameras

    boost::thread_group             m_threads;          // << Threads spawned by the server

    boost::asio::io_service::work   m_work;             // << The work object ensures that the service doesn't quit when there is work to be done

    Context                         m_context;          // << The context for this server
    
    typedef std::vector<file_saver_ptr_t> file_saver_vec_t;
    file_saver_vec_t                m_file_savers;      // << Saves to file one of the camera streams

    // NOTE: replace this with fragmenting_socket::sidecar_buffer!!
    static const size_t             MAX_BUFFER_SIZE = 0xffff;
    uint8_t                         m_buffer[MAX_BUFFER_SIZE];  // << This is the temporary buffer for all incoming data

    typedef std::shared_ptr<camera_info> camera_ptr_t;
    // typedef camera_info* camera_ptr_t;
    typedef std::vector<camera_ptr_t> camera_vec_t;
    camera_vec_t                    m_cameras;          // << A mapping of ip addresses to corresponding camera information. Not really needed..
                                                       
    uint16_t                        m_next_port;        // << The next port to assign a camera to transmit it's frames to
    uint64_t                        m_start_time = 0;   // << Time the cameras should start grabbing frames at

    camera_cb_t                     m_new_camera_cb;    // The callback to call when a new camera is created
    camera_cb_t                     m_delete_camera_cb; // Callback when a camera is deleted

    ControlServer                   m_control_server;
};


}; // namespace holo3d

#endif // _SERVER_H_
