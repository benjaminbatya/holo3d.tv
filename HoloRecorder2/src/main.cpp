// Main.cpp for HoloRecorder
#include "pch_files.hpp"

#include <stdio.h>
#include <cstring>
#include <thread>

#include <Qt/QtGui>

namespace po = boost::program_options;
namespace fs = boost::filesystem;

#include "util_funcs.hpp"

#include "server.hpp"
#include "Controller.hpp"

void display_keys();

int main(int argc, char **argv)
{    
    boost::system::error_code ec;

    try
    {
        std::string listening_port = holo3d::DEFAULT_PORT;
        std::string app_name = fs::basename(argv[0]);
        po::options_description visible(std::string("Usage: ") + app_name + " <options> \nAllowed Options");
        visible.add_options()
        ("help,h",                                                                                          "display this help message")
        ("port,p",              po::value<std::string>(&listening_port)->default_value(listening_port),     "The port to start listening for connection on")
        ;

        po::options_description all;
        all.add(visible);

        po::variables_map vm;
        try
        {
            po::store(po::command_line_parser(argc, argv).options(all).run(), vm);

            if (vm.count("help"))
            {
                INFO_CPP(visible);
                return 0;
            }

            po::notify(vm);
        }
        catch (po::error& e)
        {
            ERROR_CPP(e.what());
            INFO_CPP(visible);
            return -1;
        }

//      boost::filesystem::path path(file_prefix);
//
//      // Make sure the file is always saved in the current directory.
//      // OpenNI2 has the annoying habit of redirecting the file to its Drivers directory
//      path = boost::filesystem::absolute(path);
//      boost::filesystem::create_directories(path);
//
//      path = boost::filesystem::canonical(path);
//
//      INFO("Saving streams into %1%", path.string());
//
//      holo3d::file_saver::context context;
//      context.file_prefix = path.string();

        holo3d::Server server(listening_port); 

        QApplication app(argc, argv);

        holo3d::Controller controller(&server);

        controller.show();

        display_keys();

        return app.exec();

    } catch (std::exception &e)
    {
        ERROR_CPP(std::endl << e.what());
        return -1;
    } 
}


void display_keys()
{
    INFO("Keys:");
    INFO("ESC - close window");
    INFO("1 - Display depth frames blended over color frames");
    INFO("2 - Display only color frames");
    INFO("3 - Display only depth frames");
    INFO("p - Pause/Unpause the movie");
    INFO("l - loop/don't loop the movie");
    INFO("h - toggle display histogram");
}
