#include "pch_files.hpp"

#include "util_funcs.hpp"

#include "camera_info.hpp"
#include "file_saver.hpp"

using boost::asio::ip::udp;

namespace holo3d
{

file_saver::file_saver(boost::asio::io_service &service, const context& save_context, size_t interval)
: m_timer(service, boost::posix_time::milliseconds(interval))
, m_context(save_context)
{
}

file_saver::~file_saver()
{
    m_timer.cancel();

    INFO("camera %1%, processed %2% frames", m_camera_address, m_count); 
    if (recording()) 
    {
        fclose(m_file); // follow RAII semantics
        m_file = nullptr;
    }
}

void file_saver::run(camera_info::ptr_t weak_cam)
{   
    m_camera = weak_cam;

    INFO("file prefix = %s", m_context.file_prefix);

    // Make the setup occur asycronously as soon as possible
    m_timer.async_wait(boost::bind(&file_saver::setup, this, boost::asio::placeholders::error));
}

void file_saver::setup(const boost::system::error_code& error)
{
    ASSERT_THROW(!error, error.message());

    std::shared_ptr<camera_info> cam = m_camera.lock();
    ASSERT(cam); // just assume that it won't expire now...

    STREAM_HEADER file_header = cam->header();
    INFO("camera %1%, file_header = %2%", cam->description(), file_header);

    if (recording()) 
    {
        // Open the file for writing
        m_file = fopen(m_context.file_prefix.c_str(), "wb");
        ASSERT_THROW(m_file, "failed to open '%s' for writing", m_context.file_prefix);

        size_t ret = fwrite(&file_header, sizeof(STREAM_HEADER), 1, m_file);
        ASSERT_EQUAL(ret, 1);
    }

    std::string name = "file_saver: ";
    name += cam->description();

    m_fps.name(name);

    m_count = 0;

    if (m_init_cb) 
    {
        m_init_cb(this);
    }

    // Always call save_frame whenever a new frame received by the camera_info
    cam->set_new_frame_cb([&] (camera_info::ptr_t cam) { this->save_frame(); });
}

void file_saver::save_frame()
{
    std::shared_ptr<camera_info> cam = m_camera.lock();
    if (!cam) 
    {
        INFO("camera_info closed. returning");
        return;
    }

    camera_frame frame;
    bool has_frame = cam->pop_frame(frame);
    if (has_frame) // save if there is a frame
    {
        FRAME_HEADER frame_header = frame.frame_header(); 

        if (recording()) 
        {
            size_t ret = fwrite(&frame_header, 1, sizeof(FRAME_HEADER), m_file);
            ASSERT_EQUAL(ret, sizeof(FRAME_HEADER));

            ret = fwrite(frame.color_buffer(), 1, frame.color_buffer_size(), m_file);
            ASSERT_EQUAL(ret, frame.color_buffer_size());

            ret = fwrite(frame.depth_buffer(), 1, frame.depth_buffer_size(), m_file);
            ASSERT_EQUAL(ret, frame.depth_buffer_size());
        }

        if (m_new_frame_cb) 
        {
            m_new_frame_cb(frame);

        }

        // update the fps counter
        m_fps.update();

        m_count++;
    }
}

}; // namespace holo3d
