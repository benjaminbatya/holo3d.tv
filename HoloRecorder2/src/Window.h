// Window.h - Class containing the glut functionality

#ifndef _WINDOW_H_
#define _WINDOW_H_

#include <memory>

#include <Qt/QtGui>

#include "h3t_file.hpp"

namespace holo3d
{

// Forward decl
class file_saver;
class camera_frame;

namespace decoder
{
class base;
namespace depth { class base; }

typedef std::shared_ptr<base> color_decoder_t;
typedef std::shared_ptr<depth::base> depth_decoder_t;
};
// END forward decl

enum class DisplayModes
{
    BLEND = 1,
    COLOR = 2,
    DEPTH = 3,

    // These are to allow iterating over the enums
    MIN = BLEND,
    MAX = DEPTH,
    COUNT,
};

inline std::ostream& operator<<(std::ostream& os, const DisplayModes& mode)
{
#define OUTPUT_MODE(e) case DisplayModes::e : os << #e; break;

    switch (mode)
    {
    OUTPUT_MODE(DEPTH);
    OUTPUT_MODE(COLOR);
    OUTPUT_MODE(BLEND);
    default: os << "Unknown display mode"; break;
    }
#undef OUTPUT_MODE

    return os;
}
inline std::istream& operator>>(std::istream& is, DisplayModes& mode)
{
    std::string input;
    is >> input;

#define INPUT_MODE(m) if (input == #m) { mode = DisplayModes::m; }

    INPUT_MODE(DEPTH)
    else INPUT_MODE(COLOR)
    else INPUT_MODE(BLEND)
    else
    {
        throw ("Unknown display mode");
    }
#undef INPUT_MODE

    return is;
}

class Window : public QWidget
{
    Q_OBJECT

public:
    Window(const char* window_name, file_saver* saver, QWidget* parent = 0);
    ~Window(); // Window is not intended to be inherited from

    void show();

    /**
     * Setter/getter combo for m_use_histogram. Call this as 
     * window->use_histogram(true) or window->use_histogram()
     *  
     * @author benjamin (11/10/2014)
     * 
     * @param use sets m_use_histogram
     * 
     * @return bool the value of m_use_histogram
     */
    bool use_histogram() const;
    void set_use_histogram(bool f);
    DisplayModes video_mode() const { return m_view_state; }
    void set_video_mode(const DisplayModes& mode) { m_view_state = mode; }

    file_saver* saver() const { return m_saver; }

protected:

    void saver_init(file_saver*);
    void process_frame(camera_frame& frame);

    void paintEvent(QPaintEvent* e);

    void keyReleaseEvent(QKeyEvent *event);

    file_saver*     m_saver = nullptr;

    QPainter*       m_painter = nullptr;

    DisplayModes    m_view_state = DisplayModes::BLEND;

    QPixmap         m_color_pixmap;
    QPixmap         m_depth_pixmap;

    bool            m_paused = false;

    bool            m_use_histogram = false;

    size_t          m_texmap_x = 0;
    size_t          m_texmap_y = 0;
    RGB888Pixel*    m_color_texmap = nullptr;
    RGB888Pixel*    m_depth_texmap = nullptr;

    size_t          m_width = 0;
    size_t          m_height = 0;

    decoder::color_decoder_t m_color_decoder;
    decoder::depth_decoder_t m_depth_decoder;

    QMutex          m_mutex;
};

}; // namespace holo3d
   
#endif // _WINDOW_H_
