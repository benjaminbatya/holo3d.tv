#ifndef _FILE_SAVER_H_
#define _FILE_SAVER_H_

#include "fragmenting_socket.hpp"
#include "fps_counter.hpp"

namespace holo3d
{

class camera_info;

// This will save a h3t stream to file
class file_saver
{
public:

    struct context
    {

        std::string file_prefix = "";
        bool        recording = false;
    };

    file_saver(const file_saver& that);
    file_saver(boost::asio::io_service& service, const context& context, size_t interval);
    ~file_saver();

    void run(camera_info::ptr_t cam);

    typedef std::function<void(file_saver*)> saver_init_cb_t;
    void set_init_cb(saver_init_cb_t cb) { m_init_cb = cb; }

    typedef std::function<void(camera_frame&)> new_frame_cb_t;
    void set_new_frame_cb(new_frame_cb_t cb) { m_new_frame_cb = cb; }

    void set_recording(bool f) { m_context.recording = f; }
    bool recording() const { return m_context.recording; }

    camera_info::ptr_t camera() const { return m_camera; }

protected:

    void setup(const boost::system::error_code& error);

    void save_frame();

//  void send_data(const void* data, const size_t& amount_to_send);
//  void receive_data(void* data, const size_t& amount_to_recv);

    boost::asio::deadline_timer         m_timer;                // << timer object
    context                             m_context;              // << context in which to operate in
                                                
    FILE*                               m_file = nullptr;       // << Used to save the data to file

    camera_info::ptr_t                  m_camera;               // << weak pointer to the camera_info object

    std::string                         m_camera_address = "unknown"; // << a string description of the camera

    fps_counter                         m_fps { "file_saver" };

    size_t                              m_count = 0;

    saver_init_cb_t                     m_init_cb;              // << Invoked when the file_saver is initialized with the frame header

    new_frame_cb_t                      m_new_frame_cb;         // << The callback to invoke when a new frame is received

};

} // namespace holo3d

#endif // _FILE_SAVER_H_
