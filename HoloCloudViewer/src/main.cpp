// Main.cpp for testViewer that displays the OpenNI2 color+depth video streams in 3d or flat
#include "pch_files.hpp"


namespace po = boost::program_options;
namespace fs = boost::filesystem;

#include <stdio.h>
#include <iostream>
// #include <algorithm>

#include "util_funcs.hpp"
#include "loader.hpp"
#include "Window.hpp"

using namespace holo3d;

int main(int argc, char **argv)
{
    try
    {
        // This specifies the directory where h3t streams are
        std::string movie_path_name;
        bool loop_movie = false;

        std::string app_name = boost::filesystem::basename(argv[0]);
        po::options_description visible(std::string("Usage: ") + app_name + " <options> movie_directory\nAllowed Options");
        visible.add_options()
        ("help,h",                                                                      "produce help message")
        ("loop,l",      po::value<bool>(&loop_movie)->default_value(loop_movie),        "loop the movie")    
        ;

        po::options_description hidden(std::string("Hidden options for ") + app_name);
        hidden.add_options()
        ("movie_path", po::value<std::string>(&movie_path_name)->required(),                          "The directory to find all of the movies to play")
        ;

        po::options_description all;
        all.add(visible).add(hidden);

        po::positional_options_description required;
        required.add("movie_path", 1);

        po::variables_map vm;
        try
        {
            po::store(po::command_line_parser(argc, argv).options(all).positional(required).run(), vm);

            if (vm.count("help"))
            {
                INFO_CPP(visible);
                return 0;
            }

            po::notify(vm);
        }
        catch(boost::program_options::required_option& e) 
        { 
            ERROR_CPP("No path to movie directory specified!");
            INFO_CPP(visible);
            return -1; 
        } 
        catch (po::error& e)
        {
            ERROR_CPP(e.what());
            INFO_CPP(visible);
            return -1;
        }

        ASSERT_ALWAYS(!movie_path_name.empty());

        // Load up each of the h3t files in the movie path (they should be numbered 0-n)

        boost::system::error_code ec;

        fs::path movie_path(movie_path_name);

        if (!fs::is_directory(movie_path))
        {
            THROW("%s is not a directory", movie_path_name);
        }

//      INFO("PATH = %1%", movie_path);

        std::vector<fs::path> valid_files;

        for(fs::directory_iterator it(movie_path), eod; it != eod; it++)
        {
            fs::path path = it->path();
            if (fs::is_symlink(path))
            {
                // INFO("symlink %1%", path);
                fs::path resolved_path = boost::filesystem::read_symlink(path, ec);
                ASSERT(ec == 0);
                path = boost::filesystem::canonical(resolved_path, path.parent_path());
                // INFO("resolved to %1%", path);
            }

            if (fs::is_regular_file(path))
            {
                // INFO("Got %1%", path);

                // Ignore any video whose extension is not .h3t
                if (path.extension().string().substr(1) != holo3d::DEFAULT_FILE_EXTENSION)
                {
                    ERROR("Ignoring non holo3d file '%1%' with extension '%2%', wanted '%3%'", 
                          path, it->path().extension().string().substr(1), holo3d::DEFAULT_FILE_EXTENSION);
                    continue;
                }

                path = boost::filesystem::absolute(path);

                valid_files.push_back(path);
            }
        }

        std::sort(valid_files.begin(), valid_files.end(), std::less<fs::path>());

        if (valid_files.empty())
        {
            ERROR("No valid video files found in %1%!", movie_path_name);
            INFO_CPP(visible);
            return -1;
        }
        Window::loader_vec_t loaders;

        for (const fs::path& path : valid_files)
        {
            INFO("Loading '%1%'", path);

            Window::loader_t loader = std::make_shared<holo3d::file_loader>(path.string());
            loader->run();

            // Only keep files with the color and depth enabled
            if (loader->file_header().has_color() && loader->file_header().has_depth()) 
            {
                loaders.push_back(loader);
            }
        }

        Window viewer("H3T File Viewer", loaders);

        // viewer.loop_movie(loop_movie);

        // never returns from run
        viewer.run(argc, argv);

    } catch (std::exception &e) 
    {
        ERROR_CPP(std::endl << e.what());
        return -1;
    } 
}
