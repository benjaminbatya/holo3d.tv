// Window.h - Class containing the glut functionality

#ifndef _WINDOW_H_
#define _WINDOW_H_

#include "frame.hpp"
#include "decoder.hpp"
#include "loader.hpp"

namespace holo3d
{

enum MouseMode
{
    MODE_NONE = 0,
    MODE_TRANSLATE,
    MODE_ROTATE,
    MODE_SCALE
};

class Window
{
public:

    typedef std::shared_ptr<holo3d::loader_base>    loader_t;
    typedef std::vector<loader_t>                   loader_vec_t;

    Window(const char* window_name, loader_vec_t& loaders);
    virtual ~Window();

    void run(int argc, char** argv); // Does not return

    void display();
    void on_key(unsigned char key, int x, int y);
    void on_reshape(int width, int height);
    void on_motion(int x, int y);
    void on_mouse_action(int button, int state, int x, int y);

private:
    Window(const Window&);
    Window& operator=(const Window&);

    void init_opengl(int argc, char** argv);

    void reset_transforms();
    void recalc_eye_pos();

    void delay_display();

    loader_vec_t    m_loaders;

    decoder::color_decoder_t m_color_decoder = nullptr;
    decoder::depth_decoder_t m_depth_decoder = nullptr;

    // The frame to get from the current camera
    camera_frame m_frame;

    // Buffers to hold the decoded data
    RGB888Pixel* m_color_buffer;
    DepthPixel* m_depth_buffer;

    // Glut window identifier
    int m_window; 

    std::string         m_window_name;

    int                 m_stream_width;
    int                 m_stream_height;

    int                 m_mouse_x, m_mouse_y;

    float               m_zoom;

    int                 m_angle_x, m_angle_y;
    int                 m_trans_x, m_trans_y;

    bool                m_color;

    MouseMode           m_mode;

    bool                m_paused = false;
    bool                m_last_frame = false;
    bool                m_loop_movie = false;

    size_t              m_width, m_height, m_num_pixels;
    float               m_center_x, m_center_y;

    uint64_t        m_prev_frame_timestamp = 0;
    uint64_t        m_prev_real_time = 0;
};

}; // namespace holo3d

#endif // _WINDOW_H_
