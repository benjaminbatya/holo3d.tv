// Implementation of Window
#include "pch_files.hpp"

#include <GL/glut.h>

#include <chrono>
#include <thread>

#include <iostream>

// #include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Geometry>
// #include <Eigen/OpenGLSupport>

#include "util_funcs.hpp"

#include <math.h>

#include "Window.hpp"

const float DEG_TO_RAD = M_PI / 180;

#define GL_WIN_SIZE_X   640
#define GL_WIN_SIZE_Y   480
#define GL_Z_NEAR       90.0
#define GL_Z_FAR        11000.0
#define GL_FOVY         50.0
#define ZOOM_AMOUNT     1.2f

#define MAX_DEPTH       10000

namespace holo3d
{

// Start statics
static Window* g_self = NULL;

static Eigen::Vector3f g_eye_position;

static void glut_idle()
{
    glutPostRedisplay();
}

static void glut_display()
{
    g_self->display();
}

static void glut_keyboard(unsigned char key, int x, int y)
{
    g_self->on_key(key,x,y);
}

static void glut_reshape(int width, int height)
{
    g_self->on_reshape(width, height);
}

static void glut_motion(int x, int y)
{
    g_self->on_motion(x, y);
}

static void glut_mouse_action(int button, int state, int x, int y)
{
    g_self->on_mouse_action(button, state, x, y);
}

// End statics

// Start public

Window::Window(const char* window_name, loader_vec_t& loaders)
: m_loaders(loaders)
, m_window(0)   
, m_window_name(window_name)
, m_stream_width(0), m_stream_height(0) 
, m_mouse_x(-1), m_mouse_y(-1)
, m_zoom(1)
, m_angle_x(0),  m_angle_y(0)
, m_trans_x(0),  m_trans_y(0)
, m_color(true)
, m_mode(MODE_NONE)
{
    g_self = this;

    reset_transforms();

    // NOTE: for now we are assuming that all of the streams' formats are the same
    const STREAM_HEADER& header = m_loaders.at(0)->file_header();

    m_width = header.width;
    m_height = header.height;
    m_num_pixels = m_width * m_height;

    m_color_buffer = new RGB888Pixel[m_num_pixels];
    m_depth_buffer = new DepthPixel[m_num_pixels];

    decoder::factory factory = decoder::factory(m_width, m_height, m_width);

    m_color_decoder = factory.color(header.color_format);
    m_depth_decoder = factory.depth(header.depth_format, false);

    m_center_x = (m_width - 1)  / 2.f;
    m_center_y = (m_height - 1) / 2.f;
}

Window::~Window()
{
    delete [] m_depth_buffer;
    delete [] m_color_buffer;

    g_self = NULL;
}

void Window::run(int argc, char** argv) // Does not return
{
    init_opengl(argc, argv);

    glutMainLoop();
}

void Window::delay_display()
{
    int64_t frame_diff = m_frame.color_time_stamp() - m_prev_frame_timestamp;
    int64_t curr_time = get_time();
    int64_t real_diff = curr_time - m_prev_real_time;
    int64_t drift = real_diff - frame_diff;
//  INFO("Window::display: frame %d: ts=%d, frame_diff=%d, real_diff=%d, drift=%d",
//       m_frame.frame_number(), (int32_t)m_frame.time_stamp(),
//       (int32_t)frame_diff, (int32_t)real_diff,
//       (int32_t)drift);

    // INFO("prev timestamp = %u, time to sleep = %u", (uint32_t)g_prev_ts, (uint32_t)time_to_sleep);

    m_prev_frame_timestamp = m_frame.color_time_stamp();
    m_prev_real_time = curr_time;
    if (frame_diff < MICROS_PER_SEC)
    {
        // Sleep this thread until we need to play the next frame.
        // NOTE: This isn't accurate but I dont care about that...
        // Would have to use get_time() to
        std::this_thread::sleep_for(std::chrono::microseconds(frame_diff - drift));
    }
}

void Window::display()
{
    if (m_last_frame && m_loop_movie)
    {
        m_loaders.at(0)->run();
        m_last_frame = false;
    }

    if (!(m_paused || m_last_frame))
    {
        // get a frame from each of the loaders
        bool ret = m_loaders.at(0)->next_frame(m_frame);
   
        if(!ret)
        {
            INFO("reached the last frame");
            m_last_frame = true;
            return;
        }

        ASSERT_ALWAYS(m_frame.valid());

        delay_display();

        size_t color_size = sizeof(RGB888Pixel) * m_num_pixels;
        m_color_decoder->decode(m_color_buffer, m_frame.color_buffer(), color_size, m_frame.color_buffer_size() );

        size_t depth_size = sizeof(DepthPixel) * m_num_pixels;
        m_depth_decoder->decode_depth(m_depth_buffer, m_frame.depth_buffer(), depth_size, m_frame.depth_buffer_size());
    }
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glPointSize(1.0f);

    glBegin(GL_POINTS);
    {
        if (!m_color)
        {
            glColor3ub(255, 255, 255);
        }

        const RGB888Pixel* color_buf = m_color_buffer;
        const DepthPixel*  depth_buf = m_depth_buffer;

        for (size_t i=0; i<m_num_pixels; ++i, ++color_buf, ++depth_buf)
        {
            if (*depth_buf == 0)
            {
                continue;
            }

            if (m_color)
            {
                // const openni::RGB888Pixel* color_pixel = ((openni::RGB888Pixel*)m_color_frame.getData())[i];
                glColor3ub(color_buf->r,     // R
                           color_buf->g,     // G
                           color_buf->b);    // B
            }

            float f = 1000.f;

            // Convert from image plane coordinates to world coordinates                          
            float z = *depth_buf * f / MAX_DEPTH;             // Z = d

            // z = 1.0f / z;
            float x = (i % m_width - m_center_x) * z / f;  // X = (x - cx) * d / f
            float y = (i / m_height - m_center_y) * z / f;  // Y = (y - cy) * d / f

            // Try to bang z into somthing reasonable
            // convert from the min/max z to a z value between -256 and 256
//          float max_z = 650.f, min_z = 46.f;
//          float target_min = 256.f;
//          float target_max = -256.f;
//          float slope = (target_max - target_min) / (max_z - min_z);
//          float intersept = target_min - min_z * slope;
            // z = z * slope + intersept;
            // z = z * -512 / 604 + 295;

            glVertex3f( x, y, z);   
        }
    }
    glEnd();


    // Draw a world coordinate frame
    glLineWidth(2.f);
    glBegin(GL_LINES);
    {
        glColor3ub(255, 0, 0); // X-axis
        glVertex3f(0, 0, 0);
        glVertex3f(50, 0, 0);

        glColor3ub(0, 255, 0); // Y-axis
        glVertex3f(0, 0, 0);
        glVertex3f(0, 50, 0);

        glColor3ub(0, 0, 255); // Z-axis
        glVertex3f(0, 0, 0);
        glVertex3f(0, 0, 50);
    }
    glEnd();

    // Place the camera
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();


    // glScalef(m_zoom, m_zoom, 1);
//  gluLookAt(-7*m_angle_x, -7*m_angle_y, -1000.0,
//            0.0,           0.0,          2000.0,
//            0.0,          -1.0,          0.0);


    gluLookAt(g_eye_position(0), g_eye_position(1), g_eye_position(2),
              0.0,         0.0,          0.0,
              0.0,          -1.0,          0.0);

    glTranslatef(m_trans_x, 0.0, m_trans_y);
    glScalef(m_zoom, m_zoom, m_zoom);

    glutSwapBuffers();
}

unsigned char convert_to_lower_case(unsigned char key)
{
    if ('A' <= key && key <= 'Z')
    {
        key -= 'A';
        key += 'a';
    }

    return key;
}


void Window::on_key(unsigned char key, int x, int y)
{
    key = convert_to_lower_case(key);

    switch (key)
    {
    case 27: // esc key
    case 'q':
        glutDestroyWindow(m_window);
        // NOTE: this is the only way to get out of glutMainLoop() in this version of glut
        exit(0); 
        break;

    case 'c':
        m_color = !m_color;
        break;

    case 'r':
        reset_transforms();
        break;

    case 'p':
        m_paused = !m_paused;
        break;

    case 'l':
        m_loop_movie = !m_loop_movie;
        break;

    default:
        break;
    }
}

void print_info()
{
    INFO("Available Controls:");
    INFO("===================");
    INFO("Rotate            :   Mouse Left Button");
    INFO("Zoom              :   Mouse Wheel");
    INFO("Toggle Color      :   C");
    INFO("Reset Transforms  :   R");
    INFO("Quit              :   Q or Esc\n");
}

void Window::on_reshape(int width, int height)
{
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(GL_FOVY, (float)width / height, GL_Z_NEAR, GL_Z_FAR);

    glMatrixMode(GL_MODELVIEW);
}

void Window::on_motion(int x, int y)
{
    switch (m_mode)
    {
    case MODE_ROTATE:
        m_angle_x += x - m_mouse_x;
        m_angle_y += y - m_mouse_y;
        break;
    case MODE_TRANSLATE:
        m_trans_x += x - m_mouse_x;
        m_trans_y += y - m_mouse_y;
        break;
    default:
        return; // do nothing
        break;
    }
    m_mouse_x = x;
    m_mouse_y = y;

    recalc_eye_pos();
}


void Window::reset_transforms()
{
    m_angle_x = -157;
    m_angle_y = 18;
    m_zoom = 3.5;
    m_trans_x = m_trans_y = 0;
    m_mouse_x = m_mouse_y = -1;
    recalc_eye_pos();
}

void Window::recalc_eye_pos()
{
    // INFO("Window::recalc_eye_pos: m_angle=(%d, %d), m_mouse=(%d,%d), zoom=%f", m_angle_x, m_angle_y, m_mouse_x, m_mouse_y, m_zoom);

    float theta = m_angle_x * DEG_TO_RAD;
    float phi = m_angle_y * DEG_TO_RAD;
    float dist = 1000.0; // * m_zoom;

    // Eigen::AngleAxisf z_rot(phi, Eigen::Vector3f::UnitZ());

    Eigen::Matrix3f m; m = 
        Eigen::AngleAxisf(theta, Eigen::Vector3f::UnitY()) *
        Eigen::AngleAxisf(phi, Eigen::Vector3f::UnitX());

    g_eye_position = m * Eigen::Vector3f(0, 0, dist);
}

void Window::on_mouse_action(int button, int state, int x, int y)
{
    if (state == GLUT_DOWN)
    {
        switch (button)
        {
        case GLUT_LEFT_BUTTON:
            m_mouse_x = x;
            m_mouse_y = y;
            m_mode = MODE_ROTATE;
            break;
        case GLUT_MIDDLE_BUTTON:
            m_mouse_x = x;
            m_mouse_y = y;
            m_mode = MODE_TRANSLATE;
            break;

        case 3:
            m_zoom *= ZOOM_AMOUNT;
            break;

        case 4:
            m_zoom /= ZOOM_AMOUNT;
            break;

        default:
            break;
        }
    } else if((state == GLUT_UP && button == GLUT_LEFT_BUTTON && m_mode == MODE_ROTATE) ||
              (state == GLUT_UP && button == GLUT_MIDDLE_BUTTON && m_mode == MODE_TRANSLATE) )
    {
        m_mouse_x = -1;
        m_mouse_y = -1;
        m_mode = MODE_NONE;
    }
}

void Window::init_opengl(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(GL_WIN_SIZE_X, GL_WIN_SIZE_Y);
    glutInitWindowPosition(0, 0);

    m_window = glutCreateWindow(m_window_name.c_str());
    glClearColor(0.45f, 0.45f, 0.45f, 0.0f);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.0f);

    glMatrixMode(GL_PROJECTION);
    gluPerspective(GL_FOVY, 1.0, GL_Z_NEAR, GL_Z_FAR);

    // glutSetCursor(GLUT_CURSOR_NONE);

    glutKeyboardFunc(glut_keyboard);
    glutDisplayFunc(glut_display);
    glutIdleFunc(glut_idle);
    glutReshapeFunc(glut_reshape);
    glutMotionFunc(glut_motion);
    glutMouseFunc(glut_mouse_action);

    print_info();
}

// End public


}; // namespace holo3d
