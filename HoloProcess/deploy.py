#!/usr/bin/env python
import sys
import os.path
import os
import socket

def test_camera(cam_name):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connected = None
    try:
        s.settimeout(0.1)
        s.connect((cam_name, 22))
        connected = True
    except socket.error as e:
        print "Error connecting to %s: %s" % (cam_name, e)
        connected = False
        
    s.close()
    return connected
    

def main():
    config = 'Release'
    if len(sys.argv) > 1:
        config = sys.argv[1]
    # print '%s' % config
    if(config != 'Release' and config != 'Debug' and config != 'Profile'):
        raise SyntaxError("Either 'Release', 'Debug', and 'Profile' must be specified!")

    # Only use the Arm config for now
    config = config + '-Arm'

    # Use this to deploy HoloCamera to all attached cameras
    # num_cameras = 8
    # camera_prefix = 'camera'

    camera_map = { 
        'camera1': 'xu3',
        'camera2': 'xu3',
        'camera3': 'rpi2',
        'camera4': 'xu3'
    }
    
    # Detect the number of cores
    num_cpus = int(os.popen('cat /proc/cpuinfo | grep  processor | wc -l').readline()[0])

    # Build all the possible configurations
    configurations = {}
    for camera,model in camera_map.iteritems():    
        # Rebuild the project first
        model_config = config + '-' + model

        configurations[model_config] = 1

    for model_config,c in configurations.iteritems():
        build_cmd = 'scons -j %s CFG=%s' % (num_cpus, model_config)
        print 'running "%s"' % (build_cmd)

        build_worked = os.system(build_cmd)

        if(build_worked != 0):
            return -1

    for camera,model in camera_map.iteritems():    
        # Rebuild the project first
        model_config = config + '-' + model

        # check to see if the camera is there
#           camera = camera_prefix + str(i+1)
        
        ret = test_camera(camera)
        if not ret: continue

        # Make sure that the HoloCamera process is dead
        os.system("ssh root@" + camera + " killall -5 HoloProcess")

        # Use scp to transfer the files
        print "Transferring %s files to %s" % (model_config, camera)
        os.system("scp %s/HoloProcess %s:" % (model_config, camera))
        os.system("scp $HOLO_ROOT/core_lib/%s/libholo.so %s:" % (model_config, camera))

        # restart the camera
#       os.system("ssh root@" + camera + " shutdown -r now")

        print "Not transferring any libs in $HOLO_ROOT/config/camera/libs"
    
if __name__ == "__main__":
    main()
