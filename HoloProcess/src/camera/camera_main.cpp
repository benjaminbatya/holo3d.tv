// Main.cpp for HoloCamera

#include <thread>
#include <cstring>

#include <chrono>
#include <ctime>

#include <sys/time.h>

#include <tclap/CmdLine.h>

//#include <moodycamel/readerwriterqueue.h>

#include "util_funcs.hpp"

//#include "v4l_factory.hpp"
//#include "openni_factory.hpp"

#include "common_funcs.hpp"

//#include "frame.hpp"

//#include "encoder.hpp"

#include "h3t_file.hpp"

#include "fps_counter.hpp"

#include "camera_manager.hpp"

#include "gst_funcs.hpp"

namespace holo3d
{

namespace camera
{

namespace hs = socket;

// File-wide constants
const uint16_t NTP_SYNC_DELAY = 240;

//
//// queue of messages, each message is unique
//struct QueueElem
//{
//    protocol::MESSAGE_TYPE msg_type = protocol::MESSAGE_TYPE(0);
//    std::unique_ptr<::google::protobuf::Message> msg = nullptr;   // << needs to be static_cast depending on the value of msg_type
//    std::unique_ptr<uint8_t> data_buf = nullptr;
//    size_t data_buf_size = 0;
//
//    // QueueElem takes ownership of msg and data_buf once they are passed in
//    QueueElem(protocol::MESSAGE_TYPE type = protocol::MESSAGE_TYPE(0), ::google::protobuf::Message* msg = nullptr, uint8_t* data_buf = nullptr, size_t data_buf_size = 0)
//    : msg_type(type)
//    , msg(msg)
//    , data_buf(data_buf)
//    , data_buf_size(data_buf_size)
//    {
//    }
//
//    QueueElem(QueueElem&& that)
//    : msg_type(that.msg_type)
//    , msg(std::move(that.msg))
//    , data_buf(std::move(that.data_buf))
//    , data_buf_size(that.data_buf_size)
//    {
//    }
//
//    QueueElem& operator=(QueueElem&& that)
//    {
//        msg_type = that.msg_type;
//        msg = std::move(that.msg);
//        data_buf = std::move(that.data_buf);
//        data_buf_size = that.data_buf_size;
//
//        return *this;
//    }
//};

// moodycamel's readerwriter queue doesn't have the overhead of boost::spsc
//typedef moodycamel::ReaderWriterQueue<QueueElem> MessageQueue;

// Shared state

void handle_sync();

Manager* g_manager = nullptr;

int main(int argc, char** argv)
{   
    Gst::init(argc, argv);
     
    TCLAP::CmdLine cmd("Camera Module options", ' ', "0.1", true);

    TCLAP::UnlabeledValueArg<std::string> module_arg("module", "", true, "", "The module to load.");
    cmd.add(module_arg);

    TCLAP::ValueArg<uint16_t> port_arg("p", "port", "", false, default_port_num(), "The port to listen on");
    cmd.add(port_arg);

    cmd.parse(argc,argv);

    INFO("port = {}", port_arg.getValue());
    
    // Initialization
    // NOTE: g_manager must be initialized first
    g_manager = new Manager(port_arg.getValue());
            
    std::thread sync_thread(handle_sync);

    // TODO: setup a signal handler incase a INT signal is received so this can be cleaned up nicely

    // go into the event loop
    int ret = g_manager->run();

    // Cleanup
    INFO("cleaning up");

    delete g_manager;

    sync_thread.join();

    INFO("Finishing");

    return ret;
}

void name_thread(const char* name)
{
    // use pthread functionality to name the thread
    int rc = pthread_setname_np(pthread_self(), name);
    ASSERT_THROW_ALWAYS(rc == 0, "setting name for thread {}", name);
}

// This runs ntp sync every NTP_SYNC_DELAY seconds
void handle_sync()
{
    name_thread("handle_sync");

    uint64_t start_time, current_time;
    timeval tv;
    get_accurate_time(&tv);
    start_time = tv.tv_sec*MICROS_PER_SEC + tv.tv_usec;

    // Always do a time sync in the beginning
    do_time_sync();

    while (g_manager->is_running()) 
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        get_accurate_time(&tv);
        current_time = tv.tv_sec*MICROS_PER_SEC + tv.tv_usec;

        if (current_time - start_time > MICROS_PER_SEC*NTP_SYNC_DELAY) 
        {
            do_time_sync(); 
            start_time = current_time;
        }
    }

    INFO("Shutting down thread");
}


//
//void handle_receive(uint16_t port, MessageQueue& out_queue)
//{
//    //  INFO("called, port = {}", port);
//
//    name_thread("handle_receive");
//
//    // The receive endpoint has to always be 0.0.0.0, not localhost or 127.0.0.1!
//    hs::endpoint local_endpoint(0, port);
//
//    uint8_t buf[MAX_BUF_SIZE];
//
//    hs::FragmentingReceiver server;
//
//    if(!server.connect(local_endpoint))
//    {
//        ERROR("Failed to connect to {}", local_endpoint);
//        return;
//    }
//
//    INFO("Receiving at {}. Going into loop", local_endpoint);
//
//    hs::endpoint remote_endpoint;
//
//    // NOTE: Add a done flag for breaking out of the loop later...
//    while (!g_done)
//    {
//        size_t bytes_recved = server.receive(buf, MAX_BUF_SIZE, &remote_endpoint, 1);
//        if (bytes_recved == 0)
//        {
//            continue;
//        }
//
//        INFO("received {} bytes = from {}", bytes_recved, remote_endpoint);
//
//        // decode the buf into a message and place it into the message queue
//        MessageHeader msg_header;
//        uint8_t* ptr = msg_header.deserialize(buf, bytes_recved);
//        ASSERT(ptr);
//
//        INFO("Received {} message", protocol::MESSAGE_TYPE_Name(msg_header.type()));
//
//        switch (msg_header.type())
//        {
//        case protocol::MESSAGE_TYPE::QUERY:
//        {
//            timeval current_time;
//            get_accurate_time(&current_time);
//
//            protocol::Query msg;
//            bool res = msg.ParseFromArray(ptr, msg_header.msg_size);
//            ASSERT_ALWAYS(res);
//            INFO("received query msg: ts=({}, {}), port={}, current_time=({}, {})",
//                 msg.sender_time_stamp().seconds(), msg.sender_time_stamp().micro_seconds(),
//                 msg.sender_port(),
//                 current_time.tv_sec, current_time.tv_usec);
//
//            timeval remote_time { int32_t(msg.sender_time_stamp().seconds()), int32_t(msg.sender_time_stamp().micro_seconds()) };
//
//            timeval diff_time;
//            timeval_subtract(diff_time, current_time, remote_time);
//
//            // Reply back with a QueryAck
//            protocol::QueryAck* reply_msg = new protocol::QueryAck;
//
//            reply_msg->set_listener_type(protocol::QueryAck_ListenerType_CAMERA);
//            protocol::TimeStamp* diff = reply_msg->mutable_diff();
//
//            diff->set_seconds(diff_time.tv_sec);
//            diff->set_micro_seconds(diff_time.tv_usec);
//
//            INFO("reply_msg type = {}, diff_time={}.{}",
//                 protocol::QUERY_ACK,
//                 reply_msg->diff().seconds(),
//                 reply_msg->diff().micro_seconds());
//
//            // configure the control endpoint
//            g_control_endpoint = hs::endpoint(remote_endpoint.addr(), msg.sender_port());
//
//            // Finally try to enqueue the message
//            res = out_queue.try_enqueue({protocol::QUERY_ACK, reply_msg});
//            ASSERT_ALWAYS(res);
//            break;
//        }
//
//        case protocol::MESSAGE_TYPE::SYNC_TIME:
//            do_time_sync();
//            break;
//
//        case protocol::MESSAGE_TYPE::STREAM_START:
//        {
//            // Pass the message on
//            protocol::StreamStart* msg = new protocol::StreamStart;
//            bool res = msg->ParseFromArray(ptr, msg_header.msg_size);
//            ASSERT_ALWAYS(res);
//
//            res = out_queue.try_enqueue({msg_header.type(), msg});
//            ASSERT_ALWAYS(res);
//            break;
//        }
//
//        case protocol::MESSAGE_TYPE::INTRINSIC_CALIBRATION_START:
//        case protocol::MESSAGE_TYPE::STREAM_STOP:
//        {
//            bool res = out_queue.try_enqueue({msg_header.type()});
//            ASSERT_ALWAYS(res);
//            break;
//        }
//        default:
//            WARN("Cannot handle message type {}", msg_header.type() );
//            break;
//        }
//    }
//
//    INFO("Shutting down thread");
//}
//
//// Camera functionality
//factory_base* select_camera_factory()
//{
//    factory_base* factory = new openni_factory();
//
//    factory_base::uri_vec_t uris = factory->device_uris();
//    if (uris.size() > 0)
//    {
//        INFO("Selected OpenNI system! Found these cameras:");
//        for (auto& uri : uris)
//        {
//            INFO(uri);
//        }
//        return factory;
//    }
//
//    delete factory;
//
//    factory = new v4l_factory();
//
//    uris = factory->device_uris();
//    if (uris.size() > 0)
//    {
//        INFO("Selected V4L system! Found these cameras:");
//        for (auto& uri : uris)
//        {
//            INFO(uri);
//        }
//        return factory;
//    }
//
//    delete factory;
//
//    INFO("No suitable camera system found!");
//    return nullptr;
//}
//
//#define QUIT_THREAD(cond, ...) if(!(cond)) { ERROR(__VA_ARGS__); g_done = true; return; }
//
//enum class CAMERA_STATE : uint8_t
//{
//    STANDBY = 0,
//    STREAMING = 1,
//    LISTENING = 2
//};
//
//std::ostream& operator<<(std::ostream& os, CAMERA_STATE s)
//{
//#define OUTPUT_STATE(e) case CAMERA_STATE::e : os << #e; break;
//
//    switch (s)
//    {
//    OUTPUT_STATE(STANDBY);
//    OUTPUT_STATE(STREAMING);
//    OUTPUT_STATE(LISTENING);
//    default: os << "Unknown CAMERA_STATE " << (int)s; break;
//    }
//#undef OUTPUT_STATE
//
//    return os;
//}
//
//static CAMERA_STATE g_state = CAMERA_STATE::STANDBY;
//
//CAMERA_STATE get_state() { return g_state; }
//
//void set_state(CAMERA_STATE new_s)
//{
//    if (g_state != new_s)
//    {
//        INFO("Was in {} state, going to {} state", g_state, new_s);
//        g_state = new_s;
//    }
//}
//
//static STREAM_FORMAT g_device_color_format = STREAM_FORMAT::INVALID;
//
//void handle_camera(MessageQueue& in_queue, MessageQueue& out_queue)
//{
//    name_thread("handle_camera");
//
//    FPSCounter counter("handle_camera");
//
//    factory_base* device_factory = select_camera_factory();
//    QUIT_THREAD(device_factory, "No camera system found!");
//
//    g_device_color_format = device_factory->color_format();
//
//    factory_base::uri_vec_t uris = device_factory->device_uris();
//    // Make sure there's a camera but only use the first one found
//    QUIT_THREAD(uris.size() > 0, "No Cameras were found!");
//
//    // This gets set in the STREAM_START message
//    factory_base::device_ptr_t device;
//
//    camera_frame frame;
//
//    const uint64_t* magic = reinterpret_cast<const uint64_t*>(FILE_MAGIC);
//    frame.header().set_magic(*magic);
//    frame.header().set_major_number(MAJOR_VERSION);
//    frame.header().set_minor_number(MINOR_VERSION);
//    frame.header().set_camera_id(42);
//    frame.header().set_tracker_id(42);
//
//    // Setup the encoder
//    encoder::color_encoder_t color_encoder = nullptr;
//    uint8_t* out_buf = nullptr;
//    size_t out_buf_size = 0;
//
//    bool ret;
//
//    while (!g_done)
//    {
//        while(true)
//        {
//            // Pump the in_queue and check all incoming messages
//            QueueElem in_msg;
//            bool ret = in_queue.try_dequeue(in_msg);
//            if (!ret)
//            {
//                break;
//            }
//
//            // if its a message we care about, process the message
//            switch (in_msg.msg_type)
//            {
//            case protocol::MESSAGE_TYPE::INTRINSIC_CALIBRATION_START:
//                INFO("Responding to INTRINSIC_CALIBRATION_START message");
//
//
//                // For later...
//                // Check the frame
//
//                // Run it through OpenCV corner detection
//
//                // Pack the results into a Intrinsic calibration corners message
//
//                // Send off the message to be processed
//
//                break;
//            case protocol::MESSAGE_TYPE::STREAM_START:
//            {
//                // Get the header data from the base message
//                protocol::StreamStart* msg = static_cast<protocol::StreamStart*>(in_msg.msg.get());
//                ASSERT(msg);
//                STREAM_FORMAT output_format = convert_format_p2l(msg->header().format());
//                uint32_t fps = msg->header().fps();
//                QUALITY quality = convert_quality_p2l(msg->header().quality());
//                uint32_t width = msg->header().width();
//                uint32_t height = msg->header().height();
//
//                // TODO: change device into a weak_ptr and have device_factory manage it's lifetime.
//                device.reset(); // Have to reset the device first before getting another pointer to it.
//
//                // Only use the first device
//                device = device_factory->device(fps, width, uris[0]);
//                QUIT_THREAD(device.get(), "Failed to initialize camera {} properly!", uris[0]);
//
//                INFO("after STREAM_START: camera size = {}x{}, fps={}, format={}, quality={}",
//                     device->width(), device->height(), device->fps(), output_format, quality);
//
//                width = device->width();
//                height = device->height();
//                fps = device->fps();
//
//                // Setup the encoder
//                if (color_encoder)
//                {
//                    color_encoder.reset();
//                }
//                encoder::factory encoder_factory(width, height, quality, fps);
//
//                INFO("building new encoder, output_format={}, device_format={}", output_format, g_device_color_format);
//                color_encoder = encoder_factory.color(output_format, g_device_color_format);
//
//                if (out_buf)
//                {
//                    delete [] out_buf;
//                    out_buf = nullptr;
//                }
//                out_buf_size = width * height * sizeof(RGB888Pixel);
//                out_buf = new uint8_t[out_buf_size];
//
//                frame.header().set_format(convert_format_l2p(output_format));
//                frame.header().set_width(width);
//                frame.header().set_height(height);
//                frame.header().set_fps(fps);
//                frame.header().set_quality(convert_quality_l2p(quality));
//
//                INFO("Received STREAM_START message");
//                protocol::StreamAck* start_ack = new protocol::StreamAck;
//                protocol::Header* header = start_ack->mutable_header();
//                *header = frame.header();
//
//                protocol::Rect* rect = frame.frame().mutable_rect();
//                rect->set_x(0);
//                rect->set_y(0);
//                rect->set_width(device->width());
//                rect->set_height(device->height());
//
////              INFO("in_msg type = {}, has_type={}", MESSAGE_TYPE_Name(in_msg->type()), in_msg->has_type());
//
//                ret = out_queue.try_enqueue({protocol::STREAM_ACK, start_ack});
//                ASSERT_ALWAYS(ret);
//
//                set_state(CAMERA_STATE::STREAMING);
//                break;
//            }
//            case protocol::MESSAGE_TYPE::STREAM_STOP:
//            {
//                INFO("Received STREAM_STOP message");
//                set_state(CAMERA_STATE::STANDBY);
//                break;
//            }
//            default:
//                // Don't know what this message is so just pass it along to the out queue
//                ret = out_queue.try_enqueue(std::move(in_msg));
//                ASSERT_ALWAYS(ret);
//                break;
//            }
//        }
//
//        // Grab and process a frame depending on the state
//        switch (get_state())
//        {
//        case CAMERA_STATE::STREAMING:
//        {
//            // Get a frame from the device
//            ret = device->read_frame(frame);
//            QUIT_THREAD(ret, "Failed to read frame");
//
//            protocol::StreamFrame* frame_msg = new protocol::StreamFrame;
//
//            protocol::Frame* frame_header = frame_msg->mutable_header();
//            *frame_header = frame.frame();
//
//            const uint8_t* in_buf = frame.buffer();
//            size_t in_buf_size = frame.buffer_size();
//
//            size_t buffer_size = color_encoder->encode(out_buf,
//                                                       in_buf,
//                                                       out_buf_size,
//                                                       in_buf_size);
//
//            uint8_t* transferred_buf = new uint8_t[frame.buffer_size()];
//            ::memcpy(transferred_buf, out_buf, buffer_size);
//
//            frame_header->set_buffer_size(buffer_size);
//
//            ret = out_queue.try_enqueue({protocol::STREAM_FRAME, frame_msg, transferred_buf, buffer_size});
//            ASSERT_ALWAYS(ret);
//
////          INFO("after frame push, queue size={}", out_queue.size_approx());
//
//            counter.update();
//            break;
//        }
//        default:
//            std::this_thread::sleep_for(std::chrono::milliseconds(1));
//            break;
//        }
//    }
//
//    // Make sure the openni system is shutdown
//    delete device_factory;
//
//    INFO("Shutting down thread");
//}
//
//void send_msg(hs::FragmentingSender& sender, const QueueElem& elem)
//{
//    static const size_t MAX_SEND_SIZE = 0xffffff;
//
//    static uint8_t buf[MAX_SEND_SIZE];
//
//    size_t msg_size = serialize_msg(buf, MAX_SEND_SIZE,
//                                    elem.msg_type, elem.msg.get(),
//                                    elem.data_buf.get(), elem.data_buf_size);
//
////  INFO("Sending message {} of size {} to {}", elem.msg_type, msg_size, g_control_endpoint);
//
//    size_t bytes_sent = sender.send(buf, msg_size);
//    if (bytes_sent != msg_size)
//    {
//        ERROR("Send to {} failed", g_control_endpoint);
//        g_control_endpoint = hs::endpoint();
//        set_state(CAMERA_STATE::STANDBY);
//    }
//}
//
//void handle_send(uint16_t port, MessageQueue& in_queue)
//{
//    name_thread("handle_send");
//
//    FPSCounter counter("handle_send");
//
//    // Setup the socket
//    hs::FragmentingSender sender;
//
////  size_t count = 0;
//
//    while(!g_done)
//    {
//        // Listen for messages coming from msg_queue, serialize them and send them out to their desination
//        QueueElem elem;
//        bool ret = in_queue.try_dequeue(elem);
////      if (count % 300 == 0)
////      {
////          INFO("After deque: ret = {}, queue_size = {}", ret, in_queue.size_approx());
////      }
////      count++;
//
//        if (!ret)
//        {
//            // NOTE: Replace this with semaphores
//            std::this_thread::sleep_for(std::chrono::milliseconds(1));
//            continue;
//        }
//
////      INFO("After deque: msg type = {}, queue size={}", elem.msg_type, in_queue.size_approx());
//
//        switch (elem.msg_type)
//        {
//        case protocol::MESSAGE_TYPE::QUERY_ACK:
//            // Whenever we have to send a query ack, connect to control_endpoint first
//            sender.connect(g_control_endpoint);
//            send_msg(sender, elem);
//            break;
//        case protocol::MESSAGE_TYPE::STREAM_ACK: send_msg(sender, elem); break;
//        case protocol::MESSAGE_TYPE::STREAM_FRAME: send_msg(sender, elem); break;
//
//        default:
//            WARN("Cannot handle message type {}", elem.msg_type );
//            break;
//        }
//
//        counter.update();
//    }
//
//    INFO("Shutting down thread");
//}

}; // namespace camera

}; // namespace holo3d
