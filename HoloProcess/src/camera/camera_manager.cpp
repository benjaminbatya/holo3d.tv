#include "camera_manager.hpp"

#include "udp_socket.hpp"

#include "h3t_file.hpp"

#include "holo3d.pb.h"

#include "util_funcs.hpp"

#include "convert_funcs.hpp"

#include "common_funcs.hpp"

#include "gst_funcs.hpp"

#include <atomic>

using CapsFilterPtr = Glib::RefPtr<Gst::CapsFilter>;

namespace holo3d
{

namespace camera
{

namespace hs = holo3d::socket;

const size_t MAX_BUF_SIZE = 2<<15;

struct Manager::Impl
{
    // Flag to inidicate the the threads should quit
    std::atomic<bool> m_running { true };

    // Net members
    void init_net();
    bool send_msg(protocol::MESSAGE_TYPE msg_type, const ::google::protobuf::Message* msg);
    void process_net();
    void cleanup_net();

    hs::endpoint m_local_ep;        // << The local endpoint for the receiver

    hs::udp_receiver m_receiver;    // The net receiver
    hs::udp_sender m_sender;        // The net sender

    // Gst members
    void init_gst();
    void process_gst();
    void cleanup_gst();
    void stop_pipeline();

    hs::endpoint m_gst_remote_ep;           // << the
    PipelinePtr m_current_pipeline;
    PipelinePtr m_int_calib_pipeline;
    BusPtr      m_gst_bus;

    // The udp_sink, host and port properties are set when the StreamStart message is received
    ElementPtr m_udp_sink;
    CapsFilterPtr m_caps_filter;
};


Manager::Manager(uint16_t local_port)
: pImpl(new Impl) // I can't use std::make_unique<Impl>() until c++14 :-(
{
    pImpl->m_local_ep = hs::endpoint(0, local_port);
}

Manager::~Manager()
{
}

Manager::Manager(Manager&& rhs) = default;
Manager& Manager::operator =(Manager&& rhs) = default;

int Manager::run()
{
    int ret = 0;

    // Initialization
    pImpl->init_net();
    pImpl->init_gst();

    // Loop
    while (pImpl->m_running) 
    {
        pImpl->process_net();
        pImpl->process_gst();
    }

    // Cleanup
    pImpl->cleanup_gst();
    pImpl->cleanup_net();

    return ret;
}

bool Manager::is_running() const
{
    return pImpl->m_running;
}

void Manager::stop()
{
    pImpl->m_running = false;
}

void Manager::Impl::init_net()
{
    bool ret = m_receiver.connect(m_local_ep);
    ASSERT_THROW_ALWAYS(ret, "failed to connect!");
}

// NOTE: These should be powers of two to avoid videocrop glitches!
// Later, replace videocrop with another element which is more flexible.
const int ROI_WIDTH = 256;
const int ROI_HEIGHT = 256;

void Manager::Impl::init_gst()
{
    // Create the intrinsic calibration pipeline
    m_int_calib_pipeline = Gst::Pipeline::create("intrinsic_calibration");
    ASSERT(m_int_calib_pipeline);

    PipelinePtr pipeline = m_int_calib_pipeline;

    ElementPtr v4l2src = Gst::ElementFactory::create_element("v4l2src");
    ASSERT(v4l2src);

    v4l2src->set_property("device", std::string("/dev/video0"));
    v4l2src->set_property("do-timestamp", true);

    pipeline->add(v4l2src);

#ifdef MODEL_RPI2
    const int WIDTH = 1280;
    const int HEIGHT = 720;
    const int FPS = 30;
#else
    const int WIDTH = 640;
    const int HEIGHT = 480;
    const int FPS = 15;
#endif 

    std::string caps_str = fmt::format("video/x-raw,width={},height={},framerate={}/1", WIDTH, HEIGHT, FPS);
    INFO(caps_str);
    CapsPtr caps = Gst::Caps::create_from_string(caps_str);
    ASSERT(caps);

    m_caps_filter = Gst::CapsFilter::create();
    ASSERT(m_caps_filter);
    m_caps_filter->property_caps().set_value(caps);

    pipeline->add(m_caps_filter);

    // Link the elements together
    v4l2src->link(m_caps_filter);

#if defined MODEL_RPI2
    // This needs to be configurable
    ElementPtr video_flip = Gst::ElementFactory::create_element("videoflip");
    ASSERT(video_flip);
    video_flip->set_property("method", 5);

    pipeline->add(video_flip);
    m_caps_filter->link(video_flip);

    ElementPtr h264_encoder = Gst::ElementFactory::create_element("omxh264enc");
    ASSERT(h264_encoder);
    // Set the omxh264 encoder as per 
    // https://www.raspberrypi.org/forums/viewtopic.php?f=38&t=6852&start=200
    // h264_encoder->set_property("control-rate", "variable");      // Use constant bitrate
    // h264_encoder->set_property("target-bitrate", 2000000);    // Use 15.5MBit/s (=1280*720*30*8*0.07)
    pipeline->add(h264_encoder);
    video_flip->link(h264_encoder);

#else
    ElementPtr h264_encoder = Gst::ElementFactory::create_element("x264enc");
    ASSERT(h264_encoder);
    // NOTE: we are doing ultrafast for now to decrease the encoding load on the CPU
    h264_encoder->set_property("speed-preset", 1);

    pipeline->add(h264_encoder);
    m_caps_filter->link(h264_encoder);

#endif // defined MODEL_RPI

    ElementPtr rtp_h264_pay = Gst::ElementFactory::create_element("rtph264pay");
    ASSERT(rtp_h264_pay);
    rtp_h264_pay->set_property("config-interval", 1);
    rtp_h264_pay->set_property("pt", 96);

    m_udp_sink = Gst::ElementFactory::create_element("udpsink");
    ASSERT(m_udp_sink);
    // host and port properties are set when the StreamStart message is received
    m_udp_sink->set_property("sync", false);

    pipeline->add(rtp_h264_pay)->add(m_udp_sink);

    h264_encoder->link(rtp_h264_pay)->link(m_udp_sink);
}

bool Manager::Impl::send_msg(protocol::MESSAGE_TYPE msg_type, const ::google::protobuf::Message* msg)
{
    static uint8_t buffer[MAX_BUF_SIZE];

    size_t msg_size = serialize_msg(buffer, MAX_BUF_SIZE,
                                    msg_type, msg,
                                    nullptr, 0);

    INFO("Sending message {} of size {} to {}", msg_type, msg_size, m_sender.connected_endpoint());

    size_t bytes_sent = m_sender.send(buffer, msg_size);
    if (bytes_sent != msg_size)
    {
        ERROR("Send to {} failed", m_sender.connected_endpoint());
        m_sender.disconnect();
//      set_state(CAMERA_STATE::STANDBY);
        return false;
    }

    return true;
}

void Manager::Impl::process_net()
{
    static uint8_t buffer[MAX_BUF_SIZE];

    hs::endpoint remote_endpoint;

    size_t bytes_recved = m_receiver.receive(buffer, MAX_BUF_SIZE, &remote_endpoint, 1);
    if (bytes_recved == 0) { return; }

    INFO("received {} bytes = from {}", bytes_recved, remote_endpoint);

    // decode the buf into a message and place it into the message queue
    MessageHeader msg_header;
    uint8_t* ptr = msg_header.deserialize(buffer, bytes_recved);
    ASSERT((void*)ptr);

    INFO("Received {} message", protocol::MESSAGE_TYPE_Name(msg_header.type()));

    switch (msg_header.type())
    {
    case protocol::QUERY:
    {
        timeval current_time;
        get_accurate_time(&current_time);

        protocol::Query msg;
        bool res = msg.ParseFromArray(ptr, msg_header.msg_size);
        ASSERT(res);
        INFO("received query msg: ts=({}, {}), port={}, current_time=({}, {})",
             msg.sender_time_stamp().seconds(), msg.sender_time_stamp().micro_seconds(),
             msg.sender_port(),
             current_time.tv_sec, current_time.tv_usec);

        timeval remote_time { int32_t(msg.sender_time_stamp().seconds()), int32_t(msg.sender_time_stamp().micro_seconds()) };

        timeval diff_time;
        timeval_subtract(diff_time, current_time, remote_time);

        // Reply back with a QueryAck
        protocol::QueryAck reply_msg;

        reply_msg.set_listener_type(protocol::QueryAck_ListenerType_CAMERA);
        protocol::TimeStamp* diff = reply_msg.mutable_diff();

        diff->set_seconds(diff_time.tv_sec);
        diff->set_micro_seconds(diff_time.tv_usec);

        INFO("reply_msg type = {}, diff_time={}.{}",
             protocol::QUERY_ACK,
             reply_msg.diff().seconds(),
             reply_msg.diff().micro_seconds());

        // configure the control endpoint
        hs::endpoint ep = hs::endpoint(remote_endpoint.addr(), msg.sender_port());
        res = m_sender.connect(ep);
        ASSERT(res);

        send_msg(protocol::QUERY_ACK, &reply_msg);                
    } break;

    case protocol::SYNC_TIME:
        do_time_sync();
        break;

    case protocol::STREAM_START:
    {
        // TODO: parse the STREAM_START message and use the parameters in it
        protocol::StreamStart msg;
        bool res = msg.ParseFromArray(ptr, msg_header.msg_size);
        ASSERT(res);

        int port = msg.port();
        
        // stop the current gst pipeline
        stop_pipeline();

        // Set the destination correctly
        m_udp_sink->set_property("host", remote_endpoint.str_addr());
        m_udp_sink->set_property("port", port);

        // start the Intrinsic calibration gst pipeline
        m_current_pipeline = m_int_calib_pipeline;
        Gst::StateChangeReturn cret = m_current_pipeline->set_state(Gst::STATE_PLAYING);
        ASSERT_THROW(cret != Gst::STATE_CHANGE_FAILURE, "Failed to set pipeline to playing state");

        m_gst_bus = m_current_pipeline->get_bus();
        ASSERT(m_gst_bus);

        CapsPtr caps = m_caps_filter->property_caps().get_value();
        caps->reference();
        ASSERT(caps);
//      INFO("Num structures = {}", caps->size());

        const Gst::Structure caps_struct = caps->get_structure(0);
        int width, height;
        caps_struct.get_field("width", width);
        caps_struct.get_field("height", height);
        Gst::Fraction frame_rate;
        caps_struct.get_field("framerate", frame_rate);

        INFO("Frame size = {}x{}, framerate={}/{}", width, height, frame_rate.num, frame_rate.denom);

        protocol::StreamAck reply_msg;
        protocol::Header* header = reply_msg.mutable_header();
        const uint64_t* magic = reinterpret_cast<const uint64_t*>(FILE_MAGIC);
        header->set_magic(*magic);
        header->set_major_number(MAJOR_VERSION);
        header->set_minor_number(MINOR_VERSION);
        header->set_camera_id(42);
        header->set_tracker_id(42);
        header->set_format(protocol::STREAM_FORMAT::COLOR_H264);
        header->set_quality(protocol::QUALITY::GOOD);
        header->set_width(width);
        header->set_height(height);
        header->set_fps(frame_rate.num);

        // reply to the start
        send_msg(protocol::STREAM_ACK, &reply_msg);         

    } break;

    case protocol::INTRINSIC_CALIBRATION_START:
        break;

    case protocol::STREAM_STOP:
    case protocol::INTRINSIC_CALIBRATION_STOP:
    {
        // stop the current gst_pipeline
        stop_pipeline();
    } break;


    case protocol::ASSIGNMENT:
    {
        // save the assignment mapping
        // Start the ROI streaming pipeline
    } break;

    default:
        WARN("Cannot handle message type {}", msg_header.type() );
        break;
    }
}

void Manager::Impl::process_gst()
{
    // There's no gstreamer bus available, then skip the gstreamer update
    if(!m_current_pipeline)
    {
        return;
    }
    ASSERT(m_gst_bus);

    const MessagePtr msg = m_gst_bus->pop(Gst::MILLI_SECOND,
                                          Gst::MESSAGE_STATE_CHANGED | Gst::MESSAGE_ERROR | Gst::MESSAGE_EOS);
    if (!msg) { return; }

    switch (msg->get_message_type()) 
    {
    case Gst::MESSAGE_ERROR:
    {
        ErrorMsgPtr err = ErrorMsgPtr::cast_static(msg);
        Glib::Error error;
        std::string debug_info;
        err->parse(error, debug_info);
        ERROR("Error received from element '{}': {}", err->get_source()->get_name(), error.what());
        ERROR("Debugging information: {}", debug_info);
        m_running = false;
    } break;
    case Gst::MESSAGE_EOS:
        INFO("End of stream reached");
        m_running = false;
        break;
    case Gst::MESSAGE_STATE_CHANGED:
        if (msg->get_source() == m_current_pipeline) 
        {
            MsgStateChgPtr chgmsg = MsgStateChgPtr::cast_static(msg);
            Gst::State old_state = chgmsg->parse_old();
            Gst::State new_state = chgmsg->parse();
            INFO("Pipeline changed from {} to {}", old_state, new_state);

            // if(new_state == Gst::STATE_PLAYING) {
//                  GstBin* bin = GST_BIN(pipeline->gobj());
//                  char buf[42];
//                  snprintf(buf, 43, "graph_%d", new_state);
//                  GST_DEBUG_BIN_TO_DOT_FILE(bin, GST_DEBUG_GRAPH_SHOW_ALL, buf);
            // }
        }
        break;
    default:
        ERROR("Unexpected message '{}' received", GST_MESSAGE_SRC_NAME(msg->gobj()));
        break;
    }
}

void Manager::Impl::cleanup_net()
{
    m_receiver.disconnect();
    m_sender.disconnect();
}

void Manager::Impl::cleanup_gst()
{
    stop_pipeline();
}

void Manager::Impl::stop_pipeline()
{
    INFO("Called");
    if (m_current_pipeline) 
    {
        Gst::StateChangeReturn cret = m_current_pipeline->set_state(Gst::STATE_NULL);
        ASSERT_THROW(cret != Gst::STATE_CHANGE_FAILURE, "Failed to set pipeline to NULL state");

        // NOTE: maybe we need to block here until the pipeline is in the NULL state

        m_current_pipeline.reset(); 
    }
}

}; // namespace camera

}; // namespace holo3d
