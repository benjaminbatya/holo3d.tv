#ifndef __CAMERA_MAIN_HPP_
#define __CAMERA_MAIN_HPP_

namespace holo3d
{

namespace camera
{

int main(int argc, char** argv);

}; // namespace camera

}; // namespace holo3d


#endif // __CAMERA_MAIN_HPP_
