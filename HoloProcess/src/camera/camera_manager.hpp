#pragma once

#include <stdint.h>
#include <memory>

#include "udp_socket.hpp"

namespace holo3d
{

namespace camera
{

class Manager
{
struct Impl;

public:
    Manager(uint16_t listening_port);
    ~Manager();

    Manager(Manager&& rhs);
    Manager& operator=(Manager&& rhs);

    int run();

    bool is_running() const;
    void stop();

protected:

    std::unique_ptr<Impl> pImpl;
};

}; // namespace camera

}; // namespace holo3d
