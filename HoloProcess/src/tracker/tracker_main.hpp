#ifndef __TRACKER_MAIN_HPP_
#define __TRACKER_MAIN_HPP_

namespace holo3d
{

namespace tracker
{

int main(int argc, char** argv);

}; // namespace tracker

}; // namespace holo3d


#endif // __TRACKER_MAIN_HPP_
