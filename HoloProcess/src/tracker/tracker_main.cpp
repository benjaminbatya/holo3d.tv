// tracker_main.cpp for HoloProcess

#include <thread>

#include <sys/time.h>

#include <tclap/CmdLine.h>

#include "util_funcs.hpp"

#include "common_funcs.hpp"

#include "fragmenting_socket.hpp"

#include "h3t_file.hpp"

#include "src/holo3d.pb.h"

namespace hs = holo3d::socket;

namespace holo3d
{

namespace tracker
{

// Flag to indicate completeness

void handle_send(uint16_t port);
void handle_receive(uint16_t port);

int main(int argc, char** argv)
{    
    TCLAP::CmdLine cmd("Module options", ' ', "0.1");

    TCLAP::UnlabeledValueArg<std::string> module_arg("module", "", true, "", "The module to load.");
    cmd.add(module_arg);

    TCLAP::ValueArg<uint16_t> port_arg("p", "port", "", false, default_port_num(), "The port to listen on");
    cmd.add(port_arg);

    cmd.parse(argc,argv);

    INFO("port = {}", port_arg.getValue());

    // Spin up the various threads to run
    std::thread tx_thread(&handle_send, port_arg.getValue()); 

    std::thread rx_thread(&handle_receive, port_arg.getValue()); 

    tx_thread.join();
    rx_thread.join();

    return 0;
}

static const size_t MAX_BUF_SIZE = 65536;

class UDP_Broadcaster
{
public:
    UDP_Broadcaster(uint16_t port)
    : m_ep("192.168.10.0", port)
    {
        m_client.connect(m_ep, true);   
    }

    void broadcast(const ::google::protobuf::Message& msg)
    {
        int msg_size = msg.ByteSize();
        bool res = msg.SerializeToArray(m_buf, msg_size);
        ASSERT_THROW_ALWAYS(res, "Failed to serialize message!");

        // INFO("broadcasting {} bytes to {}", msg_size, m_ep);
        m_client.send(m_buf, msg_size);
    }

protected:

    uint8_t m_buf[MAX_BUF_SIZE];

    hs::endpoint m_ep;

    hs::FragmentingSender m_client;
};

//static size_t NUM_MESSAGES = 100;

void handle_send(uint16_t port)
{
//  UDP_Broadcaster sender(port);
//
//  // Send a Query message
//  Query msg;
//
//  // NOTE: This has to be done to force the type to be sent, all of the other fields are optional
//  msg.set_type(msg.type());
//
//  //  INFO("Msg type = {}", MESSAGE_TYPE_Name(msg.type()));
//
//  msg.set_sender_port(port);
//
//  LocationUpdate msg;

//    for (size_t i=0; i<NUM_MESSAGES; i++)
//    {
//        TimeStamp* ts = msg.mutable_sender_time_stamp();
//
//        // Set the time
//        timeval current_time;
//        get_accurate_time(&current_time);
//        ts->set_seconds(current_time.tv_sec);
//        ts->set_micro_seconds(current_time.tv_usec);
//
//        sender.broadcast(msg);
//
////      INFO("Broadcast msg");
//        std::this_thread::sleep_for(std::chrono::milliseconds(15));
//    }
}

void handle_receive(uint16_t port)
{
//    hs::endpoint ep(0, port);
//    hs::udp_receiver receive_socket;
//    receive_socket.connect(ep);
//
//    uint8_t buf[MAX_BUF_SIZE];
//    hs::endpoint remote_endpoint;
//
//    size_t num_responses = 0;
//
//    std::map<std::string, double> avg_time_diffs;
//
//    while (avg_time_diffs.size() == 0 || num_responses < NUM_MESSAGES*avg_time_diffs.size())
//    {
//        size_t bytes_recved = receive_socket.receive_from(buf, MAX_BUF_SIZE, remote_endpoint);
////      INFO("received {} bytes = from {}", bytes_recved, remote_endpoint);
//
//        // decode the buf into a message and place it into the message queue
//        holo3d::Base base;
//        bool flag = base.ParseFromArray(buf, bytes_recved);
//        ASSERT_ALWAYS(flag);
//
//        switch (base.type())
//        {
//        case MESSAGE_TYPE::QUERY_ACK:
//        {
//            QueryAck msg;
//            flag = msg.ParseFromArray(buf, bytes_recved);
//            ASSERT_ALWAYS(flag);
////          INFO("received query ack msg from {}: listener_type={}", remote_endpoint, msg.listener_type());
//
//            timeval diff_time { int32_t(msg.diff().seconds()), int32_t(msg.diff().micro_seconds()) };
//
//            double time_diff = double(diff_time.tv_sec*MICROS_PER_SEC + diff_time.tv_usec)/MICROS_PER_SEC;
////          INFO("Time difference = {}", time_diff);
//
//            if (avg_time_diffs.find(remote_endpoint.str_addr()) == avg_time_diffs.end())
//            {
//                avg_time_diffs[remote_endpoint.str_addr()] = 0.0f;
//            }
//
//            avg_time_diffs[remote_endpoint.str_addr()] += time_diff;
//
//            num_responses++;
//        }
//            break;
//        default:
//            // Ignore unrecognized messages
////          WARN("Unrecognized message {}", base.type());
//            break;
//        }
//
//    }
//
//    for (auto& it : avg_time_diffs)
//    {
//        std::string addr = it.first;
//        double time_diff = it.second;
//        time_diff /= NUM_MESSAGES;
//        INFO("average time difference for addr='{}' is {}", addr, time_diff);
//    }
}


}; // namespace tracker

}; // namespace holo3d
