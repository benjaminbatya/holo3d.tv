#ifndef __CONTROL_MAIN_HPP_
#define __CONTROL_MAIN_HPP_

namespace holo3d
{

namespace control
{

int main(int argc, char** argv);

}; // namespace control

}; // namespace holo3d


#endif // __CONTROL_MAIN_HPP_
