// tracker_main.cpp for HoloProcess

#include <thread>

#include <sys/time.h>

#include <tclap/CmdLine.h>

#include "util_funcs.hpp"

#include "common_funcs.hpp"

#include "udp_socket.hpp"

#include "h3t_file.hpp"

#include "holo3d.pb.h"

#include "convert_funcs.hpp"

#include <atomic>

#include "gst_funcs.hpp"

namespace holo3d
{

namespace control
{

namespace hs = holo3d::socket;

// Shared state

std::atomic<bool> g_running { true };

void init_gst(uint16_t local_port);
void process_gst();
void cleanup_gst();

void handle_send(uint16_t local_port, uint16_t remote_port, bool);
void handle_receive(uint16_t local_port, uint16_t remote_port);

int main(int argc, char** argv)
{    
    Gst::init(argc, argv);

    TCLAP::CmdLine cmd("Module options", ' ', "0.1");

    TCLAP::UnlabeledValueArg<std::string> module_arg("module", "", true, "", "The module to load.");
    cmd.add(module_arg);

    TCLAP::ValueArg<uint16_t> local_port_arg("l", "local_port", "", false, default_port_num(), "The local port to listen on");
    cmd.add(local_port_arg);

    TCLAP::ValueArg<uint16_t> port_arg("p", "port", "", false, default_port_num(), "The port the remote devices are listening on");
    cmd.add(port_arg);

    TCLAP::ValueArg<bool> ntp_arg("n", "ntp", "", false, false, "Indicates whether to do the ntp update or not");
    cmd.add(ntp_arg);

    cmd.parse(argc,argv);

    INFO("local_port = {}, remote_port = {}, ntp update = {}", 
         local_port_arg.getValue(), port_arg.getValue(), ntp_arg.getValue());

    // Initialization first
    init_gst(5000);

    // Spin up the various threads to run
    std::thread tx_thread(&handle_send, local_port_arg.getValue(), port_arg.getValue(), ntp_arg.getValue()); 

    std::thread rx_thread(&handle_receive, local_port_arg.getValue(), port_arg.getValue()); 

    // Main loop
    while (g_running) 
    {
        process_gst();
    }

    // Cleanup
    tx_thread.join();
    rx_thread.join();

    cleanup_gst();

    return 0;
}

static const size_t MAX_BUF_SIZE = 2<<15;
static const char* BROADCAST_ADDR = "192.168.10.0";

class UDP_Broadcaster
{
public:
    UDP_Broadcaster(hs::endpoint ep)
    : m_ep(ep)
    {
        ensure_broadcast();
    }

    bool ensure_broadcast()
    {
        if (m_client.connected_endpoint() != m_ep) 
        {
            if(!m_client.connect(m_ep, true))
            {
                ERROR("Couldn't connect to {}", m_ep);
                return false;
            }
        }   
        return true;
    }

    void broadcast(protocol::MESSAGE_TYPE msg_type, const ::google::protobuf::Message* msg = nullptr)
    {
        if (!ensure_broadcast()) 
        {
            return;
        }

        size_t msg_size = serialize_msg(m_buf, MAX_BUF_SIZE, msg_type, msg);

        INFO("broadcasting {} bytes to {}", msg_size, m_ep);
        m_client.send(m_buf, msg_size);
    }

    void send_to(const hs::endpoint& remote, protocol::MESSAGE_TYPE msg_type, const ::google::protobuf::Message* msg = nullptr)
    {
        size_t msg_size = serialize_msg(m_buf, MAX_BUF_SIZE, msg_type, msg);

        INFO("sending {} bytes to {}", msg_size, remote);
        m_client.send_to(m_buf, msg_size, remote);
    }

protected:

    uint8_t m_buf[MAX_BUF_SIZE];

    hs::endpoint m_ep;

    hs::udp_sender m_client;
};

typedef std::map<uint32_t, hs::endpoint> CameraMap_t;
CameraMap_t g_cameras;

void handle_send(uint16_t local_port, uint16_t remote_port, bool send_sync_time)
{
    hs::endpoint remote_ep(BROADCAST_ADDR, remote_port);
    UDP_Broadcaster sender(remote_ep);

//  INFO("send_sync_time = {}", send_sync_time);

    if (send_sync_time) 
    {
        // Send a SYNC_TIME message
        sender.broadcast(protocol::SYNC_TIME);

        const int sleep_time = 7;
//      INFO("Sleeping for {} seconds", sleep_time);
        std::this_thread::sleep_for(std::chrono::seconds(sleep_time));
    }

    // Send a Query message
    protocol::Query msg;

    msg.set_sender_port(local_port);

    protocol::TimeStamp* ts = msg.mutable_sender_time_stamp();

    // Set the time
    timeval current_time;
    get_accurate_time(&current_time);
    ts->set_seconds(current_time.tv_sec);
    ts->set_micro_seconds(current_time.tv_usec);

    sender.broadcast(protocol::QUERY, &msg);

//      INFO("Broadcast msg");
    // Wait two seconds for replies before going to the next task
    std::this_thread::sleep_for(std::chrono::seconds(2));

    // Send a stream start to all of the cameras
    INFO("Sending INTRINSIC_CALIBRATION_START message to all cameras");
    for (auto& camera_endpoint : g_cameras) 
    {
        sender.send_to(camera_endpoint.second, protocol::INTRINSIC_CALIBRATION_START);
    }
    sender.ensure_broadcast();

    while(g_running)
    {
//          // Do intrinsic calibration on each camera
//          for (auto& camera_endpoint : g_cameras)
//          {
//              protocol::Base msg;
//              msg.set_type(protocol::MESSAGE_TYPE::INTRINSIC_CALIBRATION_START);
//
//              sender.send(camera_endpoint.second, msg);
//          }

        
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}

void handle_receive(uint16_t local_port, uint16_t remote_port)
{
    hs::endpoint ep(0, local_port);
    hs::udp_receiver receive_socket;
    receive_socket.connect(ep);

    uint8_t buf[MAX_BUF_SIZE];
    hs::endpoint remote_endpoint;

    while (g_running) 
    {
        size_t bytes_recved = receive_socket.receive(buf, MAX_BUF_SIZE, &remote_endpoint, 50);
        if (bytes_recved == 0) // Ignore timeout
        {
            continue;
        }

        //      INFO("received {} bytes = from {}", bytes_recved, remote_endpoint);
        
        // decode the buf into a message and place it into the message queue
        MessageHeader msg_header;
        uint8_t* ptr = msg_header.deserialize(buf, bytes_recved);
        ASSERT((void*)ptr);

        switch (msg_header.type())
        {
        case protocol::QUERY_ACK:
        {
            protocol::QueryAck msg;
            bool flag = msg.ParseFromArray(ptr, msg_header.msg_size);
            ASSERT_ALWAYS(flag);
            INFO("received query ack msg from {}: listener_type={}", remote_endpoint, QueryAck_ListenerType_Name(msg.listener_type()) );

            timeval diff_time { int32_t(msg.diff().seconds()), int32_t(msg.diff().micro_seconds()) };

            double time_diff = double(diff_time.tv_sec*MICROS_PER_SEC + diff_time.tv_usec)/MICROS_PER_SEC;
            INFO("Time difference = {}", time_diff);

            // change remote_endpoint's port to the standard port
            hs::endpoint ep(remote_endpoint.addr(), remote_port);

            g_cameras[ep.addr()] = ep;
        } break;

        default:
            // Ignore unrecognized messages
            WARN("Unrecognized message {}", msg_header.type());
            break;
        }

    }
}

static PipelinePtr g_pipeline;
static BusPtr g_bus;

void init_gst(uint16_t gst_port)
{
    g_pipeline = Gst::Pipeline::create();
    ASSERT(g_pipeline);

    ElementPtr udp_src = Gst::ElementFactory::create_element("udpsrc");
    ASSERT(udp_src);
    udp_src->set_property("port", int(gst_port));

    std::string caps_str = fmt::format("application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96");
    INFO(caps_str);
    CapsPtr caps = Gst::Caps::create_from_string(caps_str);
    ASSERT(caps);

    ElementPtr caps_filter = Gst::ElementFactory::create_element("capsfilter");
    caps_filter->set_property("caps", caps);

    ElementPtr rtp_h264_depay = Gst::ElementFactory::create_element("rtph264depay");
    ASSERT(rtp_h264_depay);

    ElementPtr h264_dec = Gst::ElementFactory::create_element("avdec_h264");
    ASSERT(h264_dec);

    ElementPtr display_sink = Gst::ElementFactory::create_element("fpsdisplaysink");
    ASSERT(display_sink);

    g_pipeline->add(udp_src)->add(caps_filter)->add(rtp_h264_depay)->add(h264_dec)->add(display_sink);

    udp_src->link(caps_filter)->link(rtp_h264_depay);
    rtp_h264_depay->link(h264_dec)->link(display_sink);

    Gst::StateChangeReturn cret = g_pipeline->set_state(Gst::STATE_PLAYING);
    ASSERT_THROW(cret != Gst::STATE_CHANGE_FAILURE, "Failed to set pipeline to playing state");

    g_bus = g_pipeline->get_bus();
    ASSERT(g_bus);
}

void process_gst()
{
    // There's no gstreamer bus available, then skip the gstreamer update
    if(!g_pipeline)
    {
        return;
    }
    ASSERT(g_bus);

    MessagePtr msg = g_bus->pop(Gst::CLOCK_TIME_NONE,
                                Gst::MESSAGE_STATE_CHANGED | Gst::MESSAGE_ERROR | Gst::MESSAGE_EOS);
    if (!msg) { return; }

    switch (msg->get_message_type()) 
    {
    case Gst::MESSAGE_ERROR:
    {
        ErrorMsgPtr err = ErrorMsgPtr::cast_static(msg);
        Glib::Error error;
        std::string debug_info;
        err->parse(error, debug_info);
        ERROR("Error received from element '{}': {}", err->get_source()->get_name(), error.what());
        ERROR("Debugging information: {}", debug_info);
        g_running = false;
    } break;
    case Gst::MESSAGE_EOS:
        INFO("End of stream reached");
        g_running = false;
        break;
    case Gst::MESSAGE_STATE_CHANGED:
        if (msg->get_source() == g_pipeline) 
        {
            MsgStateChgPtr chgmsg = MsgStateChgPtr::cast_static(msg);
            Gst::State old_state = chgmsg->parse_old();
            Gst::State new_state = chgmsg->parse();
            INFO("Pipeline changed from {} to {}", old_state, new_state);

//                  GstBin* bin = GST_BIN(pipeline->gobj());
//                  char buf[42];
//                  snprintf(buf, 43, "graph_%d", new_state);
//                  GST_DEBUG_BIN_TO_DOT_FILE(bin, GST_DEBUG_GRAPH_SHOW_ALL, buf);
        }
        break;
    default:
        ERROR("Unexpected message '{}' received", GST_MESSAGE_SRC_NAME(msg->gobj()));
        break;
    }
}

void cleanup_gst()
{
    Gst::StateChangeReturn cret = g_pipeline->set_state(Gst::STATE_NULL);
    ASSERT_THROW(cret != Gst::STATE_CHANGE_FAILURE, "Failed to set pipeline to playing state");
}


}; // namespace control

}; // namespace holo3d
