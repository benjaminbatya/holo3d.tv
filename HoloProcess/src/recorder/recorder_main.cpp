// recorder_main.cpp for HoloProcess

#include <thread>

#include <sys/time.h>

#include <tclap/CmdLine.h>

#include "util_funcs.hpp"

#include "common_funcs.hpp"

#include "fragmenting_socket.hpp"

#include "h3t_file.hpp"

#include "src/holo3d.pb.h"

#include "convert_funcs.hpp"

namespace hs = holo3d::socket;

namespace holo3d
{

namespace recorder
{

// Shared state

void handle_send(uint16_t port, const hs::endpoint& ep);
void handle_receive(uint16_t port);

int main(int argc, char** argv)
{    
    TCLAP::CmdLine cmd("Module options", ' ', "0.1");

    TCLAP::UnlabeledValueArg<std::string> module_arg("module", "", true, "", "The module to load.");
    cmd.add(module_arg);

    TCLAP::UnlabeledValueArg<std::string> remote_addr_arg("address", "", true, "", "The address of the camera to record");
    cmd.add(remote_addr_arg);

    TCLAP::ValueArg<uint16_t> port_arg("p", "port", "", false, default_port_num(), "The port the remote devices are listening on");
    cmd.add(port_arg);

    cmd.parse(argc,argv);

    hs::endpoint remote_ep(remote_addr_arg.getValue());

    INFO("local_port = {}, remote_addr = {}", port_arg.getValue(), remote_ep);

    // Spin up the various threads to run
    std::thread tx_thread(&handle_send, port_arg.getValue(), remote_ep);

    std::thread rx_thread(&handle_receive, port_arg.getValue()); 

    tx_thread.join();
    rx_thread.join();

    return 0;
}

static const size_t MAX_BUF_SIZE = 65536;

void handle_send(uint16_t local_port, const hs::endpoint& ep)
{
    socket::FragmentingSender sender;
    if(!sender.connect(ep))
    {
        ERROR("Failed to connect to {}", ep);
        return;
    }

    uint8_t buf[MAX_BUF_SIZE];

    {
        {
            // Send a Query message
            protocol::Query msg;

            msg.set_sender_port(local_port);

            protocol::TimeStamp* ts = msg.mutable_sender_time_stamp();

            // Set the time
            timeval current_time;
            get_accurate_time(&current_time);
            ts->set_seconds(current_time.tv_sec);
            ts->set_micro_seconds(current_time.tv_usec);

            size_t msg_size = serialize_msg(buf, MAX_BUF_SIZE, protocol::QUERY, &msg);

            INFO("sending {} bytes to {}", msg_size, ep);
            sender.send(buf, msg_size);
        }

//      INFO("Broadcast msg");
        // Wait two seconds for replies before going to the next task
        std::this_thread::sleep_for(std::chrono::seconds(2));

        {
            // Send a stream start to all of the cameras
            INFO("Sending STREAM_START message to all cameras");
            size_t msg_size = serialize_msg(buf, MAX_BUF_SIZE, protocol::STREAM_START);
            sender.send(buf, msg_size);
        }
    }

    {
        while(true)
        {
//          // Do intrinsic calibration on each camera
//          for (auto& camera_endpoint : g_cameras)
//          {
//              protocol::Base msg;
//              msg.set_type(protocol::MESSAGE_TYPE::INTRINSIC_CALIBRATION_START);
//
//              sender.send(camera_endpoint.second, msg);
//          }

            
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }
}

void handle_receive(uint16_t port)
{
    hs::endpoint ep(0, port);
    hs::FragmentingReceiver receive_socket;
    receive_socket.connect(ep);

    uint8_t buf[MAX_BUF_SIZE];
    hs::endpoint remote_endpoint;

    while (true) 
    {
        size_t bytes_recved = receive_socket.receive(buf, MAX_BUF_SIZE, &remote_endpoint);
//      INFO("received {} bytes = from {}", bytes_recved, remote_endpoint);

        // decode the buf into a message and place it into the message queue
        MessageHeader msg_header;
        uint8_t* ptr = msg_header.deserialize(buf, bytes_recved);
        ASSERT(ptr);

        switch (msg_header.type())
        {
        case protocol::QUERY_ACK:
        {
            protocol::QueryAck msg;
            bool flag = msg.ParseFromArray(ptr, msg_header.msg_size);
            ASSERT_ALWAYS(flag);
            INFO("received query ack msg from {}: listener_type={}", remote_endpoint, QueryAck_ListenerType_Name(msg.listener_type()) );

            timeval diff_time { int32_t(msg.diff().seconds()), int32_t(msg.diff().micro_seconds()) };

            double time_diff = double(diff_time.tv_sec*MICROS_PER_SEC + diff_time.tv_usec)/MICROS_PER_SEC;
            INFO("Time difference = {}", time_diff);

            break;
        }

        default:
            // Ignore unrecognized messages
//          WARN("Unrecognized message {}", base.type());
            break;
        }

    }
}


}; // namespace control

}; // namespace holo3d
