#include "common_funcs.hpp"

#include "util_funcs.hpp"

#include <cstdlib>

namespace holo3d
{

int exec_sudo(std::string cmd)
{
    cmd = std::string("sudo ") + cmd;

    INFO("Running '{}'", cmd);
    return system(cmd.c_str());
}


static const std::string NTP_CMD = "ntpdate -b server";

// Hopefully, this will get this processor's clock within 1ms of the server's clock which should be accurate enough
// for the entire system to work properly.
void do_time_sync()
{
    // First run ntpdate if desired
#if not defined PLATFORM_Arm
    ERROR("Platform is not ARM, not running time sync!");
    return;
#endif // not defined PLATFORM_Arm

    int result = 0;

    // Script from https://readme.phys.ethz.ch/documentation/troubleshooting_ntp/
    // Use "watch ntpq -p" to watch the drift from server
    // Please note that this requires server to be the only time server in /etc/ntp.conf
//  result = exec_sudo("/etc/init.d/ntp stop");
//  ASSERT_EQUAL_ALWAYS(result, 0);

//  result = exec_sudo("rm /var/lib/ntp/ntp.drift");
//  ASSERT_EQUAL_ALWAYS(result, 0);

    result = exec_sudo(NTP_CMD);
    if (result != 0) 
    {
        ERROR("Failed to run '{}'", NTP_CMD);
    }
//  ASSERT_EQUAL_ALWAYS(result, 0);

//  result = exec_sudo("/etc/init.d/ntp start");
//  ASSERT_EQUAL_ALWAYS(result, 0);

//    // Run a test to see how many cycles occur per second
//    size_t num_iterations = 360;
//    INFO("Running cycles stability test: num_iteration=%1%", num_iterations);
//
//    uint64_t* compute_time = new uint64_t[num_iterations];
//    for (size_t i=0; i<num_iterations; i++)
//    {
//        uint64_t start_time = get_time();
//
//        uint64_t count = 0;
//        for (int x=0; x<100000000; x++)
//        {
//            count++;
//        }
//
//        compute_time[i] = get_time() - start_time;
//
////      (void*)(count);
//    }
//
//    // Calculate the mean
//    double mean = 0.0;
//
//    for (size_t i=0; i<num_iterations; i++)
//    {
////      INFO("Cycle %1%: count=%2%", i, cycles[i]);
//        mean += compute_time[i];
//    }
//    mean /= num_iterations;
//
//    // Calculate the standard deviation
//    double std_dev = 0.0;
//    for (size_t i=0; i<num_iterations; i++)
//    {
//        double temp_val = mean - compute_time[i];
//        std_dev += temp_val*temp_val;
//    }
//
//    std_dev /= num_iterations;
//    std_dev = sqrt(std_dev);
//
//    INFO("Compute time Mean=%1%, Standard Deviation=%2%", mean, std_dev);
//
//    delete [] compute_time;
}

}; // namespace holo3d
