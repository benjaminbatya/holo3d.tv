#include "pch_files.hpp"
#include "mean_shift_tracker.hpp"

#include <cstring>
#include <cmath>

#include "util_funcs.hpp"

namespace holo3d
{

template<typename T>
MeanShiftTracker<T>::MeanShiftTracker(T kernel_size)
: m_kernel_size(kernel_size)
{

}

template<typename T>
MeanShiftTracker<T>::~MeanShiftTracker()
{
    delete [] m_back_projection;
    m_back_projection = nullptr;
}


// Algo from code 9.2 (page 460) in "Feature Extraction and Image Processing"
template<typename T>
void MeanShiftTracker<T>::calc_Cb_Cr(const RGB888Pixel& p, size_t& Cb, size_t& Cr)
{
    T r = T(p.r) / 256;
    T g = T(p.g) / 256;
    T b = T(p.b) / 256;
    
    Cb = size_t(128 - 37.79*r - 74.203*g + 112*b) / 4;
    Cr = size_t(128 + 112*r - 93.786*g - 18.214*b) / 4; 
}

template<typename T>
void MeanShiftTracker<T>::reset(const MeanShiftTracker<T>::ImageInfo& image, const RECT& region)
{
    // adjust the the region
    m_region_size.x = region.width / 2;
    m_region_size.y = region.height / 2;

    m_prev_pos.x = region.x + m_region_size.x;
    m_prev_pos.y = region.y + m_region_size.y;

    // do the histrogram calculation   
    density(image, m_prev_histogram);
}

// Code from code 9.1 (page 460)
template<typename T>
void MeanShiftTracker<T>::density(const MeanShiftTracker<T>::ImageInfo& image, 
                                  MeanShiftTracker<T>::histogram_t& histogram)
{
    const Point& size = region_size();

    ::memset(histogram, 0, sizeof(histogram));

    T total = 0.0;
    const T exp_div = 2.0 * m_kernel_size * m_kernel_size;
//  const double DELTA = 0.00001;
    for (int j=-size.y; j<=size.y; j++) 
    {
        int y = m_prev_pos.y + j;

        // skip pixels out of image
        if (y < 0 || y >= int(image.height) ) { continue; }

        for (int i=-size.x; i<=size.x; i++) 
        {
            int x = m_prev_pos.x + i;

            // skip pixels out of image
            if (x < 0 || x >= int(image.width) ) { continue; }

            size_t Cb, Cr;

            const RGB888Pixel& c = image.data[y*image.stride + x];
            calc_Cb_Cr(c, Cb, Cr);
            
            T dist = -(i*i + j*j);

            T w = std::exp(dist / exp_div);
//          if (w > DELTA)
//          {
//              INFO("Cb=%d, Cr=%d, dist=%f, weight>%f = %f", Cb, Cr, dist, DELTA, w);
//          }
            histogram[Cb][Cr] += w; 
            total += w;
        }
    }

//  INFO("total = %f", total);

    for (int i=0; i<HISTO_SIZE; i++) 
    {
        for (int j=0; j<HISTO_SIZE; j++) 
        {
            histogram[i][j] /= total;
//          if (histogram[i][j] > DELTA)
//          {
//              INFO("after normalize: Cb=%d, Cr=%d, weight>%f = %f", i, j, DELTA, histogram[i][j]);
//          }
        }
    }
}


template<typename T>
void MeanShiftTracker<T>::mean_shift(const MeanShiftTracker<T>::ImageInfo& image)
{
    Point pos = prev_pos();

    static const int MAX_ITER = 10;

    for (int i=0; i<MAX_ITER; i++) 
    {
        _mean_shift(image);
        Point new_pos = prev_pos();

        int d_x = new_pos.x - pos.x;
        int d_y = new_pos.y - pos.y;

        if (d_x*d_x + d_y*d_y == 0) 
        {
            break;
        }
        pos = new_pos;
    }
}


// Follows 9.3 (page 466)
template<typename T>
void MeanShiftTracker<T>::_mean_shift(const MeanShiftTracker<T>::ImageInfo& image)
{
    const Point& size = region_size();

    // Create weight matrix
    Point w_size;
    w_size.x = size.x * 2 + 1;
    w_size.y = size.y * 2 + 1;
    T w[w_size.y][w_size.x];
    ::memset(w, 0, sizeof(w));

    // histogram from previous frame
    const histogram_t& q = histogram();

    // Get the histogram of the the current frame 
    histogram_t qs;
    density(image, qs);

    // compute weights
    for (int j = -size.y; j <= size.y; j++) 
    {
        int y = m_prev_pos.y + j;

        // skip pixels out of image
        if (y < 0 || y >= int(image.height) ) { continue; }
        for (int i = -size.x; i <= size.x; i++) 
        {
            int x = m_prev_pos.x + i;

            // skip pixels out of image
            if (x < 0 || x >= int(image.width)) { continue; }

            size_t Cb, Cr; 

            const RGB888Pixel& c = image.data[y*image.stride + x];
            calc_Cb_Cr(c, Cb, Cr);

            // Weight index
            int w_row = j + size.y;
            int w_col = i + size.x;

            // Set weight
            if (qs[Cb][Cr] > 0) 
            {
                w[w_row][w_col] = sqrt( q[Cb][Cr] / qs[Cb][Cr]);
            }
        }
    }

    T mean_sum_x, mean_sum_y, kernel_sum;
    mean_sum_x = mean_sum_y = kernel_sum = 0;
        
    for (int j=-size.y; j<=size.y; j++) 
    {
        int row = m_prev_pos.y + j;

        // skip pixels out of image
        if (row < 0 || row >= int(image.height)) { continue; }
        int w_row = j + size.y;

        for (int i=-size.x; i<=size.x; i++) 
        {
            // Position of the pixel in the image
            int col = m_prev_pos.x + i;

            // skip pixels out of image
            if(col < 0 || col >= int(image.width)) { continue; }
            int w_col = i + size.x;

            // Kernel Parameter
            T a = T(i*i + j*j) / (m_kernel_size * m_kernel_size);

            // Gaussian kernel
            T g = std::exp(-a/2);

            // Mean sum
            mean_sum_x += w[w_row][w_col] * g * col;
            mean_sum_y += w[w_row][w_col] * g * row;

            // Kernel sum
            kernel_sum += w[w_row][w_col] * g;
        }
    }

    // Update the previous position
    m_prev_pos.x = mean_sum_x / kernel_sum;
    m_prev_pos.y = mean_sum_y / kernel_sum;       
}

template <typename T>
void MeanShiftTracker<T>::back_project(const MeanShiftTracker<T>::ImageInfo& image, RGB888Pixel*& result)
{
     const histogram_t& histo = histogram();
//  for (size_t i=0; i<Tracker_t::HISTO_SIZE; i++)
//  {
//      //          std::string out_str = "";
//      for (size_t j=0; j<Tracker_t::HISTO_SIZE; j++)
//      {
//          float w = histo[i][j];
//          if (w > 0.001)
//          {
//              INFO("histo[%d][%d] = %f", i, j, w);
//              //                  out_str += std::to_string(w) + ", ";
//          }
//      }
//      //          INFO(out_str);
//  }

    size_t offset = 0;
    for (size_t j=0; j<image.height; j++, offset += image.stride-image.width) 
    {
        for (size_t i=0; i<image.width; i++, offset++) 
        {
            RGB888Pixel p = image.data[offset];
            size_t Cb, Cr;
            calc_Cb_Cr(p, Cb, Cr);

            // p.r = p.g = p.b = uint8_t(histo[Cb][Cr] * 128); 
            p.r = p.g = p.b = uint8_t(std::pow(10.0, histo[Cb][Cr]) * 40);
            result[offset] = p;
        }
    }
}

template <typename T>
void MeanShiftTracker<T>::back_project(const MeanShiftTracker<T>::ImageInfo& image, T*& result)
{
    const histogram_t& histo = histogram();

    size_t offset = 0;
    for (size_t j=0; j<image.height; j++, offset += image.stride-image.width) 
    {
        for (size_t i=0; i<image.width; i++, offset++) 
        {
            RGB888Pixel p = image.data[offset];
            size_t Cb, Cr;
            calc_Cb_Cr(p, Cb, Cr);

            result[offset] = histo[Cb][Cr];
        }
    }
}

template <typename T>
void MeanShiftTracker<T>::cam_shift(const ImageInfo& image, const T& scale)
{
    if (!m_back_projection) 
    {
        // NOTE: we have to use stride instead of width because thats what back_project expects
        m_back_projection = new T[image.stride*image.height];
    }

    back_project(image, m_back_projection);

    // Initialize moments
    T M00, M10, M01, M11, M20, M02;
    M00 = M10 = M01 = M11 = M20 = M02 = 0.0;

    const Point& size = region_size();

    for (int j = -size.y; j <= size.y; j++) 
    {
        int row = m_prev_pos.y + j;

        // skip pixels out of image
        if (row < 0 || row >= int(image.height) ) { continue; }
        for (int i = -size.x; i <= size.x; i++) 
        {
            int col = m_prev_pos.x + i;

            // skip pixels out of image
            if (col < 0 || col >= int(image.width)) { continue; }

            const T& value = m_back_projection[row*image.stride + col];

            M00 += value;
            M10 += row * value;
            M01 += col * value;
            M11 += row * col * value; 
            M20 += row * row * value;
            M02 += col * col * value;
        }
    }

    T center_row = M10 / M00;
    T center_col = M01 / M00;

    T a = M20/M00 - center_row*center_row;
    T b = 2.0 * (M11/M00 - center_row*center_col);
    T c = M02/M00 - center_col*center_col;

    T new_srow = std::round(scale * std::sqrt((a + c + std::sqrt(b*b + (a-c)*(a-c)))/2) );
    T new_scol = std::round(scale * std::sqrt((a + c - std::sqrt(b*b + (a-c)*(a-c)))/2) );

    center_row = std::round(center_row);
    center_col = std::round(center_col);

//  INFO("old pos = (%d, %d), new pos = (%f, %f)", m_prev_pos.x, m_prev_pos.y, center_col, center_row);
//  INFO("old size= (%d, %d), new size= (%f, %f)", m_region_size.x, m_region_size.y, new_scol, new_srow);

    m_prev_pos.x = int(center_col);
    m_prev_pos.y = int(center_row);

    m_region_size.x = int(new_scol);
    m_region_size.y = int(new_srow);
}


// Allow for the float and double instantiations only
template class MeanShiftTracker<float>;
template class MeanShiftTracker<double>;

}; // namespace holo3d
