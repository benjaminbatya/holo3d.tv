// Main.cpp for testViewer that displays the OpenNI2 color+depth video streams in 3d or flat
#include "pch_files.hpp"

#include <stdio.h>
#include <sstream>
#include <cstring>

namespace po = boost::program_options;

#include "util_funcs.hpp"
#include "h3t_file.hpp"
#include "camera_info.hpp"

#include "loader.hpp"

#include "Window.h"

int main(int argc, char **argv)
{    
    try
    {
        std::string file_name;
        bool use_histogram = true;
        holo3d::DisplayModes video_mode = holo3d::DisplayModes::BLEND;
        bool loop_movie = false;
        bool display_movie_stats = false;

        std::stringstream display_mode_options;
        display_mode_options << "Mode to display the video in:";
        for (int i = (int)holo3d::DisplayModes::MIN; i <= (int)holo3d::DisplayModes::MAX; i++)
        {
            display_mode_options << " " << static_cast<holo3d::DisplayModes>(i);
        }

        std::string app_name = boost::filesystem::basename(argv[0]);
        po::options_description visible(std::string("Usage: ") + app_name + " <options> movie.h3t\nAllowed Options");
        visible.add_options()
        ("help,h",                                                                      "produce help message")
        ("histogram,g", po::value<bool>(&use_histogram)->default_value(use_histogram),  "Use histogram for depth or not")
        ("loop,l",      po::value<bool>(&loop_movie)->default_value(loop_movie),        "loop the movie")    
        ("mode,m",      po::value<holo3d::DisplayModes>(&video_mode)->default_value(video_mode), display_mode_options.str().c_str())
        ("stats,s",     "display the movie stats")
        ;

        po::options_description hidden(std::string("Hidden options for ") + app_name);
        hidden.add_options()
        ("file", po::value<std::string>(&file_name)->required(),                          "The movie to play")
        ;

        po::options_description all;
        all.add(visible).add(hidden);

        po::positional_options_description required;
        required.add("file", 1);

        po::variables_map vm;
        try
        {
            po::store(po::command_line_parser(argc, argv).options(all).positional(required).run(), vm);

            if (vm.count("help"))
            {
                INFO_CPP(visible);
                return 0;
            }

            po::notify(vm);
        }
        catch(boost::program_options::required_option& e) 
        { 
            ERROR_CPP("No movie specified!");
            INFO_CPP(visible);
            return -1; 
        } 
        catch (po::error& e)
        {
            ERROR_CPP(e.what());
            INFO_CPP(visible);
            return -1;
        }

        if (vm.count("stats")) 
        {
            display_movie_stats = true;
        }

        if (!(holo3d::DisplayModes::MIN <= video_mode && video_mode <= holo3d::DisplayModes::MAX) )
        {
            ERROR("Startup mode has to be a valid DISPLAY_MODE");
            INFO_CPP(visible);
            return -1; 
        }

        ASSERT_ALWAYS(!file_name.empty());

        std::shared_ptr<holo3d::loader_base> loader = 
            std::make_shared<holo3d::file_loader>(file_name);

        if (display_movie_stats) 
        {
            loader->run();

            // Calculate the per-frame stats
            holo3d::camera_frame frame;
            uint32_t frame_count = 0;
            uint32_t color_frame_total = 0;
            uint32_t depth_frame_total = 0;
            uint64_t start_time = 0, end_time = 0;

            uint64_t average_x=0, average_y=0;
            uint64_t average_width=0, average_height=0;
            while (loader->next_frame(frame)) 
            {
                if (frame_count == 0) 
                {
                    start_time = frame.color_time_stamp();
//                  INFO("start_time=%1%", start_time);
                }
                end_time = frame.color_time_stamp();

                frame_count += 1;
                color_frame_total += frame.color_buffer_size();
                depth_frame_total += frame.depth_buffer_size();

                average_x += frame.frame_header().rect.x;
                average_y += frame.frame_header().rect.y;
                average_width += frame.frame_header().rect.width;
                average_height += frame.frame_header().rect.height;
            }

//          INFO("end_time=%1%", end_time);

            uint64_t delta = end_time - start_time;

            uint32_t total_frame_size = color_frame_total + depth_frame_total + 
                frame_count*sizeof(holo3d::FRAME_HEADER);

            total_frame_size /= frame_count;
            color_frame_total /= frame_count;
            depth_frame_total /= frame_count;

            float delta_secs = ((float)delta) / MICROS_PER_SEC ;
            float fps = ((float)frame_count) / delta_secs;

            average_x /= frame_count;
            average_y /= frame_count;
            average_width /= frame_count;
            average_height /= frame_count;

            INFO("In-depth movie info: number of frames=%1%, average total frame size=%2%"
                 "\n\ttime length (secs)=%3%, average fps=%4%"
                 "\n\taverage color frame size=%5%, average depth frame size=%6%"
                 "\n\taverage_x=%7%, average_y=%8%"
                 "\n\taverage_width=%9%, average_height=%10%",
                 frame_count, total_frame_size, 
                 delta_secs, fps, 
                 color_frame_total, depth_frame_total,
                 average_x, average_y,
                 average_width, average_height);
        }

        holo3d::Window viewer("H3T File Viewer", loader);

        viewer.use_histogram(use_histogram);
        viewer.video_mode(video_mode);
        viewer.loop_movie(loop_movie);

        // never returns from run
        viewer.run(); 

    } catch (std::exception &e) 
    {
        ERROR_CPP(std::endl << e.what());
        return -1;
    } 
}
