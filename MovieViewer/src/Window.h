#pragma once
// Window.h - Class containing the glut functionality

#include "decoder.hpp"

#include "frame.hpp"
#include "mean_shift_tracker.hpp"

namespace holo3d
{

// Forward decl
class loader_base;

// #define MAX_DEPTH 10000

enum class DisplayModes
{
    BLEND = 1,
    IMAGE = 2,
    DEPTH = 3,

    // These are to allow iterating over the enums
    MIN = BLEND,
    MAX = DEPTH,
    COUNT,
};

inline std::ostream& operator<<(std::ostream& os, const DisplayModes& mode)
{
#define OUTPUT_MODE(e) case DisplayModes::e : os << #e; break;

    switch (mode)
    {
    OUTPUT_MODE(DEPTH);
    OUTPUT_MODE(IMAGE);
    OUTPUT_MODE(BLEND);
    default: os << "Unknown display mode"; break;
    }
#undef OUTPUT_MODE

    return os;
}
inline std::istream& operator>>(std::istream& is, DisplayModes& mode)
{
    std::string input;
    is >> input;

#define INPUT_MODE(m) if (input == #m) { mode = DisplayModes::m; }

    INPUT_MODE(DEPTH)
    else INPUT_MODE(IMAGE)
    else INPUT_MODE(BLEND)
    else
    {
        throw ("Unknown display mode");
    }
#undef INPUT_MODE

    return is;
}


class Window
{
public:
    Window(const char* window_name, std::shared_ptr<loader_base> sm);
    ~Window(); // Window is not intended to be inherited from

    void run(); // Does not return

    /**
     * Setter/getter combo for m_use_histogram. Call this as 
     * window->use_histogram(true) or window->use_histogram()
     *  
     * @author benjamin (11/10/2014)
     * 
     * @param use sets m_use_histogram
     * 
     * @return bool the value of m_use_histogram
     */
    bool use_histogram() const { return m_use_histogram; }
    void use_histogram(bool f) { m_use_histogram = f; }
    DisplayModes video_mode() const { return m_view_state; }
    void video_mode(const DisplayModes& mode) { m_view_state = mode; }
    bool loop_movie() const { return m_loop_movie; }
    void loop_movie(bool f) { m_loop_movie = f; }

private:

    void handle_load();

    void shut_down();

    void delay_display();
    void display();
    // virtual void displayPostDraw() {}; // Overload to draw over the screen image

    void on_key(unsigned char key, int x, int y);
    void on_mouse(int button, int state, int x, int y);
    void display_keys();

    void init_opengl();

    static Window*  ms_self;

    void init_opengl_hooks();
    void draw_texmap(RGB888Pixel* texmap, const RECT& rect);
    void draw_depth_buffer();
    void draw_color_buffer();
    void draw_blend();
    void draw_mean_shift();

    static void glutIdle();
    static void glutDisplay();
    static void glutKeyboard(unsigned char key, int x, int y);
    static void glutMouse(int, int, int, int);

    std::shared_ptr<loader_base> m_loader;
    camera_frame    m_frame;

    std::string     m_window_name;
    size_t          m_texmap_x = 0;
    size_t          m_texmap_y = 0;
    DisplayModes    m_view_state = DisplayModes::BLEND;
    RGB888Pixel*    m_color_texmap = nullptr;
    RGB888Pixel*    m_depth_texmap = nullptr;
    size_t          m_width = 0;
    size_t          m_height = 0;

    uint64_t        m_prev_frame_timestamp = 0;
    uint64_t        m_prev_real_time = 0;

    bool            m_last_frame = false;
    bool            m_load_next = true;

    bool            m_use_histogram = false;
    bool            m_loop_movie = false;

    decoder::color_decoder_t m_color_decoder;
    decoder::depth_decoder_t m_depth_decoder;

    enum class STATE : uint16_t
    {
        MIN = 0,

        NORMAL = MIN,
        PAUSED,
        MEAN_SHIFT,

        MAX = MEAN_SHIFT,

        LEN
    };
    friend std::ostream& operator<<(std::ostream& os, Window::STATE s);
    
    STATE           m_state = STATE::NORMAL;

    bool in_state(STATE s) const { return m_state == s; }
    void goto_state(STATE s);

    RECT            m_selection = {0};  // The selection rect to calculate the meanShift histogram from
    using Tracker_t = MeanShiftTracker<double>;
    Tracker_t m_tracker { 5.0 };
};

std::ostream& operator<<(std::ostream& os, Window::STATE s);

}; // namespace holo3d
   
