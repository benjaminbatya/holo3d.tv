// Implementation of TestViewer
// This only works on linux for now...
#include "pch_files.hpp"

#include <GL/glut.h>

#include <cstring>
#include <sstream>
#include <chrono>
#include <thread>

#include "util_funcs.hpp"
#include "h3t_file.hpp"
#include "loader.hpp"

#include "Window.h"

namespace holo3d
{

#define GL_WIN_SIZE_X   1280
#define GL_WIN_SIZE_Y   1024
#define TEXTURE_SIZE    512

#define DEFAULT_DISPLAY_MODE	DISPLAY_MODE_DEPTH

#define MIN_NUM_CHUNKS(data_size, chunk_size) ((((data_size) - 1) / (chunk_size) + 1))
#define MIN_CHUNKS_SIZE(data_size, chunk_size) (MIN_NUM_CHUNKS(data_size, chunk_size) * (chunk_size))

// Start public

Window::Window(const char* window_name, std::shared_ptr<loader_base> sm)
: m_loader(sm)
, m_window_name(window_name)
{
    ASSERT(!ms_self);
    ms_self = this;
}

Window::~Window()
{
    delete[] m_color_texmap; 
    delete[] m_depth_texmap; 

    ms_self = nullptr;
}

void Window::run()
{   
    // Don't set the load_handler because it shouldn't be called more then once
    // m_loader->load_handler([&] (camera_info::ptr_t) { this->handle_load(); });

    m_loader->run();

    handle_load();

    init_opengl();

    glutMainLoop(); // Does not return
}

void Window::handle_load()
{
    // m_movie_manager must be initialized by this point.
    ASSERT(m_loader->valid());

    m_width = m_loader->width();
    m_height = m_loader->height();
 
    // Texture map init
    m_texmap_x = MIN_CHUNKS_SIZE(m_width, TEXTURE_SIZE);
    m_texmap_y = MIN_CHUNKS_SIZE(m_height, TEXTURE_SIZE);
    m_color_texmap = new RGB888Pixel[m_texmap_x * m_texmap_y];
    ASSERT(m_color_texmap);
    m_depth_texmap = new RGB888Pixel[m_texmap_x * m_texmap_y];
    ASSERT(m_depth_texmap);

    decoder::factory factory(m_width, m_height, m_texmap_x);
    m_color_decoder = factory.color(m_loader->file_header().color_format);
    m_depth_decoder = factory.depth(m_loader->file_header().depth_format, use_histogram());

    INFO("playing '%1%' at resolution %2%x%3%, color format=%4%, depth format=%5%", 
         m_loader->connection_info(), m_width, m_height,
         m_loader->file_header().color_format,
         m_loader->file_header().depth_format);

    display_keys();
}

void Window::shut_down()
{
    m_depth_decoder.reset();
    m_color_decoder.reset();
    
    m_loader.reset();

    // NOTE: This is weird, should return control straight to caller, 
    // but glutLeaveMainLoop() is not implemented on my version of GLUT
    exit(1);
}

void Window::delay_display()
{
    int64_t frame_diff = m_frame.color_time_stamp() - m_prev_frame_timestamp;
    int64_t curr_time = get_time();
    int64_t real_diff = curr_time - m_prev_real_time;
    int64_t drift = real_diff - frame_diff;
//  INFO("Window::display: frame %d: ts=%d, frame_diff=%d, real_diff=%d, drift=%d",
//       m_frame.frame_number(), (int32_t)m_frame.time_stamp(),
//       (int32_t)frame_diff, (int32_t)real_diff,
//       (int32_t)drift);

    // INFO("prev timestamp = %u, time to sleep = %u", (uint32_t)g_prev_ts, (uint32_t)time_to_sleep);

    m_prev_frame_timestamp = m_frame.color_time_stamp();
    m_prev_real_time = curr_time;
    if (frame_diff < MICROS_PER_SEC)
    {
        // Sleep this thread until we need to play the next frame.
        // NOTE: This isn't accurate but I dont care about that...
        // Would have to use get_time() to
        std::this_thread::sleep_for(std::chrono::microseconds(frame_diff - drift));
    }
}
void Window::display()
{
    if (m_last_frame && m_loop_movie)
    {
        m_loader->run();
        m_last_frame = false;
        m_load_next = true;
    }

    if (m_load_next)
    {
        bool ret = m_loader->next_frame(m_frame);
        if (!ret)
        {
            INFO("Window::display: finished playing video.");
            m_last_frame = true;
            m_load_next = false;
            return; 
        } else
        {
            m_load_next = in_state(STATE::NORMAL);
        }

        ASSERT_ALWAYS(m_frame.valid());

        // Pause playing after loading the frame but 
        // before displaying it to get the correct delay
        delay_display();

        // INFO("TestViewer::display_blended: 1 m_color_frame.isValid()=%d", m_color_frame.isValid());

        // Draw the color buffer
        m_color_decoder->decode(m_color_texmap, m_frame.color_buffer(), sizeof(RGB888Pixel) * m_texmap_x * m_texmap_y, m_frame.color_buffer_size(), m_frame.frame_header().rect);

        // Draw out the depth buffer
        m_depth_decoder->decode(m_depth_texmap, m_frame.depth_buffer(), sizeof(RGB888Pixel) * m_texmap_x * m_texmap_y, m_frame.depth_buffer_size(), m_frame.frame_header().rect);
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0, GL_WIN_SIZE_X, GL_WIN_SIZE_Y, 0, -1.0, 1.0);

    if ( in_state(STATE::MEAN_SHIFT) )
    {
        draw_blend();
        draw_mean_shift();
    } else if (!m_loader->file_header().has_depth()) 
    {
        // Draw the color frame to texture if needed
        draw_texmap(m_color_texmap, m_frame.frame_header().rect);
    } else 
    {
        draw_blend();
    }

    // Swap the OpenGL display buffers
    glutSwapBuffers();
}

void Window::draw_blend()
{
    if (m_view_state == DisplayModes::BLEND)
    {
        // INFO("TestViewer::display_blended: 2 m_color_frame.isValid()=%d", m_color_frame.isValid());
        // Write it to the GLUT window
        draw_texmap(m_depth_texmap, m_frame.frame_header().rect);
        
        // INFO("TestViewer::display_blended: 3 m_color_frame.isValid()=%d", m_color_frame.isValid());
        
        glEnable(GL_BLEND);
        // Trying different blend modes...
        glBlendFunc(GL_DST_COLOR, GL_ZERO);
        // glBlendFunc(GL_ZERO, GL_SRC_COLOR);
        // glBlendEquation(GL_FUNC_ADD);
        draw_texmap(m_color_texmap, m_frame.frame_header().rect);
        glDisable(GL_BLEND);
    
    } else if (m_view_state == DisplayModes::IMAGE)  // Draw the color frame to texture if needed
    {
        draw_texmap(m_color_texmap, m_frame.frame_header().rect);
    } else if (m_view_state == DisplayModes::DEPTH) // Draw the depth frame to texture if needed
    {
        draw_texmap(m_depth_texmap, m_frame.frame_header().rect);
    }
}

void Window::draw_texmap(RGB888Pixel* texmap, const RECT& rect)
{
    // setup to mipmap m_texmap to the GL_TEXTURE_2D target
    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_texmap_x, m_texmap_y, 0, GL_RGB, GL_UNSIGNED_BYTE, texmap);

    // Display the OpenGL texture map
    glColor4f(1, 1, 1, 1);

    glBegin(GL_QUADS);

    float frame_width = (float)m_width;
    float frame_height = (float)m_height;

    // Figure out the percentage of the frame size the rect's x,y and (x+width),(y+width) is
    float image_left = ((float)rect.x) / frame_width;
    float image_top = ((float)rect.y)  / frame_height;
    float image_right = ((float)rect.x + rect.width) / frame_width;
    float image_bottom = ((float)rect.y + rect.height) / frame_height;

    #define MAP_COORDS(image_x, image_y, x, y) \
        glTexCoord2f(((float)image_x) / m_texmap_x, ((float)image_y) / m_texmap_y); \
        glVertex2f(x * GL_WIN_SIZE_X, y * GL_WIN_SIZE_Y);

    // upper left
    MAP_COORDS(0, 0, image_left, image_top);

    // upper right
    MAP_COORDS(rect.width, 0, image_right, image_top);

    // bottom right
    MAP_COORDS(rect.width, rect.height, image_right, image_bottom);

    // bottom left
    MAP_COORDS(0, rect.height, image_left, image_bottom);

    #undef MAP_COORDS

    glEnd();

}

void Window::draw_mean_shift()
{
    if (m_tracker.is_valid()) 
    {
        glDisable(GL_TEXTURE_2D);

        glColor4f(1, 1, 1, 1);
        glBegin(GL_LINES);

        Tracker_t::Point pt = m_tracker.prev_pos();
        const Tracker_t::Point& size = m_tracker.region_size();

        int win_width = glutGet(GLUT_WINDOW_WIDTH);
        int win_height = glutGet(GLUT_WINDOW_HEIGHT);

        pt.x = pt.x * win_width / m_width;
        pt.y = pt.y * win_height / m_height;

        int half_width = size.x * win_width / m_width;
        int half_height = size.y * win_height / m_height;

        int left = pt.x - half_width;
        int right= pt.x + half_width;
        int top  = pt.y + half_height;
        int bottom=pt.y - half_height;

        left = left < 0 ? 0 : left;
        right = right >= win_width ? win_width-1 : right;
        top = top >= win_height ? win_height-1 : top;
        bottom = bottom < 0 ? 0 : bottom;

        glVertex2f(left, bottom);
        glVertex2f(right, bottom);

        glVertex2f(right, bottom);
        glVertex2f(right, top);

        glVertex2f(right, top);
        glVertex2f(left, top); 

        glVertex2f(left, top); 
        glVertex2f(left, bottom); 

        glEnd();

        glEnable(GL_TEXTURE_2D);
    }
}

void Window::goto_state(Window::STATE s)
{
    // Do whatever pre-transition actions here

    // Do transition
    INFO("GOING into state %1%", s);
    m_state = s;

    // Post-transition actions
    switch(m_state)
    {
    case STATE::NORMAL:
        m_load_next = true;
        break;
    case STATE::PAUSED:
        m_load_next = false;
        break;
    case STATE::MEAN_SHIFT:
        m_load_next = false;
        break;
    default:
        THROW("Unknown state %1%!", s);
        break;
    }
}

void Window::on_key(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27: // esc key
        shut_down();
        break;

    case '1':
        m_view_state = DisplayModes::BLEND;
        break;

    case '2':
        m_view_state = DisplayModes::IMAGE;
        break;

    case '3':
        m_view_state = DisplayModes::DEPTH;
        break;

    case 'p':
        goto_state(in_state(STATE::PAUSED) ? STATE::NORMAL : STATE::PAUSED);
        break; 

    case 'l':
        m_loop_movie = !m_loop_movie;
        goto_state(STATE::NORMAL);
        break;

    case 'h':
        use_histogram(!use_histogram());
        m_depth_decoder->use_histogram(use_histogram());
        break;

    case 's':
    {
        int next_s = (int(m_state)+1) % int(STATE::LEN);
        goto_state( STATE(next_s) );
        break;
    }
    case 'm':
        if (in_state(STATE::MEAN_SHIFT))
        {
            m_load_next = true; 

            if (m_tracker.is_valid()) 
            {
                Tracker_t::ImageInfo image;
                image.data = m_color_texmap;
                image.width = m_width;
                image.stride = m_texmap_x;
                image.height = m_height;
                m_tracker.mean_shift(image);

                m_tracker.back_project(image, m_depth_texmap);

                m_tracker.cam_shift(image, 2.5);
            }
        }
        break;

    default:
        WARN("Unknown key %d", key);
        break;
    }
}

std::ostream& operator<<(std::ostream& os, const RGB888Pixel& p)
{
    os << "(" << int(p.r) << ", " << int(p.g) << ", " << int(p.b) << ")";
    return os;
}

void Window::on_mouse(int button, int state, int x, int y)
{
//  INFO("Called! b=%d, s=%d, x=%d, y=%d", button, state, x, y);

    if ( in_state(STATE::NORMAL) )
    {
        return;
    } else if( in_state(STATE::PAUSED))
    {
        if (state == 1) 
        {
            int nx = x * m_width / glutGet(GLUT_WINDOW_WIDTH); 
            int ny = y * m_height / glutGet(GLUT_WINDOW_HEIGHT);

            size_t Cb, Cr;
            const RGB888Pixel& p = m_color_texmap[ny*m_texmap_x + nx];
            MeanShiftTracker<double>::calc_Cb_Cr(p, Cb, Cr);

            INFO("At (%1%, %2%), p=%3%, Cb = %4%, Cr = %5%", nx, ny, p, Cb, Cr);
        }

        return;
    }

    if (state == 0) // Mouse down 
    {
        m_selection.x = x;
        m_selection.y = y;
        m_selection.width = 0;
        m_selection.height = 0;
    } else
    {
        int min_x = std::min(x, int(m_selection.x));
        int min_y = std::min(y, int(m_selection.y));
        int max_x = std::max(x, int(m_selection.x));
        int max_y = std::max(y, int(m_selection.y));

//      m_selection.x = min_x;
//      m_selection.y = min_y;
//      m_selection.width = max_x - min_x + 1;
//      m_selection.height = max_y - min_y + 1;

        // Scale the region 
        min_x = min_x * m_width / glutGet(GLUT_WINDOW_WIDTH);
        max_x = max_x * m_width / glutGet(GLUT_WINDOW_WIDTH);
        min_y = min_y * m_height / glutGet(GLUT_WINDOW_HEIGHT);
        max_y = max_y * m_height / glutGet(GLUT_WINDOW_HEIGHT);
//      INFO("min = (%d, %d), max = (%d, %d)", min_x, min_y, max_x, max_y);

        RECT region;
        region.x = min_x;
        region.y = min_y;
        region.width = max_x - min_x + 1;
        region.height = max_y - min_y + 1;

//      INFO("%1%", region);

        // Reset the tracker
        Tracker_t::ImageInfo image;
        image.data = m_color_texmap;
        image.width = m_width;
        image.stride = m_texmap_x;
        image.height = m_height;

        m_tracker.reset(image, region);

        m_tracker.back_project(image, m_depth_texmap);
    }
}

void Window::display_keys()
{
    INFO("Keys:");
    INFO("ESC - quit");
    INFO("1 - Display depth frames blended over color frames");
    INFO("2 - Display only color frames");
    INFO("3 - Display only depth frames");
    INFO("p - Pause/Unpause the movie");
    INFO("l - loop/don't loop the movie");
    INFO("h - toggle display depth as histogram flag");
}

void Window::init_opengl()
{
    int zero = 0;
    glutInit(&zero, NULL);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(GL_WIN_SIZE_X, GL_WIN_SIZE_Y);
    glutCreateWindow(m_window_name.c_str());

    // glutFullWindow();

    glutKeyboardFunc(glutKeyboard);
    glutMouseFunc(glutMouse);
    glutDisplayFunc(glutDisplay);
    glutIdleFunc(glutIdle);

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
}

// End public

// Start statics
Window* Window::ms_self = nullptr;

void Window::glutIdle()
{
    glutPostRedisplay();
}

void Window::glutDisplay()
{
    Window::ms_self->display();
}

void Window::glutKeyboard(unsigned char key, int x, int y)
{
    Window::ms_self->on_key(key,x,y);
}

void Window::glutMouse(int b, int s, int x, int y)
{
    Window::ms_self->on_mouse(b, s, x, y);
}

// End statics

std:: ostream& operator<<(std::ostream& os, Window::STATE s)
{
#define OUTPUT_STATE(e) case Window::STATE::e : os << #e; break;

    switch (s)
    {
    OUTPUT_STATE(NORMAL);
    OUTPUT_STATE(PAUSED);
    OUTPUT_STATE(MEAN_SHIFT);
    default: os << "Unknown state " << (int)s; break;
    }
#undef OUTPUT_MODE

    return os;
}

}; // namespace holo3d

