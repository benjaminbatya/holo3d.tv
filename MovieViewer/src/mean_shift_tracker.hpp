#pragma once

#include <string>
#include <h3t_file.hpp>

namespace holo3d
{

// T can be float or double
template<typename T>
class MeanShiftTracker
{
public:

    MeanShiftTracker(T kernal_size);
    ~MeanShiftTracker();

    struct ImageInfo
    {
        RGB888Pixel* data;
        size_t      width;      // << width of the image in pixels
        size_t      stride;     // << stride of the image in pixels
        size_t      height;     // << height of the image in pixels
    };

    struct Point
    {
        int x;
        int y;
    };

    void reset(const ImageInfo& image, const RECT& region);
    void mean_shift(const ImageInfo& image);
    void back_project(const ImageInfo& image, RGB888Pixel*& result);
    void cam_shift(const ImageInfo& image, const T& scale = 1.0);

    static const int HISTO_SIZE = 64;
    using histogram_t = T[HISTO_SIZE][HISTO_SIZE];
        
    const histogram_t& histogram() const { return m_prev_histogram; }

    static void calc_Cb_Cr(const RGB888Pixel& p, size_t& Cb, size_t& Cr);

    const Point& prev_pos() const { return m_prev_pos; }
    const Point& region_size() const { return m_region_size; }

    bool is_valid() const { return m_region_size.x > 0 && m_region_size.y > 0; }

protected:

    void density(const ImageInfo& image, histogram_t& histo);
    void _mean_shift(const ImageInfo& image);

    void back_project(const ImageInfo& image, T*& result);

    const T m_kernel_size;

    histogram_t m_prev_histogram;

    Point m_prev_pos { 0, 0 };
    Point m_region_size { 0, 0 };

    T* m_back_projection = nullptr;

};

}; // namespace holo3d
