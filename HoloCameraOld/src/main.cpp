// Main.cpp for HoloCamera

#include "pch_files.hpp"

#include <stdio.h>
#include <cstring>
#include <signal.h>
namespace po = boost::program_options;

#include "util_funcs.hpp"

#include "option_enums.hpp"
#include "camera.hpp"

#include "control_client.hpp"

#include "v4l_factory.hpp"
#include "openni_factory.hpp"

using namespace holo3d;

// Start static initization data

// Change this to vary the number of regions to record. Later, make this an option...
std::vector<camera::RegionData> g_regions = 
{
    { {0,   0,   100, 100}, 101, 2, 2},
    { {100, 100, 100, 100}, 201, 1, -1}
};

// End static initization data

factory_base* select_camera_factory()
{
    factory_base* factory = new openni_factory();

    factory_base::uri_vec_t uris = factory->device_uris();
    if (uris.size() > 0) 
    {
        INFO("Selected OpenNI system! Found these cameras:");
        for (auto& uri : uris) 
        {
            INFO(uri);
        }
        return factory; 
    } 

    delete factory;

    factory = new v4l_factory();

    uris = factory->device_uris();
    if (uris.size() > 0) 
    {
        INFO("Selected V4L system! Found these cameras:");
        for (auto& uri : uris) 
        {
            INFO(uri);
        }
        return factory;
    }

    delete factory;

    INFO("No suitable camera system found!");
    return nullptr;
}



int main(int argc, char **argv)
{    
    {
        // Print the start message
        time_t raw_time;
        time(&raw_time);
        INFO("Started %1% on %2%", argv[0], ctime(&raw_time));
    }

    // The return value, depends on if there are errors that occur or not
    int ret = 0;

    factory_base* factory = nullptr;

    try
    {
        factory = select_camera_factory();
        ASSERT_THROW_ALWAYS(factory, "No camera found!");

        std::string port                = holo3d::DEFAULT_PORT;
        bool do_ntp_update              = false;
        size_t max_sensors              = 1;
        bool use_regions                = false;

        std::string host;
        std::string app_name            = boost::filesystem::basename(argv[0]);

        po::options_description visible(std::string("Usage: ") + app_name + " host[:port(=" + port + ")] <options> \nAllowed Options");
        visible.add_options()
        ("help,h",                                                                                  "Display this help message")
        ("ntp,n",           po::value<bool>(&do_ntp_update)->default_value(do_ntp_update),          "Do the ntpdate before running the camera, only enabled on the ARM platform")     
        ("max_sensors,m",   po::value<size_t>(&max_sensors)->default_value(max_sensors),            "Maximum number of sensors to use")
        ("regions,r",       "Indicates that regions should be sent")
        ;

        po::options_description hidden;
        hidden.add_options()
        ("host", po::value<std::string>(&host)->required(),                             "The host to connect to")
        ;

        po::options_description all;
        all.add(visible).add(hidden);

        po::positional_options_description required;
        required.add("host", 1);

        po::variables_map vm;
        try
        {
            po::store(po::command_line_parser(argc, argv).options(all).positional(required).run(), vm);

            if (vm.count("help"))
            {
                INFO_CPP(visible);
                return 0;
            }

            po::notify(vm);

            if (vm.count("regions")) 
            {

                // Never use regions for now...
//              use_regions = true;
            }
        }
        catch (po::required_option& e)
        {
            ERROR_CPP("No host specified!!\n");
            INFO_CPP(visible);
            return -1;
        }
        catch (po::error& e)
        {
            ERROR_CPP(e.what());
            INFO_CPP(visible);
            return -1;
        }

        // First run ntpdate if desired
#if defined PLATFORM_Arm
        if (do_ntp_update) 
        {
            // Run ntpdate server before trying to connect
            INFO("Running 'sudo ntpdate server'...");
            int result = system("sudo ntpdate server");
            if (result != 0) {
                ERROR("Failed to run ntpdate! result = %d. Continuing anyway", result); 
            }
        }
#endif // defined PLATFORM_Arm

        factory_base::uri_vec_t uris = factory->device_uris();
        ASSERT(uris.size() > 0);

        typedef std::unique_ptr<camera> camera_ptr_t;
        std::vector<camera_ptr_t> cameras;
        for (size_t i=0; i<max_sensors && i<uris.size(); i++)
        {
            cameras.emplace_back(new camera(factory, uris[i]));
        }

        ASSERT(!host.empty());

        size_t pos = host.find_first_of(":");
        if (pos != std::string::npos)
        {
            port = host.substr(pos+1);
            host = host.substr(0, pos);
        }

        // Setup the control channel
        holo3d::ControlClient control;

        bool done = false;

        while (!done) 
        {
            holo3d::protocol::StartData data;
            done = control.wait_for_start(data);

            INFO("After waiting for start: done = %1%", done);

            if (done) 
            {
                break;
            }

            // Figure out the streaming attibutes desired by the server and setup context
            uint16_t frame_width;
            switch (data.resolution)
            {
            case RESOLUTION::SMALL: frame_width = 320; break;
            case RESOLUTION::MEDUIM: frame_width = 640; break;
            // case RESOLUTION::LARGE: frame_width = 1024; break;
            default: THROW("INVALID resolution size %s", data.resolution); break;
            }
            uint16_t frame_height = (frame_width * 3 / 4);

            encoder::factory factory(frame_width, frame_height, data.quality);

            camera::context_t context;
            context.host = host;
            context.port = port;
            context.factory = &factory;
            context.fps = data.fps;
            context.color_format = data.color_format;
            context.depth_format = data.depth_format;

            if (use_regions)
            {
                context.region_data = g_regions;
            } else
            {
                camera::region_vec_t vec;
                vec.push_back({ {0, 0, frame_width, frame_height}, 0, 0, 0});
                context.region_data = vec;
            }

            for(size_t i=0; i<cameras.size(); i++) 
            {
                cameras[i]->start(context);
            }

            // Wait for an end message
            done = control.wait_for_end();

            for (size_t i=0; i<cameras.size(); i++)
            {
                cameras.at(i)->join(false);
            }

            if(done) 
            {
                INFO("Quitting"); 
            } else
            {
                INFO("Looping");
            }
        }

    } catch (std::runtime_error& e)
    {
        ERROR_CPP(std::endl << "Caught runtime_error: " << e.what());
        ret = -1;
    } catch (std::exception& e)
    {
        ERROR_CPP(std::endl << "Caught exception: " << e.what());
        ret = -1;
    } catch(...)
    {
        ERROR_CPP(std::endl << "Unknown exception!!");
        ret = -1;
    }

    // Make sure the openni system is shutdown
    delete factory;

    return ret;
}
