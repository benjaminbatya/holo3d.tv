#include "pch_files.hpp"

#include "h3t_file.hpp"
#include "util_funcs.hpp"
#include "fps_counter.hpp"

#include "camera.hpp"

namespace holo3d
{


camera::camera(factory_base* factory, std::string uri)
: m_factory(factory)
, m_uri(uri)
, m_service()
, m_socket(m_service, 0, 0)
, m_done(false)
, m_producer_fps("PRODUCER:" + uri)
, m_consumer_fps("CONSUMER:" + uri)
{
}

camera::~camera()
{
    delete [] m_depth_buffer;
    delete [] m_color_buffer;

    // Make sure the socket closes properly
    m_socket.close();
}

void camera::start(const context_t& context)
{
    // Save the context
    m_context = context;

    // Setup the encoders and buffers
    m_color_encoder = context.factory->color(context.color_format, m_factory->color_format());
    m_depth_encoder = context.factory->depth(context.depth_format, m_factory->depth_format());

    size_t buf_size = m_color_encoder->width() * m_color_encoder->height() * sizeof(holo3d::RGB888Pixel);
    delete [] m_color_buffer;
    m_color_buffer = new uint8_t[buf_size];
    delete [] m_depth_buffer;
    m_depth_buffer = new uint8_t[buf_size];

    m_file_header.width = m_color_encoder->width();
    m_file_header.height = m_color_encoder->height();
    m_file_header.color_format = m_color_encoder->output_format();
    m_file_header.depth_format = m_depth_encoder->output_format();

    INFO_CPP("Camera URI='" << m_uri << "', host '" << context.host << "', port=" << context.port << 
             "\n\tResolution=" << m_file_header.width << "x" << m_file_header.height << ", fps=" << context.fps <<
             "\n\tColor format=" << m_file_header.color_format << ", Depth format=" << m_file_header.depth_format);

    if (m_context.region_data[0].rect.is_empty() == false) 
    {
        INFO("Using region %1%", m_context.region_data[0].rect);
    }

    // Setup the socket's endpoint
    udp::resolver resolver(m_service);
    udp::resolver::query query(udp::v4(), context.host, context.port);
    m_socket.endpoint() = *resolver.resolve(query);

    // Get the local ip
    boost::system::error_code ec;
    ec = m_socket.open();
    ASSERT_THROW(!ec, ec.message());
    ec = m_socket.connect();
    ASSERT_THROW(!ec, ec.message());
    boost::asio::ip::address local_addr = m_socket.local_endpoint().address();
    // INFO("local endpoint = %1%", m_socket.local_endpoint());
    ec = m_socket.close();
    ASSERT_THROW(!ec, ec.message());

    // First send a NEW_CAMERA_REQUEST
    NEW_CAMERA_REQUEST request;
    request.ip_address = local_addr.to_v4().to_ulong();

    ec = m_socket.open();
    ASSERT_THROW(!ec, ec.message());

    INFO("Sending %1% to %2%", request, m_socket.endpoint());

    send_data(&request, sizeof(request));

    boost::thread wait_thread(boost::bind(&camera::wait_for_reply, this));
}

void camera::wait_for_reply()
{
    INFO("Waiting for reply from %1%, localport=%2%", m_socket.endpoint(), m_socket.local_endpoint().port());

    boost::system::error_code ec;

    // Get the reply
    NEW_CAMERA_REPLY reply;
    receive_data(&reply, sizeof(reply));
    
    INFO("Got reply = %1% from %2%", reply, m_socket.endpoint());

    // Reset the port
    ec = m_socket.close();
    ASSERT_THROW(!ec, ec.message());

    m_socket.endpoint().port((unsigned short)reply.assigned_port);
    
    uint64_t sleep_for = reply.start_time - get_time();
    
    INFO("Got reply from %1%: assigned port=%2%, sleep_for=%3%", m_socket.endpoint(), reply.assigned_port, sleep_for);

    boost::this_thread::sleep_for(boost::chrono::microseconds(sleep_for));

    m_done = false;

    m_producer_count = 0;
    m_consumer_count = 0;

    m_producer_fps.reset();
    m_consumer_fps.reset();

    // start the producer and consumer threads.
    m_producer_thread = boost::thread(boost::bind(&camera::producer_run, this));
    m_consumer_thread = boost::thread(boost::bind(&camera::consumer_run, this));
}

void camera::join(bool flush_remaining_frames)
{
    INFO("Called");
    m_done = true;
    m_flush_remaining_frames = flush_remaining_frames;

    m_producer_thread.join();

    m_consumer_thread.join(); 

    INFO("produced %d frames.", m_producer_count);
    INFO("consumed %d frames.", m_consumer_count);
}

void camera::producer_run()
{
    factory_base::device_ptr_t device = m_factory->device(m_context.fps, m_color_encoder->width(), m_uri);

    if(device == nullptr)
    {
        ERROR("Failed to create device!");
        return;
    }

    camera_frame current_frame;
    current_frame.file_header(m_file_header);

    while (!m_done)
    {
        bool success = device->read_frame(current_frame);

        if (success)
        {
            // Set the correct frame number
            static uint16_t g_num_frames = 0;
            current_frame.frame_number(g_num_frames++);
        
            if(!m_queue.push(current_frame))
            {
                ERROR("error pushing frame at %u, queue was full", current_frame.color_time_stamp());
            } else
            {
                // Only count the frame as successfully produced if it was added to the queue
                m_producer_count++;
            }

//          INFO("producer_run: sent frame %d: time_stamp=%ld.%ld",
//               current_frame.frame_number,
//               current_frame.time_stamp/MICROS_PER_SEC,
//               current_frame.time_stamp%MICROS_PER_SEC);

            // NOTE: Should this be updated always or only when current_frame is successfully added to the queue??
            m_producer_fps.update();
               
//          INFO("Sent a frame");
        } else
        {
            ERROR("device->read_frame() failed");
        }

        current_frame.reset();
    }
}

void camera::consumer_run(void)
{
    m_socket.open();
    // m_socket.connect();

    // INFO_CPP("sending file header = " << m_file_header)

    // Send the stream header
    send_data(&m_file_header, sizeof(m_file_header));

    while (!m_done)
    {
        write_frames(false);

        // Sleep for 2msec if no frames are available
        boost::this_thread::sleep(boost::posix_time::milliseconds(2));
    }

    // Clear out the queue if dont need to quit immediately
    if (m_flush_remaining_frames) 
    {
        write_frames(true);
    }

    // Send the m_finished message
    FRAME_HEADER header;
    memset(&header, 0, sizeof(FRAME_HEADER));
    // A frame number of (uint64_t)-1 indicates the end of a stream
    header.frame_number = END_STEAM_MESSAGE;
    send_data(&header, sizeof(FRAME_HEADER));

    m_socket.close();
}

void camera::write_frames(bool flushing)
{
    camera_frame current_frame;
    while (m_queue.pop(current_frame))
    {
        if (m_done && !flushing) 
        {
            break;
        }

        size_t color_buffer_size = m_color_encoder->encode(m_color_buffer, 
                                                           current_frame.color_buffer(), 
                                                           m_color_encoder->width() * m_color_encoder->height() * sizeof(holo3d::RGB888Pixel), 
                                                           current_frame.color_buffer_size());
        size_t depth_buffer_size = m_depth_encoder->encode(m_depth_buffer, 
                                                           current_frame.depth_buffer(), 
                                                           m_color_encoder->width() * m_color_encoder->height() * sizeof(holo3d::RGB888Pixel), 
                                                           current_frame.depth_buffer_size());
        
        FRAME_HEADER header = current_frame.frame_header();

        header.color_buffer_size = color_buffer_size;
        header.depth_buffer_size = depth_buffer_size;
        header.rect = m_context.region_data[0].rect;

        send_data(&header, sizeof(header));

        // NOTE: The send_data should be called immediately after each conversion/compression to avoid the extra memcpy
        send_data(m_color_buffer, color_buffer_size);
        send_data(m_depth_buffer, depth_buffer_size);

        m_consumer_count++;

        m_consumer_fps.update();

//      INFO("processed frame %d: time_stamp=%d.%d", current_frame.frame_number(),
//           (uint32_t)current_frame.color_time_stamp()/MICROS_PER_SEC,
//           (uint32_t)current_frame.color_time_stamp()%MICROS_PER_SEC);

    }
}

void camera::send_data(const void* data, const size_t& amount_to_send)
{
//  INFO("Sending %1% of size %2%", data, amount_to_send);

    boost::system::error_code error;
    size_t ret = 
        m_socket.send(data, amount_to_send, error);
    ASSERT_THROW(!error, error.message()); // error occurred
    ASSERT_EQUAL(ret, amount_to_send);
}

void camera::receive_data(void* data, const size_t& amount_to_recv)
{
    boost::system::error_code error;
    size_t ret = 
        m_socket.receive(data, amount_to_recv, error);
    ASSERT_THROW(!error, error.message()); // error occurred
    ASSERT_EQUAL(ret, amount_to_recv);
}

}; // namespace holo3d
