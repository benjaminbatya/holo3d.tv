#ifndef _CLIENT_HPP_
#define _CLIENT_HPP_

#include "h3t_file.hpp"
#include "fps_counter.hpp"
#include "encoder.hpp"
#include "frame.hpp"
#include "fragmenting_socket.hpp"
#include "factory_base.hpp"

namespace holo3d 
{

using boost::asio::ip::udp;


static const int MAX_QUEUE_SIZE = 128;
typedef boost::lockfree::spsc_queue<camera_frame, boost::lockfree::capacity<MAX_QUEUE_SIZE>> QUEUE_TYPE;

class camera
{
public:

    struct RegionData
    {
        RECT rect;
        uint16_t tracker_id;
        int16_t v_x;
        int16_t v_y;
    };

    typedef std::vector<RegionData> region_vec_t;

    struct context_t
    {
        std::string host; 
        std::string port;
        encoder::factory* factory;
        size_t          fps;
        STREAM_FORMAT   color_format;
        STREAM_FORMAT   depth_format;

        region_vec_t    region_data;
    };

    camera(factory_base* factory, std::string uri = "");
    ~camera();

    void start(const context_t& context);

    void set_done() { m_done = true; }

    void join(bool flush_remaining_frames = false);

protected:

    void wait_for_reply();

    void producer_run();
    void consumer_run();

    void write_frames(bool flushing);
    void send_data(const void* data, const size_t& amount);
    void receive_data(void* data, const size_t& amount);

    factory_base*           m_factory;
    std::string             m_uri;

    boost::asio::io_service m_service;

    fragmenting_socket      m_socket;

    encoder::color_encoder_t m_color_encoder;
    encoder::depth_encoder_t m_depth_encoder;

    uint8_t*                m_color_buffer = nullptr;
    uint8_t*                m_depth_buffer = nullptr; 

    context_t               m_context;

    boost::atomic_bool      m_done;      // << Flag to indicate that the producer thread is done

    QUEUE_TYPE              m_queue;            // << The queue which passes frames from the consumer to the producer

    int                     m_producer_count = 0;   // << The number of frames successfully processed by the producer
    int                     m_consumer_count = 0;   // << The number of frames successfully processed by the consumer

    fps_counter             m_producer_fps;
    fps_counter             m_consumer_fps;

    STREAM_HEADER           m_file_header;

    boost::thread           m_producer_thread;
    boost::thread           m_consumer_thread;

    bool                    m_flush_remaining_frames = false;
};

}; // namespace holo3d


#endif // _CLIENT_HPP_
