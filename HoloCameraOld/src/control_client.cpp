#include "pch_files.hpp"

#include "control_client.hpp"
#include "util_funcs.hpp"

#include <thread>

namespace holo3d
{

ControlClient::ControlClient()
: m_service()
, m_delay(boost::posix_time::milliseconds(33))
, m_timer (m_service, m_delay)
, m_thread([&] () { m_service.run(); })
{
    boost::system::error_code ec;

    m_socket.endpoint().port(atoi(holo3d::protocol::DEFAULT_PORT.c_str()));

    ec = m_socket.bind();
    ASSERT_THROW_ALWAYS(!ec, ec.message());

    // Set the maximum possible control message size for the sidecar
    m_socket.set_sidecar_buf_size(1024);
}

ControlClient::~ControlClient()
{
    boost::system::error_code ec;
    ec = m_socket.close();
    ASSERT_THROW_ALWAYS(!ec, ec.message());

    m_service.stop();

    m_thread.join();
}

bool ControlClient::handle_message(message_func_t fn)
{
    m_service.reset();

    do_wait();
    
    fn();

    while (true) 
    {
        if (m_state == HandlerState::GOT_KEY_PRESS) 
        {
            m_socket.cancel();
            return true;
        } else if(m_state != HandlerState::WAITING)
        {
            m_timer.cancel();
            return false;
        } else
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
}


/**
 * waits for a query message to be sent or for the keyboard to 
 * be pressed. If the QUERY message is received, it replies with 
 * a QUERY_ACK and returns false. If a button is pressed, it 
 * returns false 
 * 
 * @author benjamin (4/16/2015)
 * @param  
 *   
 * @return bool true if a button is pressed, false if a query 
 *         message was received
 */
bool ControlClient::wait_for_query()
{
//  INFO("Called");

    m_state = HandlerState::WAITING;

    return handle_message([&]
    {
        protocol::Receive::async_basic(m_socket, protocol::ID::QUERY, [&]
        {
            protocol::Send::async_basic(m_socket, protocol::ID::QUERY_ACK, [&]
            {
                m_state = HandlerState::GOT_QUERY;
            });
        });
    });
}

bool ControlClient::process_start(protocol::StartData& data)
{
    return handle_message([&]
    {
        m_socket.async_receive(m_socket.sidecar_buf(), m_socket.sidecar_buf_size(), [&](const boost::system::error_code& ec, size_t bytes)
        {
            if (ec == boost::asio::error::operation_aborted) 
            {
                INFO("Operation aborted");
                return;
            }
            ASSERT_ALWAYS(!ec);
            ASSERT_ALWAYS(bytes >= sizeof(protocol::BaseData));

            protocol::BaseData* msg = (protocol::BaseData*)m_socket.sidecar_buf();
            switch (msg->id)
            { 
            // Process QUERY and END at all times. Just send back acks
            case protocol::ID::QUERY:
                INFO("Got query message");

                protocol::Send::basic(m_socket, protocol::ID::QUERY_ACK);

                m_state = HandlerState::GOT_QUERY;
                break;
            case protocol::ID::END:
                INFO("Got end message");

                protocol::Send::basic(m_socket, protocol::ID::END_ACK);

                m_state = HandlerState::GOT_END;
                break;
            // Process and accept start messages
            case protocol::ID::START:
            {
                INFO("Got start message");

                ASSERT_ALWAYS(bytes == sizeof(protocol::StartData));

                protocol::StartData* start_msg = (protocol::StartData*)msg;
                data = *start_msg;

                protocol::Send::basic(m_socket, protocol::ID::START_ACK);

                m_state = HandlerState::GOT_START;

                break;
            }
            default:
                // Throw on invalid protocol ids. Maybe this should be less strict
                THROW("Invalid protocol id %d", (uint8_t)msg->id);
                break;
            }
       }); 
    });
}

bool ControlClient::wait_for_start(protocol::StartData& data)
{
    INFO("Called");

    do
    {
        m_state = HandlerState::WAITING;
        process_start(data);
    } while (m_state != HandlerState::GOT_START && m_state != HandlerState::GOT_KEY_PRESS);

    return m_state == HandlerState::GOT_KEY_PRESS;
}

bool ControlClient::wait_for_end()
{
    // INFO("Called");

    m_state = HandlerState::WAITING;

    return handle_message([&] 
    {
        protocol::Receive::async_basic(m_socket, protocol::ID::END, [&]
        {
            protocol::Send::async_basic(m_socket, protocol::ID::END_ACK, [&]
            {
                m_state = HandlerState::GOT_END;
            });
        });
    });
}


void ControlClient::do_wait()
{
    m_timer.expires_at(m_timer.expires_at() + m_delay);
    m_timer.async_wait([&](const boost::system::error_code& ec) { this->check_keys(ec); });
}

void ControlClient::check_keys(const boost::system::error_code& error)
{
    // an aborted operation is ok, just return
    if (error == boost::asio::error::operation_aborted) 
    {
//      ERROR("operation cancelled");
        return;
    }

    // Throw on any other error
    ASSERT_THROW(!error, "got error: %s", error.message()); 

    if(wasKeyboardHit())
    {
        m_state = HandlerState::GOT_KEY_PRESS;
    } else
    {
        this->do_wait();
    }
}

}; // namespace holo3d
