#ifndef __CONTROL_CLIENT_HPP__
#define __CONTROL_CLIENT_HPP__

#include <thread>

#include "control_protocol.hpp"
#include "fragmenting_socket.hpp"

namespace holo3d
{

class ControlClient
{
public:
    ControlClient();
    ~ControlClient();

    bool wait_for_query();

    bool wait_for_start(protocol::StartData& data);

    bool wait_for_end();

protected:

    // Different handler states
    enum class HandlerState : uint8_t
    {
        WAITING = 0
        , GOT_KEY_PRESS
        , GOT_QUERY
        , GOT_START
        , GOT_END
    };

    typedef std::function<void(void)> message_func_t;
    bool handle_message(message_func_t);

    bool process_start(protocol::StartData& data);

    void check_keys(const boost::system::error_code& error);
    void do_wait();

    boost::asio::io_service             m_service;
    boost::asio::io_service::work       m_work { m_service };

    fragmenting_socket                  m_socket { m_service, 0, 0 };

    boost::posix_time::time_duration    m_delay;
    boost::asio::deadline_timer         m_timer;

    std::thread                         m_thread;

    HandlerState                        m_state = HandlerState::WAITING;
};


}; // namespace holo3d


#endif // __CONTROL_CLIENT_HPP__ 
