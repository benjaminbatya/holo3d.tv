// Main.cpp for testViewer that displays the OpenNI2 color+depth video streams in 3d or flat

#include "pch_files.hpp"

#include <stdio.h>
#include <sstream>
#include <cstring>

namespace po = boost::program_options;

#include "util_funcs.hpp"
#include "h3t_file.hpp"

#include "SetupDialog.hpp"

void display_keys();

int main(int argc, char **argv)
{    
    try
    {
        std::string port = holo3d::DEFAULT_PORT;
        std::string app_name = boost::filesystem::basename(argv[0]);
        po::options_description visible(std::string("Usage: ") + app_name + " <options>\nAllowed Options");
        visible.add_options()
        ("help,h",                                                                      "produce help message")
        ("port,p",      po::value<std::string>(&port)->default_value(port),             "Port to listen for incoming video streams") 
        ;

        po::options_description all;
        all.add(visible);

        po::variables_map vm;
        try
        {
            po::store(po::command_line_parser(argc, argv).options(all).run(), vm);

            if (vm.count("help"))
            {
                INFO_CPP(visible);
                return 0;
            }

            po::notify(vm);
        }
        catch(boost::program_options::required_option& e) 
        { 
            ERROR_CPP("No movie specified!");
            INFO_CPP(visible);
            return -1; 
        } 
        catch (po::error& e)
        {
            ERROR_CPP(e.what());
            INFO_CPP(visible);
            return -1;
        }

        QApplication app(argc, argv);

        holo3d::SetupDialog viewer(port);

        // never returns from run
        viewer.show();

        display_keys();

        return app.exec();

    } catch (std::exception &e)
    {
        ERROR_CPP(std::endl << e.what());
        return -1;
    }
}


void display_keys()
{
    INFO("Keys:");
    INFO("ESC - close window");
    INFO("1 - Display depth frames blended over color frames");
    INFO("2 - Display only color frames");
    INFO("3 - Display only depth frames");
    INFO("p - Pause/Unpause the movie");
    INFO("l - loop/don't loop the movie");
    INFO("h - toggle display histogram");
}

