// Implementation of TestViewer
// This only works on linux for now...

#include "pch_files.hpp"

#include <cstring>
#include <sstream>
#include <chrono>
#include <thread>

#include "util_funcs.hpp"
#include "fps_counter.hpp"

#include "Window.h"

namespace holo3d
{

class loader_base;

//#define GL_WIN_SIZE_X   1280
//#define GL_WIN_SIZE_Y   1024

#define DEFAULT_DISPLAY_MODE	DISPLAY_MODE_DEPTH

// Start public

Window::Window(const std::string& host, const std::string& port)
: QDialog(nullptr)
, m_host(host)
, m_thread(port, this)
{
    m_painter = new QPainter();

    qRegisterMetaType<QImage>("QImage");

    connect(&m_thread, SIGNAL(new_frame(QImage, QImage)), this,  SLOT(update_frame(QImage, QImage)));

//  connect(&m_thread, SIGNAL(end_stream()), this, SLOT(end_stream()));
}

Window::~Window()
{
    INFO("Closing the window...");
    m_thread.quit();

    delete m_painter;
}

void Window::show()
{
    QWidget::show();
}

void Window::update_frame(const QImage& color,  const QImage& depth)
{
    if (!m_paused) 
    {
        m_color_pixmap = QPixmap::fromImage(color); 
        if (!depth.isNull()) 
        {
            m_depth_pixmap = QPixmap::fromImage(depth); 
        } else
        {
//          INFO("Not setting the depth pixmap. depth.isNull() = %1%", depth.isNull());
        }
        
        update();
    }
}

void Window::end_stream()
{
    INFO("Received an end_stream signal");
    this->accept();
}

void Window::paintEvent(QPaintEvent* e)
{
    if (m_color_pixmap.isNull()) 
    {
        return;
    }

    //  INFO("Called");
    double scaleX = ((double)width()) / m_color_pixmap.width();
    double scaleY = ((double)height()) / m_color_pixmap.height();
    double scale = std::min(scaleX, scaleY);

    m_painter->begin(this);
    m_painter->save();
    m_painter->scale(scale, scale);
    
    if (m_depth_pixmap.isNull()) 
    {   
        m_painter->drawPixmap(0, 0, m_color_pixmap);
    } else
    {
        switch (m_view_state)
        {
        case DisplayModes::BLEND:
            m_painter->drawPixmap(0, 0, m_color_pixmap);
            m_painter->setCompositionMode(QPainter::CompositionMode_Multiply);
            m_painter->drawPixmap(0, 0, m_depth_pixmap);
            break;
        case DisplayModes::COLOR:
            m_painter->drawPixmap(0, 0, m_color_pixmap);
            break;
        case DisplayModes::DEPTH:
            m_painter->drawPixmap(0, 0, m_depth_pixmap);
            break;
        default:
            THROW("INVALID DisplayMode");
            break;
        }
    }
    m_painter->restore();
    m_painter->end();

    static fps_counter fps("paintEvent");
    fps.update();
}

void Window::keyReleaseEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Escape: // esc key
        close();
        break;

    case Qt::Key_1:
        m_view_state = DisplayModes::BLEND;
        update();
        break;

    case Qt::Key_2:
        m_view_state = DisplayModes::COLOR;
        update();
        break;

    case Qt::Key_3:
        m_view_state = DisplayModes::DEPTH;
        update();
        break;

    case Qt::Key_P:
        m_paused = !m_paused;
        break;

    case Qt::Key_H:
        set_use_histogram(!use_histogram());
        break;

//  case Qt::Key_M:
//      m_stream_manager.toggle_mirroring();
//      break;

    default:
        QWidget::keyReleaseEvent(event);
        break;
    }
}

// End public

}; // namespace holo3d


