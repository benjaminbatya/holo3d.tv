// SetupDialog.hpp - Displays the setup dialog for selecting the camera and modes to run the camera in

#ifndef __SETUP_DIALOG_HPP__
#define __SETUP_DIALOG_HPP__

#include <Qt/QtGui>

#include "ui_SetupDialog.h"

namespace holo3d
{

class Window;
class ControlServer;

class SetupDialog : public QDialog, private Ui_SetupDialog
{
    Q_OBJECT
public:
    SetupDialog(const std::string& port);
    virtual ~SetupDialog();

    void show();

protected slots:
    void select_video();
    void display_video();

protected:
    std::string m_port;
    Window* m_window = nullptr;

    ControlServer*   m_comm_channel;

    std::string m_current_host {};

};

}; // namespace holo3d

#endif // __SETUP_DIALOG_HPP__
