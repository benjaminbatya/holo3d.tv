#ifndef __PROCESS_THREAD_HPP__
#define __PROCESS_THREAD_HPP__

#include <QtCore/QThread>
#include <QtCore/QMutex>

#include "h3t_file.hpp"
#include "frame.hpp"
#include "decoder.hpp"

class QImage;

namespace holo3d
{

class loader_base;

class ProcessThread : public QThread
{
    Q_OBJECT
public:
    ProcessThread(const std::string& port, QObject* parent = 0);

    void quit();

    void set_use_histogram(bool f);
    bool use_histogram() const;

signals:
    void new_frame(const QImage& color, const QImage& depth);   // << emitted when a new frame is received
    void end_stream();                                          // << emitted when the end of the stream is received

protected:
    void run();

    void handle_load();

private:
    loader_base*    m_loader;

    camera_frame    m_frame;

    size_t          m_texmap_x = 0;
    size_t          m_texmap_y = 0;
    RGB888Pixel*    m_color_texmap = nullptr;
    RGB888Pixel*    m_depth_texmap = nullptr;

    size_t          m_width = 0;
    size_t          m_height = 0;

    decoder::color_decoder_t m_color_decoder;
    decoder::depth_decoder_t m_depth_decoder;

    bool            m_use_histogram = true;
    bool            m_done = false;

    QMutex          m_mutex;
};


} // namespace holo3d


#endif // __PROCESS_THREAD_HPP__
