// Window.h - Class containing the glut functionality

#ifndef _WINDOW_H_
#define _WINDOW_H_

#include <memory>

#include <Qt/QtGui>

#include "ProcessThread.hpp"

namespace holo3d
{

// Forward decl
class loader_base;

enum class DisplayModes
{
    BLEND = 1,
    COLOR = 2,
    DEPTH = 3,

    // These are to allow iterating over the enums
    MIN = BLEND,
    MAX = DEPTH,
    COUNT,
};

inline std::ostream& operator<<(std::ostream& os, const DisplayModes& mode)
{
#define OUTPUT_MODE(e) case DisplayModes::e : os << #e; break;

    switch (mode)
    {
    OUTPUT_MODE(DEPTH);
    OUTPUT_MODE(COLOR);
    OUTPUT_MODE(BLEND);
    default: os << "Unknown display mode"; break;
    }
#undef OUTPUT_MODE

    return os;
}
inline std::istream& operator>>(std::istream& is, DisplayModes& mode)
{
    std::string input;
    is >> input;

#define INPUT_MODE(m) if (input == #m) { mode = DisplayModes::m; }

    INPUT_MODE(DEPTH)
    else INPUT_MODE(COLOR)
    else INPUT_MODE(BLEND)
    else
    {
        throw ("Unknown display mode");
    }
#undef INPUT_MODE

    return is;
}

class Window : public QDialog
{
    Q_OBJECT

public:

    Window(const std::string& host, const std::string& port);
    virtual ~Window(); // Window is not intended to be inherited from

//  void connect_to_camera(const std::string& camera_ip);

    void show(); 

    /**
     * Setter/getter combo for m_use_histogram. Call this as 
     * window->use_histogram(true) or window->use_histogram()
     *  
     * @author benjamin (11/10/2014)
     * 
     * @param use sets m_use_histogram
     * 
     * @return bool the value of m_use_histogram
     */
    bool use_histogram() const { return m_thread.use_histogram(); }
    void set_use_histogram(bool f) { m_thread.set_use_histogram(f); }
    DisplayModes video_mode() const { return m_view_state; }
    void set_video_mode(const DisplayModes& mode) { m_view_state = mode; }

protected slots:
    void update_frame(const QImage& color,  const QImage& depth);
    void end_stream();
//  void stop_camera();

protected:

    void paintEvent(QPaintEvent* e);

    void keyReleaseEvent(QKeyEvent *event);

    std::string     m_host;

    QPainter*       m_painter = nullptr;

    DisplayModes    m_view_state = DisplayModes::BLEND;

    QPixmap         m_color_pixmap;
    QPixmap         m_depth_pixmap;

    bool            m_has_frame = false;

    ProcessThread   m_thread;

    bool            m_paused = false;
};

}; // namespace holo3d
   
#endif // _WINDOW_H_
