#include "pch_files.hpp"

#include "util_funcs.hpp"
#include "h3t_file.hpp"
#include "option_enums.hpp"
#include "loader.hpp"
#include "encoder.hpp"

#include "SetupDialog.hpp"
#include "Window.h"
#include "control_server.hpp"

namespace holo3d
{

SetupDialog::SetupDialog(const std::string& port)
: QDialog(nullptr)
, m_port(port)
, m_comm_channel(new ControlServer)
{
    setupUi(this);

    connect(button_display_video, SIGNAL(clicked()), this, SLOT(display_video()));
    connect(button_quit, SIGNAL(clicked()), this, SLOT(close()));
}

SetupDialog::~SetupDialog()
{
    delete m_comm_channel;
}

void SetupDialog::show()
{   
    // Populate the lists

//  QMessageBox* msg_box = new QMessageBox();
//  msg_box->setText(tr("Finding the available cameras"));
//  msg_box->setAttribute(Qt::WA_DeleteOnClose);
//  msg_box->setStandardButtons(QMessageBox::NoButton);
//  msg_box->setWindowTitle(tr("Please wait"));
//  msg_box->setModal(false);
//  msg_box->open();

    typedef std::vector<std::string> string_vec;

    string_vec possible_cameras = { "localhost", "camera1", "camera2", "camera3", "camera4" };

    string_vec found_cameras;
    for (const std::string& cam: possible_cameras)
    {
        if(m_comm_channel->query(cam))
        {
            found_cameras.push_back(cam);
        }
    }

    for (const auto& cam : found_cameras)
    {
        QListWidgetItem* item = new QListWidgetItem(tr(cam.c_str()));
        list_cameras_available->addItem( item );
    }

    // Select the first host in the list
    QListWidgetItem* item = list_cameras_available->item(0);
    list_cameras_available->setCurrentItem(item);

    typedef std::vector<std::tuple<std::string, uint8_t>> tuple_vec;

    tuple_vec color_formats;
    for (uint8_t i=(uint8_t)COLOR_MODE::MIN; i<=(uint8_t)COLOR_MODE::MAX; i++) 
    {
        std::stringstream ss;
        ss << (COLOR_MODE)i;
        color_formats.push_back(std::make_tuple(ss.str(), i));
    }

    for (const auto& format: color_formats) 
    {
        QListWidgetItem* item = new QListWidgetItem(tr(std::get<0>(format).c_str()));
        item->setData(Qt::UserRole, std::get<1>(format));
        list_color_stream_types->addItem( item );
        // Default is JPEG for now...
        if(std::get<1>(format) == (uint8_t)STREAM_FORMAT::COLOR_JPEG)
        {
            list_color_stream_types->setCurrentItem(item);
        }
    }

    tuple_vec depth_formats;
    for (uint8_t i=(uint8_t)DEPTH_MODE::MIN; i<=(uint8_t)DEPTH_MODE::MAX; i++) 
    {
        std::stringstream ss;
        ss << (DEPTH_MODE)i;
        depth_formats.push_back(std::make_tuple(ss.str(), i));
    }

    for (const auto& format: depth_formats) 
    {
        QListWidgetItem* item = new QListWidgetItem(tr(std::get<0>(format).c_str()));
        item->setData(Qt::UserRole, std::get<1>(format));
        list_depth_stream_types->addItem( item );
        // Default is PNG for now...
        if(std::get<1>(format) == (uint8_t)STREAM_FORMAT::DEPTH_PNG)
        {
            list_depth_stream_types->setCurrentItem(item);
        }
    }

    auto resolutions = 
    { std::make_tuple("Small (320x240)", (uint8_t)RESOLUTION::SMALL)
    , std::make_tuple("Medium (640x480)", (uint8_t)RESOLUTION::MEDUIM)
    //, {"Large (1024x768)", RESOLUTION::LARGE}
    };
    for (auto& it: resolutions) 
    {
        resolution_combo->addItem(std::get<0>(it), std::get<1>(it)); 
    }

    // Default is medium for now...
    resolution_combo->setCurrentIndex(1);

    auto qualities = 
    { std::make_tuple("Realtime", (uint8_t)QUALITY::REALTIME)
    , std::make_tuple("Average", (uint8_t)QUALITY::AVERAGE)
    , std::make_tuple("Good", (uint8_t)QUALITY::GOOD)
    , std::make_tuple("Best", (uint8_t)QUALITY::BEST)
    };
    for (auto& it: qualities) 
    {
        quality_combo->addItem(std::get<0>(it), std::get<1>(it)); 
    }
    // Default is Good for now...
    quality_combo->setCurrentIndex(2);

    fps_spin_box->setValue(30);

//  msg_box->close();

    QDialog::show();
}

void SetupDialog::select_video()
{
    INFO("Called");
    if (m_window) 
    {
        INFO("Sending an end message to the camera");
        m_comm_channel->end(m_current_host);

        delete m_window; 
        m_window = nullptr;
    }

}

void SetupDialog::display_video()
{
    // Get the selected host
    QList<QListWidgetItem*> list = list_cameras_available->selectedItems();
    if (list.length() < 1) 
    {
        QMessageBox::critical(this, "Invalid Selection", "You must select a camera to use!");
        return;
    }

    m_current_host = list.at(0)->text().toStdString();

    // Get the hints from the lists
    protocol::StartData hints;

    int index = resolution_combo->currentIndex();
    hints.resolution = (RESOLUTION)resolution_combo->itemData(index).toInt();

    QListWidgetItem* item = list_color_stream_types->currentItem();
    hints.color_format = (STREAM_FORMAT)item->data(Qt::UserRole).toInt();

    item = list_depth_stream_types->currentItem();
    hints.depth_format = (STREAM_FORMAT)item->data(Qt::UserRole).toInt();

    hints.fps = (uint8_t)fps_spin_box->value();

    index = quality_combo->currentIndex();
    hints.quality = (QUALITY)quality_combo->itemData(index).toInt();

    m_window = new Window(m_current_host, m_port);

    connect(m_window, SIGNAL(accepted()), this, SLOT(select_video()));
    connect(m_window, SIGNAL(rejected()), this, SLOT(select_video()));

    m_window->show();

    m_comm_channel->start(m_current_host, hints);
}

}; // namespace holo3d
