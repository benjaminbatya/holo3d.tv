#include "pch_files.hpp"

#include "ProcessThread.hpp"

#include <QtGui/QImage>

#include "loader.hpp"
#include "util_funcs.hpp"
#include "h3t_file.hpp"
#include "fps_counter.hpp"

namespace holo3d
{

#define TEXTURE_SIZE    512

#define MIN_NUM_CHUNKS(data_size, chunk_size) ((((data_size) - 1) / (chunk_size) + 1))
#define MIN_CHUNKS_SIZE(data_size, chunk_size) (MIN_NUM_CHUNKS(data_size, chunk_size) * (chunk_size))

ProcessThread::ProcessThread(const std::string& port, QObject* parent)
: QThread(parent)
, m_loader( new udp_loader(port) )
{
    m_loader->load_handler([&] () 
    { 
        this->handle_load(); 
    });

    m_loader->set_end_stream_handler([&] () 
    { 
        INFO("Got end of stream signal");
//      m_done = true;      // Set the done flag
//      wait(20);       // Wait for the other threads to finish. NOTE: Not sure if this works or not!!!
        emit end_stream();
    });

    m_loader->run();
}

void ProcessThread::quit()
{
//  INFO("Called");
    if (!m_done)
    {
        m_done = true;      // Set the done flag
        wait(20);           // Wait for a few millis
    }
    QThread::quit();    // Finalize the quitting of the thread

    delete m_loader;
}


bool ProcessThread::use_histogram() const 
{ 
    return m_depth_decoder && m_depth_decoder->use_histogram(); 
}

void ProcessThread::set_use_histogram(bool f) 
{ 
    if (m_depth_decoder) 
    {
        QMutexLocker locker(&m_mutex);
        m_depth_decoder->use_histogram(f); 
    }
}

void ProcessThread::handle_load()
{
//  INFO("called");
    // m_movie_manager must be initialized by this point.
    ASSERT(m_loader->valid());

    m_width = m_loader->width();
    m_height = m_loader->height();

    // Texture map init
    m_texmap_x = MIN_CHUNKS_SIZE(m_width, TEXTURE_SIZE);
    m_texmap_y = MIN_CHUNKS_SIZE(m_height, TEXTURE_SIZE);
    m_color_texmap = new RGB888Pixel[m_texmap_x * m_texmap_y];
    ASSERT(m_color_texmap);
    m_depth_texmap = new RGB888Pixel[m_texmap_x * m_texmap_y];
    ASSERT(m_depth_texmap);

    decoder::factory factory(m_width, m_height, m_texmap_x);
    m_color_decoder = factory.color(m_loader->file_header().color_format);
    m_depth_decoder = factory.depth(m_loader->file_header().depth_format, use_histogram());

    INFO("playing '%1%' at %2%x%3%, color format=%4%, depth format=%5%",
         m_loader->connection_info(), m_width, m_height,
         m_loader->file_header().color_format,
         m_loader->file_header().depth_format);

    QThread::start(HighestPriority);
}

void ProcessThread::run()
{
    while (!m_done) 
    {
        if (m_loader->valid())
        {
            // Always get the most recent frame
            bool have_frame = false;
            bool ret = true;
            while (ret) 
            {
                ret = m_loader->next_frame(m_frame); 
                have_frame |= ret;
            }

            if (have_frame)
            {
//              INFO("got frame");
                ASSERT_ALWAYS(m_frame.valid());

                // INFO("TestViewer::display_blended: m_color_frame.isValid()=%d", m_color_frame.isValid());
                {
                    // Make sure non of the state can change while decoding
                    QMutexLocker locker(&m_mutex);

                    // Draw the color buffer
                    m_color_decoder->decode(m_color_texmap, (uint8_t*)m_frame.color_buffer(), sizeof(RGB888Pixel) * m_texmap_x * m_texmap_y, m_frame.color_buffer_size());

                    // Draw out the depth buffer
                    m_depth_decoder->decode(m_depth_texmap, (uint8_t*)m_frame.depth_buffer(), sizeof(RGB888Pixel) * m_texmap_x * m_texmap_y, m_frame.depth_buffer_size());
                }

                QImage color_image = QImage((uint8_t*)m_color_texmap,  m_frame.width(),  m_frame.height(), m_texmap_x*sizeof(RGB888Pixel), QImage::Format_RGB888);

                QImage depth_image;
                if (m_frame.file_header().has_depth()) 
                {
                    depth_image = QImage((uint8_t*)m_depth_texmap,  m_frame.width(),  m_frame.height(), m_texmap_x*sizeof(RGB888Pixel), QImage::Format_RGB888);
                } else
                {
//                  INFO("Not setting the depth_image! depth_image.isNull() = %1%", depth_image.isNull());
                }

                emit new_frame(color_image, depth_image);

                static fps_counter fps("ProcessThread::run");
                fps.update();
            } else
            {
//              INFO("no frame available");
            }
        }

        QThread::msleep(1); // Always sleep before checking for a new frame
    }
}

} // namespace holo3d

