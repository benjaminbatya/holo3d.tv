#pragma once

#include "h3t_file.hpp"

// forward decls
struct vpx_codec_ctx;
typedef vpx_codec_ctx vpx_codec_ctx_t;

struct vpx_codec_enc_cfg;
typedef vpx_codec_enc_cfg vpx_codec_enc_cfg_t;

namespace holo3d
{

namespace encoder
{

// Forward decl
class base;
namespace depth { class base; }

typedef std::shared_ptr<base> color_encoder_t;
typedef std::shared_ptr<depth::base> depth_encoder_t;

class factory
{
public:
    /**
     * Ctor for the encoder factory
     * 
     * @author benjamin (12/3/2014)
     * 
     * @param width of the input frames
     * @param height of the input frames
     * @param quality desired quality of the compressed frames between 
     *      0 (worst quality -> maybe smallest frame size and/or fastest compression time) and 
     *      100 (best quality -> largest frame size and/or worst compression time)
     */
    factory(size_t width, size_t height, const QUALITY quality, const uint16_t fps);

    color_encoder_t color(STREAM_FORMAT output_format, STREAM_FORMAT input_format);
    depth_encoder_t depth(STREAM_FORMAT output_format, STREAM_FORMAT input_format);

protected:

    void configure(color_encoder_t encoder, 
                   STREAM_FORMAT output_format, 
                   STREAM_FORMAT input_format);

    const size_t m_width;
    const size_t m_height;
    const QUALITY m_quality;
    const uint16_t m_fps;
};

class base
{
protected:

    // Only factory can build a decoder
    friend class factory;

    base() { }

    virtual ~base();

public:

    // Encodes a frame, returns the size of the encoded output buffer
    virtual size_t encode(uint8_t* output, const uint8_t* input, 
                          const size_t output_buffer_size, const size_t input_buffer_size,
                          RECT clipping_rect = {}) = 0; 

    inline size_t width() const { return m_width; }
    inline size_t height() const { return m_height;}
    inline size_t quality() const { return m_quality; }
    inline uint16_t fps() const { return m_fps; }
    inline size_t num_frame_pixels() const { return m_width * m_height; }

    inline STREAM_FORMAT input_format() const { return m_input_format; }
    inline STREAM_FORMAT output_format() const { return m_output_format; }

    inline size_t input_depth() const { return get_pixel_depth(input_format()); }
    inline size_t output_depth() const { return get_pixel_depth(output_format()); }

protected:

    virtual void init(const QUALITY m_quality);

    size_t convert_to_yv12(uint8_t* output, const uint8_t* input, const RECT& rect);

    inline size_t work_buf_size() { return width() * height() * sizeof(RGB888Pixel); }
    uint8_t* work_buf();
            
    inline void check_rect(RECT& rect) const;
    inline const uint8_t* calc_input_offset(const uint8_t* ptr, const RECT& rect) const;           
                    
    size_t m_width = 0;
    size_t m_height = 0;
    size_t m_quality;
    uint16_t m_fps = 0;

    STREAM_FORMAT m_input_format;
    STREAM_FORMAT m_output_format; 

    uint8_t* m_work_buf = nullptr;
};

namespace color
{

class rgb888 : public ::holo3d::encoder::base
{
public:
    size_t encode(uint8_t* output, const uint8_t* input, 
                  const size_t output_buffer_size, const size_t input_buffer_size,
                  RECT clipping_rect = {}) override;
};

class yuv422 : public ::holo3d::encoder::base
{
public:
    size_t encode(uint8_t* output, const uint8_t* input, 
                  const size_t output_buffer_size, const size_t input_buffer_size,
                  RECT clipping_rect = {}) override;
};

class yuyv : public ::holo3d::encoder::base
{
public:
    size_t encode(uint8_t* output, const uint8_t* input, 
                  const size_t output_buffer_size, const size_t input_buffer_size,
                  RECT clipping_rect = {}) override;
};

class yv12 : public ::holo3d::encoder::base
{
public:
    size_t encode(uint8_t* output, const uint8_t* input, 
                  const size_t output_buffer_size, const size_t input_buffer_size,
                  RECT clipping_rect = {}) override;
};

struct JPEGImpl;
class jpeg : public ::holo3d::encoder::base
{
public:
    size_t encode(uint8_t* output, const uint8_t* input, 
                  const size_t output_buffer_size, const size_t input_buffer_size,
                  RECT clipping_rect = {}) override;

    ~jpeg();

protected:
    friend class ::holo3d::encoder::factory;

    void init(const QUALITY m_quality) override;

    JPEGImpl* m_impl;
};

class vp8 : public ::holo3d::encoder::base
{
public:
    size_t encode(uint8_t* output, const uint8_t* input, 
                  const size_t output_buffer_size, const size_t input_buffer_size,
                  RECT clipping_rect = {}) override;

    ~vp8();

protected:
    friend class ::holo3d::encoder::factory;

    void init(const QUALITY m_quality) override;

    vpx_codec_ctx_t*    m_vpx_codec = nullptr;
    size_t              m_encoder_count = 0;

    vpx_codec_enc_cfg_t* m_config;
};
//
//class h264 : public ::holo3d::encoder::base
//{
//public:
//    size_t encode(uint8_t* output, const uint8_t* input,
//                  const size_t output_buffer_size, const size_t input_buffer_size,
//                  RECT clipping_rect = {}) override;
//
//    ~h264();
//
//protected:
//    friend class ::holo3d::encoder::factory;
//
//    void init(const QUALITY m_quality) override;
//
//    x264_param_t        m_params;
//    x264_nal_t*         m_nals = nullptr;
//    int                 m_num_nals = 0;
//    x264_t*             m_encoder = nullptr;
//
//    x264_picture_t      m_pic_in;
//    x264_picture_t      m_pic_out;
//};


}; // namespace color


namespace depth
{

class base : public ::holo3d::encoder::base
{
protected:

public:
    
protected:
    size_t encode_depth_frame(uint8_t* output, const uint8_t* input);
};

class invalid : public ::holo3d::encoder::depth::base
{
public:
    size_t encode(uint8_t *output, const uint8_t *input, 
                  const size_t output_buffer_size, const size_t input_buffer_size, 
                  RECT clipping_rect = {}) override;
};

class raw : public ::holo3d::encoder::depth::base
{
public:
    size_t encode(uint8_t* output, const uint8_t* input, 
                  const size_t output_buffer_size, const size_t input_buffer_size, 
                  RECT clipping_rect = {}) override;
};

class enc : public ::holo3d::encoder::depth::base
{
public:
    size_t encode(uint8_t* output, const uint8_t* input, 
                  const size_t output_buffer_size, const size_t input_buffer_size, 
                  RECT clipping_rect = {}) override;
};

class jpeg : public ::holo3d::encoder::depth::base
{
public:
    size_t encode(uint8_t* output, const uint8_t* input, 
                  const size_t output_buffer_size, const size_t input_buffer_size, 
                  RECT clipping_rect = {}) override;
    ~jpeg();

protected:
    friend class ::holo3d::encoder::factory;

    void init(const QUALITY m_quality) override;

    using tjhandle = void*;

    tjhandle m_jpeg_compressor = nullptr;
};

struct png_impl;

class png : public ::holo3d::encoder::depth::base
{
public:
    size_t encode(uint8_t* output, const uint8_t* input, 
                  const size_t output_buffer_size, const size_t input_buffer_size, 
                  RECT clipping_rect = {}) override;
    ~png();

protected:
    friend class ::holo3d::encoder::factory;

    void init(const QUALITY m_quality) override;

    friend struct png_impl;
    png_impl* m_impl = nullptr;
};

class vp8 : public ::holo3d::encoder::depth::base
{
public:
    size_t encode(uint8_t* output, const uint8_t* input, 
                  const size_t output_buffer_size, const size_t input_buffer_size, 
                  RECT clipping_rect = {}) override;
    ~vp8();

protected:
    friend class ::holo3d::encoder::factory;

    void init(const QUALITY m_quality) override;
    uint8_t* work_buf2();

    vpx_codec_ctx_t*    m_vpx_codec = nullptr;
    size_t              m_encoder_count = 0;
    uint8_t*            m_work_buf2 = nullptr;
};

}; // namespace depth

}; // namespace encoder

}; // namespace holo3d

