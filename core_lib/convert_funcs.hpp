#pragma once

// NOTE: This must be included after h3t_file.hpp and src/holo3d.pb.h!

namespace holo3d {

protocol::STREAM_FORMAT convert_format_l2p(STREAM_FORMAT fmt);
STREAM_FORMAT convert_format_p2l(protocol::STREAM_FORMAT fmt);

protocol::QUALITY convert_quality_l2p(QUALITY qual);
QUALITY convert_quality_p2l(protocol::QUALITY qual);

std::ostream& operator<<(std::ostream& os, protocol::MESSAGE_TYPE pf);

struct MessageHeader
{
    MessageHeader();
    MessageHeader(protocol::MESSAGE_TYPE type, uint16_t size, uint32_t data_size);

    uint8_t* serialize(uint8_t* buf, size_t buf_size) noexcept;
    uint8_t* deserialize(uint8_t* buf, size_t buf_size) noexcept;

    protocol::MESSAGE_TYPE type() { return protocol::MESSAGE_TYPE(msg_type); }
    
    uint16_t msg_type = 0;
    uint16_t msg_size = 0;
    uint32_t data_size = 0;
};

size_t serialize_msg(uint8_t* buf, size_t max_size, 
                     protocol::MESSAGE_TYPE msg_type, const ::google::protobuf::Message* msg = nullptr, 
                     uint8_t* data_buf = nullptr, uint32_t data_buf_size = 0);

}; // namespace holo3d