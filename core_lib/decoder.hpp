#ifndef _DECODER_FACTORY_HPP_
#define _DECODER_FACTORY_HPP_

// forward decls
typedef void* tjhandle;

struct vpx_codec_ctx;
typedef vpx_codec_ctx vpx_codec_ctx_t;

enum class STREAM_FORMAT;

#include "h3t_file.hpp"

namespace holo3d
{

namespace decoder
{

// Forward decl
class base;
namespace depth { class base; }

typedef std::shared_ptr<base> color_decoder_t;
typedef std::shared_ptr<depth::base> depth_decoder_t;

class factory
{
public:
    factory(size_t width, size_t height, size_t output_stride_in_pixels);

    color_decoder_t color(STREAM_FORMAT color_format);
    depth_decoder_t depth(STREAM_FORMAT depth_format, bool use_histogram = true, size_t histogram_size = 10000);

protected:
    size_t  m_width;
    size_t  m_height;
    size_t  m_output_stride_in_pixels;
};

class base
{
protected:

    // Only factory can build a decoder
    friend class factory;

    base() { }

    virtual ~base();

public:

    // Decodes a frame
    virtual void decode(holo3d::RGB888Pixel* output, 
                        const uint8_t* input, 
                        const size_t output_buffer_size_in_bytes, 
                        const size_t input_buffer_size_in_bytes,
                        const RECT& rect = {}) = 0; 

    inline size_t width() const { return m_width; }
    inline size_t height() const{ return m_height;}
    inline size_t num_pixels() const { return m_width * m_height; }

    inline size_t output_stride_in_pixels() const { return m_output_stride_in_pixels; }

    inline STREAM_FORMAT format() const { return m_format; }

protected:

    virtual void configure(size_t width, size_t height, 
                           size_t output_stride_in_pixels);
    inline void set_format(STREAM_FORMAT format) { m_format = format; }

    
    uint8_t* work_buf();
        
    size_t m_width = 0;
    size_t m_height= 0;
    size_t m_output_stride_in_pixels = 0;

    size_t  m_input_buffer_size_in_bytes = 0;

    uint8_t* m_work_buf = nullptr;

    STREAM_FORMAT   m_format = STREAM_FORMAT::INVALID;
};

namespace color
{

class rgb888 : public ::holo3d::decoder::base
{
public:
    void decode(holo3d::RGB888Pixel* output, 
                        const uint8_t* input, 
                        const size_t output_buffer_size_in_bytes, 
                        const size_t input_buffer_size_in_bytes,
                        const RECT& rect = {});
};

class yuv422 : public ::holo3d::decoder::base
{
public:
    void decode(holo3d::RGB888Pixel* output, 
                        const uint8_t* input, 
                        const size_t output_buffer_size_in_bytes, 
                        const size_t input_buffer_size_in_bytes,
                        const RECT& rect = {});
};

class yuyv : public ::holo3d::decoder::base
{
public:
    void decode(holo3d::RGB888Pixel* output, 
                        const uint8_t* input, 
                        const size_t output_buffer_size_in_bytes, 
                        const size_t input_buffer_size_in_bytes,
                        const RECT& rect = {});
};

class yv12 : public ::holo3d::decoder::base
{
public:
    void decode(holo3d::RGB888Pixel* output, 
                        const uint8_t* input, 
                        const size_t output_buffer_size_in_bytes, 
                        const size_t input_buffer_size_in_bytes,
                        const RECT& rect = {});
};

class jpeg : public ::holo3d::decoder::base
{
public:
    void decode(holo3d::RGB888Pixel* output, 
                        const uint8_t* input, 
                        const size_t output_buffer_size_in_bytes, 
                        const size_t input_buffer_size_in_bytes,
                        const RECT& rect = {});

    ~jpeg();

protected:
    friend class ::holo3d::decoder::factory;

    jpeg();

    tjhandle m_jpeg_decompressor = nullptr;
};

class vp8 : public ::holo3d::decoder::base
{
public:
    void decode(holo3d::RGB888Pixel* output, 
                        const uint8_t* input, 
                        const size_t output_buffer_size_in_bytes, 
                        const size_t input_buffer_size_in_bytes,
                        const RECT& rect = {});

    ~vp8();

protected:
    friend class ::holo3d::decoder::factory;

    vp8();

    vpx_codec_ctx_t* m_vpx_codec = nullptr;
};

}; // namespace color

namespace depth
{

class base : public ::holo3d::decoder::base
{
protected:

    friend class ::holo3d::decoder::factory;

    virtual ~base();

    virtual void configure(size_t width, size_t height, 
                           size_t output_stride_in_pixels);

public:
    
    virtual void decode(holo3d::RGB888Pixel* output, 
                        const uint8_t* input, 
                        const size_t output_buffer_size_in_bytes, 
                        const size_t input_buffer_size_in_bytes,
                        const RECT& rect = {});

    virtual void decode_depth(DepthPixel* output,
                              const uint8_t* input,
                              const size_t output_size_in_bytes,
                              const size_t input_buffer_size_in_bytes) = 0;
        
    void use_histogram(bool f) { m_use_histogram = f; }     
    
    bool    use_histogram() const { return m_use_histogram; }
    size_t  histogram_size() const { return m_histogram_size; }   

protected:

    void histogram_size(size_t s) { m_histogram_size = s; }

    void calculate_histogram(const DepthPixel* depth_buffer);

    inline void set_depth_pixel(RGB888Pixel* output, const DepthPixel& input);

    void convert_depth_to_color(RGB888Pixel* output, const DepthPixel* input);

    void decode_to_depth(DepthPixel* output, const RGB888Pixel* input);

    float*  histogram();

    uint8_t* work_buf2();
    size_t work_buf2_size() { return width() * height()*sizeof(uint8_t)*4; }

    float*  m_histogram = nullptr;
    bool    m_use_histogram = true;
    size_t  m_histogram_size = 10000;

    uint8_t* m_work_buf2 = nullptr;

#if defined OPEN_CL_ENABLED
    boost::compute::device          m_ocl_device;
    boost::compute::context         m_ocl_context;
    boost::compute::command_queue   m_ocl_queue;
    boost::compute::program         m_ocl_program;
    boost::compute::kernel          m_ocl_kernel;
    boost::compute::buffer          m_ocl_input_image;
    boost::compute::buffer          m_ocl_output_image;

    size_t                          m_ocl_origin[2];
    size_t                          m_ocl_region[2];

#endif // defined OPEN_CL_ENABLED
};

class invalid : public ::holo3d::decoder::depth::base
{
public:
    void decode(holo3d::RGB888Pixel* output, 
                const uint8_t* input, 
                const size_t output_buffer_size_in_bytes, 
                const size_t input_buffer_size_in_bytes,
                const RECT& rect = {});
    void decode_depth(DepthPixel* output,
                      const uint8_t* input,
                      const size_t output_size_in_bytes,
                      const size_t input_buffer_size_in_bytes);
};

class raw : public ::holo3d::decoder::depth::base
{
public:
    void decode(holo3d::RGB888Pixel* output, 
                const uint8_t* input, 
                const size_t output_buffer_size_in_bytes, 
                const size_t input_buffer_size_in_bytes,
                const RECT& rect = {});
    void decode_depth(DepthPixel* output,
                      const uint8_t* input,
                      const size_t output_size_in_bytes,
                      const size_t input_buffer_size_in_bytes);
};

class enc : public ::holo3d::decoder::depth::base
{
public:
    
    void decode_depth(DepthPixel* output,
                      const uint8_t* input,
                      const size_t output_size_in_bytes,
                      const size_t input_buffer_size_in_bytes);
};

class jpeg : public ::holo3d::decoder::depth::base
{
public:
    
    void decode_depth(DepthPixel* output,
                      const uint8_t* input,
                      const size_t output_size_in_bytes,
                      const size_t input_buffer_size_in_bytes);

    ~jpeg();

protected:
    friend class ::holo3d::decoder::factory;

    jpeg();

    tjhandle m_jpeg_decompressor = nullptr;
};

struct png_impl;

class png : public ::holo3d::decoder::depth::base
{
public:
    
    void decode_depth(DepthPixel* output,
                      const uint8_t* input,
                      const size_t output_size_in_bytes,
                      const size_t input_buffer_size_in_bytes);

    ~png();

protected:
    friend class ::holo3d::decoder::factory;

    png();

    friend struct png_impl;
    png_impl* m_impl = nullptr;
};

class vp8 : public ::holo3d::decoder::depth::base
{
public:
    
    void decode_depth(DepthPixel* output,
                      const uint8_t* input,
                      const size_t output_size_in_bytes,
                      const size_t input_buffer_size_in_bytes);

    ~vp8();

protected:
    friend class ::holo3d::decoder::factory;

    vp8();

    vpx_codec_ctx_t* m_vpx_codec = nullptr;
};

}; // namespace depth

}; // namespace decoder

}; // namespace holo3d

#endif // _DECODER_FACTORY_HPP_
