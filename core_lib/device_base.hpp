#ifndef _DEVICE_BASE_HPP_
#define _DEVICE_BASE_HPP_

namespace holo3d
{

class camera_frame;

class device_base
{
public:
    virtual ~device_base() = default;

    virtual bool read_frame(camera_frame&) = 0;

    uint32_t width() const { return m_width; }
    uint32_t height() const { return m_height; }
    uint32_t fps() const { return m_fps; }

protected:

    uint32_t m_width = 640;
    uint32_t m_height = 480;
    uint32_t m_fps = 30;
};

}; // namespace holo3d

#endif // _DEVICE_BASE_HPP_
