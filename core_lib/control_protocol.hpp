#ifndef _CONTROL_PROTOCOL_HPP_
#define _CONTROL_PROTOCOL_HPP_

#include <stdint.h>
#include <functional>

#include "h3t_file.hpp"

namespace holo3d
{

class fragmenting_socket;

namespace protocol
{

const std::string   DEFAULT_PORT        = "15676"; // << Default port for control messages
inline uint16_t default_port_num() { return (uint16_t)std::stoi(protocol::DEFAULT_PORT); }


enum class ID : uint8_t
{
    QUERY                   // << Broadcast message used to discover all of the available listening processes
    , QUERY_ACK             // << Acknowledgement of the query
    , SYNC_TIME             // << Command to all listeners to synchronize their clocks via ntp or another time source
    , START                 // << NOTE: Deprecated (should be using CONTROL_MESSAGE(start) instead)
    , START_ACK             // << OTE: Deprecated (should be using CONTROL_MESSAGE_ACK instead)
    , END                   // << OTE: Deprecated (should be using CONTROL_MESSAGE(stop) instead)
    , END_ACK               // << OTE: Deprecated (should be using CONTROL_MESSAGE_ACK instead)
    , FULL_FRAME            // << Used to request a full frame snapshot of the video feed
    , FULL_FRAME_ACK        // << return of the full frame data to the requester
    , CAMERA_PARAMS         // << Sending of the calibrated camera parameters to the camera process
    , LOCATION_UPDATE       // << Broadcast message to all listeners of a new location update (these might have to be batched together)
    , CONTROL_MESSAGE       // << This message is used to send a control message to another process (either start or stop the process)
    , CONTROL_MESSAGE_ACK   // << Used to acknowledge the control message
    , IMAGE_FRAGMENT        // << Used to transmit a new image fragment from a camera process to a pose process
    , UPDATE_POSE           // << Used to transmit a pose from a pose process to the control process
};
std::ostream& operator<<(std::ostream& os, const ID& id);

struct BaseData
{
    ID id; // << Indicates what type of message this is
};

struct QueryData : BaseData
{
    uint16_t sender_port;
    uint64_t clock_microsec;      // << Current time in microseconds

    QueryData(uint16_t sender_port, uint64_t clock_microsec=0);

    static ID static_id() { return ID::QUERY; }
};

struct QueryAckData : BaseData
{
    enum class TYPE : uint8_t
    {
        CAMERA
        , TRACKER
        , POSE
    };
    TYPE listener_type;
    int64_t ms_diff;

    QueryAckData(TYPE listener_type, int64_t ms_diff=0);

    static ID static_id() { return ID::QUERY_ACK; }
};

std::ostream& operator<<(std::ostream& os, const QueryAckData::TYPE& id);

std::ostream& operator<<(std::ostream& os, const QueryAckData& data);

struct LocationUpdateData : BaseData
{
    uint64_t clock_microsec;      // << Current time in microseconds

    LocationUpdateData(uint64_t clock_microsec=0);

    static ID static_id() { return ID::LOCATION_UPDATE; }
};

std::ostream& operator<<(std::ostream& os, const LocationUpdateData& data);

template<typename T> 
T* cast(void* data)
{
    protocol::BaseData* base_data = reinterpret_cast<protocol::BaseData*>(data);

    if(base_data && base_data->id == T::static_id())
    {
        return static_cast<T*>(base_data);
    }

    return nullptr;
}

// NOTE: this is deprecated!
struct StartData : BaseData
{
    RESOLUTION      resolution;     // << The requested resolution of the stream
    STREAM_FORMAT   color_format;   // << Format of the color frames
    STREAM_FORMAT   depth_format;   // << Format of the depth frames
    QUALITY         quality;        // << The quality of the streaming, probably needs to be split up based on color quality vs depth quality
    uint8_t         fps;            // << Usually between 1 and 30
};

typedef std::function<void (void)> basic_callback_t;
typedef std::function<void (const StartData& data)> start_callback_t;

}; // namespace protocol

}; // namespace holo3d

#endif // _CONTROL_PROTOCOL_HPP_
