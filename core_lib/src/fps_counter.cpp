#include "fps_counter.hpp"

#include "util_funcs.hpp"

namespace holo3d
{

struct FPSCounterPrivate
{
    std::string name;
    size_t num_frames;
    std::vector<uint64_t> prev_times;
    size_t current_idx = 0;
//  uint64_t last_time = 0;
//  int num_frames = 0;

    FPSCounterPrivate(const std::string& name, size_t expected_fps);
};

FPSCounterPrivate::FPSCounterPrivate(const std::string& name, size_t expected_fps)
: name(name)
, prev_times(expected_fps)
{ }

FPSCounter::FPSCounter(const std::string& name, size_t expected_fps) 
: pImpl(new FPSCounterPrivate(name, expected_fps))
{
    reset();
}

FPSCounter::~FPSCounter()
{
}

void FPSCounter::reset()
{
    uint64_t curr_time = get_time();
    pImpl->prev_times.assign(pImpl->prev_times.size(), curr_time);
    pImpl->current_idx = 0;
}

void FPSCounter::name(std::string name) 
{ 
    pImpl->name = name; 
}

float FPSCounter::current_fps() const
{
    size_t current_time_idx = (pImpl->prev_times.size() + pImpl->current_idx - 1) % pImpl->prev_times.size();

    uint64_t prev_time = pImpl->prev_times[pImpl->current_idx];
    uint64_t curr_time = pImpl->prev_times[current_time_idx];

    if (prev_time >= curr_time) 
    {
        return float(pImpl->prev_times.size());
    } else
    {
        return float(pImpl->prev_times.size()) * MICROS_PER_SEC / (curr_time - prev_time); 
    }
}


bool FPSCounter::update()
{
    uint64_t curr_time = get_time();
    pImpl->prev_times[pImpl->current_idx] = curr_time;
    pImpl->current_idx = (pImpl->current_idx+1) % pImpl->prev_times.size();

//  INFO("diff={}, num frames={}, fps={}", curr_time - m_last_time, m_num_frames, fps);

    if (pImpl->current_idx == 0)
    {
        float fps = current_fps();
        INFO_SHORT("{}: fps={}", pImpl->name, fps);
        return true;
    }

    return false;
}

} // namespace holo3d
