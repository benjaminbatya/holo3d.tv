#include <memory>
#include <OpenNI2/OpenNI.h>

#include <map>

#include "util_funcs.hpp"

#include "openni_factory.hpp"

namespace holo3d
{

typedef std::shared_ptr<openni_device> oni_dev_ptr_t;
typedef std::map<std::string, oni_dev_ptr_t> device_map_t;

struct openni_factory_impl
{
    bool initialized = false;
    bool enable_ir = false;

    device_map_t    devices;
};

openni_factory::openni_factory()
: m_impl(new openni_factory_impl)
{
    // Initialize OpenNI
    openni::Status stat = openni::OpenNI::initialize();
    if(stat == openni::STATUS_OK)
    {
        INFO("Initialized the Openni System");
        m_impl->initialized = true;

        m_impl->devices.clear();
    } else
    {
        ERROR("Failed to initialize OpenNI. status={}, error={}", stat, openni::OpenNI::getExtendedError());
    }
}

openni_factory::~openni_factory()
{
    if (initialized()) 
    {
        m_impl->devices.clear();

        openni::OpenNI::shutdown();
        INFO("Shut down Openni system"); 
    }
    m_impl->initialized = false;

    delete m_impl;
}

void openni_factory::set_enable_ir(bool f) { m_impl->enable_ir = f; }
bool openni_factory::initialized() { return m_impl->initialized; } 

STREAM_FORMAT openni_factory::color_format() const { return STREAM_FORMAT::COLOR_RGB888; }
STREAM_FORMAT openni_factory::depth_format() const { return STREAM_FORMAT::DEPTH_RAW; }

factory_base::uri_vec_t openni_factory::device_uris()
{
    factory_base::uri_vec_t uris(0);

    if (!initialized()) 
    {
        return uris;
    }

    openni::Array<openni::DeviceInfo> device_info_list;
    openni::OpenNI::enumerateDevices(&device_info_list);

    if (device_info_list.getSize() < 1) 
    {
        ERROR("No OpenNI devices found.");
        return uris;
    }

//  INFO("Found devices:\n");
    for (int i=0; i<device_info_list.getSize(); i++)
    {
//      INFO("Device %d: URI='%s', Vendor='%s', Name='%s', USB id=%4x:%4x", i,
//           device_info_list[i].getUri(),
//           device_info_list[i].getVendor(),
//           device_info_list[i].getName(),
//           device_info_list[i].getUsbVendorId(),
//           device_info_list[i].getUsbProductId());

        uris.push_back(device_info_list[i].getUri());
    }

    return uris;
}

factory_base::device_ptr_t openni_factory::device(int designed_fps, int desired_width, std::string uri)
{
    if (!initialized()) 
    {
        return nullptr;
    }

    if (m_impl->devices.find(uri) == m_impl->devices.end()) 
    {
        m_impl->devices[uri] = oni_dev_ptr_t(new openni_device(uri));
    }

    oni_dev_ptr_t device = m_impl->devices.at(uri);
    bool ret = device->init(designed_fps, desired_width, m_impl->enable_ir);
    if (!ret) 
    {
        return nullptr;
    }

    return device;
}

}; // namespace holo3d
