
#include <sys/timex.h>
#include <cstring>

#include "util_funcs.hpp"

static VERBOSE_LEVEL g_verbose_level = VERBOSE_LEVEL::INFO;

void set_verbose_level(VERBOSE_LEVEL new_level)
{
    g_verbose_level = new_level;
}
VERBOSE_LEVEL verbose_level()
{
    return g_verbose_level;
}

// This should be inside the WIN32 ifdef
uint64_t get_time()
{
    timeval time_struct;
    get_accurate_time(&time_struct);
    return ((unsigned long long)time_struct.tv_sec * MICROS_PER_SEC) + time_struct.tv_usec;
}

void get_accurate_time(timeval* tv)
{
    timespec tp;
    int res = clock_gettime(CLOCK_REALTIME, &tp);
    ASSERT_THROW_ALWAYS(res==0, "error: {}", std::strerror(res));

    tv->tv_sec = tp.tv_sec;
    tv->tv_usec = tp.tv_nsec / 1000;

//  ntptimeval tp;
//  int res = ntp_gettime(&tp);
//  ASSERT_THROW_ALWAYS(res==0||res==TIME_ERROR, "error: errno={}, str={}", res, std::strerror(errno));
//
//  tv->tv_sec = tp.time.tv_sec;
//  tv->tv_usec = tp.time.tv_usec;
}

bool timeval_subtract (timeval& result, timeval x, timeval y)
{
    /* Perform the carry for the later subtraction by updating y. */
    if (x.tv_usec < y.tv_usec) {
      int nsec = (y.tv_usec - x.tv_usec) / MICROS_PER_SEC + 1;
      y.tv_usec -= MICROS_PER_SEC * nsec;
      y.tv_sec += nsec;
    }
    if (x.tv_usec - y.tv_usec > MICROS_PER_SEC) 
    {
      int nsec = (x.tv_usec - y.tv_usec) / MICROS_PER_SEC;
      y.tv_usec += MICROS_PER_SEC * nsec;
      y.tv_sec -= nsec;
    }

    /* Compute the time remaining to wait.
       tv_usec is certainly positive. */
    result.tv_sec = x.tv_sec - y.tv_sec;
    result.tv_usec = x.tv_usec - y.tv_usec;

//  INFO("Time difference = {}", result);

    /* Return 1 if result is negative. */
    return x.tv_sec < y.tv_sec;
}

