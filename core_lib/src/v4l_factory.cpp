#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <linux/videodev2.h>

#include <string>
#include <vector>
#include <memory>

#include "v4l_device.hpp"
#include "v4l_factory.hpp"

const uint MIN_INDEX = 0;
const uint MAX_INDEX = 63;

namespace holo3d
{

// NOTE: V4L2 specific code is from http://linuxtv.org/downloads/v4l-dvb-apis/capture-example.html

v4l_factory::v4l_factory()
: m_cached_uris(0)
{
    // No initialization is necessary
}

v4l_factory::~v4l_factory()
{

}

factory_base::uri_vec_t v4l_factory::device_uris()
{
    if (m_cached_uris.empty()) 
    {
        const char* fmt = "/dev/video%d";
        char dev_name[256];
        // Scan for valid v4l devices
        for (uint i=MIN_INDEX; i<=MAX_INDEX; i++) 
        {
            snprintf(dev_name, 256, fmt, i);

            struct stat st;

            // Make sure the device exists
            if (0 == stat(dev_name, &st)) 
            {
                // Ensure that the device is a character device
                if (S_ISCHR(st.st_mode)) 
                {
                    m_cached_uris.push_back(dev_name);
                }
            }
        }
    }

    return(m_cached_uris);
}

factory_base::device_ptr_t v4l_factory::device(int designed_fps, int desired_width, std::string uri)
{
    v4l_device* device = new v4l_device();

    if (uri.empty()) 
    {
        uri = device_uris().front();
    }

    bool ret = device->init(designed_fps, desired_width, uri);

    if (!ret) 
    {
        delete device;
        device = nullptr;
    }

    return factory_base::device_ptr_t(device);
}

}; // namespace holo3d
