#include <string>
#include <memory>
#include <cstring>

#include "frame.hpp"

#include "holo3d.pb.h"

#include "util_funcs.hpp"

#include "h3t_file.hpp"

namespace holo3d
{

struct camera_frame_impl
{
    protocol::Header    header;
    
    protocol::Frame     frame;

    uint8_t*            buffer = nullptr;
};

camera_frame::camera_frame()
: m_impl(new camera_frame_impl)
{ 
    reset(); 
}

camera_frame::~camera_frame()
{
    // Just delete the buffer for now...
    delete m_impl->buffer;
    m_impl->buffer = nullptr;

    delete m_impl;
}

camera_frame::camera_frame(const camera_frame& that)
: camera_frame()
{
    this->operator =(that);
}

camera_frame& camera_frame::operator=(const camera_frame& that)
{
    m_impl->header = that.m_impl->header;

    m_impl->frame = that.m_impl->frame;
    
    m_impl->buffer = that.m_impl->buffer;

    return *this;
}

const protocol::Header& camera_frame::header() const { return m_impl->header; }
protocol::Header& camera_frame::header() { return m_impl->header; }
void camera_frame::set_header(const protocol::Header& file_header) { m_impl->header = file_header; }

const protocol::Frame& camera_frame::frame() const { return m_impl->frame; }
protocol::Frame& camera_frame::frame() { return m_impl->frame; }
void camera_frame::set_frame(const protocol::Frame& header) { m_impl->frame = header; }

void camera_frame::reset()
{
    m_impl->frame.Clear();
}

bool camera_frame::valid() 
{ 
    return m_impl->buffer != nullptr;
}

uint16_t    camera_frame::width() const { return m_impl->header.width(); }
uint16_t    camera_frame::height() const { return m_impl->header.height(); }

uint64_t    camera_frame::frame_number() const { return m_impl->frame.frame_number(); }
void        camera_frame::set_frame_number(uint64_t fn) { m_impl->frame.set_frame_number(fn); }

uint64_t    camera_frame::time_stamp() const 
{ 
    const protocol::TimeStamp& ts = m_impl->frame.time_stamp();
    return uint64_t(ts.seconds())*MICROS_PER_SEC + ts.micro_seconds();
}
void        camera_frame::set_time_stamp(uint64_t ts) 
{ 
    protocol::TimeStamp* mts = m_impl->frame.mutable_time_stamp();
    mts->set_seconds(uint32_t(ts/MICROS_PER_SEC));
    mts->set_micro_seconds(uint32_t(ts%MICROS_PER_SEC));
}

uint32_t    camera_frame::buffer_size() const { return m_impl->frame.buffer_size(); }

uint32_t    camera_frame::max_buffer_size() const { return width() * height() * sizeof(holo3d::RGB888Pixel); }

const uint8_t* camera_frame::buffer() const
{
    return m_impl->buffer;
}

void camera_frame::set_buffer(const void* data, uint32_t size)
{
    if (m_impl->buffer == nullptr) 
    {
        // Replace with with a pooled object later!!
        m_impl->buffer = new uint8_t[max_buffer_size()];
    }

    ASSERT_THROW_ALWAYS(size <= max_buffer_size(), "buffer size {} is greater then max_buffer_size {}", size, max_buffer_size());
    m_impl->frame.set_buffer_size(size);
    memcpy(m_impl->buffer, data, size);
}


// NOTE: Find a different pool object!
// NOTE: We are using the same pool to allocate both the color and depth buffers
// NOTE: boost::fast_pool_allocator IS NOT THREAD SAFE without a mutex!!! 
// ADD A MUTEX if RANDOM segfaults start occurring during allocation and deallocation
// static boost::pool<> g_buffer_pool(sizeof(RGB888Pixel));

//class buffer_deleter
//{
//public:
//void operator()(uint8_t* obj)
//{
//    // INFO_CPP("deleting " << obj);
//    // g_buffer_pool.ordered_free(obj);
//    delete [] obj;
//}
//};
//
//static buffer_deleter g_buffer_deleter;
//
//uint8_t* camera_frame::color_buffer()
//{
//    if(!m_color_buffer) // Do lazy allocation
//    {
//        // m_color_buffer.reset(POOL_ALLOCATOR.malloc(NUM_FRAME_PIXELS), [&](void* ptr) { POOL_ALLOCATOR.free(ptr, NUM_FRAME_PIXELS)); });
//        size_t size = max_color_buffer_size();
//        // RGB888Pixel* buf = (RGB888Pixel*)g_buffer_pool.ordered_malloc(num_pixels);
//        uint8_t* buf = new uint8_t[size];
//        m_impl->buffer.reset(buf, g_buffer_deleter);
//        // INFO_CPP("allocated " << m_color_buffer << " of size " << (num_pixels * sizeof(RGB888Pixel)));
//    }
//
//    return m_impl->buffer.get();
//}

}; // namespace holo3d
