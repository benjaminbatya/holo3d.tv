#include "util_funcs.hpp"
#include "control_protocol.hpp"

namespace holo3d
{

namespace protocol
{

std::ostream& operator<<(std::ostream& os, const ID& id)
{
#define OUTPUT_ID(e) case ID::e : os << #e; break;
    switch (id)
    {
    OUTPUT_ID(QUERY);
    OUTPUT_ID(QUERY_ACK);
    OUTPUT_ID(START);
    OUTPUT_ID(START_ACK);
    OUTPUT_ID(END);
    OUTPUT_ID(END_ACK);
    OUTPUT_ID(FULL_FRAME);
    OUTPUT_ID(FULL_FRAME_ACK);
    OUTPUT_ID(CAMERA_PARAMS); 
    OUTPUT_ID(LOCATION_UPDATE);
    OUTPUT_ID(CONTROL_MESSAGE);
    OUTPUT_ID(CONTROL_MESSAGE_ACK);
    OUTPUT_ID(IMAGE_FRAGMENT);
    OUTPUT_ID(UPDATE_POSE);        
    default: os << "Unknown protocol id=" << (uint8_t)id; break;
    }
#undef OUTPUT_ID

    return os;
}

QueryData::QueryData(uint16_t sender_port, uint64_t clock_microsec)
{
    this->id = static_id();
    this->sender_port = sender_port;
    this->clock_microsec = clock_microsec;
}

QueryAckData::QueryAckData(TYPE listener_type, int64_t ms_diff)
{
    this->id = static_id();
    this->listener_type = listener_type;
    this->ms_diff = ms_diff;
}

std::ostream& operator<<(std::ostream& os, const QueryAckData::TYPE& id)
{
#define OUTPUT_ID(e) case QueryAckData::TYPE::e : os << #e; break;
    switch (id)
    {
    OUTPUT_ID(CAMERA);
    OUTPUT_ID(TRACKER);
    OUTPUT_ID(POSE);
    default: os << "Unknown listener type id=" << (uint8_t)id; break;
    }
#undef OUTPUT_ID

    return os;
}

std::ostream& operator<<(std::ostream& os, const QueryAckData& data)
{
    os  << "ID=" << data.id 
        << ", listener type=" << data.listener_type 
        << ", ms_diff = " << data.ms_diff;

    return os;
}

LocationUpdateData::LocationUpdateData(uint64_t clock_microsec)
{
    this->id = static_id();
    this->clock_microsec = clock_microsec;
}

std::ostream& operator<<(std::ostream& os, const LocationUpdateData& data)
{
    os  << "ID=" << data.id << ", clock_microsec = " << data.clock_microsec;
    return os;
}

}; // namespace protocol



}; // namespace holo3d
