__kernel void depth_decode(__global uchar4* input_image,
                           __global ushort* output_image)
{
//  const sampler_t sampler =
//        CLK_NORMALIZED_COORDS_FALSE
//      | CLK_ADDRESS_CLAMP_TO_EDGE
//      | CLK_FILTER_NEAREST;

    // Get the work item's coordinates
    int x = get_global_id(0);
    int y = get_global_id(1);

    // int2 coords = (int2)(x, y);
    // uchar4 pixel = (uchar4)read_imageui(input_image, sampler, coords);

    const double np = 512.0;
    const double w = 10000.0; // 1<<16;

    const double p = np / w;
    const double half_p = p / 2.0;
    const double quarter_p = p / 4.0;
    const double eighth_p = p / 8.0;

    uchar4 input_idx = input_image[y*640+x];

    double L = ((double)input_idx.z) / 256;
    double Ha =  ((double)input_idx.y) / 256;
    double Hb =  ((double)input_idx.x) / 256;

    double mL = 4.0 * L / p - 0.5;
    mL = fmod(floor(mL), 4.0);
    if (mL < 0) { mL += 4.0; }
    size_t temp = (size_t)mL;

    double L0 = L - fmod(L - eighth_p, p) + (mL * quarter_p) - eighth_p;

    double delta;
    switch (temp)
    {
    case 0: delta = Ha * half_p; break;
    case 1: delta = Hb * half_p; break;
    case 2: delta = (1.0 - Ha) * half_p; break;
    case 3: delta = (1.0 - Hb) * half_p; break;
    default: delta = 0;
    }

    // ushort depth = (ushort)(w * (L0 + delta));
    ushort depth = (ushort)((x+y)*(1<<16) / (640+480));

    // barrier(CLK_GLOBAL_MEM_FENCE);

//  if(y < get_image_height(input_image) &&
//     x < get_image_width(input_image))
//  {
//      write_imageui(output_image, coords, value);
//  }
    output_image[y*640 + x] = depth;

//  // Half the width of the filter is needed for indexing memory later...
//  int half_width = (int)(filter_width / 2);
//
//  // All accesses to images return data as four imae vectors (float4)
//  float4 sum = {0.0f, 0.0f, 0.0f, 0.0f};
//
//  int filter_idx = 0;
//
//  int2 coords;
//
//  // Iterate across the filter rows
//  for(int i = -half_width; i <= half_width; i++)
//  {
//      coords.x = x + i;
//
//      // Iterate across the filter columns
//      for(int j = -half_width; j <= half_width; j++)
//      {
//          coords.y = y + j;
//
//          float4 pixel;
//
//          pixel = read_imagef(input_image, sampler, coords);
//          sum.x += pixel.x * filter[filter_idx++];
//          // sum.y += pixel.y * filter[filter_idx++];
//          // sum.z += pixel.z * filter[filter_idx++];
//      }
//  }
//
//  barrier(CLK_GLOBAL_MEM_FENCE);
//
//  // Copy the data if its in bounds
//  if(y < get_image_height(input_image) &&
//     x < get_image_width(input_image))
//  {
//      coords.x = x;
//      coords.y = y;
//
//      write_imagef(output_image, coords, sum);
//  }
}
