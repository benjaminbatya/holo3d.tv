#include <iostream>
#include <libyuv.h>
#include <cstring>
#include <memory>

#include "util_funcs.hpp"

#include "depth_encoder_map.hpp"

#include "encoder.hpp"

// Start VPX stuff

#define VPX_CODEC_DISABLE_COMPAT 1
#include "vpx/vpx_encoder.h"
#include "vpx/vp8cx.h"
#define VPX_INTERFACE (vpx_codec_vp8_cx())

// static int VPX_FLAGS = 0;

inline void VPX_CODEC_THROW(vpx_codec_ctx_t* codec, int res, const char* msg)
{
    if (res)
    {
        const char* detail = vpx_codec_error_detail(codec);
        THROW("{}: '{}'\n\tdetail: '{}'", msg, vpx_codec_error(codec) , (detail ? detail : "none") );
    } 
}

// End VPX stuff

// Start JPEG stuff

#if defined MODEL_RPI2

#include <jpeg_encoder.h>


#endif // defined MODEL_RPI2
// TODO: make this dependency an else-if

#include <turbojpeg.h>

const TJSAMP JPEG_SUBSAMPLING = TJSAMP_444;

// End JPEG stuff

// START png stuff

#include <png.h>

// END png stuff

// START x264 stuff

//#include <x264.h>

// END x264

namespace holo3d
{

namespace encoder
{

factory::factory(size_t width, size_t height, const QUALITY quality, const uint16_t fps)
: m_width(width)
, m_height(height)
, m_quality(quality)
, m_fps(fps)
{
    
}

color_encoder_t factory::color(STREAM_FORMAT output_format, STREAM_FORMAT input_format)
{
    color_encoder_t encoder;

    switch (output_format)
    {
    case STREAM_FORMAT::COLOR_RGB888: encoder.reset(new color::rgb888); break;
    case STREAM_FORMAT::COLOR_YUV422: encoder.reset(new color::yuv422); break;
    case STREAM_FORMAT::COLOR_YUYV: encoder.reset(new color::yuyv); break;
    case STREAM_FORMAT::COLOR_YV12: encoder.reset(new color::yv12); break;
    case STREAM_FORMAT::COLOR_JPEG: encoder.reset(new color::jpeg); break;
    case STREAM_FORMAT::COLOR_VP8: encoder.reset(new color::vp8); break;
//  case STREAM_FORMAT::COLOR_H264: encoder.reset(new color::h264); break;
    default: THROW("INVALID format {}", output_format); break;
    }

    configure(encoder, output_format, input_format);

    return encoder;
}

depth_encoder_t factory::depth(STREAM_FORMAT output_format, STREAM_FORMAT input_format)
{
    depth_encoder_t encoder;

    if (input_format == STREAM_FORMAT::INVALID) 
    {
        output_format = STREAM_FORMAT::INVALID;
    }

    switch (output_format)
    {
    case STREAM_FORMAT::DEPTH_RAW: encoder.reset(new depth::raw); break;
    case STREAM_FORMAT::DEPTH_ENC: encoder.reset(new depth::enc); break;
    case STREAM_FORMAT::DEPTH_JPEG: encoder.reset(new depth::jpeg); break;
    case STREAM_FORMAT::DEPTH_PNG: encoder.reset(new depth::png); break;
    case STREAM_FORMAT::DEPTH_VP8: encoder.reset(new depth::vp8); break;
    case STREAM_FORMAT::INVALID: encoder.reset(new depth::invalid); break;
    default: THROW("INVALID format {}", input_format); break;
    }

    configure(encoder, output_format, input_format);

    return encoder;
}

void factory::configure(color_encoder_t encoder, STREAM_FORMAT output_format, STREAM_FORMAT input_format)
{
    encoder->m_width = m_width;
    encoder->m_height = m_height;
    encoder->m_input_format = input_format;
    encoder->m_output_format = output_format;

    encoder->init(m_quality);
    encoder->m_fps = m_fps;
}

void base::init(const QUALITY quality)
{
    // do nothing...
}

base::~base()
{
    if (m_work_buf)
    {
        delete [] m_work_buf;
    }
}

uint8_t* base::work_buf()
{
    // I love lazy allocation
    if (!m_work_buf)
    {
        m_work_buf = new uint8_t[work_buf_size()];
    }

    return m_work_buf;
}

/**
 * converts the input image into I420 format
 * 
 * @author benjamin (3/28/2016)
 * 
 * @param output buffer to store the I420 formatted image into
 * @param input buffer to convert
 * @param rect subarea of the image to consider
 * 
 * @return size_t Size of the output buffer
 */
size_t base::convert_to_yv12(uint8_t* output, const uint8_t* input, const RECT& rect)
{
    const uint8_t* input_offset = calc_input_offset(input, rect);

    size_t num_pixels = rect.width * rect.height;

    // YV12 is the same as i420 but with the U and V planes switched
    uint8_t* y_plane = output;
    uint8_t* v_plane = output + num_pixels * 1;
    uint8_t* u_plane = output + num_pixels * 5 / 4;

    // Do the conversion here
    switch (input_format())
    {
    // NOTE: Hack! Once this is called in depth::vp8::encode(), input is in RGB888 format
    case STREAM_FORMAT::DEPTH_RAW: 
        input_offset = input + (rect.x + rect.y*width()) * sizeof(RGB888Pixel);

    case STREAM_FORMAT::COLOR_RGB888:

        libyuv::RAWToI420(input_offset, width() * sizeof(RGB888Pixel),
                          y_plane, rect.width,
                          u_plane, rect.width / 2,
                          v_plane, rect.width / 2,
                          rect.width, rect.height );
        break;
        // NOTE: this is untested
//  case STREAM_FORMAT::COLOR_YUV422:
//      libyuv::UYVYToI420(input, width() * sizeof(YUV422DoublePixel) / 2,
//                        y_plane, width(),
//                        u_plane, width() / 2,
//                        v_plane, width() / 2,
//                        width(), height() );
//      break;
    case STREAM_FORMAT::COLOR_YUYV:
        libyuv::YUY2ToI420(input_offset, width() * input_depth(),
                           y_plane, rect.width,
                           u_plane, rect.width / 2,
                           v_plane, rect.width / 2,
                           rect.width, rect.height );
        break;
    case STREAM_FORMAT::COLOR_YV12:
        ::memcpy(output, input, num_pixels * 3 / 2);
        break;
    default:
        THROW("can't convert format {}", input_format());
        break;
    }

    return num_pixels * 3 / 2;
}

void base::check_rect(RECT& rect) const
{
    if (rect.is_empty()) 
    { 
        rect.x = 0; 
        rect.y = 0;    
        rect.width = width(); 
        rect.height = height();
    } 

    ASSERT(rect.x + rect.width <= width()); 
    ASSERT(rect.y + rect.height <= height()); 
}

const uint8_t* base::calc_input_offset(const uint8_t* ptr, const RECT& rect) const
{
    return ptr + (rect.x + rect.y*width()) * input_depth();
}   

size_t vpx_quality_convert(const QUALITY& q)
{
    switch (q)
    {
    case QUALITY::REALTIME: return VPX_DL_REALTIME; break;
    case QUALITY::AVERAGE: return (VPX_DL_REALTIME + VPX_DL_GOOD_QUALITY) / 2; break;
    case QUALITY::GOOD: return VPX_DL_GOOD_QUALITY; break;
    case QUALITY::BEST: return VPX_DL_BEST_QUALITY; break;
    default: THROW( "Unknown encoder quality {}", q); break;
    }

    return 0;
}

size_t jpeg_quality_convert(const QUALITY& q)
{
    switch (q)
    {
    case QUALITY::REALTIME: return 5; break;
    case QUALITY::AVERAGE: return 45; break;
    case QUALITY::GOOD: return 75; break;
    case QUALITY::BEST: return 100; break;
    default: THROW( "Unknown encoder quality {}", q); break;
    }

    return 0;
}

namespace color
{

size_t rgb888::encode(uint8_t* output, const uint8_t* input, 
                      const size_t output_buffer_size, const size_t input_buffer_size,
                      RECT rect)
{
    check_rect(rect);

    const uint8_t* input_offset = calc_input_offset(input, rect);

    uint8_t* y_plane = work_buf();
    uint8_t* u_plane = y_plane + num_frame_pixels() * 1;
    uint8_t* v_plane = y_plane + num_frame_pixels() * 5 / 4;

    switch (input_format())
    {
    case STREAM_FORMAT::COLOR_RGB888:
    {
        libyuv::RAWToI420(input_offset, width() * input_depth(),
                          y_plane, rect.width,
                          u_plane, rect.width / 2,
                          v_plane, rect.width / 2,
                          rect.width, rect.height );

        libyuv::I420ToRAW(y_plane, rect.width,
                          u_plane, rect.width / 2,
                          v_plane, rect.width / 2,
                          output, rect.width * output_depth(),
                          rect.width, rect.height );
        break;
    }
    case STREAM_FORMAT::COLOR_YUYV:
    {
        libyuv::YUY2ToI420(input_offset, width() * input_depth(),
                           y_plane, rect.width,
                           u_plane, rect.width / 2,
                           v_plane,rect.width / 2,
                           rect.width, rect.height );

        libyuv::I420ToRAW(y_plane, rect.width,
                          u_plane, rect.width / 2,
                          v_plane, rect.width / 2,
                          output, rect.width * output_depth(),
                          rect.width, rect.height );
        break;
    }
    default:
        THROW("can't convert format {}", input_format()); 
        break;         
    }

    return rect.width * rect.height * output_depth();
}

size_t yuv422::encode(uint8_t* output, const uint8_t* input, 
                      const size_t output_buffer_size, const size_t input_buffer_size,
                      RECT rect)
{
    size_t ret = 0;

    switch (input_format())
    {
//  case openni::PIXEL_FORMAT_RGB888:
//  {
//      // Use output as a temporary buffer so another work buffer isn't needed
//      uint8_t* y_plane = work_buf();
//      uint8_t* u_plane = y_plane + num_frame_pixels() * 1;
//      uint8_t* v_plane = y_plane + num_frame_pixels() * 5 / 4;
//
//      libyuv::RAWToI420(input, width() * sizeof(holo3d::RGB888Pixel),
//                        y_plane, width(),
//                        u_plane, width() / 2,
//                        v_plane, width() / 2,
//                        width(), height() );
//      libyuv::I420ToUYVY(y_plane, width(),
//                         u_plane, width() / 2,
//                         v_plane, width() / 2,
//                         output, width() * sizeof(openni::YUV422DoublePixel),
//                         width(), height());
//      ret = width() * height() * sizeof(openni::YUV422DoublePixel)
//      break;
//  }
    default:
        THROW("can't convert format {}", input_format()); 
        break;         
    }

    return ret;
}

size_t yuyv::encode(uint8_t* output, const uint8_t* input, 
                    const size_t output_buffer_size, const size_t input_buffer_size,
                    RECT rect)
{
    check_rect(rect);

    const uint8_t* input_offset = calc_input_offset(input, rect);

    uint8_t* y_plane = work_buf();
    uint8_t* u_plane = y_plane + num_frame_pixels() * 1;
    uint8_t* v_plane = y_plane + num_frame_pixels() * 5 / 4;

    switch (input_format())
    {
    case STREAM_FORMAT::COLOR_RGB888:
    {
        libyuv::RAWToI420(input_offset, width() * input_depth(),
                          y_plane, rect.width,
                          u_plane, rect.width / 2,
                          v_plane, rect.width / 2,
                          rect.width, rect.height );

        libyuv::I420ToYUY2(y_plane, rect.width,
                           u_plane, rect.width / 2,
                           v_plane, rect.width / 2,
                           output, rect.width * output_depth(),
                           rect.width, rect.height );
        break;
    }
    case STREAM_FORMAT::COLOR_YUYV:
    {   
        libyuv::YUY2ToI420(input_offset, width() * input_depth(),
                           y_plane, rect.width,
                           u_plane, rect.width / 2,
                           v_plane,rect.width / 2,
                           rect.width, rect.height);

        libyuv::I420ToYUY2(y_plane, rect.width,
                           u_plane, rect.width / 2,
                           v_plane, rect.width / 2,
                           output, rect.width * output_depth(),
                           rect.width, rect.height);

        break;
    }
    default:
        THROW("can't convert format {}", input_format()); 
        break;         
    }

    return rect.width * rect.height * output_depth();
}

size_t yv12::encode(uint8_t* output, const uint8_t* input, 
                    const size_t output_buffer_size, const size_t input_buffer_size,
                    RECT rect)
{
    check_rect(rect);

    size_t output_size = convert_to_yv12(output, input, rect);
    
    return output_size;
}

#if defined MODEL_RPI2

struct JPEGImpl
{
};

void jpeg::init(const QUALITY quality)
{
    m_impl = new JPEGImpl;

    m_quality = jpeg_quality_convert(quality);

    bool ret = jpegEncoder::init(width(), height(), true);
    ASSERT_ALWAYS(ret);
}

jpeg::~jpeg()
{
    if (m_impl)
    {
        jpegEncoder::close();
    }

    delete m_impl;
}

size_t jpeg::encode(uint8_t* output, const uint8_t* input,
                    const size_t output_buffer_size, const size_t input_buffer_size,
                    RECT rect)
{
    check_rect(rect);

    const uint8_t* input_offset = calc_input_offset(input, rect);

    convert_to_yv12(work_buf(), input_offset, rect);

    size_t compressed_size = output_buffer_size;

    bool ret = jpegEncoder::encode(work_buf(), work_buf_size(), output, &compressed_size);
    ASSERT_THROW(ret);

//  INFO("Compressed size = {}", compressed_size);

    return compressed_size;
}

#else

struct JPEGImpl
{
    tjhandle jpeg_compressor = nullptr;
};

void jpeg::init(const QUALITY quality)
{
    m_impl = new JPEGImpl;

    m_quality = jpeg_quality_convert(quality);

    m_impl->jpeg_compressor = tjInitCompress();
    ASSERT_THROW(m_impl->jpeg_compressor, "failed to allocate the jpeg compressor: {}", tjGetErrorStr());
}

jpeg::~jpeg()
{
    if (m_impl->jpeg_compressor)
    {
        int ret = tjDestroy(m_impl->jpeg_compressor);
        ASSERT_THROW (!ret, "failed to destroy the jpeg compressor: {}", tjGetErrorStr());
    }

    delete m_impl;
}

size_t jpeg::encode(uint8_t* output, const uint8_t* input,
                    const size_t output_buffer_size, const size_t input_buffer_size,
                    RECT rect)
{
    // openni::stream frame has to be converted into RGB format
    // for the jpeg compression
    check_rect(rect);

    const uint8_t* input_offset = calc_input_offset(input, rect);

    // Use output as a temporary buffer so another work buffer isn't needed
    uint8_t* y_plane = output;
    uint8_t* u_plane = output + num_frame_pixels() * 1;
    uint8_t* v_plane = output + num_frame_pixels() * 5 / 4;

    switch (input_format())
    {
    case STREAM_FORMAT::COLOR_RGB888:
    {
        libyuv::RAWToI420(input_offset, width() * input_depth(),
                          y_plane, rect.width,
                          u_plane, rect.width / 2,
                          v_plane, rect.width / 2,
                          rect.width, rect.height );

        libyuv::I420ToRAW(y_plane, rect.width,
                          u_plane, rect.width / 2,
                          v_plane, rect.width / 2,
                          work_buf(), rect.width * get_pixel_depth(STREAM_FORMAT::COLOR_RGB888),
                          rect.width, rect.height );
        break;
    }
    case STREAM_FORMAT::COLOR_YUYV:
    {
        libyuv::YUY2ToI420(input_offset, width() * input_depth(),
                           y_plane, rect.width,
                           u_plane, rect.width / 2,
                           v_plane,rect.width / 2,
                           rect.width, rect.height );

        libyuv::I420ToRAW(y_plane, rect.width,
                          u_plane, rect.width / 2,
                          v_plane, rect.width / 2,
                          work_buf(), rect.width * get_pixel_depth(STREAM_FORMAT::COLOR_RGB888),
                          rect.width, rect.height );
        break;
    }
    case STREAM_FORMAT::COLOR_YV12:
    {
        const uint8_t* y_plane = input;
        const uint8_t* u_plane = input + num_frame_pixels() * 1;
        const uint8_t* v_plane = input + num_frame_pixels() * 5 / 4;

        libyuv::I420ToRAW(y_plane, rect.width,
                          u_plane, rect.width / 2,
                          v_plane, rect.width / 2,
                          work_buf(), rect.width * get_pixel_depth(STREAM_FORMAT::COLOR_RGB888),
                          rect.width, rect.height );
        break;
    }
    default:
        THROW("can't convert format {}", input_format());
        break;
    }

    unsigned long compressed_size = 0;

    int ret =
    tjCompress2(m_impl->jpeg_compressor, work_buf(),
                rect.width, 0, rect.height,
                TJPF_RGB, &output, &compressed_size,
                JPEG_SUBSAMPLING, quality(), TJFLAG_FASTDCT | TJFLAG_NOREALLOC);
    ASSERT_THROW(ret != -1, tjGetErrorStr());

    return compressed_size;
};

#endif // defined MODEL_RPI2

// Code from http://www.webmproject.org/docs/vp8-sdk/example__simple__encoder.html
void vp8::init(const QUALITY quality)
{
    // NOTE: Always set the quality to realtime
    m_quality = vpx_quality_convert(quality);

    // INFO("VPX interface: {}", vpx_codec_iface_name(VPX_INTERFACE));

    const vpx_codec_iface_t* iface = VPX_INTERFACE;
    ASSERT(iface);

    m_config = new vpx_codec_enc_cfg_t;

    vpx_codec_err_t res = vpx_codec_enc_config_default(iface, m_config, 0);
    ASSERT_THROW(res == VPX_CODEC_OK, "failed to get config: {}", vpx_codec_err_to_string(res));

    // Adjust default target bit-rate to account for actual desktop size.
    m_config->rc_target_bitrate = width() * height() *
      m_config->rc_target_bitrate / m_config->g_w / m_config->g_h;

    m_config->g_w = width();
    m_config->g_h = height();

//  // Use millisecond granularity time base.
//  config.g_timebase.num = 1;
//  config.g_timebase.den = 1000;
    m_config->g_pass = VPX_RC_ONE_PASS;
//
//  // Start emitting packets immediately.
//  config.g_lag_in_frames = 0;

    // Using 2 threads gives a great boost in performance for most systems with
    // adequate processing power. NB: Going to multiple threads on low end
    // windows systems can really hurt performance.
    // http://crbug.com/99179
    m_config->g_threads = 4;

//  config.g_usage = VPX_CQ;

//  config.g_profile = 2;
//
//  // Clamping the quantizer constrains the worst-case quality and CPU usage.
//  config.rc_min_quantizer = 20;
//  config.rc_max_quantizer = 30;

    m_vpx_codec = new vpx_codec_ctx_t;

    res = vpx_codec_enc_init(m_vpx_codec, iface, m_config, 0);
    if (res)
    {
        delete m_vpx_codec;
        m_vpx_codec = nullptr;
        THROW("failed initialize codec: {}", vpx_codec_err_to_string(res)); 
    }
}

vp8::~vp8()
{
    if (m_vpx_codec)
    {   
        vpx_codec_err_t  res = vpx_codec_destroy(m_vpx_codec);
        VPX_CODEC_THROW(m_vpx_codec, res, "error destroying vpx codec");
    }
    delete m_vpx_codec;

    delete m_config;
}

size_t vp8::encode(uint8_t* output, const uint8_t* input, 
                   const size_t output_buffer_size, const size_t input_buffer_size,
                   RECT rect)
{
    ASSERT_THROW(rect.is_empty(), "VP8 encoder cannot work with variable sized clipping rectangles for now.");

    if (!rect.is_empty()) 
    {
        m_config->rc_target_bitrate = rect.width * rect.height *
          m_config->rc_target_bitrate / m_config->g_w / m_config->g_h;

        m_config->g_w = rect.width;
        m_config->g_h = rect.height;

        vpx_codec_enc_config_set(m_vpx_codec, m_config);
    }

    check_rect(rect);

    convert_to_yv12(work_buf(), input, rect);

    // Encode the frame
    vpx_image_t img_wrapper;
    vpx_img_wrap(&img_wrapper, VPX_IMG_FMT_YV12, rect.width, rect.height, 1, work_buf());

//  static bool printed = false;
//  if (!printed)
//  {
//      INFO("y stride={}, u stride = {}, v stride = {}",
//           img_wrapper.stride[VPX_PLANE_Y], img_wrapper.stride[VPX_PLANE_U], img_wrapper.stride[VPX_PLANE_V]);
//      printed = true;
//  }

    //  img_wrapper.x_chroma_shift = 1;
//  img_wrapper.y_chroma_shift = 1;

    vpx_codec_err_t res = vpx_codec_encode(m_vpx_codec, &img_wrapper, m_encoder_count, 1, 
                                           0, quality());
    VPX_CODEC_THROW(m_vpx_codec, res, "write_frames: failed to encode frame");

    vpx_codec_iter_t iter = nullptr;
    const vpx_codec_cx_pkt_t *pkt;

    size_t bytes_written = 0;

    while ( (pkt = vpx_codec_get_cx_data(m_vpx_codec, &iter)) )
    {
        switch (pkt->kind)
        {
        case VPX_CODEC_CX_FRAME_PKT:
            ASSERT(bytes_written + pkt->data.frame.sz <= output_buffer_size);

            memcpy(output, pkt->data.frame.buf, pkt->data.frame.sz);

            output += pkt->data.frame.sz;
            bytes_written += pkt->data.frame.sz;
            break;
        default: // ignore everything else
            break;
        }
    }

    ASSERT(vpx_codec_get_cx_data(m_vpx_codec, &iter) == nullptr);

//  NOTE: Flushing the encoder never seems to used so its being skipped for now
//  // Flush the encoder
//  res = vpx_codec_encode(m_vpx_codec, nullptr, m_encoder_count, 1, 0, quality());
//  VPX_CODEC_THROW(m_vpx_codec, res, "write_frames: failed to flush encoder");
//
//  iter = NULL;
//  while ( (pkt = vpx_codec_get_cx_data(m_vpx_codec, &iter)) )
//  {
//      switch (pkt->kind)
//      {
//      case VPX_CODEC_CX_FRAME_PKT:
//          INFO("Flushing rest of frame");
//          ASSERT(bytes_written+pkt->data.frame.sz <= output_buffer_size);
//
//          memcpy(output, pkt->data.frame.buf, pkt->data.frame.sz);
//
//          output += pkt->data.frame.sz;
//          bytes_written += pkt->data.frame.sz;
//          break;
//      default: // ignore everything else
//          break;
//      }
//  }

    m_encoder_count++;
    
    return bytes_written;
}
//
//
//void h264::init(const QUALITY m_quality)
//{
//    int r;
//    // Ignore m_quality for now...
//
//    // Set the x264 params
//    // http://stackoverflow.com/questions/2940671/how-does-one-encode-a-series-of-images-into-h264-using-the-x264-c-api
//    // and https://gist.github.com/roxlu/6453908
//    // and http://stackoverflow.com/questions/14484458/write-x264-encoder-encode-output-nals-to-h264-file
//
//    x264_param_default_preset(&m_params,  "ultrafast",  "zerolatency")
//    m_params.i_threads = 1;
//    m_params.i_width = width();
//    m_params.i_height = height();
//    m_params.i_fps_num = fps();
//    m_params.i_fps_den = 1;
//    // Intra refresh
//    m_params.i_keyint_max = fps();
//    m_params.b_intra_refresh = 1;
//    // Rate control
//    m_params.rc.i_rc_method = X264_RC_CRF;
//    m_params.rc.f_rf_constant = 25;
//    m_params.rc.f_rf_constant_max = 35;
//    // For streaming
//    m_params.b_repeat_headers = 1;
//    m_params.b_annexb = 1;
//    x264_param_apply_profile(&m_param, "baseline");
//
//    // Create the encoder with our params
//    m_encoder = x264_encoder_open(&m_params);
//    ASSERT_ALWAYS(m_encoder);
//
//    // Get the final set of interesting parameters
//    x264_encoder_parameters(m_encoder, &m_params);
//
////  int nheader = 0;
////  int r = x264_encoder_headers(m_encoder,  &m_nals, &nheader);
////  ASSERT_ALWAYS(r >= 0);
//
////  r = x264_picture_init(&m_pic_in, X264_CSP_I420, width(), height());
////  m_pic_in.img.plane[0] = work_buf();
////  m_pic_in.img.plane[1] = work_buf() + width()*height() * 1;
////  m_pic_in.img.plane[2] = work_buf() + width()*height() * 5 / 4;
//    r = x264_picture_alloc(&m_pic_in, X264_CSP_I420, width(), height());
//    ASSERT(r == 0);
//
////  int header_size = m_nals[0].i_payload + nals[1].i_payload + m_nals[2].i_payload;
//}
//
//h264::~h264()
//{
//    x264_picture_clean(&m_pic_in);
//
//    x264_encoder_close(m_encoder);
//}
//
//size_t h264::encode(uint8_t* output, const uint8_t* input,
//                   const size_t output_buffer_size, const size_t input_buffer_size,
//                   RECT rect)
//{
//    ASSERT_THROW(rect.is_empty(), "H264 encoder cannot work with variable sized clipping rectangles for now.");
//
////  if (!rect.is_empty())
////  {
////      m_config->rc_target_bitrate = rect.width * rect.height *
////        m_config->rc_target_bitrate / m_config->g_w / m_config->g_h;
////
////      m_config->g_w = rect.width;
////      m_config->g_h = rect.height;
////
////      vpx_codec_enc_config_set(m_vpx_codec, m_config);
////  }
//
//    check_rect(rect);
//
//    convert_to_yv12(work_buf(), input, rect);
//
//    x264_picture_t pic_out;
//    x264_nal_t *nal;
//    int i_nal;
//    int i_frame_size = 0;
//
//    int frame_size = x264_encoder_encode(m_encoder, &nal, &i_nals, &pic_in, &pic_out);
//
//
//}



}; // namespace color

namespace depth
{

size_t base::encode_depth_frame(uint8_t* output, const uint8_t* input)
{
    // We don't support the other depth stream formats right now...
    ASSERT_EQUAL(input_format(), STREAM_FORMAT::DEPTH_RAW); 

    const DepthPixel* input_row = (const DepthPixel*)input;
    size_t input_stride = width();

    RGB888Pixel* output_row = (RGB888Pixel*)output;
    size_t output_stride = width();

    const uint8_t* depth_map = DEPTH_ENCODE_MAP;
    const size_t max_depth = SIZE_DEPTH_ENCODE_MAP / 3;

    // This is taken from "Adapting Standard Video Codecs for Depth Streaming"
    // NOTE: Convert this into SIMD ops later
//  const double np = 512;
//  const double w = 1<<16;

    // NOTE: Using the #defines to preserve precision
//#define p np / w
//#define half_p p / 2
//#define quarter_p p / 4

    for (size_t y = 0; y < height(); y++)
    {
        const DepthPixel* input = input_row;
        RGB888Pixel* output = output_row;
        for (size_t x = 0; x < width(); x++, input++, output++)
        {
//          double L = (0.5 + (double)*input) / w;
//          double Ha = fmod(L / half_p, 2.0);
//          if (Ha < 0.0) { Ha += 2.0; }
//          if (Ha > 1.0) { Ha = 2.0 - Ha; }
//
//          double Hb = fmod((L - quarter_p) / half_p, 2.0);
//          if (Hb < 0.0) { Hb += 2.0; }
//          if (Hb > 1.0) { Hb = 2.0 - Hb; }
//
//          output->r = (uint8_t)(L * 256);
//          output->g = (uint8_t)(Ha * 256);
//          output->b = (uint8_t)(Hb * 256);

            if (*input < max_depth)
            {
                // memcpy(output, depth_map + (*input * 3), sizeof(holo3d::RGB888Pixel));
                output->r = depth_map[*input * 3 + 0];
                output->g = depth_map[*input * 3 + 1];
                output->b = depth_map[*input * 3 + 2];
            } else
            {
                ERROR("Pixel at {}x{} is out of range! value = {}", x, y, *input);
                std::memset(output, 0, sizeof(RGB888Pixel));
            }
        }

        input_row += input_stride;
        output_row += output_stride;
    }

//#undef p
//#undef half_p
//#undef quarter_p

    return width() * height() * sizeof(RGB888Pixel);
}

size_t invalid::encode(uint8_t* output, const uint8_t* input, const size_t output_buffer_size, const size_t input_buffer_size, RECT)
{
    // Do nothing and return zero
    return 0;
}

size_t raw::encode(uint8_t* output, const uint8_t* input, const size_t output_buffer_size, const size_t input_buffer_size, RECT)
{
    ASSERT(input_buffer_size <= output_buffer_size);
    memcpy(output, input, input_buffer_size);

    return input_buffer_size;
}

size_t enc::encode(uint8_t* output, const uint8_t* input, const size_t output_buffer_size, const size_t input_buffer_size, RECT)
{
    return encode_depth_frame(output, input);
}

void jpeg::init(const QUALITY quality)
{
    m_quality = jpeg_quality_convert(quality);

    m_jpeg_compressor = tjInitCompress();
    ASSERT_THROW(m_jpeg_compressor, "failed to allocate the jpeg compressor: {}", tjGetErrorStr());
}

jpeg::~jpeg()
{
    if (m_jpeg_compressor)
    {
        int ret = tjDestroy(m_jpeg_compressor);
        ASSERT_THROW (!ret, "failed to destroy the jpeg compressor: {}", tjGetErrorStr());
    }
}

size_t jpeg::encode(uint8_t* output, const uint8_t* input, const size_t output_buffer_size, const size_t input_buffer_size, RECT)
{
    encode_depth_frame(work_buf(), input);

    unsigned long compressed_size = 0;

    tjCompress2(m_jpeg_compressor, work_buf(), 
                width(), 0, height(),
                TJPF_RGB, &output, &compressed_size, 
                JPEG_SUBSAMPLING, quality(), TJFLAG_FASTDCT | TJFLAG_NOREALLOC);

    // INFO("g_jpeg_size = {}, max size={}", g_jpeg_size, MAX_COLOR_BUFFER_SIZE);

    ASSERT(compressed_size <= output_buffer_size);

    return compressed_size;
}

struct png_impl
{
    png_structp png_ptr = nullptr;
    png_infop info_ptr = nullptr;

    uint8_t* curr_buff_ptr = nullptr;
    size_t max_buff_size = 0;

    size_t bytes_transferred = 0;    
};

void png_error(png_structp png_ptr, png_const_charp error_message)
{
    THROW(error_message);
}

void png_warning(png_structp png_ptr, png_const_charp warning_message)
{
    ERROR(warning_message);
}

void write_data(png_structp png_ptr, png_bytep data, png_size_t length)
{
    png_impl* impl = (png_impl*)png_get_io_ptr(png_ptr);

    // INFO("length = {}, transferred = {}, max = {}", length, impl->bytes_transferred, impl->max_buff_size);

    ASSERT(impl->bytes_transferred + length <= impl->max_buff_size);

    memcpy(impl->curr_buff_ptr, data, length);

    impl->curr_buff_ptr += length;
    impl->bytes_transferred += length;
}

void flush_data(png_structp png_ptr)
{
    // Don't do anything...
    // INFO("Called");
}

void png::init(const QUALITY m_quality)
{
    m_impl = new png_impl;

//  m_impl->png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, png_error, png_warning);
//  ASSERT_THROW(m_impl->png_ptr, "Failed to create the write structure");
//
//  m_impl->info_ptr = png_create_info_struct(m_impl->png_ptr);
//  ASSERT_THROW(m_impl->info_ptr, "Failed to create the info structure");
//
//  png_set_write_fn(m_impl->png_ptr, m_impl, write_data, flush_data);
}

png::~png()
{   
//  png_destroy_write_struct(&m_impl->png_ptr, &m_impl->info_ptr);

    delete m_impl;
}

size_t png::encode(uint8_t* output, const uint8_t* input, const size_t output_buffer_size, const size_t input_buffer_size, RECT)
{
    // INFO("Called");

    // START HACK, I couldn't figure out how to reset the png struct between frames
    m_impl->png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, png_error, png_warning);
    ASSERT_THROW(m_impl->png_ptr, "Failed to create the write structure");

    m_impl->info_ptr = png_create_info_struct(m_impl->png_ptr);
    ASSERT_THROW(m_impl->info_ptr, "Failed to create the info structure");

    png_set_write_fn(m_impl->png_ptr, m_impl, write_data, flush_data);
    // END HACK

    m_impl->curr_buff_ptr = output;
    m_impl->bytes_transferred = 0;
    m_impl->max_buff_size = output_buffer_size;

    // This is where libpng gotos to if theres a failure...
    if (setjmp(png_jmpbuf(m_impl->png_ptr))) { THROW("Error writing header"); }

    png_set_IHDR(m_impl->png_ptr, m_impl->info_ptr, width(), height(),
                 16, PNG_COLOR_TYPE_GRAY, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

    // INFO("writing header");
    png_write_info(m_impl->png_ptr, m_impl->info_ptr);


    /* write bytes */
    if (setjmp(png_jmpbuf(m_impl->png_ptr))) { THROW("Error writing bytes"); }

    // Setup the row_pointers
    const DepthPixel* depth_input = (const DepthPixel*)input;

    png_bytep* row_pointers = (png_bytep*)work_buf();
    for (size_t y=0; y<height(); y++)
    {
        row_pointers[y] = (uint8_t*)(depth_input + y*width());
    }
    // INFO("writing image, height = {}", height());

    png_write_image(m_impl->png_ptr, row_pointers); 

    /* end write */
    if (setjmp(png_jmpbuf(m_impl->png_ptr))){ THROW("Error during end of write"); }

    png_write_end(m_impl->png_ptr, nullptr);

    // INFO("Done");

    // START HACK, See first hack
    png_destroy_write_struct(&m_impl->png_ptr, &m_impl->info_ptr);
    // END HACK

    return m_impl->bytes_transferred;
}

// Code from http://www.webmproject.org/docs/vp8-sdk/example__simple__encoder.html
void vp8::init(const QUALITY quality)
{
    m_quality = vpx_quality_convert(quality);

    // INFO("VPX interface: {}", vpx_codec_iface_name(VPX_INTERFACE));

    const vpx_codec_iface_t* iface = VPX_INTERFACE;
    ASSERT(iface);

    vpx_codec_enc_cfg_t config;
    vpx_codec_err_t res = vpx_codec_enc_config_default(iface, &config, 0);
    ASSERT_THROW(res == VPX_CODEC_OK, "failed to get config: {}", vpx_codec_err_to_string(res));

    // Adjust default target bit-rate to account for actual desktop size.
    config.rc_target_bitrate = width() * height() *
      config.rc_target_bitrate / config.g_w / config.g_h;

    config.g_w = width();
    config.g_h = height();

//  // Use millisecond granularity time base.
//  config.g_timebase.num = 1;
//  config.g_timebase.den = 1000;
//  config.g_pass = VPX_RC_ONE_PASS;
//
//  // Start emitting packets immediately.
//  config.g_lag_in_frames = 0;
//
//  // Using 2 threads gives a great boost in performance for most systems with
//  // adequate processing power. NB: Going to multiple threads on low end
//  // windows systems can really hurt performance.
//  // http://crbug.com/99179
//  config.g_threads = 4;
//
//  config.g_profile = 2;
//
//  // Clamping the quantizer constrains the worst-case quality and CPU usage.
//  config.rc_min_quantizer = 20;
//  config.rc_max_quantizer = 30;

    m_vpx_codec = new vpx_codec_ctx_t;

    res = vpx_codec_enc_init(m_vpx_codec, iface, &config, 0);
    if (res)
    {
        delete m_vpx_codec;
        m_vpx_codec = nullptr;
        THROW("failed initialize codec: {}", vpx_codec_err_to_string(res)); 
    }
}

vp8::~vp8()
{
    if (m_vpx_codec)
    {   
        vpx_codec_err_t  res = vpx_codec_destroy(m_vpx_codec);
        VPX_CODEC_THROW(m_vpx_codec, res, "error destroying vpx codec");
    }
    delete m_vpx_codec;

    if (m_work_buf2)
    {
        delete [] m_work_buf2;
    }
}

uint8_t* vp8::work_buf2()
{
    // I love lazy allocation
    if (!m_work_buf2)
    {
        // NOTE: use the work_buf_size() for now...
        // work_buf2 is just another work buffer
        m_work_buf2 = new uint8_t[work_buf_size()];
    }

    return m_work_buf2;
}

size_t vp8::encode(uint8_t* output, const uint8_t* input, const size_t output_buffer_size, const size_t input_buffer_size, RECT rect)
{
    encode_depth_frame(work_buf(), input);

    check_rect(rect);

    convert_to_yv12(work_buf2(), work_buf(), rect);

    // Encode the frame
    vpx_image_t img_wrapper;
    vpx_img_wrap(&img_wrapper, VPX_IMG_FMT_YV12, width(), height(), 1, work_buf2());

//  static bool printed = false;
//  if (!printed)
//  {
//      INFO("y stride={}, u stride = {}, v stride = {}",
//           img_wrapper.stride[VPX_PLANE_Y], img_wrapper.stride[VPX_PLANE_U], img_wrapper.stride[VPX_PLANE_V]);
//      printed = true;
//  }

    //  img_wrapper.x_chroma_shift = 1;
//  img_wrapper.y_chroma_shift = 1;

    vpx_codec_err_t res = vpx_codec_encode(m_vpx_codec, &img_wrapper, m_encoder_count, 1, 
                                           0, quality());
    VPX_CODEC_THROW(m_vpx_codec, res, "write_frames: failed to encode frame");

    vpx_codec_iter_t iter = nullptr;
    const vpx_codec_cx_pkt_t *pkt;

    size_t bytes_written = 0;

    while ( (pkt = vpx_codec_get_cx_data(m_vpx_codec, &iter)) )
    {
        switch (pkt->kind)
        {
        case VPX_CODEC_CX_FRAME_PKT:
            ASSERT(bytes_written + pkt->data.frame.sz <= output_buffer_size);

            memcpy(output, pkt->data.frame.buf, pkt->data.frame.sz);

            output += pkt->data.frame.sz;
            bytes_written += pkt->data.frame.sz;
            break;
        default: // ignore everything else
            break;
        }
    }

    ASSERT(vpx_codec_get_cx_data(m_vpx_codec, &iter) == nullptr);

    // NOTE: Flushing the encoder never seems to used so its being skipped for now
//  // Flush the encoder
//  res = vpx_codec_encode(m_vpx_codec, nullptr, m_encoder_count, 1, 0, quality());
//  VPX_CODEC_THROW(m_vpx_codec, res, "write_frames: failed to flush encoder");
//
//  iter = NULL;
//  while ( (pkt = vpx_codec_get_cx_data(m_vpx_codec, &iter)) )
//  {
//      switch (pkt->kind)
//      {
//      case VPX_CODEC_CX_FRAME_PKT:
//          INFO("Flushing rest of frame");
//          ASSERT(bytes_written+pkt->data.frame.sz <= output_buffer_size);
//
//          memcpy(output, pkt->data.frame.buf, pkt->data.frame.sz);
//
//          output += pkt->data.frame.sz;
//          bytes_written += pkt->data.frame.sz;
//          break;
//      default: // ignore everything else
//          break;
//      }
//  }

    m_encoder_count++;
    
    return bytes_written;
}

}; // namespace depth

}; // namespace encoder

}; // namespace holo3d

