
#include <iostream>

#include "gst_funcs.hpp"

namespace holo3d
{

std::ostream& operator<<(std::ostream& os, Gst::State st)
{
    switch(st)
    {
#define OUTPUT_ENUM(e) case Gst::e : os << #e; break;
    OUTPUT_ENUM(STATE_VOID_PENDING);
    OUTPUT_ENUM(STATE_NULL);
    OUTPUT_ENUM(STATE_READY);
    OUTPUT_ENUM(STATE_PAUSED);
    OUTPUT_ENUM(STATE_PLAYING);
#undef OUTPUT_ENUM
    default: 
        os << "Unknown State=" << st; 
        break;
    }

    return os;
}

}; // namespace holo3d
