#include "fragmenting_socket.hpp"

#include "util_funcs.hpp"

#include <sstream>
#include <cstring>

#include <vector>
#include <map>
#include <memory>


namespace holo3d
{
namespace socket
{

struct ChunkHeader
{
    uint16_t message_id  = 0;   // << The number of fragmented messages sent by this sender
    uint16_t chunk_count = 0;   // << The number of chunks in the message
    uint16_t chunk_index = 0;   // << The index of this chunk
    uint16_t chunk_size  = 0;   // << The size of this chunk
};

std::ostream& operator<<(std::ostream& os, const ChunkHeader& header)
{
    os  << "{ message_id = " << header.message_id
        << ", chunk_count = " << header.chunk_count
        << ", chunk_index = " << header.chunk_index
        << ", chunk_size = " << header.chunk_size
        << "}";

    return os;
}

static const size_t MAX_PACKET_SIZE = 1500;
static const size_t MAX_CHUNK_SIZE = MAX_PACKET_SIZE - sizeof(ChunkHeader);

size_t FragmentingSender::send(const void* buffer, size_t buffer_size)
{
    if (!is_connected()) 
    {
        return 0;
    }

    const uint8_t* in_buffer = (const uint8_t*)buffer;

    uint8_t temp_buf[MAX_PACKET_SIZE];

    ChunkHeader* header = (ChunkHeader*)temp_buf;
    uint8_t* data_buf = temp_buf + sizeof(ChunkHeader);
    
    header->message_id = m_num_msgs_sent;
    m_num_msgs_sent++;

    // Make sure that we can send enough chunks
    ASSERT(buffer_size < MAX_CHUNK_SIZE*0xffff);
    header->chunk_count = (buffer_size-1) / MAX_CHUNK_SIZE + 1; 

    header->chunk_index = 0;

    size_t bytes_sent = 0;

    while (buffer_size > 0) 
    {
        header->chunk_size = std::min(buffer_size, MAX_CHUNK_SIZE);

        ::memcpy(data_buf, in_buffer, header->chunk_size);
        in_buffer += header->chunk_size;
        buffer_size -= header->chunk_size;

        int ret = udp_sender::send(temp_buf, sizeof(ChunkHeader) + header->chunk_size);
        if (ret < 0) 
        {
            ERROR("Failed to send all packets in message");
            break;
        }

        // Finally, increment the chunk id
        header->chunk_index++;
        bytes_sent += header->chunk_size;
    }

    return bytes_sent;
}

size_t FragmentingSender::send_to(const void* buffer, size_t buffer_size, const endpoint& remote_ep)
{
    if (connect(remote_ep)) 
    {
        return send(buffer, buffer_size);
    } else
    {
        ERROR("Failed to connect to {}", remote_ep);
        return 0;
    }
}

struct FragmentingReceiverImpl
{
    uint8_t*    local_buffer = nullptr;
    size_t      max_bytes = 0;
    size_t      bytes_transferred = 0;

//  struct Elem
//  {
//      uint8_t* data;
//  };

    inline uint64_t calc_hash(endpoint ep, uint16_t message_id);

    using Elem = std::unique_ptr<uint8_t>;
    using ElemList = std::vector<Elem>;
    using MessageMap = std::map<uint64_t, ElemList>;

    MessageMap prev_packets = { };

    void reset();
};

FragmentingReceiver::FragmentingReceiver()
: m_impl(new FragmentingReceiverImpl)
{

}

FragmentingReceiver::~FragmentingReceiver()
{
    delete m_impl;
}

uint64_t FragmentingReceiverImpl::calc_hash(endpoint ep, uint16_t message_id)
{
    uint64_t hash = uint64_t(message_id)<<32 | uint64_t(ep.addr());
    return hash;
}

void FragmentingReceiverImpl::reset()
{
    local_buffer = nullptr;
    max_bytes = 0;
    bytes_transferred = 0;
}

size_t FragmentingReceiver::receive(void* buffer, size_t buffer_size, endpoint* remote_ep, uint32_t timeout)
{
    m_impl->local_buffer = (uint8_t*)buffer;
    m_impl->max_bytes = buffer_size;
    m_impl->bytes_transferred = 0;

    uint8_t temp_buf[MAX_PACKET_SIZE];

    endpoint temp_ep;

    timeval tv;
    get_accurate_time(&tv);
    size_t start_time = tv.tv_sec*1000 + tv.tv_usec/1000;

    while (true) 
    {
        size_t recvd_bytes = udp_receiver::receive(temp_buf, MAX_PACKET_SIZE, &temp_ep, timeout);

        if (recvd_bytes == 0) 
        {
            break;
        }

        ChunkHeader* new_header = reinterpret_cast<ChunkHeader*>(temp_buf);

        // Add the newly received chunk to the list
        uint8_t* new_buf = new uint8_t[MAX_PACKET_SIZE];
        ::memcpy(new_buf, temp_buf, recvd_bytes);

        uint64_t hash = m_impl->calc_hash(temp_ep, new_header->message_id);

        FragmentingReceiverImpl::ElemList& elem_list = m_impl->prev_packets[hash];
        elem_list.emplace_back(new_buf);
        
        // Did we get the last chunk?
        if (new_header->chunk_index+1 == new_header->chunk_count) 
        {
            for (FragmentingReceiverImpl::Elem& elem : elem_list) 
            {
                uint8_t* ptr = elem.get();
                ChunkHeader* header = (ChunkHeader*)ptr;
                
                // check max_bytes and copy the message data into local_buffer
                if (m_impl->bytes_transferred+header->chunk_size > m_impl->max_bytes) 
                {
                    // If they cannot fit, then break
                    break;
                }

                // update bytes_transferred and local_buffer;
                ::memcpy(m_impl->local_buffer, ptr+sizeof(ChunkHeader), header->chunk_size);
                m_impl->local_buffer += header->chunk_size;
                m_impl->bytes_transferred += header->chunk_size;
            }

            m_impl->prev_packets.erase(hash);
            if (remote_ep != nullptr) 
            {
                *remote_ep = temp_ep; 
            }
            break;
        }

        if (timeout > 0) 
        {
            // Check for time out
            get_accurate_time(&tv);
            size_t current_time = tv.tv_sec*1000 + tv.tv_usec/1000;
            if (timeout>0 && current_time-start_time > timeout) 
            {
                break;
            }
        }
    }

    size_t total_received = m_impl->bytes_transferred;
    m_impl->reset();

    return total_received;
}

}; // namespace socket

}; // namespace holo3d

