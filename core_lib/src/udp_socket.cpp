#include "util_funcs.hpp"

#include "udp_socket.hpp"

//#include <sys/types.h>       // For data types
#include <sys/socket.h>      // For socket(), connect(), send(), and recv()
#include <netdb.h>           // For gethostbyname()
#include <arpa/inet.h>       // For inet_addr()
#include <unistd.h>          // For close()
#include <netinet/in.h>      // For sockaddr_in
#include <string.h>         // for strerror
//
//#include <stdexcept>

#include <memory>

namespace holo3d
{

namespace socket
{

#define SOCK_ASSERT(expr, msg, ...) ASSERT_THROW_ALWAYS(expr, std::string(msg) + ": errno={}", strerror(errno));

endpoint::endpoint()
: endpoint(0, 0)
{

}

endpoint::endpoint(uint32_t addr, uint16_t port)
: m_addr(addr)
, m_port(port)
{
}

endpoint::endpoint(std::string address, uint16_t port) 
: m_port(port)
{
    size_t split_pt = address.find(':');
    if (split_pt != std::string::npos) 
    {
        std::string port_str = address.substr(split_pt+1);
        int temp_port = std::stoi(port_str);

        m_port = uint16_t(temp_port);
        address = address.substr(0, split_pt);
    }

    hostent *host = ::gethostbyname(address.c_str());
    SOCK_ASSERT(host != NULL, "Failed to resolve name (gethostbyname())");

    m_addr = ntohl(*((unsigned long *) host->h_addr_list[0]));
}

endpoint::endpoint(const sockaddr_in& addr)
: m_addr(ntohl(addr.sin_addr.s_addr))
, m_port(ntohs(addr.sin_port))
{
}

std::string endpoint::str_addr() const
{
    char buf[64];
    snprintf(buf, 64, "%d.%d.%d.%d", (m_addr>>24)&0xFF, (m_addr>>16)&0xFF, (m_addr>>8)&0xFF, (m_addr)&0xFF);
    return buf;
}
std::string endpoint::full_addr() const
{
    std::string addr_str = str_addr();
    char temp_str[64];
    snprintf(temp_str, 64, "%s:%d", addr_str.c_str(), m_port);

    return temp_str;
}


void endpoint::extract_sockaddr(sockaddr_in& addr) const
{
    ::memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(m_addr);
    addr.sin_port = htons(m_port);
}

std::ostream& operator<<(std::ostream& os, const endpoint& ep)
{
    os << ep.full_addr();
    return os;
}

base_socket::base_socket(int type, int protocol)
: m_type(type)
, m_protocol(protocol)
{
    // Make a new socket
    rebuild_socket();
}

base_socket::~base_socket()
{
    rebuild_socket(false);
}   

bool base_socket::rebuild_socket(bool rebuild)
{
    if (this->sock_descriptor >= 0) 
    {
        int ret = ::close(sock_descriptor); 
        if (ret < 0) 
        {
            ERROR("close failed: {}", strerror(errno));
            return false;
        }
    }

    this->sock_descriptor = -1;
    m_endpoint = endpoint();

    if (!rebuild) 
    {
        return true;
    }

    this->sock_descriptor = ::socket(AF_INET, m_type, m_protocol);
    if (this->sock_descriptor < 0) 
    {
        ERROR("Socket creation failed: {}", strerror(errno)); 
        return false;
    }

    return true;
}

//  static uint16_t resolve_service(const std::string& service, const string& protocol = "udp");

udp_socket::udp_socket()
: base_socket(SOCK_DGRAM, IPPROTO_UDP)
{

}

udp_socket::~udp_socket()
{
}

bool udp_socket::disconnect()
{
    return rebuild_socket(false);
}

bool udp_receiver::connect(const endpoint& endpoint)
{
    if(!rebuild_socket())
    {
        return false;
    }

    // Bind the socket to its port and any local address
    sockaddr_in localAddr;
    endpoint.extract_sockaddr(localAddr);

    int ret = ::bind(this->sock_descriptor, (sockaddr*)&localAddr, sizeof(sockaddr_in));
    if (ret < 0) 
    {
        ERROR("Set of local port failed (bind())");
        return false;
    }

    m_endpoint = endpoint;

//  INFO("Receiver bound to {}", endpoint);
    return true;
}

size_t udp_receiver::receive(void* buffer, size_t buffer_size, endpoint* ep, uint32_t timeout)
{
    if (!is_connected()) 
    {
        ERROR("Not connected!");
        return 0;
    }

    using TimePtr = std::unique_ptr<timeval>;
    TimePtr tv_ptr = nullptr;

    if (timeout > 0) 
    {
        tv_ptr.reset(new timeval);
        tv_ptr->tv_sec = 0;
        tv_ptr->tv_usec = timeout*1000;
    }

    fd_set fdset;
    FD_ZERO(&fdset); 
    FD_SET(this->sock_descriptor, &fdset);

    int rc = select(this->sock_descriptor+1, &fdset, NULL, NULL, tv_ptr.get());
    if (rc == 0) 
    {
//      ERROR("socket timeout!");
        return 0;
    } else if(rc < 0)
    {
        int so_error;
        socklen_t len;
        getsockopt(this->sock_descriptor, SOL_SOCKET, SO_ERROR, &so_error, &len);
        ERROR("socket error: {}", strerror(so_error));
        return 0;
    } else
    {
        int ret = -1;
        if (ep != nullptr) 
        {
            sockaddr_in clntAddr; 
            socklen_t addrLen = sizeof(clntAddr);
            ret = ::recvfrom(this->sock_descriptor, buffer, buffer_size, 0, (sockaddr*)&clntAddr, &addrLen);
            *ep = endpoint(clntAddr);
        } else
        {
            ret = ::recv(this->sock_descriptor, buffer, buffer_size, 0);
        }

        if(ret < 0)
        {
            ERROR("Receive failed (read()): {}", strerror(errno));
            return 0;
        }
        
        return size_t(ret);
    }
}

bool udp_sender::connect(const endpoint& remote_endpoint)
{
    return connect(remote_endpoint, false);
}

bool udp_sender::connect(const endpoint& endpoint, bool broadcast)
{
    if(!rebuild_socket())
    {
        return false;
    }

    // If this fails, we'll hear about it when we try to send.  This will allow 
    // system that cannot broadcast to continue if they don't plan to broadcast
    int broadcastPermission = broadcast ? 1 : 0;
    int ret = ::setsockopt(this->sock_descriptor, SOL_SOCKET, SO_BROADCAST, 
                           &broadcastPermission, sizeof(broadcastPermission));
    if (ret < 0) 
    {
        ERROR("Failed to setup broadcast socket"); 
        return false;
    }

    // Get the address of the requested host
    sockaddr_in destAddr;
    endpoint.extract_sockaddr(destAddr);
    
    // Try to connect to the given port
    ret = ::connect(this->sock_descriptor, (sockaddr*)&destAddr, sizeof(destAddr));
    if (ret < 0) 
    {
        ERROR("Connect failed (connect())"); 
        return false;
    }

    m_endpoint = endpoint;

    return true;
}

size_t udp_sender::send(const void* buffer, size_t buffer_size)
{
    if (!is_connected()) 
    {
        ERROR("Not connected!");
        return 0;
    }

    // Write out the whole buffer as a single message.
    // NOTE: The socket has to be connected beforehand
    int ret = ::send(this->sock_descriptor, buffer, buffer_size, 0);
    if (ret < 0) 
    {
        ERROR("Send failed (send()): {}", strerror(errno));
        return 0;
    }

    return ret;
}

size_t udp_sender::send_to(const void* buffer, size_t buffer_size, const endpoint& endpoint)
{
    if(!connect(endpoint))
    {
        return 0;
    }

    return send(buffer, buffer_size);
}

}; // namespace socket

}; // namespace holo3d

