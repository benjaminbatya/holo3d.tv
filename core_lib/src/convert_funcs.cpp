#include "util_funcs.hpp"

#include "holo3d.pb.h"

#include "h3t_file.hpp"

#include "convert_funcs.hpp"

#include <arpa/inet.h>

namespace holo3d {

protocol::STREAM_FORMAT convert_format_l2p(STREAM_FORMAT fmt)
{
#define CH(c) case STREAM_FORMAT::c: return protocol::c; break;
    switch (fmt) {
    CH(INVALID);
    CH(COLOR_RGB888);
    CH(COLOR_YUV422);
    CH(COLOR_YUYV);
    CH(COLOR_YV12);
    CH(COLOR_JPEG);
    CH(COLOR_VP8);
    CH(COLOR_H264);

    CH(DEPTH_RAW);
    CH(DEPTH_ENC);
    CH(DEPTH_JPEG);
    CH(DEPTH_PNG);
    CH(DEPTH_VP8);
//  CH(DEPTH_H264);
//  CH(DEPTH_VY12);
    default: ASSERT(false); return protocol::INVALID; break;
    }
#undef CH
}

STREAM_FORMAT convert_format_p2l(protocol::STREAM_FORMAT fmt)
{
#define CH(c) case protocol::c: return STREAM_FORMAT::c; break;
    switch (fmt) {
    CH(INVALID);
    CH(COLOR_RGB888);
    CH(COLOR_YUV422);
    CH(COLOR_YUYV);
    CH(COLOR_YV12);
    CH(COLOR_JPEG);
    CH(COLOR_VP8);
    CH(COLOR_H264);

    CH(DEPTH_RAW);
    CH(DEPTH_ENC);
    CH(DEPTH_JPEG);
    CH(DEPTH_PNG);
    CH(DEPTH_VP8);
//  CH(DEPTH_H264);
//  CH(DEPTH_VY12);
    default: ASSERT(false); return STREAM_FORMAT::INVALID; break;
    }
#undef CH
}

protocol::QUALITY convert_quality_l2p(QUALITY qual)
{
#define CH(c) case QUALITY::c: return protocol::c; break;
    switch (qual) {
    CH(REALTIME);
    CH(AVERAGE);
    CH(GOOD);
    CH(BEST);
    default: ASSERT(false); return protocol::REALTIME; break;
    }
#undef CH
}

QUALITY convert_quality_p2l(protocol::QUALITY qual)
{
#define CH(c) case protocol::c: return QUALITY::c; break;
    switch (qual) {
    CH(REALTIME);
    CH(AVERAGE);
    CH(GOOD);
    CH(BEST);
    default: ASSERT(false); return QUALITY::REALTIME; break;
    }
#undef CH
}

std::ostream& operator<<(std::ostream& os, protocol::MESSAGE_TYPE pf)
{
    os << protocol::MESSAGE_TYPE_Name(pf);

    return os;
}

MessageHeader::MessageHeader()
{
}

MessageHeader::MessageHeader(protocol::MESSAGE_TYPE type, uint16_t size, uint32_t data_size)
: msg_type(uint16_t(type))
, msg_size(size)
, data_size(data_size)
{
}

uint8_t* MessageHeader::serialize(uint8_t* buf, size_t buf_size) noexcept
{
    if(sizeof(MessageHeader) > buf_size)
    {
        ERROR("sizeof(MessageHeader)={} > buf_size={}!", sizeof(MessageHeader), buf_size);
        return nullptr;
    }

    uint16_t* ptr = (uint16_t*)buf;
    *ptr++ = htons(uint16_t(msg_type));
    *ptr++ = htons(msg_size);
    uint32_t* ptr2 = (uint32_t*)ptr;
    *ptr2++ = htonl(data_size);

    return (uint8_t*)ptr2;
}

uint8_t* MessageHeader::deserialize(uint8_t* buf, size_t buf_size) noexcept
{
    if(sizeof(MessageHeader) > buf_size)
    {
        ERROR("sizeof(MessageHeader)={} > buf_size={}!", sizeof(MessageHeader), buf_size);
        return nullptr;
    }
    uint16_t* ptr = (uint16_t*)buf;
    msg_type = ntohs(*ptr++);
    msg_size = ntohs(*ptr++);
    uint32_t* ptr2 = (uint32_t*)ptr;
    data_size = ntohl(*ptr2++);

    return (uint8_t*)ptr2;
}

size_t serialize_msg(uint8_t* buf, size_t max_size, 
                     protocol::MESSAGE_TYPE msg_type, const ::google::protobuf::Message* msg, 
                     uint8_t* data_buf, uint32_t data_buf_size)
{
    uint16_t msg_size = msg ? msg->ByteSize() : 0;

    data_buf_size = data_buf ? data_buf_size : 0;

    size_t total_size = sizeof(MessageHeader) + msg_size + data_buf_size;

    ASSERT(total_size <= max_size);

    MessageHeader header(msg_type, msg_size, data_buf_size);
    buf = header.serialize(buf, max_size);

    if (msg) 
    {
        bool res = msg->SerializeToArray(buf, msg_size); 
        ASSERT_THROW(res, "Failed to serialize message of type {}!", msg_type);
        buf += msg_size;
    }

    // Copy the extra data buffer...
    ::memcpy(buf, data_buf, data_buf_size);

    return total_size;
}

}; // namespace holo3d
