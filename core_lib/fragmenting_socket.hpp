#pragma once

#include "udp_socket.hpp"

namespace holo3d
{

namespace socket
{

class FragmentingSender : public udp_sender
{
public:
    size_t send(const void* buffer, size_t buffer_size) override;
    size_t send_to(const void* buffer, size_t buffer_size, const endpoint& remote_ep) override;

protected:
    uint16_t m_num_msgs_sent = 0;

};

struct FragmentingReceiverImpl;

class FragmentingReceiver : public udp_receiver
{
public:
    FragmentingReceiver();
    virtual ~FragmentingReceiver();

    /** 
     * @param bufffer 
     * @param buffer_size 
     * @param ep 
     * @param timeout in milliseconds, if timeout==0, then it blocks
     */ 
    size_t receive(void* buffer, size_t buffer_size, endpoint* ep = nullptr, uint32_t timeout = 0) override;

protected:
    FragmentingReceiverImpl*    m_impl = nullptr;
};

} // namespace socket

} // namespace holo3d
