#pragma once

// NOTE: Need to disable deprecated-declarations for now until gstmm is fully c++11 compliant
// From http://stackoverflow.com/a/32587042/4228547
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gstreamermm.h>
#include <gstreamermm/appsink.h>
#pragma GCC diagnostic warning  "-Wdeprecated-declarations"

#include <glibmm/refptr.h>

// NOTE: This is needed when using lambdas with libsigc++<=2.6
namespace sigc {
    SIGC_FUNCTORS_DEDUCE_RESULT_TYPE_WITH_DECLTYPE
}

namespace holo3d {

using ElementPtr = Glib::RefPtr<Gst::Element>;
using PipelinePtr = Glib::RefPtr<Gst::Pipeline>;
using BusPtr = Glib::RefPtr<Gst::Bus>;

using MessagePtr = Glib::RefPtr<Gst::Message>;
using ErrorMsgPtr = Glib::RefPtr<Gst::MessageError>;
using MsgStateChgPtr = Glib::RefPtr<Gst::MessageStateChanged>;

using BinPtr = Glib::RefPtr<Gst::Bin>;
using PadPtr = Glib::RefPtr<Gst::Pad>;
using PadTemplatePtr = Glib::RefPtr<Gst::PadTemplate>;
using CapsPtr = Glib::RefPtr<Gst::Caps>;

using AppSinkPtr = Glib::RefPtr<Gst::AppSink>;

// helper func for connecting to signals
// From http://stackoverflow.com/questions/22563621/create-sigcslot-with-no-arguments
//#define CONNECT(src, signal, ...) (src)->signal_##signal().connect(__VA_ARGS__)

std::ostream& operator<<(std::ostream& os, Gst::State st);

}; // namespace holo3d
