#ifndef __OPENNI_FACTORY_HPP__
#define __OPENNI_FACTORY_HPP__

#include "openni_device.hpp"

#include "factory_base.hpp"

namespace holo3d
{

struct openni_factory_impl;

// This initializes the OpenNI framework and creates devices.
// Should probably be wrapped in a unique_ptr<> and passed around
class openni_factory : public factory_base
{
public:
    openni_factory();
    ~openni_factory();

    void set_enable_ir(bool f);

    factory_base::uri_vec_t device_uris() override;

    factory_base::device_ptr_t device(int designed_fps, int desired_width, std::string uri = "") override;

    bool initialized();

    STREAM_FORMAT color_format() const override;
    STREAM_FORMAT depth_format() const override;

    int default_fps() const override { return 30; }
    int default_width() const override { return 640; }

protected:

    openni_factory_impl* m_impl = nullptr;
};

}; // namespace holo3d


#endif // __OPENNI_FACTORY_HPP__
