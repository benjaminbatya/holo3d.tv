#pragma once

#include <atomic>

namespace holo3d
{

// From http://stackoverflow.com/questions/26583433/c11-implementation-of-spinlock-using-atomic
class SpinLock 
{
    std::atomic_flag locked = ATOMIC_FLAG_INIT;
public:
    void lock() 
    {
        while (locked.test_and_set(std::memory_order_acquire)) 
        { 
//          std::this_thread::sleep_for(std::chrono::microseconds(100));
            // Not sure if this will compile on ARM
            asm volatile("pause" ::: "memory"); 
        }
    }
    void unlock() 
    {
        locked.clear(std::memory_order_release);
    }
};

class SpinLocker
{
    SpinLock* m_lock;
public:
    SpinLocker(SpinLock& lock)
    : m_lock(&lock)
    {
        m_lock->lock();
    }
    ~SpinLocker()
    {
        m_lock->unlock();
    }
};

}; // namespace holo3d
