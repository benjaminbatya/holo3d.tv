#pragma once

#include <stdint.h>
#include <string>

struct sockaddr_in;

namespace holo3d
{

namespace socket
{

// NOTE: This is just IPv4 for now...
class endpoint
{
public:
    endpoint();
    endpoint(uint32_t address, uint16_t port);
    endpoint(std::string address, uint16_t port = 0);
    endpoint(const sockaddr_in& addr);

    uint32_t addr() const { return m_addr; }
    uint16_t port() const { return m_port; }

    std::string str_addr() const;
    std::string full_addr() const;

    void extract_sockaddr(sockaddr_in& sock) const;

    bool operator==(const endpoint& that) const { return this->m_addr==that.m_addr && this->m_port==that.m_port; }
    bool operator!=(const endpoint& that) const { return !this->operator ==(that); }

    bool is_valid() const { return m_addr>0 || m_port>0; }

protected:

    uint32_t m_addr;
    uint16_t m_port;
};

std::ostream& operator<<(std::ostream& os, const endpoint& ep);

class base_socket
{
public:
    virtual ~base_socket();

    bool is_connected() const { return m_endpoint.is_valid(); }

    endpoint connected_endpoint() const { return m_endpoint; }

//  static uint16_t resolve_service(const std::string& service, const string& protocol = "udp");

private:
    // Prevent value semantics on this class
    base_socket(const base_socket&);
    void operator=(const base_socket& sock);

protected:
    base_socket(int type, int protocol);

    bool rebuild_socket(bool rebuild=true);
        
    int sock_descriptor = -1;

    const int m_type;
    const int m_protocol;

    endpoint m_endpoint;
};

class udp_socket : public base_socket
{
public:
    udp_socket();
    virtual ~udp_socket();


    virtual bool connect(const endpoint& endpoint) = 0;
    virtual bool disconnect();

    // figure out the multi-cast stuff later...

protected:
};  

class udp_receiver : public udp_socket
{
public:

    bool connect(const endpoint& local_endpoint) override;

    /** 
     * @param bufffer 
     * @param buffer_size 
     * @param ep 
     * @param timeout in milliseconds, if timeout==0, then it blocks 
     * @return the number of bytes received 
     */ 
    virtual size_t receive(void* buffer, size_t buffer_size, endpoint* ep = nullptr, uint32_t timeout = 0);
};

class udp_sender : public udp_socket
{
public:

    bool connect(const endpoint& remote_endpoint) override;
    bool connect(const endpoint& remote_endpoint, bool broadcast);

    virtual size_t send(const void* buffer, size_t buffer_size);
    virtual size_t send_to(const void* buffer, size_t buffer_size, const endpoint& endpoint);
};

} // namespace socket

} // namespace holo3d

