#pragma once

#include <string>
#include <memory>

namespace holo3d
{

struct FPSCounterPrivate;

class FPSCounter
{
public:
    FPSCounter(const std::string& name = "", size_t expected_fps = 30);
    ~FPSCounter();

    void name(std::string name);

    void reset();

    bool update();

    float current_fps() const;

protected:
    std::unique_ptr<FPSCounterPrivate> pImpl;
};

}; // namespace holo3d

