#include <cxxtest/TestSuite.h>

#include <future>
#include <random>
#include <array>

#include "util_funcs.hpp"

#include "fragmenting_socket.hpp"

namespace hs = holo3d::socket;

class test_connect : public CxxTest::TestSuite
{
public:

void test_receiver(void)
{
    hs::FragmentingReceiver receiver;

    TS_ASSERT(!receiver.is_connected());

    hs::endpoint local_ep = hs::endpoint(0, 9000);
    bool ret = receiver.connect(local_ep);
    TS_ASSERT(ret);

    TS_ASSERT(receiver.is_connected());

    ret = receiver.disconnect();
    TS_ASSERT(ret);

    TS_ASSERT(!receiver.is_connected());
}

void test_sender(void)
{
    hs::FragmentingSender sender;

    TS_ASSERT(!sender.is_connected());

    hs::endpoint remote_ep = hs::endpoint("127.0.0.1", 9000);
    bool ret = sender.connect(remote_ep);
    TS_ASSERT(ret);

    TS_ASSERT(sender.is_connected());

    ret = sender.disconnect();
    TS_ASSERT(ret);

    TS_ASSERT(!sender.is_connected());
}

};

class test_fragmenting_socket : public CxxTest::TestSuite
{

hs::FragmentingSender m_sender;
hs::FragmentingReceiver m_receiver;

public:

void setUp()
{   
    hs::endpoint local_ep = hs::endpoint(0, 9000);
    bool ret = m_receiver.connect(local_ep);

    hs::endpoint remote_ep = hs::endpoint("127.0.0.1", 9000);
    ret = m_sender.connect(remote_ep);
}

void tearDown()
{
    m_receiver.disconnect();
    m_sender.disconnect();
}

inline uint8_t gen_value()
{
    return (uint8_t)::rand();
}

template <size_t N>
void fill_buffer(std::array<uint8_t, N>& buffer)
{
    for (size_t i=0; i<N; i++) 
    {
        buffer[i] = gen_value();
    }
}

template <size_t N>
void compare_buffers(std::array<uint8_t, N>& a, size_t a_size, std::array<uint8_t, N>& b, size_t b_size)
{
    TS_ASSERT_LESS_THAN(0, a_size);
    TS_ASSERT_EQUALS(a_size, b_size);

    if (a_size == b_size) 
    {
        for (size_t i = 0; i < a_size; i++) 
        {
            TS_ASSERT_EQUALS(a[i], b[i]);
            // Don't loop if a value is different
            if (a[i] != b[i]) 
            {
                break; 
            }
        }
    }
}

void test_small_message_size(void)
{
    const size_t MAX_BUF_SIZE = 1024;

    using ByteArray = std::array<uint8_t, MAX_BUF_SIZE>;

    ByteArray ret_buf;
    hs::endpoint ret_ep;

    auto fut = std::async(std::launch::async, 
                          [&] {
                              return m_receiver.receive(ret_buf.data(), MAX_BUF_SIZE, &ret_ep);
                          });

    ByteArray send_buf;
    fill_buffer(send_buf);

    size_t bytes_sent = m_sender.send(send_buf.data(), MAX_BUF_SIZE);
    TS_ASSERT_EQUALS(bytes_sent, MAX_BUF_SIZE);

    size_t bytes_received = fut.get();

    compare_buffers(ret_buf, bytes_received, send_buf, bytes_sent);

    // Make sure that the endpoint is correct
    TS_ASSERT_EQUALS(ret_ep.str_addr(), "127.0.0.1");
}

void test_large_message_size(void)
{
    const size_t MAX_BUF_SIZE = 1<<20;

    using ByteArray = std::array<uint8_t, MAX_BUF_SIZE>;

    ByteArray ret_buf;

    auto fut = std::async(std::launch::async, 
                          [&] {
                              return m_receiver.receive(ret_buf.data(), MAX_BUF_SIZE);
                          });

    ByteArray send_buf;
    fill_buffer(send_buf);

    size_t bytes_sent = m_sender.send(send_buf.data(), MAX_BUF_SIZE);
    TS_ASSERT_EQUALS(bytes_sent, MAX_BUF_SIZE);

    size_t bytes_received = fut.get();

    compare_buffers(ret_buf, bytes_received, send_buf, bytes_sent);
}

void test_timeout_100ms(void)
{
    const size_t MAX_BUF_SIZE = 1<<20;

    using ByteArray = std::array<uint8_t, MAX_BUF_SIZE>;

    ByteArray ret_buf;

    auto fut = std::async(std::launch::async, 
                          [&] {
                              return m_receiver.receive(ret_buf.data(), MAX_BUF_SIZE, nullptr, 100);
                          });

    ByteArray send_buf;
    fill_buffer(send_buf);

    size_t bytes_sent = m_sender.send(send_buf.data(), MAX_BUF_SIZE);
    TS_ASSERT_EQUALS(bytes_sent, MAX_BUF_SIZE);

    size_t bytes_received = fut.get();

    compare_buffers(ret_buf, bytes_received, send_buf, bytes_sent);
}

void test_timeout_10ms(void)
{
    const size_t MAX_BUF_SIZE = 1<<20;

    using ByteArray = std::array<uint8_t, MAX_BUF_SIZE>;

    ByteArray ret_buf;

    ByteArray send_buf;
    fill_buffer(send_buf);

    auto fut = std::async(std::launch::async, 
                          [&] {
                              return m_receiver.receive(ret_buf.data(), MAX_BUF_SIZE, nullptr, 10);
                          });

    size_t bytes_sent = m_sender.send(send_buf.data(), MAX_BUF_SIZE);

    TS_ASSERT_EQUALS(bytes_sent, MAX_BUF_SIZE);

    size_t bytes_received = fut.get();

    compare_buffers(ret_buf, bytes_received, send_buf, bytes_sent);
}

void test_timeout_5ms(void)
{
    const size_t MAX_BUF_SIZE = 1<<20;

    using ByteArray = std::array<uint8_t, MAX_BUF_SIZE>;

    ByteArray ret_buf;

    ByteArray send_buf;
    fill_buffer(send_buf);

    auto fut = std::async(std::launch::async, 
                          [&] {
                              return m_receiver.receive(ret_buf.data(), MAX_BUF_SIZE, nullptr, 5);
                          });

    size_t bytes_sent = m_sender.send(send_buf.data(), MAX_BUF_SIZE);

    size_t bytes_received = fut.get();

    TS_ASSERT_EQUALS(bytes_sent, MAX_BUF_SIZE);

    compare_buffers(ret_buf, bytes_received, send_buf, bytes_sent);
}

void test_timeout_1ms(void)
{
    const size_t MAX_BUF_SIZE = 1<<20;

    using ByteArray = std::array<uint8_t, MAX_BUF_SIZE>;

    ByteArray ret_buf;

    ByteArray send_buf;
    fill_buffer(send_buf);

    auto fut = std::async(std::launch::async, 
                          [&] {
                              while (true) 
                              {
                                  size_t received = m_receiver.receive(ret_buf.data(), MAX_BUF_SIZE, nullptr, 1); 
                                  if (received > 0) 
                                  {
                                      return received;
                                  }
                              }
                          });

    size_t bytes_sent = m_sender.send(send_buf.data(), MAX_BUF_SIZE);

    size_t bytes_received = fut.get();

    TS_ASSERT_EQUALS(bytes_sent, MAX_BUF_SIZE);

    compare_buffers(ret_buf, bytes_received, send_buf, bytes_sent);
}

void test_timeout_over(void)
{
    const size_t MAX_BUF_SIZE = 1<<20;

    using ByteArray = std::array<uint8_t, MAX_BUF_SIZE>;

    ByteArray ret_buf;

    ByteArray send_buf;
    fill_buffer(send_buf);

    auto fut = 
        std::async(std::launch::async,
                   [&] {
                       return m_receiver.receive(ret_buf.data(), MAX_BUF_SIZE, nullptr, 10);
                   });

    std::this_thread::sleep_for(std::chrono::milliseconds(20));
    size_t bytes_sent = m_sender.send(send_buf.data(), MAX_BUF_SIZE);
    TS_ASSERT_EQUALS(bytes_sent, MAX_BUF_SIZE);

    size_t bytes_received = fut.get();
    TS_ASSERT_EQUALS(bytes_received, 0);
}

void test_interleaved_messages()
{
    // Copied from fragmenting_socket.cpp
    struct ChunkHeader
    {
        uint16_t message_id  = 0;   // << The number of fragmented messages sent by this sender
        uint16_t chunk_count = 0;   // << The number of chunks in the message
        uint16_t chunk_index = 0;   // << The index of this chunk
        uint16_t chunk_size  = 0;   // << The size of this chunk
    };

    hs::udp_sender sender;
    hs::endpoint remote_ep = hs::endpoint("127.0.0.1", 9000);
    bool ret = sender.connect(remote_ep);
    TS_ASSERT(ret);

    const size_t MAX_BUF_SIZE = 1024;

    using ByteArray = std::array<uint8_t, MAX_BUF_SIZE>;

    ByteArray send_buf1, send_buf2;
    fill_buffer(send_buf1);
    fill_buffer(send_buf2);

    ByteArray ret_buf;

    auto fut = 
        std::async(std::launch::async,
                   [&] {
                       int bytes_received = m_receiver.receive(ret_buf.data(), MAX_BUF_SIZE);
                       compare_buffers(ret_buf, bytes_received, send_buf1, MAX_BUF_SIZE);

                       bytes_received = m_receiver.receive(ret_buf.data(), MAX_BUF_SIZE);
                       compare_buffers(ret_buf, bytes_received, send_buf2, MAX_BUF_SIZE);
                   });

    size_t buffer_size = MAX_BUF_SIZE;
    uint8_t* ptr1 = send_buf1.data();
    uint8_t* ptr2 = send_buf2.data();

    const size_t PACKET_SIZE = 256;
    const size_t MAX_CHUNK_SIZE = PACKET_SIZE - sizeof(ChunkHeader);

    uint8_t temp_buf[PACKET_SIZE];
    ChunkHeader* header = (ChunkHeader*)temp_buf;
    uint8_t* data_buf = temp_buf + sizeof(ChunkHeader);
 
    header->chunk_count = (MAX_BUF_SIZE-1) / MAX_CHUNK_SIZE + 1; 
    header->chunk_index = 0;

    while (buffer_size > 0) 
    {
        header->chunk_size = std::min(buffer_size, MAX_CHUNK_SIZE);

        header->message_id = 3;
        ::memcpy(data_buf, ptr1, header->chunk_size);
        sender.send(temp_buf, header->chunk_size+sizeof(ChunkHeader));
        ptr1 += header->chunk_size;

        header->message_id = 42;
        ::memcpy(data_buf, ptr2, header->chunk_size);
        sender.send(temp_buf, header->chunk_size+sizeof(ChunkHeader));
        ptr2 += header->chunk_size;

        buffer_size -= header->chunk_size;
        header->chunk_index += 1;
    }

}



};
