#pragma once

#include "holo3d.pb.h"

namespace hp = holo3d::protocol;

namespace holo3d
{

enum class COLOR_MODE : uint8_t
{
    RGB  = (uint8_t)hp::STREAM_FORMAT::COLOR_RGB888,
    YUV  = (uint8_t)hp::STREAM_FORMAT::COLOR_YUV422,
    YUYV = (uint8_t)hp::STREAM_FORMAT::COLOR_YUYV,
    YV12 = (uint8_t)hp::STREAM_FORMAT::COLOR_YV12,
    JPEG = (uint8_t)hp::STREAM_FORMAT::COLOR_JPEG,
    VP8  = (uint8_t)hp::STREAM_FORMAT::COLOR_VP8,
//  H264 = (uint8_t)hp::STREAM_FORMAT::COLOR_H264,

    MIN = RGB,
    MAX = VP8, // H264,
    COUNT = MAX+1
};

inline std::ostream& operator<<(std::ostream& os, const COLOR_MODE& mode)
{
#define OUTPUT_MODE(e) case COLOR_MODE::e : os << #e; break;

    switch (mode)
    {
    OUTPUT_MODE(YUYV);
    OUTPUT_MODE(YUV);
    OUTPUT_MODE(RGB);
    OUTPUT_MODE(VP8);
//  OUTPUT_MODE(H264);
    OUTPUT_MODE(YV12);
    OUTPUT_MODE(JPEG);
    default: os << "Unknown color mode " << (int)mode; break;
    }
#undef OUTPUT_MODE

    return os;
}

inline std::istream& operator>>(std::istream& is, COLOR_MODE& mode)
{
    std::string input;
    is >> input;

#define INPUT_MODE(m) if (input == #m) { mode = COLOR_MODE::m; }

    INPUT_MODE(YUYV)
    else INPUT_MODE(YUV)
    else INPUT_MODE(RGB)
    else INPUT_MODE(VP8)
//  else INPUT_MODE(H264)
    else INPUT_MODE(YV12)
    else INPUT_MODE(JPEG)
    else
    {
        std::string msg = "Unknown color mode '" + input + "'";
        throw std::runtime_error(msg);
    }
#undef INPUT_MODE

    return is;
}

enum class DEPTH_MODE : uint8_t
{
    RAW = (uint8_t)hp::STREAM_FORMAT::DEPTH_RAW,
    ENC = (uint8_t)hp::STREAM_FORMAT::DEPTH_ENC,
    JPEG = (uint8_t)hp::STREAM_FORMAT::DEPTH_JPEG,
    PNG = (uint8_t)hp::STREAM_FORMAT::DEPTH_PNG,
    VP8 = (uint8_t)hp::STREAM_FORMAT::DEPTH_VP8,
//  H264 = (uint8_t)hp::STREAM_FORMAT::DEPTH_H264,
//  YV12 = (uint8_t)hp::STREAM_FORMAT::DEPTH_VY12,

    MIN = RAW,
    MAX = VP8,
    COUNT = MAX+1,

    INVALID = (uint8_t)hp::STREAM_FORMAT::INVALID,
};

inline std::ostream& operator<<(std::ostream& os, const DEPTH_MODE& mode)
{
#define OUTPUT_MODE(e) case DEPTH_MODE::e : os << #e; break;

    switch (mode)
    {
    OUTPUT_MODE(RAW);
    OUTPUT_MODE(ENC);
    OUTPUT_MODE(JPEG);
    OUTPUT_MODE(PNG);
    OUTPUT_MODE(VP8);
//  OUTPUT_MODE(H264);
//  OUTPUT_MODE(YV12);
    OUTPUT_MODE(INVALID);
    default: os << "Unknown depth mode " << (int)mode; break;
    }
#undef OUTPUT_MODE

    return os;
}

inline std::istream& operator>>(std::istream& is, DEPTH_MODE& mode)
{
    std::string input;
    is >> input;

#define INPUT_MODE(m) if (input == #m) { mode = DEPTH_MODE::m; }

    INPUT_MODE(RAW)
    else INPUT_MODE(ENC)
    else INPUT_MODE(JPEG)
    else INPUT_MODE(PNG)
    else INPUT_MODE(VP8)
//  else INPUT_MODE(H264)
//  else INPUT_MODE(YV12)
    else
    {
        std::string msg = "Unknown depth mode '" + input + "'";
        throw std::runtime_error(msg);
    }
#undef INPUT_MODE

    return is;
}

}; // namespace holo3d

