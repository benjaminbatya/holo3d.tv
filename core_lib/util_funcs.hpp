
#ifndef _UTIL_FUNCS_H_
#define _UTIL_FUNCS_H_

#include <stdexcept>

#include <cppformat/format.h>

// Various utility functions

const int MICROS_PER_SEC = 1000000;

bool wasKeyboardHit();
void Sleep(int millisecs);
uint64_t get_time();
// High accuracy time (hopefully)
void get_accurate_time(timeval* tv);
bool timeval_subtract (timeval& result, timeval x, timeval y);

// Start logging functions, code borrowed from https://gist.github.com/4poc/3155033

enum class VERBOSE_LEVEL : uint8_t
{
    INFO = 0
    , WARN = 1
    , ERROR = 2
    , FATAL = 3

    , ALWAYS = 255
};
void set_verbose_level(VERBOSE_LEVEL new_level);
VERBOSE_LEVEL verbose_level();

inline const std::string method_name(const std::string& long_name)
{
    size_t end = long_name.rfind("(");
    size_t begin = long_name.rfind(" ", end) + 1;
    
    return long_name.substr(begin, end - (begin));
}

template<typename... TArgs>
void _INFO_(const std::string& file, size_t line, const std::string& func_name, const std::string& msg, const TArgs& ... args)
{
    if (verbose_level() <= VERBOSE_LEVEL::INFO)
    {
        std::string str = fmt::format("INFO {0}:{1}({2}): ", file, line, method_name(func_name));
        str = fmt::format(str+msg, args...);

        fmt::print(stdout, "{}\n", str);
    }
}
#define INFO(...) _INFO_(__FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)

template<typename... TArgs>
void _INFO_SHORT_(const std::string& file, size_t line, const std::string& func_name, const std::string& msg, const TArgs& ... args)
{
    if (verbose_level() <= VERBOSE_LEVEL::INFO)
    {
        std::string short_file = file;
        size_t begin = file.rfind('/');
        if (begin != file.npos) 
        {
            short_file = file.substr(begin+1); 
        }

        std::string str = fmt::format("INFO {0}:{1}: ", short_file, line);
        str = fmt::format(str+msg, args...);

        fmt::print(stdout, "{}\n", str);
    }
}
#define INFO_SHORT(...) _INFO_SHORT_(__FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)

template<typename... TArgs>
void _WARN_(const std::string& file, size_t line, const std::string& func_name, const std::string& msg, const TArgs& ... args)
{
    if (verbose_level() <= VERBOSE_LEVEL::WARN)
    {
        std::string str = fmt::format("WARN {0}:{1}({2}): ", file, line, method_name(func_name));
        str = fmt::format(str+msg, args...);

        fmt::print(stdout, "{}\n", str);
    }
}
#define WARN(...)   _WARN_(__FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)

template<typename... TArgs>
void _ERROR_(const std::string& file, size_t line, const std::string& func_name, const std::string& msg, const TArgs& ... args)
{
    if (verbose_level() <= VERBOSE_LEVEL::ERROR)
    {
        std::string str = fmt::format("ERROR {0}:{1}({2}): ", file, line, method_name(func_name));
        str = fmt::format(str+msg, args...);

        fmt::print(stderr, "{}\n", str);
    }
}
#define ERROR(...) _ERROR_(__FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)

template<typename... TArgs>
void _FATAL_(const std::string& file, size_t line, const std::string& func_name, const std::string& msg, const TArgs& ... args)
{
    if (verbose_level() <= VERBOSE_LEVEL::FATAL)
    {
        std::string str = fmt::format("FATAL {0}:{1}({2}): ", file, line, method_name(func_name));
        str = fmt::format(str+msg, args...);

        fmt::print(stderr, "{}\n", str);
    }
}
#define FATAL(...) _FATAL_(__FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)

template<typename... TArgs>
void _ALWAYS_(const std::string& file, size_t line, const std::string& func_name, const std::string& msg, const TArgs& ... args)
{
    if (verbose_level() <= VERBOSE_LEVEL::ALWAYS)
    {
        std::string str = fmt::format("ALWAYS {0}:{1}({2}): ", file, line, method_name(func_name));
        str = fmt::format(str+msg, args...);

        fmt::print(stdout, "{}\n", str);
    }
}
#define ALWAYS(...) _ALWAYS_(__FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)

// End logging functions

// Start exception handling functions

template<typename... TArgs>
void _THROW_(const std::string& file, size_t line, const std::string& func_name, const std::string& msg, TArgs... args)
{
    std::string str = fmt::format("{0}:{1}({2}): ", file, line, method_name(func_name));
    str = fmt::format(str+msg, args...);

    std::runtime_error e(str);
    throw e;
}

#define THROW(...) _THROW_(__FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)

// This is a always on sanity checker. Dirty because it's hard to catch
#define ASSERT_ALWAYS(expr) { if(!(expr)) THROW(#expr); }
#define ASSERT_THROW_ALWAYS(expr, ...) if(!(expr)) { THROW(__VA_ARGS__); }
#define ASSERT_EQUAL_ALWAYS(result, expected) ASSERT_ALWAYS((result) == (expected))
#define ASSERT_EQUAL_THROW_ALWAYS(result, expected, ...) if(!((result) == (expected))) { THROW(__VA_ARGS__); }

#ifdef NDEBUG

// Do the expression to avoid used warnings
#define ASSERT(expr) ((void)(expr)) 
#define ASSERT_THROW(expr, ...) ((void)(expr));

#define ASSERT_EQUAL(result, expected) ((void)(result)); ((void)(expected));
#define ASSERT_EQUAL_THROW(result, expected, ...) ((void)(result)); ((void)(expected));

#else // NDEBUG

#define ASSERT(expr) { if(!(expr)) { THROW("ASSERT failed: {}=={}", #expr, (expr)); } }
#define ASSERT_THROW(expr, ...) if(!(expr)) { THROW(__VA_ARGS__); }

#define ASSERT_EQUAL(result, expected) ASSERT((result) == (expected))
#define ASSERT_EQUAL_THROW(result, expected, ...) if(!((result) == (expected))) { THROW(__VA_ARGS__); }


/*
inline void ASSERT(expr, ...)
{
    if(!(expr))
    {
        snprintf(__THROW_STR__, __MAX_STR_LEN__, "ASSERT(#expr) failed!\n");
        // snprintf(__THROW_STR__+strlen(__THROW_STR__), __MAX_STR_LEN__-strlen(__THROW_STR__), "ERROR: file '%s', line %d: error: ", __FILE__, __LINE__);

        va_list args;
        va_start(args, fmt);
        vsnprintf(__THROW_STR__+strlen(__THROW_STR__), __MAX_STR_LEN__-strlen(__THROW_STR__), fmt, args);
        va_end(args);

        fprintf(stderr, "%s\n", __THROW_STR__);
        throw __THROW_STR__;
    }
}

inline void EQUALS(test, expected, fmt...)
{
    if(!(expected == test))
    {
        snprintf(__THROW_STR__, __MAX_STR_LEN__, "EQUAL(#test, #expected) failed!\n");
        // snprintf(__THROW_STR__+strlen(__THROW_STR__), __MAX_STR_LEN__-strlen(__THROW_STR__), "ERROR: file '%s', line %d: error: ", __FILE__, __LINE__);

        va_list args;
        va_start(args, fmt);
        snprintf(__THROW_STR__+strlen(__THROW_STR__), __MAX_STR_LEN__-strlen(__THROW_STR__), fmt, args);
        va_end(args);

        fprintf(stderr, "%s\n", __THROW_STR__);
        throw __THROW_STR__;
    }
}
*/

#endif // NDEBUG

#define ASSERT_EQUALS ASSERT_EQUAL

#endif // _UTIL_FUNCS_H_
