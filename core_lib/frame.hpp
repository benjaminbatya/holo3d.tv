#ifndef _HOLO3D_FRAME_HPP_
#define _HOLO3D_FRAME_HPP_

#include <cstdint>

namespace holo3d
{

// Forward declare classes
namespace protocol
{
    class Header;
    class Frame;
}

struct camera_frame_impl;

class camera_frame
{
public:

    camera_frame();

    camera_frame(const camera_frame& that);

    // this should not be inherited from
    ~camera_frame();

    camera_frame& operator=(const camera_frame& that);
    
    const protocol::Header& header() const;
    protocol::Header& header();
    void set_header(const protocol::Header& file_header);

    const protocol::Frame& frame() const;
    protocol::Frame& frame();
    void set_frame(const protocol::Frame& header);

    void reset();

    bool valid();
    
    // Bunch of helper functions             
    uint16_t    width() const;
    uint16_t    height() const;

    uint64_t    frame_number() const;
    void        set_frame_number(uint64_t fn);

    uint64_t    time_stamp() const;
    void        set_time_stamp(uint64_t ts);

    const uint8_t* buffer() const;
    uint32_t    buffer_size() const;

    uint32_t    max_buffer_size() const;
                     
    void        set_buffer(const void* data, uint32_t size);
                         
protected:
    camera_frame_impl*  m_impl = nullptr;
};

}; // namespace holo3d

#endif // _HOLO3D_FRAME_HPP_
