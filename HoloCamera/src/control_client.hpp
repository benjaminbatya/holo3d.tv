#ifndef __CONTROL_CLIENT_HPP__
#define __CONTROL_CLIENT_HPP__

#include <thread>

#include "control_protocol.hpp"
#include "fragmenting_socket.hpp"

namespace holo3d
{

class ControlClient
{
public:
    ControlClient(uint16_t listening_port);
    ~ControlClient();

    // Different handler states
    enum class State : uint8_t
    {
        WAITING = 0
        , KEY_PRESS
        , GOT_MESSAGE
        , SENT_REPLY
    };

    void wait_for_message(); 
    void reply_to_message();

    protocol::ID message_type() const;
    const protocol::BaseData* message_data() const;
    const boost::asio::ip::udp::endpoint& sender() const;

    State state() const { return m_state; }

protected:

    void check_keys(const boost::system::error_code& error);
    void do_wait();

    boost::asio::io_service             m_service;
    boost::asio::io_service::work       m_work;

    boost::asio::ip::udp::socket        m_socket;
    boost::asio::ip::udp::endpoint      m_remote_endpoint;

    boost::posix_time::time_duration    m_delay;
    boost::asio::deadline_timer         m_timer;

    std::thread                         m_thread;

    State                               m_state = State::WAITING;

    boost::array<uint8_t, 1024>         m_receive_buffer;
};

std::ostream& operator<<(std::ostream& os, const ControlClient::State& s);


}; // namespace holo3d


#endif // __CONTROL_CLIENT_HPP__ 
