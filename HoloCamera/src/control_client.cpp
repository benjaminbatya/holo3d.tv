#include "pch_files.hpp"

#include "control_client.hpp"
#include "util_funcs.hpp"

#include <thread>

namespace ip = boost::asio::ip;

namespace holo3d
{

ControlClient::ControlClient(uint16_t listening_port)
: m_service()
, m_work(m_service)
, m_socket(m_service, ip::udp::endpoint(ip::udp::v4(), listening_port))
, m_delay(boost::posix_time::milliseconds(33))
, m_timer (m_service, m_delay)
, m_thread([&] () { m_service.run(); })
{
    do_wait();
}

ControlClient::~ControlClient()
{
    boost::system::error_code ec;
    m_socket.close(ec);
    ASSERT_THROW_ALWAYS(!ec, ec.message());

    m_service.stop();

    m_thread.join();
}

void ControlClient::wait_for_message()
{
    m_state = State::WAITING;

    m_socket.async_receive_from(boost::asio::buffer(m_receive_buffer), m_remote_endpoint, 
                                [&] (boost::system::error_code ec, std::size_t bytes)
    {
        if (ec == boost::asio::error::operation_aborted)
        {
            WARN("Operation aborted");
            return;
        }
        ASSERT_ALWAYS(!ec);
        ASSERT_ALWAYS(bytes >= sizeof(protocol::BaseData));

        // Validate the message as much as possible
        switch (message_type())
        {
        // Process QUERY and END at all times. Just send back acks
        case protocol::ID::QUERY:
        {
            ASSERT_EQUAL_ALWAYS(bytes, sizeof(protocol::QueryData));
            protocol::QueryData* query_data = (protocol::QueryData*)message_data();
            INFO("Got query message from %1%, port=%2%", m_remote_endpoint, query_data->sender_port);       
            m_state = State::GOT_MESSAGE;
            break;
        }
        default:
            // Throw on invalid protocol ids. Maybe this should be less strict
            THROW("Invalid protocol id=%1% from %2%", (uint8_t)message_type(), m_remote_endpoint);
            break;
        }
    }); 
    
    INFO("Listening on %1%", m_socket.local_endpoint());
}

void ControlClient::reply_to_message()
{
    ASSERT_EQUAL(m_state, State::GOT_MESSAGE);

    switch (message_type())
    {
    // Process QUERY and END at all times. Just send back acks
    case protocol::ID::QUERY:
    {
        protocol::QueryData* query_data = (protocol::QueryData*)message_data();
        m_remote_endpoint.port(query_data->sender_port);

        boost::system::error_code ec;
        protocol::BaseData data;
        data.id = protocol::ID::QUERY_ACK;

        size_t bytes = m_socket.send_to(boost::asio::buffer(&data, sizeof(data)), 
                                        m_remote_endpoint, 0, ec);
        ASSERT_ALWAYS(!ec);
        ASSERT_EQUAL_ALWAYS(bytes, sizeof(data));

        m_state = State::SENT_REPLY;
        INFO("Sent query_ack message to %1%", m_remote_endpoint);
        break;
    }
    default:
        // Throw on invalid protocol ids. Maybe this should be less strict
        THROW("Invalid protocol id=%d", message_type());
        break;
    }
}

protocol::ID ControlClient::message_type() const
{
    return message_data()->id;
}

const protocol::BaseData* ControlClient::message_data() const
{
    const protocol::BaseData* msg = (const protocol::BaseData*)m_receive_buffer.data();
    return(msg);
}

const boost::asio::ip::udp::endpoint& ControlClient::sender() const
{
    return m_remote_endpoint;
}

void ControlClient::do_wait()
{
//  INFO("Called");
    m_timer.expires_at(m_timer.expires_at() + m_delay);
    m_timer.async_wait([&](const boost::system::error_code& ec) { this->check_keys(ec); });
}

void ControlClient::check_keys(const boost::system::error_code& error)
{
//  INFO("Called");
    // an aborted operation is ok, just return
    if (error == boost::asio::error::operation_aborted) 
    {
        ERROR("operation cancelled");
        return;
    }

    // Throw on any other error
    ASSERT_THROW(!error, "got error: %s", error.message()); 

    if(wasKeyboardHit())
    {
        INFO("Keys pressed");
        m_state = State::KEY_PRESS;

//      m_service.stop();
    } else
    {
        this->do_wait();
    }
}

std::ostream& operator<<(std::ostream& os, const ControlClient::State& s)
{
   #define OUTPUT_STATE(e) case ControlClient::State::e : os << #e; break;

    switch (s)
    {
    OUTPUT_STATE(WAITING);
    OUTPUT_STATE(KEY_PRESS);
    OUTPUT_STATE(GOT_MESSAGE);
    OUTPUT_STATE(SENT_REPLY);
    default: os << "Unknown state"; break;
    }
#undef OUTPUT_STATE

    return os;
}
}; // namespace holo3d
