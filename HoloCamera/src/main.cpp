// Main.cpp for HoloCamera

#include "pch_files.hpp"

#include <stdio.h>
#include <cstring>
#include <signal.h>
namespace po = boost::program_options;

#include "util_funcs.hpp"

#include "option_enums.hpp"
//#include "camera.hpp"

#include "control_client.hpp"

#include "v4l_factory.hpp"
#include "openni_factory.hpp"

using namespace holo3d;

// Start static initization data

// End static initization data

factory_base* select_camera_factory()
{
    factory_base* factory = new openni_factory();

    factory_base::uri_vec_t uris = factory->device_uris();
    if (uris.size() > 0) 
    {
        INFO("Selected OpenNI system! Found these cameras:");
        for (auto& uri : uris) 
        {
            INFO(uri);
        }
        return factory; 
    } 

    delete factory;

    factory = new v4l_factory();

    uris = factory->device_uris();
    if (uris.size() > 0) 
    {
        INFO("Selected V4L system! Found these cameras:");
        for (auto& uri : uris) 
        {
            INFO(uri);
        }
        return factory;
    }

    delete factory;

    INFO("No suitable camera system found!");
    return nullptr;
}

int main(int argc, char **argv)
{    
    {
        // Print the start message
        time_t raw_time;
        time(&raw_time);
        INFO("Started %1% on %2%", argv[0], ctime(&raw_time));
    }

    // The return value, depends on if there are errors that occur or not
    int ret = 0;

    factory_base* factory = nullptr;

    //
//  // Setup the asio service
//  boost::asio::io_service         service;
//  boost::asio::io_service::work   work { service }; // The worker to force the service to stay alive even when there is no work to be done
//  boost::thread_group             threads;          // Threads spawned by the server
//  
//  // Spawn a bunch of worker threads to handle whatever work the service is asked to do
//  // NOTE: I can't debug HoloRecorder2 in slickedit if std::thread::hardware_concurrency is used because it relies on GLIBCXX_3.4.17 and
//  // slickedit only goes to GLIBCXX_3.4.16. Hopefully slickedit support will come up with a fix
//  size_t num_threads = 4; // std::thread::hardware_concurrency();
//  INFO("Spawning %1% threads", num_threads);
//  for(size_t i=0; i<num_threads; i++)
//  {
//      threads.create_thread(boost::bind(&boost::asio::io_service::run, &service));
//  }

    try
    {
        factory = select_camera_factory();
        ASSERT_THROW_ALWAYS(factory, "No camera found!");

        std::string port                   = protocol::DEFAULT_PORT;
        bool do_ntp_update              = false;

        std::string host;
        std::string app_name            = boost::filesystem::basename(argv[0]);

        po::options_description visible(std::string("Usage: ") + app_name + " host[:port(=" + port + ")] <options> \nAllowed Options");
        visible.add_options()
        ("help,h",                                                                                  "Display this help message")
        ("ntp,n",           po::value<bool>(&do_ntp_update)->default_value(do_ntp_update),          "Do the ntpdate before running the camera, only enabled on the ARM platform")     
        ("port,p",          po::value<std::string>(&port)->default_value(port),                     "The port this camera should listen for messages on")
        ;

        po::options_description all;
        all.add(visible);

        po::variables_map vm;
        try
        {
            po::store(po::command_line_parser(argc, argv).options(all).run(), vm);

            if (vm.count("help"))
            {
                INFO_CPP(visible);
                return 0;
            }

            po::notify(vm);
        }
        catch (po::required_option& e)
        {
            ERROR_CPP("No host specified!!\n");
            INFO_CPP(visible);
            return -1;
        }
        catch (po::error& e)
        {
            ERROR_CPP(e.what());
            INFO_CPP(visible);
            return -1;
        }

        // First run ntpdate if desired
#if defined PLATFORM_Arm
        if (do_ntp_update) 
        {
            // Run ntpdate server before trying to connect
            INFO("Running 'sudo ntpdate server'...");
            int result = system("sudo ntpdate server");
            if (result != 0) {
                ERROR("Failed to run ntpdate! result = %d. Continuing anyway", result); 
            }
        }
#endif // defined PLATFORM_Arm

        factory_base::uri_vec_t uris = factory->device_uris();
        // Make sure there's a camera but only use the first one found
        ASSERT(uris.size() > 0);

//      camera* camera = new camera(factory, uris[0]);

        // Setup the control channel
        int int_port = std::atoi(port.c_str());
        ASSERT_ALWAYS(0 <= int_port && int_port <= 0xffff);
        ControlClient control((uint16_t)int_port);

        bool done = false;

        while (!done) 
        {
            control.wait_for_message();

            while (control.state() == ControlClient::State::WAITING) 
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
            }


            switch (control.state()) 
            {
            case ControlClient::State::GOT_MESSAGE:
                if (control.message_type() == protocol::ID::QUERY) 
                {
                    INFO("received query message");
                    control.reply_to_message();
                } else
                {
                    THROW("unrecongized message id = %s", control.message_type());
                }
                break;
            case ControlClient::State::KEY_PRESS:
                INFO("Keypress detected. Quitting!");
                done = true;
                break;
            default:
                THROW("Invalid state!");
                break;
            }

//          // Figure out the streaming attibutes desired by the server and setup context
//          uint16_t frame_width;
//          switch (data.resolution)
//          {
//          case RESOLUTION::SMALL: frame_width = 320; break;
//          case RESOLUTION::MEDUIM: frame_width = 640; break;
//          // case RESOLUTION::LARGE: frame_width = 1024; break;
//          default: THROW("INVALID resolution size %s", data.resolution); break;
//          }
//          uint16_t frame_height = (frame_width * 3 / 4);
//
//          encoder::factory factory(frame_width, frame_height, data.quality);
//
//          camera::context_t context;
//          context.host = host;
//          context.port = port;
//          context.factory = &factory;
//          context.fps = data.fps;
//          context.color_format = data.color_format;
//          context.depth_format = data.depth_format;
//
//          if (use_regions)
//          {
//              context.region_data = g_regions;
//          } else
//          {
//              camera::region_vec_t vec;
//              vec.push_back({ {0, 0, frame_width, frame_height}, 0, 0, 0});
//              context.region_data = vec;
//          }
//
//          for(size_t i=0; i<cameras.size(); i++)
//          {
//              cameras[i]->start(context);
//          }
//
//          // Wait for an end message
//          done = control.wait_for_end();
//
//          for (size_t i=0; i<cameras.size(); i++)
//          {
//              cameras.at(i)->join(false);
//          }

//          if(done)
//          {
//              INFO("Quitting");
//          } else
//          {
//              INFO("Looping");
//          }
        }

    } catch (std::runtime_error& e)
    {
        ERROR_CPP(std::endl << "Caught runtime_error: " << e.what());
        ret = -1;
    } catch (std::exception& e)
    {
        ERROR_CPP(std::endl << "Caught exception: " << e.what());
        ret = -1;
    } catch(...)
    {
        ERROR_CPP(std::endl << "Unknown exception!!");
        ret = -1;
    }

//  // Clean up the asio service and threads
//  service.stop();
//  threads.join_all();

    // Make sure the openni system is shutdown
    delete factory;

    return ret;
}
