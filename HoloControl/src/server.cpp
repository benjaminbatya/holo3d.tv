#include <ctime>
#include <string>
#include <memory>
#include <algorithm>

#include "util_funcs.hpp"

#include "server.hpp"

#include "holo3d.pb.h"

#include "convert_funcs.hpp"

#include "frame.hpp"

namespace hs = holo3d::socket;

namespace holo3d
{

//const int SAVE_INTERVAL = 33;

static const size_t MAX_BUF_SIZE = 0xfffff;


Server::Server(const uint16_t local_port, const uint16_t remote_port, const uint16_t gst_local_port_start) 
: m_local_port(local_port)
, m_remote_port(remote_port)
, m_gst_local_port_start(gst_local_port_start)
{
    // Spin up the receive thread
    m_thread = std::thread([&] { this->receive(); });

}

Server::~Server()
{
    for (auto& p : m_pipelines) 
    {
        stop_pipeline(p.first);
    }
    
    m_is_receiving = false;
    if (m_thread.joinable()) 
    {
        m_thread.join(); 
    }
}

void Server::receive()
{
    hs::endpoint ep(0, m_local_port);
    hs::udp_receiver receive_socket;
    if(!receive_socket.connect(ep))
    {
        return;
    }

    INFO("Listening on {}", ep);

    static const size_t MAX_RECEIVE_BUF_SIZE = 0xfffff;

    uint8_t buf[MAX_RECEIVE_BUF_SIZE];
    hs::endpoint remote_endpoint;

    m_is_receiving = true;

    while (is_receiving())
    {
        size_t bytes_recved = receive_socket.receive(buf, MAX_RECEIVE_BUF_SIZE, &remote_endpoint, 10);
        if (bytes_recved == 0) 
        {
            continue;
        }
//      INFO("received {} bytes = from {}", bytes_recved, remote_endpoint);

        std::string cam_address = remote_endpoint.str_addr();

        // decode the buf into a message and place it into the message queue
        MessageHeader msg_header;
        uint8_t* ptr = msg_header.deserialize(buf, bytes_recved);
        ASSERT((void*)ptr);

        ASSERT(sizeof(MessageHeader) + msg_header.msg_size + msg_header.data_size == bytes_recved);

        switch (msg_header.type())
        {
        case protocol::QUERY_ACK:
            receive_query_ack(ptr, msg_header.msg_size, remote_endpoint);
            break;

        case protocol::STREAM_ACK:
            process_stream_ack(ptr, msg_header.msg_size, remote_endpoint);
            break;
        
        default:
            // Ignore unrecognized messages
            WARN("Unrecognized message {}", msg_header.type());
            break;
        }
    }
}

void Server::receive_query_ack(uint8_t* buf, size_t bytes_recved, const hs::endpoint& remote_endpoint)
{
    protocol::QueryAck msg;
    bool flag = msg.ParseFromArray(buf, bytes_recved);
    ASSERT_ALWAYS(flag);
    INFO("received query ack msg from {}: listener_type={}", remote_endpoint, QueryAck_ListenerType_Name(msg.listener_type()) );

//          timeval diff_time { int32_t(msg.diff().seconds()), int32_t(msg.diff().micro_seconds()) };
//
//          double time_diff = double(diff_time.tv_sec*MICROS_PER_SEC + diff_time.tv_usec)/MICROS_PER_SEC;
//          INFO("Time difference = {}", time_diff);

    m_cameras.push_back(remote_endpoint);
}

void Server::process_stream_ack(uint8_t* buf, size_t bytes_recved, const hs::endpoint& remote_endpoint)
{
    INFO("received stream ack msg from {}", remote_endpoint);
    protocol::StreamAck msg;
    bool flag = msg.ParseFromArray(buf, bytes_recved);
    ASSERT_THROW_ALWAYS(flag, "flag = {}", flag);
    
    std::string cam_address = remote_endpoint.str_addr();

    m_headers[cam_address] = msg.header();
    
    start_pipeline(cam_address);
}

void Server::start_streaming()
{
    // stop any remaining pipelines
    stop_streaming();

    // Send START_STREAMING messages to each of the cameras listed in context
    protocol::StreamStart msg;

    protocol::Header* header = msg.mutable_header();
    uint64_t magic = *(uint64_t*)(FILE_MAGIC);
    header->set_magic(magic);
    header->set_major_number(MAJOR_VERSION);
    header->set_minor_number(MINOR_VERSION);
    header->set_format(convert_format_l2p(STREAM_FORMAT::COLOR_H264));

//  switch (context.camera_config.resolution)
//  {
//  case RESOLUTION::SMALL: header->set_width(320); header->set_height(200); break;
//  case RESOLUTION::MEDUIM: header->set_width(640); header->set_height(480); break;
//  case RESOLUTION::LARGE: header->set_width(1024); header->set_height(768); break;
//  default: ASSERT(false); break;
//  }
    // These are the ideal settings
    header->set_width(1024); header->set_height(768);
    header->set_quality(convert_quality_l2p(QUALITY::BEST));
    header->set_fps(60);

    uint8_t buf[MAX_BUF_SIZE];

    for (uint16_t i=0; i<m_cameras.size(); i++)
    {
        const hs::endpoint& endpoint = m_cameras[i];
        std::string cam_address = endpoint.str_addr();
        uint16_t gst_port = m_gst_local_port_start + i;

        m_ports[cam_address] = gst_port;

        msg.set_port(gst_port);

        size_t msg_size = serialize_msg(buf, MAX_BUF_SIZE, protocol::STREAM_START, &msg);
        ASSERT(msg_size > 0);

        socket::endpoint ep(cam_address, m_remote_port);

        INFO("Sending STREAM_START message to {}", ep);
        m_sender.send_to(buf, msg_size, ep);
    }
}

void Server::stop_streaming()
{
    for (auto& elem : m_headers) 
    {
        const std::string& cam_address = elem.first;

        // Send STOP_STREAMING messages to each of the cameras listed in context
        static uint8_t buf[MAX_BUF_SIZE];
        size_t msg_size = serialize_msg(buf, MAX_BUF_SIZE, protocol::STREAM_STOP);

        socket::endpoint ep(cam_address, m_remote_port);
        INFO("Sending STREAM_STOP message to {}", ep);
        m_sender.send_to(buf, msg_size, ep);

        stop_pipeline(cam_address);
    }

    m_headers.clear();
    m_ports.clear();
    m_pipelines.clear();
}

/**
 * Finds cameras from a list of possible cameras
 *
 * @author benjamin (11/20/2014)
 */
void Server::find_cameras()
{
    m_cameras.clear();
    
    // Send a Query message
    protocol::Query msg;

    //      INFO("Msg type = {}", MESSAGE_TYPE_Name(msg.type()));

    msg.set_sender_port(m_local_port);

    protocol::TimeStamp* ts = msg.mutable_sender_time_stamp();

    // Set the time
    timeval tmp_time;
    get_accurate_time(&tmp_time);
    ts->set_seconds(tmp_time.tv_sec);
    ts->set_micro_seconds(tmp_time.tv_usec);

    uint8_t buf[MAX_BUF_SIZE];
    size_t msg_size = serialize_msg(buf, MAX_BUF_SIZE, protocol::QUERY, &msg);

    hs::endpoint ep("192.168.10.0", m_remote_port);

    INFO("broadcasting {} bytes to {}", msg_size, ep);

    bool ret = m_sender.connect(ep, true);
    if (!ret) 
    {
        return;
    }
    // Send the query message
    m_sender.send(buf, msg_size); 

    // Wait for a second for all of the cameras in the local network to respond
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
}

size_t Server::num_cameras()
{
    return m_cameras.size();
}

std::string Server::camera_address(size_t cam_idx)
{
    return m_cameras[cam_idx].str_addr();
}

// NOTE: we stop the pipeline here but we DO not erase it from m_pipelines
// That is done in stop_streaming...
void Server::stop_pipeline(const std::string& addr)
{
    ASSERT(m_pipelines.find(addr) != m_pipelines.end());

    PipelinePtr pipeline = m_pipelines[addr];
    // Tell all listeners that the pipeline is stopping
    sig_pipeline_stopping.emit(addr);

    // Shutdown the pipeline
    pipeline->set_state(Gst::STATE_NULL);

    // wait until the pipeline's state is null
    while (true)
    {
        Gst::State state, void_state = Gst::State(0);
        Gst::StateChangeReturn ret = pipeline->get_state(state, void_state, Gst::MILLI_SECOND);
        ASSERT(ret != Gst::STATE_CHANGE_FAILURE);
            
        if (state == Gst::STATE_NULL) 
        {
            break;
        }
    }
    sig_new_frame.erase(addr);
}

void Server::start_pipeline(const std::string& addr)
{
    // NOTE: maybe build these in the ctor?
    // They never change
    std::string caps_str = "application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96";
    INFO(caps_str);
    CapsPtr caps = Gst::Caps::create_from_string(caps_str);
    ASSERT(caps);

    std::string caps_str2 = "video/x-raw,format=(string)RGB";
    INFO(caps_str2);
    CapsPtr caps2 = Gst::Caps::create_from_string(caps_str2);
    ASSERT(caps2);

    // Create each of the elements needed
    // Now build a new pipeline
    PipelinePtr pipeline = Gst::Pipeline::create();
    ASSERT(pipeline);

    ElementPtr udp_src = Gst::ElementFactory::create_element("udpsrc");
    ASSERT(udp_src);
    uint16_t port = m_ports[addr];
    udp_src->set_property("port", int(port));

    ElementPtr caps_filter = Gst::ElementFactory::create_element("capsfilter");
    caps_filter->set_property("caps", caps);      

    ElementPtr rtp_h264_depay = Gst::ElementFactory::create_element("rtph264depay");
    ASSERT(rtp_h264_depay);

    ElementPtr h264_dec = Gst::ElementFactory::create_element("avdec_h264");
    ASSERT(h264_dec);

    ElementPtr video_convert = Gst::ElementFactory::create_element("videoconvert");
    ASSERT(video_convert);

    ElementPtr caps_filter2 = Gst::ElementFactory::create_element("capsfilter");
    caps_filter2->set_property("caps", caps2);

    AppSinkPtr app_sink = Gst::AppSink::create("app_sink");
    ASSERT(app_sink);
    
    app_sink->property_drop().set_value(true);
    app_sink->property_emit_signals().set_value(true);
    app_sink->signal_new_sample().connect([this, addr] (void) { return this->handle_new_frame(addr); });

    // Add all of the elements
    pipeline->add(udp_src)->add(caps_filter)->add(rtp_h264_depay)->add(h264_dec);
    pipeline->add(video_convert)->add(caps_filter2)->add(app_sink);

    // Link all of the elements
    udp_src->link(caps_filter)->link(rtp_h264_depay)->link(h264_dec);
    h264_dec->link(video_convert)->link(caps_filter2)->link(app_sink);

    BusPtr bus = pipeline->get_bus();
    bus->enable_sync_message_emission();
    bus->signal_sync_message().connect([this, addr] (const MessagePtr& msg) { this->handle_bus_message(addr, msg); });

    m_pipelines[addr] = pipeline;

    sig_new_frame[addr] = Server::SigFrame();

    sig_pipeline_starting.emit(addr);

    // Startup the pipeline
    pipeline->set_state(Gst::STATE_PLAYING);
}

const protocol::Header& Server::header(const std::string& address)
{
    auto it = m_headers.find(address);
    ASSERT(it != m_headers.end());
    return it->second;
}

uint16_t Server::port(const std::string& address)
{
    auto it = m_ports.find(address);
    ASSERT(it != m_ports.end());
    return it->second;
}

//PipelinePtr Server::pipeline(const std::string& addr) const
//{
//    return m_pipelines[addr];
//}

Gst::FlowReturn Server::handle_new_frame(std::string addr)
{
    PipelinePtr pipeline = m_pipelines[addr];
    ASSERT(pipeline);

    ElementPtr app_sink_elem = pipeline->get_element("app_sink");
    ASSERT(app_sink_elem);

    AppSinkPtr app_sink = AppSinkPtr::cast_dynamic(app_sink_elem);
    ASSERT(app_sink);

    if (app_sink->property_eos()) 
    {
        ERROR("Reached the end of the stream! Shouldn't be here!!");
        return Gst::FLOW_EOS;
    }

    using SamplePtr = Glib::RefPtr<Gst::Sample>;
    SamplePtr sample = app_sink->pull_sample();
    ASSERT(sample);
//  INFO("0x{}", (void*)sample->gobj());

    CapsPtr caps = sample->get_caps();
    // NOTE: for gstreamermm 1.4.3, this reference MUST be here. get_caps() doesn't reference properly!
    caps->reference(); 
    ASSERT(caps);

    const Gst::Structure caps_struct = caps->get_structure(0);

//  Glib::ustring str = caps_struct.to_string();
//  INFO(str);

    const holo3d::protocol::Header& header = this->header(addr);
    
// #ifdef DEBUG
    // Sanity check
    int width, height;
    caps_struct.get_field("width", width);
    caps_struct.get_field("height", height);
    ASSERT_EQUALS(width, int(header.width()));
    ASSERT_EQUALS(height, int(header.height()));
    Glib::ustring format;
    caps_struct.get_field("format", format);
    ASSERT(format.compare("RGB") == 0);
// #endif

    using BufferPtr = Glib::RefPtr<Gst::Buffer>;
    BufferPtr buffer = sample->get_buffer();
    ASSERT(buffer);

    // The pts is invalid
//  Gst::ClockTime pts = buffer->get_pts();
//  INFO("Frame dts = {} msec", pts / GST_MSECOND );

    {
        using MapInfoPtr = Glib::RefPtr<Gst::MapInfo>;
        MapInfoPtr map_info(new Gst::MapInfo());
        bool ret = buffer->map(map_info, Gst::MAP_READ);
        ASSERT(ret);

        ASSERT_EQUALS(map_info->get_size(), width*height*sizeof(RGB888Pixel));

        sig_new_frame[addr].emit(map_info->get_data(), width, height);

//      QImage out_image = m_processor->process(map_info->get_data());
//      m_color_pixmap = QPixmap::fromImage(out_image);

        buffer->unmap(map_info);
    }
    // NOTE: ONLY update() maybe called here. NOT repaint()!
    // Because process_frame will be called from a different thread!!
//  m_parent->update();
//
//  m_display_counter.update();

    return Gst::FLOW_OK;
}


void Server::handle_bus_message(std::string addr, const MessagePtr& msg)
{
    ASSERT(msg);

    PipelinePtr pipeline = m_pipelines[addr];
    ASSERT(pipeline);

    switch (msg->get_message_type()) 
    {
    case Gst::MESSAGE_ERROR:
    {
        ErrorMsgPtr err = ErrorMsgPtr::cast_static(msg);
        Glib::Error error;
        std::string debug_info;
        err->parse(error, debug_info);
        ERROR("Pipeline {}: Error received from element '{}': {}", 
              addr, err->get_source()->get_name(), error.what());
        ERROR("Debugging information: {}", debug_info);
        // TODO: shutdown the pipeline!!
//      running = false;
    } break;
    case Gst::MESSAGE_EOS:
        INFO("Pipeline {}: End of stream reached", addr);
        // TODO: maybe shutdown the pipeline. Or throw an exception
//      running = false;
        break;
    case Gst::MESSAGE_STATE_CHANGED:
        if (msg->get_source() == pipeline) 
        {
            MsgStateChgPtr chgmsg = MsgStateChgPtr::cast_static(msg);
            Gst::State old_state = chgmsg->parse_old();
            Gst::State new_state = chgmsg->parse();
            INFO("Pipeline {} changed from {} to {}", addr, old_state, new_state);

//                  GstBin* bin = GST_BIN(pipeline->gobj());
//                  char buf[42];
//                  snprintf(buf, 43, "graph_%d", new_state);
//                  GST_DEBUG_BIN_TO_DOT_FILE(bin, GST_DEBUG_GRAPH_SHOW_ALL, buf);
        }
        break;
    default:
        ERROR("Pipeline {}: Unexpected message '{}' from {} received", addr, 
              gst_message_type_get_name(GstMessageType(msg->get_message_type())),
              msg->get_source()->get_name());
        break;
    }
}

}; // namespace holo3d
