#include "controller.hpp"

#include <iostream>
#include <fstream>
#include <cstdint>

//#include <yaml-cpp/yaml.h>

#include "server.hpp"

#include "camera_monitor.hpp"
#include "intrinsic_calibration_view.hpp"

#include "option_enums.hpp"
#include "util_funcs.hpp"

#include <Qt/QtCore>

namespace holo3d
{

class ControllerPrivate
{
    Q_DISABLE_COPY(ControllerPrivate);
    Q_DECLARE_PUBLIC(Controller);
    Controller* const   q_ptr;

    ControllerPrivate(Controller* parent, Server* server);

    Server*             m_server = nullptr;
        
    sigc::connection    m_start_conn;
    sigc::connection    m_stop_conn;

    using CameraMonitorMap = QMap<QString, CameraMonitor*>;
    CameraMonitorMap    m_views;

    QLayout*            m_monitors_layout = nullptr;

    IntrinsicCalibrationView*           m_main_view;
};

ControllerPrivate::ControllerPrivate(Controller* interface, Server* server)
: q_ptr(interface)
, m_server(server)
{

}

Controller::Controller(Server* server, QWidget* parent)
: QMainWindow(parent)
, d_ptr(new ControllerPrivate(this, server))
{
    Q_D(Controller);

    setupUi(this);

    splitter->setStretchFactor(0, 1);
    splitter->setStretchFactor(1, 3);

    d->m_monitors_layout = monitors_container->layout();

    d->m_main_view = new IntrinsicCalibrationView(selected_frame, server);
    d->m_main_view->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QLayout* layout = selected_frame->layout();
    layout->addWidget(d->m_main_view);

    connect(d->m_main_view, SIGNAL(status_message(QString)), statusBar(), SLOT(showMessage(QString)));

//  connect(load_action, SIGNAL(triggered()), this, SLOT(load_settings()));
//  connect(save_action, SIGNAL(triggered()), this, SLOT(save_settings()));
    connect(rescan_action, SIGNAL(triggered()), SLOT(rescan_cameras()));
    connect(quit_action, SIGNAL(triggered()), QApplication::instance(), SLOT(quit()));

    auto start_func = [this](std::string address)
    {
        QMetaObject::invokeMethod(this, "on_pipeline_start", Q_ARG(QString, QString(address.c_str())));
    };
    d->m_start_conn = server->sig_pipeline_starting.connect(start_func);

    auto stop_func = [this](std::string address)
    {
        QMetaObject::invokeMethod(this, "on_pipeline_stop", Q_ARG(QString, QString(address.c_str())));
    };
    d->m_stop_conn = server->sig_pipeline_stopping.connect(stop_func);
}

Controller::~Controller()
{
    Q_D(Controller);
    d->m_stop_conn.disconnect();
    d->m_start_conn.disconnect();

//  INFO("called");
    quit();
}

void Controller::show()
{
    rescan_cameras();

    // Load the last settings
//  load_settings(true);m_monitors_layout

    QMainWindow::show();
}

void Controller::closeEvent(QCloseEvent* event)
{
//  INFO("Called");

    QApplication::quit();

    event->accept();
}

void Controller::quit()
{
//  INFO("Called");
}

void Controller::rescan_cameras()
{
    Q_D(Controller);
    d->m_server->stop_streaming();

    ASSERT_EQUALS(d->m_views.size(), 0);

    d->m_server->find_cameras();

    d->m_server->start_streaming();
}

void Controller::on_pipeline_start(QString addr)
{
    Q_D(Controller);

    INFO("Pipeline {} started", addr.toStdString());

    // Create a new monitor widget
//  QWidget* test = new QLabel(QString("Camera ") + addr, this);
//  d->m_monitors_layout->addWidget(test);

    CameraMonitor* new_monitor = new CameraMonitor(this, d->m_server, addr);
    d->m_monitors_layout->addWidget(new_monitor);

    d->m_views[addr] = new_monitor;

    connect(new_monitor, SIGNAL(sig_clicked(QString)), d->m_main_view, SLOT(set_address(QString)));
}

void Controller::on_pipeline_stop(QString addr)
{
    Q_D(Controller);

    INFO("Pipeline {} stopped", addr.toStdString());

    ASSERT(d->m_views.contains(addr));

    CameraMonitor* monitor = d->m_views[addr];
    d->m_monitors_layout->removeWidget(monitor);

    delete monitor;
    d->m_views.remove(addr);
}

}; // namespace holo3d
