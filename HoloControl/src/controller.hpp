#pragma once

#include <Qt/QtGui>

#include "ui_controller.h"

namespace holo3d
{

class CameraMonitor;
class Server;

class ControllerPrivate;

/**
 * This is a Qt Controller for HoloRecorder
 * 
 * @author benjamin (3/3/2015)
 * @param  
 */
class Controller : public QMainWindow, private Ui_Controller
{
    Q_OBJECT;
    Q_DECLARE_PRIVATE(Controller);
    QScopedPointer<ControllerPrivate> const d_ptr;

public:
    Controller(Server* server, QWidget* parent=0);
    ~Controller();

    void show();
    
protected Q_SLOTS:

    void quit();
    void rescan_cameras();
    void on_pipeline_start(QString addr);
    void on_pipeline_stop(QString addr);

protected:

    void closeEvent(QCloseEvent* event);

    void populate_controls();
};

}; // namespace holo3d

