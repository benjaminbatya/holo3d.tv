#include "camera_monitor.hpp"

#include "gst_funcs.hpp"

#include "server.hpp"

#include "util_funcs.hpp"

#include "fps_counter.hpp"

#include "holo3d.pb.h"

#include "spin_lock.hpp"

namespace holo3d
{

class CameraMonitorPrivate
{
    Q_DISABLE_COPY(CameraMonitorPrivate);
    Q_DECLARE_PUBLIC(CameraMonitor);
    CameraMonitor* const q_ptr;

    CameraMonitorPrivate(CameraMonitor* parent, Server* server, QString addr);

//  AppSinkPtr m_app_sink;

    Server*             m_server;
    std::string         m_address;

    sigc::connection    m_new_frame_conn;

    QPixmap             m_pixmap;
    int                 m_width = 16, m_height = 9;

    FPSCounter          m_counter;
    SpinLock            m_processing;     // << Used to prevent the widget from being destroyed until the processor is finished

    void on_new_frame(uint8_t* data, int width, int height);
};

CameraMonitorPrivate::CameraMonitorPrivate(CameraMonitor* parent, Server* server, QString addr)
: q_ptr(parent)
, m_server(server)
, m_address(addr.toStdString())
, m_counter(m_address)
{
}

void CameraMonitorPrivate::on_new_frame(uint8_t* data, int width, int height)
{
    Q_Q(CameraMonitor);
//  INFO("New frame from address {} received, width={}, height={}", m_address, width, height);
    m_width = width;
    m_height = height;

    SpinLocker lock(m_processing);

    QImage out_image(data, m_width, m_height, QImage::Format_RGB888);
    m_pixmap = QPixmap::fromImage(out_image);

    q->update();

//  m_counter.update();
}

CameraMonitor::CameraMonitor(QWidget *parent, Server *server, QString addr)
: QWidget(parent)
, d_ptr(new CameraMonitorPrivate(this, server, addr))
{
    Q_D(CameraMonitor);
    d->m_new_frame_conn = 
        d->m_server->sig_new_frame[d->m_address].connect([d] (uint8_t* data, int width, int height) { 
        d->on_new_frame(data, width, height);
    });

    // NOTE: this QSizePolicy is required to maintain the right aspect ratio
    QSizePolicy sp(QSizePolicy::Preferred, QSizePolicy::Preferred);
    sp.setHeightForWidth(true);
    setSizePolicy(sp);

    const protocol::Header& header = d->m_server->header(addr.toStdString());

    setToolTip(QString("Camera %1, fps=%2").arg(addr).arg(header.fps()));
}

CameraMonitor::~CameraMonitor()
{
    Q_D(CameraMonitor);
    d->m_new_frame_conn.disconnect();

    SpinLocker locker(d->m_processing);

    INFO("done");
}

int CameraMonitor::heightForWidth(int w) const
{
    Q_D(const CameraMonitor);
//  INFO("Width = {}", w);
    // Preserve a 16/9 aspect ratio
    return w * d->m_height / d->m_width;
}

void CameraMonitor::paintEvent(QPaintEvent* event)
{
    QWidget::paintEvent(event);

    Q_D(CameraMonitor);

    if (d->m_pixmap.isNull()) 
    {
        return;
    }

    QPainter painter(this);

    double scaleX = ((double)width()) / d->m_width;
    double scaleY = ((double)height()) / d->m_height;
    scaleX = std::min(scaleX, scaleY);


    scaleY = scaleX;
//  if (pImpl->m_vert_flip)
//  {
//      scaleY *= -1.0;
//      m_painter.translate(0, height());
//  }

    painter.scale(scaleX, scaleY);

    painter.drawPixmap(0, 0, d->m_pixmap);
}

void CameraMonitor::mouseReleaseEvent(QMouseEvent * event)
{
    Q_D(CameraMonitor);
    Q_EMIT sig_clicked(QString(d->m_address.c_str()));
}

}; // namespace holo3d
