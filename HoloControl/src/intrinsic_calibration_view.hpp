#pragma once

#include <Qt/QtGui>

#include "ui_intrinsic_calibration_view.h"

namespace holo3d
{

class Server;

class IntrinsicCalibrationViewPrivate;

class IntrinsicCalibrationView : public QWidget, public Ui_IntrinsicCalibrationView
{
    Q_OBJECT;
    Q_DECLARE_PRIVATE(IntrinsicCalibrationView);
    QScopedPointer<IntrinsicCalibrationViewPrivate> const d_ptr;

public:
    IntrinsicCalibrationView(QWidget* parent, Server* server);
    ~IntrinsicCalibrationView();

public Q_SLOTS:
    void set_address(QString addr);

protected Q_SLOTS:
    void grab_frame(const QPixmap&);
    
Q_SIGNALS:
    void status_message(QString msg);
};

}; // namespace holo3d
