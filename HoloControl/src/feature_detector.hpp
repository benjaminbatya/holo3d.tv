#pragma once

#include <cv.h>

namespace holo3d
{

class DetectBase
{
public:

    using PointVec = std::vector<cv::Point2f>;

    virtual bool detect(const uint8_t* data, PointVec& out_corners) = 0;
    const uint8_t* render_corners(const uint8_t* data);

    virtual void set_size(int width, int height);

protected:
    DetectBase(const cv::Size& pattern_size = {0, 0} );

    const cv::Size m_pattern_size;

    cv::Mat m_cv_image;
    cv::Mat m_cv_gray;
    int m_width = 0;
    int m_height = 0;
};

class DetectNone : public DetectBase
{
public:
    bool detect(const uint8_t* data, PointVec& out_corners) override;
};

class DetectChessboardCorners : public DetectBase
{
public:
    DetectChessboardCorners();
    bool detect(const uint8_t* data, PointVec& out_corners) override;
};

class DetectCircleGrid : public DetectBase
{
public:
    DetectCircleGrid();
    bool detect(const uint8_t* data, PointVec& out_corners) override;
};

}; // namespace holo3d
