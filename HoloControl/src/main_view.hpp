#pragma once

#include <Qt/QtGui>

namespace holo3d
{

class Server;
class DetectBase;

// This follows the standard Qt PIMPL idiom at
// http://stackoverflow.com/questions/25250171/how-to-use-the-qts-pimpl-idiom
struct MainViewPrivate;

class MainView : public QWidget
{
    Q_OBJECT;
    Q_DECLARE_PRIVATE(MainView);
    QScopedPointer<MainViewPrivate> const d_ptr;

public:
    MainView(QWidget* parent, Server* server);
    ~MainView();

    int heightForWidth(int w) const override;
    void paintEvent(QPaintEvent* event) override;

public Q_SLOTS:
    void set_address(QString address);

protected Q_SLOTS:
    void show_context_menu(const QPoint& pos);
    void on_timeout();

Q_SIGNALS:
    void status_message(QString msg);
    void sig_new_frame(const QPixmap&);
};

}; // namespace holo3d
