#pragma once

//#include "file_saver.hpp"
#include <vector>
#include <map>
#include <string>
#include <thread>

#include "udp_socket.hpp"

#include "h3t_file.hpp"

#include "gst_funcs.hpp"

namespace holo3d
{

namespace protocol {
class Header;
};

class Server
{
public:
    Server(const uint16_t local_port, const uint16_t remote_port, const uint16_t gst_local_port);
    ~Server();

    void set_done(bool f);      // << call this when you want the Server to shutdown

    // Various signals
    using SigPipeline = sigc::signal<void, std::string>;
    SigPipeline sig_pipeline_starting;
    SigPipeline sig_pipeline_stopping;

    // This is emitted whenever a new frame is received.
    // The signal signature is void(uint8_t* buffer_ptr, int width, int height)
    using SigFrame = sigc::signal<void, uint8_t*, int, int>;
    using SigFrameMap = std::map<std::string, SigFrame>;
    SigFrameMap sig_new_frame;

    void find_cameras();

    size_t num_cameras();
    std::string camera_address(size_t cam_idx);

    const protocol::Header& header(const std::string& address);
    uint16_t port(const std::string& address);

//    // Causes the server to start recording based on the context
    void start_streaming();
//
//    // This cancels all of the file_savers and cleans up
    void stop_streaming();

    bool is_receiving() const { return m_is_receiving; }

//  PipelinePtr pipeline(const std::string& addr) const;

protected:

//  void handle_camera_connect(const boost::system::error_code& error,
//                             const size_t& bytes_recvd);

    void receive();

    void receive_query_ack(uint8_t* buf, size_t bytes_recved, const socket::endpoint& remote_endpoint);
    void process_stream_ack(uint8_t* buf, size_t bytes_recved, const socket::endpoint& remote_endpoint);

    void start_pipeline(const std::string& addr);
    void stop_pipeline(const std::string& addr);

    Gst::FlowReturn handle_new_frame(std::string addr);
    void handle_bus_message(std::string addr, const MessagePtr& msg);
                        
    std::thread         m_thread;           // << The thread which will receive the messages from the cameras

//  Context             m_context;          // << The context for this server
    
//  using file_saver_vec_t = std::vector<file_saver_ptr_t>;
//  file_saver_vec_t    m_file_savers;      // << Saves to file one of the camera streams

    using camera_vec_t = std::vector<socket::endpoint>;
    camera_vec_t        m_cameras;          // << Each of the discovered cameras
    
    const uint16_t      m_local_port;       // << Local port for receiving messages
    const uint16_t      m_remote_port;      // << Remote port for receiving messages
    const uint16_t      m_gst_local_port_start;   // << The next local port for gstreamer to be sent
    uint64_t            m_start_time = 0;   // << Time the cameras should start grabbing frames at

    bool                m_is_receiving = false;

    socket::udp_sender  m_sender;

    using HeaderMap     = std::map<std::string, protocol::Header>;
    HeaderMap           m_headers;

    using PortMap       = std::map<std::string, uint16_t>;
    PortMap             m_ports;

    using PipelinePtrVec= std::map<std::string, PipelinePtr>;
    PipelinePtrVec      m_pipelines;
};


}; // namespace holo3d

