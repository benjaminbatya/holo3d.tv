#include "feature_detector.hpp"

#include "h3t_file.hpp"

namespace holo3d
{

DetectBase::DetectBase(const cv::Size& pattern_size)
: m_pattern_size(pattern_size)
{
}

void DetectBase::set_size(int width, int height)
{
    m_cv_image.create(height, width, CV_8UC3);
    m_cv_gray.create(height, width, CV_8UC1);

    m_width = width;
    m_height = height;
}

bool DetectNone::detect(const uint8_t*, PointVec&)
{
    return false;
}

const uint8_t* DetectBase::render_corners(const uint8_t* data)
{
    PointVec corners;

    bool pattern_found = detect(data, corners);

    if (m_pattern_size.area() > 0) 
    {
        cv::drawChessboardCorners(m_cv_image, m_pattern_size, cv::Mat(corners), pattern_found); 
        return m_cv_image.data;
    } else
    {
        return data;
    }
}

DetectChessboardCorners::DetectChessboardCorners()
: DetectBase(cv::Size(9, 6))
{ }

bool DetectChessboardCorners::detect(const uint8_t* data, PointVec& corners)
{
    ::memcpy(m_cv_image.data, data, m_width*m_height*sizeof(RGB888Pixel));
    cv::cvtColor(m_cv_image, m_cv_gray, CV_RGB2GRAY);

    bool pattern_found = cv::findChessboardCorners(m_cv_gray, m_pattern_size, corners,
                                                   cv::CALIB_CB_ADAPTIVE_THRESH +
                                                   cv::CALIB_CB_NORMALIZE_IMAGE +
                                                   cv::CALIB_CB_FAST_CHECK);
    if (pattern_found)
    {
        cv::cornerSubPix(m_cv_gray, corners, cv::Size(11, 11), cv::Size(-1, -1),
                         cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
    }

    return pattern_found;
}

DetectCircleGrid::DetectCircleGrid()
: DetectBase({10, 7})
{ }

bool DetectCircleGrid::detect(const uint8_t* data, PointVec& corners)
{
    ::memcpy(m_cv_image.data, data, m_width*m_height*sizeof(RGB888Pixel));
    cv::cvtColor(m_cv_image, m_cv_gray, CV_RGB2GRAY);

    return cv::findCirclesGrid(m_cv_gray, m_pattern_size, corners);
}


}; // namespace holo3d
