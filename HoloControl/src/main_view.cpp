#include "main_view.hpp"

#include "gst_funcs.hpp"

#include "server.hpp"

#include "util_funcs.hpp"

#include "fps_counter.hpp"

#include "holo3d.pb.h"

#include "spin_lock.hpp"

#include "feature_detector.hpp"

#include <moodycamel/readerwriterqueue.h>

#include <cv.h>

namespace holo3d
{

struct QueueElem
{
    std::unique_ptr<uint8_t[]> data_buf;
    size_t data_buf_size;

    QueueElem(uint8_t* buf = nullptr, size_t size = 0)
    : data_buf(buf)
    , data_buf_size(size)
    { }

    QueueElem(QueueElem&& that)
    : data_buf(std::move(that.data_buf))
    , data_buf_size(that.data_buf_size)
    { }

    QueueElem& operator=(QueueElem&& that)
    {
        data_buf = std::move(that.data_buf);
        data_buf_size = that.data_buf_size;

        return *this;
    }

    const uint8_t* const operator*() const
    {
        return data_buf.get();
    }

    bool is_valid() const
    {
        return data_buf != nullptr && data_buf_size > 0;
    }
};

class MainViewPrivate
{
    Q_DISABLE_COPY(MainViewPrivate);
    Q_DECLARE_PUBLIC(MainView);
    MainView* const q_ptr;

    MainViewPrivate(MainView* parent, Server* server);

    Server*             m_server;

    sigc::connection    m_new_frame_conn;

    using BufferQueue = moodycamel::ReaderWriterQueue<QueueElem>;

    std::atomic_bool    m_running { true };
    std::thread         m_thread;
    BufferQueue         m_queue { 64 };

    QPixmap             m_pixmap;
    int                 m_width = 16, m_height = 9;

    FPSCounter          m_counter {"Main View", 30};
    SpinLock            m_processing;           // << Used to prevent the widget from being destroyed until the processor is finished
    SpinLock            m_load_frames;          // << Used to prevent frames from being loaded in on_new_frame while a new address or processor is being selected

    QString             m_address { "" };

    QMenu*              m_menu;
    QAction*            m_none_action;
    QAction*            m_chess_action;
    QAction*            m_circles_action;

    QTimer*             m_timer;

    std::unique_ptr<DetectBase> m_feature_detector;

    void on_new_frame(const uint8_t* data, int width, int height);

    void process_frame();

    void disconnect();

    void set_feature_detector(DetectBase* detector);

public:
    ~MainViewPrivate();
};

MainViewPrivate::MainViewPrivate(MainView* parent, Server* server)
: q_ptr(parent)
, m_server(server)
, m_feature_detector(new DetectNone)
{
    // start up the thread
    m_thread = std::thread(&MainViewPrivate::process_frame, this);

    m_menu = new QMenu(parent);
    m_none_action = m_menu->addAction("Detect None");
    m_chess_action = m_menu->addAction("Detect Chessboard");
    m_circles_action = m_menu->addAction("Detect Circles");
}

MainViewPrivate::~MainViewPrivate()
{
    m_running = false;
    m_thread.join();

    disconnect();
}

void MainViewPrivate::on_new_frame(const uint8_t* data, int width, int height)
{
    SpinLocker lock(m_load_frames);

    //  INFO("New frame from address {} received, width={}, height={}",
    //       m_address.toStdString(), width, height);
    ASSERT_EQUALS(width, m_width);
    ASSERT_EQUALS(height, m_height);

    size_t buf_size = width*height*sizeof(RGB888Pixel);
    uint8_t* buf = new uint8_t[buf_size];
    ::memcpy(buf, data, buf_size);

    bool success = m_queue.try_enqueue(QueueElem(buf, buf_size));

    if (!success) 
    {
        WARN("Can't enqueue frame. Skipping it..");
    } 
}

void MainViewPrivate::process_frame()
{
    while (m_running) 
    {
        QueueElem buffer;
        // get the latest buffer
        while (m_queue.try_dequeue(buffer)) 
        {
            ;
        }

        if (buffer.is_valid()) 
        {
//              INFO("Got valid buffer");

            Q_Q(MainView);

            {
                SpinLocker lock(m_processing);

                const uint8_t* data = m_feature_detector->render_corners(*buffer);
     
                QImage out_image(data, m_width, m_height, QImage::Format_RGB888);
                m_pixmap = QPixmap::fromImage(out_image);
            }

            q->update();

            bool new_fps = m_counter.update();
            if (new_fps) 
            {
                QString msg = QString("Camera %1, fps=%2").arg(m_address).arg(m_counter.current_fps());
        //      INFO("Emitting '{}'", msg.toStdString());
                Q_EMIT q->status_message(msg);
            }
        } 

        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}

void MainViewPrivate::disconnect()
{
    m_new_frame_conn.disconnect();

    SpinLocker locker(m_processing);
}

void MainViewPrivate::set_feature_detector(DetectBase* detector)
{
    SpinLocker lock(m_processing);

    m_feature_detector.reset(detector);
    m_feature_detector->set_size(m_width, m_height);
}

MainView::MainView(QWidget *parent, Server *server)
: QWidget(parent)
, d_ptr(new MainViewPrivate(this, server))
{
    // NOTE: this QSizePolicy is required to maintain the right aspect ratio
    QSizePolicy sp(QSizePolicy::Expanding, QSizePolicy::Preferred);
    sp.setHeightForWidth(true);
    setSizePolicy(sp);

    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), SLOT(show_context_menu(const QPoint&)));

    Q_D(MainView);

    d->m_timer = new QTimer(this);
    connect(d->m_timer, SIGNAL(timeout()), SLOT(on_timeout()));
    d->m_timer->start(3000); 
}

MainView::~MainView()
{
//  Q_D(MainView);
        
    // INFO("done.");
}

void MainView::set_address(QString address)
{
    Q_D(MainView);
    d->disconnect();

    SpinLocker lock(d->m_load_frames);

    d->m_address = address;

    const protocol::Header& header = d->m_server->header(address.toStdString());
    d->m_width = header.width();
    d->m_height = header.height();

    INFO("Address = {}, width = {}, height = {}", address.toStdString(), d->m_width, d->m_height);

    d->m_feature_detector->set_size(d->m_width, d->m_height);

    d->m_new_frame_conn = 
        d->m_server->sig_new_frame[address.toStdString()].connect([d] (uint8_t* data, int width, int height) { 
        d->on_new_frame(data, width, height);
    });

    setToolTip(QString("Camera ") + address);
}

int MainView::heightForWidth(int w) const
{
    Q_D(const MainView);
//  INFO("Width = {}", w);
    // Preserve a 16/9 aspect ratio
    return w * d->m_height / d->m_width;
}

void MainView::paintEvent(QPaintEvent* event)
{
    QWidget::paintEvent(event);

    Q_D(MainView);

    if (d->m_pixmap.isNull()) 
    {
        return;
    }

    QPainter painter(this);

    double scaleX = ((double)width()) / d->m_width;
    double scaleY = ((double)height()) / d->m_height;
    scaleX = std::min(scaleX, scaleY);

    scaleY = scaleX;

    painter.scale(scaleX, scaleY);

    painter.drawPixmap(0, 0, d->m_pixmap);
}

void MainView::show_context_menu(const QPoint& pos)
{
    Q_D(MainView);

    QPoint global_pos = this->mapToGlobal(pos);

    QAction* action = d->m_menu->exec(global_pos);
    if (action == d->m_none_action) 
    {
        d->set_feature_detector(new DetectNone);
    } else if(action == d->m_chess_action)
    {
        d->set_feature_detector(new DetectChessboardCorners);
    } else if(action == d->m_circles_action) 
    {
        d->set_feature_detector(new DetectCircleGrid);
    }
}

void MainView::on_timeout()
{
    Q_D(MainView);

    Q_EMIT sig_new_frame(d->m_pixmap);
}

}; // namespace holo3d
