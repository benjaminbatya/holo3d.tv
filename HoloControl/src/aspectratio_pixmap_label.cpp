#include "aspectratio_pixmap_label.hpp"

#include "util_funcs.hpp"

// NOTE: this is from http://stackoverflow.com/questions/8211982/qt-resizing-a-qlabel-containing-a-qpixmap-while-keeping-its-aspect-ratio

AspectRatioPixmapLabel::AspectRatioPixmapLabel(QWidget *parent) :
    QLabel(parent)
{
    this->setMinimumSize(1,1);
}

void AspectRatioPixmapLabel::setPixmap ( const QPixmap & p)
{
    INFO("pixmap size = ({}, {})", p.width(), p.height());

    pix = p.copy();
//  QLabel::setPixmap(pix);
    QLabel::setPixmap(pix.scaled(this->size(),
                                 Qt::KeepAspectRatio,
                                 Qt::SmoothTransformation));
}

int AspectRatioPixmapLabel::heightForWidth( int width ) const
{
    return (qreal(pix.height())*width)/pix.width();
}

QSize AspectRatioPixmapLabel::sizeHint() const
{
    int w = this->width();
    return QSize( w, heightForWidth(w) );
}

void AspectRatioPixmapLabel::resizeEvent(QResizeEvent * e)
{
    if(!pix.isNull())
    {
        QLabel::setPixmap(pix.scaled(this->size(),
                                     Qt::KeepAspectRatio, 
                                     Qt::SmoothTransformation));
    }
}


