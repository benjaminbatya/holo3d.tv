#pragma once

#include <Qt/QtGui>

namespace holo3d
{

class Server;

// This follows the standard Qt PIMPL idiom at
// http://stackoverflow.com/questions/25250171/how-to-use-the-qts-pimpl-idiom
struct CameraMonitorPrivate;

class CameraMonitor : public QWidget
{
    Q_OBJECT;
    Q_DECLARE_PRIVATE(CameraMonitor);
    QScopedPointer<CameraMonitorPrivate> const d_ptr;

public:
    CameraMonitor(QWidget* parent, Server* server, QString addr);
    ~CameraMonitor();

    int heightForWidth(int w) const override;
    void paintEvent(QPaintEvent* event) override;
    void mouseReleaseEvent(QMouseEvent * event) override;

Q_SIGNALS:
    void sig_clicked(QString);
};

}; // namespace holo3d
