#include "intrinsic_calibration_view.hpp"

#include "server.hpp"
#include "main_view.hpp"

#include "util_funcs.hpp"

#include "aspectratio_pixmap_label.hpp"

#include <Qt/QtCore>
#include <vector>

namespace holo3d
{

 
class ListDelegate : public QAbstractItemDelegate
{
public:
    ListDelegate(QObject *parent = 0)
    : QAbstractItemDelegate(parent)
    { }

    void paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
    {
        QRect r = option.rect;
//      INFO("Rect = ({},{},{},{})", r.left(), r.top(), r.width(), r.height());
        const QPixmap& pixmap = qvariant_cast<QPixmap>(index.data(Qt::DecorationRole));
        painter->drawPixmap(r, pixmap);
    }
    QSize sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const
    {
        QSize image_size = qvariant_cast<QSize>(index.data(Qt::UserRole+1));
        QObject* p = this->parent();
        QListView* view = qobject_cast<QListView*>(p);
        
        int scroll_size = QApplication::style()->pixelMetric(QStyle::PM_ScrollBarExtent);
        int width = view->width() - 8 - scroll_size;
//      INFO("Width = {}", width);
        int height = width * image_size.height()/image_size.width();

        return QSize(width, height);
    }

    virtual ~ListDelegate() { }
};

class IntrinsicCalibrationViewPrivate
{
    Q_DISABLE_COPY(IntrinsicCalibrationViewPrivate);
    Q_DECLARE_PUBLIC(IntrinsicCalibrationView);
    IntrinsicCalibrationView* const q_ptr;

    IntrinsicCalibrationViewPrivate(IntrinsicCalibrationView* parent, Server* server);

    Server*     m_server;

    MainView*   m_main_view;

    using LabelVec = std::vector<QLabel>;
    LabelVec    m_frames;
};

IntrinsicCalibrationViewPrivate::IntrinsicCalibrationViewPrivate(IntrinsicCalibrationView* interface,
                                                                 Server* server)
: q_ptr(interface)
, m_server(server)
{

}

IntrinsicCalibrationView::IntrinsicCalibrationView(QWidget* parent, Server* server)
: QWidget(parent)
, d_ptr(new IntrinsicCalibrationViewPrivate(this,server))
{
    Q_D(IntrinsicCalibrationView);
    setupUi(this);

    splitter->setStretchFactor(0, 3);
    splitter->setStretchFactor(1, 1);

    d->m_main_view = new MainView(a_video_frame, server);
    d->m_main_view->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);

    a_video_frame->setLayout(new QVBoxLayout);
    QLayout* layout = a_video_frame->layout();
    layout->addWidget(d->m_main_view);

    frame_list->setItemDelegate(new ListDelegate(frame_list));
    
    connect(d->m_main_view, SIGNAL(status_message(QString)), SIGNAL(status_message(QString)));
    connect(d->m_main_view, SIGNAL(sig_new_frame(const QPixmap&)), SLOT(grab_frame(const QPixmap&)));

}

IntrinsicCalibrationView::~IntrinsicCalibrationView()
{

}

void IntrinsicCalibrationView::set_address(QString addr)
{
    Q_D(IntrinsicCalibrationView);
    d->m_main_view->set_address(addr);
}


void IntrinsicCalibrationView::grab_frame(const QPixmap& pixmap)
{
    INFO("Called");

    QListWidgetItem* item = new QListWidgetItem(frame_list);
    item->setData(Qt::DecorationRole, pixmap);
    item->setData(Qt::UserRole+1, pixmap.size());
    frame_list->addItem(item);
}

}; // namespace holo3d
