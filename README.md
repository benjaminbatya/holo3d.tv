# README #

This is the Holo3D.tv code repository.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up  
Run ```git clone https://bensch128@bitbucket.org/bensch128/holo3d.tv.git``` to fetch the entire repository  
Run `scons` to build all configurations for all platforms  
Run `cd project; scons CFG=Config-Platform` to build that particular project for that particular configuration and platform  
Run `cd project; scons CFG=Config-Platform run` to build and run that particular project  

* Projects
    + CameraClient - This is used to record and stream RGB+D data in the h3t format.
    + MovieViewer - This is used to play files or streams of h3t video in an overlay

* Configuration
    + CameraClient
        - Debug-x86 = Debug configuration running on the x86
        - Release-x86 = Release configuration running on the x86
        - Debug-Arm = Debug configuration running on the Arm
        - Release-Arm = Release configuration running on the Arm
    + MovieViewer
        - Debug = Debug configuration running on the x86
        - Release = Release configuration running on the x86

* Dependencies
    + libvpx
    + libyuv
    + libboost
    + libGLUT
    + libOpenNI2

* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact