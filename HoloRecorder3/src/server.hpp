#pragma once

//#include "file_saver.hpp"
#include <vector>
#include <map>
#include <string>
#include <thread>

#include "udp_socket.hpp"

#include "h3t_file.hpp"

namespace holo3d
{

namespace protocol {
class Header;
};

//struct ServerImpl;

class camera_frame;

class Server
{
public:
    Server(const uint16_t local_port, const uint16_t remote_port);
    ~Server();

    void set_done(bool f);      // << call this when you want the Server to shutdown

    using NewCameraCB = std::function<void(std::string)>;
    using NewFrameCB = std::function<void(const camera_frame& frame)>;

    using NewCameraCBVec = std::vector<NewCameraCB>;
    NewCameraCBVec      m_new_camera_cb_vec;
    using NewFrameCBVec = std::vector<NewFrameCB>;
    using NewFrameCBMap = std::map<std::string, NewFrameCBVec>;
    NewFrameCBMap       m_new_frame_cb_map;

//  using NewCameraSig = Simple::Signal<void ( uint32_t ) >;
//  NewCameraSig m_new_camera_sig;
//  Simple::Signal<void(uint32_t)> m_new_camera_sig;

//  typedef Simple::Signal<void(const camera_frame& frame)> NewFrameSig;
//  using NewFrameSig  = Simple::Signal<void ( const camera_frame& frame ) >;
//  using NewFrameSigMap = std::map<uint32_t, NewFrameSig>;
//  NewFrameSigMap m_new_frame_map;

    size_t register_new_camera_cb(NewCameraCB func);
    bool remove_new_camera_cb(size_t id);
    size_t register_new_frame_cb(const std::string& addr, NewFrameCB func);
    bool remove_new_frame_cb(const std::string& addr, size_t id);

    using string_vec_t = std::vector<std::string>;

    string_vec_t find_cameras();

    struct CameraConfig {
        RESOLUTION resolution;
        STREAM_FORMAT format;
        STREAM_FORMAT depth_format;
        uint32_t fps;
        QUALITY quality;
    };

    struct Context
    {
        string_vec_t        cameras_to_run { }; // << List of cameras to run
        std::vector<uint16_t> gst_ports { };
//      std::string         save_directory { }; // << The directory to save the recordings to
//      bool                recording { };      // << Indicates whether the file_savers should record or not
        CameraConfig        camera_config { };
    };

    // Causes the server to start recording based on the context
    void start_recording(const Context& context);

    // This cancels all of the file_savers and cleans up
//  void stop_recording();

    void stop_recording(const std::string& address);

    bool is_running() const { return m_is_running; }
    bool is_recording() const { return m_is_recording; }

    const protocol::Header& header(const std::string& address);
    uint16_t port(const std::string& address);

protected:

//  void handle_camera_connect(const boost::system::error_code& error,
//                             const size_t& bytes_recvd);

    void receive();

    void receive_query_ack(uint8_t* buf, size_t bytes_recved, const socket::endpoint& remote_endpoint);
                                                        
    std::thread         m_thread;           // << The thread which will receive the messages from the cameras

//  Context             m_context;          // << The context for this server
    
//  using file_saver_vec_t = std::vector<file_saver_ptr_t>;
//  file_saver_vec_t    m_file_savers;      // << Saves to file one of the camera streams

    using camera_vec_t = std::vector<socket::endpoint>;
    camera_vec_t        m_cameras;          // << Each of the discovered cameras
    
    uint16_t            m_local_port;       // <<                                         
    uint16_t            m_remote_port;      // << 
    uint64_t            m_start_time = 0;   // << Time the cameras should start grabbing frames at

    bool                m_is_running = false;
    bool                m_is_recording = false;

    Context             m_context;

    socket::udp_sender  m_sender;

    using HeaderMap     = std::map<std::string, protocol::Header>;
    HeaderMap           m_headers;

    using PortMap       = std::map<std::string, uint16_t>;
    PortMap             m_ports;

};


}; // namespace holo3d

