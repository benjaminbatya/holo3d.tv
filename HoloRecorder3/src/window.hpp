#pragma once

// window.hpp - displays the movie
#include <memory>

#include <Qt/QtGui>

#include "h3t_file.hpp"

namespace holo3d
{

// Forward decl
class Server;

enum class DisplayModes
{
    BLEND = 1,
    COLOR = 2,
    DEPTH = 3,

    // These are to allow iterating over the enums
    MIN = BLEND,
    MAX = DEPTH,
    COUNT,
};

inline std::ostream& operator<<(std::ostream& os, const DisplayModes& mode)
{
#define OUTPUT_MODE(e) case DisplayModes::e : os << #e; break;

    switch (mode)
    {
    OUTPUT_MODE(DEPTH);
    OUTPUT_MODE(COLOR);
    OUTPUT_MODE(BLEND);
    default: os << "Unknown display mode"; break;
    }
#undef OUTPUT_MODE

    return os;
}
inline std::istream& operator>>(std::istream& is, DisplayModes& mode)
{
    std::string input;
    is >> input;

#define INPUT_MODE(m) if (input == #m) { mode = DisplayModes::m; }

    INPUT_MODE(DEPTH)
    else INPUT_MODE(COLOR)
    else INPUT_MODE(BLEND)
    else
    {
        throw ("Unknown display mode");
    }
#undef INPUT_MODE

    return is;
}


class Window : public QDialog
{
    Q_OBJECT

    struct Impl;

public:
    Window(const QString& camera_address, Server* server, QWidget* parent = 0);
    ~Window(); // Window is not intended to be inherited from

    void show();

    /**
     * Setter/getter combo for m_use_histogram. Call this as 
     * window->use_histogram(true) or window->use_histogram()
     *  
     * @author benjamin (11/10/2014)
     * 
     * @param use sets m_use_histogram
     * 
     * @return bool the value of m_use_histogram
     */
    bool use_histogram() const;
    void set_use_histogram(bool f);
    DisplayModes video_mode() const;
    void set_video_mode(const DisplayModes& mode);

//  file_saver* saver() const { return m_saver; }

    const QString& address() const;
//
//signals:
//    void closed()

protected:

    void init_window();
//  void process_frame(const camera_frame& frame);

    void paintEvent(QPaintEvent* e);

    void keyReleaseEvent(QKeyEvent *event);

    void closeEvent(QCloseEvent* event);

    std::unique_ptr<Impl>     pImpl;

    QPainter       m_painter;
};

}; // namespace holo3d

