
// NOTE: gstreamer functionality has to be included before Qt functionality
// See http://stackoverflow.com/questions/18424425/gstreamermm-and-qt-compilation-error
#include "gst_funcs.hpp"

#include <gstreamermm/appsink.h>
#include <cv.h>

#include "window.hpp"

#include "server.hpp"
#include "util_funcs.hpp"
#include "fps_counter.hpp"
#include "decoder.hpp"
#include "frame.hpp"

#include "holo3d.pb.h"

#include "convert_funcs.hpp"

namespace holo3d
{

#define DEFAULT_DISPLAY_MODE	DISPLAY_MODE_DEPTH

class ImageProcessor
{
public:
    ImageProcessor(int width, int height);

    QImage process(uint8_t* src);

protected:
    const int m_width, m_height;
    int m_thresh;
    cv::Mat cv_image, cv_gray, canny_output, drawing;
    std::vector<std::vector<cv::Point> > contours; 
    std::vector<cv::Vec4i> hierarchy; 
};

struct Window::Impl
{
    Impl(Window* parent, const QString& camera_address, Server* server)
    : m_parent(parent)
    , m_server(server)
    , m_address(camera_address)
    { }

    ~Impl()
    {
        m_server->stop_recording(m_address.toStdString());
    }

    void build_pipeline(uint16_t gst_port);
    void start_pipeline();
    void stop_pipeline();
//  void process_gst_message(const MessagePtr& msg);
    Gst::FlowReturn handle_new_frame();

    Window*         m_parent;

    Server*         m_server = nullptr;

    const QString   m_address;

    FPSCounter      m_pipeline_counter;
    FPSCounter      m_display_counter;

    DisplayModes    m_view_state = DisplayModes::BLEND;

    std::unique_ptr<ImageProcessor> m_processor;

    QPixmap         m_color_pixmap;
//  QPixmap         m_depth_pixmap;

    bool            m_paused = false;

    bool            m_use_histogram = false;

    bool            m_vert_flip     = false;

    size_t          m_width = 0;
    size_t          m_height = 0;

    QMutex          m_mutex;

    size_t          m_frame_sig_id = 0;

    PipelinePtr     m_pipeline;
    BusPtr          m_bus;

//  ElementPtr      m_display_sink;
    using AppSinkPtr = Glib::RefPtr<Gst::AppSink>;
    AppSinkPtr      m_app_sink;
};

Window::Window(const QString& camera_address, Server* server, QWidget* parent)
: QDialog(parent)
, pImpl(new Window::Impl(this, camera_address, server))   // TODO: use make_unique here!
{
    init_window();

    pImpl->m_pipeline_counter.name(fmt::format("Data-in from '{}' and pipeline", camera_address.toStdString()));
    pImpl->m_display_counter.name("Output");
}

Window::~Window()
{
    INFO("Called, address = {} ", pImpl->m_address.toStdString());

    pImpl->stop_pipeline();
}

void Window::init_window()
{
    const protocol::Header& header = pImpl->m_server->header(pImpl->m_address.toStdString());

    pImpl->m_width = header.width();
    pImpl->m_height = header.height();

    std::string str = fmt::format("Camera {} at {}x{}", 
                                  pImpl->m_address.toStdString(), pImpl->m_width, pImpl->m_height);
    setWindowTitle(str.c_str());

    uint16_t port = pImpl->m_server->port(pImpl->m_address.toStdString());
    pImpl->build_pipeline(port);
}

void Window::show()
{
    QWidget::show();

    pImpl->start_pipeline();
}

void Window::paintEvent(QPaintEvent* e)
{
    if (pImpl->m_color_pixmap.isNull())
    {
        return;
    }

    //  INFO("Called");
    double scaleX = ((double)width()) / pImpl->m_width;
    double scaleY = ((double)height()) / pImpl->m_height;
    scaleX = std::min(scaleX, scaleY);

    m_painter.begin(this);
    m_painter.save();

    scaleY = scaleX;
    if (pImpl->m_vert_flip)
    {
        scaleY *= -1.0;
        m_painter.translate(0, height());
    }

    m_painter.scale(scaleX, scaleY);

//  if (m_depth_decoder->format() != STREAM_FORMAT::INVALID)
//  {
//      switch (m_view_state)
//      {
//      case DisplayModes::BLEND:
//          m_painter->drawPixmap(0, 0, m_color_pixmap);
//          m_painter->setCompositionMode(QPainter::CompositionMode_Multiply);
//          m_painter->drawPixmap(0, 0, m_depth_pixmap);
//          break;
//      case DisplayModes::COLOR:
//          m_painter->drawPixmap(0, 0, m_color_pixmap);
//          break;
//      case DisplayModes::DEPTH:
//          m_painter->drawPixmap(0, 0, m_depth_pixmap);
//          break;
//      default:
//          THROW("INVALID DisplayMode");
//          break;
//      }
//  } else
    {
        m_painter.drawPixmap(0, 0, pImpl->m_color_pixmap);
    }

    m_painter.restore();
    m_painter.end();
}

bool Window::use_histogram() const
{ 
    return false; // m_depth_decoder->use_histogram(); 
}

void Window::set_use_histogram(bool f) 
{ 
//  QMutexLocker locker(&m_mutex);
//  m_depth_decoder->use_histogram(f);
}

void Window::keyReleaseEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Escape: // esc key
        close();
        break;

    case Qt::Key_1:
        pImpl->m_view_state = DisplayModes::BLEND;
        update();
        break;

    case Qt::Key_2:
        pImpl->m_view_state = DisplayModes::COLOR;
        update();
        break;

    case Qt::Key_3:
//      m_view_state = DisplayModes::DEPTH;
//      update();
        break;

    case Qt::Key_F:
        pImpl->m_vert_flip = !pImpl->m_vert_flip;
        break;

    case Qt::Key_P:
        pImpl->m_paused = !pImpl->m_paused;
        break;

    case Qt::Key_H:
        set_use_histogram(!use_histogram());
        break;

//  case Qt::Key_M:
//      m_stream_manager.toggle_mirroring();
//      break;

    default:
        QWidget::keyReleaseEvent(event);
        break;
    }
}

void Window::closeEvent(QCloseEvent* event)
{
    INFO("Called");

    QDialog::closeEvent(event);

    accept();

    event->accept();

    this->deleteLater();
}

const QString& Window::address() const 
{ 
    return pImpl->m_address; 
}

DisplayModes Window::video_mode() const 
{ 
    return pImpl->m_view_state; 
}

void Window::set_video_mode(const DisplayModes& mode) 
{ 
    pImpl->m_view_state = mode; 
}

void Window::Impl::build_pipeline(uint16_t gst_port)
{
    m_pipeline = Gst::Pipeline::create();
    ASSERT(m_pipeline);

    ElementPtr udp_src = Gst::ElementFactory::create_element("udpsrc");
    ASSERT(udp_src);
    udp_src->set_property("port", int(gst_port));

    std::string caps_str = fmt::format("application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96");
    INFO(caps_str);
    CapsPtr caps = Gst::Caps::create_from_string(caps_str);
    ASSERT(caps);

    ElementPtr caps_filter = Gst::ElementFactory::create_element("capsfilter");
    caps_filter->set_property("caps", caps);

    ElementPtr rtp_h264_depay = Gst::ElementFactory::create_element("rtph264depay");
    ASSERT(rtp_h264_depay);

    ElementPtr h264_dec = Gst::ElementFactory::create_element("avdec_h264");
    ASSERT(h264_dec);

    ElementPtr video_convert = Gst::ElementFactory::create_element("videoconvert");
    ASSERT(video_convert);

    caps_str = fmt::format("video/x-raw,format=(string)RGB");
    INFO(caps_str);
    caps = Gst::Caps::create_from_string(caps_str);
    ASSERT(caps);

    ElementPtr caps_filter2 = Gst::ElementFactory::create_element("capsfilter");
    caps_filter2->set_property("caps", caps);

//  m_display_sink = Gst::ElementFactory::create_element("fpsdisplaysink");
//  ASSERT(m_display_sink);

    m_app_sink = Gst::AppSink::create();
    ASSERT(m_app_sink);

    m_app_sink->property_drop().set_value(true);
    m_app_sink->property_emit_signals().set_value(true);
    m_app_sink->signal_new_sample().connect([this] (void) { return this->handle_new_frame(); });

    // Add all of the elements to the pipeline
    m_pipeline->add(udp_src)->add(caps_filter)->add(rtp_h264_depay)->add(h264_dec)->add(video_convert)->add(caps_filter2)->add(m_app_sink);
    // link all of the elements together
    udp_src->link(caps_filter)->link(rtp_h264_depay)->link(h264_dec)->link(video_convert)->link(caps_filter2)->link(m_app_sink); 

    m_bus = m_pipeline->get_bus();
    ASSERT(m_bus);  

//  m_bus->enable_sync_message_emission();
//  m_bus->signal_sync_message().connect([this](const MessagePtr& msg) { this->process_gst_message(msg); });
}

void Window::Impl::start_pipeline()
{
    Gst::StateChangeReturn cret = m_pipeline->set_state(Gst::STATE_PLAYING);
    ASSERT_THROW(cret != Gst::STATE_CHANGE_FAILURE, "Failed to set pipeline to playing state");
}

void Window::Impl::stop_pipeline()
{
    Gst::StateChangeReturn cret = m_pipeline->set_state(Gst::STATE_NULL);
    ASSERT_THROW(cret != Gst::STATE_CHANGE_FAILURE, "Failed to set pipeline to playing state");
}
//
//void Window::Impl::process_gst_message(const MessagePtr& msg)
//{
//    // Code from https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-libs/html/gst-plugins-base-libs-gstvideooverlay.html
//    // the C++ style code in videooverlay.h is not working right
//    if (!gst_is_video_overlay_prepare_window_handle_message(msg->gobj()))
//    {
//        return;
//    }
//
//    INFO("Called, msg = {}", GST_MESSAGE_SRC_NAME(msg->gobj()));
//
//    WId wid = m_parent->winId();
//    GstVideoOverlay* overlay = GST_VIDEO_OVERLAY (GST_MESSAGE_SRC (msg->gobj()));
//    gst_video_overlay_set_window_handle (overlay, wid);
//}

Gst::FlowReturn Window::Impl::handle_new_frame()
{
    if (m_paused || !m_parent->isVisible())
    {
        return Gst::FLOW_OK;
    }

    if (m_app_sink->property_eos()) 
    {
        ERROR("Reached the end of the stream! Shouldn't be here!!");
        return Gst::FLOW_EOS;
    }

    using SamplePtr = Glib::RefPtr<Gst::Sample>;
    SamplePtr sample = m_app_sink->pull_sample();
    ASSERT(sample);
//  INFO("0x{}", (void*)sample->gobj());

    // Check how fast the data and pipeline is operating
    m_pipeline_counter.update();

    // Skip every other frame
    static size_t frame_num = 0;
    frame_num++;
    if (frame_num %2 == 0) 
    {
        return Gst::FLOW_OK;
    }

    CapsPtr caps = sample->get_caps();
    // NOTE: for gstreamermm 1.4.3, this reference MUST be here. get_caps() doesn't reference properly!
    caps->reference(); 
    ASSERT(caps);

    const Gst::Structure caps_struct = caps->get_structure(0);

//  Glib::ustring str = caps_struct.to_string();
//  INFO(str);
    
// #ifdef DEBUG
    // Sanity check
    int width, height;
    caps_struct.get_field("width", width);
    caps_struct.get_field("height", height);
    ASSERT_EQUALS(width, m_width);
    ASSERT_EQUALS(height, m_height);
    Glib::ustring format;
    caps_struct.get_field("format", format);
    ASSERT(format.compare("RGB") == 0);
// #endif

    using BufferPtr = Glib::RefPtr<Gst::Buffer>;
    BufferPtr buffer = sample->get_buffer();
    ASSERT(buffer);

    {
        using MapInfoPtr = Glib::RefPtr<Gst::MapInfo>;
        MapInfoPtr map_info(new Gst::MapInfo());
        bool ret = buffer->map(map_info, Gst::MAP_READ);
        ASSERT(ret);

        ASSERT_EQUALS(map_info->get_size(), m_width*m_height*sizeof(RGB888Pixel));

        if (!m_processor) 
        {
            m_processor.reset(new ImageProcessor(width,height));
        }

        QImage out_image = m_processor->process(map_info->get_data());
        m_color_pixmap = QPixmap::fromImage(out_image);

        buffer->unmap(map_info);
    }
    // NOTE: ONLY update() maybe called here. NOT repaint()!
    // Because process_frame will be called from a different thread!!
    m_parent->update();

    m_display_counter.update();

    return Gst::FLOW_OK;
}

ImageProcessor::ImageProcessor(int width, int height)
: m_width(width)
, m_height(height)
, m_thresh(100)
, cv_image(m_height, m_width, CV_8UC3)
, cv_gray(m_height, m_width, CV_8UC1)
, canny_output(m_height, m_width, CV_8UC1)
, drawing(m_height, m_width, CV_8UC3)
{

}

QImage ImageProcessor::process(uint8_t* src)
{
    // return QImage((uint8_t*)m_color_texmap, m_width, m_height, m_width*sizeof(RGB888Pixel), QImage::Format_RGB888);

    memcpy(cv_image.data, src, m_width*m_height*sizeof(RGB888Pixel));

    cv::cvtColor(cv_image, cv_gray, CV_RGB2GRAY);
    cv::blur(cv_gray, cv_gray, cv::Size(3,3));

    /// Detect edges using canny
    Canny(cv_gray, canny_output, m_thresh, m_thresh * 2, 3); 
    /// Find contours
    findContours(canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0)); 

    /// Draw contours
    drawing = cv::Scalar(0, 0, 0);
    for (int i = 0; i < contours.size(); i++) 
    {
        cv::Scalar color = cv::Scalar(255, 255, 255); //rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255)); 
        cv::drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, cv::Point());
    }
    cv::cvtColor(drawing, cv_image, CV_BGR2RGB);

    return QImage((uint8_t*)cv_image.data, cv_image.cols, cv_image.rows, cv_image.step1(), QImage::Format_RGB888);
}


}; // namespace holo3d


