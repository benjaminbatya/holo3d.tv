#include <ctime>
#include <string>
#include <memory>
#include <algorithm>

#include "util_funcs.hpp"

#include "server.hpp"

#include "holo3d.pb.h"

#include "convert_funcs.hpp"

#include "frame.hpp"

namespace hs = holo3d::socket;

namespace holo3d
{

//const int SAVE_INTERVAL = 33;

static const size_t MAX_BUF_SIZE = 0xfffff;

Server::Server(const uint16_t local_port, const uint16_t remote_port) 
: m_local_port(local_port)
, m_remote_port(remote_port)
{
    // Spin up the receive thread
    m_thread = std::thread([&] { this->receive(); });

}

Server::~Server()
{
    m_is_running = false;
    if (m_thread.joinable()) 
    {
        m_thread.join(); 
    }
}

size_t Server::register_new_camera_cb(NewCameraCB func)
{
    m_new_camera_cb_vec.push_back(func);
    return m_new_camera_cb_vec.size()-1;
}

bool Server::remove_new_camera_cb(size_t idx)
{
    m_new_camera_cb_vec.erase(m_new_camera_cb_vec.begin() + idx);
    return true;
}

size_t Server::register_new_frame_cb(const std::string& addr, NewFrameCB func)
{
//  INFO("Called with address={}", addr);
    m_new_frame_cb_map[addr].push_back(func);
    return m_new_frame_cb_map[addr].size()-1;
}

bool Server::remove_new_frame_cb(const std::string& addr, size_t idx)
{
//  INFO("Called with address={}", addr);
    auto it = m_new_frame_cb_map.find(addr);
    ASSERT(it != m_new_frame_cb_map.end());

    NewFrameCBVec& vec = it->second;
    vec.erase(vec.begin() + idx);

    return true;
}

void Server::receive()
{
    hs::endpoint ep(0, m_local_port);
    hs::udp_receiver receive_socket;
    if(!receive_socket.connect(ep))
    {
        return;
    }

    INFO("Listening on {}", ep);

    static const size_t MAX_RECEIVE_BUF_SIZE = 0xfffff;

    uint8_t buf[MAX_RECEIVE_BUF_SIZE];
    hs::endpoint remote_endpoint;

    m_is_running = true;

    while (is_running())
    {
        size_t bytes_recved = receive_socket.receive(buf, MAX_RECEIVE_BUF_SIZE, &remote_endpoint, 10);
        if (bytes_recved == 0) 
        {
            continue;
        }
//      INFO("received {} bytes = from {}", bytes_recved, remote_endpoint);

        std::string cam_address = remote_endpoint.str_addr();

        // decode the buf into a message and place it into the message queue
        MessageHeader msg_header;
        uint8_t* ptr = msg_header.deserialize(buf, bytes_recved);
        ASSERT((void*)ptr);

        ASSERT(sizeof(MessageHeader) + msg_header.msg_size + msg_header.data_size == bytes_recved);

        switch (msg_header.type())
        {
        case protocol::QUERY_ACK:
            receive_query_ack(ptr, msg_header.msg_size, remote_endpoint);
            break;

        case protocol::STREAM_ACK:
        {
            INFO("received stream ack msg from {}", remote_endpoint);
            protocol::StreamAck msg;
            bool flag = msg.ParseFromArray(ptr, msg_header.msg_size);
            ASSERT_THROW_ALWAYS(flag, "flag = {}", flag);
            
            m_headers[cam_address] = msg.header();

            // Tell the listeners that a new video stream is coming
//          m_new_camera_sig.emit(cam_address);
            for (auto& func : m_new_camera_cb_vec)
            {
                func(cam_address);
            }
        } break;
        
        default:
            // Ignore unrecognized messages
            WARN("Unrecognized message {}", msg_header.type());
            break;
        }
    }
}

void Server::receive_query_ack(uint8_t* buf, size_t bytes_recved, const hs::endpoint& remote_endpoint)
{
    protocol::QueryAck msg;
    bool flag = msg.ParseFromArray(buf, bytes_recved);
    ASSERT_ALWAYS(flag);
    INFO("received query ack msg from {}: listener_type={}", remote_endpoint, QueryAck_ListenerType_Name(msg.listener_type()) );

//          timeval diff_time { int32_t(msg.diff().seconds()), int32_t(msg.diff().micro_seconds()) };
//
//          double time_diff = double(diff_time.tv_sec*MICROS_PER_SEC + diff_time.tv_usec)/MICROS_PER_SEC;
//          INFO("Time difference = {}", time_diff);

    m_cameras.push_back(remote_endpoint);
}

void Server::start_recording(const Context& context)
{
    // Copy the context
    m_context = context;

    // Send START_STREAMING messages to each of the cameras listed in context
    protocol::StreamStart msg;

    protocol::Header* header = msg.mutable_header();
    uint64_t magic = *(uint64_t*)(FILE_MAGIC);
    header->set_magic(magic);
    header->set_major_number(MAJOR_VERSION);
    header->set_minor_number(MINOR_VERSION);
    header->set_format(convert_format_l2p(context.camera_config.format) );

    switch (context.camera_config.resolution) 
    {
    case RESOLUTION::SMALL: header->set_width(320); header->set_height(200); break;
    case RESOLUTION::MEDUIM: header->set_width(640); header->set_height(480); break;
    case RESOLUTION::LARGE: header->set_width(1024); header->set_height(768); break;
    default: ASSERT(false); break;
    }

    header->set_quality(convert_quality_l2p(context.camera_config.quality));
    header->set_fps(context.camera_config.fps);
    
    uint8_t buf[MAX_BUF_SIZE];

    for (int i=0; i<m_context.cameras_to_run.size(); i++)
    {
        const std::string& cam_address = m_context.cameras_to_run[i];
        uint16_t port = m_context.gst_ports[i];

        m_ports[cam_address] = port;

        msg.set_port(port);

        size_t msg_size = serialize_msg(buf, MAX_BUF_SIZE, protocol::STREAM_START, &msg);
        ASSERT(msg_size > 0);

        socket::endpoint ep(cam_address, m_remote_port);

        INFO("Sending STREAM_START message to {}", ep);
        m_sender.send_to(buf, msg_size, ep);
    }

    m_is_recording = true;
}

//void Server::stop_recording()
//{
//    for (auto& cam_address : m_context.cameras_to_run)
//    {
//        stop_recording(cam_address);
//    }
//
//    m_is_recording = false;
//}

void Server::stop_recording(const std::string& address)
{
    auto it = std::find(m_context.cameras_to_run.begin(), m_context.cameras_to_run.end(), address);
    if (it == m_context.cameras_to_run.end()) 
    {
        ERROR("No camera at address {} found", address);
        return;
    }

    // Send STOP_STREAMING messages to each of the cameras listed in context
    static uint8_t buf[MAX_BUF_SIZE];
    size_t msg_size = serialize_msg(buf, MAX_BUF_SIZE, protocol::STREAM_STOP);

    socket::endpoint ep(address, m_remote_port);
    INFO("Sending STREAM_STOP message to {}", ep);
    m_sender.send_to(buf, msg_size, ep);

    m_context.cameras_to_run.erase(it);

    if (m_context.cameras_to_run.size() == 0) 
    {
        m_is_recording = false;
        INFO("No more cameras are running!");
    }
}

/**
 * Finds cameras from a list of possible cameras
 *
 * @author benjamin (11/20/2014)
 */
Server::string_vec_t Server::find_cameras()
{
    m_cameras.clear();
    string_vec_t cameras = { };
    
    // Send a Query message
    protocol::Query msg;

    //      INFO("Msg type = {}", MESSAGE_TYPE_Name(msg.type()));

    msg.set_sender_port(m_local_port);

    protocol::TimeStamp* ts = msg.mutable_sender_time_stamp();

    // Set the time
    timeval tmp_time;
    get_accurate_time(&tmp_time);
    ts->set_seconds(tmp_time.tv_sec);
    ts->set_micro_seconds(tmp_time.tv_usec);

    uint8_t buf[MAX_BUF_SIZE];
    size_t msg_size = serialize_msg(buf, MAX_BUF_SIZE, protocol::QUERY, &msg);

    hs::endpoint ep("192.168.10.0", m_remote_port);

    INFO("broadcasting {} bytes to {}", msg_size, ep);

    bool ret = m_sender.connect(ep, true);
    if (!ret) 
    {
        return cameras;
    }

    m_sender.send(buf, msg_size); 

    // Wait for a second for all of the cameras in the local network to respond
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    for (int i=0; i<m_cameras.size(); i++) 
    {
        hs::endpoint ep = m_cameras[i];
        cameras.push_back(ep.str_addr());
    }

    return cameras;
}

const protocol::Header& Server::header(const std::string& address)
{
    auto it = m_headers.find(address);
    ASSERT(it != m_headers.end());
    return it->second;
}

uint16_t Server::port(const std::string& address)
{
    auto it = m_ports.find(address);
    ASSERT(it != m_ports.end());
    return it->second;
}

}; // namespace holo3d
