// Main.cpp for HoloCamera


#include <thread>

#include <sys/time.h>

#include <tclap/CmdLine.h>

#include <google/protobuf/stubs/common.h>

// NOTE: gstreamer functionality has to be included before Qt functionality
// See http://stackoverflow.com/questions/18424425/gstreamermm-and-qt-compilation-error
#include "gst_funcs.hpp"

#include <Qt/QtGui>

#include "util_funcs.hpp"

#include "controller.hpp"

#include "h3t_file.hpp"

//namespace holo3d {
//void handle_send(uint16_t port, const hs::endpoint& ep);
//void handle_receive(uint16_t port);
//};

#include "server.hpp"

class MyApp : public QApplication
{
public:
    MyApp(int& argc, char** argv)
    : QApplication(argc, argv)
    {
    }

    virtual bool notify(QObject *receiver, QEvent *event)
    {
        try
        { 
            return QApplication::notify(receiver, event);
        }
        catch (std::exception& e) 
        {
            ERROR("Caught Exception: {}", e.what());
            return false;
        }
    }
};


int main(int argc, char** argv)
{    
    {
        // Print the start message
        time_t raw_time;
        time(&raw_time);
        INFO("Started {} on {}", argv[0], ctime(&raw_time));
    }

    Gst::init(argc, argv);

    GOOGLE_PROTOBUF_VERIFY_VERSION;

    int ret = 0;

    try
    { 
        TCLAP::CmdLine cmd(argv[0], ' ', "0.1");

        TCLAP::ValueArg<uint16_t> local_port_arg("l", "local_port", "", false, 
                                                 12345,
                                                 // holo3d::default_port_num(), 
                                                 "The local port number to use");
        cmd.add(local_port_arg);

        TCLAP::ValueArg<uint16_t> port_arg("p", "port", "", false, holo3d::default_port_num(), "The port the remote devices are listening on");
        cmd.add(port_arg);

        cmd.parse(argc,argv);

        holo3d::Server server(local_port_arg.getValue(), port_arg.getValue());

        MyApp app(argc, argv);

        holo3d::Controller controller(&server);

        controller.show();

        ret = app.exec();

    } catch (TCLAP::ArgException& e) 
    {
        ERROR("\nTCLAP exception: {}", e.what());
    } catch (std::runtime_error& e)
    {
        ERROR("\nCaught runtime_error: {}", e.what());
        ret = -1;
    } catch (std::exception& e)
    {
        ERROR("\nCaught exception: {}", e.what());
        ret = -1;

    } 
    catch(...)
    {
        ERROR("\nUnknown exception!!");
        ret = -1;
    }

    // Do final shutdown steps
    google::protobuf::ShutdownProtobufLibrary();

    return ret;
}
//
//
//static const size_t MAX_BUF_SIZE = 65536;
//
//void handle_send(uint16_t local_port, const hs::endpoint& ep)
//{
//    socket::udp_sender sender;
//    sender.connect(ep);
//
//    uint8_t buf[MAX_BUF_SIZE];
//
//    {
//        // Send a Query message
//        protocol::Query msg;
//
//        // NOTE: This has to be done to force the type to be sent, all of the other fields are optional
//        msg.set_type(msg.type());
//
////      INFO("Msg type = {}", MESSAGE_TYPE_Name(msg.type()));
//
//        msg.set_sender_port(local_port);
//
//        protocol::TimeStamp* ts = msg.mutable_sender_time_stamp();
//
//        // Set the time
//        timeval current_time;
//        get_accurate_time(&current_time);
//        ts->set_seconds(current_time.tv_sec);
//        ts->set_micro_seconds(current_time.tv_usec);
//
//        int msg_size = msg.ByteSize();
//        bool res = msg.SerializeToArray(buf, msg_size);
//        ASSERT_THROW_ALWAYS(res, "Failed to serialize QUERY message!");
//
//        INFO("broadcasting {} bytes to {}", msg_size, ep);
//        sender.send_to(buf, msg_size, ep);
//    }
//
////      INFO("Broadcast msg");
//    // Wait two seconds for replies before going to the next task
//    std::this_thread::sleep_for(std::chrono::seconds(2));
//    {
//        // Send a stream start to all of the cameras
//        INFO("Sending STREAM_START message to all cameras");
//        protocol::Base msg;
//        msg.set_type(protocol::MESSAGE_TYPE::STREAM_START);
//
//        int msg_size = msg.ByteSize();
//        bool res = msg.SerializeToArray(buf, msg_size);
//        ASSERT_THROW_ALWAYS(res, "Failed to serialize STREAM_START message!");
//
//        sender.send_to(buf, msg_size, ep);
//    }
//
//    while(true)
//    {
////          // Do intrinsic calibration on each camera
////          for (auto& camera_endpoint : g_cameras)
////          {
////              protocol::Base msg;
////              msg.set_type(protocol::MESSAGE_TYPE::INTRINSIC_CALIBRATION_START);
////
////              sender.send(camera_endpoint.second, msg);
////          }
//
//
//        std::this_thread::sleep_for(std::chrono::seconds(1));
//    }
//}
//
//void handle_receive(uint16_t port)
//{
//    hs::endpoint ep(0, port);
//    hs::udp_receiver receive_socket;
//    receive_socket.connect(ep);
//
//    uint8_t buf[MAX_BUF_SIZE];
//    hs::endpoint remote_endpoint;
//
//    while (true)
//    {
//        size_t bytes_recved = receive_socket.receive_from(buf, MAX_BUF_SIZE, remote_endpoint);
////      INFO("received {} bytes = from {}", bytes_recved, remote_endpoint);
//
//        // decode the buf into a message and place it into the message queue
//        protocol::Base base_msg;
//        bool flag = base_msg.ParseFromArray(buf, bytes_recved);
//        ASSERT_ALWAYS(flag);
//
//        switch (base_msg.type())
//        {
//        case protocol::MESSAGE_TYPE::QUERY_ACK:
//        {
//            protocol::QueryAck msg;
//            flag = msg.ParseFromArray(buf, bytes_recved);
//            ASSERT_ALWAYS(flag);
//            INFO("received query ack msg from {}: listener_type={}", remote_endpoint, QueryAck_ListenerType_Name(msg.listener_type()) );
//
//            timeval diff_time { int32_t(msg.diff().seconds()), int32_t(msg.diff().micro_seconds()) };
//
//            double time_diff = double(diff_time.tv_sec*MICROS_PER_SEC + diff_time.tv_usec)/MICROS_PER_SEC;
//            INFO("Time difference = {}", time_diff);
//
//            break;
//        }
//
//        default:
//            // Ignore unrecognized messages
////          WARN("Unrecognized message {}", base.type());
//            break;
//        }
//
//    }
//}
//
//}; // namespace holo3d

