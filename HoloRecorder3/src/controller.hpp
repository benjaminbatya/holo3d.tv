#pragma once

#include <Qt/QtGui>

#include "ui_controller.h"

namespace holo3d
{

class Window;
class Server;
class file_saver;
//class QCloseEvent;

/**
 * This is a Qt Controller for HoloRecorder
 * 
 * @author benjamin (3/3/2015)
 * @param  
 */
class Controller : public QMainWindow, private Ui_Controller
{
    Q_OBJECT
public:
    Controller(Server* server, QWidget* parent=0);
    ~Controller();

    void show();
     
protected slots:
    void build_window(QString camera_address);
    void window_closed(const QString& window);
    void toggle_recording();

    void quit();
    void load_settings(bool use_last = false);
    void save_settings(bool use_last = false);
    void rescan_cameras();

protected:

    void closeEvent(QCloseEvent* event);

    void populate_controls();

    void start_recording();
    void stop_recording();

    bool eventFilter(QObject* obj, QEvent* event);

    Server* m_server = nullptr;
        
    size_t m_sig_id;

    using WindowVec = std::vector<Window*>;
    WindowVec    m_windows;

    QSignalMapper* m_signal_mapper = nullptr;
};

}; // namespace holo3d

