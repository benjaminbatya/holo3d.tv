#include "pch_files.hpp"

#include <string>
#include <iostream>
#include <cmath>

#include <stdio.h>

namespace po = boost::program_options;

#include "h3t_file.hpp"
#include "fps_counter.hpp"
#include "encoder.hpp"

#include "util_funcs.hpp"

#include "frame.hpp"

#include "openni_factory.hpp"
#include "v4l_factory.hpp"

// These Enums only are used in main
#include <option_enums.hpp>

using namespace holo3d;

// start shared globals

static const int        MAX_QUEUE_SIZE = 128;
static                  boost::lockfree::spsc_queue<camera_frame, boost::lockfree::capacity<MAX_QUEUE_SIZE> > g_queue;

static                  boost::atomic_bool g_done(false);

struct InitData
{
    RECT rect;
    uint16_t tracker_id;
};

struct file_info_t
{
    std::string file_path;
    InitData init_data;
    STREAM_HEADER header;
    FILE* fd;
};

std::vector<file_info_t>   g_file_infos;

// Change this to vary the number of regions to record. Later, make this an option...
std::vector<InitData> g_regions = 
{
    { {0,   0,   200, 100}, 101},
    { {100, 100, 200, 200}, 201},
    { {300, 100, 300, 100}, 301},
    { {100, 100, 300, 200}, 401},
    { {100, 100, 300, 300}, 501}
};

bool                    g_use_regions = false;

struct VelocityData
{
    int16_t v_x;
    int16_t v_y;
};

std::vector<VelocityData> g_velocities =
{
    {2, 2},
    {1, -2},
    {-1, -1},
    {1, 1},
    {-2, 2}
    
};

// end shared globals

// start producer globals

static fps_counter      g_producer_fps("PRODUCER");
static size_t           g_producer_count = 0;

static size_t           g_num_frames_to_capture = 0;
static size_t           g_producer_fps_limit = 30;
// end producer globals


// start consumer globals

encoder::color_encoder_t g_color_encoder;
encoder::depth_encoder_t g_depth_encoder;

uint8_t* g_color_buffer = nullptr;
uint8_t* g_depth_buffer = nullptr; 

static fps_counter  g_consumer_fps("CONSUMER");

static int          g_consumer_count = 0;


static factory_base* g_camera_factory = nullptr;

// end consumer globals

void producer_run()
{
    factory_base::device_ptr_t device = g_camera_factory->device(g_producer_fps_limit, g_color_encoder->width());
    if(device == nullptr)
    {
        return;
    }

    camera_frame current_frame;
    current_frame.file_header(g_file_infos[0].header);

    while (!wasKeyboardHit())
    {
        bool success = device->read_frame(current_frame);

        if (success)
        {
            // Set all of the dates
            static uint16_t g_num_frames = 0;
            current_frame.frame_number(g_num_frames++);
        
            if(!g_queue.push(current_frame))
            {
                ERROR("error pushing frame at %1, queue was full", current_frame.color_time_stamp());
            } else
            {
                // Only count the frame as successfully produced if it was added to the queue
                g_producer_count++;
            }

//          INFO("producer_run: sent frame %d: time_stamp=%ld.%ld",
//               current_frame.frame_number,
//               current_frame.time_stamp/MICROS_PER_SEC,
//               current_frame.time_stamp%MICROS_PER_SEC);

            // NOTE: Should this be updated always or only when current_frame is successfully added to the queue??
            g_producer_fps.update();           

            if (g_num_frames_to_capture>0 && g_num_frames_to_capture <= g_producer_count)
            {
                INFO("Captured %1% frames. Finished.", g_producer_count);
                return;
            }
        }

        current_frame.reset();
    }
}

void write_frame(FILE* file, camera_frame& current_frame, const RECT& rect)
{
    FRAME_HEADER header;

    header.frame_number = current_frame.frame_header().frame_number;
    header.color_time_stamp = current_frame.frame_header().color_time_stamp;
    header.depth_time_stamp = current_frame.frame_header().depth_time_stamp;
    header.rect = rect;

    size_t buffer_size = g_color_encoder->encode(g_color_buffer, current_frame.color_buffer(),
                                                 g_color_encoder->width() * g_color_encoder->height() * sizeof(RGB888Pixel),
                                                 current_frame.color_buffer_size(),
                                                 header.rect);
    header.color_buffer_size = buffer_size;

    buffer_size = g_depth_encoder->encode(g_depth_buffer, current_frame.depth_buffer(),
                                          g_depth_encoder->width() * g_depth_encoder->height() * sizeof(RGB888Pixel),
                                          current_frame.depth_buffer_size(),
                                          header.rect);
    header.depth_buffer_size = buffer_size;

    size_t ret = fwrite(&header, 1, sizeof(FRAME_HEADER), file);
    ASSERT_EQUAL(ret, sizeof(FRAME_HEADER));

    ret = fwrite(g_color_buffer, 1, header.color_buffer_size, file);
    ASSERT_EQUAL(ret, header.color_buffer_size);

    ret = fwrite(g_depth_buffer, 1, header.depth_buffer_size, file);
    ASSERT_EQUAL(ret, header.depth_buffer_size);
}

void write_frames()
{
    camera_frame current_frame;

    ASSERT_EQUAL(g_regions.size(), g_velocities.size());

    while (g_queue.pop(current_frame))
    {
        for (size_t i=0; i<g_file_infos.size(); i++) 
        {
            file_info_t& info = g_file_infos[i];
            write_frame( info.fd, current_frame, info.init_data.rect );       
            
            // Update the location of the regions
            if (g_use_regions) 
            {
                VelocityData& velo = g_velocities[i];

                int x = info.init_data.rect.x;
                int y = info.init_data.rect.y;

                int new_x = x + velo.v_x;
                int new_y = y + velo.v_y;

                if (new_x < 0) 
                {
                    velo.v_x *= -1;
                    new_x *= -1;
                } else if(new_x+info.init_data.rect.width >= current_frame.file_header().width)
                {
                    velo.v_x *= -1;
                    int dist = (new_x+info.init_data.rect.width) - current_frame.file_header().width;
                    new_x = current_frame.file_header().width - (dist + info.init_data.rect.width);
                }

                if (new_y < 0) 
                {
                    velo.v_y *= -1;
                    new_y *= -1;
                } else if(new_y+info.init_data.rect.height >= current_frame.file_header().height)
                {
                    velo.v_y *= -1;
                    int dist = (new_y+info.init_data.rect.height) - current_frame.file_header().height;
                    new_y = current_frame.file_header().height - (dist + info.init_data.rect.height);
                }

                info.init_data.rect.x = new_x;
                info.init_data.rect.y = new_y;
            }

        }
        
        g_consumer_count++;

        g_consumer_fps.update();

//      INFO("read_frames: processed frame %d: time_stamp=%d.%d",
//           current_frame.frame_number,
//           (uint32_t)current_frame.time_stamp/MICROS_PER_SEC,
//           (uint32_t)current_frame.time_stamp%MICROS_PER_SEC);

    }
}

void consumer_run(void)
{
    g_color_buffer = new uint8_t[g_color_encoder->width() * g_color_encoder->height() * sizeof(RGB888Pixel)];
    g_depth_buffer = new uint8_t[g_depth_encoder->width() * g_depth_encoder->height() * sizeof(RGB888Pixel)];

    for (auto& info : g_file_infos) 
    {
        FILE *file = fopen(info.file_path.c_str(), "wb");
        if (!file)
        {
            ERROR("consumer_run: failed to open '%s' for writing. exiting...", info.file_path);
            exit(-1);
        }

        size_t ret = fwrite(&info.header, 1, sizeof(STREAM_HEADER), file);
        ASSERT_EQUAL(ret, sizeof(STREAM_HEADER));

        info.fd = file;
    }

    while (!g_done)
    {
        write_frames();

        // Sleep for 2msec if no frames are available
        boost::this_thread::sleep(boost::posix_time::milliseconds(2));
    }

    // Clear out the queue
    write_frames();

    for (auto& info : g_file_infos) 
    {
        fclose(info.fd);
    }

    delete [] g_color_buffer;
    delete [] g_depth_buffer;
}

factory_base* select_camera_factory()
{
    factory_base* factory = new openni_factory();

    factory_base::uri_vec_t uris = factory->device_uris();
    if (uris.size() > 0) 
    {
        INFO("Selected OpenNI system!");
        return factory;
    } 

    delete factory;

    factory = new v4l_factory();

    uris = factory->device_uris();
    if (uris.size() > 0) 
    {
        INFO("Selected V4L system!");
        return factory;
    }

    delete factory;

    INFO("No suitable camera system found!");
    return nullptr;
}

int main(int argc, char **argv)
{    
    int ret = 0;

    try
    {
        // First detect the camera system
        g_camera_factory = select_camera_factory(); 
        ASSERT_THROW_ALWAYS(g_camera_factory, "No camera found!");

        RESOLUTION resolution   = RESOLUTION::MEDUIM;

        COLOR_MODE color_mode   = COLOR_MODE::JPEG;
        DEPTH_MODE depth_mode   = DEPTH_MODE::JPEG;
        QUALITY enc_quality     = QUALITY::GOOD;
        size_t fps              = 30; // The maximum fps to capture frames
        
        std::string appName = boost::filesystem::basename(argv[0]);
        std::string file_name;

        std::stringstream resolution_options;
        resolution_options << "\nResolution to record the video in. Allowed values:";
        for (int i = (int)RESOLUTION::MIN; i <= (int)RESOLUTION::MAX; i++)
        {
            resolution_options << " " << static_cast<RESOLUTION>(i);
        }

        std::stringstream color_mode_options;
        color_mode_options << "\nFormat to encode the color stream in. Allowed values:";
        for (int i = (int)COLOR_MODE::MIN; i <= (int)COLOR_MODE::MAX; i++)
        {
            color_mode_options << " " << static_cast<COLOR_MODE>(i);
        }

        std::stringstream depth_mode_options;
        depth_mode_options << "\nFormat to encode the depth stream in. Allowed values:";
        for (int i = (int)DEPTH_MODE::MIN; i <= (int)DEPTH_MODE::MAX; i++)
        {
            depth_mode_options << " " << static_cast<DEPTH_MODE>(i);
        }

        std::stringstream vpx_quality_options;
        depth_mode_options << "\nVPX quality to encode in. Only used if the format is VP8. Allowed values:";
        for (int i = (int)QUALITY::MIN; i <= (int)QUALITY::MAX; i++)
        {
            vpx_quality_options << " " << static_cast<QUALITY>(i);
        }

        po::options_description visible(std::string("Usage: ") + appName + " <options> movie.h3t\nAllowed Options");
        visible.add_options()
        ("help,h",                                                                                  "Display this help message")
        ("resolution,r",    po::value<RESOLUTION>(&resolution)->default_value(resolution),          resolution_options.str().c_str())
        ("color_mode,c",    po::value<COLOR_MODE>(&color_mode)->default_value(color_mode),          color_mode_options.str().c_str())
        ("depth_mode,d",    po::value<DEPTH_MODE>(&depth_mode)->default_value(depth_mode),          depth_mode_options.str().c_str())
        ("quality,q",       po::value<QUALITY>(&enc_quality)->default_value(enc_quality),           vpx_quality_options.str().c_str())
        ("number,n",        po::value<size_t>(&g_num_frames_to_capture)->default_value(g_num_frames_to_capture), "The number of frames to capture (value==0 means no limit)")
        ("fps,f",           po::value<size_t>(&fps)->default_value(fps),                            "Maximum fps to capture camera frames at. Range is 1-30fps")
        ("region,u",        "Indicates that regions should be used")
        ;

        po::options_description hidden(std::string("Hidden options for ") + appName);
        hidden.add_options()
        ("file", po::value<std::string>(&file_name)->required(),                          "The file to save the movie to")
        ;

        po::options_description all;
        all.add(visible).add(hidden);

        po::positional_options_description required;
        required.add("file", 1);


        try
        {
            po::variables_map vm;

            po::store(po::command_line_parser(argc, argv).options(all).positional(required).run(), vm);

            if (vm.count("help"))
            {
                INFO_CPP(visible);
                return 0;
            }

            po::notify(vm);

            if (vm.count("region")) 
            {
                g_use_regions = true;
            }
        }
        catch (po::required_option& e)
        {
            ERROR_CPP("No movie specified!!\n");
            INFO_CPP(visible);
            return -1;
        }
        catch (po::error& e)
        {
            ERROR_CPP(e.what());
            INFO_CPP(visible);
            return -1;
        }

        g_producer_fps_limit = (fps < 1 ? 1 : fps);

        uint16_t frame_width;
        switch (resolution)
        {
        case RESOLUTION::SMALL: frame_width = 320; break;
        case RESOLUTION::MEDUIM: frame_width = 640; break;
        // case RESOLUTION::LARGE: frame_width = 1024; break;
        default: THROW("INVALID resolution size %s", resolution); break;
        }
        uint16_t frame_height = (frame_width * 3 / 4);

        encoder::factory factory(frame_width, frame_height, enc_quality);

        g_color_encoder = factory.color((STREAM_FORMAT)color_mode, g_camera_factory->color_format()); 
        g_depth_encoder = factory.depth((STREAM_FORMAT)depth_mode, g_camera_factory->depth_format()); 

        // Setup one region which fills the entire window
        if (g_use_regions == false) 
        {
            g_regions.clear();
            InitData full_frame = { {0, 0, frame_width, frame_height}, 0};
            g_regions.push_back(full_frame);
            g_velocities.clear();
            g_velocities.push_back({0, 0});
        }

        boost::filesystem::path path(file_name);
        // Make sure the file is always saved in the current directory. 
        // OpenNI2 has the annoying habit of redirecting the file to its Drivers directory
        path = boost::filesystem::absolute(path);

        std::string file_list = "";
        for (size_t i=0; i<g_regions.size(); i++)
        {
            InitData init_data = g_regions[i];

            boost::filesystem::path temp_path = path;
            std::string ext = (boost::format("%1%.h3t") % init_data.tracker_id).str();
            std::string file_path = temp_path.replace_extension(ext).string();

            STREAM_HEADER header;
            header.color_format = g_color_encoder->output_format();
            header.depth_format = g_depth_encoder->output_format();

            header.width = frame_width;
            header.height = frame_height;

            // TODO: Grab the id from the IP address of this camera
            header.camera_id = 0;
            header.tracker_id = init_data.tracker_id;
            
            g_file_infos.push_back( {file_path, init_data, header} );

            file_list += file_path + ", ";
        }

        INFO("%1%: writing video stream to files '%2%" 
             "'\n\tResolution=%3%x%4%, Color Format=%5%, Depth Format=%6%",  
             argv[0], file_list, 
             frame_width, frame_height, 
             g_color_encoder->output_format(),
             g_depth_encoder->output_format());

        boost::thread producer_thread(producer_run);
        boost::thread consumer_thread(consumer_run);

        producer_thread.join();

        g_done = true;

        consumer_thread.join();

        INFO("produced %d frames.", g_producer_count);
        INFO("consumed %d frames.", g_consumer_count);

    } catch (std::exception &e) 
    {
        ERROR_CPP(std::endl << e.what());
        ret = -1;
    } catch(...)
    {
        ERROR_CPP(std::endl << "Unknown exception!!");
        ret = -1;
    }

    // Make sure the instance is deleted before shutting down
    delete g_camera_factory;
    g_camera_factory = nullptr;

    INFO("Done!");

    return ret;
}

