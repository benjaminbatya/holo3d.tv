#!/usr/bin/env python
import sys
import os.path
import os
import socket

def test_camera(cam_name):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connected = None
    try:
        s.settimeout(0.1)
        s.connect((cam_name, 22))
        connected = True
    except socket.error as e:
        print "Error connecting to %s: %s" % (cam_name, e)
        connected = False
        
    s.close()
    return connected
    

def main():
    config = 'Release'
    if len(sys.argv) > 1:
        config = sys.argv[1]
    # print '%s' % config
    if(config != 'Release' and config != 'Debug'):
        raise SyntaxError("Either 'Release' or 'Debug' must be specified!")

    # Use this to deploy HoloCamera to all attached cameras
    num_cameras = 8
    camera_prefix = 'camera'

    # Rebuild the project first
    num_cpus = int(os.popen('cat /proc/cpuinfo | grep  processor | wc -l').readline()[0])

    build_cmd = 'scons -j %s CFG=%s-Arm' % (num_cpus, config)
    print 'running "%s"' % (build_cmd)

    build_worked = os.system(build_cmd)

    if(build_worked != 0):
        return -1

    for i in range(num_cameras):
        # check to see if the camera is there
        camera = camera_prefix + str(i+1)
        
        ret = test_camera(camera)
        if not ret: continue

        # Use scp to transfer the files
        print "Transferring %s files to %s" % (config, camera)
        os.system("scp %s-Arm/CameraClient %s:" % (config, camera))
        os.system("scp $HOLO_ROOT/shared/%s-Arm/libshared.so %s:" % (config, camera))
    
if __name__ == "__main__":
    main()
