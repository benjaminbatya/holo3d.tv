// tracker_main.cpp for HoloProcess

#include <thread>

#include <sys/time.h>

#include <tclap/CmdLine.h>

#include "util_funcs.hpp"

#include "common_funcs.hpp"

#include "fragmenting_socket.hpp"

#include "h3t_file.hpp"

#include "src/holo3d.pb.h"

#include "convert_funcs.hpp"

namespace hs = holo3d::socket;

namespace holo3d
{

namespace control
{

// Shared state

void handle_send(uint16_t local_port, uint16_t remote_port, bool);
void handle_receive(uint16_t local_port, uint16_t remote_port);

int main(int argc, char** argv)
{    
    TCLAP::CmdLine cmd("Module options", ' ', "0.1");

    TCLAP::UnlabeledValueArg<std::string> module_arg("module", "", true, "", "The module to load.");
    cmd.add(module_arg);

    TCLAP::ValueArg<uint16_t> local_port_arg("l", "local_port", "", false, default_port_num(), "The local port to listen on");
    cmd.add(local_port_arg);

    TCLAP::ValueArg<uint16_t> port_arg("p", "port", "", false, default_port_num(), "The port the remote devices are listening on");
    cmd.add(port_arg);

    TCLAP::ValueArg<bool> ntp_arg("n", "ntp", "", false, false, "Indicates whether to do the ntp update or not");
    cmd.add(ntp_arg);

    cmd.parse(argc,argv);

    INFO("local_port = {}, remote_port = {}, ntp update = {}", 
         local_port_arg.getValue(), port_arg.getValue(), ntp_arg.getValue());

    // Spin up the various threads to run
    std::thread tx_thread(&handle_send, local_port_arg.getValue(), port_arg.getValue(), ntp_arg.getValue()); 

    std::thread rx_thread(&handle_receive, local_port_arg.getValue(), port_arg.getValue()); 

    tx_thread.join();
    rx_thread.join();

    return 0;
}

static const size_t MAX_BUF_SIZE = 65536;
static const char* BROADCAST_ADDR = "192.168.10.0";

class UDP_Broadcaster
{
public:
    UDP_Broadcaster(hs::endpoint ep)
    : m_ep(ep)
    {
        ensure_broadcast();
    }

    bool ensure_broadcast()
    {
        if (m_client.connected_endpoint() != m_ep) 
        {
            if(!m_client.connect(m_ep, true))
            {
                ERROR("Couldn't connect to {}", m_ep);
                return false;
            }
        }   
        return true;
    }

    void broadcast(protocol::MESSAGE_TYPE msg_type, const ::google::protobuf::Message* msg = nullptr)
    {
        if (!ensure_broadcast()) 
        {
            return;
        }

        size_t msg_size = serialize_msg(m_buf, MAX_BUF_SIZE, msg_type, msg);

        INFO("broadcasting {} bytes to {}", msg_size, m_ep);
        m_client.send(m_buf, msg_size);
    }

    void send_to(const hs::endpoint& remote, protocol::MESSAGE_TYPE msg_type, const ::google::protobuf::Message* msg = nullptr)
    {
        size_t msg_size = serialize_msg(m_buf, MAX_BUF_SIZE, msg_type, msg);

        INFO("sending {} bytes to {}", msg_size, remote);
        m_client.send_to(m_buf, msg_size, remote);
    }

protected:

    uint8_t m_buf[MAX_BUF_SIZE];

    hs::endpoint m_ep;

    hs::FragmentingSender m_client;
};

typedef std::map<uint32_t, hs::endpoint> CameraMap_t;
CameraMap_t g_cameras;

void handle_send(uint16_t local_port, uint16_t remote_port, bool send_sync_time)
{
    hs::endpoint remote_ep(BROADCAST_ADDR, remote_port);
    UDP_Broadcaster sender(remote_ep);

//  INFO("send_sync_time = {}", send_sync_time);

    if (send_sync_time) 
    {
        // Send a SYNC_TIME message
        sender.broadcast(protocol::SYNC_TIME);

        const int sleep_time = 7;
//      INFO("Sleeping for {} seconds", sleep_time);
        std::this_thread::sleep_for(std::chrono::seconds(sleep_time));
    }

    {
        // Send a Query message
        protocol::Query msg;

        msg.set_sender_port(local_port);

        protocol::TimeStamp* ts = msg.mutable_sender_time_stamp();

        // Set the time
        timeval current_time;
        get_accurate_time(&current_time);
        ts->set_seconds(current_time.tv_sec);
        ts->set_micro_seconds(current_time.tv_usec);

        sender.broadcast(protocol::QUERY, &msg);

//      INFO("Broadcast msg");
        // Wait two seconds for replies before going to the next task
        std::this_thread::sleep_for(std::chrono::seconds(2));

        // Send a stream start to all of the cameras
        INFO("Sending STREAM_START message to all cameras");
        for (auto& camera_endpoint : g_cameras) 
        {
            sender.send_to(camera_endpoint.second, protocol::STREAM_START);
        }
        sender.ensure_broadcast();
    }

    {
        while(true)
        {
//          // Do intrinsic calibration on each camera
//          for (auto& camera_endpoint : g_cameras)
//          {
//              protocol::Base msg;
//              msg.set_type(protocol::MESSAGE_TYPE::INTRINSIC_CALIBRATION_START);
//
//              sender.send(camera_endpoint.second, msg);
//          }

            
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }
}

void handle_receive(uint16_t local_port, uint16_t remote_port)
{
    hs::endpoint ep(0, local_port);
    hs::FragmentingReceiver receive_socket;
    receive_socket.connect(ep);

    uint8_t buf[MAX_BUF_SIZE];
    hs::endpoint remote_endpoint;

    while (true) 
    {
        size_t bytes_recved = receive_socket.receive(buf, MAX_BUF_SIZE, &remote_endpoint);
//      INFO("received {} bytes = from {}", bytes_recved, remote_endpoint);

        // decode the buf into a message and place it into the message queue
        MessageHeader msg_header;
        uint8_t* ptr = msg_header.deserialize(buf, bytes_recved);
        ASSERT(ptr);

        switch (msg_header.type())
        {
        case protocol::QUERY_ACK:
        {
            protocol::QueryAck msg;
            bool flag = msg.ParseFromArray(ptr, bytes_recved);
            ASSERT_ALWAYS(flag);
            INFO("received query ack msg from {}: listener_type={}", remote_endpoint, QueryAck_ListenerType_Name(msg.listener_type()) );

            timeval diff_time { int32_t(msg.diff().seconds()), int32_t(msg.diff().micro_seconds()) };

            double time_diff = double(diff_time.tv_sec*MICROS_PER_SEC + diff_time.tv_usec)/MICROS_PER_SEC;
            INFO("Time difference = {}", time_diff);

            // change remote_endpoint's port to the standard port
            hs::endpoint ep(remote_endpoint.addr(), remote_port);

            g_cameras[ep.addr()] = ep;

            break;
        }

        default:
            // Ignore unrecognized messages
            WARN("Unrecognized message {}", msg_header.type());
            break;
        }

    }
}


}; // namespace control

}; // namespace holo3d
