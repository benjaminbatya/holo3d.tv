// Main.cpp for GstProcess

#include <cmath>

#include <tclap/CmdLine.h>

#include "util_funcs.hpp"

#include <gst_funcs.hpp>

namespace holo3d
{

namespace camera
{

static const uint16_t GSTREAMER_PORT = 5000;
static const uint16_t NUM_ROIS = 4;

void run_roi_streamer(uint16_t port, uint16_t num_rois);

int main(int argc, char** argv)
{    
    Gst::init(argc, argv);

    TCLAP::CmdLine cmd("Camera Module options", ' ', "0.1", true);

    TCLAP::UnlabeledValueArg<std::string> module_arg("module", "", true, "", "The module to load.");
    cmd.add(module_arg);

    TCLAP::ValueArg<uint16_t> port_arg("p", "port", "", false, GSTREAMER_PORT, "The port to use for gstreamer data");
    cmd.add(port_arg);

    TCLAP::ValueArg<uint16_t> roi_arg("n", "num_rois", "", false, NUM_ROIS, "The number of ROIs to use");
    cmd.add(roi_arg);

    cmd.parse(argc, argv);

    INFO_SHORT("port = {}", port_arg.getValue());
    INFO_SHORT("num rois = {}", roi_arg.getValue());

    run_roi_streamer(port_arg.getValue(), roi_arg.getValue());

    return 0;
};

#ifdef MODEL_RPI2
const int WIDTH = 1280;
const int HEIGHT = 720;
const int FPS = 49;

#else

const int WIDTH = 640;
const int HEIGHT = 480;
const int FPS = 30;

#endif 

// NOTE: These should be powers of two to avoid videocrop glitches!
// Later, replace videocrop with another element which is more flexible.
const int ROI_WIDTH = 256;
const int ROI_HEIGHT = 256;

void create_roi_streamer(PipelinePtr& pipeline, ElementPtr& last_elem, uint16_t port, int left, int top);

void run_roi_streamer(uint16_t port, uint16_t num_rois)
{
    ASSERT_THROW_ALWAYS(num_rois > 0, "Number of ROIs has to be greater then 0!");

    // Create a pipeline and add the elements to it
    PipelinePtr pipeline = Gst::Pipeline::create();
    ASSERT(pipeline);

    ElementPtr v4l2src = Gst::ElementFactory::create_element("v4l2src");
    ASSERT(v4l2src);

    v4l2src->set_property("device", std::string("/dev/video0"));
    v4l2src->set_property("do-timestamp", true);

    pipeline->add(v4l2src);

    std::string caps_str = fmt::format("video/x-raw,width={},height={},framerate={}/1", WIDTH, HEIGHT, FPS);
    INFO(caps_str);
    CapsPtr caps = Gst::Caps::create_from_string(caps_str);
    ASSERT(caps);

    ElementPtr caps_filter = Gst::ElementFactory::create_element("capsfilter");
    ASSERT(caps_filter);
    caps_filter->set_property("caps", caps);

    pipeline->add(caps_filter);

    // Link the elements together
    v4l2src->link(caps_filter);

    ElementPtr last_elem = caps_filter;
#if defined MODEL_RPI2
    ElementPtr video_flip = Gst::ElementFactory::create_element("videoflip");
    ASSERT(video_flip);
    video_flip->set_property("method", 5);

    pipeline->add(video_flip);

    caps_filter->link(video_flip);
    last_elem = video_flip;
#endif

    ElementPtr tee = Gst::ElementFactory::create_element("tee");
    ASSERT(tee);
    pipeline->add(tee);
    last_elem->link(tee);

//  int num_roi_x = int(std::ceil(std::sqrt(num_rois)));
//  int num_roi_y = ((num_rois-1)/num_roi_x) + 1;
//  int x_offset = (WIDTH - ROI_WIDTH) / num_roi_x;
//  int y_offset = (HEIGHT - ROI_HEIGHT) / num_roi_y;

    int x_offset = ROI_WIDTH;
    int y_offset = ROI_HEIGHT;

//  int x_offset = 0;
//  int y_offset = 0;

    int left = 0;
    int top = 0;

    for (int i=0; i<int(num_rois); i++, port++)
    {
        create_roi_streamer(pipeline, tee, port, left, top); 

        left += x_offset;
        if (left + ROI_WIDTH >= WIDTH) 
        {
            left = 0;
            top += y_offset;
        }
    }
    
    Gst::StateChangeReturn cret = pipeline->set_state(Gst::STATE_PLAYING);
    ASSERT_THROW(cret != Gst::STATE_CHANGE_FAILURE, "Failed to set pipeline to playing state");

    BusPtr bus = pipeline->get_bus();
    ASSERT(bus);

    bool running = true;
    while (running) 
    {
        MessagePtr msg = bus->pop(Gst::CLOCK_TIME_NONE,
                                  Gst::MESSAGE_STATE_CHANGED | Gst::MESSAGE_ERROR | Gst::MESSAGE_EOS);
        if (!msg) { continue; }

        switch (msg->get_message_type()) 
        {
        case Gst::MESSAGE_ERROR:
        {
            ErrorMsgPtr err = ErrorMsgPtr::cast_static(msg);
            Glib::Error error;
            std::string debug_info;
            err->parse(error, debug_info);
            ERROR("Error received from element '{}': {}", err->get_source()->get_name(), error.what());
            ERROR("Debugging information: {}", debug_info);
            running = false;
        } break;
        case Gst::MESSAGE_EOS:
            INFO("End of stream reached");
            running = false;
            break;
        case Gst::MESSAGE_STATE_CHANGED:
            if (msg->get_source() == pipeline) 
            {
                MsgStateChgPtr chgmsg = MsgStateChgPtr::cast_static(msg);
                Gst::State old_state = chgmsg->parse_old();
                Gst::State new_state = chgmsg->parse();
                INFO("Pipeline changed from {} to {}", old_state, new_state);

//                  GstBin* bin = GST_BIN(pipeline->gobj());
//                  char buf[42];
//                  snprintf(buf, 43, "graph_%d", new_state);
//                  GST_DEBUG_BIN_TO_DOT_FILE(bin, GST_DEBUG_GRAPH_SHOW_ALL, buf);
            }
            break;
        default:
            ERROR("Unexpected message received");
            break;
        }
    }

    pipeline->set_state(Gst::STATE_NULL);
}

void create_roi_streamer(PipelinePtr& pipeline, ElementPtr& last_elem, uint16_t port, int left, int top)
{
    // For now, skip the tee and create one ROI
    ElementPtr queue = Gst::ElementFactory::create_element("queue");
    ASSERT(queue);

    ElementPtr video_crop = Gst::ElementFactory::create_element("videocrop");
    ASSERT(video_crop);

    int right = WIDTH - ROI_WIDTH - left;
    int bottom = HEIGHT - ROI_HEIGHT - top;

    video_crop->set_property("left", left);
    video_crop->set_property("right", right);
    video_crop->set_property("top", top);
    video_crop->set_property("bottom", bottom);

#if defined MODEL_RPI2
    ElementPtr h264_encoder = Gst::ElementFactory::create_element("omxh264enc");
    ASSERT(h264_encoder);
#else
    ElementPtr h264_encoder = Gst::ElementFactory::create_element("x264enc");
    ASSERT(h264_encoder);
    // NOTE: we are doing ultrafast for now to decrease the encoding load on the CPU
    h264_encoder->set_property("speed-preset", 1);
#endif // defined MODEL_RPI

    ElementPtr rtp_h264_pay = Gst::ElementFactory::create_element("rtph264pay");
    ASSERT(rtp_h264_pay);
    rtp_h264_pay->set_property("config-interval", 1);
    rtp_h264_pay->set_property("pt", 96);

    ElementPtr udp_sink = Gst::ElementFactory::create_element("udpsink");
    ASSERT(udp_sink);
    udp_sink->set_property("host", Glib::ustring("192.168.10.1"));
    udp_sink->set_property("port", int(port));
    udp_sink->set_property("sync", false);

    pipeline->add(queue)->add(video_crop)->add(h264_encoder);
    pipeline->add(rtp_h264_pay)->add(udp_sink);

    last_elem->link(queue)->link(video_crop)->link(h264_encoder)->link(rtp_h264_pay)->link(udp_sink);

}

}; // namespace camera

}; // namespace holo3d
