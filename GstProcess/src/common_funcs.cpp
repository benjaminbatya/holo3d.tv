#include "common_funcs.hpp"

#include "util_funcs.hpp"

#include "gst_funcs.hpp"

#include <cstdlib>

namespace holo3d
{

int exec_sudo(std::string cmd)
{
    cmd = std::string("sudo ") + cmd;

    INFO("Running '{}'", cmd);
    return system(cmd.c_str());
}

// Hopefully, this will get this processor's clock within 1ms of the server's clock which should be accurate enough
// for the entire system to work properly.
void do_time_sync()
{
    // First run ntpdate if desired
#if not defined PLATFORM_Arm
    ERROR("Platform is not ARM, not running time sync!");
    return;
#endif // not defined PLATFORM_Arm

    int result = 0;

    result = exec_sudo("ntpdate -b server");
    ASSERT_EQUAL_ALWAYS(result, 0);

}

}; // namespace holo3d
