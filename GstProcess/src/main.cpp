// Main.cpp for HoloCamera

#include <stdio.h>
#include <tclap/CmdLine.h>
#include <functional>
#include <map>

#include <google/protobuf/stubs/common.h>

#include "util_funcs.hpp"

#include "camera/camera_main.hpp"
//#include "control/control_main.hpp"
//#include "pose/pose_main.hpp"
#include "recorder/recorder_main.hpp"
//#include "tracker/tracker_main.hpp"


enum class MODULE : uint8_t
{
    NONE
    , CAMERA
    , CONTROL
//  , POSE
    , RECORDER
    , TRACKER

    , MIN = CAMERA
    , MAX = TRACKER

    , COUNT = MAX+1
};

std::ostream& operator<<(std::ostream& os, const MODULE& m)
{
#define MODULE(e) case MODULE::e : os << #e; break;

    switch (m)
    {
    MODULE(NONE);
    MODULE(CAMERA);
    MODULE(CONTROL);
    //  MODULE(POSE);
    MODULE(RECORDER)
    MODULE(TRACKER);
    default: os << "Unknown module " << (int)m; break;
    }
#undef MODULE

    return os;
}

std::istream& operator>>(std::istream& is, MODULE& m)
{
    std::string input;
    is >> input;

    // Convert to upper case for a correct match if the input is in lowercase
    std::transform(input.begin(), input.end(), input.begin(), ::toupper);

#define INPUT_MODE(e) if (input == #e) { m = MODULE::e; }

    INPUT_MODE(NONE)
    else INPUT_MODE(CAMERA)
    else INPUT_MODE(CONTROL)
//  else INPUT_MODE(POSE)
    else INPUT_MODE(RECORDER)
    else INPUT_MODE(TRACKER)
    else
    {
        std::string msg = "Unknown module '" + input + "'";
        throw std::runtime_error(msg);
    }
#undef INPUT_MODE

    return is;
}

using ModuleFunc = std::function<void(int,char**)>;
using ModuleMap = std::map<std::string, ModuleFunc>;

int main(int argc, char **argv)
{    
    {
        // Print the start message
        time_t raw_time;
        time(&raw_time);
        INFO("Started {} on {}", argv[0], ctime(&raw_time));
    }

    GOOGLE_PROTOBUF_VERIFY_VERSION;

    // The return value, depends on if there are errors that occur or not
    int ret = 0;

    ModuleMap available_modules;
    available_modules["camera"] = holo3d::camera::main;
//  available_modules["control"] = holo3d::control::main;
    available_modules["recorder"] = holo3d::recorder::main;
//  available_modules["tracker"] = holo3d::tracker::main;
//  available_modules["pose"] = holo3d::pose::main;

    std::string all_mods = "";
    std::string sep = "";
    for (auto m : available_modules) 
    {
        all_mods += sep + "'" + m.first + "'"; 
        sep = ", ";
    }

    try
    {
        // Find the module to load
        ModuleFunc module_func = nullptr;
        for (int i=1; i<argc; i++) 
        {
            std::string module_to_load = argv[i];
            std::transform(module_to_load.begin(), module_to_load.end(), module_to_load.begin(), ::tolower);

            auto it = available_modules.find(module_to_load);
            if (it != available_modules.end()) 
            {
                module_func = it->second;
                break;
            }           
        }

        ASSERT_THROW_ALWAYS(module_func, "No valid module selected!! options are {}", all_mods);

        module_func(argc, argv);    
        
    } catch (TCLAP::ArgException& e) 
    {
        ERROR("\nTCLAP exception: {}", e.what());
    } catch (std::runtime_error& e)
    {
        ERROR("\nCaught runtime_error: {}", e.what());
        ret = -1;
    } catch (std::exception& e)
    {
        ERROR("\nCaught exception: {}", e.what());
        ret = -1;

    } 
    catch(...)
    {
        ERROR("\nUnknown exception!!");
        ret = -1;
    }

    // Do final shutdown steps
    google::protobuf::ShutdownProtobufLibrary();

    return ret;
}

