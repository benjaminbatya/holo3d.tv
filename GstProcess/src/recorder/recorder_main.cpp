// recorder_main.cpp for GstProcess

#include <tclap/CmdLine.h>

#include "util_funcs.hpp"

#include <gst_funcs.hpp>

#include <thread>

namespace holo3d
{

namespace recorder
{

static const uint16_t GSTREAMER_PORT = 5000;
static const uint16_t NUM_ROIS = 4;

void run_rois(uint16_t port, uint16_t num_rois);
void spawn_roi(PipelinePtr& pipeline, uint16_t port);

int main(int argc, char** argv)
{    
    Gst::init(argc, argv);

    TCLAP::CmdLine cmd("Recorder Module options", ' ', "0.1", true);

    TCLAP::UnlabeledValueArg<std::string> module_arg("module", "", true, "", "The module to load.");
    cmd.add(module_arg);

    TCLAP::ValueArg<uint16_t> port_arg("p", "port", "", false, GSTREAMER_PORT, "The port to use for gstreamer data");
    cmd.add(port_arg);

    TCLAP::ValueArg<uint16_t> roi_arg("n", "num_rois", "", false, NUM_ROIS, "The number of ROIs to display");
    cmd.add(roi_arg);

    cmd.parse(argc, argv);

    INFO_SHORT("port = {}", port_arg.getValue());
    INFO_SHORT("num rois = {}", roi_arg.getValue());

    run_rois(port_arg.getValue(), roi_arg.getValue());

    return 0;
}

void run_rois(uint16_t port, uint16_t num_rois)
{
    ASSERT_THROW_ALWAYS(num_rois > 0, "Number of ROIs has to be greater then 0!");

    PipelinePtr pipeline = Gst::Pipeline::create();
    ASSERT(pipeline);

    for (int i=0; i<int(num_rois); i++, port++) 
    {
        spawn_roi(pipeline, port);
    }

    Gst::StateChangeReturn cret = pipeline->set_state(Gst::STATE_PLAYING);
    ASSERT_THROW(cret != Gst::STATE_CHANGE_FAILURE, "Failed to set pipeline to playing state");

    BusPtr bus = pipeline->get_bus();
    ASSERT(bus);

    bool running = true;
    while (running) 
    {
        MessagePtr msg = bus->pop(Gst::CLOCK_TIME_NONE,
                                  Gst::MESSAGE_STATE_CHANGED | Gst::MESSAGE_ERROR | Gst::MESSAGE_EOS);
        if (!msg) { continue; }

        switch (msg->get_message_type()) 
        {
        case Gst::MESSAGE_ERROR:
        {
            ErrorMsgPtr err = ErrorMsgPtr::cast_static(msg);
            Glib::Error error;
            std::string debug_info;
            err->parse(error, debug_info);
            ERROR("Error received from element '{}': {}", err->get_source()->get_name(), error.what());
            ERROR("Debugging information: {}", debug_info);
            running = false;
        } break;
        case Gst::MESSAGE_EOS:
            INFO("End of stream reached");
            running = false;
            break;
        case Gst::MESSAGE_STATE_CHANGED:
            if (msg->get_source() == pipeline) 
            {
                MsgStateChgPtr chgmsg = MsgStateChgPtr::cast_static(msg);
                Gst::State old_state = chgmsg->parse_old();
                Gst::State new_state = chgmsg->parse();
                INFO("Pipeline changed from {} to {}", old_state, new_state);

//                  GstBin* bin = GST_BIN(pipeline->gobj());
//                  char buf[42];
//                  snprintf(buf, 43, "graph_%d", new_state);
//                  GST_DEBUG_BIN_TO_DOT_FILE(bin, GST_DEBUG_GRAPH_SHOW_ALL, buf);
            }
            break;
        default:
            ERROR("Unexpected message received");
            break;
        }
    }

    pipeline->set_state(Gst::STATE_NULL);
}

void spawn_roi(PipelinePtr& pipeline, uint16_t port)
{
    ElementPtr udp_src = Gst::ElementFactory::create_element("udpsrc");
    ASSERT(udp_src);
    udp_src->set_property("port", int(port));

    std::string caps_str = fmt::format("application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96");
    INFO(caps_str);
    CapsPtr caps = Gst::Caps::create_from_string(caps_str);
    ASSERT(caps);

    ElementPtr caps_filter = Gst::ElementFactory::create_element("capsfilter");
    caps_filter->set_property("caps", caps);

    ElementPtr rtp_h264_depay = Gst::ElementFactory::create_element("rtph264depay");
    ASSERT(rtp_h264_depay);

    ElementPtr h264_dec = Gst::ElementFactory::create_element("avdec_h264");
    ASSERT(h264_dec);

    ElementPtr display_sink = Gst::ElementFactory::create_element("fpsdisplaysink");
    ASSERT(display_sink);

    pipeline->add(udp_src)->add(caps_filter)->add(rtp_h264_depay)->add(h264_dec)->add(display_sink);

    udp_src->link(caps_filter)->link(rtp_h264_depay);
    rtp_h264_depay->link(h264_dec)->link(display_sink);
}


}; // namespace control

}; // namespace holo3d
