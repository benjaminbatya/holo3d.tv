__author__ = 'benjamin'

from PySide import QtCore, QtGui, QtUiTools
import numpy as np
import math

from holo3d_utils import INFO, ERROR

from VolumeObject import VolumeObject

class VolumeEditor(QtGui.QWidget):
    def __init__(self, parent):
        super(VolumeEditor, self).__init__(parent)

        self._current_obj = None
        self._setting = False

        ui_loader = QtUiTools.QUiLoader()

        file = QtCore.QFile("VolumeEditor.ui")
        file.open(QtCore.QFile.ReadOnly)
        self._form = ui_loader.load(file, self)
        file.close()

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self._form)
        self.setLayout(layout)

        self._origin_edits = [None] * 3
        self._origin_edits[0] = self._form.x_pos
        self._origin_edits[1] = self._form.y_pos
        self._origin_edits[2] = self._form.z_pos

        self._size_edits = [None] * 3
        self._size_edits[0] = self._form.x_size
        self._size_edits[1] = self._form.y_size
        self._size_edits[2] = self._form.z_size

        for i in xrange(3):
            self._origin_edits[i].valueChanged.connect(self.origin_changed)

        for i in xrange(3):
            self._size_edits[i].valueChanged.connect(self.size_changed)

        self._rot_edit = self._form.y_rot
        self._rot_edit.valueChanged.connect(self.rot_changed)

        self.hide()

    def edit(self, object):
        self._current_obj = object

        if object is None or type(object) is not VolumeObject:
            ERROR("Cannot edit object %r" % str(object))
            self.hide()
            return

        self._setting = True

        vec = object.origin()
        for i in xrange(3):
            self._origin_edits[i].setValue(vec[i])

        vec = object.size()
        for i in xrange(3):
            self._size_edits[i].setValue(vec[i])

        rot = object.rotation()
        rot = math.degrees(rot)
        self._rot_edit.setValue(rot)

        self._setting = False

        self.show()

    def origin_changed(self):
        if self._setting: return

        vec = [None] * 3
        for i in xrange(3):
            vec[i] = self._origin_edits[i].value()

        self._current_obj.set_origin(vec)

    def size_changed(self):
        if self._setting: return

        vec = [None] * 3
        for i in xrange(3):
            vec[i] = self._size_edits[i].value()

        self._current_obj.set_size(vec)

    def rot_changed(self):
        if self._setting: return

        val = self._rot_edit.value()
        val = math.radians(val)

        self._current_obj.set_rotation(val)