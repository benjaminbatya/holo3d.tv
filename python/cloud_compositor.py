__author__ = 'benjamin'

from PySide import QtCore

from holo3d_utils import INFO, ERROR
from fusion_model import Fusion_Model
from GatherAlgo import GatherCL

from OpenGL.GL import *
from OpenGL.GLU import *

import OpenGL.arrays as GLArrays

import pyopencl as cl
import sys
import os
import math
import numpy as np

import struct

POINT_SIZE = 16  # 4 floats = 16 bytes per point. 4 components works better on the GPU
COLOR_SIZE = 4  # 4 bytes per color. 4 componentsworks better on the GPU

class CloudCompositor(QtCore.QObject):
    '''
    This class converts the frames into point clouds,
    composites them into a single large cloud and
    produces a point vbo and a color vbo.
    '''

    changed_signal = QtCore.Signal()            # << Fired whenever the data in the compositer changes

    def __init__(self, parent, model):
        super(CloudCompositor, self).__init__(parent)

        self._model = model

        self._point_vbo = None
        self._color_vbo = None
        self._gl_objects = []
        self._point_count = 0

        self._clinit()

        model.project_changed.connect(self.project_loaded)
        model.time_value_changed.connect(self.set_dirty)
        model.volume_filter_signal.connect(self.set_dirty)

        self.set_dirty()

        self._gather = GatherCL()

    def _clinit(self):
        # Setup the context
        platforms = cl.get_platforms()
        from pyopencl.tools import get_gl_sharing_context_properties
        if sys.platform == "darwin":
            self._ctx = cl.Context(properties=get_gl_sharing_context_properties(),
                             devices=[])
        else:
            self._ctx = cl.Context(properties=[
                (cl.context_properties.PLATFORM, platforms[0])]
                + get_gl_sharing_context_properties(), devices=None)
        self._queue = cl.CommandQueue(self._ctx)

        # Load the program
        f = open("cloud_compositor.cl", 'r')
        fstr = "".join(f.readlines())
        f.close()

        # HACK: Disable program caching to allow opencl's printf to work
        # See http://documen.tician.de/pyopencl/runtime.html#programs-and-kernels
        # and http://www.thebigblob.com/opencl-bugs/
        os.environ["PYOPENCL_NO_CACHE"] = "1"
        self._program = cl.Program(self._ctx, fstr).build()

        # Calculate all of the variables needed to run the program
        device = self._ctx.devices[0]
        work_group_size = self._program.generate_depth.get_work_group_info(cl.kernel_work_group_info.WORK_GROUP_SIZE, device)
        group_size = int(math.sqrt(work_group_size))
        self._local_shape = (group_size, group_size)

        self._mf = cl.mem_flags

        # Note: Maximum of 8 cameras at 640x480 for now...
        self._max_points = 8 * 640 * 480

        self._all_points_np = np.zeros((self._max_points, 4), dtype=np.float32)
        self._all_colors_np = np.zeros((self._max_points, 4), dtype=np.uint8)

        self._all_points_cl = cl.Buffer(self._ctx, self._mf.READ_WRITE | self._mf.COPY_HOST_PTR, hostbuf=self._all_points_np)
        self._all_colors_cl = cl.Buffer(self._ctx, self._mf.READ_WRITE | self._mf.COPY_HOST_PTR, hostbuf=self._all_colors_np)

        # Build the point vbo here
        point_vbo = glGenBuffers(1)
        self._point_vbo = point_vbo

        glBindBuffer(GL_ARRAY_BUFFER, point_vbo)
        point_size = self._max_points * POINT_SIZE
        ret = glBufferData(GL_ARRAY_BUFFER, point_size, None, GL_DYNAMIC_DRAW)

        # INFO("point_vbo: ret = %r" % ret)

        # Build the color vbo here
        color_vbo = glGenBuffers(1)
        self._color_vbo = color_vbo

        glBindBuffer(GL_ARRAY_BUFFER, color_vbo)
        color_size = self._max_points * COLOR_SIZE
        ret = glBufferData(GL_ARRAY_BUFFER, color_size, None, GL_DYNAMIC_DRAW)

        # INFO("color_vbo: ret = %r" % ret)

        self._point_cl = cl.GLBuffer(self._ctx, self._mf.READ_WRITE, int(point_vbo))
        self._color_cl = cl.GLBuffer(self._ctx, self._mf.READ_WRITE, int(color_vbo))

        self._queue.finish()

        self._gl_objects = [self._point_cl, self._color_cl]

    @QtCore.Slot()
    def set_dirty(self):
        '''
        indicate that everything needs to be regenerated
        '''
        self._dirty = True

        self.set_plane_dirty()

        self.changed_signal.emit()

    def set_plane_dirty(self):
        '''
        indicate that the various results that use the vbos need to be regenerated
        :return:
        '''
        self._mean_dirty = True
        self._num_valid_dirty = True
        self._plane_dirty = True

    @QtCore.Slot()
    def project_loaded(self):
        '''Listen to all of the new camera nodes'''
        files = self._model.files()

        for file_path in files:
            node = self._model.camera_nodes(file_path)

            node.changed_signal.connect(self.set_dirty)
            node.show_cloud_changed.connect(self.set_dirty)

        volume = self._model.volume_of_interest()
        volume.changed_signal.connect(self.set_dirty)

    def _recalc(self):
        '''
        Recalculates the vbos from data in the model
        As the points are added, they are filtered and transformed by camera
        Filtering means removing the points with depth<=small amount (0??) and depth >= 10000 (or less)
        Maybe try to do some merging between points that should be the same

        Could potentially split the change in the camera transformation from loading the points
        (aka, load the points once a frame into VBOs and then only when the camera's transform changes, recomposite the scene)
        For now, load_frames is called whenever the frame changes or the cameras' transformation changes
        '''

        if not self._dirty:
            return

        self._dirty = False

        self._point_count = 0

        files = self._model.files()

        debug_host = np.zeros((4,4), dtype=np.float32)
        debug_device = cl.Buffer(self._ctx, self._mf.READ_WRITE | self._mf.COPY_HOST_PTR, hostbuf=debug_host)

        # Take each of the files and convert each pixel in each frame into a 3d point.
        for file_path in files:
            node = self._model.camera_nodes(file_path)

            if node is None:
                continue

            if not node.show_cloud():
                continue

            # INFO("file_path = %r" % file_path)

            # NOTE: The the camera matrix has to be transposed, cast and reshaped to get it into the shape that opencl likes
            rot_mat = node.matrix().T.astype(np.float32).reshape(16,1)
            # INFO("rot_mat = \n%r" % rot_mat)

            frame = self._model.current_frame(file_path)

            width = frame.file_header().width
            height = frame.file_header().height

            global_shape = (width, height)

            color_buf = frame.color_buffer()
            depth_buf = frame.depth_buffer()

            # context = {
            #     'only_display_cloud_edges': self._only_display_cloud_edges
            # }

            # allocate the input buffers
            in_color = cl.Buffer(self._ctx, self._mf.READ_ONLY | self._mf.COPY_HOST_PTR, hostbuf=color_buf)
            in_depth = cl.Buffer(self._ctx, self._mf.READ_ONLY | self._mf.COPY_HOST_PTR, hostbuf=depth_buf)
            in_matrix = cl.Buffer(self._ctx, self._mf.READ_ONLY | self._mf.COPY_HOST_PTR, hostbuf=rot_mat)

            # Maybe use structs instead: http://enja.org/2011/03/30/adventures-in-opencl-part-3-constant-memory-structs/
            control_host = np.array([self._max_points, self._point_count], dtype=np.int32)
            control_device = cl.Buffer(self._ctx, self._mf.READ_WRITE | self._mf.COPY_HOST_PTR, hostbuf=control_host)

            kernel_args = (in_color, in_depth, in_matrix, control_device, self._all_points_cl, self._all_colors_cl, debug_device)

            self._program.generate_depth(self._queue, global_shape, self._local_shape, *(kernel_args))

            # Note: don't worry about the self._point_count for now
            # cl.enqueue_copy(self._queue, control_host, control_device)

            # cl.enqueue_copy(self._queue, debug_host, debug_device)

            # self._queue.finish()

            self._point_count += width*height

            # INFO("After: temp_value = \n%r" % debug_host) # [0:4])
            # value = np.dot(rot_mat, debug_host[0])
            # INFO("Value = %r" % value)
            # INFO("After: value = %r" % debug_host[4:8])
            # INFO("debugging = %r" % debug_host)

        # Filter points depending on the filter specified by the model
        if(self._model.volume_filter() == Fusion_Model.VOLUME_FILTER_TYPES.None):
            # Just copy the points from _all_points to _points
            kernel_args = (self._all_points_cl, self._all_colors_cl, self._point_cl, self._color_cl)

            global_shape = (self._point_count,)

            cl.enqueue_acquire_gl_objects(self._queue, self._gl_objects)

            self._program.copy_points(self._queue, global_shape, None, *(kernel_args))

            cl.enqueue_release_gl_objects(self._queue, self._gl_objects)

        else: # Do Volume and floor filtering...
            # Then do the different types of filtering
            volume_mat = self._model.volume_of_interest().matrix().astype(np.float32)
            # INFO("volume_mat = \n%r" % volume_mat)
            volume_mat = volume_mat.reshape(16,1)

            volume_cl = cl.Buffer(self._ctx, self._mf.READ_ONLY | self._mf.COPY_HOST_PTR, hostbuf=volume_mat)

            num_pts_per_item = 1024
            num_items = self._point_count / num_pts_per_item
            global_shape = (num_items,)
            local_shape = None

            counts_np = np.zeros((num_items,1), dtype=np.uint32)
            counts_cl = cl.Buffer(self._ctx, self._mf.READ_WRITE | self._mf.COPY_HOST_PTR, hostbuf=counts_np)

            if(self._model.volume_filter() == Fusion_Model.VOLUME_FILTER_TYPES.Volume):

                kernel_args = (self._all_points_cl, volume_cl, np.uint32(num_pts_per_item), counts_cl)

                self._program.volume_filter_count(self._queue, global_shape, local_shape, *(kernel_args))

            elif(self._model.volume_filter() == Fusion_Model.VOLUME_FILTER_TYPES.Floor):

                floor = self._model.floor()
                mean = floor.mean()
                normal = floor.normal()

                params = struct.pack('ffffffffffi',
                                     mean[0], mean[1], mean[2], 1.0,
                                     normal[0], normal[1], normal[2], floor.d(),
                                     floor.dist_mean(), floor.dist_std(), num_pts_per_item)
                # print len(params), struct.calcsize('ffffffffffi')
                params_cl = cl.Buffer(self._ctx, self._mf.READ_ONLY, len(params))
                cl.enqueue_copy(self._queue, params_cl, params)

                kernel_args = (self._all_points_cl, volume_cl, params_cl, counts_cl)

                self._program.floor_filter_count(self._queue, global_shape, local_shape, *(kernel_args))

            else:
                raise TypeError("Invalid filter type %s" % str(self._model.volume_filter()))

            cl.enqueue_copy(self._queue, counts_np, counts_cl)

            # Calc the prefix sum
            # NOTE: Do this with opencl later so counts doesn't have to be copied
            offsets = np.cumsum(counts_np)
            # Start with zero
            offsets = np.concatenate(([0], offsets)).astype(np.uint32)

            offsets_cl = cl.Buffer(self._ctx, self._mf.READ_ONLY | self._mf.COPY_HOST_PTR, hostbuf=offsets)

            # Get the valid items from the output buffer and transform them

            # Copy the points from _all_points to _points and respect the volume and offsets
            kernel_args = (self._all_points_cl, self._all_colors_cl,
                           offsets_cl, np.uint32(num_pts_per_item),
                           self._point_cl, self._color_cl)

            cl.enqueue_acquire_gl_objects(self._queue, self._gl_objects)

            self._program.volume_filter_copy(self._queue, global_shape, None, *(kernel_args))

            cl.enqueue_release_gl_objects(self._queue, self._gl_objects)

            # Update the point count from the offsets
            self._point_count = int(offsets[-1])

        self._queue.finish()

        self.set_plane_dirty()

    def point_vbo(self):
        '''Returns the composited point vbo'''
        self._recalc()

        return self._point_vbo

    def color_vbo(self):
        '''Returns the composited color vbo'''
        self._recalc()

        return self._color_vbo

    def num_points(self):
        self._recalc()

        return self._point_count

    def valid_points(self):
        self._recalc()

        return self._point_cl

    def _calc_sizes(self):
        num_pts = self.num_points()
        group_size = self._local_shape[0] * self._local_shape[1]

        num_work_groups = num_pts / group_size

        if (num_pts % group_size > 0):
            num_work_groups += 1

        return (num_pts, group_size, num_work_groups)

    def num_valid_points(self):
        # Make sure the vbos are up to date
        self._recalc()

        if not self._num_valid_dirty:
            return self._num_valid

        self._num_valid_dirty = False

        (num_pts, group_size, num_work_groups) = self._calc_sizes()

        count_host = np.zeros((num_work_groups,), dtype=np.uint32)

        count_device = cl.Buffer(self._ctx, self._mf.READ_WRITE | self._mf.COPY_HOST_PTR, hostbuf=count_host)

        locals_device = cl.LocalMemory(group_size * 4)

        cl.enqueue_acquire_gl_objects(self._queue, self._gl_objects)

        global_shape = (num_pts,)
        local_shape = (group_size,)

        kernel_args = (self._color_cl, locals_device, count_device)

        self._program.num_valid_points(self._queue, global_shape, local_shape, *(kernel_args))

        # Note: don't worry about the self._point_count for now
        cl.enqueue_copy(self._queue, count_host, count_device)

        cl.enqueue_release_gl_objects(self._queue, self._gl_objects)
        self._queue.finish()

        self._num_valid = np.sum(count_host)

        # INFO("count = %r" % count)

        return self._num_valid

    def points_mean(self):
        count = self.num_valid_points()

        if not self._mean_dirty:
            return self._mean
        self._mean_dirty = False

        if count < 1:
            ERROR("No valid points!")
            self._mean = None
            return self._mean

        (num_pts, group_size, num_work_groups) = self._calc_sizes()

        means_host = np.zeros((num_work_groups, 4), dtype=np.float32)

        means_device = cl.Buffer(self._ctx, self._mf.READ_WRITE | self._mf.COPY_HOST_PTR, hostbuf=means_host)

        locals_device = cl.LocalMemory(group_size * 8 * 8)

        cl.enqueue_acquire_gl_objects(self._queue, self._gl_objects)

        global_shape = (self.num_points(),)
        local_shape = (group_size,)

        kernel_args = (self._point_cl, self._color_cl, locals_device, means_device)

        self._program.points_mean(self._queue, global_shape, local_shape, *(kernel_args))

        cl.enqueue_copy(self._queue, means_host, means_device)

        cl.enqueue_release_gl_objects(self._queue, self._gl_objects)
        self._queue.finish()

        means = np.sum(means_host, axis=0)

        means /= count

        (x, y, z, w) = means.tolist()
        self._mean = np.array([x, y, z])

        # INFO("means = %r" % str(means))

        return self._mean

    def plane_fit(self):
        count = self.num_valid_points()

        if not self._plane_dirty:
            return self._plane

        self._plane_dirty = False

        if count < 3:
            ERROR("Not enough points!")
            self._plane = None
            return self._plane

        (num_pts, group_size, num_work_groups) = self._calc_sizes()

        cl.enqueue_acquire_gl_objects(self._queue, self._gl_objects)

        self._valid_pts = self._gather.run(self._point_cl, self._color_cl, num_pts, 1024)

        cl.enqueue_release_gl_objects(self._queue, self._gl_objects)
        self._queue.finish()

        # INFO("count = %r" % valid_pts.shape[0])

        # Subtract out the mean
        pts = self._valid_pts - self.points_mean()

        U, s, V = np.linalg.svd(pts, full_matrices=False)

        normal = V[-1]
        if(np.dot(normal, [0, 1, 0]) < 0):
            # Flip the normal so it points up
            normal *= -1

        self._plane = normal

        # Note: I think the normal is unitary..
        assert np.isclose(np.linalg.norm(normal), 1.0)

        return self._plane

    def calc_distances(self):
        '''
        Calculates the mean and standard deviation of the distances of the points from the plane
        The code follows http://mathworld.wolfram.com/Point-PlaneDistance.html
        :return:
        '''
        normal = self.plane_fit()
        mean = self.points_mean()

        # Calculate d from ax + by + cz + d = 0
        d = np.dot(normal, mean) * -1.0

        pts = self._valid_pts

        distances = np.dot(pts, normal) + d

        dist_mean = np.mean(distances)
        dist_std = np.std(distances, dtype=np.float64)

        # INFO("mean = %r, std = %r" % (dist_mean, dist_std))

        return (d, dist_mean, dist_std)
