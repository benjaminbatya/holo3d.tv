__author__ = 'benjamin'

from PySide import QtCore
import numpy as np
import os
import view_calculator
import color_matcher

from holo3d_utils import INFO, ERROR

import holo3d

PIXEL_DEPTH = 3

class CameraFrame(holo3d.camera_frame):
    _color_mapper = color_matcher.Naive() # The color matcher to use

    def __init__(self, loader):
        super(CameraFrame, self).__init__()
        self._loader = loader

        self._num_pixels = self.file_header().width * self.file_header().height

        self._color_buffer = None   # << The decoded color buffer for this frame
        self._rgb_depth_buffer = None   # << The decoded rgb depth buffer for this frame
        self._depth_buffer = None       # << The raw depth values for this frame as np.ushort16

        self._rect = None           # << The clipping rectangle used to limit getChessboardGrid()

        self._corners = None        # << The grid corners located in this frame
        self._corners_bbox = None   # << The bounding boxes for the grid corners

        self._color_name = None     # << The color identified in the grid in this frame

    def base_name(self):
        return os.path.basename(self.file_path())

    def file_path(self):
        return self._loader.file_path()

    def num_pixels(self):
        return self._num_pixels

    def file_header(self):
        return self._loader.file_header

    def color_buffer(self):
        if self._color_buffer is None:
            self._color_buffer = np.ndarray(shape=(self._num_pixels, PIXEL_DEPTH), dtype=np.uint8)

            self._loader.color_decoder().decode(self._color_buffer,
                                                super(CameraFrame, self).color_buffer,
                                                self._num_pixels * PIXEL_DEPTH,
                                                super(CameraFrame, self).color_buffer_size)

        return self._color_buffer

    def rgb_depth_buffer(self):
        if self._rgb_depth_buffer is None:
            self._rgb_depth_buffer = np.ndarray(shape=(self._num_pixels, PIXEL_DEPTH), dtype=np.uint8)

            self._loader.depth_decoder().decode(self._rgb_depth_buffer,
                                                super(CameraFrame, self).depth_buffer,
                                                self._num_pixels * PIXEL_DEPTH,
                                                super(CameraFrame, self).depth_buffer_size)

        return self._rgb_depth_buffer

    def depth_buffer(self):
        if self._depth_buffer is None:
            self._depth_buffer = np.ndarray(shape=(self._num_pixels, 1), dtype=np.uint16)

            self._loader.depth_decoder().decode_depth(self._depth_buffer,
                                                      super(CameraFrame, self).depth_buffer,
                                                      self._num_pixels * 2,
                                                      super(CameraFrame, self).depth_buffer_size)

        return self._depth_buffer

    # Note: maybe move corners and camera_position into view_calculator
    def corners(self, clipping_rect, grid):
        if self._corners is None:

            # Update the dialog widget
            # Get the corners
            data = self.color_buffer().reshape((self.height, self.width, PIXEL_DEPTH))

            # print "Shape =", data.shape
            # Clip the data
            data = data[clipping_rect.top():clipping_rect.bottom()+1,
                        clipping_rect.left():clipping_rect.right()+1]

            corners, bbox = view_calculator.corners(data, grid)

            self._corners = []
            self._corners_bbox = None

            if corners:
                self._corners = corners
                self._corners_bbox = QtCore.QRect(bbox[0], bbox[1], bbox[2], bbox[3])

                # Crop data
                data = data[bbox[1]:bbox[1]+bbox[3], bbox[0]:bbox[0]+bbox[2]]

                # start_time = time()
                # 3) calculate the best color match for the grid
                # self._color_name, color_value = self._color_mapper.calc(data)

                # if self.base_name().startswith("0"): self._color_name = "blue"
                # elif self.base_name().startswith("1"): self._color_name = "black"

                # delta_time = time() - start_time
                # INFO("delta_time = %dms" % (delta_time*1000))
                # INFO("camera %s: best color match is %s, color value=(%s)" %
                #       (self.base_name(), self._color_name, color_value))
            else:
                # ERROR("Camera %s: frame %d: No corners found!!" %
                #       (self.base_name(), self.frame_header.frame_number))
                pass

        if len(self._corners) == 0:
            return None, None
        else:
            return self._corners, self._corners_bbox

    def color_name(self, clipping_rect, grid):
        if self._color_name is None:
            self.corners(clipping_rect, grid)

        return self._color_name

    def camera_position(self, clipping_rect, grid, camera_matrix, dist_coeffs, color_name):
        corners, bbox = self.corners(clipping_rect, grid)

        if corners is None:
            return (None, None)

        (rotate_vec, translate_vec) = view_calculator.camera_position(color_name,
                                                                      corners, clipping_rect,
                                                                      camera_matrix, dist_coeffs)
        # INFO("Camera=%s\n" % Camera_position)

        return rotate_vec, translate_vec
