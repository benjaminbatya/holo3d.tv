__author__ = 'benjamin'

import unittest
import sys
import math

import numpy as np
import pyopencl as cl

class LeastSquaresTest(unittest.TestCase):
    def __init__(self, method_name):
        super(LeastSquaresTest, self).__init__(method_name)

        # Setup all of the opencl context
        platforms = cl.get_platforms()
        from pyopencl.tools import get_gl_sharing_context_properties
        self._ctx = self._ctx = cl.create_some_context()
        self._queue = cl.CommandQueue(self._ctx)

        # Load the program
        f = open("cloud_compositor.cl", 'r')
        fstr = "".join(f.readlines())

        self._program = cl.Program(self._ctx, fstr).build()

        self._mf = cl.mem_flags

        # Calculate all of the variables needed to run the program
        device = self._ctx.devices[0]
        work_group_size = self._program.generate_depth.get_work_group_info(cl.kernel_work_group_info.WORK_GROUP_SIZE, device)
        group_size = int(math.sqrt(work_group_size))
        self._local_shape = (group_size, group_size)
        self._group_size = work_group_size

    def setUp(self):
        self._points_host = np.array([[-1, 0, -1, 1], [-1, 0, 1, 1], [1, 0, 1, 1], [1, 0, -1, 1]], dtype=np.float32)
        self._colors_host = np.ones((4, 4), dtype=np.float32)

    def test_count(self):
        num_pts = self._points_host.shape[0]
        num_work_groups = num_pts / self._group_size

        if (num_pts % self._group_size > 0):
            num_work_groups += 1

        self._points_device = cl.Buffer(self._ctx, self._mf.READ_ONLY | self._mf.COPY_HOST_PTR, hostbuf=self._points_host)
        self._colors_device = cl.Buffer(self._ctx, self._mf.READ_ONLY | self._mf.COPY_HOST_PTR, hostbuf=self._colors_host)

        count_host = np.zeros((num_work_groups,), dtype=np.uint32)
        count_device = cl.Buffer(self._ctx, self._mf.READ_WRITE | self._mf.COPY_HOST_PTR, hostbuf=count_host)

        locals_device = cl.LocalMemory(self._group_size * 4)

        global_shape = (num_pts,)
        local_shape = (self._group_size,)

        kernel_args = (self._colors_device, locals_device, count_device)

        self._program.num_valid_points(self._queue, global_shape, local_shape, *(kernel_args))

        # Note: don't worry about the self._point_count for now
        cl.enqueue_copy(self._queue, count_host, count_device)

        self._queue.finish()

        count = np.sum(count_host)

        self.assertEqual(count, self._points_host.shape[0])

    # def _calc_normal(self):
    #     num_pts = self._points_host.shape[0]
    #     num_work_groups = num_pts / self._group_size
    #
    #     if (num_pts % self._group_size > 0):
    #         num_work_groups += 1
    #
    #     self._points_device = cl.Buffer(self._ctx, self._mf.READ_ONLY | self._mf.COPY_HOST_PTR, hostbuf=self._points_host)
    #     self._colors_device = cl.Buffer(self._ctx, self._mf.READ_ONLY | self._mf.COPY_HOST_PTR, hostbuf=self._colors_host)
    #
    #     debug_host = np.zeros((self._group_size,8), dtype=np.float64)
    #     debug_device = cl.Buffer(self._ctx, self._mf.READ_WRITE | self._mf.COPY_HOST_PTR, hostbuf=debug_host)
    #
    #     values_host = np.zeros((num_work_groups,8), dtype=np.float64)
    #     values_device = cl.Buffer(self._ctx, self._mf.READ_WRITE | self._mf.COPY_HOST_PTR, hostbuf=values_host)
    #
    #     locals_device = cl.LocalMemory(self._group_size * 8 * 8)
    #
    #     global_shape = (num_pts,)
    #     local_shape = (self._group_size,)
    #
    #     kernel_args = (self._points_device, self._colors_device, locals_device, values_device, debug_device)
    #
    #     self._program.plane_fit(self._queue, global_shape, local_shape, *(kernel_args))
    #
    #     # Note: don't worry about the self._point_count for now
    #     cl.enqueue_copy(self._queue, values_host, values_device)
    #
    #     cl.enqueue_copy(self._queue, debug_host, debug_device)
    #
    #     self._queue.finish()
    #
    #     # print("debug info = \n%r" % debug_host)
    #
    #     values = np.sum(values_host, axis=0)
    #
    #     (x, y, z, x2, z2, xy, xz, yz) = values.tolist()
    #
    #     mat = np.array([[x2, xz, x],
    #                     [xz, z2, z],
    #                     [ x,  z, self._points_host.shape[0]]], dtype=np.float64)
    #     vec = np.array([xy, yz, y], dtype=np.float64)
    #
    #     det = np.linalg.det(mat)
    #
    #     # Check if the det is close to zero, if it is there's no solution possible
    #     if np.isclose(det, 0.0):
    #         return None
    #
    #     inv_mat = np.linalg.inv(mat)
    #
    #     id = np.dot(inv_mat, mat)
    #
    #     normal = np.dot(inv_mat, vec)
    #
    #     # normal *= -1.0 # Flip the normal so it points in the correct direction (generally up)
    #     # Swap the y and z axis
    #     (x, z, y) = normal.tolist()
    #     normal = np.array([x, y, z])
    #
    #     # Get the unit vector
    #     norm = np.linalg.norm(normal)
    #     normal /= norm
    #
    #     return normal

    def _calc_normal_2(self):
        # Subtract out the mean
        points = self._points_host[:, 0:3]
        sum = np.sum(points, axis=0)
        mean = sum / points.shape[0]

        points -= mean

        # Run the SVD algo in numpy
        U, s, V = np.linalg.svd(points, full_matrices=False)

        # print ("U = %r" % U)
        # print ("V = %r" % V)
        # print ("s = %r" % s)

        # Get the last element
        normal = V[-1]

        if(np.dot(normal, [0, 1, 0]) < 0):
            # Flip the normal so it points up
            normal *= -1

        return normal

    def test_plane_fit(self):

        normal = self._calc_normal_2()

        # INFO("Plane normal = %r" % normal)

        self.assertAlmostEqual(normal.tolist(), [0.0, 1.0, 0.0])

    def test_plane_fit_rot_45(self):
        # test rotating 45 degrees around the z-axis
        angle = math.radians(45)
        test_matrix = np.array([[+math.cos(angle), math.sin(angle), 0.0],
                            [-math.sin(angle), math.cos(angle), 0.0],
                            [0.0             , 0.0,             1.0]])

        for i in xrange(self._points_host.shape[0]):
            self._points_host[i, 0:3] = np.dot(test_matrix, self._points_host[i, 0:3])

        normal = self._calc_normal_2()

        # INFO("Plane normal = %r" % normal)

        expected = [1.0, 1.0, 0.0]
        norm = np.linalg.norm(expected)
        expected /= norm

        self.assert_(np.allclose(normal, expected), "result=%r, expected=%r" % (normal, expected) )

    def test_plane_fit_rot_offset_and_neg_45(self):
        self._points_host[:, 1] += 1
        # test rotating 45 degrees around the z-axis
        angle = math.radians(-45)
        test_matrix = np.array([[+math.cos(angle), math.sin(angle), 0.0],
                            [-math.sin(angle), math.cos(angle), 0.0],
                            [0.0             , 0.0,             1.0]])

        for i in xrange(self._points_host.shape[0]):
            self._points_host[i, 0:3] = np.dot(test_matrix, self._points_host[i, 0:3])

        normal = self._calc_normal_2()

        # INFO("Plane normal = %r" % normal)

        expected = [-1.0, 1.0, 0.0]
        norm = np.linalg.norm(expected)
        expected /= norm

        self.assert_(np.allclose(normal, expected), "result=%r, expected=%r" % (normal, expected) )