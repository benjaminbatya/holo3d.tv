__author__ = 'benjamin'

from holo3d_utils import INFO
import numpy as np
import sys
import os
import math

import pyopencl as cl

class OpenCL():
    def __init__(self, shape):
        self._shape = shape
        # Initialize all of the needed
        self._ctx = cl.create_some_context()
        self._queue = cl.CommandQueue(self._ctx)

        f = open("depth_generator.cl", 'r')
        fstr = "".join(f.readlines())
        f.close()
        self._program = cl.Program(self._ctx, fstr).build()

        device = self._ctx.devices[0]
        work_group_size = self._program.generate_depth.get_work_group_info(cl.kernel_work_group_info.WORK_GROUP_SIZE, device)
        self._work_group_size = int(math.sqrt(work_group_size))

        self._mf = cl.mem_flags

        self._result_np = np.zeros((shape[0]*shape[1], 4), dtype=np.float32)

        # Allocate the global results buffer
        self._result_buf = cl.Buffer(self._ctx, self._mf.WRITE_ONLY, self._result_np.nbytes)

    def calc(self, data, context):
        # Make each color value into a char4
        # data = data.reshape((self._shape[0], self._shape[1], 1))
        # print data[0]

        # allocate the input buffer
        in_buf = cl.Buffer(self._ctx, self._mf.READ_ONLY | self._mf.COPY_HOST_PTR, hostbuf=data)

        self._program.generate_depth(self._queue, self._shape, (self._work_group_size,self._work_group_size),
                                     in_buf, self._result_buf,
                                     np.int32(50 if context['only_display_cloud_edges'] else -1) )

        cl.enqueue_read_buffer(self._queue, self._result_buf, self._result_np)

        self._queue.finish()

        return self._result_np
