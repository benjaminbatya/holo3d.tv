# -*- python -*-

"""
This is the python fusion visualizer.
Displays the videos on the left side, the 3d fusion view in the middle, 
a python console on the right and a timeline on the bottom.

@author: benjamin
"""

import sys
import os

# print "PYTHONPATH =", os.environ["PYTHONPATH"]

from PySide import QtCore, QtGui, QtUiTools
from camera_view import Camera_View
from fusion_widget import Fusion_Widget
from fusion_model import Fusion_Model
from holo3d_utils import INFO, ERROR
import movie_slider

from CameraObject import CameraObject
from VolumeObject import VolumeObject

from camera_object_editor import CameraNodeEditor
from VolumeEditor import VolumeEditor
import numpy as np

class Main_Window(QtGui.QMainWindow):
    def __init__(self, model):
        super(Main_Window, self).__init__()
        
        self._model = model        

        self._prevent_exit = False

        self.build_ui()  

        self.set_playing(False)

        model.project_changed.connect(self.project_changed)

        model.playing_video_signal.connect(self.set_playing)

        model.status_message_set.connect(self.set_status_message)
        model.selected_object_changed.connect(self.object_selected)

    def build_ui(self):
        # Load the ui file
        ui_loader = QtUiTools.QUiLoader()
        # NOTE: Later make a build script to turn this into a py file automatically
        file = QtCore.QFile("fusion_viewer_frame.ui")
        file.open(QtCore.QFile.ReadOnly)
        self.ui = ui_loader.load(file, self)
        file.close()        
        
        # Setup the menu actions
        self.ui.action_quit.triggered.connect(self.close)
        self.ui.action_load_project.triggered.connect(self.load_project)
        self.ui.action_save_project.triggered.connect(self.save_project)
        self.ui.action_import_settings.triggered.connect(self.import_settings)

        self._recent_project_actions = []
        self._load_app_data()
        self._model.app_settings().recent_projects_changed.connect(self._load_app_data)

        self.ui.action_display_blend.triggered[bool].connect(self.display_blend)
        self.ui.action_display_color.triggered[bool].connect(self.display_color)
        self.ui.action_display_depth.triggered[bool].connect(self.display_depth)    

        # Make the display modes exclusive
        group = QtGui.QActionGroup(self)
        group.enabled = True
        group.exclusive = True
        group.visible = True
        group.addAction(self.ui.action_display_blend)
        group.addAction(self.ui.action_display_color)
        group.addAction(self.ui.action_display_depth)

        # self.ui.action_calibrate_ext.triggered.connect(self.calibrate_extrinsic)
        # self.ui.only_display_cloud_edges_action.triggered[bool].connect(self.action_only_display_cloud_edges)

        # Setup the fusion widget
        container = self.ui.findChild(QtGui.QFrame, 'main_3d_view')
        widget = Fusion_Widget(self, self._model)
        layout = QtGui.QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(widget)            
        container.setLayout(layout)
        self._main_3d_view = widget

        # Set up the cameras
        num_cameras = 4 
        self._camera_views = []    
        for i in range(num_cameras):
            container = self.ui.findChild(QtGui.QFrame, 'camera_' + str(i))
            
            widget = Camera_View(self, i, self._model)
#            print gl_widget.video
            layout = QtGui.QHBoxLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            layout.addWidget(widget)            
            container.setLayout(layout)
            self._camera_views.append(widget)
        
            self._model.project_changed.connect(widget.project_changed)
            self._model.time_value_changed.connect(widget.load_frame)

        # Setup the play button
        play_button = self.ui.findChild(QtGui.QPushButton, 'play_button')
        self._play_button = play_button
        play_button.clicked.connect(self._model.set_play_video)

        # setup the time slider     
        time_slider_frame = self.ui.findChild(QtGui.QFrame, 'time_slider_frame')
        self._time_slider = movie_slider.movie_slider(time_slider_frame, self._model)
        layout = QtGui.QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self._time_slider)
        time_slider_frame.setLayout(layout)

        self._time_slider.time_changed.connect(self._model.set_time)
        self._model.time_value_changed.connect(self.update_slider)

        self._frame_spinner = self.ui.frame_spinner
        self._frame_spinner.setSingleStep(10)
        self._frame_spinner.valueChanged[int].connect(self._time_slider.setValue)
        self._time_slider.valueChanged[int].connect(self._frame_spinner.setValue)

        # Setup the status bar
        self._status_bar = self.ui.findChild(QtGui.QStatusBar, "status_bar")

        self._attribute_editor = self.ui.findChild(QtGui.QWidget, "data_container")

#        print "build_ui: showing UI"
        self.ui.show()

    def set_status_message(self, message):
        self._status_bar.showMessage(message)

    def object_attr_form(self):
        return self._object_attr_form

    # def calibrate_extrinsic(self):
    #     self._model.freeze_ext_camera_props()

    def display_blend(self, display):
        # INFO("display = %d" % display)
        # Update the camera views
        for cam in self._camera_views:
            cam.display(Camera_View.DISPLAY_OPTION.BLEND)

    def display_color(self, display):
        # INFO("display = %d" % display)
        # Update the camera views
        for cam in self._camera_views:
            cam.display(Camera_View.DISPLAY_OPTION.COLOR)
        
    def display_depth(self, display):
        # INFO("display = %d" % display)
        # Update the camera views
        for cam in self._camera_views:
            cam.display(Camera_View.DISPLAY_OPTION.DEPTH)

    def load_project(self):
        self._model.set_play_video(False)

        dir_path = QtGui.QFileDialog.getExistingDirectory(self, "Open Directory", QtCore.QDir.currentPath())

        # Note: We have to detect if the cancel button was pressed because PySide has a weird bug where app.exec_()
        # exits when cancel is pressed and dir_path == ""
        if len(dir_path) == 0:
            self._prevent_exit = True
        self._model.load_project(dir_path)

    def save_project(self):
        self._model.save_project()

    def import_settings(self):
        new_settings = QtGui.QFileDialog.getOpenFileName(self, "Import Settings",
                                                         self._model.project_path(),
                                                         "Settings (*.yaml)")
        file_path = str(new_settings[0])

        self._prevent_exit = True

        if len(file_path) == 0:
            ERROR("No settings file selected")
            return

        # Reload the project
        self._model.import_settings(file_path)

    def project_changed(self, project_path):

        # Reconfigure the time slider
        time_codes = self._model.all_frame_offsets()

        color_set = [QtCore.Qt.red, QtCore.Qt.blue, QtCore.Qt.green, QtCore.Qt.darkYellow]
        colors = {}
        index = 0
        for file in time_codes:
            colors[file] = color_set[index]
            index += 1

        self._time_slider.set_time_info(time_codes, colors)

        self.ui.setWindowTitle(project_path)

        self.set_status_message("Loaded project '%r'" % project_path)

    def set_playing(self, flag):
        self._time_slider.setEnabled(not flag)
        self._frame_spinner.setEnabled(not flag)

        if flag:
            self._play_button.setIcon( self.style().standardIcon(QtGui.QStyle.SP_MediaStop) )
            self._play_button.setText( "Stop" )
        else:
            self._play_button.setIcon( self.style().standardIcon(QtGui.QStyle.SP_MediaPlay) )
            self._play_button.setText( "Play" )


    def update_slider(self):
        self._time_slider.set_time(self._model.time_value())

    def object_selected(self):
        obj = self._model.selected_object()

        # Remove all of the previous children
        layout = self._attribute_editor.layout()
        for i in reversed(range(layout.count())):
            layout.itemAt(i).widget().deleteLater()

        if obj is None: return

        # Note: move this to a EditorFactory
        if type(obj) is CameraObject:
            self._object_editor = CameraNodeEditor(self._attribute_editor)
        elif type(obj) is VolumeObject:
            self._object_editor = VolumeEditor(self._attribute_editor)

        layout.addWidget(self._object_editor)

        self._object_editor.edit(obj)

    @QtCore.Slot(bool)
    def action_only_display_cloud_edges(self, f):
        # self._main_3d_view.set_only_display_cloud_edges(f)
        pass

    @QtCore.Slot()
    def _load_app_data(self):
        menu = self.ui.menu_recent_projects

        recent_projects = self._model.app_settings().recent_projects()

        for action in self._recent_project_actions:
            # INFO("Deleting submenu = %r" % action.text())
            menu.removeAction(action)

        self._recent_project_actions = []

        self._recent_project_signal_mapper = QtCore.QSignalMapper(self)
        self._recent_project_signal_mapper.mapped[str].connect(self._project_clicked)

        for project in recent_projects:
            action = menu.addAction(project)
            self._recent_project_signal_mapper.setMapping(action, project)
            action.triggered.connect(self._recent_project_signal_mapper.map)
            self._recent_project_actions.append(action)

    @QtCore.Slot(str)
    def _project_clicked(self, project):
        # INFO("Opening '%r'" % project)
        self._model.load_project(project)
        

def main():
#    num_frames = 100
    # Set the global printing options
    np.set_printoptions(precision=3)

    app = QtGui.QApplication(sys.argv)
    
    model = Fusion_Model()
    # NOTE: we need to have main_window assigned but not use 
    # otherwise nothing will happen
    main_window = Main_Window(model)

    # Note: don't load any project in the beginning
    # Initialize the model
    # model.load_project(DIR_PATH)

    try:
        # Dont return from this
        while True:
            ret = app.exec_()
            if not main_window._prevent_exit:
                break
            else:
                INFO("QApplication quit unexpectedly, resetting...")
                main_window._prevent_exit = False
    except:
        ERROR("Unexpected error: %r" % sys.exc_info()[0])

    INFO("Exiting: return value = %r" % ret)
    sys.exit(ret)

if __name__ == '__main__':
    main()  