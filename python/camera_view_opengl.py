# -*- coding: utf-8 -*-
"""
Created on Tue Jan 20 11:44:51 2015

@author: benjamin
"""
import holo3d
import numpy as np
from PySide import QtCore, QtGui, QtOpenGL
from fusion_model import Fusion_Model
from holo3d_utils import (INFO, enum)
import os.path
import sys

try:
    from OpenGL.GL import *
    import OpenGL.arrays.vbo as glvbo
    # from OpenGL import GLU
except ImportError:
    app = QtGui.QApplication(sys.argv)
    QtGui.QMessageBox.critical(None, "OpenGL hellogl",
                               "PyOpenGL must be installed to runhis ts example")
    sys.exit(1)

class Camera_View(QtOpenGL.QGLWidget):
    """
    This is a document test
    """
    
    DISPLAY_OPTION = enum('BLEND', 'COLOR', 'DEPTH')    
    
    def __init__(self, parent, index, model):
        QtOpenGL.QGLWidget.__init__(self, parent)
        
        self._model = model
        self._video_name = None
        self._video_index = index
         
        self._frame = holo3d.camera_frame()
        
        self._display_option = self.DISPLAY_OPTION.BLEND
        
        self._display_harris = False
        
#        self._harris_data = np.array([0, 0, -0.5, -0.5, 0.5, 0.5], dtype=np.float32)
#        self._harris_data = self._harris_data.reshape((3,2))

        self._harris_data = np.linspace(-1., 1.)
        self._harris_data = np.array([self._harris_data, self._harris_data], dtype=np.float32)
        self._harris_data = self._harris_data.transpose()
                                                          
        print self._harris_data
        self._harris_vbo = glvbo.VBO(self._harris_data)
                     
    @property
    def video(self):
        """The video to play in this Camera_View"""
        return self._video_name

    def _set_video(self, value):
#        INFO("called")
            
        INFO("loading " + value)
        self._video_name = value

        # Set the tooltip
        self.setToolTip(self._video_name)

        # Load the video from video_name  
        self._loader = holo3d.file_loader(self._video_name)
        self._loader.run()
        INFO("width={}, height={}".format(self._loader.width, self._loader.height))        
        
        self._num_pixels = self._loader.width * self._loader.height        
                
        factory = holo3d.factory_decoder(self._loader.width, 
                                         self._loader.height, 
                                         self._loader.width)
        self._color_decoder = factory.color(self._loader.file_header.color_format)
        self._depth_decoder = factory.depth(self._loader.file_header.depth_format)
        
        self._offset_times = self._model.get_offset_times(self._video_name)        
        
        self._color_buffer = np.ndarray(shape=(self._num_pixels, 3), 
                                        dtype=np.uint8)
        
        self._depth_buffer = np.ndarray(shape=(self._num_pixels, 3), 
                                        dtype=np.uint8)       
        
    def load_frame(self, time_stamp):
        if(not self._video_name):
            return
        
        """ Find the frame with the closest timestamp to time_stamp and load it
        """
#        INFO("cam=%s,time_stamp=%d" % (os.path.basename(self._video_name), time_stamp))
        frame_to_load = len(self._offset_times)-1 # By default load the last frame
        for i in range(len(self._offset_times)-1):
            prev_frame_time = self._offset_times[i].stamp
            next_frame_time = self._offset_times[i+1].stamp
#            INFO("str(prev_frame_time)={}".format(str(prev_frame_time)))
#            INFO("str(next_frame_time)={}".format(str(next_frame_time)))
            if(abs(time_stamp-prev_frame_time) <= abs(time_stamp-next_frame_time)):
                frame_to_load = i
                break
        
#        INFO("camera {}: going frame {}".format(os.path.basename(self._video_name), i))
        
        # Restart the loader
        self._loader.run()
        # Load the correct frame
        for i in range(frame_to_load+1):    
            self._loader.next_frame(self._frame)
        
        self._color_decoder.decode(self._color_buffer, 
                                   self._frame.color_buffer, 
                                   self._num_pixels * 3, 
                                   self._frame.color_buffer_size)
                                   
        self._depth_decoder.decode(self._depth_buffer, 
                                   self._frame.depth_buffer, 
                                   self._num_pixels * 3, 
                                   self._frame.depth_buffer_size)

        self._load_buffer(self._buffer_id, self._texture_id, 
                          self._color_buffer, 3)

        self._load_buffer(self._depth_buffer_id, self._depth_texture_id,
                          self._depth_buffer, 3)         
        
        self._do_refresh()
        
    def _load_buffer(self, buffer_id, texture_id, data_buf, pixel_depth):
        buf_size = self._num_pixels * pixel_depth
        # create pixel buffer object for transferring textures
        # Adapted from code at https://gist.github.com/pwuertz/6507cb95562f1db6df84
        # With changing QtGui.GLOpenGLShader... to QOpenGL.QShader...

        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer_id)
        glBufferData(GL_PIXEL_UNPACK_BUFFER, buf_size, 
                     None, GL_STREAM_DRAW)
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0)                    
                 
        # map and modify pixel buffer
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer_id)
        pbo_addr = glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY)
        # write to PBO using ctypes memmove
        ctypes.memmove(pbo_addr, data_buf.ctypes.data, buf_size)
        # write to PBO using numpy interface
        pbo_ptr = ctypes.cast(pbo_addr, ctypes.POINTER(ctypes.c_uint8))
        pbo_np = np.ctypeslib.as_array(pbo_ptr, 
                                       shape=(self._num_pixels, pixel_depth))
        pbo_np[:] = data_buf
        glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER)
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0)
        
        # create texture from pixel buffer object
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer_id)
        glBindTexture(GL_TEXTURE_2D, texture_id)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR) # GL_LINEAR_MIPMAP_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 
                     self._loader.width, self._loader.height, 
                     0, GL_RGB, GL_UNSIGNED_BYTE, None)
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0)   
        
    def initializeGL(self):
#       print "initializeGL: called"
                
        # Generate the buffer and texture       
        self._buffer_id = glGenBuffers(1)
        self._texture_id = glGenTextures(1)      
  
        self._depth_buffer_id = glGenBuffers(1)
        self._depth_texture_id = glGenTextures(1)
                
        # create a shader for coloring the texture
        shader_program = QtOpenGL.QGLShaderProgram()
        vertex_src = """
        varying vec2 vTexCoord;
        
        void main() {
            vTexCoord = gl_MultiTexCoord0;
            gl_Position = gl_Vertex;
        }
        """
        fragment_src = """
        uniform sampler2D myTexture;        
        varying vec2 vTexCoord;
                
        void main() {
            gl_FragColor = texture2D(myTexture, vTexCoord);
        }
        """
        shader_program.addShaderFromSourceCode(QtOpenGL.QGLShader.Vertex, vertex_src)
        shader_program.addShaderFromSourceCode(QtOpenGL.QGLShader.Fragment, fragment_src)
        shader_program.link()
        self._shader_program = shader_program
                
    def paintGL(self):
        glClear(GL_COLOR_BUFFER_BIT)
        
        if not self._video_name: return
#    
#        INFO("drawing camera {}".format(os.path.basename(self._video_name)))        
        
        target = QtCore.QRectF(-1, -1, 2, 2)
        self._shader_program.bind()
        
        if self._display_option == self.DISPLAY_OPTION.BLEND:
            self.drawTexture(target, self._texture_id) 
            glEnable(GL_BLEND)
            glBlendFunc(GL_DST_COLOR, GL_ZERO)
            self.drawTexture(target, self._depth_texture_id)         
            glDisable(GL_BLEND)
        elif self._display_option == self.DISPLAY_OPTION.COLOR:
            self.drawTexture(target, self._texture_id)
        elif self._display_option == self.DISPLAY_OPTION.DEPTH:
            self.drawTexture(target, self._depth_texture_id) 
        else:
            raise RuntimeError("This should never happen!!")
        # turn off the program
        glUseProgram(0)  
        
        if(self._display_harris):
            glColor(1, 1, 1)
            self._harris_vbo.bind()
            glEnableClientState(GL_VERTEX_ARRAY)
            glVertexPointer(2, GL_FLOAT, 0, self._harris_vbo)
            glDrawArrays(GL_POINTS, 0, self._harris_data.shape[0])
            glDisableClientState(GL_VERTEX_ARRAY) 
            self._harris_vbo.unbind()                       
                                                
    def resizeGL(self, w, h):
        glViewport(0, 0, w, h)
        
    def _do_refresh(self):
        # NOTE: This very weird call to both update and updateGL
        # was the only way I could force all of the camera_views to 
        # redraw properly
        self.update()  
        self.updateGL()    
        
    def display(self, option):
        self._display_option = option
        
        self._do_refresh()
       
    def project_changed(self, project_path):
        """Reloads the widget when the project changes"""
        
        self._video_name = None
        # If thise camera is out of range from the total number of cameras
        # then leave it uninitialized
        if(self._video_index >= len(self._model.files())):
            return
        
        # have to finalize my delayed initialization
        self._set_video( self._model.get_file_path(self._video_index) )
        
    def display_harris(self, flag):
        self._display_harris = flag

        self._do_refresh()        
        
        
        
        