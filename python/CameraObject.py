__author__ = 'benjamin'

import numpy as np
import cv2
from os import path

from PySide import QtCore
from holo3d_utils import INFO, ERROR
import transformations

# Define the grid colors and names
GRID_COLORS = (
    (0.4, 0.4, 0.4, 1.),    # Black
    (0.0, 0.0, 1.0, 1.),    # Blue
    (1.0, 0.0, 0.0, 1.),    # Red
    (0.0, 1.0, 0.0, 1.),    # Green

    (0.8, 0.0, 0.8, 1.)    # Purple
    #None                # No grid on the bottom
)

COLOR_NAMES = (
    "black",
    "blue",
    "red",
    "green",

    "purple"
)

class CameraObject(QtCore.QObject):

    changed_signal = QtCore.Signal()            # << Fired whenever this node changes
    show_cloud_changed = QtCore.Signal(bool)    # << Fired whenever the show_cloud flag is changed

    def __init__(self, file_path, parent=None):

        super(CameraObject, self).__init__(parent)

        self._translation = np.zeros((3,), dtype=np.float32)
        self._rotation = np.zeros((3,), dtype=np.float32)

        self._matrix = None

        # self._camera_position = None

        self._modified = True

        self._file_path = file_path

        self._intrinsic_matrix = np.identity(3, dtype=np.float32)
        self._intrinsic_modified = False

        self._focal_dists = np.zeros((2,), dtype=np.float32)
        self._offsets = np.zeros((2,), dtype=np.float32)
        self._distortion_coeffs = np.zeros((5,), dtype=np.float32)

        # Default intrinsic camera parameters
        self.set_focal_distances((524.0, 524.0))
        self.set_offsets((320.0, 240.0)) # 316.7, 238.5

        dist_coeffs = [0.2402, -0.6861, 0.0, 0.0, -0.0015] # [ 2.35054207e-01, 8.06498781e-01, 0.0, 0.0, -2.44]
        self.set_distortion_coeffs(dist_coeffs)

        self._color = 'black'

        self._dirty = True
        self._frozen = False

        self._show_cloud = True

    def file_path(self):
        return self._file_path

    def __str__(self):
        return self._file_path

    def show_cloud(self):
        return self._show_cloud

    def set_show_cloud(self, f):
        if self._show_cloud == f: return

        self._show_cloud = f

        self.show_cloud_changed.emit(f)

    def frozen(self):
        return self._frozen

    def set_frozen(self, f):
        self._frozen = f
        # Dirty is set to the opposite of frozen
        # If the node is frozen, then the translation and rotation vector should not be allowed to be changed
        # If the node is unfrozen, then the translation/rotation vecs should be updated as soon as possible
        self._dirty = not f

    def dirty(self):
        return self._dirty

    def set_dirty(self, f):
        # Cannot dirty a frozen node
        if not self._frozen:
            self._dirty = f

    def intrinsic_matrix(self):
        if self._intrinsic_modified:
            self._intrinsic_matrix[0, 0] = self._focal_dists[0]
            self._intrinsic_matrix[1, 1] = self._focal_dists[1]
            self._intrinsic_matrix[0, 2] = self._offsets[0]
            self._intrinsic_matrix[1, 2] = self._offsets[1]

            self._intrinsic_modified = False

        return self._intrinsic_matrix

    def focal_distances(self):
        return self._focal_dists

    def set_focal_distances(self, distances):
        distances = np.array(distances, dtype=np.float32).reshape((2,1))
        if np.allclose(self._focal_dists, distances):
            return

        self._focal_dists = distances
        self._intrinsic_modified = True

        self.changed_signal.emit()

    def offsets(self):
        return self._offsets

    def set_offsets(self, offsets):
        offsets = np.array(offsets, dtype=np.float32).reshape((2,1))
        if np.allclose(self._offsets, offsets):
            return

        self._offsets = offsets
        self._intrinsic_modified = True

        self.changed_signal.emit()

    def distortion_coeffs(self):
        return self._distortion_coeffs

    def set_distortion_coeffs(self, coeffs):
        coeffs = np.array(coeffs, dtype=np.float32).reshape((5,1))
        if np.allclose(self._distortion_coeffs, coeffs):
            return

        self._distortion_coeffs = coeffs
        self.changed_signal.emit()

    def color(self):
        return self._color

    def set_color(self, color):
        if self._color == color:
            return

        self._color = color
        self.changed_signal.emit()

    # Static color methods
    @staticmethod
    def all_colors():
        return COLOR_NAMES

    @staticmethod
    def color_value_by_index(index):
        return GRID_COLORS[index]

    def color_value(self):
        color_value = GRID_COLORS[0]
        for i,name in enumerate(COLOR_NAMES):
            if self._color == name:
                color_value = GRID_COLORS[i]
                break

        return color_value

    def matrix(self):
        self._recalc_transform()

        # recalc if the translation or rotation changes
        if self._modified:
            self._matrix = transformations.compose_matrix(None, None, self._rotation, self._translation)

            self._modified = False

        # Return the transpose of the matrix because that's how opengl wants it
        return self._matrix.T

    def _set_matrix(self, matrix):
        scale, shear, angles, trans, persp = transformations.decompose_matrix(matrix)
        # ignore everything except for angles and trans for now
        self.set_rotation(angles)
        self.set_translation(trans)

    def camera_position(self):
        return self.translation()

    def translation(self):
        self._recalc_transform()
        return self._translation

    def set_translation(self, vec):
        vec = np.array(vec, dtype=np.float32).reshape((3,))
        if np.allclose(vec, self._translation):
            return

        if not self.frozen():
            INFO("Setting translation on an unfrozen node!")

        self._translation = vec
        self._modified = True

        self.changed_signal.emit()

    def rotation(self):
        self._recalc_transform()
        return self._rotation

    def set_rotation(self, vec):
        vec = np.array(vec, dtype=np.float32).reshape((3,))
        if np.allclose(vec, self._rotation):
            return

        if not self.frozen():
            INFO("Setting rotation on an unfrozen node!")

        self._rotation = vec
        self._modified = True

        self.changed_signal.emit()

    def serialize(self):
        obj = {}

        obj['matrix'] = self._matrix.tolist()

        obj['focal_distances'] = self._focal_dists.tolist()
        obj['offsets'] = self._offsets.tolist()
        obj['distortion_coeffs'] = self._distortion_coeffs.tolist()

        if self._color is not None:
            obj['color'] = self._color

        return obj

    def deserialize(self, obj):
        # Always set the node as frozen
        self.set_frozen(True)

        if 'rotation' in obj and 'translation' in obj:
            self._convert_vecs(obj['rotation'], obj['translation'])

        if 'matrix' in obj:
            self._set_matrix(obj['matrix'])

        if 'focal_distances' in obj:
            self.set_focal_distances(obj['focal_distances'])

        if 'offsets' in obj:
            self.set_offsets(obj['offsets'])

        if 'distortion_coeffs' in obj:
            self.set_distortion_coeffs(obj['distortion_coeffs'])

        if 'color' in obj:
            self.set_color(obj['color'])

    def _recalc_transform(self):
        # Recalc trasnlation and rotation if the node is marked as dirty
        if self._dirty:

            # Recalculate the camera orientation and position
            intrinsic_matrix = self.intrinsic_matrix()
            dist_coeffs = self.distortion_coeffs()
            frame = self.parent().current_frame(self._file_path)
            rotate_vec, translate_vec = frame.camera_position(self.parent().clipping_rect(), self.parent().grid_size(),
                                                              intrinsic_matrix, dist_coeffs,
                                                              self.color())
            self._dirty = False

            # no camera position found so don't draw
            if rotate_vec is None or translate_vec is None:
                ERROR("Failed to get a rotation vector or translation vector for camera %s!" % path.basename(self._file_path) )
                return

            self._convert_vecs(rotate_vec, translate_vec)

    def _convert_vecs(self, rot, trans):
        rot_vec = np.array(rot, dtype=np.float32).reshape((3,))
        rotate_matrix = cv2.Rodrigues(rot_vec)[0].T      # Get the rodrigues matrix from the rotation vector
        trans_vec = np.array(trans, dtype=np.float32).reshape((3,1))
        camera_position = -np.matrix(rotate_matrix) * np.matrix(trans_vec) # calculate the global camera_position

        # INFO("camera=%r, global_pos=%r"% (self.file_path(), self._camera_position))

        rot_mat = rotate_matrix     # Build a homogenous matrix from the rotation matrix and translation vector
        rot_mat = np.column_stack([rot_mat, camera_position])
        rot_mat = np.row_stack([rot_mat, (0,0,0, 1)])

        self._set_matrix(rot_mat)

