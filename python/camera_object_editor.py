__author__ = 'benjamin'

from PySide import QtCore, QtGui, QtUiTools
import numpy as np
import math

class CameraNodeEditor(QtGui.QWidget):
    def __init__(self, parent=None):
        super(CameraNodeEditor, self).__init__(parent)

        self._current_node = None
        self._setting = False

        ui_loader = QtUiTools.QUiLoader()

        file = QtCore.QFile("object_attributes_form.ui")
        file.open(QtCore.QFile.ReadOnly)
        self._object_attr_form = ui_loader.load(file, self)
        file.close()

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self._object_attr_form)
        self.setLayout(layout)

        self._pos_boxes = [None] * 3
        self._pos_boxes[0] = self._object_attr_form.x_pos
        self._pos_boxes[1] = self._object_attr_form.y_pos
        self._pos_boxes[2] = self._object_attr_form.z_pos

        self._rot_boxes = [None] * 3
        self._rot_boxes[0] = self._object_attr_form.x_rot
        self._rot_boxes[1] = self._object_attr_form.y_rot
        self._rot_boxes[2] = self._object_attr_form.z_rot

        for i in xrange(3):
            self._pos_boxes[i].valueChanged.connect(self.translation_changed)

        for i in xrange(3):
            self._rot_boxes[i].valueChanged.connect(self.rotation_changed)

        self.hide()

    _RAD_TO_DEG = 180.0 / math.pi
    _DEG_TO_RAD = math.pi / 180.0

    def edit(self, node):
        self._current_node = node

        if node is None:
            self.hide()
            return

        self._setting = True

        vec = node.translation()
        for i in xrange(3):
            self._pos_boxes[i].setValue(vec[i])

        # Get the rotation values here!!

        vec = node.rotation()
        for i in xrange(3):
            val = vec[i] * self._RAD_TO_DEG
            self._rot_boxes[i].setValue(val)

        self._setting = False

        self.show()

    def translation_changed(self):
        if self._setting: return

        # Grab the translate values
        vec = [None] * 3
        for i in xrange(3):
            vec[i] = self._pos_boxes[i].value()

        self._current_node.set_translation(vec)

    def rotation_changed(self):
        if self._setting: return
        # Grab the translate values
        vec = [None] * 3
        for i in xrange(3):
            val = self._rot_boxes[i].value() * self._DEG_TO_RAD
            vec[i] = val

        self._current_node.set_rotation(vec)
