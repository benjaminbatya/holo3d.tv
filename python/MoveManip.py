__author__ = 'benjamin'

import holo3d
import numpy as np
from PySide import QtCore, QtGui, QtOpenGL
from fusion_model import Fusion_Model
from holo3d_utils import (INFO, ERROR, enum, unproject2)
import os.path
import sys
import cv2
import numpy as np
import math

try:
    import OpenGL.GL as gl
    import OpenGL.GLU as glu
    import OpenGL.arrays.vbo as glvbo
    import OpenGL.arrays as GLArrays

except ImportError:
    app = QtGui.QApplication(sys.argv)
    QtGui.QMessageBox.critical(None, "OpenGL hellogl",
                               "PyOpenGL must be installed")
    sys.exit(1)

_NAMES = enum(X=0, Y=1, Z=2)

_AXIS = ( (1.0, 0.0, 0.0),
          (0.0, 1.0, 0.0),
          (0.0, 0.0, 1.0) )

_AXIS = np.array(_AXIS)

_AXIS_LENGTH = 10.0
_DELTA_SCALE_FACTOR = 6.0

class MoveManip:
    '''Manipulator for moving the selected object'''

    def __init__(self, widget, file_path):
        self._widget = widget
        self._file_path = file_path

        self._selected_axis = None

        self._delta = np.zeros((3,), dtype=np.float32)

        self._start_pt = None

    def draw(self, file_path):
        if(self._file_path != file_path):
            return

        if (self._start_pt is None):
            # Capture the various matrices so they can be used in the updating
            # Note: This is inefficient but it works...
            self._modelview_mat = gl.glGetDoublev(gl.GL_MODELVIEW_MATRIX)
            # INFO("modelview matrix =\n%r" % self._modelview_mat)
            self._projection_mat = gl.glGetDoublev(gl.GL_PROJECTION_MATRIX)
            #INFO("projection matrix =\n%r" % self._projection_mat)
            self._viewport = gl.glGetIntegerv(gl.GL_VIEWPORT)
            #INFO("viewport =\n%r" % self._viewport)

        self._widget.set_color((1., 0., 0., 1.,))
        if self._selected_axis is not None and np.allclose(self._selected_axis, _AXIS[_NAMES.X]):
            self._widget.set_color((1., 1., 1., 1.,))

        gl.glBegin(gl.GL_LINES)
        gl.glVertex3f(0.,  0., 0.)
        gl.glVertex3f(_AXIS_LENGTH, 0., 0.)
        gl.glEnd()

        self._widget.set_color((0., 1., 0., 1.,))
        if self._selected_axis is not None and np.allclose(self._selected_axis, _AXIS[_NAMES.Y]):
            self._widget.set_color((1., 1., 1., 1.,))

        gl.glBegin(gl.GL_LINES)
        gl.glVertex3f(0.,  0., 0.)
        gl.glVertex3f(0., _AXIS_LENGTH, 0.)
        gl.glEnd()

        self._widget.set_color((0., 0., 1., 1.,))
        if self._selected_axis is not None and np.allclose(self._selected_axis, _AXIS[_NAMES.Z]):
            self._widget.set_color((1., 1., 1., 1.,))

        gl.glBegin(gl.GL_LINES)
        gl.glVertex3f(0.,  0., 0.)
        gl.glVertex3f(0., 0., _AXIS_LENGTH)
        gl.glEnd()

    def apply_transform(self, file_path):
        '''Applies the transform in this manip'''
        if self._selected_axis is None: return

        if(self._file_path != file_path): return

        gl.glTranslatef(self._delta[0],
                        self._delta[1],
                        self._delta[2])

    def select(self):
        # Draw the final camera position

        node = self._widget._model.camera_nodes(self._file_path)
        if node is None:
            return

        rot_mat = node.matrix()

        gl.glPushMatrix()

        gl.glMultMatrixf(rot_mat)

        self.apply_transform(self._file_path)

        gl.glLoadName(_NAMES.X)
        gl.glBegin(gl.GL_LINES)
        gl.glVertex3f(0.,  0., 0.)
        gl.glVertex3f(_AXIS_LENGTH, 0., 0.)
        gl.glEnd()

        gl.glLoadName(_NAMES.Y)
        gl.glBegin(gl.GL_LINES)
        gl.glVertex3f(0.,  0., 0.)
        gl.glVertex3f(0., _AXIS_LENGTH, 0.)
        gl.glEnd()

        gl.glLoadName(_NAMES.Z)
        gl.glBegin(gl.GL_LINES)
        gl.glVertex3f(0.,  0., 0.)
        gl.glVertex3f(0., 0., _AXIS_LENGTH)
        gl.glEnd()

        gl.glPopMatrix()

    def process_best_hit(self, best_hit):
        self._selected_axis = None
        if best_hit is not None:
            # INFO("Selected axis = %r" % best_hit)
            self._selected_axis = _AXIS[best_hit]

        return self._selected_axis

    def _calcClosestPtToAxis(self, event):
        # These are in global space
        (ray_start, ray_dir) = unproject2(event.x(), event.y(),
                                          self._modelview_mat,
                                          self._projection_mat,
                                          self._viewport)

        axis = self._selected_axis

        # This is from https://bitbucket.org/bensch128/omtoolbox/src/17b03a4825f7f1cb83607ad191ebdba9065fc2bc/plug-ins/libs/BaseManip/CustomManip3D.cpp?at=master
        b = ray_dir.dot(axis)
        d = ray_dir.dot(ray_start)
        e = axis.dot(ray_start)

        det = 1.0 - b*b

        if det < 0.000001:
            # Lines are parallel, just signal an error and return the origin
            ERROR("Lines are parallel!")
            return None

        t = -(b*d - e) / det

        return (axis * t)

    def handle_press_event(self, event):
        '''Returns True if event has handled here, False otherwise'''
        if self._selected_axis is None:
            return False

        self._start_pt = self._calcClosestPtToAxis(event)

        # INFO("start pt = %r" % self._start_pt)

        return True

    def handle_move_event(self, event):
        '''Returns True if event has handled here, False otherwise'''
        if self._selected_axis is None:
            return False

        pt = self._calcClosestPtToAxis(event)

        # INFO("pt = %r" % pt)

        # NOTE: Really, I should unproject the motion into world space,
        # dot the delta with the selected axis and use the result
        self._delta = pt - self._start_pt

        return True

    def handle_release_event(self, event):
        '''Returns True if event has handled here, False otherwise'''
        if self._selected_axis is None:
            return False

        # Apply the transformation to the target camera
        node = self._widget._model.camera_nodes(self._file_path)
        trans = node.translation()
        change = self._delta
        new_trans = trans - change
        node.set_translation(new_trans)

        self._selected_axis = None

        self._delta = np.zeros((3,), dtype=np.float32)

        self._start_pt = None

        return True