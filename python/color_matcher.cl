// NOTE: the PYOPENCL_COMPILER_OUTPUT environment variable MUST be set to 1 for this to not complain
// See http://stackoverflow.com/questions/20722242/compilerwarning-with-opencl
#define INFO(_format, ...) {\
    printf((__constant char *)"\n(%u,%u,%u) ", get_global_id(0), get_group_id(0), get_local_id(0)); \
    printf((__constant char *)(_format), __VA_ARGS__); \
}

uchar4 convert(uchar4 input)
{
    uchar4 output;

    output.s0 = (input.s0 + 2*input.s1 + input.s2) / 4;
    output.s1 = input.s2 - input.s1;
    output.s2 = input.s0 - input.s1;
    output.s3 = 0;

    return output;
}

__kernel void reduce_colors(const int num_colors,
                            __global uchar4* colors,
                            __global uchar4* input,
                            __local unsigned int* local_result,
                            __global unsigned int* global_result)
{
    int gid = get_global_id(0);

    // Initialize the local results
    for(int i=0; i<num_colors; i++)
    {
        local_result[i] = 0;
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    // Do the local reduction
    uchar4 value = input[gid];
    value = convert(value);
    int min_idx;
    unsigned int min_dist = (unsigned int)(-1);
    for(int i=0; i<num_colors; i++)
    {
        uchar4 color = colors[i];
        uchar4 delta = abs_diff(value, color);
        unsigned int dist = ((unsigned int)delta.x) + delta.y + delta.z;
        if(dist < min_dist)
        {
            min_dist = dist;
            min_idx = i;
        }
    }
    atomic_inc(local_result + min_idx);

    // Clear the global results before updating them
    if(gid == 0)
    {
//        INFO("Clearing arrays, gid=%d", gid);
        for(int i=0; i<num_colors; i++)
        {
            global_result[i] = 0;
        }
    }

    barrier(CLK_GLOBAL_MEM_FENCE);

    // Update the global results
    if(get_local_id(0) == 0)
    {
//        for(int i=0; i<num_colors; i++)
//        {
//            INFO("local_result[%d] = %d", i, local_result[i]);
//        }
        for(int i=0; i<num_colors; i++)
        {
            atomic_add(global_result + i, local_result[i]);
        }
//        INFO("global_result[0]=%d", global_result[0]);
//        INFO("global_result[1]=%d", global_result[1]);
    }


}
