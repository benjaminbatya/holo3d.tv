__author__ = 'benjamin'

import numpy as np

from PySide import QtCore
from holo3d_utils import INFO, ERROR
import transformations

class VolumeObject(QtCore.QObject):

    changed_signal = QtCore.Signal()            # << Fired whenever this node changes

    def __init__(self, parent):
        super(VolumeObject, self).__init__(parent)

        # The volume of interest is denoted by an origin, a size in each deminsion, and a rotation around the Y axis
        self._origin = np.array([24, 24, 24], dtype=np.float32) # np.zeros((3,1), dtype=np.float32)
        self._size = np.ones((3,1), dtype=np.float32) * 12
        self._rotation = 0.0   # << rotation is in radians, angle around the Y axis

        self._modified = True
        self._points = [None] * self.num_points()
        self._matrix = np.identity(4, dtype=np.float32)

    def origin(self):
        return self._origin

    def set_origin(self, value):
        value = np.array(value, dtype=np.float32).reshape(3,1)
        if np.allclose(self._origin, value):
            return

        self._origin = value
        self._modified = True
        self.changed_signal.emit()

    def size(self):
        return self._size

    def set_size(self, value):
        value = np.array(value, dtype=np.float32).reshape(3,1)
        if np.allclose(self._size, value):
            return

        self._size = value
        self._modified = True
        self.changed_signal.emit()

    def rotation(self):
        return self._rotation

    def set_rotation(self, value):
        value = float(value)
        if self._rotation == value:
            return

        self._rotation = value
        self._modified = True
        self.changed_signal.emit()

    def num_points(self):
        return 8

    def _recalc(self):
        '''Calculates the position of each of the cuboid's vertices'''

        if not self._modified:
            return

        rot_matrix = np.array([[np.cos(self._rotation), -np.sin(self._rotation)],
                               [np.sin(self._rotation),  np.cos(self._rotation)]]).reshape(2,2)

        # Calc min x, min y, min z
        for i in xrange(2):
            x = (float(i)-0.5) * self._size[0] + self._origin[0]
            for j in xrange(2):
                y = (float(j)-0.5) * self._size[1] + self._origin[1]
                for k in xrange(2):
                    z = (float(k)-0.5) * self._size[2] + self._origin[2]
                    pt = np.zeros((3,1), dtype=np.float32)
                    pt[0] = rot_matrix[0,0]*x + rot_matrix[0,1]*z
                    pt[1] = y
                    pt[2] = rot_matrix[1,0]*x + rot_matrix[1,1]*z

                    self._points[i + j*2 + k*4] = pt

        # Calculate the transformation matrix of the volume
        # Scale
        self._matrix = np.identity(4, dtype=np.float32)
        self._matrix[0,0] = self._size[0] / 2
        self._matrix[1,1] = self._size[1] / 2
        self._matrix[2,2] = self._size[2] / 2

        # Rotate
        rot_mat = transformations.rotation_matrix(self._rotation, (0, 1, 0))
        self._matrix = np.dot(rot_mat, self._matrix)

        # Translate
        self._matrix[0,3] = self._origin[0]
        self._matrix[1,3] = self._origin[1]
        self._matrix[2,3] = self._origin[2]

        # Invert
        self._matrix = np.linalg.inv(self._matrix)

        self._modified = False

    def point(self, index):
        self._recalc()

        return self._points[index]

    # def matrix(self):
    #     self._recalc()
    #
    #     return self._matrix()

    def matrix(self):
        self._recalc()

        return self._matrix

    def serialize(self):
        obj = {}
        obj['origin'] = self._origin.tolist()
        obj['size'] = self._size.tolist()
        obj['rotation'] = self._rotation

        return obj

    def deserialize(self, obj):
        if 'origin' in obj:
            self.set_origin(obj['origin'])

        if 'size' in obj:
            self.set_size(obj['size'])

        if 'rotation' in obj:
            self.set_rotation(obj['rotation'])
