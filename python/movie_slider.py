__author__ = 'benjamin'


from PySide import QtCore, QtGui, QtUiTools

from holo3d_utils import *

class movie_slider(QtGui.QSlider):
    TIME_FREQUENCY = 10000

    time_changed = QtCore.Signal(long)

    def __init__(self, parent, model):
        QtGui.QSlider.__init__(self, parent)

        self._model = model

        self._painter = QtGui.QPainter()
        self.setOrientation(QtCore.Qt.Horizontal)
        self.set_time_info( None, None )

        self._num_ticks = 20

        # Make the tick
        self._tick_path = QtGui.QPainterPath()
        self._tick_path.moveTo(-1, -1)
        self._tick_path.lineTo(+1, -1)
        self._tick_path.lineTo(+1, +1)
        self._tick_path.lineTo(-1, +1)

        self._top_marker = QtGui.QPainterPath()
        self._top_marker.moveTo(-3, 0)
        self._top_marker.lineTo(+3, 0)
        self._top_marker.lineTo(0, -2)

        self._bottom_marker = QtGui.QPainterPath()
        self._top_marker.moveTo(-3, 0)
        self._top_marker.lineTo(+3, 0)
        self._top_marker.lineTo(0, +2)

        self.valueChanged.connect(self._value_changed)

    # def sliderChange(self, change):
    #     INFO("Called: change=%s" % str(change))

    def calc_pos(self, value):
        if(value <= self.minimum()): return 0
        elif(value >= self.maximum()): return self.width()
        else: return (value - self.minimum()) * (self.width()) / (self.maximum() - self.minimum())

    def paintEvent(self, e):
        enabled = self.isEnabled()
        color = None

        # INFO("silderPosition = %d" % self.sliderPosition())
        self._painter.begin(self)

        self._painter.eraseRect(self.rect())

        # Draw the ticks
        tick_interval = float(self.width()) / self._num_ticks
        # tick_interval = self.calc_pos(self.tickInterval())
        self._painter.setPen(QtCore.Qt.darkGray)
        for tick in range(self._num_ticks+1):
            self._painter.drawLine(tick*tick_interval, 0, tick*tick_interval, self.height())

        colors = {}
        if self._time_codes is not None:
            for vid in self._time_codes:
                if enabled:
                    gl_color = self._model.camera_nodes(vid).color_value()
                    color = QtGui.QColor()
                    color.setRed(gl_color[0] * 255)
                    color.setGreen(gl_color[1] * 255)
                    color.setBlue(gl_color[2] * 255)
                else:
                    color = QtCore.Qt.lightGray
                colors[vid] = color

            # Draw the time codes
            y_pos = 10
            for vid in self._time_codes:
                #INFO("vid=%s" % vid)
                for code in self._time_codes[vid]:
                    # Figure out where to draw the tick
                    x_pos = self.calc_pos(code.stamp / self.TIME_FREQUENCY - self._min_time)

                    path = self._tick_path.translated(x_pos, y_pos)
                    self._painter.fillPath(path, QtGui.QBrush(colors[vid]))

                y_pos += 4

        # Draw the slider
        slider_x = self.calc_pos(self.value())

        # path = self._bottom_marker.translated(slider_x, 4)
        # self._painter.fillPath(path, QtGui.QBrush(QtCore.Qt.gray))

        path = self._top_marker.translated(slider_x, self.height()-20)
        self._painter.fillPath(path, QtGui.QBrush(QtCore.Qt.gray))

        color = QtCore.Qt.blue
        if not enabled: color = QtCore.Qt.gray
        self._painter.setPen(color)
        self._painter.drawLine(slider_x, 0, slider_x, self.height())

        self._painter.end()

    def set_time(self, time):
        value = (time / self.TIME_FREQUENCY) - self._min_time

        if value == self.value():
            return

        self.setValue(value)
        self.update()

    def _value_changed(self):
        # INFO("CALLED")
        self.setToolTip("Current Time = %d" % self.value())

        value = self.value()
        new_time = (value + self._min_time) * self.TIME_FREQUENCY

        self.time_changed.emit( new_time )

    def set_time_info(self, codes, colors):
        self._time_codes = codes
        self._time_colors = colors

        if(self._time_codes is None):
            self._min_time = 0
            self._max_time = 100

        else:
            # calculate the min and max of all the time_codes
            mins = []
            maxs = []
            for path, times in codes.items():
                stamps = [ (st.stamp / self.TIME_FREQUENCY) for st in times ]
                mins.append(min( stamps ) )
                maxs.append(max( stamps ) )

            # print mins
            # print maxs

            min_time = min(mins)
            max_time = max(maxs)

            self._min_time = min_time
            self._max_time = max_time - min_time

        self.setMinimum(0)
        self.setMaximum(self._max_time)
        self.setSingleStep(self._max_time / 100)
        self.setPageStep(self._max_time / 20)
