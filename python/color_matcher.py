__author__ = 'benjamin'

from holo3d_utils import INFO
import numpy as np
import sys
import os

import pyopencl as cl
import pyopencl.array as cl_array

COLOR_NAMES = ("red", "green", "blue", "black", "white", "purple")
COLOR_VALUES = ((255,0,0), (0,255,0), (0,0,255), (0,0,0), (255,255,255), (204, 0, 204))
COLOR_FLOAT_VALUES = []

# From http://www.easyrgb.com/index.php?X=MATH&H=02#text2 RGB->XYZ
def convert_to_float(RGB):
    return (float(RGB[0]) / 255, float(RGB[1]) / 255, float(RGB[2]) / 255, 1.0)

for i in xrange(len(COLOR_VALUES)):
    COLOR_FLOAT_VALUES.append(convert_to_float(COLOR_VALUES[i]))

def RGBtoXYZ(RGB):
    (r, g, b, a) = convert_to_float(RGB)

    if(r > 0.04045): r = ((r + 0.055) / 1.055) ** 2.4
    else:            r = r / 12.92
    if(g > 0.04045): g = ((g + 0.055) / 1.055) ** 2.4
    else:            g = g / 12.92
    if(b > 0.04045): b = ((b + 0.055) / 1.055) ** 2.4
    else:            b = b / 12.92

    r *= 100.0
    g *= 100.0
    b *= 100.0

    # Observer. = 2 degrees, Illuminant = D65
    X = r * 0.4124 + g * 0.3576 + b * 0.1805
    Y = r * 0.2126 + g * 0.7152 + b * 0.0722
    Z = r * 0.0193 + g * 0.1192 + b * 0.9505

    return (X, Y, Z)

def XYZtoCIELab(XYZ):
    # Observer= 2 degrees, Illuminant= D65
    x = XYZ[0] / 95.047
    y = XYZ[1] / 100.000
    z = XYZ[2] / 108.883

    if (x > 0.008856): x = x ** ( 1.0 / 3.0 )
    else:              x = ( 7.787 * x ) + (16.0 / 116.0 )
    if (y > 0.008856): y = y ** ( 1.0 / 3.0 )
    else:              y = ( 7.787 * y ) + ( 16.0 / 116.0 )
    if (z > 0.008856): z = z ** ( 1.0 / 3.0 )
    else:              z = ( 7.787 * z ) + ( 16.0 / 116.0 )

    L = ( 116.0 * y ) - 16.0
    a = 500.0 * ( x - y )
    b = 200.0 * ( y - z )

    return (L, a, b)


CIELab_VALUES = []
for color in COLOR_VALUES:
    CIELab_VALUES.append(XYZtoCIELab(RGBtoXYZ(color)))

class _Base:

    def _get_match(self, histogram):
        INFO(str(histogram))

        best_match = None
        max_value = 0
        for i in range(3):
            if(histogram[i] > max_value):
                best_match = i
                max_value = histogram[i]

        # HACK: Check if the number of black pixels more then 5x the max_value
        # If so, set best match to black
        # NOTE: Should be using calibration shapes instead of colors.
        # They would be immune to changes in light intensity
        if(best_match is None):
            best_match = 3

        return COLOR_NAMES[best_match], COLOR_VALUES[best_match]

class Naive(_Base):
    def calc(self, data):
        # histogram = np.zeros(len(CIELab_VALUES), dtype=np.int32)
        # tmp = data.reshape((data.shape[0]*data.shape[1], 3)).astype(np.int32)
        # for rgb in tmp:
        #     pixel = XYZtoCIELab(RGBtoXYZ(rgb))
        #     min_idx = None
        #     min_dist = sys.maxint
        #
        #     for i in xrange(len(CIELab_VALUES)-1):
        #         color = CIELab_VALUES[i]
        #         # dist = abs(pixel[1] - color[1]) + abs(pixel[2] - color[2]) # Ignore the luminacy values
        #         dist = abs(pixel[0] - color[0]) + abs(pixel[1] -color[1]) + abs(pixel[2] - color[2])
        #         if dist < min_dist:
        #             min_dist = dist
        #             min_idx = i
        #
        #     histogram[min_idx] += 1
        #
        # return self._get_match(histogram)

        ## NOTE: temprary hack!
        color = 0
        return COLOR_NAMES[color], COLOR_VALUES[color]

class OpenCL(_Base):
    def __init__(self):
        global CIELab_VALUES

        # Initialize all of the needed
        self._ctx = cl.create_some_context()
        self._queue = cl.CommandQueue(self._ctx)

        f = open("color_matcher.cl", 'r')
        fstr = "".join(f.readlines())
        f.close()
        self._program = cl.Program(self._ctx, fstr).build()

        device = self._ctx.devices[0]
        self._work_group_size = self._program.reduce_colors.get_work_group_info(cl.kernel_work_group_info.WORK_GROUP_SIZE, device)

        self._mf = cl.mem_flags

        # allocate the constants
        self._colors = np.array(CIELab_VALUES, dtype=np.uint8)
        self._colors = np.insert(self._colors, 3, 0, axis=1)
        INFO("Colors = \n%r" % self._colors)
        self._colors_buf = cl.Buffer(self._ctx, self._mf.READ_ONLY | self._mf.COPY_HOST_PTR, hostbuf=self._colors)
        # cl.enqueue_write_buffer(queue, colors_buf, colors).wait()

        self._result_np = np.zeros(self._colors.shape[0], dtype=np.uint32)
        # Allocate the local results buffer
        self._local_buf = cl.LocalMemory(self._result_np.nbytes)
        # Allocate the global results buffer
        self._result_buf = cl.Buffer(self._ctx, self._mf.WRITE_ONLY, self._result_np.nbytes)

    def calc(self, data):
        # Make each color value into a char4
        tmp = data.reshape((data.shape[0]*data.shape[1], 3))
        tmp = np.insert(tmp, 3, 0, axis=1)
        # print tmp[0]

        # allocate the input buffer
        in_buf = cl.Buffer(self._ctx, self._mf.READ_ONLY | self._mf.COPY_HOST_PTR, hostbuf=tmp)

        self._program.reduce_colors(self._queue, (tmp.shape[0],), (self._work_group_size,),
                              np.int32(self._colors.shape[0]), self._colors_buf,
                              in_buf, self._local_buf, self._result_buf)

        cl.enqueue_read_buffer(self._queue, self._result_buf, self._result_np)
        # queue.finish()

        return self._get_match(self._result_np)
