__author__ = 'benjamin'

import numpy as np

class FloorModel:
    def __init__(self, mean=np.zeros((3,)), normal=np.zeros((3,)), d=0, dist_mean=0, dist_std=0):
        self.set_mean(mean)
        self.set_normal(normal)
        self.set_d(d)
        self.set_dist_mean(dist_mean)
        self.set_dist_std(dist_std)

    def set_mean(self, val):
        self._mean = np.array(val, dtype=np.float32).reshape(3,)

    def mean(self):
        return self._mean.copy()

    def set_normal(self, val):
        self._normal = np.array(val, dtype=np.float32).reshape(3,)
        norm = np.linalg.norm(self._normal)
        if not np.isclose(norm, 0.0):
            self._normal /= norm

    def normal(self):
        return self._normal.copy()

    def set_d(self, val):
        self._d = float(val)

    def d(self):
        return self._d

    def set_dist_mean(self, val):
        self._dist_mean = float(val)

    def dist_mean(self):
        return self._dist_mean

    def set_dist_std(self, val):
        self._dist_std = float(val)

    def dist_std(self):
        return self._dist_std

    def serialize(self):
        obj = {}
        obj['mean'] = self._mean.tolist()
        obj['normal'] = self._normal.tolist()
        obj['d'] = self._d
        obj['dist_mean'] = self._dist_mean
        obj['dist_std'] = self._dist_std

        return obj

    def deserialize(self, obj):
        if 'mean' in obj:
            self.set_mean(obj['mean'])

        if 'normal' in obj:
            self.set_normal(obj['normal'])

        if 'plane' in obj:
            self.set_normal(obj['plane'])

        if 'd' in obj:
            self.set_d(obj['d'])

        if 'dist_mean' in obj:
            self.set_dist_mean(obj['dist_mean'])

        if 'dist_std' in obj:
            self.set_dist_std(obj['dist_std'])

