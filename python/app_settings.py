__author__ = 'benjamin'

from PySide import QtCore
from holo3d_utils import INFO, ERROR
import os
import yaml

SETTINGS_FILE = "app_settings.yaml"

class AppSettings(QtCore.QObject):

    recent_projects_changed = QtCore.Signal()

    def __init__(self, parent):
        super(AppSettings, self).__init__(parent)

        self._recent_projects = []

        self.load_app_settings()

    def app_settings_file(self):
        # Load the app_settings from local directory
        curr_dir = os.getcwd()
        file_path = curr_dir + os.sep + SETTINGS_FILE
        return file_path

    def load_app_settings(self):
        file_path = self.app_settings_file()
        if not os.path.isfile(file_path):
            ERROR("Setting file '%s' does not exist!" % file_path)
            return

        stream = file(file_path, 'r')
        data_to_deserialize = yaml.load(stream)
        stream.close()

        if 'recent_files' in data_to_deserialize:
            self._recent_projects = data_to_deserialize['recent_files']

        self.recent_projects_changed.emit()

    def save_app_settings(self):
        data_to_serialize = {}

        data_to_serialize['recent_files'] = self._recent_projects

        file_path = self.app_settings_file()
        stream = file(file_path, 'w')
        yaml.dump(data_to_serialize, stream)
        stream.close()

        INFO("Saved app settings to %s" % file_path)

    def recent_projects(self):
        return self._recent_projects

    def add_recent_project(self, project):
        project = str(project)
        projects = self.recent_projects()
        projects = [project] + projects

        from collections import OrderedDict
        self._recent_projects = list(OrderedDict.fromkeys(projects))

        self.save_app_settings()

        self.recent_projects_changed.emit()
