
// NOTE: the PYOPENCL_COMPILER_OUTPUT environment variable MUST be set to 1 for this to not complain
// See http://stackoverflow.com/questions/20722242/compilerwarning-with-opencl
// I have a idea that printf doesn't work well if there is more the one context initialized at the same time
#define INFO(_format, ...) {\
    printf((__constant char *)"\n(%u,%u,%u) ", get_global_id(0), get_group_id(0), get_local_id(0)); \
    printf((__constant char *)(_format), __VA_ARGS__); \
    printf((__constant char *)"\n"); \
}

// Values from "Consumer Depth Cameras for Computer Vision" book
#define DEPTH_TO_M 0.001f
#define M_TO_INCH 39.3701f
#define DEPTH_TO_INCH DEPTH_TO_M * M_TO_INCH
#define SCL 1.0f / 524.0f

#define K1 0.2402
#define K2 -0.6861
#define K3 -0.0015
#define P1 0.0
#define P2 0.0

__kernel void generate_depth(   __global uchar* in_color,
                                __global ushort* in_depth,
                                __constant float4* in_matrix,
                                __constant uint* control_vals,
                                __global float4* out_point,
                                __global uchar4* out_color,
                                __global float4* out_debug)
{
    int width = get_global_size(0);
    int height = get_global_size(1);

    int x_idx = get_global_id(0);
    int y_idx = get_global_id(1);

    int idx = y_idx * width + x_idx;

    int max_idx = control_vals[0];
    int idx_start = control_vals[1];

    int out_idx = idx + idx_start;

    // Don't go over the maximum just in case
    if(out_idx >= max_idx)
    {
        return;
    }

    // Calculate the xyz position of the point
    int center_x = width / 2;
    int center_y = height / 2;

    unsigned short depth = in_depth[idx];

    float x = x_idx - center_x;
    float y = y_idx - center_y;
    float z = DEPTH_TO_INCH * depth;

    // The radial and tangential correction factors from the book don't seem to work right now
    float x_corrected = x;
    float y_corrected = y;

    // Radial distortion correction
    // From http://docs.opencv.org/doc/tutorials/calib3d/camera_calibration/camera_calibration.html
    // r2 is r^2
    // float r2 = x*x + y*y;

    // float x_corrected = x * (1.0 + K1 * r2 + K2 * r2 * r2 + K3 * r2 * r2 * r2);
    // float y_corrected = y * (1.0 + K1 * r2 + K2 * r2 * r2 + K3 * r2 * r2 * r2);

    // Note: this is a slightly faster polynomial expansion version
    // float x_corrected = x * (1.0 + (K1 + (K2 + K3 * r2) * r2) * r2);
    // float y_corrected = y * (1.0 + (K1 + (K2 + K3 * r2) * r2) * r2);

    // Tangential distortion correction
    // x_corrected += 2.0 * P1 * x * y + P2 * (r2 + 2.0*x*x);
    // y_corrected += P1 * (r2 + 2.0*x*y) + 2.0 * P2 * x * y;

    x = x_corrected * z * SCL;  // NOTE: this is supposed to be -1 * (x_idx - ...) but it is a mistake
    y = y_corrected * z * SCL;

    float4 temp_value = (float4)(x, y, z, 1.0f);

//    out_point[out_idx] = temp_value;

    // Transform the point by in_matrix and normalize
    float4 value;
    float w = dot(in_matrix[3], temp_value);
    value.s0 = dot(in_matrix[0], temp_value) / w;
    value.s1 = dot(in_matrix[1], temp_value) / w;
    value.s2 = dot(in_matrix[2], temp_value) / w;
    value.s3 = 1.0f;

    out_point[out_idx] = value;

    // Generate the color
    // Use the out_color alpha value to indicate if it is filtered or not (0=filtered out, 255=left in)
    uint color_idx = idx*3;

    uchar r = in_color[color_idx+0];
    uchar g = in_color[color_idx+1];
    uchar b = in_color[color_idx+2];

    uchar a = 255;

    out_color[out_idx] = (uchar4)(r, g, b, a);
}

__kernel void copy_points(  __global float4* in_points,
                            __global uchar4* in_colors,
                            __global float4* out_points,
                            __global uchar4* out_colors)
{
    uint gid = get_global_id(0);

    out_points[gid] = in_points[gid];
    out_colors[gid] = in_colors[gid];
}

__kernel void volume_filter_count(  __global float4* in_points,
                                    __constant float4* volume,
                                    const uint num_points,
                                    __global uint* out_counts)
{
    uint gid = get_global_id(0);

    uint pt_idx = gid*num_points;
    uint count = 0;
    for(int i=0; i<num_points; i++, pt_idx++)
    {
        float4 value = in_points[pt_idx];

        // Check for filtering
        float w = dot(volume[3], value);
        float x = dot(volume[0], value) / w;
        float y = dot(volume[1], value) / w;
        float z = dot(volume[2], value) / w;

        if(-1.0 <= x && x <= 1.0 &&
           -1.0 <= y && y <= 1.0 &&
           -1.0 <= z && z <= 1.0 )
        {
            count++;

            // Mark the in_point so we know this needs to be copied
            value.s3 = 0.0;
            in_points[pt_idx] = value;
        }
    }

    out_counts[gid] = count;
}

typedef struct
{
    float4 mean;
    float4 normal;
    float dist_mean;
    float dist_std;
    uint  num_points;
} FloorFilterParams;

__kernel void floor_filter_count(  __global float4* in_points,
                                    __constant float4* volume,
                                    __constant FloorFilterParams* params,
                                    __global uint* out_counts)
{
    uint gid = get_global_id(0);

//    if(gid == 0)
//    {
//        INFO("mean=(%f,%f,%f), normal = (%f,%f,%f,%f), dist_mean=%f, dist_std=%f",
//             params->mean.s0,
//             params->mean.s1,
//             params->mean.s2,
//
//             params->normal.s0,
//             params->normal.s1,
//             params->normal.s2,
//             params->normal.s3,
//
//             params->dist_mean,
//             params->dist_std);
//    }

    uint pt_idx = gid*params->num_points;
    uint count = 0;
    for(int i=0; i<params->num_points; i++, pt_idx++)
    {
        float4 point = in_points[pt_idx];

        // Check for filtering
        float w = dot(volume[3], point);
        float x = dot(volume[0], point) / w;
        float y = dot(volume[1], point) / w;
        float z = dot(volume[2], point) / w;

        if(-1.0 <= x && x <= 1.0 &&
           -1.0 <= y && y <= 1.0 &&
           -1.0 <= z && z <= 1.0 )
        {
            // Check the distance of the point from the plane
            // note that this doesn't subtract the mean because
            // we are using d instead as part of the dot product
            float4 test = point;
            test.s3 = 1.0;
            float dist = fabs(dot(params->normal, test));

            // Remove all of the points within
            if(dist > 2.0f*params->dist_std)
            {
                count++;

                // Mark the in_point so we know this needs to be copied
                point.s3 = 0.0;
                in_points[pt_idx] = point;
            }
        }
    }

    out_counts[gid] = count;
}

// NOTE: Might be able to remove this duplicate volume check by marking in_points above
__kernel void volume_filter_copy(   __global float4* in_points,
                                    __global uchar4* in_colors,
                                    __global uint* offsets,
                                    const uint num_points,
                                    __global float4* out_points,
                                    __global uchar4* out_colors)
{
    uint gid = get_global_id(0);

    uint offset = offsets[gid];

    uint pt_idx = gid*num_points;

    for(int i=0; i<num_points; i++, pt_idx++)
    {
        float4 value = in_points[pt_idx];

        // Check for filtering
        if(value.s3 == 0.0)
        {
            // Restore the value
            value.s3 = 1.0;
            in_points[pt_idx] = value;

            // Copy the point and the color
            out_points[offset] = value;
            out_colors[offset] = in_colors[pt_idx];
            offset++;
        }
    }
}


// Note: maybe figure out how to use vector operands to process partial_sum faster
__kernel void num_valid_points( __global uchar4* in_colors,
                                __local  uint* partial_sum,
                                __global uint* out_count)
{
    int gid = get_global_id(0);
    int lid = get_local_id(0);

    int global_size = get_global_size(0);
    int group_size = get_local_size(0);

    uchar4 color = in_colors[gid];

    if(color.s3 == 0)
    {
        partial_sum[lid] = 0;
    } else
    {

        partial_sum[lid] = 1;
    }

//    INFO("Got partial_sum = %d", partial_sum[lid]);

    barrier(CLK_LOCAL_MEM_FENCE);

    // Here's the standard reduction algorithm from
    // "OpenCL in Action" p. 229. Don't need to try to parallelize multiple work-groups
    for(int i = group_size/2; i>0; i >>= 1)
    {
        if(lid < i && (gid+i) < global_size) // Make sure that the invalid indices are not summed up
        {
            partial_sum[lid] += partial_sum[lid + i];
        }
//        INFO("i=%d, partial_sum = %d", i, partial_sum[lid]);

        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if(lid == 0)
    {
        // We only need to store (the total number of points rounded up to the closest multiple of group_size) / group_size
        // I think this will work
        uint idx = gid / group_size;
        out_count[idx] = partial_sum[lid];

//        INFO("num_valid_points: Hello %s", "world");
    }
}

// The math follows http://www.had2know.com/academics/least-squares-plane-regression.html
// expect that I am switching y and z. I'm assuming y is expressed in terms of x and z so
// that the plane's normal can point up
__kernel void plane_fit(__global float4* in_points,
                        __global uchar4* in_colors,
                        __local double8* partial_values,
                        __global double8* out_values)
{
    int gid = get_global_id(0);
    int lid = get_local_id(0);

    int global_size = get_global_size(0);
    int group_size = get_local_size(0);

    uchar4 color = in_colors[gid];

    if(color.s3 == 0)
    {
        partial_values[lid] = (double8)(0.0);
    } else
    {
        float4 pt = in_points[gid];

        double x  = pt.s0;
        double y  = pt.s1;
        double z  = pt.s2;

        double x2 = x * x;
        double z2 = z * z;

        double xy = x * y;
        double xz = x * z;
        double yz = y * z;

        partial_values[lid] = (double8)(x, y, z, x2, z2, xy, xz, yz);
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    // Here's the standard reduction algorithm from
    // "OpenCL in Action" p. 229. Don't need to try to parallelize multiple work-groups
    for(int i = group_size/2; i>0; i >>= 1)
    {
        if(lid < i && (gid+i) < global_size) // Make sure that the invalid indices are not summed up
        {
            partial_values[lid] += partial_values[lid + i];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if(lid == 0)
    {
        // We only need to store (the total number of points rounded up to the closest multiple of group_size) / group_size
        // I think this will work
        uint idx = gid / group_size;
        out_values[idx] = partial_values[lid];
    }
}

// This calculates the mean and of the points
__kernel void points_mean(  __global float4* in_points,
                            __global uchar4* in_colors,
                            __local float4* partial_means,
                            __global float4* out_means)
{
    int gid = get_global_id(0);
    int lid = get_local_id(0);

    int group_size = get_local_size(0);

    uchar4 color = in_colors[gid];

    if(color.s3 == 0)
    {
        partial_means[lid] = (float4)(0.0f);
    } else
    {
        float4 pt = in_points[gid];

        double x  = pt.s0;
        double y  = pt.s1;
        double z  = pt.s2;

        partial_means[lid] = (float4)(x, y, z, 0.0f);
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    // Here's the standard reduction algorithm from
    // "OpenCL in Action" p. 229. Don't need to try to parallelize multiple work-groups
    for(int i = group_size/2; i>0; i >>= 1)
    {
        if(lid < i)
        {
            partial_means[lid] += partial_means[lid + i];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if(lid == 0)
    {
        // We only need to store (the total number of points rounded up to the closest multiple of group_size) / group_size
        // I think this will work
        uint idx = gid / group_size;
        out_means[idx] = partial_means[lid];
    }
}