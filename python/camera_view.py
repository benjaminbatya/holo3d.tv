# -*- coding: utf-8 -*-
"""
Created on Tue Jan 20 11:44:51 2015

@author: benjamin
"""
import numpy as np
from PySide import QtCore, QtGui, QtUiTools

import os.path
import sys
import cv2
import cv
import math

import holo3d
from time import time

from fusion_model import Fusion_Model
from holo3d_utils import *
import color_matcher
import intrinsic_calibration_dialog
import view_calculator

from CameraObject import CameraObject

# from matplotlib import pyplot as plt

class Camera_View(QtGui.QWidget):
    """Displays a frame from a movie in a holo directory/project"""
    
    DISPLAY_OPTION = enum('BLEND', 'COLOR', 'DEPTH')

    PIXEL_DEPTH = 3
    
    def __init__(self, parent, index, model):
        super(Camera_View, self).__init__(parent)

        self._model = model
        self._video_name = None
        self._video_index = index

        self._display_option = self.DISPLAY_OPTION.BLEND

        # NOTE: maybe move this to a custom dialog class
        self._dialog_image = None
        self._dialog_rect = None
        self._corners = None

        self._painter = QtGui.QPainter()

        # Setup the image processing dialog
        ui_loader = QtUiTools.QUiLoader()
        file = QtCore.QFile("camera_view_dialog.ui")
        file.open(QtCore.QFile.ReadOnly)
        self._dialog = ui_loader.load(file, self)
        file.close()

        # It has to be modeless
        self._dialog.setModal(False)

        self._dialog.setWindowTitle("NOT SET YET!")

        self._dialog._widget = ImageWidget(self._dialog.image_frame)
        layout = QtGui.QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self._dialog._widget)
        self._dialog.image_frame.setLayout(layout)
        self._dialog.show()
        self._dialog.setVisible(False)

        self._dialog._widget.resizeEvent = self._on_dialog_resize
        ## End setting up dialog

        ## Setup the grid color selector
        file = QtCore.QFile("grid_color_selector.ui")
        file.open(QtCore.QFile.ReadOnly)
        self._grid_color_selector = ui_loader.load(file, self)
        file.close()

        ## Populate it with the color names
        for name in CameraObject.all_colors():
            item = QtGui.QListWidgetItem()
            item.setText(name)
            item.color_name = name
            self._grid_color_selector.color_list.addItem(item)

        self._grid_color_selector.botton_box.accepted.connect(self._grid_color_selector.accept)
        ## End grid color selector

        ## Setup the right-click context menu
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self._show_context_menu)
        ## End context menu

        # Not needed anymore
        # self._color_mapper = color_matcher.Naive()

    def base_name(self):
        return os.path.basename(self._video_name)

    @property
    def video(self):
        """The video to play in this Camera_View"""
        return self._video_name

    def _set_video(self, value):
#        INFO("called")

        INFO("loading " + value)
        self._video_name = value

        # Set the tooltip
        self.setToolTip(self._video_name)

        self._color_image = None
        self._depth_image = None

        self._widget = None

        self._dialog.setWindowTitle(self.base_name())

    def load_frame(self):

        if(not self._video_name):
            return

        # INFO("video_name = %r" % self._video_name)

        frame = self._model.current_frame(self._video_name)

        # Get the new color and depth image
        color_buffer = frame.color_buffer()
        self._color_image = self._convert_image(color_buffer)
        depth_buffer = frame.rgb_depth_buffer()
        self._depth_image = self._convert_image(depth_buffer)

        ## Update the dialog widget
        # Set the image to draw in the dialog
        self._dialog_image = self._color_image
        self._dialog_rect = self._model.clipping_rect()
        self._corners, self._corners_bbox = frame.corners(self._model.clipping_rect(), self._model.grid_size())

        # node = self._model.camera_nodes(self._video_name)
        # self._color_name = node.color()

        self._on_dialog_resize()
        ## end updating the dialog widget

        # depth_buffer = frame.depth_buffer()
        # depth_buffer = depth_buffer.reshape(depth_buffer.shape[0]).astype(int, copy=False)
        #
        # max_delta = 5000
        #
        # bins = np.zeros(shape=(max_delta*2,), dtype=int)
        # for y in xrange(frame.height):
        #     offset = y*frame.width
        #
        #     # # Calculate the delta
        #     # prev = int(0)
        #     # for x in xrange(frame.width):
        #     #     val = int(depth_buffer[x + offset][0])
        #     #     line[x] = val - prev
        #     #     prev = val
        #     line = depth_buffer[offset:offset+frame.width]
        #     prev_line = np.insert(depth_buffer[offset:offset+frame.width-1], 0, 0)
        #     line -= prev_line
        #
        #     # # Check the run of the first line
        #     # set = None
        #     # start = None
        #     # for x in xrange(frame.width):
        #     #     val = line[x]
        #     #     if set is None:
        #     #         set = val
        #     #         start = x
        #     #     elif set != val:
        #     #         print "y=%r, Run of %r from %r to %r" %(y, set, start, x)
        #     #         set = val
        #     #         start = x
        #     #
        #     # print "y=%r, Run of %r from %r to %r" %(y, set, start, frame.width-1)
        #
        #     line += max_delta
        #
        #     local_bins = np.bincount(line)
        #     local_size = len(local_bins)
        #     global_size = len(bins)
        #     # if local_size > global_size:
        #     #     num = local_size - global_size
        #     #     to_add = np.zeros(shape=(num,), dtype=int)
        #     #     bins = np.append(bins, to_add)
        #     # elif
        #     if local_size < global_size:
        #         num = global_size - local_size
        #         to_add = np.zeros(shape=(num,), dtype=int)
        #         local_bins = np.append(local_bins, to_add)
        #
        #     bins += local_bins
        #
        # count = 0
        # total = 0
        # for i in xrange(bins.shape[0]):
        #     if bins[i] > 0:
        #         # print "value=%r, count=%r" % (i-max_delta, bins[i])
        #         count += 1
        #         total += bins[i]
        #
        # print "Count=%r, total=%r, len(bins)=%r" % (count, total, len(bins))

        # bins = np.bincount(depth_buffer.flatten())
        #
        # count = 0
        # total = 0
        # for i in xrange(bins.shape[0]):
        #     if bins[i] > 0:
        #         print "value=%r, count=%r" % (i, bins[i])
        #         count += 1
        #         total += bins[i]
        #
        # print "Count=%r, total=%r" % (count, total)

        self.update()

    def _convert_image(self, data_buf):
        # print data_buf.shape
        # print data_buf.dtype

        file_header = self._model.file_header(self._video_name)
        data_buf = data_buf.reshape((file_header.height, file_header.width, 3))

        return OpenCVQImage(data_buf)

    def paintEvent(self, e):

        if not self._video_name: return

#        INFO("drawing camera {}".format(self.base_name()))

        origin = QtCore.QPoint(0, 0)
        rect = QtCore.QRect(0, 0, self.width(), self.height())
        self._painter.begin(self)

        if self._display_option == self.DISPLAY_OPTION.BLEND:
            if self._color_image and self._depth_image:
                # Make sure its always scaled properly to fit the widget dimensions
                color_scaled = self._color_image.scaled(self.width(), self.height(), QtCore.Qt.KeepAspectRatio)
                depth_scaled = self._depth_image.scaled(self.width(), self.height(), QtCore.Qt.KeepAspectRatio)

                self._painter.drawImage(origin, color_scaled)
                self._painter.setCompositionMode(QtGui.QPainter.CompositionMode_Multiply)
                self._painter.drawImage(origin, depth_scaled)

        elif self._display_option == self.DISPLAY_OPTION.COLOR:
            if self._color_image:
                scaled = self._color_image.scaled(self.width(), self.height(), QtCore.Qt.KeepAspectRatio)
                self._painter.drawImage(origin, scaled)

        elif self._display_option == self.DISPLAY_OPTION.DEPTH:
            if self._depth_image:
                scaled = self._depth_image.scaled(self.width(), self.height(), QtCore.Qt.KeepAspectRatio)
                self._painter.drawImage(origin, scaled)

        else:
            raise RuntimeError("This should never happen!!")

        # self._draw_harris_data(painter)

        # self._draw_

        self._painter.end()

    def display(self, option):
        self._display_option = option
        
        self.update()
       
    def project_changed(self, project_path):
        """Reloads the widget when the project changes"""
        
        self._video_name = None
        # If thise camera is out of range from the total number of cameras
        # then leave it uninitialized
        if(self._video_index >= len(self._model.files())):
            return
        
        # have to finalize my delayed initialization
        self._set_video( self._model.get_file_path(self._video_index) )

    def _draw_corners(self, painter):
        if self._corners:
            painter.setCompositionMode(QtGui.QPainter.CompositionMode_SourceOver)
            pen = self._painter.pen()
            pen.setColor(QtCore.Qt.red)
            pen.setWidth(2)
            pen.setCapStyle(QtCore.Qt.RoundCap)

            painter.setPen(pen)
            painter.setRenderHint(QtGui.QPainter.Antialiasing, True)

            # print type(self._harris_data)
            # print self._harris_data.shape

            idx = 0
            # center = QtCore.QPoint(0, 0)
            num_pts = len(self._corners)
            for x,y in self._corners:
                # print x,y
                pen.setColor(QtGui.QColor((num_pts - idx - 1) * 256 / num_pts, 0, idx * 256 / num_pts))
                idx += 1
                painter.setPen(pen)
                painter.drawPoint(x, y)
                # center.setX(center.x() + x)
                # center.setY(center.y() + y)

            # center = center / len(self._corners)
            # center.y /= len(self._harris_data)

            # Draw the center
            # pen.setColor(QtCore.Qt.blue)
            # pen.setWidth(10)
            # painter.setPen(pen)

            # painter.drawPoint(center)

        if self._corners_bbox:
            pen = painter.pen()

            node = self._model.camera_nodes(self._video_name)
            color_value = node.color_value()

            color = QtGui.QColor()
            color.setRedF(color_value[0])
            color.setGreenF(color_value[1])
            color.setBlueF(color_value[2])

            pen.setColor(color)
            painter.setPen(pen)
            painter.drawRect(self._corners_bbox)

        pen = painter.pen()
        pen.setColor(QtGui.QColor(0, 0, 255))
        pen.setWidth(4)
        painter.setPen(pen)
        rect = self._dialog_rect
        painter.drawPoint(rect.width()/2, rect.height()/2)


    def _show_context_menu(self, pt):
        # INFO("Type = " + str(type(pt)))
        # INFO("Point = " + str(pt))
        global_pt = self.mapToGlobal(pt)

        menu = QtGui.QMenu()

        node = self._model.camera_nodes(self._video_name)

        flag = not self._dialog.isVisible()
        dialog_action = QtGui.QAction(self)
        if flag:
            dialog_action.setText("Show Dialog")
        else:
            dialog_action.setText("Hide Dialog")
        menu.addAction(dialog_action)

        calibrate_camera = QtGui.QAction(self)
        calibrate_camera.setText("Calibrate Camera")
        menu.addAction(calibrate_camera)

        clear_calibration = QtGui.QAction(self)
        clear_calibration.setText("Clear Calibration")
        menu.addAction(clear_calibration)

        show_cloud_flag = not node.show_cloud()
        show_cloud_action = QtGui.QAction(self)
        if show_cloud_flag:
            show_cloud_action.setText("Show Cloud")
        else:
            show_cloud_action.setText("Hide Cloud")
        menu.addAction(show_cloud_action)

        selected_item = menu.exec_(global_pt)
        if selected_item == dialog_action:
            self._dialog.setVisible(flag)
        elif selected_item == calibrate_camera:

            # First do the intrinsic camera calibration
            # dialog = intrinsic_calibration_dialog.intrinsic_calibration_dialog(self, self._model, self._video_name)
            # dialog.show()

            self._grid_color_selector.exec_()
            selected_items = self._grid_color_selector.color_list.selectedItems()
            if len(selected_items) < 1:
                ERROR("Need to select a color!")
            else:
                item = selected_items[0]
                node.set_color(item.color_name)

            # Then do the extrinsic camera calibration
            self._model.freeze_ext_camera_props(self._video_name)

        elif selected_item == clear_calibration:
            self._model.clear_calibration(self._video_name)
        elif selected_item == show_cloud_action:
            node.set_show_cloud(show_cloud_flag)
        else:
            # INFO("Nothing selected!")
            pass

    def _on_dialog_resize(self, size=None):
        if not self._dialog_image:
            return

        dialog_image = self._dialog_image.copy(self._dialog_rect)

        # INFO("called, type(size)=" + str(type(size)))
        self._painter.begin(dialog_image)

        self._draw_corners(self._painter)

        self._painter.end()

        self._dialog._widget.set_image(dialog_image)

    # def _convert_gray_to_color(self, gray):
    #     ret = np.ndarray(shape=(gray.shape[0], gray.shape[1], 3), dtype=np.uint8)
    #     for y in range(gray.shape[0]):
    #         for x in range(gray.shape[1]):
    #             val = np.uint8(gray[y,x])
    #             ret[y, x] = [val, val, val]
    #
    #     return ret