# -*- coding: utf-8 -*-
"""
Created on Tue Jan 20 22:42:18 2015

@author: benjamin
"""

# caller_name() is copied verbadium from https://gist.github.com/techtonik/2151727

# Public Domain, i.e. feel free to copy/paste
# Considered a hack in Python 2

import inspect
import traceback
import sys
import time
import datetime

import numpy as np
import OpenGL.GL as gl
import OpenGL.GLU as glu
from PySide import QtCore, QtGui

def _caller_name(skip=2):
    """Get a name of a caller in the format module.class.method

       `skip` specifies how many levels of stack to skip while getting caller
       name. skip=1 means "who calls me", skip=2 "who calls my caller" etc.

       An empty string is returned if skipped levels exceed stack height
       Copied from 
    """
    stack = inspect.stack()
    start = 0 + skip
    if len(stack) < start + 1:
      return ''
    parentframe = stack[start][0]    

    name = []
    module = inspect.getmodule(parentframe)
    # `modname` can be None when frame is executed directly in console
    # TODO(techtonik): consider using __main__
    if module:
        name.append(module.__name__)
    # detect classname
    if 'self' in parentframe.f_locals:
        # I don't know any way to detect call from the object method
        # XXX: there seems to be no way to detect static method call - it will
        #      be just a function call
        name.append(parentframe.f_locals['self'].__class__.__name__)
    codename = parentframe.f_code.co_name
    if codename != '<module>':  # top level usually
        name.append( codename ) # function or a method
    del parentframe
    return ".".join(name)

def STACK():
    traceback.print_stack()

def INFO(msg):
    print "{}: {}".format(_caller_name(), msg)

def ERROR(msg):
    sys.stderr.write( "ERROR: {}: {}\n".format(_caller_name(), msg) )

# From http://stackoverflow.com/questions/36932/how-can-i-represent-an-enum-in-python    
def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

# Code from http://stackoverflow.com/questions/21030391/how-to-normalize-array-numpy
def normalized(a, axis=-1, order=2):
    l2 = np.atleast_1d(np.linalg.norm(a, order, axis))
    l2[l2==0] = 1
    return a / np.expand_dims(l2, axis)


 # Code for drawing a cube
_cube_vertices = (
    (1, -1, -1),  # 0
    (1, 1, -1),   # 1
    (-1, 1, -1),  # 2
    (-1, -1, -1), # 3
    (1, -1, 1),   # 4
    (1, 1, 1),    # 5
    (-1, -1, 1),  # 6
    (-1, 1, 1),   # 7
)
_cube_edges = (
    (0,1),
    (0,3),
    (0,4),
    (2,1),
    (2,3),
    (2,7),
    (6,3),
    (6,4),
    (6,7),
    (5,1),
    (5,4),
    (5,7)
)

_cube_faces = (
    (4,5,7,6),    # +Z
    (3,2,1,0),    # -Z

    (0,1,5,4),    # +X
    (2,3,6,7),    # -X

    (1,2,7,5)     # +Y
    # (4,6,3,0),    # -Y, don't draw the bottom
)

_cube_normals = None

def draw_cube(draw_wireframe):
    global _cube_normals
    if not _cube_normals:
        _cube_normals = []
        for face in _cube_faces:
            normal = np.zeros(3, dtype=np.float64)
            u = np.array(_cube_vertices[face[0]]) - np.array(_cube_vertices[face[1]])
            v = np.array(_cube_vertices[face[1]]) - np.array(_cube_vertices[face[2]])
            normal = np.cross(u, v)
            # normal = normalized(normal)
            _cube_normals.append(normal.tolist())

    gl.glPushMatrix()
    # Scale the cube by half so that it is always a unit cube
    gl.glScalef(0.5, 0.5, 0.5)

    if draw_wireframe:
        gl.glBegin(gl.GL_LINES)

        for edge in _cube_edges:
            for vert in edge:
                gl.glVertex3fv(_cube_vertices[vert])

        gl.glEnd()
    else:
        gl.glBegin(gl.GL_QUADS)

        for i in range(len(_cube_faces)):
            gl.glNormal3fv(_cube_normals[i])
            for vert in _cube_faces[i]:
                gl.glVertex3fv(_cube_vertices[vert])

        gl.glEnd()

    gl.glPopMatrix()

def draw_grid(grid, draw_wireframe):
    for i in range(grid[0]):
        for j in range(grid[1]):
            if ((i+j) % 2) == 0: continue # Skip every other square
            if draw_wireframe: gl.glBegin(gl.GL_LINE_LOOP)
            else: gl.glBegin(gl.GL_QUADS)
            gl.glVertex3f(i+0, j+0, 0)
            gl.glVertex3f(i+1, j+0, 0)
            gl.glVertex3f(i+1, j+1, 0)
            gl.glVertex3f(i+0, j+1, 0)
            gl.glEnd()

_pyramid_vertices = (
    (0, 0, 1),      # 0 - peak
    (-1, -1, -1),   # 1
    (+1, -1, -1),   # 2
    (+1, +1, -1),   # 3
    (-1, +1, -1)    # 4
)

_pyramid_edges = (
    (0, 1),
    (0, 2),
    (0, 3),
    (0, 4),
    (1, 2),
    (2, 3),
    (3, 4),
    (4, 1)
)

_pyramid_faces = (
    (0, 1, 2),
    (0, 2, 3),
    (0, 3, 4),
    (0, 4, 1),
    (1, 3, 2),
    (1, 4, 3)
)

_pyramid_normals = None

def draw_pyramid(draw_wireframe):
    gl.glPushMatrix()
    gl.glScalef(0.5, 0.5, 0.5)

    if draw_wireframe:
        gl.glBegin(gl.GL_LINES)

        for edge in _pyramid_edges:
            for vert in edge:
                gl.glVertex3fv(_pyramid_vertices[vert])
    else:
        global _pyramid_normals
        if not _pyramid_normals:
            _pyramid_normals = []
            for face in _pyramid_faces:
                normal = np.zeros(3, dtype=np.float64)
                u = np.array(_pyramid_vertices[face[0]]) - np.array(_pyramid_vertices[face[1]])
                v = np.array(_pyramid_vertices[face[1]]) - np.array(_pyramid_vertices[face[2]])
                normal = np.cross(u, v)
                normal = normalized(normal)
                _pyramid_normals.append(normal.tolist())

        gl.glBegin(gl.GL_TRIANGLES)

        for i in range(len(_pyramid_faces)):
            gl.glNormal3fv(_pyramid_normals[i])
            for vert in _pyramid_faces[i]:
                gl.glVertex3fv(_pyramid_vertices[vert])

    gl.glEnd()

    gl.glPopMatrix()

def unproject(x, y):
    '''
    Unprojects the screen space coordinates (x, y) to the near point and a direction vector
    :param x: The x coordinate
    :param y: The y coordinate
    :return: tuple of the near point and direction vector
    '''
    model_view_mat = gl.glGetDoublev(gl.GL_MODELVIEW_MATRIX)
    projection_mat = gl.glGetDoublev(gl.GL_PROJECTION_MATRIX)
    viewport = gl.glGetIntegerv(gl.GL_VIEWPORT)

    near_pt = glu.gluUnProject(x, y, 0.0, model_view_mat, projection_mat, viewport)
    near_pt = np.array(near_pt)
    far_pt  = glu.gluUnProject(x, y, 0.2, model_view_mat, projection_mat, viewport)
    far_pt = np.array(far_pt)

    vec = far_pt - near_pt

    vec_mag = np.sqrt(vec.dot(vec))
    vec /= vec_mag

    return (near_pt, vec)

def unproject2(x, y, modelview_mat, projection_mat, viewport):
    '''
    Unprojects the screen space coordinates (x, y) to the near point and a direction vector
    :param x: The x coordinate
    :param y: The y coordinate
    :return: tuple of the near point and direction vector
    '''

    near_pt = glu.gluUnProject(x, viewport[3] - y, 0.0, modelview_mat, projection_mat, viewport)
    near_pt = np.array(near_pt)
    far_pt  = glu.gluUnProject(x, viewport[3] - y, 1.0, modelview_mat, projection_mat, viewport)
    far_pt = np.array(far_pt)

    vec = far_pt - near_pt

    vec_mag = np.sqrt(vec.dot(vec))
    vec /= vec_mag

    return (near_pt, vec)

class OpenCVQImage(QtGui.QImage):
    """Converts a cv image to a QImage"""
    def __init__(self, cv_img):
        format = QtGui.QImage.Format_RGB888
        height, width, bytesPerComponent = cv_img.shape

        bytesPerLine = bytesPerComponent * width
        # INFO("width=%d, height=%d, bytesPerComponent=%d" % (width, height, bytesPerComponent))
        # print cv_img.shape
        # print cv_img.dtype

        # Keep a reference so Qt does not crash!
        # This trick is from ImageQt in http://svn.effbot.org/public/stuff/sandbox/pil/ImageQt.py
        self.data = cv_img.data

        super(OpenCVQImage, self).__init__(cv_img.data, width, height, bytesPerLine, format)


class ImageWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(ImageWidget, self).__init__(parent)

        self._image = None
        self._painter = QtGui.QPainter()

    def set_image(self, img):
        self._image = img
        self.update()

    def paintEvent(self, e):
        if self._image is None: return

        # For some reason, scaling the image prevents Qt from blowing up
        # Also, Keep the image always as large as possible
        image = self._image.scaled(self.width(),
                                   self.height(),
                                   QtCore.Qt.KeepAspectRatio)

        self._painter.begin(self)
        self._painter.drawImage(QtCore.QPoint(0,0), image)
        self._painter.end()

def get_time():
    '''Returns the current time in milliseconds'''
    return int(round(time.time() * 1000))