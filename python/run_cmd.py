#!/usr/bin/env python

__author__ = 'benjamin'

import sys
import os.path
import os
import socket
import time
import datetime

def INFO(msg):
    print "{}".format(msg)

def ERROR(msg):
    sys.stderr.write( "ERROR: {}\n".format(msg) )

def unix_time(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return delta.total_seconds()

def unix_time_millis(dt):
    return unix_time(dt) * 1000.0

# Need to execute "avconv -f video4linux2 -s 640x480 -r 45 -i /dev/video0 -vcodec libx264 -f mp4 -y %s > output.log 2>&1"

def main():
    current_time = datetime.datetime.now()
    current_time = int(unix_time_millis(current_time))

    # print sys.argv

    if len(sys.argv) < 2:
        print current_time + 5000
        return

    if len(sys.argv) < 3:
        print current_time + int(sys.argv[1])
        return

    # Number of milliseconds since the epoch to start recording the video
    start_time = int(sys.argv[1])

    cmd = " ".join(sys.argv[2:])
    # cmd = "avconv -f video4linux2 -s 640x480 -r 45 -i /dev/video0 -vcodec libx264 -f mp4 -y output.mp4 > output.log 2>&1"

    delta = start_time - current_time

    # INFO("Delta = %s, cmd='%s'" % (delta, cmd))

    if(delta > 0):
        #INFO("start_time=%s, current_time = %s, sleeping for %s seconds" % (start_time, current_time, delta))
        INFO("sleeping for %s ms" % delta)
        time.sleep(float(delta) / 1000)

    INFO("Executing command '%s'" % cmd)

    os.system(cmd)

    return 0

if __name__ == "__main__":
    main()
