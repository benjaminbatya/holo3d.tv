__author__ = 'benjamin'

import datetime
import holo3d
from holo3d_utils import INFO, ERROR

from camera_frame import CameraFrame

class TimeStruct:
    MICROS_PER_SEC = 1000000

    def __init__(self, stamp, delta=None):
        self.stamp = stamp
        self.second = self.stamp / TimeStruct.MICROS_PER_SEC
        self.millisec = self.stamp % TimeStruct.MICROS_PER_SEC / 1000
        self.date = datetime.datetime.fromtimestamp(self.second)
        self.delta = delta

    def __str__(self):
        return "{}.{}". format(str(self.second), str(self.millisec))

class FileLoader(holo3d.file_loader):
    def __init__(self, file_path):
        super(FileLoader, self).__init__(file_path)
        # Always reset the Loader when it is created
        self.reset()

        self._file_path = file_path
        self._frame_times = None
        self._frame_indices = None
        self._color_decoder = None
        self._depth_decoder = None
        self._min_time = None
        self._max_time = None

    def reset(self):
        """This is an alias for self.run()"""
        self.run()

    def file_path(self):
        return self._file_path

    def frame_times(self):
        if self._frame_times is None:
            self.reset()
            frame = holo3d.camera_frame()
            frame_times = []

            while(True):
                valid = self.next_frame(frame)
                # Once we get an invalid frame, we are done
                if(not valid): break

                frame_times.append(self._get_time_info(frame))

            self._frame_times = frame_times

        return self._frame_times

    def min_time(self):
        # Note: we probably should just assume that the minimum time is at index 0
        if self._min_time is None:
            times = self.frame_times()
            self._min_time = min( [ st.stamp for st in times])

        return self._min_time

    def max_time(self):
        # Note: we should probably just assume that the maximum time is at index len(frame_times())-1
        if self._max_time is None:
            times = self.frame_times()
            self._max_time = max( [ st.stamp for st in times])

        return self._max_time

    def _get_time_info(self, frame):
        # Note: Just average the color and depth time stamps for now
        avg_time = (frame.color_time_stamp + frame.depth_time_stamp) / 2
        delta = frame.color_time_stamp - frame.depth_time_stamp

        time = TimeStruct(avg_time, delta)
        return time

    def num_frames(self):
        return len(self.frame_times())

    def frame_indices(self):
        """This should be a monotonically increasing array from 0 to num_frames"""
        if self._frame_indices is None:
            self.reset()
            frame = holo3d.camera_frame()
            frame_indices = []

            index = 0
            while(True):
                valid = self.next_frame(frame)
                if(not valid):
                    ERROR("Invalid frame %d" % index)
                    return None
                index += 1

                frame_indices.append(frame.frame_number)


            self._frame_indices = frame_indices

        return self._frame_indices

    def frame_at_index(self, index):
        frame = CameraFrame(self)

        if(index < 0 or self.num_frames() <= index):
            ERROR("frame index %d out of bounds" % index)
            return None

        self.reset()
        for idx in range(index+1):
            valid = self.next_frame(frame)
            if(not valid):
                ERROR("Invalid frame %d" % idx)
                return None

        return frame

    def frame_at_time(self, time_code):
        frame_times = self.frame_times()
        frame_to_load = len(frame_times)-1 # By default load the last frame
        prev_delta = abs(time_code - frame_times[0].stamp)
        for i in range(len(frame_times)-1):
            next_frame_time = frame_times[i+1].stamp
#            INFO("str(prev_frame_time)={}".format(str(prev_frame_time)))
#            INFO("str(next_frame_time)={}".format(str(next_frame_time)))
            delta = abs(time_code-next_frame_time)
            if(prev_delta <= delta):
                frame_to_load = i
                break
            else:
                prev_delta = delta

        return self.frame_at_index(frame_to_load)

    def _create_decoders(self):
        factory = holo3d.factory_decoder(self.file_header.width, self.file_header.height, self.file_header.width)
        self._color_decoder = factory.color(self.file_header.color_format)
        self._depth_decoder = factory.depth(self.file_header.depth_format)

    def color_decoder(self):
        if self._color_decoder is None:
            self._create_decoders()

        return self._color_decoder

    def depth_decoder(self):
        if self._depth_decoder is None:
            self._create_decoders()

        return self._depth_decoder

