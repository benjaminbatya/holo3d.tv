__author__ = 'benjamin'

import OpenGL.GL as gl

class CameraSelector:
    VOLUME_INDEX = 100  # Make sure the volume index is different from the camera indices

    def __init__(self, widget):
        self._widget = widget

    def select(self):
        # Draw the cameras here
        idx = 0
        for file_path in self._widget._model.files():

            node = self._widget._model.camera_nodes(file_path)

            if node is None:
                continue

            rot_mat = node.matrix()

            # Draw the final camera position
            gl.glPushMatrix()

            gl.glMultMatrixf(rot_mat)

            gl.glLoadName(idx)
            idx += 1

            self._widget.draw_camera(file_path, False)

            gl.glPopMatrix()

        if self._widget._model.display_volume():
            # Draw the volume of interest cuboid
            gl.glLoadName(self.VOLUME_INDEX)
            self._widget.draw_volume_of_interest(False)

    def process_best_hit(self, best_hit):
        # INFO("best_hit = %r" % best_hit)
        if best_hit is None:
            return None

        elif best_hit < len(self._widget._model.files()):
            file_path = self._widget._model.get_file_path(best_hit)
            return self._widget._model.camera_nodes(file_path)

        elif best_hit == self.VOLUME_INDEX:
            return self._widget._model.volume_of_interest()

        else:
            return None

