__author__ = 'benjamin'

import numpy as np
import pyopencl as cl
import sys

import unittest

class BaseCL(object):
    def __init__(self, program):
        # Setup the context
        platforms = cl.get_platforms()
        from pyopencl.tools import get_gl_sharing_context_properties
        try:
            if sys.platform == "darwin":
                self._ctx = cl.Context(properties=get_gl_sharing_context_properties(),
                                 devices=[])
            else:
                self._ctx = cl.Context(properties=[
                    (cl.context_properties.PLATFORM, platforms[0])]
                    + get_gl_sharing_context_properties(), devices=None)
        except TypeError:
            self._ctx = cl.Context(properties=[(cl.context_properties.PLATFORM, platforms[0])], devices=None)

        self._queue = cl.CommandQueue(self._ctx)

        # Load the program
        f = open(program, 'r')
        fstr = "".join(f.readlines())

        self._program = cl.Program(self._ctx, fstr).build()

        # Calculate all of the variables needed to run the program
        device = self._ctx.devices[0]

    def ctx(self):
        return self._ctx

class GatherCL(BaseCL):
    def __init__(self):
        super(GatherCL, self).__init__('GatherAlgo.cl')

    def run(self, points_cl, colors_cl, num_pts, num_pts_per_item):

        num_items = num_pts / num_pts_per_item

        # Step 1: Calculate the counts
        count_np = np.zeros((num_items,), dtype=np.uint32)
        count_cl = cl.Buffer(self._ctx, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=count_np)

        global_shape = (num_items,)
        local_shape = None

        kernel_args = (colors_cl, np.int32(num_pts_per_item), count_cl)

        self._program.count_valid(self._queue, global_shape, local_shape, *(kernel_args))

        cl.enqueue_copy(self._queue, count_np, count_cl)
        self._queue.finish()

        # Step 2: calculate the prefix sum of counts
        offsets = np.cumsum(count_np)

        offsets = np.concatenate(([0], offsets)).astype(np.uint32)

        # Step 3: Get the valid items from the output buffer and transform them
        num_valid = offsets[-1]

        valid_np = np.zeros((num_valid,3), dtype=np.float32)
        valid_cl = cl.Buffer(self._ctx, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=valid_np)

        offsets_cl = cl.Buffer(self._ctx, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=offsets)

        kernel_args = (points_cl, colors_cl, np.int32(num_pts_per_item), offsets_cl, valid_cl)

        self._program.copy_valid(self._queue, global_shape, local_shape, *(kernel_args))

        cl.enqueue_copy(self._queue, valid_np, valid_cl)
        self._queue.finish()

        return valid_np

## Unit test for GatherAlgo
class GatherCLTest(unittest.TestCase):
    def __init__(self, method_name):
        super(GatherCLTest, self).__init__(method_name)

        self._gather_cl = GatherCL()

    def test_Step_1(self):
        points_np = np.array([[-1, -1, 1, 1],
                              [-1, +1, 2, 1],
                              [+1, -1, 3, 1],
                              [+1, +1, 4, 1]]).astype(np.float32)
        colors_np = np.array([[0,0,0,1],
                              [0,0,0,1],
                              [0,0,0,0],
                              [0,0,0,1]]).astype(np.uint8)

        points_cl = cl.Buffer(self._gather_cl.ctx(),
                              cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR,
                              hostbuf=points_np)
        colors_cl = cl.Buffer(self._gather_cl.ctx(),
                              cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR,
                              hostbuf=colors_np)

        valid_points = self._gather_cl.run(points_cl, colors_cl, points_np.shape[0], 2)

        self.assertEqual(valid_points.shape[0], 3)
