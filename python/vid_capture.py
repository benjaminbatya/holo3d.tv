#!/usr/bin/env python

__author__ = 'benjamin'

import sys
import os.path
import os
import socket
import time
import datetime

from run_cmd import unix_time_millis

def curr_time_millis():
    t = datetime.datetime.now()
    return int(unix_time_millis(t))

from holo3d_utils import INFO, ERROR

MS_IN_S = 1000

# Use this to deploy HoloCamera to all attached cameras
NUM_CAMERA = 8
CAMERA_PREFIX = 'camera'

DELAY_TIME = 5 # default time to wait for the camera to get ready (in seconds)

FILM_TIME = 10 # time to wait for the filming (in seconds)

# High fps, standard quality, use to record actions
# ACTION_CMD = "avconv -f video4linux2 -s 640x480 -r 45 -i /dev/video0 -vcodec mjpeg -qscale 20 -f avi -y output.avi > output.log 2>&1"
# Decrease the framerate to insure a steady stream of frames
ACTION_CMD = "avconv -f video4linux2 -s 640x480 -r 30 -i /dev/video0 -vcodec mjpeg -qscale 20 -f avi -y output.avi > output.log 2>&1"
# Low fps, max quality, use to calibrate
CALIB_CMD = "avconv -f video4linux2 -s 640x480 -r 15 -i /dev/video0 -vcodec mjpeg -qscale 1 -f avi -y output.avi > output.log 2>&1"

EXEC_CMD = False

def test_camera(cam_name):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connected = None
    try:
        s.settimeout(0.1)
        s.connect((cam_name, 22))
        connected = True
    except socket.error as e:
        ERROR("connecting to %s: %s" % (cam_name, e))
        connected = False

    s.close()
    return connected

def ssh_cmd(camera, cmd):
    if EXEC_CMD:
        INFO("on camera %s, running '%s'" % (camera, cmd))
        os.system("ssh " + camera + " " + cmd + "")
    else:
        INFO("on camera %s, fake run '%s'" % (camera, cmd))

def run_cmd(cmd):
    if EXEC_CMD:
        INFO("running '%s'" % cmd)
        os.system(cmd)
    else:
        INFO("would have run '%s'" % cmd)

def scp_file(camera, directory):
    run_cmd("scp %s:output.avi %s/%s.avi" % (camera, directory, camera))

def create_dir():
    date = time.localtime()
    time_str = time.strftime("%Y%m%d_%H_%M_%S", date)
    home = os.path.expanduser("~")
    directory = "%s/videos/%s" % (home, time_str)

    if not os.path.exists(directory):
        if EXEC_CMD:
            INFO("Creating directory '%s'" % directory)
            os.makedirs(directory)
        else:
            INFO("fake create directory '%s'" % directory)

    return directory

def main():
    
    cam_cmd = CALIB_CMD
    if len(sys.argv) > 2:
        capture_type = sys.argv[2]
        if capture_type == "action":
            cam_cmd = ACTION_CMD
    
    global DELAY_TIME
    if len(sys.argv) > 3:
        DELAY_TIME = int(sys.argv[3])

    if DELAY_TIME < 1:
        ERROR("Delay time is too small!")
        return

    valid_cameras = []

    for i in range(NUM_CAMERA):
        # check to see if the camera is there
        camera = CAMERA_PREFIX + str(i+1)

        ret = test_camera(camera)
        if ret:
            valid_cameras.append(camera)

    for camera in valid_cameras:
        # Make sure that the HoloCamera process is dead
        ssh_cmd(camera, "killall -5 HoloCamera")

        # Delete the previous output files
        ssh_cmd(camera, "rm output.avi output.log ps.log")

    current_time = curr_time_millis()
    start_time = current_time + (DELAY_TIME+1)*MS_IN_S

    for camera in valid_cameras:
        # Start the watch process
        # ssh_cmd(camera, "nohup watch -t -n 0.1 -p \'(ps -eo pcpu,pid,user,args | sort -r -k1 | head -5) | tee -a ps.log\' &")

        # Start the recording process
        file_name = "output.mp4"
        ssh_cmd(camera, 'nohup ./run_cmd.py %s "%s" &' % (start_time, cam_cmd))
        #ssh_cmd(camera, 'nohup ./run_cmd.py %s exec &' % (start_time))

    # Do a count down
    current_time = curr_time_millis()

    # INFO("sleeping for %s ms" % delta)
    it = DELAY_TIME
    while it > 0:
        print it
        it -= 1
        time.sleep(1)
        current_time = curr_time_millis()

    # Wait until the start time
    remainder_time = start_time - current_time
    time.sleep(remainder_time / MS_IN_S)

    # Wait for 10 seconds past the start time
    # INFO("Sleeping for %s seconds" % FILM_TIME)
    # time.sleep(FILM_TIME)

    # Wait for the enter to be pressed to end the recording
    raw_input("Start recording at %s.\nPress Enter to stop..." % datetime.datetime.now())

    for camera in valid_cameras:
        # Kill the recording process
        ssh_cmd(camera, "killall -INT avconv")

        # Kill the watch process
        # ssh_cmd(camera, "killall -INT watch")

    dir_str = create_dir()

    time.sleep(1)

    for camera in valid_cameras:
        scp_file(camera, dir_str)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        EXEC_CMD = True

    main()

    # camera = "camera3"
    # dir_str = create_dir()
    # scp_file(camera, dir_str)