// NOTE: the PYOPENCL_COMPILER_OUTPUT environment variable MUST be set to 1 for this to not complain
// See http://stackoverflow.com/questions/20722242/compilerwarning-with-opencl
#define INFO(_format, ...) { \
    printf((__constant char *)("\\n(%u,%u,%u) "), get_global_id(0), get_group_id(0), get_local_id(0)); \
    printf((__constant char *)(_format), __VA_ARGS__); \
}

// Values from "Consumer Depth Cameras for Computer Vision" book
#define DEPTH_TO_M 0.001f
#define M_TO_INCH 39.3701f
#define DEPTH_TO_INCH DEPTH_TO_M * M_TO_INCH
#define SCL 1.0f / 524.0f

#define K1 0.2402
#define K2 -0.6861
#define K3 -0.0015
#define P1 0.0
#define P2 0.0

__kernel void generate_depth(__global unsigned short* input,
                             __global float4* output,
                             const int display_cloud_from_edge)
{
    int width = get_global_size(0);
    int height = get_global_size(1);

    int x_idx = get_global_id(0);
    int y_idx = get_global_id(1);

    size_t idx = y_idx*width + x_idx;

    //     if(idx == 0)
    // {
        // INFO("dims = %dx%d\\n", width, height);
    // }

    float4 value = (float4)(-1000.0f, -1000.0f, -1000.0f, 1.0f);

    bool display = true;
    if(display_cloud_from_edge > 0)
    {
        if(display_cloud_from_edge <= x_idx && x_idx < (width - display_cloud_from_edge) &&
           display_cloud_from_edge <= y_idx && y_idx < (height - display_cloud_from_edge))
        {
            display = false;
        }
    }

    if(display)
    {
        int center_x = width / 2;
        int center_y = height / 2;

        unsigned short depth = input[idx];

        float x = x_idx - center_x;
        float y = y_idx - center_y;
        float z = depth * DEPTH_TO_INCH;

        // The radial and tangential correction factors from the book don't seem to work right now
        float x_corrected = x;
        float y_corrected = y;

        // Radial distortion correction
        // From http://docs.opencv.org/doc/tutorials/calib3d/camera_calibration/camera_calibration.html
        // r2 is r^2
        // float r2 = x*x + y*y;

        // float x_corrected = x * (1.0 + K1 * r2 + K2 * r2 * r2 + K3 * r2 * r2 * r2);
        // float y_corrected = y * (1.0 + K1 * r2 + K2 * r2 * r2 + K3 * r2 * r2 * r2);

        // Note: this is a slightly faster polynomial expansion version
        // float x_corrected = x * (1.0 + (K1 + (K2 + K3 * r2) * r2) * r2);
        // float y_corrected = y * (1.0 + (K1 + (K2 + K3 * r2) * r2) * r2);

        // Tangential distortion correction
        // x_corrected += 2.0 * P1 * x * y + P2 * (r2 + 2.0*x*x);
        // y_corrected += P1 * (r2 + 2.0*x*y) + 2.0 * P2 * x * y;

        x = x_corrected * z * SCL;  // NOTE: this is supposed to be -1 * (x_idx - ...) but it is a mistake
        y = y_corrected * z * SCL;

        value = (float4)(x, y, z, 1.0f);
    }

    output[idx] = value;
}