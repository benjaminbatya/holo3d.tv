# -*- coding: utf-8 -*-
"""
Created on Mon Jan 26 22:05:37 2015

@author: benjamin
"""

from PySide import QtCore, QtGui, QtOpenGL
import sys
import numpy as np
import math
import time

from holo3d_utils import (INFO, enum, draw_cube, draw_pyramid, draw_grid, get_time)
from fusion_model import Fusion_Model
from CameraSelector import CameraSelector
from CameraObject import CameraObject
from VolumeObject import VolumeObject
from cloud_compositor import CloudCompositor
from tracker.blob_tracker import BlobTracker
from floor_model import FloorModel
from MoveManip import MoveManip
from RotateManip import RotateManip


try:
    import OpenGL.GL as gl
    import OpenGL.GLU as glu
    import OpenGL.arrays.vbo as glvbo
    import OpenGL.arrays as GLArrays

except ImportError:
    app = QtGui.QApplication(sys.argv)
    QtGui.QMessageBox.critical(None, "OpenGL hellogl",
                               "PyOpenGL must be installed to runhis ts example")
    sys.exit(1) 


# Code from http://stackoverflow.com/questions/6802577/python-rotation-of-3d-vector
def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    theta = np.asarray(theta)
    axis = axis/math.sqrt(np.dot(axis, axis))
    a = math.cos(theta/2)
    b, c, d = -axis*math.sin(theta/2)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.array([[aa+bb-cc-dd, 2*(bc+ad),    2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd,    2*(cd+ab)],
                     [2*(bd+ac),   2*(cd-ab),  aa+dd-bb-cc]])

MOUSE_MODE = enum('None', 'Translate', 'Rotate', 'Scale')   # << Different mouse modes
# INTERACTION_MODE = enum('Navigate', 'Translate', 'Rotate')  # << Different interaction modes,
#                                                             # navigate = user moves through the scene
#                                                             # translate = user changes the selected camera's position
#                                                             # rotate = user changes the selected camera's orientation
GL_Z_NEAR      = 10                                         # << The z near plane
GL_Z_FAR       = 11000                                      # << The z far plane
GL_FOVY        = 50.0                                       # The field of view
ZOOM_AMOUNT    = 1.2                                        # Standard amount to increase of decrease zoom by

class Fusion_Widget(QtOpenGL.QGLWidget):
    """Widget displaying the point clouds and fusion and experiments"""    
    def __init__(self, parent, model):
        QtOpenGL.QGLWidget.__init__(self, parent)
        
        self._model = model

        self._draw_wireframe = True

        self._compositor = None

        self._tracker = None

        self._selected_object = None

        self._reset_transforms()

        self._first_run = True

        self._only_display_cloud_edges = False

        ## Setup the right-click context menu
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self._show_context_menu)
        ## End context menu

        self._camera_selector = CameraSelector(self)

        model.selected_object_changed.connect(self.selected_object_changed)
        model.project_changed.connect(self.project_changed)
        model.time_value_changed.connect(self.frame_changed)

    def initializeGL(self):
        """Initialize OpenGL, VBOs, upload data to the GPU, etc."""
        gl.glClearColor(0, 0, 0, 0)
        gl.glClearDepth(1.0)
        gl.glDepthFunc(gl.GL_LESS)
        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glShadeModel(gl.GL_SMOOTH)

        # gl.glEnable(gl.GL_LIGHTING)
        gl.glEnable(gl.GL_LIGHT0)

        light_pos = (0.0, 0.0, 60.0)
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, light_pos)

        gl.glEnable(gl.GL_VERTEX_ARRAY)

        # Initialize CloudCompositor here after opencl has been initialized
        self._compositor = CloudCompositor(self, self._model)

        self._tracker = BlobTracker(self, self._model, self._compositor)

    def resizeGL(self, w, h):
        """Called when the widget get resized"""
        # INFO("w=%r, h=%r" % (w, h))

        gl.glViewport(0, 0, w, h)

        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()

        glu.gluPerspective(GL_FOVY, float(w)/float(h), GL_Z_NEAR, GL_Z_FAR)
        gl.glMatrixMode(gl.GL_MODELVIEW)

    def set_color(self, color):
        # gl.glMaterialfv(gl.GL_FRONT, gl.GL_AMBIENT_AND_DIFFUSE, color)
        gl.glColor4fv(color)

    def paintGL(self):
        """Paints the scene"""
        if self._first_run:
            # Delay just a little bit because this seems to help the display fully initialize
            # This is a BIG maybe
            time.sleep(0.1)
            self._first_run = False

        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

        gl.glLoadIdentity()

        gl.glTranslatef(self._translate[0], self._translate[1], 0.0)

        glu.gluLookAt(  self._eye_pos[0], self._eye_pos[1], self._eye_pos[2],
                        0, 0, 0,
                        0, 1, 0)


        gl.glEnable( gl.GL_POLYGON_OFFSET_FILL )

        # Use the context menu to toggle the cube on and off
        if self._model.display_markers():

            self.draw_coord_frame()

            self.set_color((0.5, 0.5, 0.5, 1.))

            gl.glPolygonOffset( 1., 1.)

            # Draw the cube
            gl.glPushMatrix()
            # Scale the cube to 12in x 12in x 12in
            gl.glScalef(12.0, 12.0, 12.0)
            draw_cube(False) # (self._draw_wireframe)
            gl.glPopMatrix()

            # Draw the grid onto the cube
            grid = self._model.grid_size()
            for i in range(4):
                self.set_color(CameraObject.color_value_by_index(i))

                gl.glPushMatrix()
                gl.glRotatef(i*90, 0, 1, 0)
                gl.glTranslatef(-float(grid[0])/2., -float(grid[1])/2., 6.)
                draw_grid(grid, True) #self._draw_wireframe)
                gl.glPopMatrix()

            # Draw the top
            self.set_color(CameraObject.color_value_by_index(4))

            gl.glPushMatrix()
            gl.glRotatef(90, 0, 1, 0)
            gl.glRotatef(-90, 1, 0, 0)
            gl.glTranslatef(-float(grid[0])/2., -float(grid[1])/2., 6.)
            draw_grid(grid, True) # self._draw_wireframe)
            gl.glPopMatrix()

            gl.glPolygonOffset( -1., -1. )

        if self._model.display_volume():
            self.set_color((0.5, 0.5, 0.5, 1.))
            if type(self._selected_object) is VolumeObject:
                self.set_color((1., 1., 1., 1.))
            self.draw_volume_of_interest(True)

        self._draw_frames()

        self._draw_blobs()

        gl.glDisable(gl.GL_POLYGON_OFFSET_FILL)
        # End grid

    def draw_volume_of_interest(self, draw_wireframe):
        node = self._model.volume_of_interest()
        gl.glPushMatrix()

        o = node.origin()
        gl.glTranslate(o[0], o[1], o[2])

        r = node.rotation() * 180.0 / math.pi
        gl.glRotatef(r, 0, 1, 0)

        s = node.size()
        gl.glScalef(s[0], s[1], s[2])

        draw_cube(draw_wireframe)

        gl.glPopMatrix()

        self.draw_floor()

    def draw_floor(self):
        floor = self._model.floor()

        if floor is None:
            return

        mean = floor.mean()
        normal = floor.normal()

        z_vec = np.cross(normal, (1.0, 0.0, 0.0))

        x_vec = np.cross(normal, z_vec)

        normal *= 50.0
        z_vec  *= 50.0
        x_vec  *= 50.0

        gl.glPushMatrix()

        gl.glTranslate(mean[0], mean[1], mean[2])

        gl.glPointSize(4)
        self.set_color((1.0, 1.0, 1.0, 1.0))
        gl.glBegin(gl.GL_POINTS)

        gl.glVertex3f(0.0, 0.0, 0.0)

        gl.glEnd()

        gl.glBegin(gl.GL_LINES)

        self.set_color((0.0, 1.0, 0.0, 1.0))
        gl.glVertex3f(0.0, 0.0, 0.0)
        gl.glVertex3f(normal[0], normal[1], normal[2])

        self.set_color((0.0, 0.0, 1.0, 1.0))
        gl.glVertex3f(0.0, 0.0, 0.0)
        gl.glVertex3f(z_vec[0], z_vec[1], z_vec[2])

        self.set_color((1.0, 0.0, 0.0, 1.0))
        gl.glVertex3f(0.0, 0.0, 0.0)
        gl.glVertex3f(x_vec[0], x_vec[1], x_vec[2])

        gl.glEnd()

        gl.glEnable(gl.GL_POINT_SMOOTH)
        gl.glPointSize(2)
        gl.glEnable(gl.GL_BLEND)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)

        gl.glBegin(gl.GL_TRIANGLE_STRIP)

        self.set_color((0.6, 0.6, 0.6, 0.5))
        bot_left = (x_vec + z_vec) * -1
        gl.glVertex3f(bot_left[0], bot_left[1], bot_left[2])
        bot_right = x_vec - z_vec
        gl.glVertex3f(bot_right[0], bot_right[1], bot_right[2])
        top_left = z_vec - x_vec
        gl.glVertex3f(top_left[0], top_left[1], top_left[2])
        top_right = x_vec + z_vec
        gl.glVertex3f(top_right[0], top_right[1], top_right[2])

        gl.glEnd()

        gl.glDisable(gl.GL_BLEND)

        gl.glPopMatrix()

    def draw_coord_frame(self):
        self.set_color((1., 0., 0., 1.,))
        gl.glBegin(gl.GL_LINES)
        gl.glVertex3f(0.,  0., 0.)
        gl.glVertex3f(50., 0., 0.)
        gl.glEnd()

        self.set_color((0., 1., 0., 1.,))
        gl.glBegin(gl.GL_LINES)
        gl.glVertex3f(0.,  0., 0.)
        gl.glVertex3f(0., 50., 0.)
        gl.glEnd()

        self.set_color((0., 0., 1., 1.,))
        gl.glBegin(gl.GL_LINES)
        gl.glVertex3f(0.,  0., 0.)
        gl.glVertex3f(0., 0., 50.)
        gl.glEnd()

    def _draw_frames(self):

        # Draw the composited cloud
        point_vbo = self._compositor.point_vbo()
        color_vbo = self._compositor.color_vbo()
        num_points = self._compositor.num_points()

        gl.glEnable(gl.GL_POINT_SMOOTH)
        gl.glPointSize(2)
        gl.glEnable(gl.GL_BLEND)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)

        gl.glEnableClientState(gl.GL_VERTEX_ARRAY)
        gl.glEnableClientState(gl.GL_COLOR_ARRAY)

        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, point_vbo)
        gl.glVertexPointer(4, gl.GL_FLOAT, 0, None)

        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, color_vbo)
        gl.glColorPointer(4, gl.GL_UNSIGNED_BYTE, 0, None)

        gl.glDrawArrays( gl.GL_POINTS, 0, num_points)

        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, 0)

        gl.glDisableClientState(gl.GL_VERTEX_ARRAY)
        gl.glDisableClientState(gl.GL_COLOR_ARRAY)

        gl.glDisable(gl.GL_BLEND)

        gl.glFinish()

        # Draw the cameras
        for file_path in self._model.files():

            node = self._model.camera_nodes(file_path)

            if node is None:
                continue

            rot_mat = node.matrix()

            # Draw the final camera position
            gl.glPushMatrix()

            gl.glMultMatrixf(rot_mat)

            if self._selected_manip is not None:
                self._selected_manip.apply_transform(file_path)

            if self._model.display_markers():

                if self._selected_manip is not None:
                    self._selected_manip.draw(file_path)

                color_value = node.color_value()

                # INFO("object_selected = '%r', file_path='%r'" % (self._camera_selected, file_path))
                if(type(self._model.selected_object()) is CameraObject and
                   self._model.selected_object().file_path() == file_path):
                    color_value = (1., 1., 1., 1.)

                self.set_color(color_value)

                self.draw_camera(file_path)

            gl.glPopMatrix()

    def draw_camera(self, file_path, draw_wireframe = True):
        gl.glPushMatrix()

        gl.glRotatef(180, 0, 1, 0)

        gl.glBegin(gl.GL_LINES)
        gl.glVertex3f(0, 0, 0)
        gl.glVertex3f(0, 0, -80)
        gl.glEnd()

        gl.glPushMatrix()
        gl.glTranslatef(0., 0., -0.5)
        draw_pyramid(draw_wireframe)
        gl.glPopMatrix()

        gl.glPushMatrix()
        gl.glTranslatef(0., 0., +0.5)
        draw_cube(draw_wireframe)
        gl.glPopMatrix()

        gl.glPopMatrix()

    def _draw_blobs(self):

        self.set_color((0.0, 0.5, 0.0, 1.0))

        for estimate in self._tracker.estimates():
            # draw each blob as a cube
            gl.glPushMatrix()

            pos = estimate.position()
            gl.glTranslatef(pos[0], pos[1], pos[2])

            size = estimate.size()
            gl.glScalef(size[0], size[1], size[2])

            draw_cube(True)

            gl.glPopMatrix()

    def _reset_transforms(self):
        self._angle = np.array([-45., 45.])
        self._distance = 120.0

        self._translate = np.array([0.0, 0.0])

        self._recalc_eye_pos()
        self._mouse_mode = MOUSE_MODE.None

        self.selected_object_changed()

        self._selected_manip = None

    def mousePressEvent(self, event):
        handled = False
        if self._selected_manip:
            # Check for selection
            hit = self._do_selection(event, self._selected_manip)
            if hit is not None:
                self._selected_manip.handle_press_event(event)
                handled = True

        if not handled:
            # INFO("event=%s" % event)
            self._last_pos = event.pos()

            # Check for a selection countdown
            self._last_time = get_time()

            if event.button() == QtCore.Qt.LeftButton:
                # Go into the rotate mode
                self._mouse_mode = MOUSE_MODE.Rotate

            elif event.button() == QtCore.Qt.MiddleButton:
                self._mouse_mode = MOUSE_MODE.Translate

        self.update()

        event.accept()

    def mouseMoveEvent(self, event):
        handled = False
        if self._selected_manip:
            handled = self._selected_manip.handle_move_event(event)

        if not handled:
             # INFO("event=%s" % event)
            dx = event.x() - self._last_pos.x()
            dy = event.y() - self._last_pos.y()

            if self._mouse_mode == MOUSE_MODE.Rotate:
                self._angle[0] += dx
                self._angle[1] += dy

                self._angle[1] = min(90, self._angle[1])
                self._angle[1] = max(-90, self._angle[1])
            elif self._mouse_mode == MOUSE_MODE.Translate:
                self._translate[0] += dx / 2.0
                self._translate[1] -= dy / 2.0

            self._last_pos = event.pos()

            self._recalc_eye_pos()

        self.update()

        event.accept()

    def mouseReleaseEvent(self, event):
        handled = False
        if self._selected_manip:
            handled = self._selected_manip.handle_release_event(event)

        if not handled:
            time_delta = get_time() - self._last_time
            if time_delta < 200:

                selected_object = self._do_selection(event, self._camera_selector)

                self._model.set_selected_object(selected_object)

                # Setup the selected manipulator
                self._selected_manip = None

                if type(selected_object) is CameraObject:
                    # INFO("mouse mode = %r" % self._mouse_mode)
                    if self._mouse_mode == MOUSE_MODE.Translate:
                        self._selected_manip = MoveManip(self, selected_object.file_path())
                    elif self._mouse_mode == MOUSE_MODE.Rotate:
                        self._selected_manip = RotateManip(self, selected_object.file_path())

            self._mouse_mode = MOUSE_MODE.None

        self.update()

        event.accept()

    def wheelEvent(self, event):
        num_degrees = event.delta() / 8
        num_steps = num_degrees / 15

        if event.orientation() == QtCore.Qt.Vertical:
            if event.delta() < 0:
                self._distance /= ZOOM_AMOUNT
            elif event.delta > 0:
                self._distance *= ZOOM_AMOUNT

        self._recalc_eye_pos()

        self.update()

        event.accept()

    def _show_context_menu(self, pt):
        global_pt = self.mapToGlobal(pt)

        menu = QtGui.QMenu()

        cube_action = QtGui.QAction(self)

        flag = not self._model.display_markers()
        if flag:
            cube_action.setText("Show Virtual Markers")
        else:
            cube_action.setText("Hide Virtual Markers")
        menu.addAction(cube_action)

        volume_action = QtGui.QAction(self)
        volume_flag = not self._model.display_volume()
        if volume_flag:
            volume_action.setText("Show Volume of Interest")
        else:
            volume_action.setText("Hide Volume of Interest")
        menu.addAction(volume_action)

        volume_filter_none_action = QtGui.QAction("No Point Filtering", self)
        volume_filter_none_action.setCheckable(True)
        volume_filter_volume_action = QtGui.QAction("Filter Volume Points", self)
        volume_filter_volume_action.setCheckable(True)
        volume_filter_floor_action = QtGui.QAction("Filter Floor Points", self)
        volume_filter_floor_action.setCheckable(True)

        group = QtGui.QActionGroup(self)
        group.addAction(volume_filter_none_action)
        group.addAction(volume_filter_volume_action)
        group.addAction(volume_filter_floor_action)

        volume_filter_flag = self._model.volume_filter()
        if volume_filter_flag == Fusion_Model.VOLUME_FILTER_TYPES.None:
            volume_filter_none_action.setChecked(True)
        elif volume_filter_flag == Fusion_Model.VOLUME_FILTER_TYPES.Volume:
            volume_filter_volume_action.setChecked(True)
        else:
            volume_filter_floor_action.setChecked(True)

        menu.addAction(volume_filter_none_action)
        menu.addAction(volume_filter_volume_action)
        menu.addAction(volume_filter_floor_action)

        plane_fit_action = QtGui.QAction(self)
        plane_fit_action.setText("Do linear plane fit")
        menu.addAction(plane_fit_action)

        count_visible_points = QtGui.QAction(self)
        count_visible_points.setText("Count number of visible points")
        menu.addAction(count_visible_points)

        do_tracking_action = QtGui.QAction(self)
        do_tracking = not self._tracker.active()
        if do_tracking:
            do_tracking_action.setText("Start Tracking")
        else:
            do_tracking_action.setText("Stop Tracking")
        menu.addAction(do_tracking_action)

        selected_item = menu.exec_(global_pt)
        if selected_item == cube_action:
            self._model.set_display_markers(flag)
        elif selected_item == volume_action:
            self._model.set_display_volume(volume_flag)
        elif selected_item == volume_filter_none_action:
            self._model.set_volume_filter(Fusion_Model.VOLUME_FILTER_TYPES.None)
        elif selected_item == volume_filter_volume_action:
            self._model.set_volume_filter(Fusion_Model.VOLUME_FILTER_TYPES.Volume)
        elif selected_item == volume_filter_floor_action:
            self._model.set_volume_filter(Fusion_Model.VOLUME_FILTER_TYPES.Floor)
        elif selected_item == plane_fit_action:
            self._plane_fit()
        elif selected_item == count_visible_points:
            num_points = self._compositor.num_valid_points()
            self._model.set_status_message("Number of visible points = %r" % num_points)
        elif selected_item == do_tracking_action:
            self._tracker.set_active(do_tracking)
        else:
            # INFO("Nothing selected!")
            pass

        self.update()

    def _recalc_eye_pos(self):
        rad_angle = [-math.radians(a) for a in np.nditer(self._angle)]

        rot_matrix_y = rotation_matrix([0.,1.,0.], rad_angle[0])
        rot_matrix_x = rotation_matrix([1.,0.,0.], rad_angle[1])

        m = rot_matrix_y.dot(rot_matrix_x)

        self._eye_pos = m.dot(np.array([0., 0., self._distance]))
        
    def project_changed(self, project_path):
        """Reloads the widget when the project changes"""
        # INFO("Called")
        self._reset_transforms()

        files = self._model.files()

        for file_path in files:
            node = self._model.camera_nodes(file_path)
            node.show_cloud_changed.connect(self.update)

        volume = self._model.volume_of_interest()
        volume.changed_signal.connect(self.update)

    # Default selection rectangle size
    _SELECT_RECT_SIZE = 21
    _SELECT_HALF_WIDTH = int(_SELECT_RECT_SIZE / 2)

    def frame_changed(self):
        self.update()

    def _do_selection(self, event, select_handler):
        '''Checks for a camera to be hit'''
        # INFO("Checking for hits")
        # Start picking
        gl.glSelectBuffer(512)
        gl.glRenderMode(gl.GL_SELECT)

        # Setup the projection matrix
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glPushMatrix()
        gl.glLoadIdentity()
        view_port = gl.glGetIntegerv(gl.GL_VIEWPORT)
        glu.gluPickMatrix(event.x() - self._SELECT_HALF_WIDTH, view_port[3] - event.y() - self._SELECT_HALF_WIDTH,
                          self._SELECT_RECT_SIZE, self._SELECT_RECT_SIZE, view_port)

        glu.gluPerspective(GL_FOVY, float(view_port[2]) / float(view_port[3]), GL_Z_NEAR, GL_Z_FAR)

        # Setup the modelview matrix
        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glPushMatrix()
        gl.glLoadIdentity()
        gl.glTranslatef(self._translate[0], self._translate[1], 0.0)
        glu.gluLookAt(self._eye_pos[0], self._eye_pos[1], self._eye_pos[2],
                      0, 0, 0,
                      0, 1, 0)

        gl.glInitNames()
        gl.glPushName(-1)  # Push a bogus name

        select_handler.select()

        # Stop Picking
        gl.glPopName()
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glPopMatrix()
        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glPopMatrix()
        gl.glFlush()

        # Process the hits
        buffer = gl.glRenderMode(gl.GL_RENDER)
        best_hit = None
        best_min_depth = sys.float_info.max
        for hit_record in buffer:
            # INFO("hit_record = %r" % hit_record)
            min_depth, max_depth, names = hit_record
            # INFO("names = %r, min_depth=%r" % (names, min_depth))
            if (min_depth < best_min_depth):
                best_hit = names[0]
                best_min_depth = min_depth

        return select_handler.process_best_hit(best_hit)

    def selected_object_changed(self):
        if self._selected_object:
            self._selected_object.changed_signal.disconnect(self.update)

        self._selected_object = self._model.selected_object()

        if self._selected_object:
            self._selected_object.changed_signal.connect(self.update)

    # @QtCore.Slot(bool)
    # def set_only_display_cloud_edges(self, f):
    #     self._only_display_cloud_edges = f
    #
    #     self.load_frame()

    def _plane_fit(self):
        num_points = self._compositor.num_valid_points()
        INFO("num_points = %r" % num_points)

        mean = self._compositor.points_mean()
        INFO("Mean = %r" % str(mean))

        plane = self._compositor.plane_fit()
        INFO("Least Squares plane = %r" % plane)

        (d, dist_mean, dist_std) = self._compositor.calc_distances()

        self._model.set_floor(FloorModel(mean, plane, d, dist_mean, dist_std))
