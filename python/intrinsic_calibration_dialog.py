__author__ = 'benjamin'

import numpy as np
import cv
import cv2
from PySide import QtCore, QtGui, QtUiTools

import holo3d

from holo3d_utils import *
import movie_slider
import file_loader
from camera_frame import CameraFrame

class intrinsic_calibration_dialog(QtCore.QObject):
    """This is a dialog for calibrating the intrinsic parameters of a camera"""

    def __init__(self, parent, model, camera_path):
        QtCore.QObject.__init__(self, parent)

        self._model = model
        self._camera_path = camera_path
        self._file_path = None
        self._loader = None

        # Setup the image processing dialog
        ui_loader = QtUiTools.QUiLoader()
        file = QtCore.QFile("intrinsic_calibration_dialog.ui")
        file.open(QtCore.QFile.ReadOnly)
        self._dialog = ui_loader.load(file, parent)
        file.close()

        self._dialog.exit_button.clicked.connect(self._dialog.accept)
        self._dialog.calc_button.clicked.connect(self._calc_int_params)
        self._dialog.load_video_button.clicked.connect(self._load_video)
        self._dialog.select_frame_button.clicked.connect(self._select_frame)

        self._frame_slider = movie_slider.movie_slider(self._dialog.slider_frame)
        layout = QtGui.QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self._frame_slider)
        self._dialog.slider_frame.setLayout(layout)
        self._frame_slider.time_changed.connect(self.time_changed)

        self._widget = ImageWidget(self._dialog.video_frame)
        layout = QtGui.QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self._widget)
        self._dialog.video_frame.setLayout(layout)

        self._selected_frames_list = self._dialog.selected_frames_list

        self._output_text = self._dialog.camera_parameters_textbox;

        self._delay_timer = QtCore.QTimer(self)
        self._delay_timer.setSingleShot(True)
        self._delay_timer.timeout.connect(self._do_update)

        self._painter = QtGui.QPainter()

        self._frame = None

    def _load_video(self):
        self._loader = None
        file_path = QtGui.QFileDialog.getOpenFileName(self.parent(),
                                                      self.tr("Open Calibration movie"),
                                                      self._model.project_path(),
                                                      self.tr("Holo3d Movies (*.%s)" % holo3d.DEFAULT_EXTENSION()))
        self._file_path = None
        self._loader = None
        if len(file_path[0]) == 0:
            return
        else:
            self._file_path = str(file_path[0])

        # INFO("file_name = %s" % self._file_path)
        self._dialog.video_label.setText(self._file_path)

        self._loader = file_loader.FileLoader(self._file_path)

        self._file_header = self._loader.file_header

        self._num_pixels = self._file_header.width * self._file_header.height

        codes = {'default': self._loader.frame_times()}
        colors = {'default': QtCore.Qt.red}

        self._frame_slider.set_time_info(codes, colors)

        time = self._loader.min_time()

        self.time_changed(time)

        self._selected_frames_list.clear()

    @QtCore.Slot(long)
    def time_changed(self, new_time):
        # INFO("Called: time = %d" % new_time)
        if self._file_path is None:
            return

        self._time_value = new_time

        if not self._delay_timer.isActive():
            # INFO("setting single shot")
            self._delay_timer.start(100)

    def _do_update(self):
        # INFO("Called: time = %d" % self._time_value)
        frame = self._loader.frame_at_time(self._time_value)

        color_buffer = frame.color_buffer()

        color_buffer = color_buffer.reshape((self._file_header.height, self._file_header.width, 3))

        image = OpenCVQImage(color_buffer)

        # Calculate the corners
        self._corners, self._corners_bbox = frame.corners(self._model.clipping_rect(), self._model.grid_size())
        if self._corners:
            top_left = self._model.clipping_rect()
            # Adjust the points and bbox for the clipping_rect()
            # NOTE: refactor frame.corners to do this automatically
            self._corners = [(x+top_left.x(), y+top_left.y()) for x,y in self._corners]

            self._corners_bbox = QtCore.QRect(self._corners_bbox.left() + top_left.x(),
                                              self._corners_bbox.top() + top_left.y(),
                                              self._corners_bbox.width(), self._corners_bbox.height())

            # Setup the painter for drawing
            self._painter.begin(image)

            self._painter.setCompositionMode(QtGui.QPainter.CompositionMode_SourceOver)
            pen = self._painter.pen()
            pen.setColor(QtCore.Qt.red)
            pen.setWidth(3)
            pen.setCapStyle(QtCore.Qt.RoundCap)

            self._painter.setPen(pen)
            self._painter.setRenderHint(QtGui.QPainter.Antialiasing, True)

            # Draw the corners
            idx = 0
            num_pts = len(self._corners)
            for x,y in self._corners:
                # print x,y
                pen.setColor(QtGui.QColor((num_pts - idx - 1) * 256 / num_pts, 0, idx * 256 / num_pts))
                idx += 1
                self._painter.setPen(pen)
                self._painter.drawPoint(x, y)

            # Draw the lines
            pen = self._painter.pen()
            pen.setWidth(1)
            self._painter.setPen(pen)
            idx = 0
            num_pts = len(self._corners)
            prev_point = None
            for x,y in self._corners:
                # print x,y
                pen.setColor(QtGui.QColor((num_pts - idx - 1) * 256 / num_pts, 0, idx * 256 / num_pts))
                idx += 1
                self._painter.setPen(pen)
                if prev_point is not None:
                   self._painter.drawLine(x, y, prev_point[0], prev_point[1])
                prev_point = (x, y)

            # Draw the bbox
            pen = self._painter.pen()
            pen.setColor(QtGui.QColor(0, 255, 0))
            self._painter.setPen(pen)
            self._painter.drawRect(self._corners_bbox)

            # Done painting
            self._painter.end()

        # NOTE: I have to figure out why this is causing crashes with certain videos...
        self._widget.set_image(image)

        self._frame = frame

    def _select_frame(self):
        # INFO("CALLED")
        if self._corners is None:
            ERROR("Cannot select this frame. No corners to use!!")
            return
        frame_num = self._frame.frame_number
        item = QtGui.QListWidgetItem()
        item.setText("frame %d" % frame_num)
        item.frame_num = frame_num
        item.corners = self._corners
        self._selected_frames_list.addItem(item)

    def _calc_int_params(self):
        # INFO("CALLED")

        # # Go through each frame and if a chessboard grid is found, use it to help find the intrinsic camera parameters
        # self._loader.reset()
        #
        # image_points = []
        # idx = 0
        # while True:
        #     frame = CameraFrame(self._loader)
        #     retval = self._loader.next_frame(frame)
        #     if not retval:
        #         break
        #
        #     if idx > 50:
        #         pass
        #     idx += 1
        #     # Calculate the corners
        #     corners, corners_bbox = frame.corners(self._model.clipping_rect(), self._model.grid_size())
        #     if corners is None: # Skip this frame if no corners were found
        #         continue
        #     image_points.append(np.array(corners, dtype=np.float32))

        if(self._selected_frames_list.count() < 1):
            ERROR("Select at least one frame to calibrate against!")
            return

        # Take each of the selected frames
        # Run findChessboardCorners on each one
        frames = []
        image_points = []
        for index in xrange(self._selected_frames_list.count()):
            item = self._selected_frames_list.item(index)
            num = item.frame_num
            frames.append(num)
            image_points.append(np.array(item.corners, dtype=np.float32))

        # image_points = np.array(image_points, dtype=np.float32)
        # INFO("frames = %r" % frames)

        # INFO("Num frames = %d" % len(image_points))

        # Setup the XYZ points
        x = np.linspace(-4., 4., 9)
        y = np.linspace(-2.5, 2.5, 6)

        yy, xx = np.meshgrid(y, x)

        points = np.array(zip( xx.ravel(), yy.ravel()) )
        z_axis = np.zeros(shape=(points.shape[0],))
        points = np.column_stack((points, z_axis))
        points = np.array(points, dtype=np.float32)

        object_points = [points for i in xrange(len(image_points))]

        size = (self._loader.width, self._loader.height)

        camera_matrix = [524.0, 0.0, 320.0,
                         0.0, 524.0, 240.0,
                         0.0,   0.0,   1.0]
        camera_matrix =  np.array(camera_matrix, dtype=np.float32).reshape((3,3)) # The camera calibration matrix MUST be 3x3
        dist_coeffs = [0.2402, -0.6861, 0.0, 0.0, -0.0015]
        dist_coeffs = np.array(dist_coeffs, dtype=np.float32)

        retval, camera_matrix, dist_coeffs, rvecs, tvecs = \
            cv2.calibrateCamera(object_points, image_points,
                                size, camera_matrix, dist_coeffs,
                                flags=cv.CV_CALIB_USE_INTRINSIC_GUESS)

        if not retval:
            ERROR("Camera calibration failed!")
            return

        shape = dist_coeffs.shape
        dist_coeffs = dist_coeffs.reshape((shape[1], dist_coeffs.shape[0]))

        # Print the output
        output = \
        """camera_matrix : %r
           dist_coeffs : %r""" % (camera_matrix, dist_coeffs)

        INFO(output)

        self._output_text.setText(output)

        # NOTE: THIS SHOULD FAIL!!
        self._model.set_int_camera_props(self._camera_path, camera_matrix, dist_coeffs)

        # Save the output to the current project for the selected camera
        # NOTE: do this later!!

    def show(self):
        # It has to be modal
        self._dialog.setModal(True)
        self._dialog.show()


