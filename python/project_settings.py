__author__ = 'benjamin'

from PySide import QtCore

from holo3d_utils import INFO, ERROR, enum

import os
from os import path
import yaml

class ProjectSettings(QtCore.QObject):

    VOLUME_FILTER_TYPES = enum('None', 'Volume', 'Floor')

    volume_filter_signal = QtCore.Signal(int)
    display_markers_signal = QtCore.Signal(bool)
    display_volume_signal = QtCore.Signal(bool)

    def __init__(self, parent):
        super(ProjectSettings, self).__init__(parent)

        self._project_path = None

    def reset(self):

        self._display_markers = True
        self._display_volume = False
        self._volume_filter = ProjectSettings.VOLUME_FILTER_TYPES.None

    def project_path(self):
        return self._project_path

    def set_project_path(self, path):
        self._project_path = str(path)

    def volume_filter(self):
        return self._volume_filter

    def set_volume_filter(self, flag):
        if (flag == self._volume_filter):
            return

        self._volume_filter = flag

        self.volume_filter_signal.emit(flag)

    def display_markers(self):
        return self._display_markers

    def set_display_markers(self, flag):
        if self._display_markers == flag:
            return

        self._display_markers = flag

        self.display_markers_signal.emit(flag)

    def display_volume(self):
        return self._display_volume

    def set_display_volume(self, flag):
        if self._display_volume == flag:
            return

        self._display_volume = flag

        self.display_volume_signal.emit(flag)

    def _props_file_path(self):
        return path.join(self.project_path(), 'camera_properties.yaml')

    def serialize_props(self, data):

        data['volume_filter'] = self._volume_filter
        data['display_markers'] = self._display_markers
        data['display_volume'] = self._display_volume

        stream = file(self._props_file_path(), 'w')
        yaml.dump(data, stream)
        stream.close()

        INFO("Saved camera properties to %s" % self._props_file_path())

    def deserialize_props(self, file_path):

        INFO("Loading project data from %s" % file_path)

        stream = file(file_path, 'r')
        data = yaml.load(stream)
        stream.close()

        if 'volume_filter' in data:
            filter = data['volume_filter']
            self.set_volume_filter(filter)

        if 'display_markers' in data:
            flag = data['display_markers']
            self.set_display_markers(flag)

        if 'display_volume' in data:
            flag = data['display_volume']
            self.set_display_volume(flag)

        return data
