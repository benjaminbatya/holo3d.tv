__author__ = 'benjamin'

import holo3d
import numpy as np
from PySide import QtCore, QtGui, QtOpenGL
from fusion_model import Fusion_Model
from holo3d_utils import (INFO, ERROR, enum, unproject2, draw_cube)
import os.path
import sys
import cv2
import numpy as np
import math

try:
    import OpenGL.GL as gl
    import OpenGL.GLU as glu
    import OpenGL.arrays.vbo as glvbo
    import OpenGL.arrays as GLArrays

except ImportError:
    app = QtGui.QApplication(sys.argv)
    QtGui.QMessageBox.critical(None, "OpenGL",
                               "PyOpenGL must be installed to use the module")
    sys.exit(1)

_NAMES = enum(X=0, Y=1, Z=2)

_AXIS = ( (1.0, 0.0, 0.0),
          (0.0, 1.0, 0.0),
          (0.0, 0.0, 1.0) )

_AXIS = np.array(_AXIS)

_SCALE_FACTOR = 10.0

class RotateManip:
    '''Manipulator for rotating the selected object'''

    _POINTS = None
    _NUM_POINTS = 30
    _DEG_TO_RAD = math.pi / 180

    def __init__(self, widget, file_path):
        self._widget = widget
        self._file_path = file_path

        self._angle = 0.0
        self._selected_axis = None
        self._start_pt = None
        self._curr_pt = None

        if RotateManip._POINTS is None:
            INFO("Generating _POINTS")
            angles = np.ndarray((RotateManip._NUM_POINTS,), dtype=np.float32)
            deg_per_point = 360.0 / RotateManip._NUM_POINTS
            for i in xrange(RotateManip._NUM_POINTS):
                angles[i] = RotateManip._DEG_TO_RAD * deg_per_point * i

            x = np.zeros(RotateManip._NUM_POINTS, dtype=np.float32)
            y = np.cos(angles) * _SCALE_FACTOR
            z = np.sin(angles) * _SCALE_FACTOR

            RotateManip._POINTS = np.vstack((x, y, z)).T

    def _draw_circle(self):

        gl.glBegin(gl.GL_LINE_LOOP)
        for i in xrange(RotateManip._NUM_POINTS):
            pt = RotateManip._POINTS[i]
            gl.glVertex3f(pt[0], pt[1], pt[2])
        gl.glEnd()

    def draw(self, file_path):
        if(self._file_path != file_path):
            return

        if(self._start_pt is None):
            # Capture the various matrices so they can be used in the updating
            # Note: This is inefficient but it works...
            self._modelview_mat = gl.glGetDoublev(gl.GL_MODELVIEW_MATRIX)
            #INFO("modelview matrix = %r" % self._modelview_mat)
            self._projection_mat = gl.glGetDoublev(gl.GL_PROJECTION_MATRIX)
            #INFO("projection matrix = %r" % self._projection_mat)
            self._viewport = gl.glGetIntegerv(gl.GL_VIEWPORT)
            #INFO("viewport = %r" % self._viewport)

        # gl.glBegin(gl.GL_LINES)
        # self._widget.set_color((1.0, 0.0, 0.0, 1.0))
        # gl.glVertex3f(0,0,0)
        # gl.glVertex3f(5,0,0)
        # self._widget.set_color((0.0, 1.0, 0.0, 1.0))
        # gl.glVertex3f(0,0,0)
        # gl.glVertex3f(0,5,0)
        # self._widget.set_color((0.0, 0.0, 1.0, 1.0))
        # gl.glVertex3f(0,0,0)
        # gl.glVertex3f(0,0,5)
        # gl.glEnd()
        #
        # # Draw a cube at (10, 0, 0)
        # self._widget.set_color((0.8, 0.8, 0.8, 1.0))
        # gl.glPushMatrix()
        # gl.glTranslate(10, 0, 0)
        # draw_cube(False)
        # gl.glPopMatrix()

        # if self._start_pt is not None:
        #     self._widget.set_color((0.5, 0.5, 0.5, 1.))
        #     gl.glBegin(gl.GL_LINES)
        #     gl.glVertex3f(0.0, 0.0, 0.0)
        #     gl.glVertex3f(self._start_pt[0], self._start_pt[1], self._start_pt[2])
        #     gl.glEnd()
        #
        # if self._curr_pt is not None:
        #     self._widget.set_color((1.0, 1.0, 1.0, 1.))
        #     gl.glBegin(gl.GL_LINES)
        #     gl.glVertex3f(0.0, 0.0, 0.0)
        #     gl.glVertex3f(self._curr_pt[0], self._curr_pt[1], self._curr_pt[2])
        #     gl.glEnd()

        self._widget.set_color((1., 0., 0., 1.,))
        if self._selected_axis is not None and np.allclose(self._selected_axis, _AXIS[_NAMES.X]):
            self._widget.set_color((1., 1., 1., 1.,))

        self._draw_circle()

        self._widget.set_color((0., 1., 0., 1.,))
        if self._selected_axis is not None and np.allclose(self._selected_axis, _AXIS[_NAMES.Y]):
            self._widget.set_color((1., 1., 1., 1.,))

        gl.glPushMatrix()
        gl.glRotatef(90.0, 0.0, 0.0, 1.0)
        self._draw_circle()
        gl.glPopMatrix()

        self._widget.set_color((0., 0., 1., 1.,))
        if self._selected_axis is not None and np.allclose(self._selected_axis, _AXIS[_NAMES.Z]):
            self._widget.set_color((1., 1., 1., 1.,))

        gl.glPushMatrix()
        gl.glRotatef(90.0, 0.0, 1.0, 0.0)
        self._draw_circle()
        gl.glPopMatrix()

    def apply_transform(self, file_path):
        '''Applies the transform in this manip'''
        if self._selected_axis is None: return

        # Don't apply the transform if the file_path doesn't match
        if(self._file_path != file_path): return

        gl.glRotatef(math.degrees(self._angle),
                     self._selected_axis[0],
                     self._selected_axis[1],
                     self._selected_axis[2])

    def select(self):

        node = self._widget._model.camera_nodes(self._file_path)
        if node is None:
            return

        rot_mat = node.matrix()

        # Draw the final camera position
        gl.glPushMatrix()

        gl.glMultMatrixf(rot_mat)

        self.apply_transform(self._file_path)

        gl.glLoadName(_NAMES.X)
        self._draw_circle()

        gl.glLoadName(_NAMES.Y)
        gl.glPushMatrix()
        gl.glRotatef(90.0, 0.0, 0.0, 1.0)
        self._draw_circle()
        gl.glPopMatrix()

        gl.glLoadName(_NAMES.Z)
        gl.glPushMatrix()
        gl.glRotatef(90.0, 0.0, 1.0, 0.0)
        self._draw_circle()
        gl.glPopMatrix()

        gl.glPopMatrix()

    def process_best_hit(self, best_hit):
        self._selected_axis = None
        if best_hit is not None:
            # INFO("Selected axis = %r" % best_hit)
            self._selected_axis = _AXIS[best_hit]

        return self._selected_axis

    def _calcClosestPtToCircle(self, event):
        # These are in global space
        (ray_start, ray_dir) = unproject2(event.x(), event.y(),
                                          self._modelview_mat,
                                          self._projection_mat,
                                          self._viewport)
        axis = self._selected_axis

        dot_prod = axis.dot(ray_dir)

        # Check that the axis and ray_dir are not orthogonal to each other
        if np.abs(dot_prod) < 0.000001:
            ERROR("axis and ray_dir are orthogonal to each other")
            return None

        # First calculate intersection point of the axis plane and ray_dir
        # From http://geomalgorithms.com/a05-_intersect-1.html
        # P_0 is the origin because everything is computed in the Node's local space
        w = ray_start
        temp = -axis.dot(w)
        s_1 = temp / dot_prod

        temp2 = ray_dir * s_1
        intersection_pt = ray_start + temp2

        # Then calculate the difference vector and normalize it
        diff_vec = intersection_pt
        diff_mag = np.sqrt(diff_vec.dot(diff_vec))
        diff_vec /= diff_mag

        # Convert back into a row vector
        return diff_vec * 10.0

    def handle_press_event(self, event):
        '''Returns True if event has handled here, False otherwise'''
        if self._selected_axis is None:
            return False

        self._start_pt = self._calcClosestPtToCircle(event)

        # INFO("start_pt = %r" % self._start_pt)

        # self._origin = (0.0, 0.0, 0.0)

        return True

    def handle_move_event(self, event):
        '''Returns True if event has handled here, False otherwise'''
        if self._selected_axis is None:
            return False

        self._curr_pt = self._calcClosestPtToCircle(event)

        # INFO("curr_pos = %r" % self._curr_pt)

        # Calculate the angle between start_pt and curr_pt
        self._angle = self._start_pt.dot(self._curr_pt) / 100.0
        self._angle = math.acos(self._angle)

        return True

    def handle_release_event(self, event):
        '''Returns True if event has handled here, False otherwise'''
        if self._selected_axis is None:
            return False

        # Apply the transformation to the target camera
        node = self._widget._model.camera_nodes(self._file_path)
        rotation = node.rotation()
        vec = self._selected_axis * self._angle
        new_rot = rotation + vec
        node.set_rotation(new_rot)

        self._selected_axis = None
        self._start_pt = None
        self._curr_pt = None

        self._angle = 0.0

        return True
