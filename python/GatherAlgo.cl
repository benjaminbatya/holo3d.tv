
// NOTE: the PYOPENCL_COMPILER_OUTPUT environment variable MUST be set to 1 for this to not complain
// See http://stackoverflow.com/questions/20722242/compilerwarning-with-opencl
// I have a idea that printf doesn't work well if there is more the one context initialized at the same time
#define INFO(_format, ...) {\
    printf((__constant char *)"\n(%u,%u,%u) ", get_global_id(0), get_group_id(0), get_local_id(0)); \
    printf((__constant char *)(_format), __VA_ARGS__); \
}

__kernel void count_valid(  __global uchar4* colors,
                            const uint num_points,
                            __global uint* count)
{
    uint gid = get_global_id(0);

    uint pt_idx = gid*num_points;
    for(int i=0; i<num_points; i++, pt_idx++)
    {
        if(colors[pt_idx].s3 > 0)
        {
            count[gid]++;
        }
    }
}

__kernel void copy_valid(   __global float4* points,
                            __global uchar4* colors,
                            const uint num_points,
                            __global uint* offsets,
                            __global float* out_points)
{
    uint gid = get_global_id(0);

    uint offset = offsets[gid];

    uint pt_idx = gid*num_points;
    for(int i=0; i<num_points; i++, pt_idx++)
    {
        if(colors[pt_idx].s3 > 0)
        {
            float4 pt = points[pt_idx];
            out_points[3*offset+0] = pt.s0;
            out_points[3*offset+1] = pt.s1;
            out_points[3*offset+2] = pt.s2;
            offset++;
        }
    }
}