__author__ = 'benjamin'

import numpy as np

from holo3d_utils import INFO, ERROR


MAX_ITERATIONS = 20
MIN_POINTS_IN_ESTIMATE = 10

class TrackerSettings(object):
    def __init__(self, left, right, bottom, top, radius, percent_samples,
                 min_points=MIN_POINTS_IN_ESTIMATE, max_iterations=MAX_ITERATIONS,
                 origin=None, rot_mat=None):
        self.left = int(left)
        self.right = int(right)
        self.bottom = int(bottom)
        self.top = int(top)

        self.radius = radius
        self.radius_sq = self.radius**2
        self.percent_samples = percent_samples
        self.min_points = min_points
        self.max_iterations = max_iterations
        self.origin = (None if origin is None else origin.astype(np.float64))
        self.rot_mat = (None if rot_mat is None else rot_mat.astype(np.float64)) # This is to handle the rotation applied to the containing volume

    def tuple(self):
        if self.origin is None or self.rot_mat is None:
            return (self.left, self.right, self.bottom, self.top,
                    self.radius, self.percent_samples, self.min_points, self.max_iterations)
        else:
            return (self.left, self.right, self.bottom, self.top,
                    self.radius, self.percent_samples, self.min_points, self.max_iterations,
                    self.origin, self.rot_mat)

class Cluster(object):
    def __init__(self, mean, width_sq=None, height_sq=None, settled=None, num_points = None):
        # This is a hack for method overloading
        if width_sq is None:
            width_sq = mean[1]
            height_sq = mean[2]
            settled = mean[3]
            num_points = mean[4]
            mean = mean[0]

        self.mean = mean
        self.width_sq = width_sq
        self.height_sq = height_sq
        self.settled = settled
        self.num_points = ( 0 if num_points is None else num_points)

    def __str__(self):
        ret = "(mean:%r, width_sq:%r, height_sq:%r, settled:%r" % (self.mean, self.width_sq, self.height_sq, self.settled)
        if self.num_points is not None:
            ret += ", num_points:%r" % self.num_points

        return ret

    def __eq__(self, other):
        return self.equal(other)

    def __ne__(self, other):
        return not self.equal(other)

    def tuple(self):
        return (self.mean, self.width_sq, self.height_sq, self.settled, self.num_points)

    def equal(self, other, tol=0.0001, report=False):
        # print "idx = %r, ocl[0]=%r, naive[0]=%r" % (idx, updated_clusters_ocl[idx][0], updated_clusters_naive[idx][0])
        if not np.allclose(self.mean, other.mean, atol=tol):
            if report: ERROR("mean: %r != %r" % ( self.mean, other.mean ) )
            return False

        # Note: I think that ocl and naive have to be compared to 0.001 precision because
        # in opencl we are using float precision and in naive we are using double precision
        if not np.allclose(self.width_sq, other.width_sq, atol=tol):
            if report: ERROR("width: %r != %r" % (self.width_sq, other.width_sq))
            return False

        if not np.allclose(self.height_sq, other.height_sq, atol=tol):
            if report: ERROR("height: %r != %r" % (self.height_sq, other.height_sq))
            return False

        if self.settled != other.settled:
            if report: ERROR("settled: %r != %r" % (self.settled, other.settled))
            return False

        return True


class BaseTracker(object):
    def __init__(self, settings):
        self.settings = settings

    def set_points(self, points):
        '''
        sets the internal points
        :param points: must be convertible to numpy.ndarray format
        :return: none
        '''
        self._points = np.array(points)

    def points(self):
        return self._points

    def num_points(self):
        return len(self._points)

    def set_clusters(self, clusters):
        '''
        :param clusters: Are of Cluster type
        :return:
        '''
        self._clusters = clusters

    def clusters(self):
        return self._clusters

    def num_clusters(self):
        return len(self._clusters)

    def create_clusters(self):

        num_points = self._points.shape[0]
        num_samples = int(num_points * self.settings.percent_samples)

        indices = np.random.choice(num_points, num_samples)

        clusters = []
        for i in indices:
            # first index is the mean of the cluster
            # Second index is the width squared
            # Third index is the height squared
            # Fourth index indicates if the cluster is settled or not
            clusters.append(Cluster(self._points[i], self.settings.radius_sq, self.settings.radius_sq, False))

        self.set_clusters(clusters)


