__author__ = 'benjamin'

import numpy as np
import math

from base_tracker import BaseTracker, Cluster

from holo3d_utils import INFO
from base_funcs import *

class NaiveTracker(BaseTracker):
    '''
    This implements naive, basic tracking and clustering algorithms.
    It is inherited by OCLTracker to implement faster algorithms
    '''

    def __init__(self, settings):
        super(NaiveTracker, self).__init__(settings)

    def regen_clusters(self):
        clusters = self.cluster_points(self.clusters(), self._points)

        self.set_clusters(clusters)

        return self.clusters()

    def merge_clusters(self, clusters):
        # print "iteration %r, num _clusters = %r" % (i, len(clusters))

        num_clusters = len(clusters)

        # merge together _clusters if their means are closer then the greater radius
        # num_unsettled = 0
        new_clusters = []
        for est_i in xrange(num_clusters):
            c_i = clusters[est_i]
            mean_i = c_i.mean
            # print "Forward merge: elem %r, mean = %s" % (est_i, mean_i)

            removed_i = False

            for est_j in xrange(est_i+1, num_clusters):
                c_j = clusters[est_j]
                mean_j = c_j.mean
                width_sq_j = c_j.width_sq
                height_sq_j = c_j.height_sq

                diff_vec = mean_i - mean_j
                delta_sq = diff_vec.dot(diff_vec)

                if delta_sq < (width_sq_j + height_sq_j):
                    # print "Removing cluster %r because of cluster %r" % (est_i, est_j)
                    removed_i = True
                    break

            if not removed_i:
                # print "Forward merge: Saving estimate %r" % (est_i) #, mean_i, math.sqrt(radius_sq_i))
                new_clusters.append(c_i)
                # if not settled_i:
                #     num_unsettled += 1

        clusters = new_clusters
        num_clusters = len(clusters)

        # print "After pruning, num clusters = %r" % len(clusters)

        # merge in reverse
        num_unsettled = 0
        new_clusters = []
        for est_i in xrange(num_clusters-1, -1, -1):

            c_i = clusters[est_i]
            mean_i = c_i.mean

            removed_i = False

            for est_j in xrange(est_i-1, -1, -1):
                c_j = clusters[est_j]
                mean_j = c_j.mean
                width_sq_j = c_j.width_sq
                height_sq_j = c_j.height_sq

                diff_vec = mean_i - mean_j
                delta_sq = diff_vec.dot(diff_vec)

                # if(i >= 4) and (est_i==0):
                #     pass

                if delta_sq < (width_sq_j + height_sq_j):
                    # print "Removing duplicate estimate %i (mean=%r, radius=%r) because of estimate %r" % \
                    #       (est_i, mean_i, math.sqrt(radius_sq_i), est_j)
                    removed_i = True
                    break

            if not removed_i:
                # print "Reverse merge: Saving estimate %r" % (est_i) #, mean_i, math.sqrt(radius_sq_i))
                new_clusters.append(c_i)
                if not c_i.settled:
                    num_unsettled += 1

        return (new_clusters, num_unsettled)

    def partitioned_merge_clusters(self, clusters):

        # Calculate the min and max
        min = np.array((+1000000, +1000000), dtype=np.float64)
        max = np.array((-1000000, -1000000), dtype=np.float64)

        # Make sure that we are dealing with more then one cluster
        if len(clusters) < 2:
            return (clusters, 0)

        for c in clusters:
            mean = c.mean
            if(mean[0] < min[0]): min[0] = mean[0]
            if(mean[0] > max[0]): max[0] = mean[0]

            if(mean[1] < min[1]): min[1] = mean[1]
            if(mean[1] > max[1]): max[1] = mean[1]

        # Round min and max
        min = np.floor(min)
        max = np.ceil(max)

        # Partition the area into grid from min to max
        # and into pieces of size self.radius*2 or smaller
        cell_size = self.settings.radius * 2
        x_cell_count = round_up_to_multiple(max[0] - min[0], cell_size) / cell_size
        y_cell_count = round_up_to_multiple(max[1] - min[1], cell_size) / cell_size

        # Create a grid of cells
        # From http://stackoverflow.com/questions/6142689/initialising-an-array-of-fixed-size-in-python
        cell_grid = np.empty((x_cell_count, y_cell_count), dtype=object)
        # Assign the clusters to the grid

        for c in clusters:
            mean = c.mean
            idx = ((mean - min) / cell_size).astype(np.int32)
            if cell_grid[idx[0], idx[1]] is None: cell_grid[idx[0], idx[1]] = []
            cell_grid[idx[0], idx[1]].append(c)
            # INFO("Assigning cluster %s to cell (%s, %s)" %(c, idx[0], idx[1]))

        # Run merge_clusters_in_cell on each cell
        new_grid = np.empty((x_cell_count, y_cell_count), dtype=object)
        # From http://docs.scipy.org/doc/numpy/reference/arrays.nditer.html#tracking-an-index-or-multi-index
        it = np.nditer(cell_grid, flags=['multi_index', 'refs_ok'])
        while not it.finished:
            cl = it[0].tolist()

            if cl is not None:
                (new_clusters, unsettled) = self.merge_clusters(cl)
                idx = it.multi_index
                new_grid[idx[0], idx[1]] = new_clusters
                # INFO("After merging: len(new_clusters)=%s in cell (%s, %s)" %(len(new_clusters), idx[0], idx[1]))

            it.iternext()

        new_clusters = []
        it = np.nditer(new_grid, flags=['multi_index', 'refs_ok'])
        while not it.finished:
            clusters = it[0].tolist()
            if clusters is not None:
                new_clusters = new_clusters + clusters
            it.iternext()

        # for c in new_clusters:
        #     INFO("Before final merge: c=%s" % c)

        (new_clusters, unsettled) = self.merge_clusters(new_clusters)

        return (new_clusters, unsettled)

    def run_EM(self, clusters, points):
        # num_unsettled = 0

        # Run EM and check for settled clusters
        new_clusters = []
        for cluster in clusters:
            # If the cluster is settled already, don't run EM on it again
            if cluster[3] is True:
                new_clusters.append(cluster)
                continue

            # Try different sizes for maximization
            indices = maximize(points, cluster.mean, cluster.width_sq, cluster.height_sq) # self.radius_sq, self.radius_sq) # cluster[1]*1.1, cluster[2]*1.1) #

            # Only save the cluster if it contains more then the required minimum number of points
            if len(indices) >= self.settings.min_points:
                (mean, width_sq, height_sq) = expectation(points, indices)

                if np.allclose(mean, cluster[0]): # and np.allclose(width_sq, cluster[1]) and np.allclose(height_sq, cluster[2]):
                    # Mark the cluster as settled
                    new_clusters.append(Cluster(mean, width_sq, height_sq, True, num_points=len(indices)))
                    # print "Settled: mean=%r, radius=%r" % ( mean, math.sqrt(radius_sq) )

                else:
                    # The cluster is still unsettled
                    new_clusters.append(Cluster(mean, width_sq, height_sq, False, num_points=len(indices)))
                    # num_unsettled += 1
                    # print "New cluster: mean=%r, radius=%r" % ( mean, math.sqrt(radius_sq) )

        return new_clusters

    def run_new_EM(self, clusters, points):

        # For each of the points find the closest point
        num_clusters = len(clusters)
        num_points = len(points)

        min_dist = [None] * num_points
        closest_cluster = [None] * num_points

        for idx in xrange(num_points):
            pt = points[idx]
            for c_idx in xrange(num_clusters):
                c = clusters[c_idx]
                mean = c.mean
                width_sq = c.width_sq
                height_sq = c.height_sq
                diff_vec = pt - mean
                delta_sq = diff_vec.dot(diff_vec)

                if(delta_sq < (width_sq + height_sq)):
                    if(closest_cluster[idx] is None or delta_sq < min_dist[idx]):
                        closest_cluster[idx] = c_idx
                        min_dist[idx] = delta_sq

        # Gather the points into clusters
        index_list = [None] * num_clusters
        for idx in xrange(num_points):
            c_idx = closest_cluster[idx]
            if c_idx is None: continue
            if index_list[c_idx] is None:
                index_list[c_idx] = []
            index_list[c_idx].append(idx)

        # Calculate the new mean and size of each clusters
        new_clusters = []
        for c_idx in xrange(num_clusters):
            cluster = clusters[c_idx]

            indices = index_list[c_idx]
            # if indices is None: continue

            if indices is not None and len(indices) >= self.settings.min_points:
                (mean, width_sq, height_sq) = expectation(points, indices)

                # if c_idx == 35:
                #     print "cluster 35: height_sq = %r" % height_sq
                #     print "indices = %r" % indices
                #     print "points = \n%r" % points[indices]

                if np.allclose(mean, cluster.mean): # and np.allclose(width_sq, cluster.width_sq) and np.allclose(height_sq, cluster.height_sq):
                    # Mark the cluster as settled
                    new_clusters.append(Cluster(mean, width_sq, height_sq, True, num_points=len(indices)))
                    # print "Settled cluster: mean=%r, size=%r" % ( mean, (width_sq, height_sq) )

                else:
                    # The cluster is still unsettled
                    new_clusters.append(Cluster(mean, width_sq, height_sq, False, num_points=len(indices)))
                    # num_unsettled += 1
                    # print "Unsettled cluster: mean=%r, size=%r" % ( mean, (width_sq, height_sq) )
            # else:
            #     new_clusters.append(Cluster(cluster.mean, cluster.width_sq, cluster.height_sq, True, num_points=len(indices)))
            #     # print "cluster %r, count=%r" % (c_idx, (0 if indices is None else len(indices) ) )

        return new_clusters

    def cluster_points_slow(self, clusters, points):
        # INFO("cluster_points: called, num clusters = %r" % len(clusters))

        for i in xrange(self.settings.max_iterations):

            (clusters, num_unsettled) = self.merge_clusters(clusters)

            # INFO("iteration %r, After pruning, num unsettled _clusters = %r" % (i, num_unsettled))
            # for c in clusters:
            #     INFO("clusters = %s" % c)

            # Break if there are no more unsettled groups
            if num_unsettled == 0:
                break

            clusters = self.run_new_EM(clusters, points)

            # print "After EM: number of unsettled _clusters = %r" %  num_unsettled

            # ax = fig.add_subplot(1,1,1)
            #
            # for (mean, radius_sq) in _clusters:
            #     c = plt.Circle(mean, radius=math.sqrt(radius_sq), color=colors[i], fill=False)
            #     ax.add_patch(c)


        # print "Finished iterating, num _clusters = %r" % len(_clusters)

        return clusters

    def cluster_points(self, clusters, points):
        # INFO("cluster_points: called, num clusters = %r" % len(clusters))

        for i in xrange(self.settings.max_iterations):

            (clusters, num_unsettled) = self.partitioned_merge_clusters(clusters)

            # INFO("iteration %r, After pruning, num unsettled _clusters = %r" % (i, num_unsettled))
            # for c in clusters:
            #     INFO("clusters = %s" % c)

            # Break if there are no more unsettled groups
            if num_unsettled == 0:
                break

            clusters = self.run_new_EM(clusters, points)

            # print "After EM: number of unsettled _clusters = %r" %  num_unsettled

            # ax = fig.add_subplot(1,1,1)
            #
            # for (mean, radius_sq) in _clusters:
            #     c = plt.Circle(mean, radius=math.sqrt(radius_sq), color=colors[i], fill=False)
            #     ax.add_patch(c)


        # print "Finished iterating, num _clusters = %r" % len(_clusters)

        return clusters

    def track_map_points_to_indices(self):
        clusters = self.clusters()
        points = self.points()

        num_clusters = self.num_clusters()
        num_points = self.num_points()

        mapped_idxs = np.full((num_points,), num_clusters, dtype=np.int32)

        # Short circuit the mapping phase
        if num_clusters < 1:
            print("track_map_points_to_indices() ERROR: num clusters = 0")
            return mapped_idxs

        for pt_idx in xrange(len(points)):
            pt = points[pt_idx]

            # if(pt_idx<10): print("idx=%r: pt = %r" % (pt_idx, pt))

            # Find the closest cluster
            best_cluster = num_clusters
            min_dist_sq = 10**8
            for cluster_idx in xrange(num_clusters):
                delta = clusters[cluster_idx].mean - pt
                # if(pt_idx==0): print("cluster=%r, cluster_pos=%r, pt=%r" % (cluster_idx, clusters[cluster_idx][0], pt))
                # if(pt_idx==0): print("clster=%r, delta=%r" % (cluster_idx, delta))
                dist_sq = delta.dot(delta)
                # if(pt_idx==0): print("clster=%r, dist_sq=%r, min_dist_sq=%r" % (cluster_idx, dist_sq, min_dist_sq))
                if dist_sq < min_dist_sq:
                    best_cluster = cluster_idx
                    min_dist_sq = dist_sq

            # If the closest cluster is more self.radius_sq away, then mark point as untracked
            if min_dist_sq > (clusters[best_cluster].width_sq + clusters[best_cluster].height_sq):
                best_cluster = num_clusters

            mapped_idxs[pt_idx] = best_cluster

        return mapped_idxs

    def track_reduce_points(self):

        mapped_idxs = self.track_map_points_to_indices()

        num_points = len(self._points)
        num_mapped_groups = len(self._clusters)+1

        clustered_idx = []
        for idx in xrange(num_mapped_groups):
            clustered_idx.append([])

        for pt_idx in xrange(num_points):
            cluster = mapped_idxs[pt_idx]
            clustered_idx[cluster].append(pt_idx)

        self.clustered_idx = clustered_idx

        return clustered_idx

    def track_recalc_existing_clusters(self):

        self.track_reduce_points()

        # Only use the tracked point indices
        # The last one is untracked point indices
        clustered_idx = self.clustered_idx[0:-1]

        new_clusters = []

        # Ignore the untracked points
        num_clusters = len(clustered_idx)

        for cluster_idx in xrange(num_clusters):
            clustered = clustered_idx[cluster_idx]

            # Skip _clusters with a few number of points
            # NOTE: this should only happen if the cluster is beyond the boundary
            if clustered is None or len(clustered) < self.settings.min_points:
                continue

            clustered_pts = self._points[clustered]

            mean = np.sum(clustered_pts, axis=0)
            mean /= len(clustered)

            cluster_min = np.nanmin(clustered_pts, axis=0)
            cluster_max = np.nanmax(clustered_pts, axis=0)

            abs_min = np.abs(mean - cluster_min)
            abs_max = np.abs(mean - cluster_max)

            width = max(abs_min[0], abs_max[0])
            height = max(abs_min[1], abs_max[1])

            width_sq = width**2
            height_sq = height**2

            new_clusters.append(Cluster(mean, width_sq, height_sq, False, num_points=len(clustered)))

        return new_clusters

    def track_clusters(self):

        # (clusters, points) = (self._clusters, self._points)

        updated_clusters = self.track_recalc_existing_clusters()

        # Only track new _clusters that are outside the bounds
        # NOTE: This could be close to the bounds as well
        untracked_idx = self.clustered_idx[-1]
        valid_pts = []
        # INFO("num untracked points=%r" % len(untracked_idx))

        if self.settings.origin != None and self.settings.matrix != None:
             for idx in untracked_idx:
                pt = self._points[idx]
                temp_pt = pt - self.settings.origin
                temp_pt2 = self.settings.matrix.dot(temp_pt)
                transformed_pt = temp_pt2 + self.settings.origin
                # if transformed_pt[0] < -1 or transformed_pt[0] > 1 or transformed_pt[1] < -1 or transformed_pt[1] > 1:
                if transformed_pt[0] < self.settings.left or transformed_pt[0] > self.settings.right or transformed_pt[1] < self.settings.bottom or transformed_pt[1] > self.settings.top:
                    valid_pts.append(pt)
        else:
            for idx in untracked_idx:
                pt = self._points[idx]
                if pt[0] < self.settings.left or pt[0] > self.settings.right or pt[1] < self.settings.bottom or pt[1] > self.settings.top:
                    valid_pts.append(pt)

        if len(valid_pts) > 0:
            # INFO("num valid points=%r" % len(valid_pts))
            # untracked_pts = np.array(untracked_pts)
            untracked_pts = np.array(valid_pts)

            num_points = untracked_pts.shape[0]
            num_samples = int(num_points * self.settings.percent_samples)

            indices = np.random.choice(num_points, num_samples)

            new_clusters = []
            for i in indices:
                # first index is the mean of the cluster
                # Second index is the width squared
                # Third index is the height squared
                # Fourth index indicates if the cluster is settled or not
                new_clusters.append(Cluster(untracked_pts[i], self.settings.radius_sq, self.settings.radius_sq, False))

            new_clusters = self.cluster_points(new_clusters, untracked_pts)

            updated_clusters = updated_clusters + new_clusters

        self.set_clusters(updated_clusters)

        return updated_clusters

if __name__ == "__main__":
    import unittest
    import time

    from holo3d import CPPTracker

    (LEFT, RIGHT, BOTTOM, TOP) = (-200, 200, -200, 200)
    (RADIUS, PERCENT_SAMPLES) = (30.0, 0.1)

    # class Test_NaiveTracker(unittest.TestCase):
    #
    #     def setUp(self):
    #         init_rand()
    #
    #         self.naive = NaiveTracker(LEFT, RIGHT, TOP, BOTTOM, RADIUS, PERCENT_SAMPLES)
    #
    #         self.blobs = gen_blobs(LEFT, RIGHT, BOTTOM, TOP) #, 20, 200)
    #
    #         (self.indices, self.points, self.clusters) = init_data(self.blobs, PERCENT_SAMPLES, RADIUS**2)
    #
    #         self.naive.set_points(self.points)
    #
    #         self.naive.set_clusters(self.clusters)
    #
    #         self.naive.regen_clusters()
    #
    #     def tearDown(self):
    #         pass

        # def test_partitioned_cluster(self):
        #     start = time.clock()
        #
        #     serial_clusters = self.naive.merge_clusters(self.clusters)
        #
        #     end = time.clock()
        #     delta = end - start
        #     print("delta for serial_merge_clusters = %r ms" % (delta*1000))
        #     start = time.clock()
        #
        #     partitioned_clusters = self.naive.partitioned_merge_clusters(self.clusters)
        #
        #     end = time.clock()
        #     delta = end - start
        #     print("delta for partitioned_merge_clusters = %r ms" % (delta*1000))
        #
        #     self.assertEquals(len(partitioned_clusters), len(serial_clusters))
        #
        #     for idx in xrange(len(partitioned_clusters)):
        #         self.assertEquals(partitioned_clusters[idx][0].tolist(), serial_clusters[idx][0].tolist())
        #         self.assertEquals(partitioned_clusters[idx][1], serial_clusters[idx][1])
        #         self.assertEquals(partitioned_clusters[idx][2], serial_clusters[idx][2])
        #         self.assertEquals(partitioned_clusters[idx][3], serial_clusters[idx][3])

    unittest.main()
