
// NOTE: the PYOPENCL_COMPILER_OUTPUT environment variable MUST be set to 1 for this to not complain
// See http://stackoverflow.com/questions/20722242/compilerwarning-with-opencl
// I have a idea that printf doesn't work well if there is more the one context initialized at the same time
#define INFO(_format, ...) {\
    printf((__constant char *)"\n(%u,%u,%u) ", get_global_id(0), get_group_id(0), get_local_id(0)); \
    printf((__constant char *)(_format), __VA_ARGS__); \
}

#define ASSERT(x) { \
    if(!(x)) { \
        INFO("Assert(%s) failed at line %d", #x, __LINE__); \
        return; \
	}; \
}
//
//typedef struct _Estimate_t
//{
//    float4 mean;
//    float radius_sq;    // The radius of the estimate
//    float settled;      // FALSE means unsettled, TRUE means settled
//    float active;       // FALSE means inactive, TRUE means active
//    float pad;
//} Estimate_t;
//
//__constant const float FALSE_F = 0.0f;
//__constant const float TRUE_F = 1.0f;
//
//__kernel void init_estimates(   __global float4* in_points,
//                                __global uint*   in_indices,
//                                const float radius_sq,
//                                __global Estimate_t* out_estimates)
//{
//    int gid = get_global_id(0);
//
//    uint index = in_indices[gid];
//
//    if (gid == 0)
//    {
//        INFO("Indice = %d", index);
//    }
//    const float4 point = in_points[index];
//
//    out_estimates[gid].mean = point;
//    out_estimates[gid].radius_sq = radius_sq;
//    out_estimates[gid].settled = FALSE_F;
//    out_estimates[gid].active = TRUE_F;
//}
//
//__kernel void prune_estimates(  __constant int* params,
//                                __global Estimate_t* estimates,
//                                __global float4* points)
//{
//    int gid = get_global_id(0);
//    int est_total = get_global_size(0);
//
//    const Estimate_t est_i = estimates[gid];
//
//    // This estimate is already inactive
//    if(est_i.active == FALSE_F)
//    {
//        return;
//    }
//
//    bool remove = false;
//    for(int j=gid+1; j<est_total; j++)
//    {
//        const Estimate_t est_j = estimates[j];
//        float4 diff_vec = est_i.mean - est_j.mean;
//        float delta_sq = dot(diff_vec, diff_vec);
//
//        if(delta_sq < est_j.radius_sq)
//        {
//            remove = true;
//            break;
//        }
//    }
//
//    if(remove)
//    {
//        estimates[gid].active = FALSE_F;
//    }
//}
//
//__kernel void count_unsettled_estimates(__global int* params,
//                                        __global Estimate_t* estimates,
//                                        __global float4* points)
//{
//    int gid = get_global_id(0);
//
//    ASSERT(gid == 0);
//
//    int num_estimates = params[0];
//    int num_unsettled = 0;
//    int num_active = 0;
//
//    for(int i=0; i<num_estimates; i++)
//    {
//        const Estimate_t est = estimates[i];
//        if(est.active == TRUE_F)
//        {
//            num_active++;
//            if(est.settled == FALSE_F)
//            {
//                num_unsettled++;
//            }
//        }
//    }
//
//    params[1] = num_unsettled;
//    params[2] = num_active;
//}
//
//__kernel void update_mean(  __constant int* params,
//                            const int estimate_index,
//                            __global Estimate_t* estimates,
//                            __global float4* points,
//                            __locals float4* temp_sums)
//{
//    int gid = get_global_id(0);
//    int group_size = get_global_size(0);
//    int max_size = params[0];
//    int idx = gid*group_size;
//    for(int i=0; i<group_size && idx<max_size; i++,idx++)
//    {
//        temp_sums[i] = points[idx];
//    }
//
//    barrier()
//}
//
//__kernel void maximize_estimates(   __constant int* params,
//                                    __global Estimate_t* estimates,
//                                    __global float4* points)
//{
//    int gid = get_global_id(0);
//
//}
//
//__kernel void update_estimates( __constant int* params,
//                                __global Estimate_t* estimates,
//                                __global float4* points)
//{
//    int gid = get_global_id(0);
//}

__kernel void volume_count( const uint num_points,
                            const uint total_points,
                            __constant float4* volume,
                            __global float4* all_points,
                            __global uint* out_counts)
{
    uint gid = get_global_id(0);

    uint pt_idx = gid*num_points;
    uint count = 0;
    for(int i=0; i<num_points && pt_idx<total_points; i++, pt_idx++)
    {
        float4 point = all_points[pt_idx];

//        // Check for filtering
//        float w = dot(volume[3], point);
//        float x = dot(volume[0], point) / w;
//        float y = dot(volume[1], point) / w;
//        float z = dot(volume[2], point) / w;
//
//        if(-1.0 <= x && x <= 1.0 &&
//           -1.0 <= y && y <= 1.0 &&
//           -1.0 <= z && z <= 1.0 )
        {
            count++;
            point.s3 = 0.0f;
            all_points[pt_idx] = point;
        }
    }

    out_counts[gid] = count;
}

// This does an serial inclusive scan of the counts
// It's naive but avoids copying to the host
__kernel void prefix_sum(   const uint num_counts,
                            __global uint* in_counts,
                            __global uint* out_offsets)
{
    int gid = get_global_id(0);
    ASSERT(gid == 0);

    int acc = 0;
    for(int i=0; i<num_counts; i++)
    {
        acc += in_counts[i];
        out_offsets[i] = acc;
    }
}

__kernel void copy_points(  const uint num_points,
                            const uint total_points,
                            __constant uint* offsets,
                            __global float4* all_points,
                            __global float4* out_points)
{
    uint gid = get_global_id(0);
    uint pt_idx = gid*num_points;

    uint offset = 0;
    if(gid > 0) { offset = offsets[gid-1]; }

    for(int i=0; i<num_points && pt_idx<total_points; i++, pt_idx++)
    {
        float4 point = all_points[pt_idx];
        if(point.s3 == 0.0f)
        {
            point.s3 = 1.0f;
            all_points[pt_idx] = point;
            out_points[offset++] = point;
        }
    }
}

__kernel void copy_raw_points(  const uint num_points,
                                __global float4* all_points,
                                __global float2* out_points)
{
    for(int i=0; i<num_points; i++)
    {
        out_points[i] = all_points[i].s02;
    }
}