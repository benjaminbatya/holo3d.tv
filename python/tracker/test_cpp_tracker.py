__author__ = 'benjamin'


from base_funcs import *

import unittest
import time

from holo3d import CPPTracker

from base_tracker import TrackerSettings
from naive_tracker import NaiveTracker

from holo3d_utils import INFO

class CPP_Blob_Tracker(CPPTracker):
    def __init__(self, settings):
        tups = settings.tuple()
        super(CPP_Blob_Tracker, self).__init__(tups)

    def set_clusters(self, clusters):
        # Convert the clusters to tuples
        new_clusters = []
        for c in clusters:
            tup = c.tuple()
            new_clusters.append(tup)
        super(CPP_Blob_Tracker, self).set_clusters(new_clusters)

    def clusters(self):
        clusters = super(CPP_Blob_Tracker, self).clusters()

        return self._convert_tuple_to_cluster(clusters)

    def _convert_tuple_to_cluster(self, clusters):
        # Convert the clusters from tuples to Clusters
        temp = []
        for c in clusters:
            temp.append(Cluster(c))

        return temp

    def track_recalc_existing_clusters(self):
        clusters = super(CPP_Blob_Tracker, self).track_recalc_existing_clusters()

        return self._convert_tuple_to_cluster(clusters)

    def merge_clusters(self):
        merged_clusters = super(CPP_Blob_Tracker, self).merge_clusters()

        merged_clusters = self._convert_tuple_to_cluster(merged_clusters)

        num_unsettled = 0
        for c in merged_clusters:
            if not c.settled:
                num_unsettled += 1

        return (merged_clusters, num_unsettled)

    def partitioned_merge_clusters(self):
        merged_clusters = super(CPP_Blob_Tracker, self).partitioned_merge_clusters()

        merged_clusters = self._convert_tuple_to_cluster(merged_clusters)

        num_unsettled = 0
        for c in merged_clusters:
            if not c.settled:
                num_unsettled += 1

        return (merged_clusters, num_unsettled)

    def run_EM(self):
        clusters = super(CPP_Blob_Tracker, self).run_EM()

        return self._convert_tuple_to_cluster(clusters)

    def cluster_points(self):
        clusters = super(CPP_Blob_Tracker, self).cluster_points()

        return self._convert_tuple_to_cluster(clusters)

    def regen_clusters(self):
        clusters = self.cluster_points()

        return clusters

    def track_clusters(self):
        clusters = super(CPP_Blob_Tracker, self).track_clusters()

        return self._convert_tuple_to_cluster(clusters)


if __name__ == "__main__":

    (LEFT, RIGHT, BOTTOM, TOP) = (-200, 200, -200, 200)
    (RADIUS, PERCENT_SAMPLES) = (30.0, 0.1)

    class Test_CPPTracker(unittest.TestCase):
        def setUp(self):
            init_rand()

            settings = TrackerSettings(LEFT, RIGHT, TOP, BOTTOM, RADIUS, PERCENT_SAMPLES)

            self.naive = NaiveTracker(settings)

            self.cpp = CPP_Blob_Tracker(settings)

            self.blobs = gen_blobs(LEFT, RIGHT, BOTTOM, TOP) #, 20, 200)

            (self.indices, self.points, self.clusters) = init_data(self.blobs, PERCENT_SAMPLES, RADIUS**2)

            self.naive.set_points(self.points)
            self.cpp.set_points(self.points)

            self.naive.set_clusters(self.clusters)

            self.naive.regen_clusters()

            # NOTE: use the naive clusters to initialize the cpp tracker
            self.cpp.set_clusters(self.naive.clusters())

            # total = 0
            # for blob in self.blobs:
            #     total += blob['count']
            #
            # print "Total = %r" % total

        def tearDown(self):
            pass

        def test_map_clusters(self):

            start = time.clock()

            mapped_naive = self.naive.track_map_points_to_indices()

            end = time.clock()
            naive_delta = end - start
            print("delta for naive track_map_points_to_indices = %r ms" % (naive_delta*1000))
            start = time.clock()

            mapped_cpp = self.cpp.track_map_points_to_indices()

            end = time.clock()
            cpp_delta = end - start
            print("delta for cpp track_map_points_to_indices = %r ms" % (cpp_delta*1000))

            print("times speed up = x%r" % (naive_delta / cpp_delta))

            self.assertEquals(mapped_cpp.shape, mapped_naive.shape)
            self.assertEquals(mapped_cpp.dtype, mapped_naive.dtype)

            for idx in xrange(len(self.points)):
                self.assertEquals(mapped_cpp[idx], mapped_naive[idx])

        def test_reduce_clusters(self):
            start = time.clock()

            clustered_idx_naive = self.naive.track_reduce_points()

            end = time.clock()
            naive_delta = end - start
            print("delta for naive track_reduce_points = %r ms" % (naive_delta*1000))
            start = time.clock()

            clustered_idx_cpp = self.cpp.track_reduce_points()

            end = time.clock()
            cpp_delta = end - start
            print("delta for cpp track_reduce_points = %r ms" % (cpp_delta*1000))

            print("times speed up = x%r" % (naive_delta / cpp_delta))

            self.assertEquals(len(clustered_idx_cpp), len(self.naive.clusters())+1)

            self.assertEquals(len(clustered_idx_cpp), len(clustered_idx_naive))

            for idx in xrange(len(clustered_idx_naive)):
                self.assertEquals(clustered_idx_cpp[idx], clustered_idx_naive[idx])

        def test_recalc_clusters(self):

            start = time.clock()

            naive_clusters = self.naive.track_recalc_existing_clusters()

            end = time.clock()
            naive_delta = end - start
            print("delta for naive.track_recalc_existing_clusters = %r ms" % (naive_delta*1000))

            # for c in naive_clusters:
            #     print c

            start = time.clock()

            cpp_clusters = self.cpp.track_recalc_existing_clusters()

            end = time.clock()
            cpp_delta = end - start
            print("delta for cpp track_recalc_existing_clusters = %r ms" % (cpp_delta*1000))

            print("times speed up = x%r" % (naive_delta / cpp_delta))

            self.assertEquals(len(cpp_clusters), self.cpp.num_clusters())

            self.assertEquals(len(cpp_clusters), len(cpp_clusters))

            for idx in xrange(self.cpp.num_clusters()):
                self.assertTrue(cpp_clusters[idx].equal(naive_clusters[idx], report=True))

        def test_regen_clusters(self):

            naive_clusters = self.naive.regen_clusters()

            cpp_clusters = self.cpp.regen_clusters()

            naive_clusters.sort(key=lambda c: c.mean[0])
            cpp_clusters.sort(key=lambda c: c.mean[0])

            self.assertEquals(len(cpp_clusters), self.cpp.num_clusters())

            self.assertEquals(len(cpp_clusters), len(naive_clusters))

            for idx in xrange(self.cpp.num_clusters()):
                self.assertTrue(cpp_clusters[idx].equal(naive_clusters[idx], report=True))

        def test_track_clusters(self):

            start = time.clock()

            naive_clusters = self.naive.track_clusters()

            end = time.clock()
            naive_delta = end - start
            print("delta for naive.track_clusters = %r ms" % (naive_delta*1000))
            start = time.clock()

            cpp_clusters = self.cpp.track_clusters()

            end = time.clock()
            cpp_delta = end - start
            print("delta for cpp.track_clusters = %r ms" % (cpp_delta*1000))

            print("times speed up = x%r" % (naive_delta / cpp_delta))

            self.assertEquals(len(cpp_clusters), self.cpp.num_clusters())

            self.assertEquals(len(cpp_clusters), len(naive_clusters))

            for idx in xrange(self.cpp.num_clusters()):
                self.assertTrue(cpp_clusters[idx].equal(naive_clusters[idx], report=True))

    class Test_CPP_Clustering(unittest.TestCase):
        def setUp(self):
            init_rand()

            settings = TrackerSettings(LEFT, RIGHT, TOP, BOTTOM, RADIUS, PERCENT_SAMPLES)

            self.naive = NaiveTracker(settings)

            self.cpp = CPP_Blob_Tracker(settings)

            self.blobs = gen_blobs(LEFT, RIGHT, BOTTOM, TOP)#, 40, 200)

            # total = 0
            # for blob in self.blobs:
            #     total += blob['count']
            #
            # print "Total = %r" % total

            (self.indices, self.points, self.clusters) = init_data(self.blobs, PERCENT_SAMPLES, RADIUS**2)

            # print "Num clusters = %r" % len(self.clusters)

            self.naive.set_points(self.points)
            self.cpp.set_points(self.points)

            self.naive.set_clusters(self.clusters)

            # for c in self.clusters:
            #     INFO("passed in cluster = %s" % c)

            self.cpp.set_clusters(self.clusters)

        def test_merge_clusters(self):

            start = time.clock()

            (naive_clusters, naive_unsettled) = self.naive.merge_clusters(self.clusters)

            end = time.clock()
            naive_delta = end - start
            print("delta for naive.merge_clusters = %r ms" % (naive_delta*1000))
            start = time.clock()

            (cpp_clusters, cpp_unsettled) = self.cpp.merge_clusters()

            end = time.clock()
            cpp_delta = end - start
            print("delta for cpp.merge_clusters = %r ms" % (cpp_delta*1000))

            print("times speed up = x%r" % (naive_delta / cpp_delta))

            # for c in naive_clusters:
            #     INFO("naive cluster = %s" % c)
            #
            # for c in cpp_clusters:
            #     INFO("cpp cluster = %s" % c)

            self.assertEquals(cpp_unsettled, naive_unsettled)
            self.assertEquals(len(cpp_clusters), len(naive_clusters))

            for idx in xrange(len(cpp_clusters)):
                self.assertTrue(cpp_clusters[idx].equal(naive_clusters[idx], report=True))

        def test_partitioned_merge_clusters(self):

            start = time.clock()

            (naive_clusters, naive_unsettled) = self.naive.partitioned_merge_clusters(self.clusters)

            end = time.clock()
            naive_delta = end - start
            print("delta for naive.partitioned_merge_clusters = %r ms" % (naive_delta*1000))
            start = time.clock()

            (cpp_clusters, cpp_unsettled) = self.cpp.partitioned_merge_clusters()

            end = time.clock()
            cpp_delta = end - start
            print("delta for cpp.partitioned_merge_clusters = %r ms" % (cpp_delta*1000))

            print("times speed up = x%r" % (naive_delta / cpp_delta))

            naive_clusters.sort(key=lambda c: c.mean[0])
            cpp_clusters.sort(key=lambda c: c.mean[0])
            #
            # for c in naive_clusters:
            #     INFO("naive cluster = %s" % c)
            #
            # for c in cpp_clusters:
            #     INFO("cpp cluster = %s" % c)

            self.assertEquals(cpp_unsettled, naive_unsettled)
            self.assertEquals(len(cpp_clusters), len(naive_clusters))

            for idx in xrange(len(cpp_clusters)):
                self.assertTrue(cpp_clusters[idx].equal(naive_clusters[idx], report=True))

        def test_EM(self):
            start = time.clock()

            naive_clusters = self.naive.run_new_EM(self.clusters, self.points)

            end = time.clock()
            naive_delta = end - start
            print("delta for naive.run_EM = %r ms" % (naive_delta*1000))
            start = time.clock()

            cpp_clusters = self.cpp.run_EM()

            end = time.clock()
            cpp_delta = end - start
            print("delta for cpp.run_EM = %r ms" % (cpp_delta*1000))

            print("times speed up = x%r" % (naive_delta / cpp_delta))

            # for c in naive_clusters:
            #     INFO("naive cluster = %s" % c)
            #
            # for c in cpp_clusters:
            #     INFO("cpp cluster = %s" % c)

            # self.assertEquals(len(cpp_clusters), self.cpp.num_clusters())
            self.assertEquals(len(cpp_clusters), len(naive_clusters))

            for idx in xrange(len(cpp_clusters)):
                self.assertTrue(cpp_clusters[idx].equal(naive_clusters[idx], report=True))

        def test_cluster_points(self):
            start = time.clock()

            naive_clusters = self.naive.cluster_points(self.clusters, self.points)

            end = time.clock()
            naive_delta = end - start
            print("delta for naive.cluster_points = %r ms" % (naive_delta*1000))
            start = time.clock()

            cpp_clusters = self.cpp.cluster_points()

            end = time.clock()
            cpp_delta = end - start
            print("delta for cpp.cluster_points = %r ms" % (cpp_delta*1000))

            print("times speed up = x%r" % (naive_delta / cpp_delta))

            naive_clusters.sort(key=lambda c: c.width_sq)
            cpp_clusters.sort(key=lambda c: c.width_sq)

            # for c in naive_clusters:
            #     INFO("naive cluster = %s" % c)
            #
            # for c in cpp_clusters:
            #     INFO("cpp cluster = %s" % c)

            # self.assertEquals(len(cpp_clusters), self.cpp.num_clusters())
            self.assertEquals(len(cpp_clusters), len(naive_clusters))

            for idx in xrange(len(cpp_clusters)):
                self.assertTrue(cpp_clusters[idx].equal(naive_clusters[idx], report=True))

        def test_cluster_points_alot(self):
            self.blobs = gen_blobs(LEFT, RIGHT, BOTTOM, TOP, 80, 200)

            # total = 0
            # for blob in self.blobs:
            #     total += blob['count']
            #
            # print "Total = %r" % total

            (self.indices, self.points, self.clusters) = init_data(self.blobs, PERCENT_SAMPLES, RADIUS**2)

            print "Num clusters = %r" % len(self.clusters)

            self.naive.set_points(self.points)
            self.cpp.set_points(self.points)

            self.naive.set_clusters(self.clusters)

            self.cpp.set_clusters(self.clusters)

            start = time.clock()

            naive_clusters = self.naive.cluster_points(self.clusters, self.points)

            end = time.clock()
            naive_delta = end - start
            print("delta for naive.cluster_points_alot = %r ms" % (naive_delta*1000))
            start = time.clock()

            cpp_clusters = self.cpp.cluster_points()

            end = time.clock()
            cpp_delta = end - start
            print("delta for cpp.cluster_points_alot = %r ms" % (cpp_delta*1000))

            print("times speed up = x%r" % (naive_delta / cpp_delta))

            naive_clusters.sort(key=lambda c: c.width_sq)
            cpp_clusters.sort(key=lambda c: c.width_sq)

            # for c in naive_clusters:
            #     INFO("naive cluster = %s" % c)
            #
            # for c in cpp_clusters:
            #     INFO("cpp cluster = %s" % c)

            # self.assertEquals(len(cpp_clusters), self.cpp.num_clusters())
            self.assertEquals(len(cpp_clusters), len(naive_clusters))

            for idx in xrange(len(cpp_clusters)):
                self.assertTrue(cpp_clusters[idx].equal(naive_clusters[idx], report=True))

    unittest.main()