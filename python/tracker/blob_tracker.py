__author__ = 'benjamin'

from PySide import QtCore

import pyopencl as cl
import pyopencl.array as cl_array
import numpy as np
import sys
import os
import time
import math

from holo3d_utils import INFO, ERROR

from naive_tracker import NaiveTracker
from base_tracker import TrackerSettings
from test_cpp_tracker import CPP_Blob_Tracker

class Blob:
    def __init__(self, pos, size):
        self._position = np.array(pos, dtype=np.float32).reshape(3,1)
        self._size = np.array(size, dtype=np.float32).reshape(3,1)

    def position(self):
        return self._position

    def size(self):
        return self._size

    def __str__(self):
        return "(mean=%r,size=%r" % (self._position.tolist(), self._size.tolist())

class BlobTracker(QtCore.QObject):
    # This is maximum amount of time (in microsecs) to allow to be changed before a complete blob retracking is required
    MAX_CHANGE_AMOUNT = 200000

    def __init__(self, parent, model, compositer):
        super(BlobTracker, self).__init__(parent)

        self._clinit()

        self._model = model
        self._model.project_changed.connect(self.project_changed)
        self._model.time_value_changed.connect(self.time_changed)

        self._compositor = compositer

        self._active = False
        self._tracker = None

        self.project_changed()

    def active(self): return self._active

    def set_active(self, flag):
        if flag:
            INFO("Starting to track, reinitializing all clusters")
            if not self._active:
                self.init_all_estimates()
                # for blob in self.estimates():
                #     INFO("Blob = %s" % blob)

                # INFO("Points mean = %s" % self._compositor.points_mean())

        else:
            INFO("Stopping tracking")

        self._active = flag

    # Note: Move this boilerplate code into a common base class for all of the users of OpenCL
    def _clinit(self):
        # Setup the context
        platforms = cl.get_platforms()
        from pyopencl.tools import get_gl_sharing_context_properties
        if sys.platform == "darwin":
            self._ctx = cl.Context(properties=get_gl_sharing_context_properties(),
                             devices=[])
        else:
            self._ctx = cl.Context(properties=[
                (cl.context_properties.PLATFORM, platforms[0])]
                + get_gl_sharing_context_properties(), devices=None)
        self._queue = cl.CommandQueue(self._ctx)

        # Load the program
        # import inspect
        # path = inspect.currentframe()
        # INFO("current working path = %r" % __file__)
        f = open("tracker/blob_tracker.cl", 'r')
        fstr = "".join(f.readlines())
        f.close()

        # HACK: Disable program caching to allow opencl's printf to work
        # See http://documen.tician.de/pyopencl/runtime.html#programs-and-kernels
        # and http://www.thebigblob.com/opencl-bugs/
        os.environ["PYOPENCL_NO_CACHE"] = "1"
        self._program = cl.Program(self._ctx, fstr).build()

        # Calculate all of the variables needed to run the program
        device = self._ctx.devices[0]
        self._group_size = self._program.prefix_sum.get_work_group_info(cl.kernel_work_group_info.WORK_GROUP_SIZE, device)

        self._mf = cl.mem_flags

    @QtCore.Slot()
    def project_changed(self):
        self._full_recalc = True

        self._clusters = []

        self._prev_time = sys.maxint        # << Used to track how much the time value changed

    @QtCore.Slot()
    def time_changed(self):
        if(not self._active): return

        visible_points = self.get_all_points()
        volume = self._model.volume_of_interest()
        origin = volume.origin()
        volume_size = volume.size()

        start = time.clock()

        self._tracker.set_points(visible_points)

        self._tracker.track_clusters()

        end = time.clock()
        delta = end - start
        INFO("time to track clusters = %r ms" % (delta*1000))

        self._extract_clusters(origin, volume_size)

    PERCENTAGE_SAMPLE = 0.1         # << Select 10% of the points for sampling
    RADIUS = 3.0                    # << Set the estimate radius to 3inches
    MAX_ITERATIONS = 10             # << Maximum number of times to relax the estimates before giving up
    MIN_POINTS_IN_ESTIMATE = 200    # << Minimum number of points that each estimate must contain

    # This is the amount of inner border to leave for recalculating new incoming clusters
    BORDER_WIDTH = 1

    def get_all_points(self):
        # Refind all clusters
        points_cl = self._compositor.valid_points()
        num_points = self._compositor.num_points()

        if num_points == 0: return None

        gl_objects = [points_cl]

        cl.enqueue_acquire_gl_objects(self._queue, gl_objects)

        copies_cl = cl.Buffer(self._ctx, self._mf.WRITE_ONLY, num_points*4*2)

        global_shape = (1,)
        local_shape = None
        kernel_args = (np.uint32(num_points), points_cl, copies_cl)
        self._program.copy_raw_points(self._queue, global_shape, local_shape, *(kernel_args))

        cl.enqueue_release_gl_objects(self._queue, gl_objects)

        copies_np = np.ndarray((num_points, 2), dtype=np.float32)
        cl.enqueue_copy(self._queue, copies_np, copies_cl)

        self._queue.finish()

        return copies_np

    def init_all_estimates(self):
        '''This resets the tracking for all estimates'''

        visible_points = self.get_all_points()
        if visible_points is None: return

        # Run all of the serial estimate finding code here
        print "Num of returned points = ", visible_points.shape[0]

        # Get the x and z bounds from the VolumeNode
        volume = self._model.volume_of_interest()
        origin = volume.origin()
        volume_size = volume.size()

        width = (volume_size[0] / 2)
        height = (volume_size[2] / 2)

        left = origin[0] - width + BlobTracker.BORDER_WIDTH
        right = origin[0] + width - BlobTracker.BORDER_WIDTH
        bottom = origin[2] - height + BlobTracker.BORDER_WIDTH
        top = origin[2] + height - BlobTracker.BORDER_WIDTH

        # Keep the translation and rotation+scale separate because tracked points are not in homogenous coordinates
        origin = np.array((origin[0], origin[2])).reshape(2,)

        angle = volume.rotation()
        sina = math.sin(angle)
        cosa = math.cos(angle)
        rot_mat = np.array(((cosa, -sina), (sina, cosa)))

        settings = TrackerSettings(left, right, bottom, top, BlobTracker.RADIUS, BlobTracker.PERCENTAGE_SAMPLE,
                                   min_points=BlobTracker.MIN_POINTS_IN_ESTIMATE,
                                   max_iterations=BlobTracker.MAX_ITERATIONS,
                                   origin=origin, rot_mat=rot_mat)

        # Set the dims in the tracker
        # self._tracker = NaiveTracker(settings)
        self._tracker = CPP_Blob_Tracker(settings)

        start = time.clock()

        self._tracker.set_points(visible_points)

        self._tracker.create_clusters()

        self._tracker.regen_clusters()

        end = time.clock()
        delta = end - start
        INFO("time to regen clusters = %r ms" % (delta*1000))

        self._extract_clusters(origin, volume_size)

    def _extract_clusters(self, origin, volume_size):
        clusters = self._tracker.clusters()
        self._clusters = []
        idx = 0
        for c in clusters:
            size = np.array((c.width_sq, (volume_size[1] / 2) ** 2, c.height_sq))
            size = np.sqrt(size)
            INFO("cluster %r: (%s)" % ( idx, c ) )
            idx += 1
            size *= 2
            blob = Blob((c.mean[0], origin[1], c.mean[1]), size)
            self._clusters.append(blob)

    def estimates(self):
        return self._clusters
