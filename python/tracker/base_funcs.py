__author__ = 'benjamin'

import numpy as np
from base_tracker import Cluster

def init_rand(seed = 100):
    np.random.seed(seed)

def gen_points(blobs):
    # Generate the initial set of points
    x = []
    y = []

    for blob in blobs:
        # Generate a random blob
        new_x = np.random.standard_normal(blob['count']) * blob['w'] + blob['pos'][0]
        new_y = np.random.standard_normal(blob['count']) * blob['h'] + blob['pos'][1]

        x = np.hstack((x, new_x))
        y = np.hstack((y, new_y))

    points = np.vstack((x,y)).T

    return points

def maximize(points, pos, width_sq, height_sq):
    '''
    :param points:
    :param pos:
    :param within_delta:
    :return: (list of points within delta of pos)
    '''
    ret = []

    num_pts = len(points)
    for i in xrange(num_pts):
        vec = pos - points[i]
        check = vec.dot(vec)

        if check < (width_sq + height_sq):
            ret.append(i)

    return ret

def expectation(points, indices):
    '''
    :param points:
    :return: (mean of points, radius of points from mean)
    '''
    mean = np.sum(points[indices], axis=0)
    mean /= len(indices)

    max_width_sq = 0.0
    max_height_sq = 0.0
    for i in indices:
        vec = mean - points[i]
        width_sq = vec[0]*vec[0]
        height_sq = vec[1]*vec[1]

        if max_width_sq < width_sq:
            max_width_sq = width_sq

        if max_height_sq < height_sq:
            max_height_sq = height_sq

    return(mean, max_width_sq, max_height_sq)

def rand(low, high):
    return int(np.random.random() * (high - low) + low)

# From http://stackoverflow.com/questions/14267555/how-can-i-find-the-smallest-power-of-2-greater-than-n-in-python
# was bit_shift_length
def closest_pow_of_2(x):
    return 1<<(x-1).bit_length()

# From http://stackoverflow.com/questions/8866046/python-round-up-integer-to-next-hundred
def round_up_to_multiple(number, multiple):
    num = number + (multiple - 1)
    return num - (num % multiple)

def gen_blobs(left, right, bottom, top, max_blobs=10, max_count=200):
    # Generate the initial data for the blobs
    blobs = []

    # Generate the blob parameters
    for i in xrange(rand(1, max_blobs)):
        blob = {'count':rand(20, max_count),
             'x':rand(left, right), 'y':rand(bottom, top),
             'w':rand(5, 25), 'h':rand(5, 25),
             'dir_x':rand(-10,10), 'dir_y':rand(-10, 10)}
        blob['pos'] = np.array([ blob['x'], blob['y'] ])
        blob['size'] = np.array([ blob['w'], blob['h'] ])
        blob['dir'] = np.array([ blob['dir_x'], blob['dir_y'] ])
        blobs.append( blob )
    # print("Num blobs = %r" % len(blobs))

    return blobs

def init_data(blobs, percent_samples, radius_sq):
    points = gen_points(blobs)

    num_points = points.shape[0]
    num_samples = int(num_points * percent_samples)

    indices = np.random.choice(num_points, num_samples)

    clusters = []
    for i in indices:
        # first index is the mean of the cluster
        # Second index is the width squared
        # Third index is the height squared
        # Fourth index indicates if the cluster is settled or not
        clusters.append(Cluster(points[i], radius_sq, radius_sq, False))

    return (indices, points, clusters)


if __name__ == "__main__":
    import unittest

    class TestFuncs(unittest.TestCase):

        def test_closest_pow_of_2(self):

            self.assertEquals(closest_pow_of_2(0), 2)
            self.assertEquals(closest_pow_of_2(1), 1)
            self.assertEquals(closest_pow_of_2(2), 2)
            self.assertEquals(closest_pow_of_2(3), 4)
            self.assertEquals(closest_pow_of_2(5), 8)
            self.assertEquals(closest_pow_of_2(313), 512)
            self.assertEquals(closest_pow_of_2(1023), 1024)
            self.assertEquals(closest_pow_of_2(1024), 1024)
            self.assertEquals(closest_pow_of_2(1025), 2048)

        def test_round_up_to_multiple(self):

            self.assertEquals(round_up_to_multiple(0, 1), 0)
            self.assertEquals(round_up_to_multiple(1, 42), 42)
            self.assertEquals(round_up_to_multiple(33, 3), 33)
            self.assertEquals(round_up_to_multiple(33, 7), 35)
            self.assertEquals(round_up_to_multiple(123, 11), 132)

    unittest.main()