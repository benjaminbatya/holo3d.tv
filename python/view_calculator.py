__author__ = 'benjamin'

from holo3d_utils import INFO, ERROR
import numpy as np
import cv2
import math

def corners(data, grid):
    # print "New Shape =", data.shape

    # Find the chessboard corners. It works!!!
    grid = (grid[1]-1, grid[0]-1)

    gray = cv2.cvtColor(data, cv2.COLOR_RGB2GRAY)

    # Shrink the grid by one on each side so findChessboardCorners works properly
    # Also flip the indices

    ret, corners = cv2.findChessboardCorners(gray, grid)
    #, flags=cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE)

    ret_corners = None
    bbox = None

    desired_num_corners = grid[0] * grid[1]
    if ret or (corners is not None and corners.shape[0] == desired_num_corners): # corners is not None and corners.shape[0] > 0:
        # INFO("Corners found!")

        # This might help, not sure...
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.1)
        cv2.cornerSubPix(gray, corners, (5,5), (-1,-1), criteria)

        ret_corners = []
        for i in corners:
            x,y = i.ravel()
            ret_corners.append((x,y))
            # print x,y

        ret_corners.reverse()

        # Calculate the bounding box of the corners
        # Note: I have to beat the bbox_data into this weird shape because that's what boundingRect expects
        # Really I should write my own bounding rect method...
        bbox_data = np.array(ret_corners, dtype=np.float32).reshape(len(ret_corners), 1, 2)
        bbox = cv2.boundingRect(bbox_data)
    # else:
    #     if corners is not None:
    #         ERROR("ret is false, corners.shape[0]=%r" % corners.shape[0])
    #     else:
    #         ERROR("ret is false, corners is None")

    return ret_corners, bbox

def camera_position(color_name, corners, rect, camera_matrix, dist_coeffs):

    # NOTE: have to adjust the corners so the grid lines up correctly
    new_corners = []
    for c in corners:
        new_c = (c[0]+rect.left(), c[1]+rect.top())
        new_corners.append(new_c)

    new_corners = np.array(new_corners)
    # Use a lookup table to classify the XYZ location of the corner points

    # This is the amount to rotate the grid of points to get them into their correct location
    rotation_map = {
        'black': 0,
        'blue': +90,
        'red': +180,
        'green': +270,
        'white': None }

    # HACK: I screwed up getting the orientation of
    # each of the faces on the cube to be the same
    # Red and green are rotated 180 degrees from
    # black and blue and it matters to the findChessboardGrid
    # algorithm. This is a temporary hack until I
    # build another cube
    if color_name == 'black' or color_name == 'blue':
        x = np.linspace(-4., 4., 9)
        y = np.linspace(-2.5, 2.5, 6)
    else:
        x = np.linspace(4., -4., 9)
        y = np.linspace(2.5, -2.5, 6)


    yy, xx = np.meshgrid(y, x)

    points = np.array(zip( xx.ravel(), yy.ravel()) )
    z_axis = np.ones(shape=(points.shape[0],)) * 6
    points = np.column_stack((points, z_axis))

    angle = math.radians(rotation_map[color_name])

    # Always rotate around the y axis
    rot_matrix = np.array([[math.cos(angle), 0., math.sin(angle)],
                          [0.,         1.,          0.],
                          [-math.sin(angle), 0.,  math.cos(angle)] ])
    new_vectors = []
    for vec in points:
        # print vec
        new_vectors.append(rot_matrix.dot(vec))

    # # small test, rotate 45 degrees around the z-axis
    # # NOTE: REMOVE ME!!
    # angle = math.radians(45)
    # test_matrix = np.array([[+math.cos(angle), math.sin(angle), 0.0],
    #                         [-math.sin(angle), math.cos(angle), 0.0],
    #                         [0.0             , 0.0,             1.0]])
    # test_vectors = []
    # for vec in new_vectors:
    #     vec = np.dot(test_matrix, vec)
    #     test_vectors.append(vec)
    #
    # new_vectors = test_vectors

    # print "original vectors="
    # print np.array(vectors)

    new_vectors = np.array(new_vectors, dtype=np.float32)
    # low_values = np.abs(new_vectors) < 0.001
    # new_vectors[low_values] = 0

    # 5) Use solvePnP calculate the transformation between the cube's XYZ locations and the frame's UV locations

    # if camera_matrix is None:
    #     # Camera parameters from the book
    #     camera_matrix = [524.0,   0.0, 320.0, # 316.7,
    #            0.0, 524.0, 240.0, # 238.5,
    #            0.0,   0.0,   1.0]

    # camera_matrix =  np.array(camera_matrix, dtype=np.float32).reshape((3,3)) # The camera calibration matrix MUST be 3x3 for solvePnP
    # if dist_coeffs is None:
    #     # Distance coefficents from the book
    #     dist_coeffs = [0.2402, -0.6861, 0.0, 0.0, -0.0015]

    # dist_coeffs = np.array(dist_coeffs, dtype=np.float32)

    succeed, rotate_vec, translate_vec = cv2.solvePnP(new_vectors, new_corners, camera_matrix, dist_coeffs)

    if not succeed:
        return None, None

    # INFO("results from solvePnP:")
    # print(rotate_vec)
    # print(translate_vec)

    return rotate_vec, translate_vec
