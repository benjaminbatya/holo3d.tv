#!/usr/bin/env python
import sys
import os.path
import os

# Set up the software for a ubuntu 14.04 server.
# Requires eth0 be pointing to the outside and
# eth1 be connected to the camera's network

TEST_EXEC = False
def myExec(cmd):
	print("executing '" + cmd + "'")
	if(TEST_EXEC is not True): os.system(cmd)

def installSysFile(camera, dirname, fname):
    fullpath = dirname + "/" + fname
    bakfile = fullpath + ".orig"

    # don't copy over the original file if it exists already
    cmd = "ssh root@" + camera + " cp " + fullpath + " " + bakfile
    myExec(cmd)
     
    # copy over the system file
    # scp rc.local root@camera4:/etc/rc.local
    cmd = "scp " + fname + " root@" + camera + ":" + fullpath
    myExec(cmd)

def main(argv):
    if len(argv) < 2:
        print "Must specify an argument!"
        return
    
    # First decide which camera to install the configuration files to
    camera = argv[1]
    
    # Then install the udev rule files
    # installSysFile("/etc/udev/rules.d", "primesense-usb.rules")
    #installSysFile("/etc/udev/rules.d", "51-kinect.rules")
        
    # Install the kernel tweaks to allow the RGB and RAW large buffers to be received well
    installSysFile(camera, "/etc/sysctl.d", "60-holo3d.conf")
    
    # Install the ntp.conf
    installSysFile(camera, "/etc", "ntp.conf")
    
    # Install the new rc.local file
    installSysFile(camera, "/etc", "rc.local")

    # Install the sudo file
    installSysFile(camera, "/etc/sudoers.d", "benjamin_sudo")
    
    # Restart the camera now so that the changes are applied
    myExec("ssh root@" + camera + " shutdown -r now")
    

if __name__ == "__main__":
    main(sys.argv)
    
    