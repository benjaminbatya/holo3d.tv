#!/usr/bin/python

import sys
import os.path
import os

# Set up the software for a ubuntu 14.04 server.
# Requires eth0 be pointing to the outside and
# eth1 be connected to the camera's network

TEST_EXEC = False
def myExec(cmd):
        print("executing '" + cmd + "'")
        if(TEST_EXEC is not True): os.system(cmd)

if __name__ == "__main__":
	# This will create a ssh tunnel to the ODROID at the specified ip address
	# Use: 
	#   ssh -p 38080 root@localhost
	#   scp -P 38080 local_file root@localhost:
	#   scp -P 38080 root@localhost:remote_file .
	
	# Check the arguments
	tunnel_addr = "1"
	if len(sys.argv) >= 2: tunnel_addr = sys.argv[1]

	cmd = ( "ssh -v -L3808{0}:localhost:3808{0} 192.168.1.104 -t "
		"ssh -v -L3808{0}:localhost:22 root@192.168.10.10{0}").format(tunnel_addr)

	myExec(cmd)


	
